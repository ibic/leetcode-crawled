---
title: "Make The String Great"
weight: 1412
#id: "make-the-string-great"
---
## Description
<div class="description">
<p>Given a string <code>s</code> of lower and upper case English letters.</p>

<p>A good string is a string which doesn&#39;t have&nbsp;<strong>two adjacent characters</strong> <code>s[i]</code> and <code>s[i + 1]</code> where:</p>

<ul>
	<li><code>0 &lt;= i &lt;= s.length - 2</code></li>
	<li><code>s[i]</code> is a lower-case letter and <code>s[i + 1]</code> is the same letter but in upper-case&nbsp;or <strong>vice-versa</strong>.</li>
</ul>

<p>To make the string good, you can choose <strong>two adjacent</strong> characters that make the string bad and remove them. You can keep doing this until the string becomes good.</p>

<p>Return <em>the string</em> after making it good. The answer is guaranteed to be unique under the given constraints.</p>

<p><strong>Notice</strong> that an empty string is also good.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;leEeetcode&quot;
<strong>Output:</strong> &quot;leetcode&quot;
<strong>Explanation:</strong> In the first step, either you choose i = 1 or i = 2, both will result &quot;leEeetcode&quot; to be reduced to &quot;leetcode&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abBAcC&quot;
<strong>Output:</strong> &quot;&quot;
<strong>Explanation:</strong> We have many possible scenarios, and all lead to the same answer. For example:
&quot;abBAcC&quot; --&gt; &quot;aAcC&quot; --&gt; &quot;cC&quot; --&gt; &quot;&quot;
&quot;abBAcC&quot; --&gt; &quot;abBA&quot; --&gt; &quot;aA&quot; --&gt; &quot;&quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;s&quot;
<strong>Output:</strong> &quot;s&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 100</code></li>
	<li><code>s</code> contains only lower and upper case English letters.</li>
</ul>
</div>

## Tags
- String (string)
- Stack (stack)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Simple Solution using Stack [ Explained]
- Author: Shradha1994
- Creation Date: Sun Aug 09 2020 12:07:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 09 2020 12:10:17 GMT+0800 (Singapore Standard Time)

<p>
**Explanation** -
It should be noted that the difference between the any lowercase and uppercase alphabet is 32. Example - ASCII value of `a is 97` and `A is 65` , 97-65 = 32

Using same trick, we can delete adjacent characters with absolute difference of 32.

**`Example`** -
If current character is A and a is at top of stack, we pop a and dont insert A.
Otherwise, we insert the current character in stack.
Collect result in String at the end.

```
class Solution {
    public String makeGood(String s) {
        Stack<Character> stack = new Stack();
        for(int i=0;i<s.length();i++){
            if(!stack.isEmpty() && Math.abs(stack.peek()-s.charAt(i)) == 32)
                stack.pop();
            else
                stack.push(s.charAt(i));
        }
        char res[] = new char[stack.size()];
        int index = stack.size()-1;
        while(!stack.isEmpty()){
            res[index--] = stack.pop();
        }
        return new String(res);
    }
```

`*Feel free to ask questions in comment section, do upvote if you understood the solution*`
</p>


### C++ Brute-Force + Two Pointers
- Author: votrubac
- Creation Date: Sun Aug 09 2020 12:02:13 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 21 2020 13:48:23 GMT+0800 (Singapore Standard Time)

<p>
#### Brute-Force
Just do what the problem asks us to do until you can\'t.

```cpp
string makeGood(string s, int sz = 0) {
    while (sz != s.size()) {
        sz = s.size();
        for (int i = 0; i + 1 < s.size(); ++i)
            if (abs(s[i] - s[i + 1]) == 32)
                s = s.substr(0, i) + s.substr(i + 2);
    }
    return s;
}
```

**Complexity Analysis**
- Time: O(n * n) - in the worst case, we are going through the entire string n times, and also copyting it n times. 
- Memory: O(n) to create a copy of the string.

#### Optimized Solution
We can use the two pointers pattern to construct the resulting string in one pass. `i` indicates the current pointer in the input string, and `p` - the insertion point in the result string. If the current character in the input string "conflicts" with the last character of the result string - we decrease `p`. Otherwise, we copy the current character to the input string and increase `p`.

Note that we can use the same input string to hold the output - because `p` will never go ahead of `i`. In the end, we return the portion of the string till the insertion point `p`.

```cpp
string makeGood(string s, int p = 0) {
    for (int i = 0; i < s.size(); ++i) {
        if (p > 0 && abs(s[i] - s[p - 1]) == 32)
            --p;
        else
            s[p++] = s[i];
    }
    return s.substr(0, p);
}
```
**Complexity Analysis**
- Time: O(n). We are going through the string once.
- Memory: O(1), as we reuse the same string.
</p>


### Clean Python 3, stack O(N)
- Author: lenchen1112
- Creation Date: Sun Aug 09 2020 12:06:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 09 2020 12:09:40 GMT+0800 (Singapore Standard Time)

<p>
Just check if next character makes the string bad, if so pop the stack. Otherwise, push to the stack.
Time: `O(N)`
Space: `O(N)`
```
class Solution:
    def makeGood(self, s: str) -> str:
        result = []
        for c in s:
            if not result:
                result.append(c)
            elif result[-1].isupper() and result[-1].lower() == c:
                result.pop()
            elif result[-1].islower() and result[-1].upper() == c:
                result.pop()
            else:
                result.append(c)
        return \'\'.join(result)
```
</p>


