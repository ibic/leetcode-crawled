---
title: "Find All Duplicates in an Array"
weight: 419
#id: "find-all-duplicates-in-an-array"
---
## Description
<div class="description">
<p>Given an array of integers, 1 &le; a[i] &le; <i>n</i> (<i>n</i> = size of array), some elements appear <b>twice</b> and others appear <b>once</b>.</p>

<p>Find all the elements that appear <b>twice</b> in this array.</p>

<p>Could you do it without extra space and in O(<i>n</i>) runtime?</p>
</p>
<p><b>Example:</b><br/>
<pre>
<b>Input:</b>
[4,3,2,7,8,2,3,1]

<b>Output:</b>
[2,3]
</pre>
</div>

## Tags
- Array (array)

## Companies
- Microsoft - 3 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Lyft - 4 (taggedByAdmin: false)
- Pocket Gems - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Brute Force

**Intuition**

Check for a second occurrence of every element in the rest of the array.

**Algorithm**

When we iterate over the elements of the input array, we can simply look for any other occurrence of the current element in the rest of the array.

Since an element can only occur once _or_ twice, we don't have to worry about getting duplicates of elements that appear twice:
+ **Case - I:** If an element occurs only once in the array, when you look for it in the rest of the array, you'll find nothing.
+ **Case - II:** If an element occurs twice, you'll find the second occurrence of the element in the rest of the array. When you chance upon the second occurrence in a later iteration, it'd be the same as **Case - I** (since there are no more occurrences of this element in the rest of the array).

<iframe src="https://leetcode.com/playground/h8VPCzk5/shared" frameBorder="0" width="100%" height="327" name="h8VPCzk5"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n^2)$$. \
\
For each element in the array, we search for another occurrence in the rest of the array. Hence, for the $$i^{{th}}$$ element in the array, we might end up looking through all $$n - i$$ remaining elements in the worst case. So, we can end up going through about $$n^2$$ elements in the worst case. \
\
$$n-1 + n-2 + n-3 + .... + 1 + 0 \ = \ \sum_{1}^{n}(n-i) \ \simeq \ n^2$$

* Space complexity : No extra space required, other than the space for the output list.

<br />

---

#### Approach 2: Sort and Compare Adjacent Elements

**Intuition**

After sorting a list of elements, all elements of equivalent value get placed together. Thus, when you sort an array, equivalent elements form contiguous blocks.

**Algorithm**

1. Sort the array.
2. Compare every element with it's neighbors. If an element occurs more than once, it'll be equal to at-least one of it's neighbors.

To simplify:
1. Compare every element with its predecessor.
    + Obviously the first element doesn't have a predecessor, so we can skip it.
2. Once we've found a match with a predecessor, we can skip the next element entirely!
    + **Why?** Well, if an element matches with its predecessor, it cannot possibly match with its successor _as well_. Thus, the next iteration (i.e. comparison between the next element and the current element) can be safely skipped. 

<iframe src="https://leetcode.com/playground/8t4hbiLx/shared" frameBorder="0" width="100%" height="327" name="8t4hbiLx"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n \log{n}) + \mathcal{O}(n) \simeq \mathcal{O}(n \log{n})$$.

  + A performant comparison-based sorting algorithm will run in $$\mathcal{O}(n \log{n})$$ time. Note that this can be reduced to $$\mathcal{O}(n)$$ using a special sorting algorithm like [Radix Sort](https://en.wikipedia.org/wiki/Radix_sort).

  + Traversing the array after sorting takes linear time i.e. $$\mathcal{O}(n)$$.

* Space complexity : No extra space required, other than the space for the output list. Sorting can be done in-place.

<br />

---

#### Approach 3: Store Seen Elements in a Set / Map

**Intuition**

In [Approach 1](#approach-1-brute-force) we used two loops (one nested within the other) to look for two occurrences of an element. In almost all similar situations, you can usually substitute one of the loops with a set / map. Often, it's a worthy trade-off: **for a bit of extra memory, you can reduce the order of your runtime complexity.**

**Algorithm**

We store all elements that we've seen till now in a map / set. When we visit an element, we query the map / set to figure out if we've seen this element before.

<iframe src="https://leetcode.com/playground/K3QAUQVS/shared" frameBorder="0" width="100%" height="327" name="K3QAUQVS"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n)$$ average case. $$\mathcal{O}(n^2)$$ worst case.

  + It takes a linear amount of time to iterate through the array.
  + Lookups in a hashset are constant time on average, however those can degrade to linear time in the worst case. Note that an alternative is to use tree-based sets, which give logarithmic time lookups _always_.

* Space complexity : Upto $$\mathcal{O}(n)$$ extra space required for the set.
  + If you are tight on space, you can significantly reduce your physical space requirements by using bitsets [^note-3-0] instead of sets. This data-structure requires just one bit per element, so you can be done in just $$n$$ bits of data for elements that go up-to $$n$$. Of course, this doesn't reduce your space complexity: bitsets still grow linearly with the range of values that the elements can take.

<br />

---

#### Approach 4: Mark Visited Elements in the Input Array itself

**Intuition**

All the above approaches have ignored a key piece of information in the problem statement:

> The integers in the input array `arr` satisfy `1 ≤ arr[i] ≤ n`, where `n` is the size of array. [^note-4-0]

This presents us with two key insights:

1. All the integers present in the array are positive.
  i.e. `arr[i] > 0` for any valid index `i`. [^note-4-1]
2. The decrement of any integers present in the array must be an accessible index in the array. \
  i.e. for any integer `x` in the array, `x-1` is a valid index, and thus, `arr[x-1]` is a valid reference to an element in the array. [^note-4-2]

**Algorithm**

1. Iterate over the array and for every element `x` in the array, negate the value at index `abs(x)-1`. [^note-4-3]
    + The negation operation effectively marks the value `abs(x)` as _seen / visited_.

> **Pop Quiz:** Why do we need to use `abs(x)`, instead of `x`? 

2. Iterate over the array again, for every element `x` in the array:
    + If the value at index `abs(x)-1` is positive, it must have been negated twice. Thus `abs(x)` must have appeared twice in the array. We add `abs(x)` to the result.
    + In the above case, when we reach the second occurrence of `abs(x)`, we need to avoid fulfilling this condition again. So, we'll additionally negate the value at index `abs(x)-1`.

<iframe src="https://leetcode.com/playground/Yr8rFwCR/shared" frameBorder="0" width="100%" height="361" name="Yr8rFwCR"></iframe>

> **Pop Quiz:** Can you do this in a single loop?

Definitely! Notice that if an element `x` occurs just once in the array, the value at index `abs(x)-1` becomes negative and remains so for all of the iterations that follow.

1. Traverse through the array. When we see an element `x` for the first time, we'll negate the value at index `abs(x)-1`.
2. But, the next time we see an element `x`, we _don't_ need to negate again! If the value at index `abs(x)-1` is already negative, we know that we've seen element `x` before.

So, now we are relying on a single negation to mark the visited status of an element. This is similar to what we did in [Approach 3](#approach-3-store-seen-elements-in-a-set), except that we are re-using the array (with some smart negations) instead of a separate set.

<iframe src="https://leetcode.com/playground/9aJzjneJ/shared" frameBorder="0" width="100%" height="310" name="9aJzjneJ"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n)$$. We iterate over the array twice. Each negation operation occurs in constant time.

* Space complexity : No extra space required, other than the space for the output list. We re-use the input array to store visited status.

<br />

[^note-3-0]: C++ provides an excellent `std::bitset` in the [standard library](https://en.cppreference.com/w/cpp/utility/bitset).

[^note-4-0]: Some readers will notice a similarity with [the pigeonhole principle](https://en.wikipedia.org/wiki/Pigeonhole_principle). While this doesn't really come into play in [Approach 4](#approach-4-mark-visited-elements-in-the-input-array-itself), we utilized it indirectly in  [Approach 3](#approach-3-store-seen-elements-in-a-set): since some elements appear twice, the number of unique elements is less than the size of the array. If every unique element gets a bucket in our map / set, some buckets are bound to have more than one element in them!

[^note-4-1]: Because, `arr[i] >= 1 ` for any valid index `i` of array `arr`.

[^note-4-2]: Because, all elements in the array are integers that lie in the range $$[1, n]$$ (where $$n$$ is length of the array). Thus, their decrements are integers that lie in the range $$[0, n-1]$$ (which is precisely the set of valid indices for an array of length $$n$$).

[^note-4-3]: The `abs()` function provides the absolute value.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Simple Solution
- Author: YuxinCao
- Creation Date: Tue Oct 25 2016 11:19:02 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 00:17:27 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    // when find a number i, flip the number at position i-1 to negative. 
    // if the number at position i-1 is already negative, i is the number that occurs twice.
    
    public List<Integer> findDuplicates(int[] nums) {
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < nums.length; ++i) {
            int index = Math.abs(nums[i])-1;
            if (nums[index] < 0)
                res.add(Math.abs(index+1));
            nums[index] = -nums[index];
        }
        return res;
    }
}
```
</p>


### Python O(n) time O(1) space
- Author: Wayne-x
- Creation Date: Thu Oct 27 2016 04:07:52 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 18:59:11 GMT+0800 (Singapore Standard Time)

<p>
O(1) space not including the input and output variables

The idea is we do a linear pass using the input array itself as a hash to store which numbers have been seen before. We do this by making elements at certain indexes negative. See the full explanation here

http://www.geeksforgeeks.org/find-duplicates-in-on-time-and-constant-extra-space/

```
class Solution(object):
    def findDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        res = []
        for x in nums:
            if nums[abs(x)-1] < 0:
                res.append(abs(x))
            else:
                nums[abs(x)-1] *= -1
        return res
```
</p>


### C++ beats 98%
- Author: fffffffffffffffffff
- Creation Date: Fri Jan 20 2017 02:44:57 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 08:09:50 GMT+0800 (Singapore Standard Time)

<p>
```
vector<int> findDuplicates(vector<int>& nums) {
        vector<int> res;
        for(int i = 0; i < nums.size(); i ++){
            nums[abs(nums[i])-1] = -nums[abs(nums[i])-1];
            if(nums[abs(nums[i])-1] > 0) res.push_back(abs(nums [i]));
        }
        return res;
    }
```

Same mark by negation as a lot of people use, if you ever come across a value that is positive after negating if you know you've seen it before!
</p>


