---
title: "Shortest Word Distance III"
weight: 229
#id: "shortest-word-distance-iii"
---
## Description
<div class="description">
<p>Given a list of words and two words <em>word1</em> and <em>word2</em>, return the shortest distance between these two words in the list.</p>

<p><em>word1</em> and <em>word2</em> may be the same and they represent two individual words in the list.</p>

<p><strong>Example:</strong><br />
Assume that words = <code>[&quot;practice&quot;, &quot;makes&quot;, &quot;perfect&quot;, &quot;coding&quot;, &quot;makes&quot;]</code>.</p>

<pre>
<b>Input:</b> <em>word1</em> = <code>&ldquo;makes&rdquo;</code>, <em>word2</em> = <code>&ldquo;coding&rdquo;</code>
<b>Output:</b> 1
</pre>

<pre>
<b>Input:</b> <em>word1</em> = <code>&quot;makes&quot;</code>, <em>word2</em> = <code>&quot;makes&quot;</code>
<b>Output:</b> 3
</pre>

<p><strong>Note:</strong><br />
You may assume <em>word1</em> and <em>word2</em> are both in the list.</p>

</div>

## Tags
- Array (array)

## Companies
- LinkedIn - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 12-16 lines Java, C++
- Author: StefanPochmann
- Creation Date: Fri Aug 07 2015 21:12:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 07:32:57 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1 ... Java "short"**

`i1` and `i2` are the indexes where word1 and word2 were last seen. Except if they're the same word, then `i1` is the previous index.

    public int shortestWordDistance(String[] words, String word1, String word2) {
        long dist = Integer.MAX_VALUE, i1 = dist, i2 = -dist;
        for (int i=0; i<words.length; i++) {
            if (words[i].equals(word1))
                i1 = i;
            if (words[i].equals(word2)) {
                if (word1.equals(word2))
                    i1 = i2;
                i2 = i;
            }
            dist = Math.min(dist, Math.abs(i1 - i2));
        }
        return (int) dist;
    }

---

**Solution 2 ... Java "fast"**

Same as solution 1, but minimizing the number of string comparisons.

    public int shortestWordDistance(String[] words, String word1, String word2) {
        long dist = Integer.MAX_VALUE, i1 = dist, i2 = -dist;
        boolean same = word1.equals(word2);
        for (int i=0; i<words.length; i++) {
            if (words[i].equals(word1)) {
                if (same) {
                    i1 = i2;
                    i2 = i;
                } else {
                    i1 = i;
                }
            } else if (words[i].equals(word2)) {
                i2 = i;
            }
            dist = Math.min(dist, Math.abs(i1 - i2));
        }
        return (int) dist;
    }

---

**Solution 3 ... C++ "short"**

C++ version of solution 1.

    int shortestWordDistance(vector<string>& words, string word1, string word2) {
        long long dist = INT_MAX, i1 = dist, i2 = -dist;
        for (int i=0; i<words.size(); i++) {
            if (words[i] == word1)
                i1 = i;
            if (words[i] == word2) {
                if (word1 == word2)
                    i1 = i2;
                i2 = i;
            }
            dist = min(dist, abs(i1 - i2));
        }
        return dist;
    }

---

**Solution 4 ... C++ "fast"**

C++ version of solution 2.

    int shortestWordDistance(vector<string>& words, string word1, string word2) {
        long long dist = INT_MAX, i1 = dist, i2 = -dist;
        bool same = word1 == word2;
        for (int i=0; i<words.size(); i++) {
            if (words[i] == word1) {
                i1 = i;
                if (same)
                    swap(i1, i2);
            } else if (words[i] == word2) {
                i2 = i;
            }
            dist = min(dist, abs(i1 - i2));
        }
        return dist;
    }
</p>


### Short Java solution 10 lines O(n), modified from Shortest Word Distance I
- Author: blackminimi
- Creation Date: Thu Nov 19 2015 08:41:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 08:08:18 GMT+0800 (Singapore Standard Time)

<p>
 
 **Shortest Word Distance I**:

    public class Solution {
        public int shortestWordDistance(String[] words, String word1, String word2) {
            int index = -1;
            int min = words.length;
            for (int i = 0; i < words.length; i++) {
                if (words[i].equals(word1) || words[i].equals(word2)) {
                    if (index != -1 && !words[index].equals(words[i])) {
                        min = Math.min(i - index, min);
                    }
                    index = i;
                }
            }
            return min;
        }
    }

**Shortest Word Distance III**:
  

    public class Solution {
            public int shortestWordDistance(String[] words, String word1, String word2) {
                int index = -1;
                int min = words.length;
                for (int i = 0; i < words.length; i++) {
                    if (words[i].equals(word1) || words[i].equals(word2)) {
                        if (index != -1 && (word1.equals(word2) || !words[index].equals(words[i]))) {
                            min = Math.min(i - index, min);
                        }
                        index = i;
                    }
                }
                return min;
            }
        }
</p>


### AC Python solution clean and fast in one loop
- Author: dietpepsi
- Creation Date: Thu Oct 01 2015 12:29:04 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 28 2018 16:46:15 GMT+0800 (Singapore Standard Time)

<p>
    def shortestWordDistance(self, words, word1, word2):
        n = len(words)
        ans = n
        p1 = p2 = -n
        same = word1 == word2
        for i in xrange(n):
            if words[i] == word1:
                p1 = i
                ans = min(ans, i - p2)
                if same:
                    p2 = p1
            elif not same and words[i] == word2:
                p2 = i
                ans = min(ans, i - p1)
        return ans


Only check if word1 and word2 is same once and save it in a boolean and use it wisely. This way we don't lose the speed and the code remain clean.
</p>


