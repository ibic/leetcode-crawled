---
title: "Nested List Weight Sum II"
weight: 347
#id: "nested-list-weight-sum-ii"
---
## Description
<div class="description">
<p>Given a nested list of integers, return the sum of all integers in the list weighted by their depth.</p>

<p>Each element is either an integer, or a list -- whose elements may also be integers or other lists.</p>

<p>Different from the <a href="https://leetcode.com/problems/nested-list-weight-sum/">previous question</a> where weight is increasing from root to leaf, now the weight is defined from bottom up. i.e., the leaf level integers have weight 1, and the root level integers have the largest weight.</p>

<p><strong>Example 1:</strong></p>

<div>
<pre>
<strong>Input: </strong><span id="example-input-1-1">[[1,1],2,[1,1]]</span>
<strong>Output: </strong><span id="example-output-1">8 
<strong>Explanation: </strong>F</span>our 1&#39;s at depth 1, one 2 at depth 2.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[1,[4,[6]]]</span>
<strong>Output: </strong><span id="example-output-2">17 
<strong>Explanation:</strong> O</span>ne 1 at depth 3, one 4 at depth 2, and one 6 at depth 1; 1*3 + 4*2 + 6*1 = 17.
</pre>
</div>
</div>

</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- LinkedIn - 29 (taggedByAdmin: true)
- DiDi - 4 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### No depth variable, no multiplication
- Author: StefanPochmann
- Creation Date: Thu Jun 23 2016 20:51:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 12:09:51 GMT+0800 (Singapore Standard Time)

<p>
Inspired by [lzb700m's solution](https://leetcode.com/discuss/110042/share-my-2ms-intuitive-one-pass-bfs-solution) and [one of mine](https://leetcode.com/discuss/95184/no-depth-variable). Instead of multiplying by depth, add integers multiple times (by going level by level and adding the unweighted sum to the weighted sum after each level).

    public int depthSumInverse(List<NestedInteger> nestedList) {
        int unweighted = 0, weighted = 0;
        while (!nestedList.isEmpty()) {
            List<NestedInteger> nextLevel = new ArrayList<>();
            for (NestedInteger ni : nestedList) {
                if (ni.isInteger())
                    unweighted += ni.getInteger();
                else
                    nextLevel.addAll(ni.getList());
            }
            weighted += unweighted;
            nestedList = nextLevel;
        }
        return weighted;
    }
</p>


### JAVA AC BFS solution
- Author: jzhang0228
- Creation Date: Tue Jun 28 2016 02:21:04 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 04:44:51 GMT+0800 (Singapore Standard Time)

<p>
    public int depthSumInverse(List<NestedInteger> nestedList) {
            if (nestedList == null) return 0;
            Queue<NestedInteger> queue = new LinkedList<NestedInteger>();
            int prev = 0;
            int total = 0;
            for (NestedInteger next: nestedList) {
                queue.offer(next);
            }
            
            while (!queue.isEmpty()) {
                int size = queue.size();
                int levelSum = 0;
                for (int i = 0; i < size; i++) {
                    NestedInteger current = queue.poll();
                    if (current.isInteger()) levelSum += current.getInteger();
                    List<NestedInteger> nextList = current.getList();
                    if (nextList != null) {
                        for (NestedInteger next: nextList) {
                            queue.offer(next);
                        }
                    }
                }
                prev += levelSum;
                total += prev;
            }
            return total;
        }
</p>


### The question needs to be re-defined.
- Author: hyj143
- Creation Date: Thu Jun 23 2016 11:34:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 09:36:32 GMT+0800 (Singapore Standard Time)

<p>
The weight increases from the leaf to the root.

However, the following situation is not clearly defined. I will illustrate it using a tree structure.

        a
      /    \
     b      c
              \
               d

What is the weight of a?  Is it 2 to 3?
</p>


