---
title: "Students Report By Geography"
weight: 1507
#id: "students-report-by-geography"
---
## Description
<div class="description">
A U.S graduate school has students from Asia, Europe and America. The students&#39; location information are stored in table <code>student</code> as below.
<p>&nbsp;</p>

<pre>
| name   | continent |
|--------|-----------|
| Jack   | America   |
| Pascal | Europe    |
| Xi     | Asia      |
| Jane   | America   |
</pre>

<p>&nbsp;</p>
<a href="https://en.wikipedia.org/wiki/Pivot_table"> Pivot</a> the continent column in this table so that each name is sorted alphabetically and displayed underneath its corresponding continent. The output headers should be America, Asia and Europe respectively. It is guaranteed that the student number from America is no less than either Asia or Europe.

<p>&nbsp;</p>
For the sample input, the output is:

<p>&nbsp;</p>

<pre>
| America | Asia | Europe |
|---------|------|--------|
| Jack    | Xi   | Pascal |
| Jane    |      |        |
</pre>

<p>&nbsp;</p>
<b>Follow-up:</b> If it is unknown which continent has the most students, can you write a query to generate the student report?

<p>&nbsp;</p>

</div>

## Tags


## Companies
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using "session variables" and `join` [Accepted]

**Intuition**

Assign a separate auto increment row id to each of the continent, and then join them together.

**Algorithm**

To set the row id for each continent, we need to use session variables.
For example, we can use the following statement to assign a auto increment row number for students in America.

```sql
SELECT 
    row_id, America
FROM
    (SELECT @am:=0) t,
    (SELECT 
        @am:=@am + 1 AS row_id, name AS America
    FROM
        student
    WHERE
        continent = 'America'
    ORDER BY America) AS t2
;
```

    | row_id | America |
    |--------|---------|
    | 1      | Jack    |
    | 2      | Jane    |

Similarly, we can assign other dedicated row id for other continents as the following result.

    | row_id | Asia |
    |--------|------|
    | 1      | Xi   |

    | row_id | Europe |
    |--------|--------|
    | 1      | Jesper |


Then if we join these 3 temp tables together and using the same row_id as the condition, we can have the following table.

    | row_id | America | Asia | Europe |
    |--------|---------|------|--------|
    | 1      | Jack    | Xi   | Pascal |
    | 2      | Jane    |      |        |

One issue you may encounter is the student list for America is not complete if you use regular inner join since there are more records in this list comparing with the other two. So you may have a solution to use the `outer join`. Correct! But how to arrange the 3 tables? The trick is to put the America list in the middle so that we can use `right (outer) join` and `right (outer) join` to connect with other two tables.

**MySQL**

```sql
SELECT 
    America, Asia, Europe
FROM
    (SELECT @as:=0, @am:=0, @eu:=0) t,
    (SELECT 
        @as:=@as + 1 AS asid, name AS Asia
    FROM
        student
    WHERE
        continent = 'Asia'
    ORDER BY Asia) AS t1
        RIGHT JOIN
    (SELECT 
        @am:=@am + 1 AS amid, name AS America
    FROM
        student
    WHERE
        continent = 'America'
    ORDER BY America) AS t2 ON asid = amid
        LEFT JOIN
    (SELECT 
        @eu:=@eu + 1 AS euid, name AS Europe
    FROM
        student
    WHERE
        continent = 'Europe'
    ORDER BY Europe) AS t3 ON amid = euid
;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### (^_^) MySQL Solutions: WINDOW / variables (Follow-up answer)
- Author: 120668385
- Creation Date: Sat Jun 06 2020 04:31:36 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 18 2020 06:12:02 GMT+0800 (Singapore Standard Time)

<p>
You need to understand:
1. How to generate a useful row_id (using ROW_NUMBER() window function? or using Variables?)
2. GROUP BY row_id
3. CASE clause 
3. MAX function can consider only non-null values
________________________________
# CHOICE 1: using WINDOW function
```
SELECT
        MAX(CASE WHEN continent = \'America\' THEN name END )AS America,
        MAX(CASE WHEN continent = \'Asia\' THEN name END )AS Asia,
        MAX(CASE WHEN continent = \'Europe\' THEN name END )AS Europe  
FROM (SELECT *, ROW_NUMBER()OVER(PARTITION BY continent ORDER BY name) AS row_id FROM student) AS t
GROUP BY row_id
```
**Explanation**:
In order to get the following table, you need to write a query like this
```
SELECT *, ROW_NUMBER()OVER(PARTITION BY continent ORDER BY name) AS row_id FROM student
```
![image](https://assets.leetcode.com/users/120668385/image_1591389768.png)
Using GROUP BY to group rows with same row_id

*For example row_id = 1:*
![image](https://assets.leetcode.com/users/120668385/image_1591389853.png)
*For example row_id = 2:*
![image](https://assets.leetcode.com/users/120668385/image_1591389862.png)
After GROUP BY, using CASE function to get each continent\'s data, and **using MAX function to get all of the non-null values**.

***************************************************
# CHOICE 2: using variables
```
SELECT MAX(America) AS America, MAX(Asia) as Asia, MAX(Europe) AS Europe
FROM (
	SELECT 
        CASE WHEN continent = \'America\' THEN @r1 := @r1 + 1
             WHEN continent = \'Asia\'    THEN @r2 := @r2 + 1
             WHEN continent = \'Europe\'  THEN @r3 := @r3 + 1 END row_id,
        CASE WHEN continent = \'America\' THEN name END America,
        CASE WHEN continent = \'Asia\'    THEN name END Asia,
        CASE WHEN continent = \'Europe\'  THEN name END Europe
    FROM student, (SELECT @r1 := 0, @r2 := 0, @r3 := 0) tmp
    ORDER BY name) t
GROUP BY row_id;
```
**Explanation**:
Get a revised table with row_id like this:
```
SELECT 
        CASE WHEN continent = \'America\' THEN @r1 := @r1 + 1
             WHEN continent = \'Asia\'    THEN @r2 := @r2 + 1
             WHEN continent = \'Europe\'  THEN @r3 := @r3 + 1 END row_id,
        CASE WHEN continent = \'America\' THEN name END America,
        CASE WHEN continent = \'Asia\'    THEN name END Asia,
        CASE WHEN continent = \'Europe\'  THEN name END Europe
FROM student, (SELECT @r1 := 0, @r2 := 0, @r3 := 0) tmp
ORDER BY name
```
![image](https://assets.leetcode.com/users/120668385/image_1591389588.png)
Using **GROUP BY** and **MAX** to get the final result, which is same as the explanation of CHOICE 1.
</p>


### follow up accepted solution
- Author: Morganmm
- Creation Date: Thu Oct 18 2018 01:40:09 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 01:40:09 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT MAX(America) AS America, MAX(Asia) as Asia, MAX(Europe) AS Europe
FROM (
    SELECT 
        CASE WHEN continent = \'America\' THEN @r1 := @r1 + 1
             WHEN continent = \'Asia\'    THEN @r2 := @r2 + 1
             WHEN continent = \'Europe\'  THEN @r3 := @r3 + 1 END row_id,
        CASE WHEN continent = \'America\' THEN name END America,
        CASE WHEN continent = \'Asia\'    THEN name END Asia,
        CASE WHEN continent = \'Europe\'  THEN name END Europe
    FROM student, (SELECT @r1 := 0, @r2 := 0, @r3 := 0) AS row_id
    ORDER BY name
) t
GROUP BY row_id;
```
</p>


### Simple MS SQL solution without join, beats 93%
- Author: user9157V
- Creation Date: Mon Feb 03 2020 13:44:37 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 03 2020 13:44:37 GMT+0800 (Singapore Standard Time)

<p>
```
select min(America) as America, min(Asia) as Asia, min(Europe) as Europe from
    (select 
        row,
        case when continent = \'America\' then name else null end as America,
        case when continent = \'Asia\' then name else null end as Asia,
        case when continent = \'Europe\' then name else null end as Europe
    from
        (select *,
        row_number() over(partition by continent order by name) as row
        from student) t1) t2
group by row;
```
</p>


