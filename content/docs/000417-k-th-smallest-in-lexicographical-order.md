---
title: "K-th Smallest in Lexicographical Order"
weight: 417
#id: "k-th-smallest-in-lexicographical-order"
---
## Description
<div class="description">
<p>Given integers <code>n</code> and <code>k</code>, find the lexicographically k-th smallest integer in the range from <code>1</code> to <code>n</code>.</p>

<p>Note: 1 &le; k &le; n &le; 10<sup>9</sup>.</p>

<p><b>Example:</b>
<pre>
<b>Input:</b>
n: 13   k: 2

<b>Output:</b>
10

<b>Explanation:</b>
The lexicographical order is [1, 10, 11, 12, 13, 2, 3, 4, 5, 6, 7, 8, 9], so the second smallest number is 10.
</pre>
</p>

</div>

## Tags


## Companies
- ByteDance - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Hulu - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Concise/Easy-to-understand Java 5ms solution with Explaination
- Author: NathanNi
- Creation Date: Mon Oct 24 2016 15:24:15 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 18:23:46 GMT+0800 (Singapore Standard Time)

<p>
Original idea comes from 
http://bookshadow.com/weblog/2016/10/24/leetcode-k-th-smallest-in-lexicographical-order/

Actually this is a denary tree (each node has 10 children). Find the kth element is to do a k steps preorder traverse of the tree.
![0_1477293053966_upload-40379731-118a-4753-bed9-1cb372790d4b](/uploads/files/1477293057263-upload-40379731-118a-4753-bed9-1cb372790d4b.png) 

Initially, image you are at node 1 (variable: curr), 
the goal is move (k - 1) steps to the target node x.  (substract steps from k after moving)
when k is down to 0, curr will be finally at node x, there you get the result.

we don't really need to do a exact k steps preorder traverse of the denary tree, **the idea is to calculate the steps between curr and curr + 1 (neighbor nodes in same level), in order to skip some unnecessary moves.**

**Main function**
Firstly, calculate how many steps curr need to move to curr + 1.
1. if the steps <= k, we know we can move to curr + 1, and narrow down k to k - steps.

2. else if the steps > k, that means the curr + 1 is actually behind the target node x in the preorder path, we can't  jump to curr + 1. What we have to do is to move forward only 1 step (curr * 10 is always next preorder node) and repeat the iteration.



**calSteps function**

1.  how to calculate the steps between curr and curr + 1?
   Here we come up a idea to calculate by level.
   Let n1 = curr, n2 = curr + 1.
   n2 is always the next right node beside n1's right most node (who shares the same ancestor "curr")
(refer to the pic, 2 is right next to 1, 20 is right next to 19, 200 is right next to 199).

   2. so, if n2 <= n, what means n1's right most node exists, we can simply add the number of nodes from n1 to n2 to steps.

   3. else if n2 > n, what means n (the biggest node) is on the path between n1 to n2, add (n + 1 - n1) to  steps.

   4. organize this flow to "steps += Math.min(n + 1, n2) - n1; n1 *= 10; n2 *= 10;"



**Here is the code snippet:**

    public int findKthNumber(int n, int k) {
        int curr = 1;
        k = k - 1;
        while (k > 0) {
            int steps = calSteps(n, curr, curr + 1);
            if (steps <= k) {
                curr += 1;
                k -= steps;
            } else {
                curr *= 10;
                k -= 1;
            }
        }
        return curr;
    }
    //use long in case of overflow
    public int calSteps(int n, long n1, long n2) {
        int steps = 0;
        while (n1 <= n) {
            steps += Math.min(n + 1, n2) - n1;
            n1 *= 10;
            n2 *= 10;
        }
        return steps;
    }
</p>


### C++/Python 0ms O((log n)^2)-time O(1)-space super easy solution with detailed explanations
- Author: zhiqing_xiao
- Creation Date: Sun Oct 23 2016 21:11:24 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 17:32:32 GMT+0800 (Singapore Standard Time)

<p>
This solution is inspired by @mgch's code.

**General ideas:**
**1. Calculate the number of numbers that prefix with `result`.**
    Numbers prefixed by `result` are the union of the following intervals:
     [`result`, `result`+1)
     [`result`*10,  (`result`+1)*10 )
     [`result`*100,  (`result`+1)*100 )
    ...
    But they also shall belong to [1, n]
    Therefore, we can easily calculate the number of elements prefixed by `result` using the following code (not considering overflow):

                int count = 0;
                for (int first = result, last = result + 1; // the first interval contains only one element: {result}
                    first <= n; // the interval is not empty
                    first *= 10, last *= 10) // increase a digit
                {
                    // valid interval = [first, last) union [first, n]
                    count += (min(n + 1, last) - first); // add the length of interval
                }

**2. Search the next prefix, or search more detailedly.**
* If the number of numbers that prefixed by `result` is smaller than the remaining `k`, we do not need to consider the numbers prefixed by `result` any more, and march to the next prefix: `result`+1;
* Otherwise, we need to search more detailedly, by appending another digit to the prefix. Such search shall start with `result`*10.

**C++**

    class Solution {
    public:
        int findKthNumber(int n, int k)
        {
            int result = 1;
            for(--k; k > 0; )
            {
                // calculate #|{result, result*, result**, result***, ...}|
                int count = 0;
                for (long long first = static_cast<long long>(result), last = first + 1;
                    first <= n; // the interval is not empty
                    first *= 10, last *= 10) // increase a digit
                {
                    // valid interval = [first, last) union [first, n]
                    count += static_cast<int>((min(n + 1LL, last) - first)); // add the length of interval
                }
                
                if (k >= count)
                {   // skip {result, result*, result**, result***, ...}
                    // increase the current prefix
                    ++result;
                    k -= count;
                }
                else
                {   // not able to skip all of {result, result*, result**, result***, ...}
                    // search more detailedly
                    result *= 10;
                    --k;
                }
            }
            return result;
        }
    };

**Python**

    class Solution(object):
        def findKthNumber(self, n, k):
            result = 1;
            k -= 1
            while k > 0:
                count = 0
                interval = [result, result+1]
                while interval[0] <= n:
                    count += (min(n+1, interval[1]) - interval[0])
                    interval = [10*interval[0], 10*interval[1]]
                
                if k >= count:
                    result += 1
                    k -= count
                else:
                    result *= 10
                    k -= 1
            return result



**Complexities:**

* **Time Complexity:**   *O*( (log*n*) ^ 2 )
Here, log*n* is the number of digits in *n*, and it is also the number of replications of  appending zero to search detailedly. Each such appending introduces the increasement of prefix at most 10 times, and each increasement may require *O*(log*n*) time to calculate the number of numbers that prefixed by the `result`.
* **Space Complexity:** *O*(1)
</p>


### An O(log^2) implementation using prefixCount
- Author: newlywed
- Creation Date: Sun Apr 22 2018 10:43:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 22 2018 10:43:18 GMT+0800 (Singapore Standard Time)

<p>
Given a prefix ***a***, and an upper bound ***n***, ***prefixCount(p, n)*** returns the number of integers in [***1, n***], with prefix ***p***. For instance, ***prefixCount(12, 123) = 5***, since all numbers with prefix ***12*** are [***12, 120, 121, 122, 123***]. With prefixCount, we can implement ***findKthNumber*** as following:
```
int findKthNumber(int n, int k) {
        --k;
        int prefix = 1;
        while (k) {
            int c = prefixCount(prefix, n);
            if (k < c) { //current prefix is good, refine it
                --k;
                prefix *= 10;
            } else { //current prefix is too small, move to next prefix
                k -= c;
                prefix++;
            }
        }
        return prefix;
    }
```
Assume ***prefixCount*** takes ***O(log n)*** time. The running time of above function is ***O***(***log^2 n***). Now let\u2019s talk about how to implement ***prefixCount***.

A naive way to compute ***prefixCount*** is:
```
int prefixCount(int p, int n) {
    if (p > n) return 0;
    int res = 1;
    for (int i = 0; i < 10; ++i) res += prefixCount(p * 10 + i, n);
    return res;
}
```
The time complexity is ***O***(***n***). However, you might notice, if ***p*** is not prefix of ***n***,  all ***prefixCount***(***p***  * ***10*** + ***i***), for ***9 >= i >=0*** will return the same value. This leads to the optimization:
```
int prefixCount(int p, int n) {
    if (p > n) return 0;
    if (p is not prefix of n) return 1 + 10 * prefixCount(p * 10 + 0, n);
    int res = 1;
    for (int i = 0; i < 10; ++i) res += prefixCount(p * 10 + i, n);
    return res;
}
```
Assume you can check if ***p*** is prefix of ***n*** in constant time, with this optimization, the time complexity becomes ***O***(***log n***), since the depth of the recursion is ***O***(***log n***) and there are ***10*** branches at each level (when ***p*** is the prefix of ***n***).

Checking if ***p*** is prefix of ***n*** is trivial if we slight change our recursion to maintain the order of magnitude of ***n/p***. Here is the complete implementation:
```
int prefixCount(uint64_t p, int n, int mask) {
    if (p > n) return 0;
    if (p != n / mask) return 1 + 10 * prefixCount(p * 10 + 0, n, mask / 10);
    int res = 1;
    for (int i = 0; i < 10; ++i) res += prefixCount(p * 10 + i, n, mask / 10);
    return res;
}
int prefixCount(uint64_t  p, int n) {
    uint64_t mask = 1;
    while (p * mask <= n) mask *= 10;
    return prefixCount(p, n, mask / 10);
}
```







</p>


