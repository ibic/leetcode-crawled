---
title: "Number of Squareful Arrays"
weight: 947
#id: "number-of-squareful-arrays"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of non-negative integers, the array is <em>squareful</em> if for every pair of adjacent elements, their sum is a perfect square.</p>

<p>Return the number of permutations of A that are squareful.&nbsp; Two permutations <code>A1</code> and <code>A2</code> differ if and only if there is some index <code>i</code> such that <code>A1[i] != A2[i]</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,17,8]</span>
<strong>Output: </strong><span id="example-output-1">2</span>
<strong>Explanation: </strong>
[1,8,17] and [17,8,1] are the valid permutations.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[2,2,2]</span>
<strong>Output: </strong><span id="example-output-2">1</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 12</code></li>
	<li><code>0 &lt;= A[i] &lt;= 1e9</code></li>
</ol>
</div>

## Tags
- Math (math)
- Backtracking (backtracking)
- Graph (graph)

## Companies
- Apple - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Backtracking

**Intuition**

Construct a graph where an edge from $$i$$ to $$j$$ exists if $$A[i] + A[j]$$ is a perfect square.  Our goal is to investigate Hamiltonian paths of this graph: paths that visit all the nodes exactly once.

**Algorithm**

Let's keep a current `count` of what values of nodes are left to visit, and a count `todo` of how many nodes left to visit.

From each node, we can explore all neighboring nodes (by value, which reduces the complexity blowup).

Please see the inline comments for more details.

<iframe src="https://leetcode.com/playground/dUv8aXHE/shared" frameBorder="0" width="100%" height="500" name="dUv8aXHE"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^N)$$, where $$N$$ is length of `A`.  A tighter bound is outside the scope of this article.  However, it can be shown that the underlying graph is triangle free, as well as other properties that would dramatically shrink this complexity.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Dynamic Programming

**Intuition**

As in *Approach 1*, construct an underlying graph.  Since the number of nodes is small, we can use dynamic programming with a 'visited' mask.

**Algorithm**

We construct the graph in the same method as in *Approach 1*.

Now, let `dfs(node, visited)` be the number of ways from `node` to visit the remaining unvisited nodes.  Here, `visited` is a mask: `(visited >> i) & 1` is true if and only if the `i`th node has been visited.

Afterwards, we may have overcounted if there are repeated values in `A`.  To account for this, for every `x` in `A`, if `A` contains `x` a total of `k` times, we divide the answer by `k!`.

<iframe src="https://leetcode.com/playground/T9de9W2z/shared" frameBorder="0" width="100%" height="500" name="T9de9W2z"></iframe>

**Complexity Analysis**

* Time Complexity:  $$\mathcal{O}(N^2 \cdot 2^N)$$, where $$N$$ is length of `A`.
Here we have $$N \cdot 2^N$$ states in total, 
and each state has up to $$N$$ children to dfs.
More formally, the time complexity could be computed as
$$
\mathcal{O}(\sum\limits_{k = 1}^{N}{C_N^K \cdot C_k^2}) = 
\mathcal{O}(N^2 \cdot 2^N)
$$

* Space Complexity:  $$\mathcal{O}(N \cdot 2^N)$$.
<br />
<br />

## Accepted Submission (python3)
```python3
from collections import Counter

class Solution:
    def isSquare(self, n):
        return int(n ** 0.5) ** 2 == n
    
    def bt(self, prev, left):
        if not left:
            return 1
        r = 0
        prevItem = float("-inf")
        for i, item in enumerate(left):
            if prevItem != item and self.isSquare(prev + item):
                r += self.bt(item, left[:i] + left[i+1:])
                prevItem = item
        return r
 
    def numSquarefulPerms(self, A: List[int]) -> int:
#        r = 0
#        prevItem = float("inf")
#        for i, item in enumerate(A):
#            if prevItem != item:
#                r += self.bt(item, A[:i] + A[i+1:])
#                prevItem = item
#        return r
        r = 0
        counts = Counter(A)
        pairs = {key: {val for val in counts if self.isSquare(val + key)} for key in counts}
        lena = len(A)
        def btm(a, leny):
            if leny == lena:
                return 1
            counts[a] -= 1
            sr = 0
            for b in pairs[a]:
                if counts[b] > 0:
                    sr += btm(b, leny + 1)
            counts[a] += 1
            return sr
        for a in counts:
            r += btm(a, 1)
        return r

```

## Top Discussions
### [C++/Python] Backtracking
- Author: lee215
- Creation Date: Sun Feb 17 2019 12:04:05 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 27 2020 11:06:47 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**:
1. Count numbers ocuurrence.
2. For each number `i`, find all possible next number `j` that `i + j` is square.
3. Backtracking using dfs.
<br>

## **Time Complexity**
It\'s `O(N!)` if we have N different numbers and any pair sum is square.
We can easily make case for N = 3 like [51,70,30].

Seems that no hard cases for this problem and int this way it reduces to O(N).
<br>

**C++:**
```cpp
    unordered_map<int, int> count;
    unordered_map<int, unordered_set<int>> cand;
    int res = 0;
    int numSquarefulPerms(vector<int>& A) {
        for (int &a : A) count[a]++;
        for (auto &i : count) {
            for (auto &j : count) {
                int x = i.first, y = j.first, s = sqrt(x + y);
                if (s * s == x + y)
                    cand[x].insert(y);
            }
        }
        for (auto e : count)
            dfs(e.first, A.size() - 1);
        return res;
    }

    void dfs(int x, int left) {
        count[x]--;
        if (!left) res++;
        for (int y : cand[x])
            if (count[y] > 0)
                dfs(y, left - 1);
        count[x]++;
    }
```

**Python:**
```py
    def numSquarefulPerms(self, A):
        c = collections.Counter(A)
        cand = {i: {j for j in c if int((i + j)**0.5) ** 2 == i + j} for i in c}

        def dfs(x, left=len(A) - 1):
            c[x] -= 1
            count = sum(dfs(y, left - 1) for y in cand[x] if c[y]) if left else 1
            c[x] += 1
            return count
        return sum(map(dfs, c))
```

</p>


### [4ms/C++] Simple Backtracking like Permutations II
- Author: yasuoNF
- Creation Date: Sun Feb 17 2019 12:19:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 17 2019 12:19:04 GMT+0800 (Singapore Standard Time)

<p>
My solution is based on the idea of https://leetcode.com/problems/permutations-ii/discuss/18596/A-simple-C%2B%2B-solution-in-only-20-lines

The only difference is that by calling the recursion every time, the program would check whether the sum of the number on current index and the previous is a square number or not, to make sure the sub array from 0 to current index satisfys the Squareful definition.
```
class Solution {
public:
    int numSquarefulPerms(vector<int>& A) {
        sort(A.begin(), A.end());
        int ans = 0;
        pmt(A, 0, ans);
        return ans;
    }
    void pmt(vector<int> A, int idx, int& ans) {
        if (idx >= A.size()) {
            ++ans;
        }
        for (int i = idx; i < A.size(); ++i) {
            if (i > idx && A[i] == A[idx]) continue;
            swap(A[i], A[idx]);
            if ((idx == 0) || (idx > 0 && isSquare(A[idx] + A[idx - 1]))) {
                pmt(A, idx + 1, ans);
            }
        }
    }
    bool isSquare(int v) {
        int r = sqrt(v);
        return r * r == v;
    }
};
```

</p>


### Java DFS + Unique Perms
- Author: witchthewicked
- Creation Date: Sun Feb 17 2019 12:13:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 17 2019 12:13:10 GMT+0800 (Singapore Standard Time)

<p>
1. Avoid duplicate numbers in recursion by sorting ```Arrays.sort(A);``` 
    and use the ```A[i]==A[i-1]``` trick while iterating
     
2. Before entering another recursion level check to make sure you are forming a square <-- this part I figured out post-contest 
```
if (lastNumber!=-1){
                    if (isSquare(A[i],lastNumber)==false)
                        continue;
                }
```
	 
	 

Full code below
For you: 

``` 
class Solution {
    private boolean isSquare(int a, int b){
        double sqr = Math.sqrt(a+b);
        boolean res = (sqr - Math.floor(sqr)) == 0;
        return res;
    }
    
    private int count = 0;
    private void helper(List<Integer> temp, int[] A, boolean[] used, int lastNumber){
        if (temp.size()==A.length){
            count++;
        } else {
            for (int i = 0; i<A.length;i++){
                if (used[i] || (i>0 && A[i]==A[i-1] && !used[i-1]))continue;
                if (lastNumber!=-1){
				// if we cant form a square we can not proceed to form a squareful array
                    if (isSquare(A[i],lastNumber)==false)
                        continue;
                }
                used[i] = true;
                temp.add(A[i]);
                helper(temp,A,used,A[i]);
                temp.remove(temp.size()-1);
                used[i] = false;
            }
        }
        
    }
    public int numSquarefulPerms(int[] A) {
        Arrays.sort(A);
        helper(new ArrayList(),A,new boolean[A.length],-1);
        return count;
    }
}
```
</p>


