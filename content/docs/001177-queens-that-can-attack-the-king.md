---
title: "Queens That Can Attack the King"
weight: 1177
#id: "queens-that-can-attack-the-king"
---
## Description
<div class="description">
<p>On an <strong>8x8</strong> chessboard, there can be multiple Black Queens and one White King.</p>

<p>Given an array of integer coordinates <code>queens</code> that represents the positions of the Black Queens, and a pair of coordinates <code>king</code> that represent the position of the White King, return the coordinates of all the queens (in any order) that can attack the King.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/10/01/untitled-diagram.jpg" style="width: 321px; height: 321px;" /></p>

<pre>
<strong>Input:</strong> queens = [[0,1],[1,0],[4,0],[0,4],[3,3],[2,4]], king = [0,0]
<strong>Output:</strong> [[0,1],[1,0],[3,3]]
<strong>Explanation:</strong>&nbsp; 
The queen at [0,1] can attack the king cause they&#39;re in the same row. 
The queen at [1,0] can attack the king cause they&#39;re in the same column. 
The queen at [3,3] can attack the king cause they&#39;re in the same diagnal. 
The queen at [0,4] can&#39;t attack the king cause it&#39;s blocked by the queen at [0,1]. 
The queen at [4,0] can&#39;t attack the king cause it&#39;s blocked by the queen at [1,0]. 
The queen at [2,4] can&#39;t attack the king cause it&#39;s not in the same row/column/diagnal as the king.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/10/01/untitled-diagram-1.jpg" style="width: 321px; height: 321px;" /></strong></p>

<pre>
<strong>Input:</strong> queens = [[0,0],[1,1],[2,2],[3,4],[3,5],[4,4],[4,5]], king = [3,3]
<strong>Output:</strong> [[2,2],[3,4],[4,4]]
</pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/10/01/untitled-diagram-2.jpg" style="width: 321px; height: 321px;" /></strong></p>

<pre>
<strong>Input:</strong> queens = [[5,6],[7,7],[2,1],[0,7],[1,6],[5,1],[3,7],[0,3],[4,0],[1,2],[6,3],[5,0],[0,4],[2,2],[1,1],[6,4],[5,4],[0,0],[2,6],[4,5],[5,2],[1,4],[7,5],[2,3],[0,5],[4,2],[1,0],[2,7],[0,1],[4,6],[6,1],[0,6],[4,3],[1,7]], king = [3,4]
<strong>Output:</strong> [[2,3],[1,4],[1,6],[3,7],[4,3],[5,4],[4,5]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= queens.length&nbsp;&lt;= 63</code></li>
	<li><code>queens[0].length == 2</code></li>
	<li><code>0 &lt;= queens[i][j] &lt;&nbsp;8</code></li>
	<li><code>king.length == 2</code></li>
	<li><code>0 &lt;= king[0], king[1] &lt; 8</code></li>
	<li>At most one piece is allowed in a cell.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Media.net - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Check 8 steps in 8 Directions
- Author: lee215
- Creation Date: Sun Oct 13 2019 12:07:33 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 14 2019 20:40:16 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Start from the position of king,
we try to find a queen in 8 directions.
I didn\'t bother skipping the case where `(i,j) = (0,0)`
<br>

## **Complexity**
Time `O(1)`, Space `O(1)`
as the size of chessboard is limited.

For the chessboard of `N * N`
Time `O(queens + 8N)`
Space `O(queens)`
<br>

**Python:**
```python
    def queensAttacktheKing(self, queens, king):
        res = []
        queens = {(i, j) for i, j in queens}
        for i in [-1, 0, 1]:
            for j in [-1, 0, 1]:
                for k in xrange(1, 8):
                    x, y = king[0] + i * k, king[1] + j * k
                    if (x, y) in queens:
                        res.append([x, y])
                        break
        return res
```

</p>


### Java short and concise beat 100%
- Author: WALL__E
- Creation Date: Sun Oct 13 2019 12:10:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 13 2019 12:13:56 GMT+0800 (Singapore Standard Time)

<p>
Start from King and reach to the Queens on 8 directions. Break on that direction if one queen is found.
```
class Solution {
    public List<List<Integer>> queensAttacktheKing(int[][] queens, int[] king) {
        List<List<Integer>> result = new ArrayList<>();
        boolean[][] seen = new boolean[8][8];
        for (int[] queen : queens) seen[queen[0]][queen[1]] = true;
        int[] dirs = {-1, 0, 1};
        for (int dx : dirs) {
            for (int dy : dirs) {
                if (dx == 0 && dy == 0) continue;
                int x = king[0], y = king[1];
                while (x + dx >= 0 && x + dx < 8 && y + dy >= 0 && y + dy < 8) {
                    x += dx;
                    y += dy;
                    if (seen[x][y]) {
                        result.add(Arrays.asList(x, y));
                        break;
                    }
                }
            }
        }
        return result;
    }   
}
```
</p>


### C++ Tracing
- Author: votrubac
- Creation Date: Sun Oct 13 2019 12:34:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 13 2019 12:38:10 GMT+0800 (Singapore Standard Time)

<p>
Trace in all directions from the king until you hit a queen or go off the board. Since the board is limited to `8 x 8`, we can use a boolean matrix to lookup queen positions; we could use a hash map for a larger board.
```
vector<vector<int>> queensAttacktheKing(vector<vector<int>>& queens, vector<int>& k) {
    bool b[8][8] = {};
    for (auto& q : queens) b[q[0]][q[1]] = true;
    vector<vector<int>> res;
    for (auto i = -1; i <= 1; ++i)
        for (auto j = -1; j <= 1; ++j) {
            if (i == 0 && j == 0) continue;
            auto x = k[0] + i, y = k[1] + j;
            while (min(x, y) >= 0 && max(x, y) < 8) {
                if (b[x][y]) {
                    res.push_back({ x, y });
                    break;
                }
                x += i, y += j;
            }
        }
    return res;
}
```
</p>


