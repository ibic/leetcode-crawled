---
title: "Palindrome Removal"
weight: 1089
#id: "palindrome-removal"
---
## Description
<div class="description">
<p>Given an integer array&nbsp;<code>arr</code>, in one move you can select a <strong>palindromic</strong>&nbsp;subarray <code>arr[i], arr[i+1], ..., arr[j]</code>&nbsp;where <code>i &lt;= j</code>, and remove that subarray from the given array. Note that after removing a subarray, the elements on the left and on the right of that subarray move to fill the gap left by the removal.</p>

<p>Return the minimum number of moves needed&nbsp;to remove all numbers from the array.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2]
<strong>Output:</strong> 2
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,3,4,1,5]
<strong>Output:</strong> 3
<b>Explanation: </b>Remove [4] then remove [1,3,1] then remove [5].
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 100</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 20</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Microsoft - 6 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ O(N^3) 13 lines DP with explanation
- Author: plus2047
- Creation Date: Sun Nov 03 2019 00:19:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 03 2019 00:20:46 GMT+0800 (Singapore Standard Time)

<p>
```cpp
class Solution {
public:
    int minimumMoves(vector<int>& arr) {
        int n = arr.size();
        
        // dp[left][right] = the min move for arr[left]...arr[right] (both included).
        // the max number of move is n.
        vector<vector<int>> dp(n, vector<int>(n, n));
        
        // handle edge situation: subarray size == 1
        for(int i = 0; i < n; i++) { dp[i][i] = 1; }
        
        // handle edge situation: subarray size == 2
        for(int i = 0; i < n - 1; i++) { dp[i][i + 1] = arr[i] == arr[i + 1] ? 1 : 2; }
        
        // for subarray size >= 3:
        for(int size = 3; size <= n; size++) {
            for(int left = 0, right = left + size - 1; right < n; left++, right++) {
                
                // if arr[left] == arr[right], then the two number: arr[left] and arr[right] can be
                // removed when the last move of subarray arr[left + 1:right - 1]
                if(arr[left] == arr[right]) {
                    dp[left][right] = dp[left + 1][right - 1];
                }
                
                // or, if we cannot remove arr[left] and arr[right] in one move (the last move),
                // the subarray arr[left:right] must can be split into two subarrays
                // and remove them one by one.
                for(int mid = left; mid < right; mid++) {
                    dp[left][right] = min(dp[left][right], dp[left][mid] + dp[mid + 1][right]);
                }
            }
        }
        return dp[0][n - 1];
    }
};

// and this is the clear version:
// class Solution {
// public:
//     int minimumMoves(vector<int>& arr) {
//         int n = arr.size();
//         vector<vector<int>> dp(n, vector<int>(n, n));
//         for(int i = 0; i < n; i++) { dp[i][i] = 1; }
//         for(int i = 0; i < n - 1; i++) { dp[i][i + 1] = arr[i] == arr[i + 1] ? 1 : 2; }
//         for(int size = 3; size <= n; size++) {
//             for(int left = 0, right = left + size - 1; right < n; left++, right++) {
//                 if(arr[left] == arr[right]) { dp[left][right] = dp[left + 1][right - 1]; }
//                 for(int mid = left; mid < right; mid++) {
//                     dp[left][right] = min(dp[left][right], dp[left][mid] + dp[mid + 1][right]);
//                 }
//             }
//         }
//         return dp[0][n - 1];
//     }
// };
```
</p>


### [Java/C++/Python3] DP with Details Comments
- Author: lee215
- Creation Date: Sun Nov 03 2019 00:04:11 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 04 2019 22:24:01 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
`A[i]` can be removed alone or it makes a pair.
<br>

# Solution 1: Bottom up DP
Here\'s a [link](https://www.geeksforgeeks.org/minimum-steps-to-delete-a-string-after-repeated-deletion-of-palindrome-substrings/) to the geeksforgeeks article, for the curious.
**Java**
from @g4g

```java
    public int minimumMoves(int[] A) {
        int N = A.length;
        //  declare dp array and initialize it with 0s
        int[][] dp = new int[N + 1][N + 1];
        for (int i = 0; i <= N; i++)
            for (int j = 0; j <= N; j++)
                dp[i][j] = 0;
        // loop for subarray length we are considering
        for (int len = 1; len <= N; len++) {
            // loop with two variables i and j, denoting starting and ending of subarray
            for (int i = 0, j = len - 1; j < N; i++, j++) {
                // If subarray length is 1, then 1 step will be needed
                if (len == 1)
                    dp[i][j] = 1;
                else {
                    // delete A[i] individually and assign result for subproblem (i+1,j)
                    dp[i][j] = 1 + dp[i + 1][j];
                    // if current and next element are same, choose min from current and subproblem (i+2,j)
                    if (A[i] == A[i + 1])
                        dp[i][j] = Math.min(1 + dp[i + 2][j], dp[i][j]);
                    // loop over all right elements and suppose Kth element is same as A[i] then
                    // choose minimum from current and two subarray after ignoring ith and A[K]
                    for (int K = i + 2; K <= j; K++)
                        if (A[i] == A[K])
                            dp[i][j] = Math.min(dp[i + 1][K - 1] + dp[K + 1][j], dp[i][j]);
                }
            }
        }
        return dp[0][N - 1];
    }
```

**C++**
from @g4g
```cpp
    int minimumMoves(vector<int>& A) {
        int N = A.size();
        vector<vector<int>> dp(N + 1, vector<int>(N + 1))
        for (int len = 1; len <= N; len++) {
            for (int i = 0, j = len - 1; j < N; i++, j++) {
                if (len == 1)
                    dp[i][j] = 1;
                else {
                    dp[i][j] = 1 + dp[i + 1][j];
                    if (A[i] == A[i + 1])
                        dp[i][j] = min(1 + dp[i + 2][j], dp[i][j]);
                    for (int K = i + 2; K <= j; K++)
                        if (A[i] == A[K])
                            dp[i][j] = min(dp[i + 1][K - 1] + dp[K + 1][j], dp[i][j]);
                }
            }
        }
        return dp[0][N - 1];
    }
```
<br>

# Solution 2: Top down DP
**Java**
```java
    int[][] dp;
    public int minimumMoves(int[] A) {
        int n = A.length;
        dp = new int[n][n];
        return dfs(0, n - 1, A);
    }
    int dfs(int i, int j, int[] A) {
        if (i > j) return 0;
        if (dp[i][j] > 0) return dp[i][j];
        int res = dfs(i, j - 1, A) + 1;
        if (j > 0 && A[j] == A[j - 1])
            res = Math.min(res, dfs(i, j - 2, A) + 1);
        for (int k = i; k < j - 1; ++k)
            if (A[k] == A[j])
                res = Math.min(res, dfs(i, k - 1, A) + dfs(k + 1, j - 1, A));
        dp[i][j] = res;
        return res;
    }
```
**C++**
```cpp
    vector<vector<int>> dp;
    int minimumMoves(vector<int>& A) {
        int n = A.size();
        dp = vector(n, vector<int>(n));
        return dfs(0, n - 1, A);
    }
    int dfs(int i, int j, vector<int>& A) {
        if (i > j) return 0;
        if (dp[i][j] > 0) return dp[i][j];
        int res = dfs(i, j - 1, A) + 1;
        if (j > 0 && A[j] == A[j - 1])
            res = min(res, dfs(i, j - 2, A) + 1);
        for (int k = i; k < j - 1; ++k)
            if (A[k] == A[j])
                res = min(res, dfs(i, k - 1, A) + dfs(k + 1, j - 1, A));
        dp[i][j] = res;
        return res;
    }
```

Wrote it in 1-line but it\'s too long.
**Python3**
```py
    def minimumMoves(self, A):
        @lru_cache(None)
        def dp(i, j):
            if i > j: return 0
            return min(dp(i, k - 1) + (dp(k + 1, j - 1) if k < j - 1 else 1) for k in range(i, j + 1) if A[k] == A[j])
        return dp(0, len(A) - 1)
```

The whole version is that
```py
from functools import lru_cache
class Solution(object):
    def minimumMoves(self, A):
        @lru_cache(None)
        def dp(i, j):
            if i > j: return 0
            res = dp(i, j - 1) + 1
            if A[j] == A[j - 1]:
                res = min(res, dp(i, j - 2) + 1)
            for k in range(i, j - 1):
                if A[j] == A[k]:
                    res = min(res, dp(i, k - 1) + dp(k + 1, j - 1))
            return res
        return dp(0, len(A) - 1)
```

</p>


### DP solution with detailed video explanation
- Author: chemouna
- Creation Date: Sun Nov 03 2019 09:11:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 03 2019 09:11:02 GMT+0800 (Singapore Standard Time)

<p>

```
 def minimumMovesSol2(self, arr):
        n = len(arr)
        dp = [[n for _ in range(n)] for _ in range(n)]
        for l in range(n - 1, -1, -1):
            for r in range(l, n):
                if l == r:
                    dp[l][r] = 1
                elif l + 1 == r:
                    dp[l][r] = 2
                    if arr[l] == arr[r]:
                        dp[l][r] = 1
                else:
                    dp[l][r] = min(
                        dp[l + 1][r - 1] + (0 if arr[l] == arr[r] else 2),
                        dp[l][r], dp[l + 1][r] + 1,
                        dp[l][r - 1] + 1)
                    for k in range(l, r):
                        dp[l][r] = min(dp[l][r], dp[l][k] + dp[k + 1][r])
        return dp[0][n - 1]
```

**Video explanation**
https://www.youtube.com/watch?v=KxvTeK2nv28
</p>


