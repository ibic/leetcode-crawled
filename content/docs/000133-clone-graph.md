---
title: "Clone Graph"
weight: 133
#id: "clone-graph"
---
## Description
<div class="description">
<p>Given a reference of a node in a&nbsp;<strong><a href="https://en.wikipedia.org/wiki/Connectivity_(graph_theory)#Connected_graph" target="_blank">connected</a></strong>&nbsp;undirected graph.</p>

<p>Return a <a href="https://en.wikipedia.org/wiki/Object_copying#Deep_copy" target="_blank"><strong>deep copy</strong></a> (clone) of the graph.</p>

<p>Each node in the graph contains a val (<code>int</code>) and a list (<code>List[Node]</code>) of its neighbors.</p>

<pre>
class Node {
    public int val;
    public List&lt;Node&gt; neighbors;
}
</pre>

<p>&nbsp;</p>

<p><strong>Test case format:</strong></p>

<p>For simplicity sake, each&nbsp;node&#39;s value is the same as the node&#39;s index (1-indexed). For example, the first node with&nbsp;<code>val = 1</code>, the second node with <code>val = 2</code>, and so on.&nbsp;The graph is represented in the test case using an adjacency list.</p>

<p><b>Adjacency list</b>&nbsp;is a collection of unordered&nbsp;<b>lists</b>&nbsp;used to represent a finite graph. Each&nbsp;list&nbsp;describes the set of neighbors of a node in the graph.</p>

<p>The given node will&nbsp;always be the first node&nbsp;with&nbsp;<code>val = 1</code>. You must return the <strong>copy of the given node</strong> as a reference to the cloned graph.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/11/04/133_clone_graph_question.png" style="width: 500px; height: 550px;" />
<pre>
<strong>Input:</strong> adjList = [[2,4],[1,3],[2,4],[1,3]]
<strong>Output:</strong> [[2,4],[1,3],[2,4],[1,3]]
<strong>Explanation:</strong> There are 4 nodes in the graph.
1st node (val = 1)&#39;s neighbors are 2nd node (val = 2) and 4th node (val = 4).
2nd node (val = 2)&#39;s neighbors are 1st node (val = 1) and 3rd node (val = 3).
3rd node (val = 3)&#39;s neighbors are 2nd node (val = 2) and 4th node (val = 4).
4th node (val = 4)&#39;s neighbors are 1st node (val = 1) and 3rd node (val = 3).
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/01/07/graph.png" style="width: 163px; height: 148px;" />
<pre>
<strong>Input:</strong> adjList = [[]]
<strong>Output:</strong> [[]]
<strong>Explanation:</strong> Note that the input contains one empty list. The graph consists of only one node with val = 1 and it does not have any neighbors.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> adjList = []
<strong>Output:</strong> []
<strong>Explanation:</strong> This an empty graph, it does not have any nodes.
</pre>

<p><strong>Example 4:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/01/07/graph-1.png" style="width: 272px; height: 133px;" />
<pre>
<strong>Input:</strong> adjList = [[2],[1]]
<strong>Output:</strong> [[2],[1]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= Node.val &lt;= 100</code></li>
	<li><code>Node.val</code> is unique for each node.</li>
	<li>Number of Nodes will not exceed 100.</li>
	<li>There is no repeated edges and no self-loops in the graph.</li>
	<li>The Graph is connected and all nodes can be visited starting from the given node.</li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)
- Graph (graph)

## Companies
- Facebook - 27 (taggedByAdmin: true)
- Google - 7 (taggedByAdmin: true)
- Microsoft - 3 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- DiDi - 2 (taggedByAdmin: false)
- Docusign - 2 (taggedByAdmin: false)
- Apple - 5 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)
- VMware - 2 (taggedByAdmin: false)
- Twitter - 4 (taggedByAdmin: false)
- Pocket Gems - 2 (taggedByAdmin: true)
- Mathworks - 2 (taggedByAdmin: false)
- Qualtrics - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Splunk - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---

#### Approach 1: Depth First Search

**Intuition**

> Note: As we can see this question has garnered a lot of negative reviews. It has a lot more dislikes than the likes. We have tried to improve the problem statement to make it more understandable. However, these are the kinds of situations you might get into in an interview when the problem statement might look a little absurd. What is important then is to ask the interviewer to clarify the problem. This problem statement was confusing to me as well initially and that's why I decided to write the solution hoping to clarify most of the doubts that the readers might have had.

The basic intuition for this problem is to just copy as we go. We need to understand that we are dealing with a graph and this means a node could have any number of neighbors. This is why `neighbors` is a list. What is also crucial to understand is that we don't want to get stuck in a cycle while we are traversing the graph. According to the problem statement, any given undirected edge could be represented as two directional edges. So, if there is an undirected edge between node `A` and node `B`, the graph representation for it would have a `directed` edge from `A to B` and another from `B to A`. After all, an undirected graph is a set of nodes that are connected together, where all the edges are bidirectional. How else would you say that `A` could be reached from `B` and `B` could be reached from `A`?

<center>
<img src="../Figures/133/133_Clone_Graph_1.png" width="200"/>
</center>

To avoid getting stuck in a loop we would need some way to keep track of the nodes which have already been copied. By doing this we don't end up traversing them again.

**Algorithm**

1. Start traversing the graph from the given node.

2. We would take a hash map to store the reference of the copy of all the nodes that have already been visited and cloned. The `key` for the hash map would be the node of the original graph and corresponding `value` would be the corresponding cloned node of the cloned graph. If the node already exists in the `visited` we return corresponding stored reference of the cloned node.

    For a given edge `A - B`, since `A` is connected to `B` and `B` is also connected to `A` if we don't use `visited` we will get stuck in a cycle.

    <center>
    <img src="../Figures/133/133_Clone_Graph_2.png" width="600"/>
    </center>

3. If we don't find the node in the `visited` hash map, we create a copy of it and put it in the hash map. Note, how it's important to create a copy of the node and add to the hash map before entering recursion.

    <pre>
      clone_node = Node(node.val, [])
      visited[node] = clone_node
    </pre>

    In the absence of such an ordering, we would be caught in the recursion because on encountering the node again in somewhere down the recursion again, we will be traversing it again thus getting into cycles.

    <center>
    <img src="../Figures/133/133_Clone_Graph_3.png" width="600"/>
    </center>

4. Now make the recursive call for the neighbors of the `node`. Pay attention to how many recursion calls we will be making for any given `node`. For a given `node` the number of recursive calls would be equal to the number of its neighbors. Each recursive call made would return the clone of a neighbor. We will prepare the list of these clones returned and put into neighbors of clone `node` which we had created earlier. This way we will have cloned the given `node` and it's `neighbors`.

> Tip: Recursion could get a bit cumbersome to grasp, if you try to get into every call yourself and try to see what's happening. And why look at every call when every call does the same thing with different inputs. So, you just worry about ONE such call and let the recursion do the rest. And of course always handle the base case or the termination condition of the recursion. Otherwise how would it end?
<br>

<iframe src="https://leetcode.com/playground/89ZatNRu/shared" frameBorder="0" width="100%" height="500" name="89ZatNRu"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ since we process each node exactly once.  
* Space Complexity: $$O(N)$$. This space is occupied by the `visited` hash map and in addition to that, space would also be occupied by the recursion stack since we are adopting a recursive approach here. The space occupied by the recursion stack would be equal to $$O(H)$$ where $$H$$ is the height of the graph. Overall, the space complexity would be $$O(N)$$.
<br/>
<br/>

---

#### Approach 2: Breadth First Search

**Intuition**

We could agree DFS is a good enough solution for this problem. However, if the recursion stack is what we are worried about then DFS is not our best bet. Sure, we can write an iterative version of depth first search by using our own stack. However, we also have the BFS way of doing iterative traversal of the graph and we'll be exploring that solution as well.

<center>
<img src="../Figures/133/133_Clone_Graph_4.png" width="600"/>
</center>

The difference is only in the traversal of DFS and BFS. As the name says it all, DFS explores the depths of the graph first and BFS explores the breadth. Based on the kind of graph we are expecting we can chose one over the other. We would need the `visited` hash map in both the approaches to avoid cycles.

**Algorithm**

1. We will use a hash map to store the reference of the copy of all the nodes that have already been visited and copied. The `key` for the hash map would be the node of the original graph and corresponding `value` would be the corresponding cloned node of the cloned graph. The `visited` is used to prevent cycles and get the cloned copy of a node.

2. Add the first node to the queue. Clone the first node and add it to `visited` hash map.

3. Do the BFS traversal.
    - Pop a node from the front of the queue.
    - Visit all the neighbors of this node.
    - If any of the neighbors was already visited then it must be present in the `visited` dictionary. Get the clone of this neighbor from `visited` in that case.
    - Otherwise, create a clone and store in the `visited`.
    - Add the clones of the neighbors to the corresponding list of the clone node.

<iframe src="https://leetcode.com/playground/FAkUBZKT/shared" frameBorder="0" width="100%" height="500" name="FAkUBZKT"></iframe>

**Complexity Analysis**

* Time Complexity : $$O(N)$$ since we process each node exactly once.

* Space Complexity : $$O(N)$$. This space is occupied by the `visited` dictionary and in addition to that, space would also be occupied by the queue since we are adopting the BFS approach here. The space occupied by the queue would be equal to $$O(W)$$ where $$W$$ is the width of the graph. Overall, the space complexity would be $$O(N)$$.
<br/>

## Accepted Submission (java)
```java
/**
 * Definition for undirected graph.
 * class UndirectedGraphNode {
 *     int label;
 *     List<UndirectedGraphNode> neighbors;
 *     UndirectedGraphNode(int x) { label = x; neighbors = new ArrayList<UndirectedGraphNode>(); }
 * };
 */
import java.util.HashMap;
import java.util.Map;

public class Solution {
    public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
        Map<UndirectedGraphNode, UndirectedGraphNode> visited = new HashMap<>();
        return cloneGraph(node, visited);
    }

    public UndirectedGraphNode cloneGraph(UndirectedGraphNode node, Map<UndirectedGraphNode, UndirectedGraphNode> visited) {
        if (node == null) {
            return null;
        }
        UndirectedGraphNode newNode = new UndirectedGraphNode(node.label);
        visited.put(node, newNode);
        for (UndirectedGraphNode n: node.neighbors) {
            if (visited.containsKey(n)) {
                newNode.neighbors.add(visited.get(n));
            } else {
                newNode.neighbors.add(cloneGraph(n, visited));
            }
        }
        return newNode;
    }
}
```

## Top Discussions
### Depth First Simple Java Solution
- Author: mohamede1945
- Creation Date: Wed Mar 04 2015 18:12:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 10:27:16 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        private HashMap<Integer, UndirectedGraphNode> map = new HashMap<>();
        public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
            return clone(node);
        }
    
        private UndirectedGraphNode clone(UndirectedGraphNode node) {
            if (node == null) return null;
            
            if (map.containsKey(node.label)) {
                return map.get(node.label);
            }
            UndirectedGraphNode clone = new UndirectedGraphNode(node.label);
            map.put(clone.label, clone);
            for (UndirectedGraphNode neighbor : node.neighbors) {
                clone.neighbors.add(clone(neighbor));
            }
            return clone;
        }
    }
</p>


### C++ BFS/DFS
- Author: jianchao-li
- Creation Date: Wed Jun 24 2015 16:44:49 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 22:42:49 GMT+0800 (Singapore Standard Time)

<p>
To clone a graph, you will need to traverse it. Both BFS and DFS are for this purpose. But that is not all you need. To clone a graph, you need to have a copy of each node and you need to **avoid copying the same node for multiple times**. So you still need a mapping from an original node to its copy.

**BFS**

```cpp
class Solution {
public:
    Node* cloneGraph(Node* node) {
        if (!node) {
            return NULL;
        }
        Node* copy = new Node(node -> val, {});
        copies[node] = copy;
        queue<Node*> todo;
        todo.push(node);
        while (!todo.empty()) {
            Node* cur = todo.front();
            todo.pop();
            for (Node* neighbor : cur -> neighbors) {
                if (copies.find(neighbor) == copies.end()) {
                    copies[neighbor] = new Node(neighbor -> val, {});
                    todo.push(neighbor);
                }
                copies[cur] -> neighbors.push_back(copies[neighbor]);
            }
        }
        return copy;
    }
private:
    unordered_map<Node*, Node*> copies;
};
```

**DFS**

The following succinct DFS code is taken from [this post](https://leetcode.com/problems/clone-graph/discuss/42362/9-line-c%2B%2B-DFS-Solution).

```cpp
class Solution {
public:
    Node* cloneGraph(Node* node) {
        if (!node) {
            return NULL;
        }
        if (copies.find(node) == copies.end()) {
            copies[node] = new Node(node -> val, {});
            for (Node* neighbor : node -> neighbors) {
                copies[node] -> neighbors.push_back(cloneGraph(neighbor));
            }
        }
        return copies[node];
    }
private:
    unordered_map<Node*, Node*> copies;
};
```
</p>


### Python solutions (BFS, DFS iteratively, DFS recursively).
- Author: OldCodingFarmer
- Creation Date: Fri Sep 11 2015 05:32:55 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 15 2020 23:44:11 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def cloneGraph1(self, node): #DFS iteratively
        if not node:
            return node
        m = {node: Node(node.val)}
        stack = [node]
        while stack:
            n = stack.pop()
            for neigh in n.neighbors:
                if neigh not in m:
                    stack.append(neigh)
                    m[neigh] = Node(neigh.val)
                m[n].neighbors.append(m[neigh])
        return m[node]
        
    def cloneGraph2(self, node): # BFS 
        if not node:
            return node
        m = {node: Node(node.val)}
        deque = collections.deque([node])
        while deque:
            n = deque.popleft()
            for neigh in n.neighbors:
                if neigh not in m:
                    deque.append(neigh)
                    m[neigh] = Node(neigh.val)
                m[n].neighbors.append(m[neigh])
        return m[node]
        
    def cloneGraph(self, node): # DFS recursively
        if not node:
            return node
        m = {node: Node(node.val)}
        self.dfs(node, m)
        return m[node]
    
    def dfs(self, node, m):
        for neigh in node.neighbors:
            if neigh not in m:
                m[neigh] = Node(neigh.val)
                self.dfs(neigh, m)
            m[node].neighbors.append(m[neigh])
```
</p>


