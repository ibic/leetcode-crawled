---
title: "Array Nesting"
weight: 530
#id: "array-nesting"
---
## Description
<div class="description">
<p>A zero-indexed array A of length N contains all integers from 0 to N-1. Find and return the longest length of set S, where S[i] = {A[i], A[A[i]], A[A[A[i]]], ... } subjected to the rule below.</p>

<p>Suppose the first element in S starts with the selection of element A[i] of index = i, the next element in S should be A[A[i]], and then A[A[A[i]]]&hellip; By that analogy, we stop adding right before a duplicate element occurs in S.</p>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> A = [5,4,0,3,1,6,2]
<b>Output:</b> 4
<b>Explanation:</b> 
A[0] = 5, A[1] = 4, A[2] = 0, A[3] = 3, A[4] = 1, A[5] = 6, A[6] = 2.

One of the longest S[K]:
S[0] = {A[0], A[5], A[6], A[2]} = {5, 6, 2, 0}
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>N is an integer within the range [1, 20,000].</li>
	<li>The elements of A are all distinct.</li>
	<li>Each element of A is an integer within the range [0, N-1].</li>
</ol>

</div>

## Tags
- Array (array)

## Companies
- Bloomberg - 2 (taggedByAdmin: false)
- Apple - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

The simplest method is to iterate over all the indices of the given $$nums$$ array. For every index $$i$$ chosen, we find the element $$nums[i]$$ and increment the $$count$$ for a new element added for the current index $$i$$. Since $$nums[i]$$ has to act as the new index for finding the next element belonging to the set corresponding to the index $$i$$, the new index is $$j=nums[i]$$.

We continue this process of index updation and keep on incrementing the $$count$$ for new elements added to the set corresponding to the index $$i$$. Now, since all the elements in $$nums$$ lie in the range $$(0,..., N-1)$$, the new indices generated will never lie outside the array size limits. But, we'll always reach a point where the current element becomes equal to the element  $$nums[i]$$ with which we started the nestings in the first place. Thus, after this, the new indices generated will be just the repetitions of the previously generated ones, and thus would not lead to an increase in the size of the current set. Thus, this condition of the current number being equal to the starting number acts as the terminating condition for $$count$$ incrementation for a particular index.

We do the same process for every index chosen as the starting index. At the end, the maximum value of $$count$$ obtained gives the size of the largest set.

<iframe src="https://leetcode.com/playground/K6QuRdnw/shared" frameBorder="0" name="K6QuRdnw" width="100%" height="326"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. In worst case, for example- `[1,2,3,4,5,0]`, loop body will be executed $$n^2$$ times.

* Space complexity : $$O(1)$$. Constant space is used.

---
#### Approach #2 Using Visited Array [Accepted]

**Algorithm**

In the last approach, we observed that in the worst case, all the elements of the $$nums$$ array are added to the sets corresponding to all the starting indices. But, all these sets correspond to the same set of elements only, leading to redundant calculations.

We consider a simple example and see how this problem can be resolved. From the figure below, we can see that the elements in the current nesting shown by arrows form a cycle. Thus, the same elements will be added to the current set irrespective of the first element chosen to be added to the set out of these marked elements.

![Array_Nesting](../Figures/565/Array_Nesting.PNG)

Thus, when we add an element $$nums[j]$$ to a set corresponding to any of the indices, we mark its position as visited in a $$visited$$ array. This is done so that whenever this index is chosen as the starting index in the future, we do not go for redundant $$count$$ calculations, since we've already considered the elements linked with this index, which will be added to a new(duplicate) set.

By doing so, we ensure that the duplicate sets aren't considered again and again.

Further, we can also observe that no two elements at indices $$i$$ and $$j$$ will lead to a jump to the same index $$k$$, since it would require $$nums[i] = nums[j] = k$$, which isn't possible since all the elements are distinct. Also, because of the same reasoning, no element outside any cycle could lead to an element inside the cycle. Because of this, the use of $$visited$$ array goes correctly. 

<iframe src="https://leetcode.com/playground/XQA6FiH7/shared" frameBorder="0" name="XQA6FiH7" width="100%" height="394"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Every element of the $$nums$$ array will be considered atmost once.

* Space complexity : $$O(n)$$. $$visited$$ array of size $$n$$ is used.

---
#### Approach #3 Without Using Extra Space [Accepted]

**Algorithm**

In the last approach, the $$visited$$ array is used just to keep a track of the elements of the array which have already been visited. Instead of making use of a separate array to keep track of the same, we can mark the visited elements in the original array $$nums$$ itself. Since, the range of the elements can only be between 1 to 20,000, we can put a very large integer value $$\text{Integer.MAX_VALUE}$$ at the position which has been visited. The rest process of traversals remains the same as in the last approach.

<iframe src="https://leetcode.com/playground/7DmKnygx/shared" frameBorder="0" name="7DmKnygx" width="100%" height="394"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Every element of the $$nums$$ array will be considered atmost once.

* Space complexity : $$O(1)$$. Constant Space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] [Java] Clean Code - O(N)
- Author: alexander
- Creation Date: Sun May 28 2017 11:04:42 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 21:29:51 GMT+0800 (Singapore Standard Time)

<p>
The idea is to, start from every number, find ``circle``s in those ``index-pointer-chains``, every time you find a set (a circle) ``mark every number as visited (-1)`` so that next time you won't step on it again.
**C++**
```
class Solution {
public:
    int arrayNesting(vector<int>& a) {
        size_t maxsize = 0;
        for (int i = 0; i < a.size(); i++) {
            size_t size = 0;
            for (int k = i; a[k] >= 0; size++) {
                int ak = a[k];
                a[k] = -1; // mark a[k] as visited;
                k = ak;
            }
            maxsize = max(maxsize, size);
        }

        return maxsize;
    }
};
```
**Java**
```
public class Solution {
    public int arrayNesting(int[] a) {
        int maxsize = 0;
        for (int i = 0; i < a.length; i++) {
            int size = 0;
            for (int k = i; a[k] >= 0; size++) {
                int ak = a[k];
                a[k] = -1; // mark a[k] as visited;
                k = ak;
            }
            maxsize = Integer.max(maxsize, size);
        }

        return maxsize;
    }
}
```
</p>


### [Java/C++/Python] Straight Forward
- Author: lee215
- Creation Date: Wed May 31 2017 15:29:31 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 02 2020 14:55:06 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**:
For each element `i` in A,
repetly find `A[i]` until a visited element.
Update res to the length of the path.
<br>

# **Complexity**:
Each element will be visited once,
Time `O(N)`
Space `O(N)` (space can be `O(1)`)
<br>

**Java:**
```java
    public int arrayNesting(int[] A) {
        int res = 0, n = A.length;
        boolean[] seen = new boolean[n];
        for (int i : A) {
            int cnt = 0;
            while (!seen[i]) {
                seen[i] = true;
                cnt++;
                i = A[i];
            }
            res = Math.max(res, cnt);
        }
        return res;
    }
```

**C++:**
```cpp
    int arrayNesting(vector<int>& A) {
        int res = 0, n = A.size();
        vector<bool> seen(n);
        for (int i: A) {
            int cnt = 0;
            while (!seen[i]) {
                seen[i] = true;
                cnt++;
                i = A[i];
            }
            res = max(res, cnt);
        }
        return res;
    }
```

**Python:**
```python
    def arrayNesting(self, A):
        seen, res = [0] * len(A), 0
        for i in A:
            cnt = 0
            while not seen[i]:
                seen[i], cnt, i = 1, cnt + 1, A[i]
            res = max(res, cnt)
        return res
```

</p>


### This is actually DFS
- Author: helloworldzt
- Creation Date: Sun May 28 2017 11:04:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 06 2018 04:20:26 GMT+0800 (Singapore Standard Time)

<p>
```
	/ ** 
	 * This is actually a DFS.  Use a visited map to keep track of visited node. If a 
number is visited before, then the set that starts at this number must be smaller then
 previous max. So we can safely skip this number. In total it's O(n) complexity.
	 */



public int arrayNesting(int[] nums) {
        int max = Integer.MIN_VALUE;
        boolean[] visited = new boolean[nums.length];
        for (int i = 0; i < nums.length; i++) {
        	if (visited[i]) 
        		continue;
        	max = Math.max(max, calcLength(nums, i, visited));
        }
        return max;
    }
	
	private int calcLength(int[] nums, int start, boolean[] visited) {
		int i = start, count = 0;
		while (count == 0 || i != start) {
			visited[i] = true;
			i = nums[i];
			count++;
		}
		return count;
	}
```
</p>


