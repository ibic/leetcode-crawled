---
title: "How Many Apples Can You Put into the Basket"
weight: 1046
#id: "how-many-apples-can-you-put-into-the-basket"
---
## Description
<div class="description">
<p>You have some apples, where <code>arr[i]</code> is the weight of the <code>i</code>-th apple.&nbsp; You also have a basket that can carry up to <code>5000</code> units of weight.</p>

<p>Return the maximum number of apples you can put in the basket.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [100,200,150,1000]
<strong>Output:</strong> 4
<strong>Explanation: </strong>All 4 apples can be carried by the basket since their sum of weights is 1450.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [900,950,800,1000,700,800]
<strong>Output:</strong> 5
<strong>Explanation: </strong>The sum of weights of the 6 apples exceeds 5000 so we choose any 5 of them.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 10^3</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 10^3</code></li>
</ul>

</div>

## Tags
- Greedy (greedy)

## Companies
- Virtu Financial - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy Python with Sort
- Author: bw1226
- Creation Date: Sun Sep 22 2019 00:38:25 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 09 2020 03:11:40 GMT+0800 (Singapore Standard Time)

<p>
```
    def maxNumberOfApples(self, arr: List[int]) -> int:
        arr.sort()
        summation, cnt = 0, 0
        for number in arr:
            if summation + number < 5000:
                summation += number
                cnt += 1
            else:
                break
        return cnt
```
If you like this solution please consider giving it a star on my [github](https://github.com/bwiens/leetcode-python). Means a lot to me.
</p>


### Java backpack O(5000N)
- Author: ycj28c
- Creation Date: Thu Nov 14 2019 04:56:58 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Nov 14 2019 04:56:58 GMT+0800 (Singapore Standard Time)

<p>
Another way use backpack idea (but slower than greedy):

dp[i][j] present the max cnt when at apple i and weight j
dp[i][j] = Math.max(dp[i-1][j], dp[i-1][j-weight] + 1)
```
public int maxNumberOfApples(int[] arr) {
	int[][] dp = new int[arr.length + 1][5001];
	
	for(int i=1;i<=arr.length;i++){
		for(int j=0;j<=5000;j++){
			dp[i][j] = dp[i-1][j];
			int weight = arr[i-1];
			if(j-weight >=0){
				dp[i][j] = Math.max(dp[i][j], dp[i-1][j-weight] + 1);
			}
		}
	}
	return dp[arr.length][5000];
}
```
</p>


### Greedy | Java | Sort O(nlogn)
- Author: tacoman
- Creation Date: Tue Oct 01 2019 11:47:49 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 01 2019 11:47:49 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int maxNumberOfApples(int[] arr) {
        Arrays.sort(arr);
        int i = 0, weight = 0; 
        for(i=0; i<arr.length && weight + arr[i] <= 5000; i++) {
            weight += arr[i]; 
        }
        return i; 
    }
}
```
</p>


