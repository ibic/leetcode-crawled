---
title: "Split a String Into the Max Number of Unique Substrings"
weight: 1454
#id: "split-a-string-into-the-max-number-of-unique-substrings"
---
## Description
<div class="description">
<p>Given a string&nbsp;<code>s</code><var>,</var>&nbsp;return <em>the maximum&nbsp;number of unique substrings that the given string can be split into</em>.</p>

<p>You can split string&nbsp;<code>s</code> into any list of&nbsp;<strong>non-empty substrings</strong>, where the concatenation of the substrings forms the original string.&nbsp;However, you must split the substrings such that all of them are <strong>unique</strong>.</p>

<p>A <strong>substring</strong> is a contiguous sequence of characters within a string.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;ababccc&quot;
<strong>Output:</strong> 5
<strong>Explanation</strong>: One way to split maximally is [&#39;a&#39;, &#39;b&#39;, &#39;ab&#39;, &#39;c&#39;, &#39;cc&#39;]. Splitting like [&#39;a&#39;, &#39;b&#39;, &#39;a&#39;, &#39;b&#39;, &#39;c&#39;, &#39;cc&#39;] is not valid as you have &#39;a&#39; and &#39;b&#39; multiple times.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aba&quot;
<strong>Output:</strong> 2
<strong>Explanation</strong>: One way to split maximally is [&#39;a&#39;, &#39;ba&#39;].
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aa&quot;
<strong>Output:</strong> 1
<strong>Explanation</strong>: It is impossible to split the string any further.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>
	<p><code>1 &lt;= s.length&nbsp;&lt;= 16</code></p>
	</li>
	<li>
	<p><code>s</code> contains&nbsp;only lower case English letters.</p>
	</li>
</ul>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ Brute-Force
- Author: votrubac
- Creation Date: Sun Sep 20 2020 12:12:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 20 2020 14:50:39 GMT+0800 (Singapore Standard Time)

<p>
This one puzzled me quite a bit. It sounded too hard for the second problem in the contest.

Then I looked at the constraints, and realized that a simple DFS without memoisation would do.

```cpp
unordered_set<string> st;
int maxUniqueSplit(string &s, int p = 0) {
    if (p == s.size())
        return 0;
    int res = -1;
    for (int sz = 1; p + sz <= s.size(); ++sz) {
        auto it = st.insert(s.substr(p, sz));
        if (it.second) {
            int n_res = maxUniqueSplit(s, p + sz);
            if (n_res != -1)
                res = max(res, 1 + n_res);
            st.erase(it.first);
        }    
    }
    return res;
}
```
</p>


### [Python3] Backtracking
- Author: kunqian
- Creation Date: Sun Sep 20 2020 12:03:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 20 2020 12:03:07 GMT+0800 (Singapore Standard Time)

<p>
```python
def maxUniqueSplit(self, s: str) -> int:
	seen = set()
	return self.helper(s, seen)

def helper(self, s, seen):
	ans = 0
	if s:
		for i in range(1, len(s) + 1):
			candidate = s[:i]
			if candidate not in seen:
				seen.add(candidate)
				ans = max(ans, 1 + self.helper(s[i:], seen))
				seen.remove(candidate)
	return ans
```
</p>


