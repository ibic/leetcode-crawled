---
title: "Minimum Increment to Make Array Unique"
weight: 895
#id: "minimum-increment-to-make-array-unique"
---
## Description
<div class="description">
<p>Given an array of integers A, a <em>move</em> consists of choosing any <code>A[i]</code>, and incrementing it by <code>1</code>.</p>

<p>Return the least number of moves to make every value in <code>A</code> unique.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,2,2]</span>
<strong>Output: </strong><span id="example-output-1">1</span>
<strong>Explanation: </strong> After 1 move, the array could be [1, 2, 3].
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[3,2,1,2,1,7]</span>
<strong>Output: </strong><span id="example-output-2">6</span>
<strong>Explanation: </strong> After 6 moves, the array could be [3, 4, 1, 2, 5, 7].
It can be shown with 5 or less moves that it is impossible for the array to have all unique values.
</pre>

<p>&nbsp;</p>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= A.length &lt;= 40000</code></li>
	<li><code>0 &lt;= A[i] &lt; 40000</code></li>
</ol>

<div>
<div>&nbsp;</div>
</div>
</div>

## Tags
- Array (array)

## Companies
- IXL - 4 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: false)
- Coursera - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Counting

**Intuition**

Let's count the quantity of each element.  Clearly, we want to increment duplicated values.

For each duplicate value, we could do a "brute force" solution of incrementing it repeatedly until it is not unique.  However, we might do a lot of work - consider the work done by an array of all ones.  We should think of how to amend our solution to solve this case as well.

What we can do instead is lazily evaluate our increments.  If for example we have `[1, 1, 1, 1, 3, 5]`, we don't need to process all the increments of duplicated `1`s.  We could take three ones (`taken = [1, 1, 1]`) and continue processing.  When we find an empty place like `2`, `4`, or `6`, we can then recover that our increment will be `2-1`, `4-1`, and `6-1`.

**Algorithm**

Count the values.  For each possible value `x`:

* If there are 2 or more values `x` in `A`, save the extra duplicated values to increment later.
* If there are 0 values `x` in `A`, then a saved value `v` gets incremented to `x`.

In Java, the code is less verbose with a slight optimization:  we record only the number of saved values, and we subtract from the answer in advance.  In the `[1, 1, 1, 1, 3, 5]` example, we do `taken = 3` and `ans -= 3` in advance, and later we do `ans += 2; ans += 4; ans += 6`.  This optimization is also used in *Approach 2*.

<iframe src="https://leetcode.com/playground/ERkq3p4g/shared" frameBorder="0" width="100%" height="412" name="ERkq3p4g"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Maintain Duplicate Info

**Intuition**

Let's imagine the array is sorted and we are moving from left to right.  As in Approach 1, we want to take duplicate values to release later.

**Algorithm**

There are two cases.

* If `A[i-1] == A[i]`, we have a duplicate to take.

* If `A[i-1] < A[i]`, we might be able to place our taken values into those free positions.  Specifically, we have `give = min(taken, A[i] - A[i-1] - 1)` possible values to release, and they will have final values `A[i-1] + 1, A[i-1] + 2, ..., A[i-1] + give`.  This has a sum of $$A[i-1] * \text{give} + (\sum_{k=1}^{give})$$.

<iframe src="https://leetcode.com/playground/e8ZaKfu9/shared" frameBorder="0" width="100%" height="429" name="e8ZaKfu9"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N\log N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$ in additional space complexity, depending on the specific implementation of the built in sort.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Nov 25 2018 12:06:20 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 02 2020 22:41:53 GMT+0800 (Singapore Standard Time)

<p>
## **Solution 1: Just Sort, O(NlogN)**
Sort the input array.
Compared with previous number,
the current number need to be at least prev + 1.

Time Complexity: `O(NlogN)` for sorting
Space: `O(1)` for in-space sort

Note that you can apply "O(N)" sort in sacrifice of space.
Here we don\'t talk further about sort complexity.

**C++:**
```cpp
    int minIncrementForUnique(vector<int>& A) {
        sort(A.begin(), A.end());
        int res = 0, need = 0;
        for (int a: A) {
            res += max(need - a, 0);
            need = max(a, need)+1;
        }
        return res;
    }
```

**Java:**
```java
    public int minIncrementForUnique(int[] A) {
        Arrays.sort(A);
        int res = 0, need = 0;
        for (int a : A) {
            res += Math.max(need - a, 0);
            need = Math.max(a, need) + 1;
        }
        return res;
    }
```

**Python:**
```py
    def minIncrementForUnique(self, A):
        res = need = 0
        for i in sorted(A):
            res += max(need - i, 0)
            need = max(need + 1, i + 1)
        return res
```
<br>

## **Solution 2, O(KlogK)**

Same idea as solution 1 above.
But instead of assign value one by one,
we count the input numbers first, and assign values to all same value at one time.

This solution has only `O(N)` time for cases like `[1,1,1,1,1,1,.....]`

Time Complexity:
`O(NlogK)` using TreeMap in C++/Java
`O(N + KlogK)` using HashMap in Python
Space: `O(K)` for in-space sort


**C++:**
```cpp
    int minIncrementForUnique(vector<int>& A) {
        map<int,int> count;
        for (int a : A) count[a]++;
        int res = 0, need = 0;
        for (auto x: count) {
            res += x.second * max(need - x.first, 0) + x.second * (x.second - 1) / 2;
            need = max(need, x.first) + x.second;
        }
        return res;
    }
```

**Java:**
```java
    public int minIncrementForUnique(int[] A) {
        TreeMap<Integer, Integer> count = new TreeMap<>();
        for (int a : A) count.put(a, count.getOrDefault(a, 0) + 1);
        int res = 0, need = 0;
        for (int x: count.keySet()) {
            int v = count.get(x);
            res += v * Math.max(need - x, 0) + v * (v - 1) / 2;
            need = Math.max(need, x) + v;
        }
        return res;
    }
```

**Python:**
```py
    def minIncrementForUnique(self, A):
        c = collections.Counter(A)
        res = need = 0
        for x in sorted(c):
            res += c[x] * max(need - x, 0) + c[x] * (c[x] - 1) / 2
            need = max(need, x) + c[x]
        return res
```
<br>

## **Solution 3: Union Find, O(N)**

Time: Amortized `O(N)`
Space: `O(N)`

**C++**
```cpp
    unordered_map<int, int> root;
    int minIncrementForUnique(vector<int>& A) {
        int res = 0;
        for (int a : A)
            res += find(a) - a;
        return res;
    }
    int find(int x) {
        return root[x] = root.count(x) ? find(root[x] + 1) : x;
    }
```

**Python**
```py
    def minIncrementForUnique(self, A):
        root = {}
        def find(x):
            root[x] = find(root[x] + 1) if x in root else x
            return root[x]
        return sum(find(a) - a for a in A)
```
</p>


### C++ concise solution O(nlogn) complexity, with explanation & example step by step
- Author: ashkanxy
- Creation Date: Sun Nov 25 2018 12:14:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 25 2018 12:14:22 GMT+0800 (Singapore Standard Time)

<p>
The idea is to sort the input -O(nlogn)- , then we move forward from the beginning of the array till the end. 
As soon as we found a condition that the current element is less than or equal to the previous elements then we need to update the current array element. 
here is an example of the given input.
A = [3,2,1,2,1,7]
Sorted A = [1,**1**,2,2,3,7]
After reaching the second 1 on the array since the condition is satisfied A[i]<=A[i-1] so we need to update the A[i] by A[i-1]+1. 
At the same time we need to keep track of result by 
result += A[i-1]+ 1 - A[i];

The rest of iterations are as following :

A = [1,2,**2**,2,3,7]
res= 1

A = [1,2,3,**2**,3,7]
res= 2

A = [1,2,3,4,**3**,7]
res= 4

A = [1,2,3,4,5,**7**]
res= 6

```
class Solution {
public:
    int minIncrementForUnique(vector<int>& A) {
        int s = A.size();
        int res=0;
        if (s<2)  return 0;
        sort(A.begin(),A.end());        
        for (int i=1; i<s; ++i) {
            if (A[i]<=A[i-1]){
                res+=A[i-1]+1 -A[i];
                A[i]= A[i-1]+1;
            }
        }
        return res;
    }
};
```
</p>


### Java O(n + m) solution without sort
- Author: AzraelZealous
- Creation Date: Mon Nov 26 2018 13:43:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 26 2018 13:43:03 GMT+0800 (Singapore Standard Time)

<p>
```
    public int minIncrementForUnique(int[] A) {
        int[] count = new int[40002];
        int result = 0;
        int max = 0;
        for (int a : A) {
            count[a]++;
            max = Math.max(max, a);
        }
        for (int i = 0; i < max; i++) {
            if (count[i] <= 1) {
                continue;
            }
            int diff = count[i] - 1;
            result += diff;
            count[i + 1] += diff;
            count[i] = 1;
        }
        int diff = count[max] - 1;
        result += (1 + diff) * diff / 2;
        return result;
    }
```
Where ``n`` is the size of input, ``m`` is the largest number of input with the maximize of 40,000.
Use Arithmetic progression to count the final result.
</p>


