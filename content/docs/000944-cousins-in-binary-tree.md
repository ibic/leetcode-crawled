---
title: "Cousins in Binary Tree"
weight: 944
#id: "cousins-in-binary-tree"
---
## Description
<div class="description">
<p>In a binary tree, the root node is at depth <code>0</code>, and children of each depth <code>k</code> node are at depth <code>k+1</code>.</p>

<p>Two nodes of a binary tree are <em>cousins</em> if they have the same depth, but have <strong>different parents</strong>.</p>

<p>We are given the <code>root</code> of a binary tree with unique values, and the values <code>x</code>&nbsp;and <code>y</code>&nbsp;of two different nodes in the tree.</p>

<p>Return&nbsp;<code>true</code>&nbsp;if and only if the nodes corresponding to the values <code>x</code> and <code>y</code> are cousins.</p>

<p>&nbsp;</p>

<p><strong>Example 1:<br />
<img alt="" src="https://assets.leetcode.com/uploads/2019/02/12/q1248-01.png" style="width: 180px; height: 160px;" /></strong></p>

<pre>
<strong>Input: </strong>root = <span id="example-input-1-1">[1,2,3,4]</span>, x = <span id="example-input-1-2">4</span>, y = <span id="example-input-1-3">3</span>
<strong>Output: </strong><span id="example-output-1">false</span>
</pre>

<div>
<p><strong>Example 2:<br />
<img alt="" src="https://assets.leetcode.com/uploads/2019/02/12/q1248-02.png" style="width: 201px; height: 160px;" /></strong></p>

<pre>
<strong>Input: </strong>root = <span id="example-input-2-1">[1,2,3,null,4,null,5]</span>, x = <span id="example-input-2-2">5</span>, y = <span id="example-input-2-3">4</span>
<strong>Output: </strong><span id="example-output-2">true</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/02/13/q1248-03.png" style="width: 156px; height: 160px;" /></strong></p>

<pre>
<strong>Input: </strong>root = <span id="example-input-3-1">[1,2,3,null,4]</span>, x = 2, y = 3
<strong>Output: </strong><span id="example-output-3">false</span>
</pre>
</div>
</div>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree will be between <code>2</code> and <code>100</code>.</li>
	<li>Each node has a unique integer value from <code>1</code> to <code>100</code>.</li>
</ul>

</div>

## Tags
- Tree (tree)
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

Let's first take a quiz and see how good your aptitude is. This quiz is mainly to help you understand the question better. If you understand the question well, then just hop on to the approaches.

<center>
<img src="../Figures/993/993_Cousins_Binary_Tree_1.png" width="800"/>
</center>
<br>

Let me admit this, I was really bad at visualizing family trees and hence would perform badly in family tree questions.

The best I could do was draw a family tree and visualize it and then come to some conclusion. That's why this aptitude question was thrown at you and that explanation was needed :P

This was an easy one. What if we are given a family tree with a depth of 25 and now the same question is posed? Maybe you can answer this question only if it involves your cousins, or maybe not.
<br/>
<br/>

---

#### Approach 1: Depth First Search with Branch Pruning

**Intuition**

We can do a depth-first traversal and find the depth and parent for each node. Once we know the depth and parent for each node we can easily find out if they are cousins. Let's look at the pseudo-code for this before we try to optimize it a bit.

<pre>
// This pseudo-code recursively traverses the tree and
// records the depth and parent for each node.
function dfs(node, parentNode = None) {
	if (node != null) {
		depth[node.val] = 1 + depth[parentNode.val]
		parent[node.val] = parentNode
		dfs(node.left, node)
		dfs(node.right, node)
	}
}
</pre>

The above pseudo-code would give us the depth and parent for each node. To find out whether or not `x` and `y` are cousins is just one step away.

<pre>
// If x and y are at same depth but have different parents.
depth[x] == depth[y] and parent[x] != parent[y]
</pre>

<center>
<img src="../Figures/993/993_Cousins_Binary_Tree_2.png" width="800"/>
</center>
<br>

Now let's see if this brute-force recursive approach can be optimized for some scenarios.

> If `Node x` or `Node y` is lying very shallow in the tree, then does it make any sense to iterate down the entire tree?

In the above example, `Node 3` and `Node 4` are both cousins and hence at the same depth. What if we find one of the nodes very early on during the traversal?
How would that help us?

<center>
<img src="../Figures/993/993_Cousins_Binary_Tree_3.png" width="800"/>
</center>
<br>

The diagram above shows that we encounter `Node 3` very early on. This would help us to restrict our search space for the other node i.e. `Node 4`. For the second node, we do not need to go beyond the depth at which the first node was found, thus saving traversal of the subtree below node 3.

We can search for the desired nodes in the tree recursively. Whenever either of the given nodes is found, we record its parent and depth.

**Algorithm**

1. Start traversing the tree from the root node. Look for `Node x` and `Node y`.

2. Record the depth when the first node i.e. either of `x` or `y` is found and return `true`.

3. Once one of the nodes is discovered, for every other recursive call after this discovery, we return `false` if the current depth is more than the recorded depth. This basically means we didn't find the other node at the same depth and there is no point going beyond. This step of pruning helps to speed up the recursion by reducing the number of recursive calls.

4. Return `true` when the other node is discovered and has the same depth as the recorded depth.

5. Recurse the left and the right subtree of the current node. If both left and right recursions return `true` and the current node is not their immediate parent, then `Node x` and `Node y` are cousins. Thus, `isCousin` is set to value `true`.

<iframe src="https://leetcode.com/playground/wMB7jDxE/shared" frameBorder="0" width="100%" height="500" name="wMB7jDxE"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of nodes in the binary tree. In the worst case, we might have to visit all the nodes of the binary tree.

	Let's look into one such scenario. When both `Node x` and `Node y` are the leaf nodes and at the last level of the tree, the algorithm has no reasons to prune the recursion. It can only come to a conclusion once it visits both the nodes. If one of these nodes is the last node to be discovered the algorithm inevitably goes through each and every node in the tree.

* Space Complexity: $$O(N)$$. This is because the maximum amount of space utilized by the recursion stack would be $$N$$, as the height of a skewed binary tree could be, at worst, $$N$$. For a left skewed or a right skewed binary tree, where the desired nodes are lying at the maximum depth possible, the algorithm would have to maintain a recursion stack of the height of the tree.
<br/>
<br/>

---

#### Approach 2: Breadth First Search with Early Stopping

I will repeat my question
> If `Node x` or `Node y` is lying very shallow in the tree, then does it make any sense to iterate down the entire tree?

<center>
<img src="../Figures/993/993_Cousins_Binary_Tree_4.png" width="800"/>
</center>
<br>

Since this problem is about finding cousins, i.e. nodes lying at the same level/depth, it seems more natural to do a level order traversal of the tree.

If we do a level order traversal for the aforementioned example, we would only traverse until depth 2. At depth 2, we discover `Node 4`, but we do not find `Node 6` at the same level. Hence we can just stop our traversal and conclude that the nodes are not cousins.

Note, if the nodes are cousins, we would find both the nodes at the same depth.
However, this is also true for siblings. We need to figure out how to determine when two nodes are siblings. One way to find out that they are siblings is when we are adding the nodes to the queue. If `Node x` and `Node y` are left and right children of a node, this would mean that they are siblings. Therefore, we would return `false`.

There is a cleaner implementation for the level order traversal for this problem, though. For each node, we can add a delimiter to the queue after its children are added. These delimiters help us define boundaries for each parent and the siblings that are confined within those.
This implementation was shared by [@votrubac](https://leetcode.com/votrubac/). You can refer to his C++ implementation in the [discussion section](https://leetcode.com/problems/cousins-in-binary-tree/discuss/238624/C++-level-order-traversal)

**Algorithm**

1. Do a level order traversal of the tree using a queue.

2. For every node that is popped off the queue, check if the node is either `Node x` or `Node y`. If it is, then for the first time, set both `siblings` and `cousins` flags as `true`. The flags are set as `true` to mark the possibility of siblings or cousins.

3. To distinguish siblings from cousins, we insert markers in the queue. After inserting the children for each node, we also insert a `null` marker. This marker defines a boundary for each set of siblings and hence helps us to differentiate a pair of siblings from cousins.

4. Whenever we encounter the `null` marker during our traversal, we set the `siblings` flag to `false`. This is because the marker marks the end of the siblings territory.

5. The second time we encounter a node which is equal to `Node x` or `Node y` we will have clarity about whether or not we are still within the siblings boundary. If we are within the siblings boundary, i.e. if the `siblings` flag is still `true`, then we return `false`. Otherwise, we return `true`.

<center>
<img src="../Figures/993/993_Cousins_Binary_Tree_5.png" width="600"/>
</center>
<br>

In the above diagram, `Node 3` and `Node 3'` are children of the same parent. Hence the `siblings` flag remains `true`.

<center>
<img src="../Figures/993/993_Cousins_Binary_Tree_6.png" width="800"/>
</center>
<br>

Clearly, `Node 3` and `Node 4` have different parents. Hence, we do encounter a `null` marker after `Node 3'`. The `null` marker marks the end of siblings for `Node 3`, and hence when `Node 4` is found, we know it is the cousin of `Node 3`.

<iframe src="https://leetcode.com/playground/sLHESwXi/shared" frameBorder="0" width="100%" height="500" name="sLHESwXi"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of nodes in the binary tree. In the worst case, we might have to visit all the nodes of the binary tree. Similar to approach 1 this approach would also have a complexity of $$O(N)$$ when the `Node x` and `Node y` are present at the last level of the binary tree. The algorithm would follow the standard BFS approach and end up in checking each node before discovering the desired nodes.

* Space Complexity: $$O(N)$$. In the worst case, we need to store all the nodes of the last level in the queue. The last level of a binary tree can have a maximum of $$\frac{N}{2}$$ nodes. Not to forget we would also need space for $$\frac{N}{4}$$ `null` markers, one for each pair of siblings. That results in a space complexity of $$O(\frac{3N}{4})$$ = $$O(N)$$ (You are right Big-O notation doesn't care about constants).
<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] BFS time and space beat 100%
- Author: Frimish
- Creation Date: Mon Feb 18 2019 13:22:15 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 18 2019 13:22:15 GMT+0800 (Singapore Standard Time)

<p>

	public boolean isCousins(TreeNode root, int A, int B) {
        if (root == null) return false;
		Queue<TreeNode> queue = new LinkedList<>();
		queue.offer(root);
		while (!queue.isEmpty()) {
			int size = queue.size();
			boolean isAexist = false;		
			boolean isBexist = false;		
			for (int i = 0; i < size; i++) {
				TreeNode cur = queue.poll();
                if (cur.val == A) isAexist = true;
                if (cur.val == B) isBexist = true;
				if (cur.left != null && cur.right != null) { 
					if (cur.left.val == A && cur.right.val == B) { 
						return false;
					}
					if (cur.left.val == B && cur.right.val == A) { 
						return false;
					}
				}
				if (cur.left != null) {
					queue.offer(cur.left);
				}
				if (cur.right != null) {
					queue.offer(cur.right);
				}
			}
			if (isAexist && isBexist)  return true;
		}
		return false;
    }
</p>


### Java easy to understand and clean solution
- Author: daijidj
- Creation Date: Tue Feb 19 2019 14:18:18 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Feb 19 2019 14:18:18 GMT+0800 (Singapore Standard Time)

<p>
```
//dfs
class Solution {
    TreeNode xParent = null;
    TreeNode yParent = null;
    int xDepth = -1, yDepth = -1;
    
    public boolean isCousins(TreeNode root, int x, int y) {
        getDepthAndParent(root, x, y, 0, null);
        return xDepth == yDepth && xParent != yParent? true: false;
    }
    //get both the depth and parent for x and y
    public void getDepthAndParent(TreeNode root, int x, int y, int depth, TreeNode parent){
        if(root == null){
            return;
        }
        if(root.val == x){
            xParent = parent;
            xDepth = depth;
        }else if(root.val == y){
            yParent = parent;
            yDepth = depth;
        }       
        getDepthAndParent(root.left, x, y, depth + 1, root);
        getDepthAndParent(root.right, x, y, depth + 1, root);
    }
}
```
</p>


### Python straightforward & clean DFS
- Author: cenkay
- Creation Date: Sat Mar 02 2019 10:32:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 02 2019 10:32:08 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def isCousins(self, root: TreeNode, x: int, y: int) -> bool:
        def dfs(node, parent, depth, mod):
            if node:
                if node.val == mod:
                    return depth, parent
                return dfs(node.left, node, depth + 1, mod) or dfs(node.right, node, depth + 1, mod)
        dx, px, dy, py = dfs(root, None, 0, x) + dfs(root, None, 0, y)
        return dx == dy and px != py
```
</p>


