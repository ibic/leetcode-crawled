---
title: "Open the Lock"
weight: 674
#id: "open-the-lock"
---
## Description
<div class="description">
<p>You have a lock in front of you with 4 circular wheels. Each wheel has 10 slots: <code>&#39;0&#39;, &#39;1&#39;, &#39;2&#39;, &#39;3&#39;, &#39;4&#39;, &#39;5&#39;, &#39;6&#39;, &#39;7&#39;, &#39;8&#39;, &#39;9&#39;</code>. The wheels can rotate freely and wrap around: for example we can turn <code>&#39;9&#39;</code> to be <code>&#39;0&#39;</code>, or <code>&#39;0&#39;</code> to be <code>&#39;9&#39;</code>. Each move consists of turning one wheel one slot.</p>

<p>The lock initially starts at <code>&#39;0000&#39;</code>, a string representing the state of the 4 wheels.</p>

<p>You are given a list of <code>deadends</code> dead ends, meaning if the lock displays any of these codes, the wheels of the lock will stop turning and you will be unable to open it.</p>

<p>Given a <code>target</code> representing the value of the wheels that will unlock the lock, return the minimum total number of turns required to open the lock, or -1 if it is impossible.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> deadends = [&quot;0201&quot;,&quot;0101&quot;,&quot;0102&quot;,&quot;1212&quot;,&quot;2002&quot;], target = &quot;0202&quot;
<strong>Output:</strong> 6
<strong>Explanation:</strong>
A sequence of valid moves would be &quot;0000&quot; -&gt; &quot;1000&quot; -&gt; &quot;1100&quot; -&gt; &quot;1200&quot; -&gt; &quot;1201&quot; -&gt; &quot;1202&quot; -&gt; &quot;0202&quot;.
Note that a sequence like &quot;0000&quot; -&gt; &quot;0001&quot; -&gt; &quot;0002&quot; -&gt; &quot;0102&quot; -&gt; &quot;0202&quot; would be invalid,
because the wheels of the lock become stuck after the display becomes the dead end &quot;0102&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> deadends = [&quot;8888&quot;], target = &quot;0009&quot;
<strong>Output:</strong> 1
<strong>Explanation:</strong>
We can turn the last wheel in reverse to move from &quot;0000&quot; -&gt; &quot;0009&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> deadends = [&quot;8887&quot;,&quot;8889&quot;,&quot;8878&quot;,&quot;8898&quot;,&quot;8788&quot;,&quot;8988&quot;,&quot;7888&quot;,&quot;9888&quot;], target = &quot;8888&quot;
<strong>Output:</strong> -1
Explanation:
We can&#39;t reach the target without getting stuck.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> deadends = [&quot;0000&quot;], target = &quot;8888&quot;
<strong>Output:</strong> -1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;=&nbsp;deadends.length &lt;= 500</code></li>
	<li><code><font face="monospace">deadends[i].length == 4</font></code></li>
	<li><code><font face="monospace">target.length == 4</font></code></li>
	<li>target <strong>will not be</strong> in the list <code>deadends</code>.</li>
	<li><code>target</code> and <code>deadends[i]</code> consist of digits only.</li>
</ul>

</div>

## Tags
- Breadth-first Search (breadth-first-search)

## Companies
- Google - 8 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Breadth-First Search [Accepted]

**Intuition**

We can think of this problem as a shortest path problem on a graph: there are `10000` nodes (strings `'0000'` to `'9999'`), and there is an edge between two nodes if they differ in one digit, that digit differs by 1 (wrapping around, so `'0'` and `'9'` differ by 1), and if both nodes are not in `deadends`.

**Algorithm**

To solve a shortest path problem, we use a breadth-first search.  The basic structure uses a Queue `queue` plus a Set `seen` that records if a node has ever been enqueued.  This pushes all the work to the `neighbors` function - in our Python implementation, all the code after `while queue:` is template code, except for `if node in dead: continue`.

As for the `neighbors` function, for each position in the lock `i = 0, 1, 2, 3`, for each of the turns `d = -1, 1`, we determine the value of the lock after the `i`-th wheel has been turned in the direction `d`.

Care should be taken in our algorithm, as the graph does not have an edge unless *both* nodes are not in `deadends`.  If our `neighbors` function checks only the `target` for being in `deadends`, we also need to check whether `'0000'` is in `deadends` at the beginning.  In our implementation, we check at the visitor level so as to neatly handle this problem in all cases.

In Java, our implementation also inlines the neighbors function for convenience, and uses `null` inputs in the `queue` to represent a layer change.  When the layer changes, we `depth++` our global counter, and `queue.peek() != null` checks if there are still nodes enqueued.

<iframe src="https://leetcode.com/playground/y9nkNbyv/shared" frameBorder="0" width="100%" height="500" name="y9nkNbyv"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2 * \mathcal{A}^N + D)$$ where $$\mathcal{A}$$ is the number of digits in our alphabet, $$N$$ is the number of digits in the lock, and $$D$$ is the size of `deadends`.  We might visit every lock combination, plus we need to instantiate our set `dead`.  When we visit every lock combination, we spend $$O(N^2)$$ time enumerating through and constructing each node. 

* Space Complexity: $$O(\mathcal{A}^N + D)$$, for the `queue` and the set `dead`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Regular java BFS solution and 2-end BFS solution with improvement
- Author: yaoyimingg
- Creation Date: Mon Dec 25 2017 14:34:23 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 02:38:35 GMT+0800 (Singapore Standard Time)

<p>
Basic solution, runtime: 260ms
```
class Solution {
    public int openLock(String[] deadends, String target) {
        Queue<String> q = new LinkedList<>();
        Set<String> deads = new HashSet<>(Arrays.asList(deadends));
        Set<String> visited = new HashSet<>();
        q.offer("0000");
        visited.add("0000");
        int level = 0;
        while(!q.isEmpty()) {
            int size = q.size();
            while(size > 0) {
                String s = q.poll();
                if(deads.contains(s)) {
                    size --;
                    continue;
                }
                if(s.equals(target)) return level;
                StringBuilder sb = new StringBuilder(s);
                for(int i = 0; i < 4; i ++) {
                    char c = sb.charAt(i);
                    String s1 = sb.substring(0, i) + (c == '9' ? 0 : c - '0' + 1) + sb.substring(i + 1);
                    String s2 = sb.substring(0, i) + (c == '0' ? 9 : c - '0' - 1) + sb.substring(i + 1);
                    if(!visited.contains(s1) && !deads.contains(s1)) {
                        q.offer(s1);
                        visited.add(s1);
                    }
                    if(!visited.contains(s2) && !deads.contains(s2)) {
                        q.offer(s2);
                        visited.add(s2);
                    }
                }
                size --;
            }
            level ++;
        }
        return -1;
    }
}
```
Regular 2 - end solution, runtime: 85ms
```
class Solution {
    public int openLock(String[] deadends, String target) {
        Set<String> begin = new HashSet<>();
        Set<String> end = new HashSet<>();
        Set<String> deads = new HashSet<>(Arrays.asList(deadends));
        begin.add("0000");
        end.add(target);
        int level = 0;
        while(!begin.isEmpty() && !end.isEmpty()) {
            Set<String> temp = new HashSet<>();
            for(String s : begin) {
                if(end.contains(s)) return level;
                if(deads.contains(s)) continue;
                deads.add(s);
                StringBuilder sb = new StringBuilder(s);
                for(int i = 0; i < 4; i ++) {
                    char c = sb.charAt(i);
                    String s1 = sb.substring(0, i) + (c == '9' ? 0 : c - '0' + 1) + sb.substring(i + 1);
                    String s2 = sb.substring(0, i) + (c == '0' ? 9 : c - '0' - 1) + sb.substring(i + 1);
                    if(!deads.contains(s1))
                        temp.add(s1);
                    if(!deads.contains(s2))
                        temp.add(s2);
                }
            }
            level ++;
            begin = end;
            end = temp;
        }
        return -1;
    }
}
```
You can still improve this 2-end solution, by adding:
```
if (begin.size() > end.size()) {
    temp = begin;
    begin = end;
    end = temp;
}
```
By always picking a smaller set, this process could reduce a little(since in this problem the scale on both sides are similar) time complexity and memory complexity. Here's the full version, runtime: 80ms
```
class Solution {
    public int openLock(String[] deadends, String target) {
        Set<String> begin = new HashSet<>();
        Set<String> end = new HashSet<>();
        Set<String> deads = new HashSet<>(Arrays.asList(deadends));
        begin.add("0000");
        end.add(target);
        int level = 0;
        Set<String> temp;
        while(!begin.isEmpty() && !end.isEmpty()) {
            if (begin.size() > end.size()) {
                temp = begin;
                begin = end;
                end = temp;
            }
            temp = new HashSet<>();
            for(String s : begin) {
                if(end.contains(s)) return level;
                if(deads.contains(s)) continue;
                deads.add(s);
                StringBuilder sb = new StringBuilder(s);
                for(int i = 0; i < 4; i ++) {
                    char c = sb.charAt(i);
                    String s1 = sb.substring(0, i) + (c == '9' ? 0 : c - '0' + 1) + sb.substring(i + 1);
                    String s2 = sb.substring(0, i) + (c == '0' ? 9 : c - '0' - 1) + sb.substring(i + 1);
                    if(!deads.contains(s1))
                        temp.add(s1);
                    if(!deads.contains(s2))
                        temp.add(s2);
                }
            }
            level ++;
            begin = temp;
        }
        return -1;
    }
}
```
</p>


### Accepted Python/Java BFS  + how to avoid TLE
- Author: li-_-il
- Creation Date: Sun Dec 24 2017 12:59:05 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 00:31:23 GMT+0800 (Singapore Standard Time)

<p>
Shortest path finding, when the weights are constant, as in this case = 1, BFS is the best way to go.
Best way to avoid TLE is by using deque and popleft() .
[Using list() and pop(0) is a linear operation in Python, resulting in TLE]

Python:

```
    def openLock(self, deadends, target):
        marker, depth = \'x\', -1
        visited, q = set(deadends), deque([\'0000\'])

        while q:
            size = len(q)
            depth += 1
            for _ in range(size):
                node = q.popleft()
                if node == target: return depth
                if node in visited: continue
                visited.add(node)
                q.extend(self.successors(node))
        return -1

    def successors(self, src):
        res = []
        for i, ch in enumerate(src):
            num = int(ch)
            res.append(src[:i] + str((num - 1) % 10) + src[i+1:])
            res.append(src[:i] + str((num + 1) % 10) + src[i+1:])
        return res
```

Java:

```
public static int openLock(String[] deadends, String target) {
        Queue<String> q = new LinkedList<>();
        Set<String> visited = new HashSet<>(Arrays.asList(deadends));
        int depth = -1;
        q.addAll(Arrays.asList("0000"));
        while(!q.isEmpty()) {
            depth++;
            int size = q.size();
            for(int i = 0; i < size; i++) {
                String node = q.poll();
                if(node.equals(target))
                    return depth;
                if(visited.contains(node))
                    continue;
                visited.add(node);
                q.addAll(getSuccessors(node));
            }
        }
        return -1;
    }
	
    private static List<String> getSuccessors(String str) {
        List<String> res = new LinkedList<>();
        for (int i = 0; i < str.length(); i++) {
            res.add(str.substring(0, i) + (str.charAt(i) == \'0\' ? 9 :  str.charAt(i) - \'0\' - 1) + str.substring(i+1));
            res.add(str.substring(0, i) + (str.charAt(i) == \'9\' ? 0 :  str.charAt(i) - \'0\' + 1) + str.substring(i+1));
        }
        return res;
    }
```
</p>


### BFS solution C++
- Author: PlotCondor
- Creation Date: Sun Dec 24 2017 12:09:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 07:01:29 GMT+0800 (Singapore Standard Time)

<p>
Every node has 8 edges. The nodes in dead ends cannot be visited. Find the shortest path from the initial node to the target.


```
class Solution {
public:
    int openLock(vector<string>& deadends, string target) {
        unordered_set<string> dds(deadends.begin(), deadends.end());
        unordered_set<string> visited;
        queue<string> bfs;
        string init = "0000";
        if (dds.find(init) != dds.end()) return -1;
        visited.insert("0000");
        bfs.push("0000");
        int res = 0;
        while (!bfs.empty()) {
            int sz = bfs.size();
            for (int i = 0; i < sz; i++) {
                string t = bfs.front(); bfs.pop();
                vector<string> nbrs = move(nbrStrs(t));
                for (auto s : nbrs) {
                    if (s == target) return ++res;
                    if (visited.find(s) != visited.end()) continue;
                    if (dds.find(s) == dds.end()) {
                        bfs.push(s);
                        visited.insert(s);
                    }
                }
            }
            ++res;
        }
        return -1;
    }
    
    
    vector<string> nbrStrs(string key) {
        vector<string> res;
        for (int i = 0 ; i < 4; i++) {
            string tmp = key;
            tmp[i] = (key[i] - '0' + 1) % 10 + '0';
            res.push_back(tmp);
            tmp[i] = (key[i] - '0' - 1 + 10) % 10 + '0';
            res.push_back(tmp);
         }
        return res;
    }
};
```

Bidirectional BFS improves the efficiency
```
    int openLock(vector<string>& deadends, string target) {
        unordered_set<string> dds(deadends.begin(), deadends.end());
        unordered_set<string> q1, q2, pass, visited;
        string init = "0000";
        if (dds.find(init) != dds.end() || dds.find(target) != dds.end()) return -1;
        visited.insert("0000");
        q1.insert("0000"), q2.insert(target);
        int res = 0;
        while (!q1.empty() && !q2.empty()) {
            if (q1.size() > q2.size()) swap(q1, q2);
            pass.clear();
            for (auto ss : q1) {
                vector<string> nbrs = nbrStrs(ss);
                for (auto s : nbrs) {
                    if (q2.find(s) != q2.end()) return res + 1;
                    if (visited.find(s) != visited.end()) continue;
                    if (dds.find(s) == dds.end()) {
                        pass.insert(s);
                        visited.insert(s);
                    }
                }
            }
            swap(q1, pass);
            res++;
        }
        return -1;
    }
```
</p>


