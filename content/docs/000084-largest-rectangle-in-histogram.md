---
title: "Largest Rectangle in Histogram"
weight: 84
#id: "largest-rectangle-in-histogram"
---
## Description
<div class="description">
<p>Given <em>n</em> non-negative integers representing the histogram&#39;s bar height where the width of each bar is 1, find the area of largest rectangle in the histogram.</p>

<p>&nbsp;</p>

<p><img src="https://assets.leetcode.com/uploads/2018/10/12/histogram.png" style="width: 188px; height: 204px;" /><br />
<small>Above is a histogram where width of each bar is 1, given height = <code>[2,1,5,6,2,3]</code>.</small></p>

<p>&nbsp;</p>

<p><img src="https://assets.leetcode.com/uploads/2018/10/12/histogram_area.png" style="width: 188px; height: 204px;" /><br />
<small>The largest rectangle is shown in the shaded area, which has area = <code>10</code> unit.</small></p>

<p>&nbsp;</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> [2,1,5,6,2,3]
<strong>Output:</strong> 10
</pre>

</div>

## Tags
- Array (array)
- Stack (stack)

## Companies
- Amazon - 9 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- ByteDance - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Huawei - 2 (taggedByAdmin: false)
- Bloomberg - 4 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Flipkart - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
[TOC]
## Summary

We need to find the rectangle of largest area that can be formed by using the given bars of histogram.

## Solution

---
#### Approach 1: Brute Force

Firstly, we need to take into account the fact that the height of the rectangle
formed between any two bars will always be limited by the height of the shortest
bar lying between them which can be understood by looking at the figure below:

![Largest Rectangle](https://leetcode.com/media/original_images/84_Largest_Rectangle1.PNG)

Thus, we can simply start off by considering every possible
pair of bars and finding the area of the rectangle formed between them using the
height of the shortest bar lying between them as the height
 and the spacing between
them as the width of the rectangle. We can thus, find the required rectangle with the
 maximum area.


 <iframe src="https://leetcode.com/playground/Ajp6g5ny/shared" frameBorder="0" width="100%" height="310" name="Ajp6g5ny"></iframe>


**Complexity Analysis**

* Time complexity : $$O(n^3)$$. We have to find the minimum height bar $$O(n)$$ lying
between every pair $$O(n^2)$$.

* Space complexity : $$O(1)$$. Constant space is used.
<br />
<br />
---
#### Approach 2: Better Brute Force

**Algorithm**

We can do one slight modification in the previous approach to optimize it to some extent.
 Instead of taking every possible pair and then finding the bar of minimum height
 lying between them everytime, we can find the bar of minimum height for
 current pair by using the minimum height bar of the previous pair.

In mathematical terms, $$minheight=\min(minheight, heights(j))$$, where $$heights(j)$$
 refers to the height of the $$j$$th bar.


 <iframe src="https://leetcode.com/playground/ZBhW9Xc7/shared" frameBorder="0" width="100%" height="276" name="ZBhW9Xc7"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. Every possible pair is considered

* Space complexity : $$O(1)$$. No extra space is used.
<br />
<br />
---
#### Approach 3: Divide and Conquer Approach

**Algorithm**

This approach relies on the observation that the rectangle with maximum area will be
the maximum of:

1. The widest possible rectangle with height equal to the height of the shortest bar.

2. The largest rectangle confined to the left of the shortest bar(subproblem).

3. The largest rectangle confined to the right of the shortest bar(subproblem).

Let's take an example:
```
[6, 4, 5, 2, 4, 3, 9]
```

 Here, the shortest bar is of height 2. The area of the widest rectangle using this
  bar as height is 2x8=16. Now, we need to look for cases 2 and 3 mentioned above.
  Thus, we repeat the same process to the left and right of 2. In the left of 2, 4
  is the minimum, forming an area of rectangle 4x3=12. Further, rectangles of area 6x1=6 and
   5x1=5 exist in its left and right respectively. Similarly we find an area of 3x3=9, 4x1=4 and 9x1=9
   to the left of 2. Thus, we get 16 as the correct maximum area. See the figure below for further clarification:

   ![Divide and Conquer](https://leetcode.com/media/original_images/84_Largest_Rectangle2.PNG)


<iframe src="https://leetcode.com/playground/xg5CtMHZ/shared" frameBorder="0" width="100%" height="293" name="xg5CtMHZ"></iframe>

**Complexity Analysis**

* Time complexity :

    Average Case: $$O\big(n \log n\big)$$.

    Worst Case: $$O(n^2)$$. If the numbers in the array are sorted, we don't gain the advantage of divide and conquer.

* Space complexity : $$O(n)$$. Recursion with worst case depth $$n$$.
<br />
<br />
---
#### Approach 4: Better Divide and Conquer

**Algorithm**

You can observe that in the Divide and Conquer Approach, we gain the advantage, since
 the large problem is divided into substantially smaller subproblems. But, we won't gain
  much advantage with that approach if the array happens to be sorted in either
  ascending or descending order, since every time we need to find the minimum number in a
   large subarray $$O(n)$$. Thus, the overall complexity becomes $$O(n^2)$$ in the worst case.
   We can reduce the time complexity by using a Segment Tree to find the minimum every time which
   can be done in $$O\big(\log n\big)$$ time.

   For implementation, click [here](https://leetcode.com/problems/largest-rectangle-in-histogram/discuss/28941/segment-tree-solution-just-another-idea-onlogn-solution).

**Complexity Analysis**

* Time complexity : $$O\big(n\log n\big)$$. Segment tree takes $$\log n$$ for a total of $$n$$ times.

* Space complexity : $$O(n)$$. Space required for Segment Tree.
<br />
<br />
---
#### Approach 5: Using Stack

**Algorithm**

In this approach, we maintain a stack. Initially, we push a -1 onto the stack to mark the end.
We start with the leftmost bar and keep
pushing the current bar's index onto the stack until we get two successive numbers
 in descending order, i.e. until we get $$a[i]<a[i-1]$$. Now, we start popping the
 numbers from the stack until we hit a number $$stack[j]$$ on the stack such that $$a\big[stack[j]\big] \leq a[i]$$.
 Every time we pop, we find out the area of rectangle formed using the current element as the height
  of the rectangle and the difference between the the current element's index pointed to in the original array and the element $$stack[top-1] -1$$ as the width
  i.e. if we pop an element $$stack[top]$$ and i is the current index to which we are pointing in the original array, the current area of the rectangle will be considered as:

  $$
  (i-stack[top-1]-1) \times a\big[stack[top]\big].
  $$

  Further, if we reach the end of the array, we pop all the elements of the
  stack and at every pop, this time we use the following equation to find the area:
     $$(stack[top]-stack[top-1]) \times a\big[stack[top]\big]$$, where $$stack[top]$$ refers to the
      element just popped. Thus, we can get the area of the
   of the largest rectangle by comparing the new area found everytime.

   The following example will clarify the process further:
   ```
[6, 7, 5, 2, 4, 5, 9, 3]
   ```

!?!../Documents/84_Largest_Rectangle.json:1000,563!?!

<iframe src="https://leetcode.com/playground/vMvUjZy9/shared" frameBorder="0" width="100%" height="310" name="vMvUjZy9"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. $$n$$ numbers are pushed and popped.

* Space complexity : $$O(n)$$. Stack is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 5ms O(n) Java solution explained (beats 96%)
- Author: anton4
- Creation Date: Sat Mar 05 2016 20:14:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 09:39:43 GMT+0800 (Singapore Standard Time)

<p>
For any bar `i` the maximum rectangle is of width `r - l - 1` where r - is the last coordinate of the bar to the **right** with height `h[r] >= h[i]` and l - is the last coordinate of the bar to the **left** which height `h[l] >= h[i]`

So if for any `i` coordinate we know his utmost higher (or of the same height) neighbors to the right and to the left, we can easily find the largest rectangle:

    int maxArea = 0;
    for (int i = 0; i < height.length; i++) {
        maxArea = Math.max(maxArea, height[i] * (lessFromRight[i] - lessFromLeft[i] - 1));
    }

The main trick is how to effectively calculate `lessFromRight` and `lessFromLeft` arrays. The trivial solution is to use **O(n^2)** solution and for each `i` element first find his left/right heighbour in the second inner loop just iterating back or forward:

    for (int i = 1; i < height.length; i++) {              
        int p = i - 1;
        while (p >= 0 && height[p] >= height[i]) {
            p--;
        }
        lessFromLeft[i] = p;              
    }

The only line change shifts this algorithm from **O(n^2)** to **O(n)** complexity: we don't need to rescan each item to the left - we can reuse results of previous calculations and "jump" through indices in quick manner:

    while (p >= 0 && height[p] >= height[i]) {
          p = lessFromLeft[p];
    }

Here is the whole solution:

    public static int largestRectangleArea(int[] height) {
        if (height == null || height.length == 0) {
            return 0;
        }
        int[] lessFromLeft = new int[height.length]; // idx of the first bar the left that is lower than current
        int[] lessFromRight = new int[height.length]; // idx of the first bar the right that is lower than current
        lessFromRight[height.length - 1] = height.length;
        lessFromLeft[0] = -1;

        for (int i = 1; i < height.length; i++) {
            int p = i - 1;

            while (p >= 0 && height[p] >= height[i]) {
                p = lessFromLeft[p];
            }
            lessFromLeft[i] = p;
        }

        for (int i = height.length - 2; i >= 0; i--) {
            int p = i + 1;

            while (p < height.length && height[p] >= height[i]) {
                p = lessFromRight[p];
            }
            lessFromRight[i] = p;
        }

        int maxArea = 0;
        for (int i = 0; i < height.length; i++) {
            maxArea = Math.max(maxArea, height[i] * (lessFromRight[i] - lessFromLeft[i] - 1));
        }

        return maxArea;
    }
</p>


### AC Python clean solution using stack 76ms
- Author: dietpepsi
- Creation Date: Sat Oct 24 2015 00:45:25 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 19:02:29 GMT+0800 (Singapore Standard Time)

<p>
    def largestRectangleArea(self, height):
        height.append(0)
        stack = [-1]
        ans = 0
        for i in xrange(len(height)):
            while height[i] < height[stack[-1]]:
                h = height[stack.pop()]
                w = i - stack[-1] - 1
                ans = max(ans, h * w)
            stack.append(i)
        height.pop()
        return ans



    # 94 / 94 test cases passed.
    # Status: Accepted
    # Runtime: 76 ms
    # 97.34%

The stack maintain the indexes of buildings with ascending height. Before adding a new building pop the building who is taller than the new one. The building popped out represent the height of a rectangle with the new building as the right boundary and the current stack top as the left boundary. Calculate its area and update ans of maximum area. Boundary is handled using dummy buildings.
</p>


### Short and Clean O(n) stack based JAVA solution
- Author: legendaryengineer
- Creation Date: Tue Jan 20 2015 04:51:19 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 01 2020 03:30:40 GMT+0800 (Singapore Standard Time)

<p>
For explanation, please see https://bit.ly/2we8Wfx

```
    public int largestRectangleArea(int[] heights) {
        int len = heights.length;
        Stack<Integer> s = new Stack<>();
        int maxArea = 0;
        for (int i = 0; i <= len; i++){
            int h = (i == len ? 0 : heights[i]);
            if (s.isEmpty() || h >= heights[s.peek()]) {
                s.push(i);
            } else {
                int tp = s.pop();
                maxArea = Math.max(maxArea, heights[tp] * (s.isEmpty() ? i : i - 1 - s.peek()));
                i--;
            }
        }
        return maxArea;
    }
```

OP\'s Note: Two years later I need to interview again. I came to this problem and I couldn\'t understand this solution. After reading the explanation through the link above, I finally figured this out again. 
Two key points that I found helpful while understanding the solution: 

1. When a bar is popped, we calculate the area with the popped bar at index ``tp`` as shortest bar. Now we know the rectangle **height** is ``heights[tp]``, we just need rectangle width to calculate the area.
2. How to determine rectangle **width**? The maximum width we can have here would be made of all connecting bars with height greater than or equal to ``heights[tp]``. ``heights[s.peek() + 1] >= heights[tp]`` because the index on top of the stack right now ``s.peek()`` is the first index left of ``tp`` with height smaller than tp\'s height (if ``s.peek()`` was greater then it should have already been poped out of the stack). ``heights[i - 1] >= heights[tp]`` because index ``i`` is the first index right of ``tp`` with height smaller than tp\'s height (if ``i`` was greater then ``tp`` would have remained on the stack). Now we multiply height ``heights[tp]`` by width ``i - 1 - s.peek()`` to get the **area**.


</p>


