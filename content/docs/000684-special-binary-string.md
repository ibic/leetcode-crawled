---
title: "Special Binary String"
weight: 684
#id: "special-binary-string"
---
## Description
<div class="description">
<p>
<i>Special</i> binary strings are binary strings with the following two properties:
</p><p>
<li>The number of 0's is equal to the number of 1's.</li>
<li>Every prefix of the binary string has at least as many 1's as 0's.</li>
</p><p>
Given a special string <code>S</code>, a <i>move</i> consists of choosing two consecutive, non-empty, special substrings of <code>S</code>, and swapping them.  <i>(Two strings are consecutive if the last character of the first string is exactly one index before the first character of the second string.)</i>
</p><p>
At the end of any number of moves, what is the lexicographically largest resulting string possible?
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> S = "11011000"
<b>Output:</b> "11100100"
<b>Explanation:</b>
The strings "10" [occuring at S[1]] and "1100" [at S[3]] are swapped.
This is the lexicographically largest string possible after some number of swaps.
</pre>
</p>

<p><b>Note:</b><ol>
<li><code>S</code> has length at most <code>50</code>.</li>
<li><code>S</code> is guaranteed to be a <i>special</i> binary string as defined above.</li>
</ol></p>
</div>

## Tags
- String (string)
- Recursion (recursion)

## Companies
- Citrix - 4 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- Coursera - 2 (taggedByAdmin: true)
- Quip (Salesforce) - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Recursion [Accepted]

**Intuition**

We can represent binary strings as "up and down" drawings, as follows:

<img src="../Figures/761/Special_Binary_String_1.png" width=80% height=80% />

In such a drawing, `"1"` is a line up one unit, and `"0"` is a line down one unit, where all lines are 45 degrees from horizontal.

Then, a binary string is *special* if and only if its up and down drawing does not cross below the starting horizontal line.

Now, say a special binary string is a *mountain* if it has no special proper prefix.  When viewed through the lens of up and down drawings, a special binary string is a mountain if it touches the starting horizontal line only at the very beginning and the very end of the drawing.  Notice that every special string can be written as consecutive mountains.

Without loss of generality, we can assume we only swap mountains.  Because if we swap special adjacent substrings A and B, and A has mountain decomposition $$A = M_1M_2\dots M_k$$, then we could instead swap $$B$$ and $$M_k$$, then $$B$$ and $$M_{k-1}$$, and so on.

Also, if $$S$$ has mountain decomposition $$S = M_1M_2\dots M_k$$, and we choose $$A$$ to start not at the start of some $$M_i$$, then $$A$$ has global height $$h > 0$$, and so $$A$$ cannot be special if it includes parts of another mountain $$M_{i+1}$$ as the end of mountain $$M_i$$ will cause it to dip to global height $$0 < h$$.

**Algorithm**

Say `F(String S)` is the desired `makeLargestSpecial` function.  If `S` has mountain decomposition $$S = M_1M_2\dots M_k$$, the answer is $$\text{reverse_sorted}(F(M_1), F(M_2), \dots, F(M_k))$$, as swaps `A, B` involving multiple $$M_i$$ cannot have `A` or `B` start differently from where these $$M_i$$ start.

It remains to determine how to handle the case when $$S = S_0, S_1, \dots , S_{N-1}$$ is a mountain.  In that case, it must start with `"1"` and end with `"0"`, so the answer is `"1" + F([S[1], S[2], ..., S[N-2]]) + "0"`.

<iframe src="https://leetcode.com/playground/9sXYARuN/shared" frameBorder="0" width="100%" height="378" name="9sXYARuN"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2)$$, where $$N$$ is the length of $$S$$.

* Space Complexity: $$O(N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Easy and Concise Recursion
- Author: lee215
- Creation Date: Sun Jan 07 2018 12:12:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 10:06:10 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**:

Just 4 steps:
1. Split S into several special strings (as many as possible).
2. Special string starts with 1 and ends with 0. Recursion on the middle part.
3. Sort all special strings in lexicographically largest order.
4. Join and output all strings.

## **Update**

The middle part of a special string may not be another special string. But in my recursion it is.
For example, 1M0 is a splitted special string. M is its middle part and it must be another special string.

Because:
1. The number of 0\'s is equal to the number of 1\'s in ```M```
2. If there is a prefix ```P``` of ```M```which has one less 1\'s than 0\'s, ```1P``` will make up a special string. ```1P``` will be found as special string before ```1M0``` in my solution.
It means that every prefix of ```M``` has at least as many 1\'s as 0\'s.

Based on 2 points above, M is a special string.

## **Solution**

**Java**
```
public String makeLargestSpecial(String S) {
        int count = 0, i = 0;
        List<String> res = new ArrayList<String>();
        for (int j = 0; j < S.length(); ++j) {
          if (S.charAt(j) == \'1\') count++;
          else count--;
          if (count == 0) {
            res.add(\'1\' + makeLargestSpecial(S.substring(i + 1, j)) + \'0\');
            i = j + 1;
          }
        }
        Collections.sort(res, Collections.reverseOrder());
        return String.join("", res);
    }
```

**C++**
```
string makeLargestSpecial(string S) {
    int count = 0, i = 0;
    vector<string> res;
    for (int j = 0; j < S.size(); ++j) {
      if (S[j] == \'1\') count++;
      else count--;
      if (count == 0) {
        res.push_back(\'1\' + makeLargestSpecial(S.substr(i + 1, j - i - 1)) + \'0\');
        i = j + 1;
      }
    }
    sort(res.begin(), res.end(), greater<string>());
    string res2 = "";
    for (int i = 0; i < res.size(); ++i) res2 += res[i];
    return res2;
  }
```

**Python**
```
def makeLargestSpecial(self, S):
        count = i = 0
        res = []
        for j, v in enumerate(S):
            count = count + 1 if v==\'1\' else count - 1
            if count == 0:
                res.append(\'1\' + self.makeLargestSpecial(S[i + 1:j]) + \'0\')
                i = j + 1
        return \'\'.join(sorted(res)[::-1])
```

Upvote and enjoy.

</p>


### Think of it as Valid-Parentheses
- Author: alexander
- Creation Date: Mon Jan 08 2018 09:33:52 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 22:37:20 GMT+0800 (Singapore Standard Time)

<p>
According to the [description ](https://leetcode.com/contest/weekly-contest-66/problems/special-binary-string/), there are 2 requirement for `Special-String`
1. The number of 0's is equal to the number of 1's.
2. Every prefix of the binary string has at least as many 1's as 0's.

The 2nd definition is essentially saying that at any point of the string, you cannot have more 0's than 1's.

If we map `'1'` to `'('`, `'0'`s to `')'`, a `Special-String` is essentially `Valid-Parentheses`, therefore share all the properties of a `Valid-Parentheses`
A `VP` (`Valid-Parentheses`) have 2 form:
1. single nested `VP` like `"(())"`, or `"1100"`;
2. a number of consecutive `sub-VP`s like `"()(())"`, or `"101100"`, which contains `"()" + "(())"` or `"10" + "1100"`

And this problem is essentially ask you to reorder the `sub-VP`s in a `VP` to make it bigger. If we look at this example : `"()(())"` or `"101100"`, how would you make it bigger? 
Answer is, by moving the 2nd sub-string to the front. Because deeply nested VP contains more consecutive `'('`s or `'1'`s in the front. That will make reordered string bigger.

The above example is straitforward, and no recursion is needed. But, what if the groups of `sub-VP`s are not in the root level? 
Like if we put another `"()(())"` inside `"()(())"`, like `"()((` `()(())` `))"`, in this case we will need to recursively reorder the children, make them `MVP`(`Max-Valid-Parentheses`), then reorder in root.

To summarize, we just need to reorder all groups of `VP`s or `SS`'s at each level to make them `MVP`, then reorder higher level `VP`s;

```
class Solution {
public:
    string makeLargestSpecial(string s) {
        int i = 0;
        return dfs(s, i);
    }

private:
    string dfs(string& s, int& i) {
        string res;
        vector<string> toks;
        while (i < s.size() && res.empty()) {
            if (s[i++] == '1') toks.push_back(dfs(s, i));
            else res += "1";
        }
        bool prefix = res.size();
        sort(toks.begin(), toks.end());
        for (auto it = toks.rbegin(); it != toks.rend(); it++) res += *it;
        if (prefix) res += '0';
        return res;
    }
};
```
</p>


### Logical Thinking with Clear Code
- Author: GraceMeng
- Creation Date: Wed Aug 01 2018 15:38:26 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 04:00:34 GMT+0800 (Singapore Standard Time)

<p>
Thanks for the original posts.

**Logical Thinking**
If we regard 1, 0 in the definition of the special string as \'(\' and \')\' separately,
the problem is actually to get the string which is so-called **valid parenthesis** and meanwhile **is the lexicographically largest**.
It is intuitive that we prefer **deeper** valid parenthesis to sit in front (**deeper** means the string surrounded with more pairs of parenthesis, e.g., \'(())\' is deeper than \'()\' ). We can achieve that by **sorting them reversely**. 
we go through `S`. Whenever the parentheses we met can be balanced (i.e., `balance == 0`), we construct valid parentheses -- by putting `( `on the left boundary, `)`on the right boundary, and doing with the inner part following the same pattern.


**Clear Java Code**
```
    public String makeLargestSpecial(String S) {
        
        int balance = 0, l = 0;
        List<String> subResults = new LinkedList<>();
        for (int r = 0; r < S.length(); r++) {
            if (S.charAt(r) == \'0\') {
                balance--;
            }
            else {
                balance++;
            }
            if (balance == 0) {
                subResults.add("1" + makeLargestSpecial(S.substring(l + 1, r)) + "0");
                l = r + 1;
            }
        }
        Collections.sort(subResults, Collections.reverseOrder());
        
        return String.join("", subResults);
    }
```
**I would appreciate your VOTE UP ;)**
</p>


