---
title: "Print Immutable Linked List in Reverse"
weight: 1213
#id: "print-immutable-linked-list-in-reverse"
---
## Description
<div class="description">
<p>You are given an immutable linked list, print out all values of each node in reverse with the help of the following&nbsp;interface:</p>

<ul>
	<li><code>ImmutableListNode</code>:&nbsp;An interface of immutable linked list, you are given the head of the list.</li>
</ul>

<p>You need to use the following functions to access the linked list (you <strong>can&#39;t</strong> access the <code>ImmutableListNode</code> directly):</p>

<ul>
	<li><code>ImmutableListNode.printValue()</code>: Print value of the current node.</li>
	<li><code>ImmutableListNode.getNext()</code>: Return the next node.</li>
</ul>

<p>The input is only given to initialize the linked list internally.&nbsp;You must solve this problem without modifying the linked list. In other words, you must operate&nbsp;the linked list using only the mentioned&nbsp;APIs.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> head = [1,2,3,4]
<strong>Output:</strong> [4,3,2,1]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> head = [0,-4,-1,3,-5]
<strong>Output:</strong> [-5,3,-1,-4,0]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> head = [-2,0,6,4,4,-6]
<strong>Output:</strong> [-6,4,4,6,0,-2]
</pre>

<ul>
</ul>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The length of the linked list&nbsp;is between <code>[1, 1000]</code>.</li>
	<li>The value of each&nbsp;node in the linked list&nbsp;is between <code>[-1000, 1000]</code>.</li>
</ul>

<p>&nbsp;</p>

<p><strong>Follow up:</strong></p>

<p>Could you solve this problem in:</p>

<ul>
	<li>Constant space complexity?</li>
	<li>Linear time complexity and less than linear space complexity?</li>
</ul>

</div>

## Tags


## Companies
- Facebook - 2 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Jave Solution including Follow Up
- Author: cappuccinuo
- Creation Date: Mon Nov 25 2019 00:14:02 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 25 2019 00:14:02 GMT+0800 (Singapore Standard Time)

<p>
Iterative - Recursive
```Java
class Solution {
    public void printLinkedListInReverse(ImmutableListNode head) {
        if (head == null) {
            return;
        }
        printLinkedListInReverse(head.getNext());
        head.printValue();
    }
}
```

Iterative - With Stack
```Java
class Solution {
    public void printLinkedListInReverse(ImmutableListNode head) {
        Stack<ImmutableListNode> stack = new Stack<>();
        while (head != null) {
            stack.push(head);
            head = head.getNext();
        }
        while (!stack.isEmpty()) {
            stack.pop().printValue();
        }
    }
}
```

Follow Up - Constant space complexity (Time: O(n^2))
```Java
class Solution {
    public void printLinkedListInReverse(ImmutableListNode head) {
        int numNodesCount = getNumNodesCount(head);
        for (int i = numNodesCount; i >= 1; i--) {
            printNthNode(head, i);
        }
    }
    
    private void printNthNode(ImmutableListNode head, int index) {
        ImmutableListNode node = head;
        for (int i = 0; i < index - 1; i++) {
            node = node.getNext();
        }
        node.printValue();
    }
    
    private int getNumNodesCount(ImmutableListNode head) {
        int count = 0;
        ImmutableListNode node = head;
        while (node != null) {
            count++;
            node = node.getNext();
        }
        return count;
    }
}
```

Follow Up - Linear time complexity and less than linear space complexity
Idea - Split the list to sqrt(n) equal-size small list. Print each part in reverse order.
```Java
class Solution {
    public void printLinkedListInReverse(ImmutableListNode head) {
        // Time: O(n)
        int numNodesCount = getNumNodesCount(head);
        
        // Time: O(n) Space: O(sqrt(n))
        int step = (int)Math.sqrt(numNodesCount) + 1;
        Stack<ImmutableListNode> headNodes = new Stack<>();
        addNodeWithStep(head, step, headNodes);
        
        // Time: O(n) Space: O(sqrt(n))
        printEachHeadNodesInReverseOrder(headNodes);
    }
    
    private int getNumNodesCount(ImmutableListNode head) {
        int count = 0;
        ImmutableListNode node = head;
        while (node != null) {
            count++;
            node = node.getNext();
        }
        return count;
    }
    
    private void addNodeWithStep(ImmutableListNode head, int step, Stack<ImmutableListNode> headNodes) {
        ImmutableListNode node = head;
        int i = 0;
        while (node != null) {
            if (i % step == 0) {
                headNodes.push(node);
            }
            node = node.getNext();
            i++;
        }
    }
    
    private void printEachHeadNodesInReverseOrder(Stack<ImmutableListNode> headNodes) {
        ImmutableListNode startNode = null;
        ImmutableListNode endNode = null;
        ImmutableListNode tempNode = null;
        
        while (!headNodes.isEmpty()) {
            endNode = startNode;
            startNode = headNodes.pop();
            tempNode = startNode;
            
            Stack<ImmutableListNode> stack = new Stack<>();
            while (tempNode != endNode) {
                stack.push(tempNode);
                tempNode = tempNode.getNext();
            }
            
            while (!stack.isEmpty()) {
                stack.pop().printValue();
            }
        }
    }
}
```
</p>


### [Python] 4 solutions, space complexity O(n) O(n^(1/t)) O(lgn) O(1)
- Author: migeater
- Creation Date: Sat Dec 07 2019 03:01:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 26 2020 02:40:26 GMT+0800 (Singapore Standard Time)

<p>
Welcome to add more combinations of time and space complexity.

n is length of linked list.
|time        | space           | algorithm  | 
| ------------- |:-------------:| -----:|
 O(n)     |O(n)| [recursion or use stack](#recursion-or-use-stack)
 O(n)   | O(n^(1/t) + t)  for an arbitrary positive integer t |    [sqrt decomposition](#sqrt-decomposition)
O(n lg n)|O(lg n)      |   [divide and conquer](#divide-and-conquer)
O(n^2) |O(1)| [load a magazine?](#load-a-magazine)

## recursion or use stack
```py
class Solution:
    def printLinkedListInReverse(self, head: \'ImmutableListNode\') -> None:
        if head:
            self.printLinkedListInReverse(head.getNext());
            head.printValue();
```

## sqrt decomposition

1. Divide the linked list into n^(1/2) blocks of size n^(1/2); 
1. Then the start-nodes of these blocks are stored in a stack of length n^(1/2).
1. Finally, take blocks from the stack and *print it recursively* with time O(n^(1/2)) and space O(n^(1/2)).

The total time complexity is O(n) and space O(n^(1/2)).

```py
class Solution:
    def printLinkedListInReverseDirect(self, head, size):
        if size and head:
            self.printLinkedListInReverseDirect(head.getNext(),size-1)
            head.printValue()
    
    def printLinkedListInReverse(self, head: \'ImmutableListNode\') -> None:
        def getLinkedListSize(head):
            size=0
            while head!=None:
                size+=1
                head=head.getNext()
            return size
        
        size=getLinkedListSize(head)
        
        num_blocks = math.ceil(math.sqrt(size))
        block_size = math.ceil(size/num_blocks)
        
        blocks = [] # using List as Stack
        head_cpy,cur = head,0
        for cur in range(size):
            if cur%block_size==0:
                blocks.append(head_cpy)
            head_cpy=head_cpy.getNext()
        
        for i in range(num_blocks-1,0 -1,-1):
            self.printLinkedListInReverseDirect(blocks[i], block_size)
        
```

You can cost less space if you use such decomposition technique to print blocks, see [miracle173\'s answer](https://stackoverflow.com/a/41575182/7721525)

```py
class Solution:
    def printLinkedListInReverseDirect(self, head, size):
        if size :
            self.printLinkedListInReverseDirect(head.getNext(),size-1)
            head.printValue()
    def printLinkedListInReverse(self, head: \'ImmutableListNode\') -> None:
        def getLinkedListSize(head):
            size=0
            while head!=None:
                size+=1
                head=head.getNext()
            return size
        
        size=getLinkedListSize(head)
        self.printLinkedListInReverseDecomposition(head,size,t=3)
    def printLinkedListInReverseDecomposition(self, head, size, t):
        if size==1:
            head.printValue()
            return
        num_blocks = math.ceil(size**(1/t)) # at least two blocks.
        block_size = math.ceil(size/num_blocks)
        
        blocks = [] # using List as Stack
        
        head_cpy,cur = head,0
        
        for cur in range(size):
            if cur%block_size==0:
                blocks.append(head_cpy)
            head_cpy=head_cpy.getNext()
        
        for i in range(num_blocks-1,0 -1,-1):
            nxt_size = block_size 
            if i==num_blocks-1:
                nxt_size = cur%block_size+1
                
            if t==2:
                self.printLinkedListInReverseDirect(blocks[i], nxt_size)
            else:
                self.printLinkedListInReverseDecomposition(blocks[i], nxt_size,t-1)
                
```
## divide and conquer

Divide into two parts print the right part first using this algorithm again.

```py
class Solution:
    def printLinkedListInReverse(self, head: \'ImmutableListNode\') -> None:
        def getLinkedListSize(head):
            size=0
            while head!=None:
                size+=1
                head=head.getNext()
            return size
        
        def helper(head,n):
            if n>1:
                half=head
                for _ in range(n//2):
                    half=half.getNext()
                helper(half,n-n//2)
                helper(head,n//2)
            elif n!=0:
                head.printValue()

        size=getLinkedListSize(head)
        helper(head,size)
```

T(n)=2T(n/2)+n/2
use master theorem
$$ f(n)=\Theta(n^1log^0(n)) $$
$$ T(n)=\Theta(n^1log^1(n)) $$

## load a magazine?

```py
class Solution:
    def printLinkedListInReverse(self, head: \'ImmutableListNode\') -> None:
        tail=None
        while tail!=head:
            curr=head
            while curr.getNext()!=tail:
                curr=curr.getNext()
            tail=curr
            tail.printValue()
```

</p>


### Linear time with sub-linear space. O(n) time with O(sqrt(n)) space.
- Author: nullPtr13
- Creation Date: Sun Nov 24 2019 01:04:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 24 2019 01:04:29 GMT+0800 (Singapore Standard Time)

<p>
I belive this is O(n) time and O(sqrt(n)) space complexity. Idea is to break list into 2D matrix and use space for bookkeping for starting of rows and individual row context. 


```
class Solution {
    
    void printNodes(ImmutableListNode* node, int total )
    {
        vector<ImmutableListNode*> buffer;
        for( int i=0; i<total; ++i )
        {
            buffer.push_back(node);
            node = node->getNext();
            if( node == nullptr ) break;
        }
        
        while( !buffer.empty() )
        {
            buffer.back()->printValue();
            buffer.pop_back();
        }
    }
public:
    void printLinkedListInReverse(ImmutableListNode* head) {
        
        if( head == nullptr ) return;
        
        int count = 0;
        ImmutableListNode* current = head;
        while( current )
        {
            ++count;
            current = current->getNext();
        }
        
        int total = ceil(sqrt(count));
        
        vector<ImmutableListNode*> checkPoints;
        current = head;
        count = -1;
        while( current )
        {
            ++count;
            if( count % total == 0 )
            {
                checkPoints.push_back(current);
            }
            current = current->getNext();
        }
        
        while( !checkPoints.empty() )
        {
            ImmutableListNode* node = checkPoints.back();
            printNodes(node, total);
            checkPoints.pop_back();
        }
    }
};
```
</p>


