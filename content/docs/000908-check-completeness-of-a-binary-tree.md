---
title: "Check Completeness of a Binary Tree"
weight: 908
#id: "check-completeness-of-a-binary-tree"
---
## Description
<div class="description">
<p>Given a binary tree, determine if it is a <em>complete binary tree</em>.</p>

<p><u><b>Definition of a complete binary tree from <a href="http://en.wikipedia.org/wiki/Binary_tree#Types_of_binary_trees" target="_blank">Wikipedia</a>:</b></u><br />
In a complete binary tree every level, except possibly the last, is completely filled, and all nodes in the last level are as far left as possible. It can have between 1 and 2<sup>h</sup> nodes inclusive at the last level h.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2018/12/15/complete-binary-tree-1.png" style="width: 180px; height: 145px;" /></strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,2,3,4,5,6]</span>
<strong>Output: </strong><span id="example-output-1">true</span>
<span><strong>Explanation: </strong></span>Every level before the last is full (ie. levels with node-values {1} and {2, 3}), and all nodes in the last level ({4, 5, 6}) are as far left as possible.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2018/12/15/complete-binary-tree-2.png" style="width: 200px; height: 145px;" /></strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[1,2,3,4,5,null,7]</span>
<strong>Output: </strong><span id="example-output-2">false</span>
<strong>Explanation: </strong>The node with value 7 isn&#39;t as far left as possible.<span>
</span></pre>

<div>&nbsp;</div>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li>The tree will have between 1 and 100 nodes.</li>
</ol>

</div>

## Tags
- Tree (tree)

## Companies
- Facebook - 11 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Breadth First Search

**Intuition**

This problem reduces to two smaller problems: representing the "location" of each node as a `(depth, position)` pair, and formalizing what it means for nodes to all be left-justified.

If we have say, 4 nodes in a row with depth 3 and positions 0, 1, 2, 3; and we want 8 new nodes in a row with depth 4 and positions 0, 1, 2, 3, 4, 5, 6, 7; then we can see that the rule for going from a node to its left child is `(depth, position) -> (depth + 1, position * 2)`, and the rule for going from a node to its right child is `(depth, position) -> (depth + 1, position * 2 + 1)`.  Then, our row at depth $$d$$ is completely filled if it has $$2^{d-1}$$ nodes, and all the nodes in the last level are left-justified when their positions take the form `0, 1, ...` in sequence with no gaps.

A cleaner way to represent depth and position is with a code: `1` will be the root node, and for any node with code `v`, the left child will be `2*v` and the right child will be `2*v + 1`.  This is the scheme we will use.  Under this scheme, our tree is complete if the codes take the form `1, 2, 3, ...` in sequence with no gaps.

**Algorithm**

At the root node, we will associate it with the code `1`.  Then, for each node with code `v`, we will associate its left child with code `2 * v`, and its right child with code `2 * v + 1`.

We can find the codes of every node in the tree in "reading order" (top to bottom, left to right) sequence using a breadth first search.  (We could also use a depth first search and sort the codes later.)

Then, we check that the codes are the sequence `1, 2, 3, ...` with no gaps.  Actually, we only need to check that the last code is correct, since the last code is the largest value.


<iframe src="https://leetcode.com/playground/WyDMreFf/shared" frameBorder="0" width="100%" height="480" name="WyDMreFf"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the number of nodes in the tree.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] BFS Level Order Traversal
- Author: lee215
- Creation Date: Sun Dec 16 2018 12:06:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 16 2018 12:06:17 GMT+0800 (Singapore Standard Time)

<p>
Use BFS to do a level order traversal,
add childrens to the bfs queue,
until we met the first empty node.

For a complete binary tree,
there should not be any node after we met an empty one.


**Java:**
```
    public boolean isCompleteTree(TreeNode root) {
        Queue<TreeNode> bfs = new LinkedList<TreeNode>();
        bfs.offer(root);
        while (bfs.peek() != null) {
            TreeNode node = bfs.poll();
            bfs.offer(node.left);
            bfs.offer(node.right);
        }
        while (!bfs.isEmpty() && bfs.peek() == null)
            bfs.poll();
        return bfs.isEmpty();
    }
```

**C++:**
```
    bool isCompleteTree(TreeNode* root) {
        vector<TreeNode*> bfs;
        bfs.push_back(root);
        int i = 0;
        while (i < bfs.size() && bfs[i]) {
            bfs.push_back(bfs[i]->left);
            bfs.push_back(bfs[i]->right);
            i++;
        }
        while (i < bfs.size() && !bfs[i])
            i++;
        return i == bfs.size();
    }
```

**Python:**
```
    def isCompleteTree(self, root):
        bfs = [root]
        i = 0
        while bfs[i]:
            bfs.append(bfs[i].left)
            bfs.append(bfs[i].right)
            i += 1
        return not any(bfs[i:])
```


Also you may want to return earlier.
We can stop the first while loop when met the first null child.
From then on there should not be any more child.
This optimisation help reduce half of operations.


```
    public boolean isCompleteTree(TreeNode root) {
        Queue<TreeNode> bfs = new LinkedList<TreeNode>();
        bfs.offer(root);
        while (true) {
            TreeNode node = bfs.poll();
            if (node.left == null) {
                if (node.right != null)
                    return false;
                break;
            }
            bfs.offer(node.left);
            if (node.right == null) break;
            bfs.offer(node.right);
        }
        while (!bfs.isEmpty()) {
            TreeNode node = bfs.poll();
            if (node.left != null || node.right != null)
                return false;
        }
        return true;
    }
```
</p>


### Java easy Level-Order Traversal, one while-loop
- Author: motorix
- Creation Date: Sun Dec 16 2018 14:12:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 16 2018 14:12:58 GMT+0800 (Singapore Standard Time)

<p>
When level-order traversal in a complete tree, after the last node, all nodes in the queue should be null. 
Otherwise, the tree is not complete.
```
    public boolean isCompleteTree(TreeNode root) {
        boolean end = false;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while(!queue.isEmpty()) {
            TreeNode cur = queue.poll();
            if(cur == null) end = true;
            else{
                if(end) return false;
                queue.add(cur.left);
                queue.add(cur.right);
            }
        }
        return true;
    }
```
Time Complexity: O(N), where N is the number of nodes.
Space Complexity: O(N)
</p>


### Python solution
- Author: ZitaoWang
- Creation Date: Sat Feb 23 2019 12:13:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Feb 23 2019 12:13:57 GMT+0800 (Singapore Standard Time)

<p>
DFS. Time complexity: `O(n)`, space complexity: `O(h)`.

```
class Solution(object):
    def isCompleteTree(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        # number of nodes, right_most_coords
        def dfs(root, coord):
            if not root:
                return 0, 0
            l = dfs(root.left, 2*coord)
            r = dfs(root.right, 2*coord+1)
            tot = l[0]+r[0]+1
            right_most = max(coord, l[1], r[1])
            return tot, right_most
        if not root:
            return True
        tot, right_most = dfs(root, 1)
        return tot == right_most
```

BFS. Time complexity: `O(n)`, space complexity: `O(n)`.

```
class Solution(object):
    def isCompleteTree(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        if not root:
            return True
        q = collections.deque([(root, 1)])
        res = []
        while q:
            u, coord = q.popleft()
            res.append(coord)
            if u.left:
                q.append((u.left, 2*coord))
            if u.right:
                q.append((u.right, 2*coord+1))
        return len(res) == res[-1]
```
</p>


