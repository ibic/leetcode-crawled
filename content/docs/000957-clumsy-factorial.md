---
title: "Clumsy Factorial"
weight: 957
#id: "clumsy-factorial"
---
## Description
<div class="description">
<p>Normally, the factorial of a positive integer <code>n</code>&nbsp;is the product of all positive integers less than or equal to <code>n</code>.&nbsp; For example, <code>factorial(10) = 10 * 9 * 8 * 7 * 6 * 5 * 4 * 3 * 2 * 1</code>.</p>

<p>We instead make a <em>clumsy factorial:</em>&nbsp;using the integers in decreasing order, we&nbsp;swap out the multiply operations for a fixed rotation of operations:&nbsp;multiply (*), divide (/), add (+) and subtract (-) in this order.</p>

<p>For example, <code>clumsy(10) = 10 * 9 / 8 + 7 - 6 * 5 / 4 + 3 - 2 * 1</code>.&nbsp; However, these operations are still applied using the usual order of operations of arithmetic: we do all multiplication and division steps before any addition or subtraction steps, and multiplication and division steps are processed left to right.</p>

<p>Additionally, the division that we use is <em>floor division</em>&nbsp;such that&nbsp;<code>10 * 9 / 8</code>&nbsp;equals&nbsp;<code>11</code>.&nbsp; This guarantees the result is&nbsp;an integer.</p>

<p><code><font face="sans-serif, Arial, Verdana, Trebuchet MS">Implement the&nbsp;</font>clumsy</code>&nbsp;function&nbsp;as defined above: given an integer <code>N</code>, it returns the clumsy factorial of <code>N</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>4
<strong>Output:</strong>&nbsp;7
<strong>Explanation:</strong> 7 = 4 * 3 / 2 + 1
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">10
</span><strong>Output: </strong><span id="example-output-1">12
</span><strong>Explanation: </strong>12 = 10 * 9 / 8 + 7 - 6 * 5 / 4 + 3 - 2 * 1
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= N &lt;= 10000</code></li>
	<li><code>-2^31 &lt;= answer &lt;= 2^31 - 1</code>&nbsp; (The answer is guaranteed to fit within a 32-bit integer.)</li>
</ol>

</div>

## Tags
- Math (math)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### You never think of this, amazing O(1) solution
- Author: Lisanaaa
- Creation Date: Sun Mar 10 2019 12:12:11 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 09 2019 20:52:37 GMT+0800 (Singapore Standard Time)

<p>

As defined in the description of problem, `Additionally, the division that we use is floor division such that 10 * 9 / 8 equals 11. `

We can easily observe below:

```
5 * 4 / 3 = 6
6 * 5 / 4 = 7
10 * 9 / 8 = 11
...
...
...

```

so we can get this formula: `i * (i-1) / (i-2) = i+1` when `i >= 5`


we can simplify our computation as below:

```
    i * (i-1) / (i-2) + (i-3) - (i-4) * (i-5) / (i-6) + (i-7) - (i-8) * .... + rest elements
=   (i+1) + "(i-3)" - "(i-4) * (i-5) / (i-6)" + "(i-7)" - "(i-8) * " .... + rest elements
=   (i+1) + "(i-3) - (i-3)" + "(i-7) - (i-7)" +  ....  + rest elements
=   (i+1) + rest elements
```

we can call each 4 numbers a `chunk`, so from `N // 4` we can know how many chunks there are, then the rest `0`, `1`, `2` and `3` elements will influence our final result.


1. when `0` element left: final result is `(i+1) + ... + 5 - (4*3/2) + 1`, which is `i+1`
2. when `1` element left: final result is `(i+1) + ... + 6 - (5*4/3) + 2 - 1 `, which is `i+2`
3. when `2` element left: final result is `(i+1) + ... + 7 - (6*5/4) + 3 - 2 * 1 `, which is `i+2`
3. when `3` element left: final result is `(i+1) + ... + 8 - (7*6/5) + 4 - 3 * 2 / 1 `, which is `i-1`

After consider the corner case, we can arrive at the solution:

```
class Solution:
    def clumsy(self, N: int) -> int:
        if N <= 2:
            return N
        if N <= 4:
            return N + 3
        
        if (N - 4) % 4 == 0:
            return N + 1
        elif (N - 4) % 4 <= 2:
            return N + 2
        else:
            return N - 1
```

Simple implementation, credit to `@motorix`

```python
class Solution:
    def clumsy(self, N: int) -> int:
        magic = [1, 2, 2, -1, 0, 0, 3, 3]
        return N + (magic[N % 4] if N > 4 else magic[N + 3])
```

Time complexity: O(1)
Space complexity: O(1)



</p>


### [C++/Java] Brute Force
- Author: lee215
- Creation Date: Sun Mar 10 2019 12:05:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 10 2019 12:05:02 GMT+0800 (Singapore Standard Time)

<p>
## Solution 1, Brute Force

**Java:**
```
    public int clumsy(int N) {
        if (N == 0) return 0;
        if (N == 1) return 1;
        if (N == 2) return 2;
        if (N == 3) return 6;
        return N * (N - 1) / (N - 2) + helper(N - 3);
    }
    public int helper(int N) {
        if (N == 0) return 0;
        if (N == 1) return 1;
        if (N == 2) return 1;
        if (N == 3) return 1;
        return N - (N - 1) * (N - 2) / (N - 3) + helper(N - 4);
    }
```

<br>

## Solution 2, Improve to O(1)

`N * N - 3 * N = N * N - 3 * N + 2 - 2`
`N * (N - 3) = (N - 1) * (N - 2) - 2`\u3002\uFF08factorization\uFF09
`N = (N - 1) * (N - 2) / (N - 3) - 2 / (N - 3)` \uFF08Divide `N - 3` on both side\uFF09
`N - (N - 1) * (N - 2) / (N - 3) = - 2 / (N - 3)`
`- 2 / (N - 3) = 0`, If `N - 3 > 2`.


So when `N > 5`, `N - (N - 1) * (N - 2) / (N - 3) = 0`

Now it\'s `O(1)`

```
    public int clumsy(int N) {
        if (N == 0) return 0;
        if (N == 1) return 1;
        if (N == 2) return 2;
        if (N == 3) return 6;
        return N * (N - 1) / (N - 2) + helper(N - 3);
    }
    public int helper(int N) {
        if (N == 0) return 0;
        if (N == 1) return 1;
        if (N == 2) return 1;
        if (N == 3) return 1;
        if (N == 4) return -2;
        if (N == 5) return 0;
        return helper((N - 2) % 4 + 2);
    }
```

<br>

# Solution 3, Better Format

Now make a summary.
N = 0, return 0
N = 1, return 1
N = 2, return 2
N = 3, return 6
N = 4, return 7
N = 5 + 4K, return N + 2
N = 6 + 4K, return N + 2
N = 7 + 4K, return N - 1
N = 8 + 4K, return N + 1


**Java**
```
    public int clumsy(int N) {
        if (N == 1) return 1;
        if (N == 2) return 2;
        if (N == 3) return 6;
        if (N == 4) return 7;
        if (N % 4 == 1) return N + 2;
        if (N % 4 == 2) return N + 2;
        if (N % 4 == 3) return N - 1;
        return N + 1;
    }
```

**C++**
```
    int clumsy(int N) {
        if (N == 1) return 1;
        if (N == 2) return 2;
        if (N == 3) return 6;
        if (N == 4) return 7;
        if (N % 4 == 1) return N + 2;
        if (N % 4 == 2) return N + 2;
        if (N % 4 == 3) return N - 1;
        return N + 1;
    }
```
</p>


### [Python] 1-line Cheat
- Author: lee215
- Creation Date: Sun Mar 10 2019 12:05:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 10 2019 12:05:18 GMT+0800 (Singapore Standard Time)

<p>
```
    def clumsy(self, N):
        op = itertools.cycle("*/+-")
        return eval("".join(str(x) + next(op) for x in range(N, 0, -1))[:-1])
```

`O(1)` version
```
    def clumsy(self, N):
        return [0, 1, 2, 6, 7][N] if N < 5 else N + [1, 2, 2, - 1][N % 4]
```
</p>


