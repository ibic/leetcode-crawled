---
title: "Hexspeak"
weight: 1086
#id: "hexspeak"
---
## Description
<div class="description">
<p>A decimal number can be converted to its&nbsp;<em>Hexspeak representation</em>&nbsp;by first converting it to an uppercase hexadecimal string, then replacing all occurrences of the digit <code>0</code> with the letter <code>O</code>, and the digit <code>1</code> with the letter <code>I</code>.&nbsp; Such a representation&nbsp;is <em>valid</em>&nbsp;if and only if it consists only of the letters in the set <code>{&quot;A&quot;, &quot;B&quot;, &quot;C&quot;, &quot;D&quot;, &quot;E&quot;, &quot;F&quot;, &quot;I&quot;, &quot;O&quot;}</code>.</p>

<p>Given a string <code>num</code>&nbsp;representing a decimal integer <code>N</code>, return the Hexspeak representation of <code>N</code> if it is valid, otherwise return <code>&quot;ERROR&quot;</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> num = &quot;257&quot;
<strong>Output:</strong> &quot;IOI&quot;
<b>Explanation: </b> 257 is 101 in hexadecimal.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> num = &quot;3&quot;
<strong>Output:</strong> &quot;ERROR&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= N &lt;= 10^12</code></li>
	<li>There are no leading zeros in the given string.</li>
	<li>All answers must be in uppercase letters.</li>
</ul>

</div>

## Tags
- Math (math)
- String (string)

## Companies
- Virtu Financial - 4 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ String Stream
- Author: votrubac
- Creation Date: Sun Dec 01 2019 06:00:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 01 2019 06:00:25 GMT+0800 (Singapore Standard Time)

<p>
```
string toHexspeak(string num) {
    auto n = stol(num);
    stringstream st;
    st << hex << uppercase  << n;
    string s(st.str());
    for (auto i = 0; i < s.size(); ++i) {
        if (s[i] > \'1\' && s[i] <= \'9\') return "ERROR";
        if (s[i] == \'0\') s[i] = \'O\';
        if (s[i] == \'1\') s[i] = \'I\';
    }
    return s;
}
```
</p>


### [Java] Simple & Concise Solution
- Author: manrajsingh007
- Creation Date: Sun Dec 01 2019 00:02:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 01 2019 00:14:34 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public String toHexspeak(String num) {
        
        long n = Long.parseLong(num);
        HashMap<Integer, Character> map = new HashMap<>();
		map.put(0, \'O\');
		map.put(1, \'I\');
        map.put(10, \'A\');
        map.put(11, \'B\');
        map.put(12, \'C\');
        map.put(13, \'D\');
        map.put(14, \'E\');
        map.put(15, \'F\');
        
        String ans = "";
        
        while(n > 0){
            int rem = (int)(n % 16);
            if(rem > 1 && rem < 10) return "ERROR";
            n = n / 16;
			ans = map.get(rem) + ans;
        }
        
        return ans;
        
    }
}
</p>


### [Java/Python 3] 6 and 2 liners w/ analysis.
- Author: rock
- Creation Date: Sun Dec 01 2019 00:04:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 02 2019 19:50:20 GMT+0800 (Singapore Standard Time)

<p>
**Java**
```java
    public String toHexspeak(String num) {
        long n = Long.parseLong(num);
        String res = Long.toHexString(n).toUpperCase().replace(\'1\', \'I\').replace(\'0\', \'O\');
        for (char c : res.toCharArray())
            if (Character.isDigit(c))
                return "ERROR";
        return res;        
    }
```
**Python 3: credit to @dylan20**
```python
    def toHexspeak(self, num: str) -> str:
        # s = hex(int(num)).upper()[2 :].replace(\'0\', \'O\').replace(\'1\', \'I\')
        s = hex(int(num)).upper()[2 :].translate(str.maketrans(\'01\', \'OI\'))
        return \'ERROR\' if any(c.isdigit() for c in s) else s
```
**Analysis**
Time & space: `O(num.length())`
</p>


