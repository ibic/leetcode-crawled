---
title: "Minimum Area Rectangle"
weight: 889
#id: "minimum-area-rectangle"
---
## Description
<div class="description">
<p>Given a set of points in the xy-plane, determine the minimum area of a rectangle formed from these points, with sides parallel to the x and y axes.</p>

<p>If there isn&#39;t any rectangle, return 0.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[1,1],[1,3],[3,1],[3,3],[2,2]]</span>
<strong>Output: </strong><span id="example-output-1">4</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[[1,1],[1,3],[3,1],[3,3],[4,1],[4,3]]</span>
<strong>Output: </strong><span id="example-output-2">2</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note</strong>:</p>

<ol>
	<li><code>1 &lt;= points.length &lt;= 500</code></li>
	<li><code>0 &lt;=&nbsp;points[i][0] &lt;=&nbsp;40000</code></li>
	<li><code>0 &lt;=&nbsp;points[i][1] &lt;=&nbsp;40000</code></li>
	<li>All points are distinct.</li>
</ol>
</div>
</div>
</div>

## Tags
- Hash Table (hash-table)

## Companies
- Facebook - 6 (taggedByAdmin: false)
- Goldman Sachs - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Amazon - 4 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Nvidia - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Sort by Column

**Intuition**

Count each rectangle by right-most edge.

**Algorithm**

Group the points by `x` coordinates, so that we have columns of points.  Then, for every pair of points in a column (with coordinates `(x,y1)` and `(x,y2)`), check for the smallest rectangle with this pair of points as the rightmost edge.  We can do this by keeping memory of what pairs of points we've seen before.

<iframe src="https://leetcode.com/playground/ukzEcKTt/shared" frameBorder="0" width="100%" height="497" name="ukzEcKTt"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the length of `points`.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Count by Diagonal

**Intuition**

For each pair of points in the array, consider them to be the long diagonal of a potential rectangle.  We can check if all 4 points are there using a Set.

For example, if the points are `(1, 1)` and `(5, 5)`, we check if we also have `(1, 5)` and `(5, 1)`.  If we do, we have a candidate rectangle.

**Algorithm**

Put all the points in a set.  For each pair of points, if the associated rectangle are 4 distinct points all in the set, then take the area of this rectangle as a candidate answer.

<iframe src="https://leetcode.com/playground/9RmP7y7w/shared" frameBorder="0" width="100%" height="412" name="9RmP7y7w"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the length of `points`.

* Space Complexity:  $$O(N)$$, where $$H$$ is the height of the tree.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java N^2 Hashmap
- Author: wangzi6147
- Creation Date: Sun Nov 11 2018 12:03:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 11 2018 12:03:04 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int minAreaRect(int[][] points) {
        Map<Integer, Set<Integer>> map = new HashMap<>();
        for (int[] p : points) {
            if (!map.containsKey(p[0])) {
                map.put(p[0], new HashSet<>());
            }
            map.get(p[0]).add(p[1]);
        }
        int min = Integer.MAX_VALUE;
        for (int[] p1 : points) {
            for (int[] p2 : points) {
                if (p1[0] == p2[0] || p1[1] == p2[1]) { // if have the same x or y
                    continue;
                }
                if (map.get(p1[0]).contains(p2[1]) && map.get(p2[0]).contains(p1[1])) { // find other two points
                    min = Math.min(min, Math.abs(p1[0] - p2[0]) * Math.abs(p1[1] - p2[1]));
                }
            }
        }
        return min == Integer.MAX_VALUE ? 0 : min;
    }
}
```
</p>


### [Python] O(N^1.5), 80ms
- Author: lee215
- Creation Date: Sun Nov 11 2018 12:02:30 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 17 2019 22:19:49 GMT+0800 (Singapore Standard Time)

<p>
## Solution 1, Brute force
`N = 500` is really loose.
Just check all pairs of points.
Time `O(N^2)` , 1000ms ~ 1200ms

**Python**
```python
    def minAreaRect(self, points):
        seen = set()
        res = float(\'inf\')
        for x1, y1 in points:
            for x2, y2 in seen:
                if (x1, y2) in seen and (x2, y1) in seen:
                    area = abs(x1 - x2) * abs(y1 - y2)
                    if area and area < res:
                        res = area
            seen.add((x1, y1))
        return res if res < float(\'inf\') else 0
```
<br>

## Solution 2
Another idea is that,
For each `x` value in sorted order, check all `y` pairs.

In most cases, all points randomly distribute.
Only 500 points in 40001 * 40001 possible coordinates.
No two points will have the same `x` value or `y` value.
There will be no rectangle and the result is `0`.
This will be `O(N)` solution.

However, it seems that, in the test cases, it has a really big amount of rectangles.
In these worst cases, the time complexity is `O(nx * nx * ny)` < `O(N ^ 1.5)`.

In the extreme worst cases, like all points have `x = 0` or `y = 0`
Time complexity will be `O(N^2)`

Time `O(N^1.5)`, 80ms
<br>

```py
    def minAreaRect(self, points):
        n = len(points)
        nx = len(set(x for x, y in points))
        ny = len(set(y for x, y in points))
        if nx == n or ny == n:
            return 0

        p = collections.defaultdict(list)
        if nx > ny:
            for x, y in points:
                p[x].append(y)
        else:
            for x, y in points:
                p[y].append(x)

        lastx = {}
        res = float(\'inf\')
        for x in sorted(p):
            p[x].sort()
            for i in range(len(p[x])):
                for j in range(i):
                    y1, y2 = p[x][j], p[x][i]
                    if (y1, y2) in lastx:
                        res = min(res, (x - lastx[y1, y2]) * abs(y2 - y1))
                    lastx[y1, y2] = x
        return res if res < float(\'inf\') else 0
```


</p>


### C++ hashmap + set intersection
- Author: votrubac
- Creation Date: Sun Nov 11 2018 12:03:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 11 2018 12:03:23 GMT+0800 (Singapore Standard Time)

<p>
1. Collect x coords in a hash map. 
2. For each x coord, collect y coords in a set (sorted).
3. Go through each pair of x coords in the hash map. Ignore x coords that have less than 2 y-s.
4. Intersect two sets to get common y coords for this pair of x coords.
5. Since y coords are sorted, you only need to calculate the area for the ```y[k] - y[k - 1]``` pairs.
```
int minAreaRect(vector<vector<int>>& points, int res = INT_MAX) {
  unordered_map<int, set<int>> x;
  for (auto p : points) x[p[0]].insert(p[1]);
  for (auto i = x.begin(); i != x.end(); ++i)
    for (auto j = next(i); j != x.end(); ++j) {
      if (i->second.size() < 2 || j->second.size() < 2) continue;
      vector<int> y;
      set_intersection(begin(i->second), end(i->second),
        begin(j->second), end(j->second), back_inserter(y));
      for (int k = 1; k < y.size(); ++k)
        res = min(res, abs(j->first - i->first) * (y[k] - y[k - 1]));
    }
  return res == INT_MAX ? 0 : res;
}
```
## Complexity Analysis
Runtime: *O(n * n)*. We check each x coordinate agains all other x coordinates. The set intersection is linear.
Memory: *O(n)*. We store all points in our sets.
</p>


