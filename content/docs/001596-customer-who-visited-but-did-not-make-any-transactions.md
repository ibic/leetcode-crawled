---
title: "Customer Who Visited but Did Not Make Any Transactions"
weight: 1596
#id: "customer-who-visited-but-did-not-make-any-transactions"
---
## Description
<div class="description">
<p>Table: <code>Visits</code></p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| visit_id    | int     |
| customer_id | int     |
+-------------+---------+
visit_id is the primary key for this table.
This table contains information about the customers who visited the mall.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Transactions</code></p>

<pre>
+----------------+---------+
| Column Name    | Type    |
+----------------+---------+
| transaction_id | int     |
| visit_id       | int     |
| amount         | int     |
+----------------+---------+
transaction_id is the primary key for this table.
This table contains information about the transactions made during the visit_id.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the IDs of the users who visited without making&nbsp;any transactions and the number of times they made these types of visits.</p>

<p>Return the result table sorted in <strong>any order</strong>.</p>

<p>The query result format is in the following example:</p>

<pre>
<code>Visits</code>
+----------+-------------+
| visit_id | customer_id |
+----------+-------------+
| 1        | 23          |
| 2        | 9           |
| 4        | 30          |
| 5        | 54          |
| 6        | 96          |
| 7        | 54          |
| 8        | 54          |
+----------+-------------+

<code>Transactions</code>
+----------------+----------+--------+
| transaction_id | visit_id | amount |
+----------------+----------+--------+
| 2              | 5        | 310    |
| 3              | 5        | 300    |
| 9              | 5        | 200    |
| 12             | 1        | 910    |
| 13             | 2        | 970    |
+----------------+----------+--------+

Result table:
+-------------+----------------+
| customer_id | count_no_trans |
+-------------+----------------+
| 54          | 2              |
| 30          | 1              |
| 96          | 1              |
+-------------+----------------+
Customer with id = 23 visited the mall once and made one transaction during the visit with id = 12.
Customer with id = 9 visited the mall once and made one transaction during the visit with id = 13.
Customer with id = 30 visited the mall once and did not make any transactions.
Customer with id = 54 visited the mall three times. During 2 visits they did not make any transactions, and during one visit they made 3 transactions.
Customer with id = 96 visited the mall once and did not make any transactions.
As we can see, users with IDs 30 and 96 visited the mall one time without making any transactions. Also user 54 visited the mall twice and did not make any transactions.
</pre>

</div>

## Tags


## Companies
- NerdWallet - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### simple MySQL solution (Visits.visit_id not in (SELECT visit_id FROM Transactions))
- Author: eminem18753
- Creation Date: Fri Sep 11 2020 13:41:41 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 11 2020 19:02:46 GMT+0800 (Singapore Standard Time)

<p>
```
# Write your MySQL query statement below
SELECT customer_id, COUNT(*) as count_no_trans
FROM Visits
WHERE visit_id NOT IN (SELECT DISTINCT visit_id FROM Transactions)
GROUP BY customer_id;
```
</p>


### Simplest - Just a LEFT JOIN - No SUB-Query.
- Author: verisimilitude
- Creation Date: Sun Sep 27 2020 01:40:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 27 2020 01:40:02 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT
	customer_id,
	COUNT(Visits.visit_id) AS count_no_trans
FROM
	visits
LEFT JOIN Transactions
	ON Visits.visit_id = Transactions.visit_id
WHERE 
	Transactions.visit_id IS NULL
GROUP BY customer_id
```
</p>


### MySQL -No SubQuery
- Author: swethasekhar
- Creation Date: Sun Oct 04 2020 09:42:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 09:42:43 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT v.customer_id,COUNT(v.visit_id)as count_no_trans
from Visits v
LEFT JOIN Transactions t ON v.visit_id=t.visit_id
where t.visit_id IS NULL
group by v.customer_id
</p>


