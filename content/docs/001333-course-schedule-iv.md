---
title: "Course Schedule IV"
weight: 1333
#id: "course-schedule-iv"
---
## Description
<div class="description">
<p>There are a total of <code>n</code> courses you have to take, labeled from <code>0</code> to <code>n-1</code>.</p>

<p>Some courses may have direct prerequisites, for example, to take course 0 you have first to take course 1, which is expressed as a pair: <code>[1,0]</code></p>

<p>Given the total number of courses <code>n</code>,&nbsp;a list of direct&nbsp;<code>prerequisite</code> <strong>pairs</strong> and a list of <code>queries</code> <strong>pairs</strong>.</p>

<p>You should answer for each <code>queries[i]</code> whether the course <code>queries[i][0]</code> is a&nbsp;prerequisite of the course&nbsp;<code>queries[i][1]</code> or not.</p>

<p>Return <em>a list of boolean</em>, the answers to the given <code>queries</code>.</p>

<p>Please note that if course <strong>a</strong> is a prerequisite of course <strong>b</strong> and course <strong>b</strong> is a prerequisite&nbsp;of course <strong>c</strong>, then, course <strong>a</strong> is a&nbsp;prerequisite of course <strong>c</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/04/17/graph.png" style="width: 300px; height: 300px;" />
<pre>
<strong>Input:</strong> n = 2, prerequisites = [[1,0]], queries = [[0,1],[1,0]]
<strong>Output:</strong> [false,true]
<strong>Explanation:</strong> course 0 is not a prerequisite of course 1 but the opposite is true.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 2, prerequisites = [], queries = [[1,0],[0,1]]
<strong>Output:</strong> [false,false]
<strong>Explanation:</strong> There are no prerequisites and each course is independent.
</pre>

<p><strong>Example 3:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/04/17/graph-1.png" style="width: 300px; height: 300px;" />
<pre>
<strong>Input:</strong> n = 3, prerequisites = [[1,2],[1,0],[2,0]], queries = [[1,0],[1,2]]
<strong>Output:</strong> [true,true]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 3, prerequisites = [[1,0],[2,0]], queries = [[0,1],[2,0]]
<strong>Output:</strong> [false,true]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 5, prerequisites = [[0,1],[1,2],[2,3],[3,4]], queries = [[0,4],[4,0],[1,3],[3,0]]
<strong>Output:</strong> [true,false,true,false]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 100</code></li>
	<li><code>0 &lt;= prerequisite.length &lt;= (n * (n - 1) / 2)</code></li>
	<li><code>0 &lt;= prerequisite[i][0], prerequisite[i][1] &lt; n</code></li>
	<li><code>prerequisite[i][0] != prerequisite[i][1]</code></li>
	<li>The prerequisites graph has no cycles.</li>
	<li>The prerequisites graph has no repeated edges.</li>
	<li><code>1 &lt;= queries.length &lt;= 10^4</code></li>
	<li><code>queries[i][0] != queries[i][1]</code></li>
</ul>

</div>

## Tags
- Graph (graph)

## Companies
- Uber - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python] Floyd–Warshall Algorithm - Clean code - O(n^3)
- Author: hiepit
- Creation Date: Sun May 31 2020 00:01:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 31 2020 01:07:28 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
This problem is about check if 2 vertices are connected in directed graph. Floyd-Warshall `O(n^3)` is an algorithm that will output the minium distance of any vertices. We can modified it to output if any vertices is connected or not.

**Complexity:**
- Time: `O(n^3)`
- Space: `O(n^2)`

**More Floy-warshall problems:**
- [1334. Find the City With the Smallest Number of Neighbors at a Threshold Distance](https://leetcode.com/problems/find-the-city-with-the-smallest-number-of-neighbors-at-a-threshold-distance/discuss/491446/)

**Java**
```java
class Solution {
    public List<Boolean> checkIfPrerequisite(int n, int[][] prerequisites, int[][] queries) {
        boolean[][] connected = new boolean[n][n];
        for (int[] p : prerequisites)
            connected[p[0]][p[1]] = true; // p[0] -> p[1]
        for (int k = 0; k < n; k++)
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    connected[i][j] = connected[i][j] || connected[i][k] && connected[k][j];
        List<Boolean> ans = new ArrayList<>();
        for (int[] q : queries)
            ans.add(connected[q[0]][q[1]]);
        return ans;
    }
}
```

**Python**
```
class Solution:
    def checkIfPrerequisite(self, n, prerequisites, queries):
        connected = [[False] * n for i in xrange(n)]

        for i, j in prerequisites:
            connected[i][j] = True

        for k in range(n):
            for i in range(n):
                for j in range(n):
                    connected[i][j] = connected[i][j] or (connected[i][k] and connected[k][j])

        return [connected[i][j] for i, j in queries]
```
</p>


### C++ | DFS | Memoization
- Author: wh0ami
- Creation Date: Sun May 31 2020 00:25:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 31 2020 14:30:06 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    vector<vector<int>>dp;
    bool dfs(unordered_map<int, vector<int>>& mp, int a, int b) {
        
        if (dp[a][b] != -1)
            return dp[a][b];
        
        for (int i = 0; i < mp[a].size(); i++) {
            if (mp[a][i] == b || dfs(mp, mp[a][i], b))
                return dp[a][b] = 1;
        }
        
        return dp[a][b] = 0;
    }
public:
    vector<bool> checkIfPrerequisite(int n, vector<vector<int>>& prerequisites, vector<vector<int>>& queries) {
        
        unordered_map<int, vector<int>>mp;
        
        dp.resize(n, vector<int>(n, -1));
        for (int i = 0; i < prerequisites.size(); i++) {
            mp[prerequisites[i][1]].push_back(prerequisites[i][0]);
            dp[prerequisites[i][1]][prerequisites[i][0]] = 1;
        }
            
        vector<bool>res;
        for (int i = 0; i < queries.size(); i++) {
            bool p = dfs(mp, queries[i][1], queries[i][0]);
            res.push_back(p);
        }
        
        return res;
    }
};
```
</p>


### Transitive closure - Floyd Warshall with detailed explaination - python ,c++, java
- Author: sankethbk7777
- Creation Date: Sun May 31 2020 00:04:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 31 2020 00:19:25 GMT+0800 (Singapore Standard Time)

<p>
Brute force :
    for each i th query 
        start dfs from queries[i][0]
        if you reach queries[i][1] return True 
        else False
        


Since there can be 10^4 queries, we cannot do dfs every time for each query to
find if there is a path between queries[i][0] and queries[i][1]

We must answer each query in O(1) time 

What is transitive closure of a graph 

It is a matrix m in which 
m[i][j] is True if there j is reachable from i (can be a more than 1 edge path)
m[i][j] is False if j cannot be reached from i


Once we get the matrix of transitive closure, each query can be answered in O(1) time 
eg: query = (x,y) , answer will be m[x][y] 

To compute the matrix of transitive closure we use Floyd Warshall\'s algorithm which takes O(n^3) time and O(n^2) space.

Python3 

```
class Solution:
    def checkIfPrerequisite(self, n: int, prerequisites: List[List[int]], queries: List[List[int]]) -> List[bool]:
    
        def floydWarshall(reachable):
            for k in range(n):
                for i in range(n):
                    for j in range(n):
                        reachable[i][j] = reachable[i][j] or (reachable[i][k] and reachable[k][j])
            
            return reachable
            
        
        adjMatrix =  [[ 0 for i in range(n)] for j in range(n)]
        for i in prerequisites:
            adjMatrix[i[0]][i[1]] = 1
        
        #print(adjMatrix)
        ans = []
        floydWarshall(adjMatrix)
        #print(adjMatrix)
        for i in queries:
            ans.append(bool(adjMatrix[i[0]][i[1]]))
        
        return ans
```

C++
```
class Solution {
public:
    vector<bool> checkIfPrerequisite(int n, vector<vector<int>>& prerequisites, vector<vector<int>>& queries) {
        
        vector<vector<int>> adjMatrix(n, vector<int>(n));
        for (int i = 0; i < prerequisites.size(); ++i){
            adjMatrix[prerequisites[i][0]][prerequisites[i][1]] = 1;
        }
        
        FloydWarshall(n, adjMatrix);
        vector<bool> ans;
        
        for (int i = 0; i < queries.size(); ++i){
            ans.push_back(adjMatrix[queries[i][0]][queries[i][1]]);
        }
        
        return ans;
        
    }
    
    void FloydWarshall(int n, vector<vector<int>>& m){
        
        for (int k = 0; k < n ; ++k){
            for (int i = 0; i < n ; ++i){
                for (int j = 0; j < n ; ++j){
                    m[i][j] = m[i][j] or (m[i][k] and m[k][j]);
                }
            }
        }
    }
};
```

Java

```

class Solution {
    public List<Boolean> checkIfPrerequisite(int n, int[][] prerequisites, int[][] queries) {
        
        boolean adjMatrix[][] = new boolean[n][n];
        
        for (int[] i : prerequisites){
            adjMatrix[i[0]][i[1]] = true;
        }
        
        for (int k = 0; k < n; ++k){
            for (int i = 0; i < n ; ++i){
                for (int j = 0; j < n ; ++j){
                    adjMatrix[i][j] = adjMatrix[i][j] || (adjMatrix[i][k] && adjMatrix[k][j]);
                }
            }
        }
        
        List<Boolean> ans = new ArrayList<Boolean>();
        
        for (int i = 0; i < queries.length; ++i){
            ans.add(adjMatrix[queries[i][0]][queries[i][1]]);
        }
        
        return ans;
        
        
    }
}

```


</p>


