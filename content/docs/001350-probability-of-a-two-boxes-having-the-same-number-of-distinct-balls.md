---
title: "Probability of a Two Boxes Having The Same Number of Distinct Balls"
weight: 1350
#id: "probability-of-a-two-boxes-having-the-same-number-of-distinct-balls"
---
## Description
<div class="description">
<p>Given <code>2n</code> balls of <code>k</code> distinct colors. You will be given an integer array <code>balls</code> of size <code>k</code> where <code>balls[i]</code> is the number of balls of color <code>i</code>.&nbsp;</p>

<p>All the balls will be <strong>shuffled uniformly at random</strong>,&nbsp;then we will distribute the first <code>n</code> balls to the first box and the remaining <code>n</code> balls to the other box (Please read the explanation of the second example carefully).</p>

<p>Please note that the two boxes are considered different. For example, if we have two balls of colors <code>a</code> and <code>b</code>, and two boxes <code>[]</code> and <code>()</code>, then the distribution <code>[a] (b)</code> is considered different than the distribution <code>[b] (a)&nbsp;</code>(Please read the explanation of the first&nbsp;example carefully).</p>

<p>We want to <em>calculate the probability</em> that the two boxes have the same number of distinct balls.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> balls = [1,1]
<strong>Output:</strong> 1.00000
<strong>Explanation:</strong> Only 2 ways to divide the balls equally:
- A ball of color 1 to box 1 and a ball of color 2 to box 2
- A ball of color 2 to box 1 and a ball of color 1 to box 2
In both ways, the number of distinct colors in each box is equal. The probability is 2/2 = 1
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> balls = [2,1,1]
<strong>Output:</strong> 0.66667
<strong>Explanation:</strong> We have the set of balls [1, 1, 2, 3]
This set of balls will be shuffled randomly and we may have one of the 12 distinct shuffles with equale probability (i.e. 1/12):
[1,1 / 2,3], [1,1 / 3,2], [1,2 / 1,3], [1,2 / 3,1], [1,3 / 1,2], [1,3 / 2,1], [2,1 / 1,3], [2,1 / 3,1], [2,3 / 1,1], [3,1 / 1,2], [3,1 / 2,1], [3,2 / 1,1]
After that we add the first two balls to the first box and the second two balls to the second box.
We can see that 8 of these 12 possible random distributions have the same number of distinct colors of balls in each box.
Probability is 8/12 = 0.66667
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> balls = [1,2,1,2]
<strong>Output:</strong> 0.60000
<strong>Explanation:</strong> The set of balls is [1, 2, 2, 3, 4, 4]. It is hard to display all the 180 possible random shuffles of this set but it is easy to check that 108 of them will have the same number of distinct colors in each box.
Probability = 108 / 180 = 0.6
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> balls = [3,2,1]
<strong>Output:</strong> 0.30000
<strong>Explanation:</strong> The set of balls is [1, 1, 1, 2, 2, 3]. It is hard to display all the 60 possible random shuffles of this set but it is easy to check that 18 of them will have the same number of distinct colors in each box.
Probability = 18 / 60 = 0.3
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> balls = [6,6,6,6,6,6]
<strong>Output:</strong> 0.90327
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= balls.length &lt;= 8</code></li>
	<li><code>1 &lt;= balls[i] &lt;= 6</code></li>
	<li><code>sum(balls)</code> is even.</li>
	<li>Answers within <code>10^-5</code> of the actual value will be accepted as correct.</li>
</ul>

</div>

## Tags
- Math (math)
- Backtracking (backtracking)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Struggling with probability problems? Read this.
- Author: grawlixes
- Creation Date: Sun May 31 2020 12:03:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 01 2020 03:46:07 GMT+0800 (Singapore Standard Time)

<p>
When it comes to generic software engineering interview questions, probability problems are not as hard as they seem. Their solutions are often the same as your average backtracking (and DP if it\'s a harder problem) problem.

Let\'s define the following terms:
A *permutation* is some unique combination of the 2N-length array. If our input is [2,1,1] like in the second test case, an example permutation is [2 / 1,1,3].

A *valid permutation* is a permutation that can be considered in our answer, but may or may not meet the specific requirements of what we\'re counting. The above example for a permutation is not a *valid* permutation, because the number of balls in the first half is not equal to the number of balls in the second half. We want to divide the array in half, after all - the above permutation doesn\'t do that. An example of a valid permutation is [1, 1 / 2, 3].

A *successful* permutation is a valid permutation that meets the requirements of what we\'re counting. The above example of a valid permutation isn\'t *successful* because the number of unique balls in the first half (1) isn\'t the same as the number of unique balls in the second half (2). An example of a *successful* permutation is [1, 2 / 1, 3].

A simple strategy, which actually works here, is to just count the number of "successful" permutations and divide it by the number of "valid" permutations. I do this in my solution below.

Please reach out for any questions; I typed this up quickly, so there may be gaps I didn\'t explain. It makes me sad to see candidates mess up on probability questions like this, because if you know how to backtrack, you can solve problems like this. Good luck in your future interviews!

```
class Solution:
    def getProbability(self, balls: List[int]) -> float:
        from math import factorial
        
        firstHalf = {}
        secondHalf = {}
        
        # successful permutations
        self.good = 0
        # total number of valid permutations
        self.all = 0
        def dfs(i):
            if i == len(balls):
                s1 = sum(firstHalf.values())
                s2 = sum(secondHalf.values())
                # invalid permutation if the total number of balls in each
                # half is not equal, because we only consider permutations
                # with equal balls in each half
                if s1 != s2:
                    return 0
                
                # Get the number of permutations in the FIRST HALF of the result array.
				# If you don\'t understand, search "geeks for geeks number of distinct permutations" on Google.
                prod1 = 1
                for k in firstHalf:
                    prod1 *= factorial(firstHalf[k])
                p1 = factorial(s1) / prod1
                
                # Same as above but for the SECOND HALF of the array.
                prod2 = 1
                for k in secondHalf:
                    prod2 *= factorial(secondHalf[k])
                p2 = factorial(s2) / prod2
                
                # We can use each permutation as many times as possible since the problem
                # tells us they\'re all unique regardless of order. So [1, 2 / 1, 3] is separate
                # from [2, 1 / 3, 1].
                self.all += p1 * p2
                # only add to the "successful" permutations if we meet our success criteria: equal number 
                # of unique balls in each half of the array.
                self.good += p1 * p2 if len(firstHalf) == len(secondHalf) else 0
            else:
                # This will calculate every permutation of splitting the number of balls of color i
                # into each half. We So if there are 3 balls of color i, the iterations will split like this,
                # in order:
                # 0 -> first: 3, second: 0
                # 1 -> first: 2, second: 1
                # 2 -> first: 1, second: 2
                # 3 -> first: 0, second: 3
                firstHalf[i] = balls[i]
                for _ in range(((balls[i])) + 1):
                    dfs(i + 1)
                    
                    if i in firstHalf:
                        firstHalf[i] -= 1
                        if firstHalf[i] == 0:
                            del firstHalf[i]
                    secondHalf[i] = secondHalf.get(i, 0) + 1
                    
                del secondHalf[i]
                
        dfs(0)
        print(self.good, self.all)
        # if we have X good permutations and Y total permutations, the odds that a randomly
        # selected permutation will be "good" is X / Y AS LONG AS each permutation is equally likely.
        return self.good / self.all
```
</p>


### [Python] 10 Lines, 90%, Multionomial coefficients, explained
- Author: DBabichev
- Creation Date: Sun May 31 2020 12:05:10 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 08 2020 20:59:23 GMT+0800 (Singapore Standard Time)

<p>
This is an interesting mathematical problem. Denote by `k` different number of balls and by `B1, ..., Bk` number of balls of each type. Then we need to put into first box `X1, ..., Xk` balls and into the second `B1 - X1, ..., Bk - Xk`. We need to check 2 properties:
1. Number of balls in two boxes are equal.
2. Number of ball types in each boxes are equal.

Then we need to evaluate total number of possibilites for boxes, using **Multinomial coefficients**, (see https://en.wikipedia.org/wiki/Multinomial_theorem for more details) and divide it by total number of possibilities. Let us work through example: `B1, B2, B3 = 1, 2, 3`, then all possible ways to put into boxes are:
1. (0, 0, 0) and (1, 2, 3)
2. (0, 0, 1) and (1, 2, 2)
3. (0, 0, 2) and (1, 2, 1)

...

22. (1, 2, 1) and (0, 0, 2)
23. (1, 2, 2) and (0, 0, 1)
24. (1, 2, 3) and (0, 0, 0)

But only for 2 of them holds the above two properties:
1. (0, 2, 1) and (1, 0, 2)
2. (1, 0, 2) and (0, 2, 1)

So, the answer in this case will be `[M(0, 2, 1) * M(1, 0, 2) + M(1, 0, 2) * M(0, 2, 1)]/ M(1, 2, 3)`

**Complexity** We check all possible `(B1+1) * ... * (Bk+1)` splits into two boxes, and then evaluate number of zeros and multinomial coefficientes, so it is `O(B1 ... Bk * k)`

**Update** There is more elegant way to use multinomial coefficients, which I updated.

```
class Solution:
    def multinomial(self, n):
        return factorial(sum(n))/prod([factorial(i) for i in n])
  
    def getProbability(self, balls):
        k, n, Q = len(balls), sum(balls)// 2, 0
        arrays = [range(0,i+1) for i in balls]
        t = list(product(*arrays))
        for i in range(len(t)):
            if sum(t[i]) == n and t[i].count(0) == t[-i-1].count(0):
                Q += self.multinomial(t[i]) * self.multinomial(t[-i-1]) 

        return Q / self.multinomial(list(balls))     
```

If you have any questions, feel free to ask. If you like solution and explanations, please **Upvote**!
</p>


### C++ Backtrack with explanation
- Author: lzl124631x
- Creation Date: Sun May 31 2020 12:03:48 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 01 2020 05:21:31 GMT+0800 (Singapore Standard Time)

<p>
See my latest update in repo [LeetCode](https://github.com/lzl124631x/LeetCode)

## Solution 1.

First, compute the total number of distinct shuffles. 

```
total = factorial(sum( A[i] | 0 <= i < k )) / (factorial(A[0]) * factorial(A[1]) * ... * factorial(A[k-1]))
```

where `factorial(x)` is the number of permutations of `x` different items. For example, `factorial(3) = 1 * 2 * 3 = 6`.

I denote the right-hand side of the above equation as `perm(A)` where `A` is an array of numbers. We\'ll need it later.

Then we need to compute the count of valid splits. Assume array `a` and `b` is a split of `A`, then:

* `size(a) == size(b) == size(A) == k`
* For each `0 <= i < k`, `a[i] + b[i] = A[i]`

For example, if `A = [1, 2, 3]`, then `a = [1, 0, 1]`, `b = [0, 2, 2]` is a split of `A`.

A split is valid if:
* Each of them contains half of the balls: `sum( a[i] | 0 <= i < k ) == sum( b[i] | 0 <= i < k ) == sum( A[i] | 0 <= i < k ) / 2`
* They contain equal number of distinct colors: `count(a) == count(b)` where `count(x)` is the number of positive numbers in array `x`.

For example, if `A = [1, 1, 2]`, then `a = [1, 0, 1]`, `b = [0, 1, 1]` is a *valid* split of `A`.

So we can use DFS to get different valid splits of `A`. For each valid split `a, b`, the count of distinct permutation of the split is `perm(a) * perm(b)` .

The answer is the sum of `perm(a) * perm(b)` of all valid splits `a, b` divided by `total`.

```cpp
// OJ: https://leetcode.com/contest/weekly-contest-191/problems/probability-of-a-two-boxes-having-the-same-number-of-distinct-balls/
// Author: github.com/lzl124631x
// Time: O((M+1)^K * MK) where M is the maximum number of A[i]. It\'s O(7^8 * 6 * 8) given the constraints of this problem.
// Space: O(K)
class Solution {
    double perm(vector<int> &A) {
        double ans = 1;
        for (int i = 0, j = 1; i < A.size(); ++i) {
            for (int k = 1; k <= A[i]; ++k, ++j) ans = ans * j / k; 
        }
        return ans;
    }
    int sum = 0;
    double dfs(vector<int> &A, vector<int>& a, vector<int> &b, int i, int sa, int sb) {
        if (sa > sum / 2 || sb > sum / 2) return 0; // invalid split because either `a` or `b` takes up more than half of the balls.
        if (i == A.size()) {
            int ca = 0, cb = 0;
            for (int j = 0; j < A.size(); ++j) ca += a[j] > 0;
            for (int j = 0; j < A.size(); ++j) cb += b[j] > 0;
            if (ca != cb) return 0; // invalid split because `a` and `b` don\'t have the same number of distinct colors.
            return perm(a) * perm(b);
        }
        double ans = 0;
        for (int j = 0; j <= A[i]; ++j) { // try different splits at the `i`-th element, i.e. a[i] + b[i] = A[i]
            a[i] = j;
            b[i] = A[i] - j;
            ans += dfs(A, a, b, i + 1, sa + a[i], sb + b[i]);
        }
        return ans;
    }
public:
    double getProbability(vector<int>& A) {
        sum = accumulate(begin(A), end(A), 0);
        vector<int> a(A.size()), b(A.size());
        return dfs(A, a, b, 0, 0, 0) / perm(A);
    }
};
```
</p>


