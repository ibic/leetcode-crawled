---
title: "Copy List with Random Pointer"
weight: 138
#id: "copy-list-with-random-pointer"
---
## Description
<div class="description">
<p>A linked list is given such that each node contains an additional random pointer which could point to any node in the list or null.</p>

<p>Return a <a href="https://en.wikipedia.org/wiki/Object_copying#Deep_copy" target="_blank"><strong>deep copy</strong></a> of the list.</p>

<p>The Linked List is represented in the input/output as a list of <code>n</code> nodes. Each node is represented as a pair of <code>[val, random_index]</code> where:</p>

<ul>
	<li><code>val</code>: an integer representing <code>Node.val</code></li>
	<li><code>random_index</code>: the index of the node (range from <code>0</code> to <code>n-1</code>) where random pointer points to, or <code>null</code> if it does not point to any node.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/12/18/e1.png" style="width: 700px; height: 142px;" />
<pre>
<strong>Input:</strong> head = [[7,null],[13,0],[11,4],[10,2],[1,0]]
<strong>Output:</strong> [[7,null],[13,0],[11,4],[10,2],[1,0]]
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/12/18/e2.png" style="width: 700px; height: 114px;" />
<pre>
<strong>Input:</strong> head = [[1,1],[2,1]]
<strong>Output:</strong> [[1,1],[2,1]]
</pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/12/18/e3.png" style="width: 700px; height: 122px;" /></strong></p>

<pre>
<strong>Input:</strong> head = [[3,null],[3,0],[3,null]]
<strong>Output:</strong> [[3,null],[3,0],[3,null]]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> head = []
<strong>Output:</strong> []
<strong>Explanation:</strong> Given linked list is empty (null pointer), so return null.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>-10000 &lt;= Node.val &lt;= 10000</code></li>
	<li><code>Node.random</code> is null or pointing to a node in the linked list.</li>
	<li>The number of nodes will not exceed 1000.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Linked List (linked-list)

## Companies
- Amazon - 44 (taggedByAdmin: true)
- Facebook - 17 (taggedByAdmin: false)
- Microsoft - 6 (taggedByAdmin: true)
- Bloomberg - 4 (taggedByAdmin: true)
- eBay - 3 (taggedByAdmin: false)
- Qualtrics - 3 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- Capital One - 2 (taggedByAdmin: false)
- Oracle - 6 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Expedia - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)
- Visa - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---

Lets first look at how the linked list looks like
<center>
<img src="../Figures/138/138_Copy_List_Random_1.png" width="500"/>
</center>

In the above diagram, for a given node the `next` pointer points to the next node in the linked list. The `next` pointer is something standard for a linked list and this is what ***links*** the nodes together. What is interesting about the diagram and this problem is the `random` pointer which, as the name suggests can point to any node in the linked list or can be a null.

#### Approach 1: Recursive

**Intuition**

The basic idea behind the recursive solution is to consider the linked list like a graph. Every node of the Linked List has 2 pointers (edges in a graph). Since, random pointers add the randomness to the structure we might visit the same node again leading to cycles.

<center>
<img src="../Figures/138/138_Copy_List_Random_2.png" width="500"/>
</center>

In the diagram above we can see the random pointer points back to the previously seen node hence leading to a cycle. We need to take care of these cycles in the implementation.

All we do in this approach is to just traverse the graph and clone it. Cloning essentially means creating a new node for every unseen node you encounter. The traversal part will happen recursively in a depth first manner. Note that we have to keep track of nodes already processed because, as pointed out earlier, we can have cycles because of the random pointers.

**Algorithm**

1. Start traversing the graph from `head` node.

    Lets see the linked structure as a graph. Below is the graph representation of the above linked list example.
    <center>
    <img src="../Figures/138/138_Copy_List_Random_7.png" width="500"/>
    </center>

    In the above example `head` is where we begin our graph traversal.

2. If we already have a cloned copy of the current node in the visited dictionary, we use the cloned node reference.  
3. If we don't have a cloned copy in the visited dictionary, we create a new node and add it to the visited dictionary.
`visited_dictionary[current_node] = cloned_node_for_current_node.`
4. We then make two recursive calls, one using the `random` pointer and the other using `next` pointer. The diagram from step 1, shows `random` and `next` pointers in red and blue color respectively. Essentially we are making recursive calls for the children of the current node. In this implementation, the children are the nodes pointed by the `random` and the `next` pointers.
<pre>
cloned_node_for_current_node.next = copyRandomList(current_node.next);
cloned_node_for_current_node.random = copyRandomList(current_node.random);
</pre>

<br>

<iframe src="https://leetcode.com/playground/bkd3BnZr/shared" frameBorder="0" width="100%" height="500" name="bkd3BnZr"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ where N is the number of nodes in the linked list.
* Space Complexity: $$O(N)$$. If we look closely, we have the recursion stack and we also have the space complexity to keep track of nodes already cloned i.e. using the visited dictionary. But asymptotically, the complexity is $$O(N)$$.
<br/>
<br/>

---

#### Approach 2: Iterative with $$O(N)$$ Space

**Intuition**

The iterative solution to this problem does not model it as a graph, instead simply treats it as a LinkedList.
When we are iterating over the list, we can create new nodes via the random pointer or the next pointer whichever points to a node that doesn't exist in our old --> new dictionary.

**Algorithm**

1. Traverse the linked list starting at `head` of the linked list.
    <center>
    <img src="../Figures/138/138_Copy_List_Random_3.png" width="600"/>
    </center>

    In the above diagram we create a new cloned `head` node. The cloned node is shown using dashed lines. In the implementation we would even store the reference of this newly created node in a visited dictionary.

2. Random Pointer
    * If the `random` pointer of the current node $$i$$ points to the a node $$j$$ and a clone of $$j$$ already exists in the visited dictionary, we will simply use the cloned node reference from the visited dictionary.
    * If the `random` pointer of the current node $$i$$ points to the a node $$j$$ which has not been created yet, we create a new node corresponding to $$j$$ and add it to the visited dictionary.

    <center>
    <img src="../Figures/138/138_Copy_List_Random_4.png" width="600"/>
    </center>

    In the above diagram the `random` pointer of node $$A$$ points to a node $$C$$. Node $$C$$ which was not visited yet as we can see from the previous diagram. Hence we create a new cloned $$C'$$ node corresponding to node $$C$$ and add it to visited dictionary.

3. Next Pointer
    * If the `next` pointer of the current node $$i$$ points to the a node $$j$$ and a clone of $$j$$ already exists in the visited dictionary, we will simply use the cloned node reference from the visited dictionary.
    * If the `next` pointer of the current node $$i$$ points to the a node $$j$$ which has not been created yet, we create a new node corresponding to $$j$$ and add it to the visited dictionary.

    <center>
    <img src="../Figures/138/138_Copy_List_Random_5.png" width="600"/>
    </center>

    In the above diagram the `next` pointer of node $$A$$ points to a node $$B$$. Node $$B$$ which was not visited yet as we can see from the previous diagram. Hence we create a new cloned $$B'$$ node corresponding to node $$B$$ and add it to visited dictionary.

4. We repeat steps 2 and 3 until we reach the end of the linked list.

    <center>
    <img src="../Figures/138/138_Copy_List_Random_6.png" width="600"/>
    </center>

    In the above diagram, the `random` pointer of node $$B$$ points to an already visited node $$A$$. Hence in step 2, we don't create a new copy for the clone. Instead we point `random` pointer of cloned node $$B'$$ to already existing cloned node $$A'$$.

    Also, the `next` pointer of node $$B$$ points to an already visited node $$C$$. Hence in step 3, we don't create a new copy for the clone. Instead we point `next` pointer of cloned node $$B'$$ to already existing cloned node $$C'$$.

<iframe src="https://leetcode.com/playground/3R56nxxb/shared" frameBorder="0" width="100%" height="500" name="3R56nxxb"></iframe>

**Complexity Analysis**

* Time Complexity : $$O(N)$$ because we make one pass over the original linked list.
* Space Complexity : $$O(N)$$ as we have a dictionary containing mapping from old list nodes to new list nodes. Since there are $$N$$ nodes, we have $$O(N)$$ space complexity.
<br/>
<br/>

---

#### Approach 3: Iterative with $$O(1)$$ Space

**Intuition**

Instead of a separate dictionary to keep the old node --> new node mapping, we can tweak the original linked list and keep every cloned node next to its original node. This interleaving of old and new nodes allows us to solve this problem without any extra space. Lets look at how the algorithm works.

**Algorithm**

1. Traverse the original list and clone the nodes as you go and place the cloned copy next to its original node. This new linked list is essentially a interweaving of original and cloned nodes.

    <center>
    <img src="../Figures/138/138_Copy_List_Random_8_1.png" width="800"/>
    </center>
    <center>
    <img src="../Figures/138/138_Copy_List_Random_8_2.png" width="800"/>
    </center>

    As you can see we just use the value of original node to create the cloned copy. The `next` pointer is used to create the weaving. Note that this operation ends up modifying the original linked list.
    <pre>
    cloned_node.next = original_node.next
    original_node.next = cloned_node
    </pre>

2. Iterate the list having both the new and old nodes intertwined with each other and use the original nodes' random pointers to assign references to random pointers for cloned nodes. For eg. If `B` has a random pointer to `A`, this means `B'` has a random pointer to `A'`.

    <center>
    <img src="../Figures/138/138_Copy_List_Random_9_1.png" width="800"/>
    </center>

3. Now that the `random` pointers are assigned to the correct node, the `next` pointers need to be correctly assigned to unweave the current linked list and get back the original list and the cloned list.

    <center>
    <img src="../Figures/138/138_Copy_List_Random_10.png" width="800"/>
    </center>


<iframe src="https://leetcode.com/playground/5RVZLP5S/shared" frameBorder="0" width="100%" height="500" name="5RVZLP5S"></iframe>

**Complexity Analysis**

* Time Complexity : $$O(N)$$
* Space Complexity : $$O(1)$$
<br /><br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A solution with constant space complexity O(1) and linear time complexity O(N)
- Author: liaison
- Creation Date: Tue Jan 20 2015 03:53:11 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 00:07:12 GMT+0800 (Singapore Standard Time)

<p>
An intuitive solution is to keep a hash table for each node in the list, via which we just need to iterate the list in 2 rounds respectively to create nodes and assign the values for their random pointers. As a result, the space complexity of this solution is `O(N)`, although with a linear time complexity. 

*Note:  if we do not consider the space reversed for the output, then we could say that the algorithm does not consume any additional memory during the processing, i.e. O(1) space complexity*

As an optimised solution, we could reduce the space complexity into constant. ***The idea is to associate the original node with its copy node in a single linked list. In this way, we don\'t need extra space to keep track of the new nodes.***

The algorithm is composed of the follow three steps which are also 3 iteration rounds. 

 1. Iterate the original list and duplicate each node. The duplicate
    of each node follows its original immediately.
 2. Iterate the new list and assign the random pointer for each
    duplicated node.
 3. Restore the original list and extract the duplicated nodes.

The algorithm is implemented as follows:
```java
public RandomListNode copyRandomList(RandomListNode head) {
  RandomListNode iter = head, next;

  // First round: make copy of each node,
  // and link them together side-by-side in a single list.
  while (iter != null) {
    next = iter.next;

    RandomListNode copy = new RandomListNode(iter.label);
    iter.next = copy;
    copy.next = next;

    iter = next;
  }

  // Second round: assign random pointers for the copy nodes.
  iter = head;
  while (iter != null) {
    if (iter.random != null) {
      iter.next.random = iter.random.next;
    }
    iter = iter.next.next;
  }

  // Third round: restore the original list, and extract the copy list.
  iter = head;
  RandomListNode pseudoHead = new RandomListNode(0);
  RandomListNode copy, copyIter = pseudoHead;

  while (iter != null) {
    next = iter.next.next;

    // extract the copy
    copy = iter.next;
    copyIter.next = copy;
    copyIter = copy;

    // restore the original list
    iter.next = next;

    iter = next;
  }

  return pseudoHead.next;
}
```
</p>


### Java O(n) solution
- Author: jeantimex
- Creation Date: Wed Jul 08 2015 06:25:28 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 12:57:08 GMT+0800 (Singapore Standard Time)

<p>
    public RandomListNode copyRandomList(RandomListNode head) {
      if (head == null) return null;
      
      Map<RandomListNode, RandomListNode> map = new HashMap<RandomListNode, RandomListNode>();
      
      // loop 1. copy all the nodes
      RandomListNode node = head;
      while (node != null) {
        map.put(node, new RandomListNode(node.label));
        node = node.next;
      }
      
      // loop 2. assign next and random pointers
      node = head;
      while (node != null) {
        map.get(node).next = map.get(node.next);
        map.get(node).random = map.get(node.random);
        node = node.next;
      }
      
      return map.get(head);
    }
</p>


### 2 clean C++ algorithms without using extra array/hash table.  Algorithms are explained step by step.
- Author: satyakam
- Creation Date: Tue Dec 09 2014 01:59:13 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 04:58:17 GMT+0800 (Singapore Standard Time)

<p>
    //
    // Here's how the 1st algorithm goes.
    // Consider l1 as a node on the 1st list and l2 as the corresponding node on 2nd list.
    // Step 1:
    // Build the 2nd list by creating a new node for each node in 1st list. 
    // While doing so, insert each new node after it's corresponding node in the 1st list.
    // Step 2:
    // The new head is the 2nd node as that was the first inserted node.
    // Step 3:
    // Fix the random pointers in the 2nd list: (Remember that l1->next is actually l2)
    // l2->random will be the node in 2nd list that corresponds l1->random, 
    // which is next node of l1->random.
    // Step 4:
    // Separate the combined list into 2: Splice out nodes that are part of second list. 
    // Return the new head that we saved in step 2.
    //
    
    RandomListNode *copyRandomList(RandomListNode *head) {
        RandomListNode *newHead, *l1, *l2;
        if (head == NULL) return NULL;
        for (l1 = head; l1 != NULL; l1 = l1->next->next) {
            l2 = new RandomListNode(l1->label);
            l2->next = l1->next;
            l1->next = l2;
        }
            
        newHead = head->next;
        for (l1 = head; l1 != NULL; l1 = l1->next->next) {
            if (l1->random != NULL) l1->next->random = l1->random->next;
        }
            
        for (l1 = head; l1 != NULL; l1 = l1->next) {
            l2 = l1->next;
            l1->next = l2->next;
            if (l2->next != NULL) l2->next = l2->next->next;
        }
    
        return newHead;
    }


    //
    // Here's how the 2nd algorithm goes.
    // Consider l1 as a node on the 1st list and l2 as the corresponding node on 2nd list.
    // Step 1:
    // Build the 2nd list by creating a new node for each node in 1st list. 
    // While doing so, set the next pointer of the new node to the random pointer 
    // of the corresponding node in the 1st list.  And set the random pointer of the 
    // 1st list's node to the newly created node.
    // Step 2:
    // The new head is the node pointed to by the random pointer of the 1st list.
    // Step 3:
    // Fix the random pointers in the 2nd list: (Remember that l1->random is l2)
    // l2->random will be the node in 2nd list that corresponds to the node in the 
    // 1st list that is pointed to by l2->next, 
    // Step 4:
    // Restore the random pointers of the 1st list and fix the next pointers of the 
    // 2nd list. random pointer of the node in 1st list is the next pointer of the 
    // corresponding node in the 2nd list.  This is what we had done in the 
    // 1st step and now we are reverting back. next pointer of the node in 
    // 2nd list is the random pointer of the node in 1st list that is pointed to 
    // by the next pointer of the corresponding node in the 1st list.
    // Return the new head that we saved in step 2.
    //

    RandomListNode *copyRandomList(RandomListNode *head) {
        RandomListNode *newHead, *l1, *l2;
        if (head == NULL) return NULL;

        for (l1 = head; l1 != NULL; l1 = l1->next) {
            l2 = new RandomListNode(l1->label);
            l2->next = l1->random;
            l1->random = l2;
        }
        
        newHead = head->random;
        for (l1 = head; l1 != NULL; l1 = l1->next) {
            l2 = l1->random;
            l2->random = l2->next ? l2->next->random : NULL;
        }
        
        for (l1 = head; l1 != NULL; l1 = l1->next) {
            l2 = l1->random;
            l1->random = l2->next;
            l2->next = l1->next ? l1->next->random : NULL;
        }

        return newHead;
    }
</p>


