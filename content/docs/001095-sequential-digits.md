---
title: "Sequential Digits"
weight: 1095
#id: "sequential-digits"
---
## Description
<div class="description">
<p>An&nbsp;integer has <em>sequential digits</em> if and only if each digit in the number is one more than the previous digit.</p>

<p>Return a <strong>sorted</strong> list of all the integers&nbsp;in the range <code>[low, high]</code>&nbsp;inclusive that have sequential digits.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> low = 100, high = 300
<strong>Output:</strong> [123,234]
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> low = 1000, high = 13000
<strong>Output:</strong> [1234,2345,3456,4567,5678,6789,12345]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>10 &lt;= low &lt;= high &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- F5 Networks - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Sliding Window

One might notice that all integers that have sequential digits
are substrings of string "123456789". 
Hence to generate all such integers of a given length, just move 
the window of that length along "123456789" string.

The advantage of this method is that it will generate the integers that are
already in the sorted order. 

![diff](../Figures/1291/sliding.png)

**Algorithm**

- Initialize sample string "123456789". This string contains 
all integers that have sequential digits as substrings.
Let's implement sliding window algorithm to generate them.

- Iterate over all possible string lengths: from the 
length of `low` to the length of `high`. 

    - For each length iterate over all possible start indexes:
    from `0` to `10 - length`.
    
        - Construct the number from digits inside the sliding window
        of current length.
        
        - Add this number in the output list `nums`, 
        if it's greater than `low` and less than `high`.
        
- Return `nums`.

**Implementation**

<iframe src="https://leetcode.com/playground/Rwndd7td/shared" frameBorder="0" width="100%" height="344" name="Rwndd7td"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(1)$$. Length of sample string is 9, and 
lengths of low and high are between 2 and 9. Hence the nested loops are executed 
no more than $$8 \times 8 = 64$$ times.

* Space complexity: $$\mathcal{O}(1)$$ to keep not more than 36 integers 
with sequential digits.
<br /> 
<br />


---
#### Approach 2: Precomputation 


Actually, there are 36 integers with the sequential digits.
Here is how we calculate it.

Starting from 9 digits in the sample string, one could construct 
9 - 2 + 1 = 8 integers of length 2, 9 - 3 + 1 = 7 integers of length 3, 
and so on and so forth.
In total, it would make 8 + 7 + ... + 1 = 36 integers. 

As one can see, we could precompute the results all at once and then 
select the integers that are less than `high` and greater than `low`. 

**Implementation**

<iframe src="https://leetcode.com/playground/FwjU4XJ2/shared" frameBorder="0" width="100%" height="480" name="FwjU4XJ2"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(1)$$ both for precomputation and during runtime. 
Precomputation: Length of sample string is 9, 
and the nested loops are executed $$8 \times 8 = 64$$ times.
Runtime: One iterates over an array of 36 integers. 

* Space complexity: $$\mathcal{O}(1)$$ to keep 36 integers 
that have sequential digits.
<br /> 
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Just a joke
- Author: Tangcodes
- Creation Date: Sun Dec 15 2019 12:09:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 15 2019 12:17:56 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public List<Integer> sequentialDigits(int low, int high) {
        int[] allNums = {12,23,34,45,56,67,78,89,
                         123,234,345,456,567,678,789,
                         1234,2345,3456,4567,5678,6789,
                         12345,23456,34567,45678,56789,
                         123456,234567,345678,456789,
                         1234567,2345678,3456789,
                         12345678,23456789,
                         123456789};
        List<Integer> res = new ArrayList<>();
        int n = allNums.length;
        for (int i = 0; i < n; i++) {
            if (allNums[i] < low) continue;
            if (allNums[i] > high) break;
            res.add(allNums[i]);
        }
        return res;
    }
}
```
</p>


### C++ BFS
- Author: gagandeepahuja09
- Creation Date: Sun Dec 15 2019 12:13:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 15 2019 12:13:13 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    vector<int> sequentialDigits(int l, int h) {
        queue<int> q;
        for(int i = 1; i <= 9; i++) {
            q.push(i);
        }
        vector<int> ret;
        while(!q.empty()) {
            int f = q.front();
            q.pop();
            if(f <= h && f >= l) {
                ret.push_back(f);
            }
            if(f > h)
                break;
            int num = f % 10;
            if(num < 9) {
                q.push(f * 10 + (num + 1));
            }
        }
        return ret;
    }
};
```
</p>


### [Python] Solution, using queue, explained
- Author: DBabichev
- Creation Date: Sat Sep 19 2020 16:11:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 19 2020 16:11:42 GMT+0800 (Singapore Standard Time)

<p>
Let us create queue, where we put numbers `1,2,3,4,5,6,7,8,9` at the beginning. Then on each step we are going to extract number and put this number with added to the end incremented last digit: so we have on the next step:

`2,3,4,5,6,7,8,9,12`
`3,4,5,6,7,8,9,12,23`
...
`12,23,34,45,56,67,78,89`
`23,34,45,56,67,78,89,123`,
...

On each, when we extract number from the beginning we check if it is in our range and if it is, we put it into our `out` list. Then we add new candidate to the end of our queue. In this way we make sure, that we generate elements in increasing order.

**Complexity**:  There will be `9+8+...+ 1` numbers with sqeuential digits at all and for each of them we need to check it at most once (though in practice we wil finish earlier), so time complexity is `O(45) = O(1)`. Space complexity is also `O(45) = O(1)`.

```
class Solution:
    def sequentialDigits(self, low, high):
        out = []
        queue = deque(range(1,10))
        while queue:
            elem = queue.popleft()
            if low <= elem <= high:
                out.append(elem)
            last = elem % 10
            if last < 9: queue.append(elem*10 + last + 1)
                    
        return out
```

If you have any questions, feel free to ask. If you like solution and explanations, please **Upvote!**
</p>


