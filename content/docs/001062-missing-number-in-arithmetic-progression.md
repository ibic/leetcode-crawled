---
title: "Missing Number In Arithmetic Progression"
weight: 1062
#id: "missing-number-in-arithmetic-progression"
---
## Description
<div class="description">
<p>In some array <code>arr</code>, the values were in arithmetic progression: the values&nbsp;<code>arr[i+1] - arr[i]</code>&nbsp;are all&nbsp;equal for every&nbsp;<code>0 &lt;= i &lt; arr.length - 1</code>.</p>

<p>Then, a value from <code>arr</code>&nbsp;was removed that <strong>was&nbsp;not the first or last value in the array</strong>.</p>

<p>Return the removed value.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [5,7,11,13]
<strong>Output:</strong> 9
<strong>Explanation: </strong>The previous array was [5,7,<strong>9</strong>,11,13].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [15,13,12]
<strong>Output:</strong> 14
<strong>Explanation: </strong>The previous array was [15,<strong>14</strong>,13,12].</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>3 &lt;= arr.length &lt;= 1000</code></li>
	<li><code>0 &lt;= arr[i] &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Audible - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Arithmetic Sum and Binary Search
- Author: lee215
- Creation Date: Sun Oct 20 2019 00:03:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 00:47:06 GMT+0800 (Singapore Standard Time)

<p>
Please reply and upvote now.
Don\'t have prime membership.
Cannot even read and modify my own post later, when it\'s locked.
<br>

## Soluton 1: Arithmetic Sequence Sum
`arithmetic sequence sum = (first + last) * n / 2`
In this problem, the first and last value are not removed.
`first = min(A)`, `last = max(A)`
We can calulate the sum of arithmetic sequence.
The difference between `sum - sum(A)` is the missing number.
<br>

## **Complexity**
Time `O(N)`
Space `O(1)`
<br>

**Java:**
```java
    public int missingNumber(int[] A) {
        int first = A[0], last = A[0], sum = 0, n = A.length;
        for (int a : A) {
            first = Math.min(first, a);
            last = Math.max(last, a);
            sum += a;
        }
        return (first + last) * (n + 1) / 2 - sum;
    }
```

**C++:**
```cpp
    int missingNumber(vector<int>& A) {
        int first = A[0], last = A[0], sum = 0, n = A.size();
        for (int a : A) {
            first = min(first, a);
            last = max(last, a);
            sum += a;
        }
        return (first + last) * (n + 1) / 2 - sum;
    }
```

**Python:**
```python
    def missingNumber(self, A):
        return (min(A) + max(A)) * (len(A) + 1) / 2 - sum(A)
```
<br>

## Solution 2: Binary Search
**Java:**
```java
    public int missingNumber(int[] A) {
        int n = A.length, d = (A[n - 1] - A[0]) / n, left = 0, right = n;
        while (left < right) {
            int mid = (left + right) / 2;
            if (A[mid] == A[0] + d * mid)
                left = mid + 1;
            else
                right = mid;
        }
        return A[0] + d * left;
    }
```

**C++:**
```cpp
    int missingNumber(vector<int>& A) {
        int n = A.size(), d = (A[n - 1] - A[0]) / n, left = 0, right = n;
        while (left < right) {
            int mid = (left + right) / 2;
            if (A[mid] == A[0] + d * mid)
                left = mid + 1;
            else
                right = mid;
        }
        return A[0] + d * left;
    }
```

**Python:**
```python
    def missingNumber(self, A):
        n = len(A)
        d = (A[-1] - A[0]) / n
        left, right = 0, n
        while left < right:
            mid = (left + right) / 2
            if A[mid] == A[0] + d * mid:
                left = mid + 1
            else:
                right = mid
        return A[0] + d * left
```
</p>


### Java simple scan
- Author: yjyjyj
- Creation Date: Sun Oct 20 2019 16:50:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 16:50:54 GMT+0800 (Singapore Standard Time)

<p>
calculate the different between each two number 
then scan till mismatch



```
class Solution {
    public int missingNumber(int[] arr) {
        final int N = arr.length;
        final int d = (arr[N-1] - arr[0]) / N;
        int i = arr[0];
        for(int j: arr) {
            if(i!=j)
                break;
            i+=d;
        }
        return i;
    }
}
```
</p>


### Early return in C, 0ms
- Author: ggorlen
- Creation Date: Sun Oct 20 2019 12:42:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 12:45:39 GMT+0800 (Singapore Standard Time)

<p>
```
int missingNumber(int* arr, int arrSize) {
    for (int i = 2; i < arrSize; i++) {
        if (arr[i-1] - arr[i-2] != arr[i] - arr[i-1]) {
            return arr[i] + arr[i-2] - arr[i-1];
        }
    }
  
    return 0;
}
```

The idea is to compare every element with its immediate neighbors. If the gaps between the neighbors and the middle element don\'t match, we need to determine the value of the missing element. This can be done by subtracting the gap between the left element and the middle element from the right element.

For example, if the three numbers in a triplet are `[3, 5, 9]`, we return `9 + 3 - 5 = 12 - 5 = 7`. If the gap is on the other side of the triplet, as is the case in `[3, 7, 9]`, we return `9 + 3 - 7 = 12 - 7 = 5`.

`return 0;` is unreachable since input is always valid.
</p>


