---
title: "Kth Largest Element in a Stream"
weight: 710
#id: "kth-largest-element-in-a-stream"
---
## Description
<div class="description">
<p>Design a class to find&nbsp;the <code>k<sup>th</sup></code> largest element in a stream. Note that it is the <code>k<sup>th</sup></code> largest element in the sorted order, not the <code>k<sup>th</sup></code> distinct element.</p>

<p>Implement&nbsp;<code>KthLargest</code>&nbsp;class:</p>

<ul>
	<li><code>KthLargest(int k, int[] nums)</code>&nbsp;Initializes the object with the integer <code>k</code> and the stream of integers <code>nums</code>.</li>
	<li><code>int add(int val)</code>&nbsp;Returns the element representing the <code>k<sup>th</sup></code> largest element in the stream.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;KthLargest&quot;, &quot;add&quot;, &quot;add&quot;, &quot;add&quot;, &quot;add&quot;, &quot;add&quot;]
[[3, [4, 5, 8, 2]], [3], [5], [10], [9], [4]]
<strong>Output</strong>
[null, 4, 5, 5, 8, 8]

<strong>Explanation</strong>
KthLargest kthLargest = new KthLargest(3, [4, 5, 8, 2]);
kthLargest.add(3);   // return 4
kthLargest.add(5);   // return 5
kthLargest.add(10);  // return 5
kthLargest.add(9);   // return 8
kthLargest.add(4);   // return 8
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= k &lt;= 10<sup>4</sup></code></li>
	<li><code>0 &lt;= nums.length &lt;= 10<sup>4</sup></code></li>
	<li><code>-10<sup>4</sup> &lt;= nums[i] &lt;= 10<sup>4</sup></code></li>
	<li><code>-10<sup>4</sup> &lt;= val &lt;= 10<sup>4</sup></code></li>
	<li>At most <code>10<sup>4</sup></code> calls will be made to <code>add</code>.</li>
</ul>
</div>

## Tags
- Heap (heap)
- Design (design)

## Companies
- Amazon - 5 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Nutanix - 3 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java, Priority Queue
- Author: climberig
- Creation Date: Fri Jul 13 2018 10:21:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 09:29:25 GMT+0800 (Singapore Standard Time)

<p>
Keep track of the k biggest elements in the minimum priority queue ```q```. ```q.peek()``` is the answer.
```java
   class KthLargest {
        final PriorityQueue<Integer> q;
        final int k;

        public KthLargest(int k, int[] a) {
            this.k = k;
            q = new PriorityQueue<>(k);
            for (int n : a)
                add(n);
        }

        public int add(int n) {
            if (q.size() < k)
                q.offer(n);
            else if (q.peek() < n) {
                q.poll();
                q.offer(n);
            }
            return q.peek();
        }
    }
</p>


### Python simple heapq solution beats 100 %
- Author: cenkay
- Creation Date: Fri Jul 13 2018 00:39:55 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 07:12:50 GMT+0800 (Singapore Standard Time)

<p>
```
class KthLargest(object):

    
    def __init__(self, k, nums):
        self.pool = nums
        self.k = k
        heapq.heapify(self.pool)
        while len(self.pool) > k:
            heapq.heappop(self.pool)

            
    def add(self, val):
        if len(self.pool) < self.k:
            heapq.heappush(self.pool, val)
        elif val > self.pool[0]:
            heapq.heapreplace(self.pool, val)
        return self.pool[0]
    
```
</p>


### Explanation of MinHeap solution (NO CODE)
- Author: skazantsev
- Creation Date: Mon Jul 23 2018 01:18:52 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 08:05:15 GMT+0800 (Singapore Standard Time)

<p>
We can build a `MinHeap` that contains only `k` largest elements.
On `add`:
  - compare a new element `x` with `min` to decide if we should `pop min` and `insert x`
  - take into account a case when `heap_size` is less than `k`

Construction is simply calling the `add` function `N` times.

Time complexity:
- Construction: `O(N * logK)`
- Adding: `O(logK)`

Additional memory:
- `O(K)` (can be reduced to `O(1)` by reusing memory of the existing array)

Have fun!
</p>


