---
title: "Construct Binary Tree from Preorder and Inorder Traversal"
weight: 105
#id: "construct-binary-tree-from-preorder-and-inorder-traversal"
---
## Description
<div class="description">
<p>Given preorder and inorder traversal of a tree, construct the binary tree.</p>

<p><strong>Note:</strong><br />
You may assume that duplicates do not exist in the tree.</p>

<p>For example, given</p>

<pre>
preorder =&nbsp;[3,9,20,15,7]
inorder = [9,3,15,20,7]</pre>

<p>Return the following binary tree:</p>

<pre>
    3
   / \
  9  20
    /  \
   15   7</pre>

</div>

## Tags
- Array (array)
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 17 (taggedByAdmin: false)
- Microsoft - 6 (taggedByAdmin: false)
- Google - 6 (taggedByAdmin: false)
- Bloomberg - 5 (taggedByAdmin: true)
- Apple - 3 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Facebook - 7 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Square - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### How to traverse the tree

There are two general strategies to traverse a tree:

- *Breadth First Search* (`BFS`)

    We scan through the tree level by level, following the order of height,
    from top to bottom. The nodes on higher level would be visited before
    the ones with lower levels.
     
- *Depth First Search* (`DFS`)

    In this strategy, we adopt the `depth` as the priority, so that one
    would start from a root and reach all the way down to certain leaf,
    and then back to root to reach another branch.

    The DFS strategy can further be distinguished as
    `preorder`, `inorder`, and `postorder` depending on the relative order
    among the root node, left node and right node.
    
On the following figure the nodes are numerated in the order you visit them,
please follow ```1-2-3-4-5``` to compare different strategies.

![postorder](../Figures/145_transverse.png)

Here the problem is to construct a binary tree from its preorder and
inorder traversal.
<br />
<br />


---
#### Approach 1: Recursion

**Tree definition**

First of all, here is the definition of the ```TreeNode``` which we would use.

<iframe src="https://leetcode.com/playground/dHQJDgvV/shared" frameBorder="0" width="100%" height="225" name="dHQJDgvV"></iframe>
 
**Algorithm**

As discussed above the preorder traversal follows `Root -> Left -> Right` order,
that makes it very convenient to construct the tree from its root.

Let's do it. The first element in the *preorder* list is a root. 
This root splits *inorder* list into left and right subtrees.
Now one have to pop up the root from preorder list since 
it's already used as a tree node and then repeat the step above for the
left and right subtrees. 

<!--![LIS](../Figures/105/105_tr.gif)-->
!?!../Documents/105_LIS.json:1008,542!?!

<iframe src="https://leetcode.com/playground/aecR9NNg/shared" frameBorder="0" width="100%" height="500" name="aecR9NNg"></iframe>

**Complexity analysis**

* Time complexity : $$\mathcal{O}(N)$$. Let's compute the solution with the help of 
[master theorem](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)) 
$$T(N) = aT\left(\frac{b}{N}\right) + \Theta(N^d)$$.
The equation represents dividing the problem 
up into $$a$$ subproblems of size $$\frac{N}{b}$$ in $$\Theta(N^d)$$ time. 
Here one divides the problem in two subproblemes `a = 2`, the size of each subproblem 
(to compute left and right subtree) is a half of initial problem `b = 2`, 
and all this happens in a constant time `d = 0`.
That means that $$\log_b(a) > d$$ and hence we're dealing with 
[case 1](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)#Case_1_example)
that means $$\mathcal{O}(N^{\log_b(a)}) = \mathcal{O}(N)$$ time complexity.

* Space complexity : $$\mathcal{O}(N)$$, since we store the entire tree.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My Accepted Java Solution
- Author: jiaming2
- Creation Date: Tue Sep 30 2014 14:16:58 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 12:48:27 GMT+0800 (Singapore Standard Time)

<p>
Hi guys, this is my Java solution. I read this [post][1], which is very helpful.

The basic idea is here:
Say we have 2 arrays, PRE and IN.
Preorder traversing implies that PRE[0] is the root node.
Then we can find this PRE[0] in IN, say it's IN[5].
Now we know that IN[5] is root, so we know that IN[0] - IN[4] is on the left side, IN[6] to the end is on the right side.
Recursively doing this on subarrays, we can build a tree out of it :)

Hope this helps.

    public TreeNode buildTree(int[] preorder, int[] inorder) {
        return helper(0, 0, inorder.length - 1, preorder, inorder);
    }
    
    public TreeNode helper(int preStart, int inStart, int inEnd, int[] preorder, int[] inorder) {
        if (preStart > preorder.length - 1 || inStart > inEnd) {
            return null;
        }
        TreeNode root = new TreeNode(preorder[preStart]);
        int inIndex = 0; // Index of current root in inorder
        for (int i = inStart; i <= inEnd; i++) {
            if (inorder[i] == root.val) {
                inIndex = i;
            }
        }
        root.left = helper(preStart + 1, inStart, inIndex - 1, preorder, inorder);
        root.right = helper(preStart + inIndex - inStart + 1, inIndex + 1, inEnd, preorder, inorder);
        return root;
    }


  [1]: http://leetcode.com/2011/04/construct-binary-tree-from-inorder-and-preorder-postorder-traversal.html
</p>


### Python short recursive solution.
- Author: OldCodingFarmer
- Creation Date: Fri Aug 14 2015 01:05:04 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 12:47:23 GMT+0800 (Singapore Standard Time)

<p>
        
    def buildTree(self, preorder, inorder):
        if inorder:
            ind = inorder.index(preorder.pop(0))
            root = TreeNode(inorder[ind])
            root.left = self.buildTree(preorder, inorder[0:ind])
            root.right = self.buildTree(preorder, inorder[ind+1:])
            return root
</p>


### Simple O(n) without map
- Author: StefanPochmann
- Creation Date: Sun Jun 14 2015 22:39:43 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 05:08:22 GMT+0800 (Singapore Standard Time)

<p>
**Javascript solution:**

    var buildTree = function(preorder, inorder) {
        p = i = 0
        build = function(stop) {
            if (inorder[i] != stop) {
                var root = new TreeNode(preorder[p++])
                root.left = build(root.val)
                i++
                root.right = build(stop)
                return root
            }
            return null
        }
        return build()
    };

**Python solution:**

    def buildTree(self, preorder, inorder):
        def build(stop):
            if inorder and inorder[-1] != stop:
                root = TreeNode(preorder.pop())
                root.left = build(root.val)
                inorder.pop()
                root.right = build(stop)
                return root
        preorder.reverse()
        inorder.reverse()
        return build(None)

Note: See [MissMary's answer](https://leetcode.com/discuss/40381/simple-o-n-without-map?show=44432#a44432) for a possible improvement and my thoughts about it.

---

**Explanation/Discussion:**

Consider this input:

    preorder: [1, 2, 4, 5, 3, 6]
    inorder: [4, 2, 5, 1, 6, 3]

The obvious way to build the tree is:

 1. Use the first element of `preorder`, the `1`, as root.
 2. Search it in `inorder`.
 3. Split `inorder` by it, here into `[4, 2, 5]` and `[6, 3]`.
 4. Split the rest of  `preorder` into two parts as large as the `inorder` parts, here into `[2, 4, 5]` and `[3, 6]`.
 5. Use `preorder = [2, 4, 5]` and `inorder = [4, 2, 5]` to add the left subtree.
 6. Use `preorder = `[3, 6]` and `inorder = `[6, 3]` to add the right subtree.

But consider the worst case for this: A tree that's not balanced but is just a straight line to the left. Then inorder is the reverse of preorder, and already the cost of step 2, searching in `inorder`, is O(n^2) overall. Also, depending on how you "split" the arrays, you're looking at O(n^2) runtime and possibly O(n^2) space for that as well.

You can bring the runtime for searching down to O(n) by building a map from value to index before you start the main work, and I've seen several solutions do that. But that is O(n) additional space, and also the splitting problems remain. To fix those, you can use pointers into `preorder` and `inorder` instead of splitting them. And when you're doing that, you don't need the value-to-index map, either.

Consider the example again. Instead of finding the `1` in `inorder`, splitting the arrays into parts and recursing on them, just recurse on the full remaining arrays and **stop** when you come across the `1` in `inorder`. That's what my above solution does. Each recursive call gets told where to stop, and it tells its subcalls where to stop. It gives its own root value as stopper to its left subcall and its parent`s stopper as stopper to its right subcall.

**Language details:**

Small trick in my Javascript solution: The top-most call doesn't explicitly get a stopper value, so its `stop` is `undefined`. Which is good, because that's also what `inorder[i]` is when we have consumed all values, i.e., when `i` is `inorder.length`.

About the Python solution: I'm not sure there's a good way to have those `p` and `i` pointers that I use in my Javascript solution, so instead I opted for popping elements from `preorder` and `inorder`. Since popping from the front with `pop(0)` is expensive O(n), I reverse them before I start so I can use the cheap O(1) popping from the back.
</p>


