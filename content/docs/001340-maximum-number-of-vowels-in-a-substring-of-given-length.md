---
title: "Maximum Number of Vowels in a Substring of Given Length"
weight: 1340
#id: "maximum-number-of-vowels-in-a-substring-of-given-length"
---
## Description
<div class="description">
<p>Given a string <code>s</code> and an integer <code>k</code>.</p>

<p>Return <em>the maximum number of vowel letters</em> in any substring of <code>s</code>&nbsp;with&nbsp;length <code>k</code>.</p>

<p><strong>Vowel letters</strong> in&nbsp;English are&nbsp;(a, e, i, o, u).</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abciiidef&quot;, k = 3
<strong>Output:</strong> 3
<strong>Explanation:</strong> The substring &quot;iii&quot; contains 3 vowel letters.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aeiou&quot;, k = 2
<strong>Output:</strong> 2
<strong>Explanation:</strong> Any substring of length 2 contains 2 vowels.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;leetcode&quot;, k = 3
<strong>Output:</strong> 2
<strong>Explanation:</strong> &quot;lee&quot;, &quot;eet&quot; and &quot;ode&quot; contain 2 vowels.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;rhythms&quot;, k = 4
<strong>Output:</strong> 0
<strong>Explanation:</strong> We can see that s doesn&#39;t have any vowel letters.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;tryhard&quot;, k = 4
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 10^5</code></li>
	<li><code>s</code>&nbsp;consists of lowercase English letters.</li>
	<li><code>1 &lt;= k &lt;= s.length</code></li>
</ul>
</div>

## Tags
- String (string)
- Sliding Window (sliding-window)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] Slide Window O(n) codes.
- Author: rock
- Creation Date: Sun May 24 2020 12:24:23 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 26 2020 02:32:42 GMT+0800 (Singapore Standard Time)

<p>


```java
    public int maxVowels(String s, int k) {
        int ans = 0;
        // Set<Character> vowels = new HashSet<>(Arrays.asList(\'a\', \'e\', \'i\', \'o\', \'u\'));
        var vowels = Set.of(\'a\', \'e\', \'i\', \'o\', \'u\'); // Java 11 Collection factory method, credit to @Sithis
        for (int i = 0, winCnt = 0; i < s.length(); ++i) {
            if (vowels.contains(s.charAt(i))) {
                ++winCnt; 
            }
            if (i >= k && vowels.contains(s.charAt(i - k))) {
                --winCnt;
            }
            ans = Math.max(winCnt, ans);
        }
        return ans;
    }
```

```python
    def maxVowels(self, s: str, k: int) -> int:
        vowels = {\'a\', \'e\', \'i\', \'o\', \'u\'}
        ans = cnt = 0
        for i, c in enumerate(s):
            if c in vowels:
                cnt += 1
            if i >= k and s[i - k] in vowels:
                cnt -= 1
            ans  = max(cnt, ans)
        return ans    
```
</p>


### C++ Sliding Window
- Author: votrubac
- Creation Date: Sun May 24 2020 12:03:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 24 2020 14:03:04 GMT+0800 (Singapore Standard Time)

<p>
Just a simple sliding window pattern: count vowels moving in and out of window, and keep track of the maximum.

```cpp
int vowels[26] = {1,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,1};
int maxVowels(string s, int k) {
    int max_vow = 0;
    for (auto i = 0, cur_vow = 0; i < s.size(); ++i) {
        cur_vow += vowels[s[i] - \'a\'];
        if (i >= k)
            cur_vow -= vowels[s[i - k] - \'a\'];
        max_vow = max(max_vow, cur_vow);
    }
    return max_vow;
}
```

**Complexity Analysis**
- Time: O(n), we do through the string exactly once.
- Memory: O(1).
</p>


### [C++/Py3] Explained w/ Diagram - Sliding Window
- Author: luctivud
- Creation Date: Sun May 24 2020 12:10:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 24 2020 16:43:28 GMT+0800 (Singapore Standard Time)

<p>
# **Idea and Approach**
The basic idea to solve this kind of problem is to create a window of the given size and keep on moving it till the end while maintaining the count of the vowels we *gain* and *lose* in this transition.

Below is the implementation and steps explained through diagram:

* Traverse through all the elements of the first window and keep track of the vowels.
![image](https://assets.leetcode.com/users/luctivud/image_1590301154.png)


* Move the window to the right side without changing the size.
* Decrement the count in currentCount if any vowel is removed from the left side during transition
* Increase if vowel is added from the right side.
![image](https://assets.leetcode.com/users/luctivud/image_1590301197.png)


* The answer will be the maximum of all the counts encountered while moving the window.
![image](https://assets.leetcode.com/users/luctivud/image_1590301178.png)


# **Python 3 Code**

```
class Solution:
    def maxVowels(self, s: str, k: int) -> int:
        # Maximum vowels i.e. ans
        ans: int = 0
            
        # Vowels in current window
        currCount: int = 0
            
        # String of vowels
        vowels: str = "aeiou"
            
        # Using sliding window technique to 
        # calculate number of vowels in each window and 
        # update the count
        for i, v in enumerate(s):
            if i >= k:
                if s[i-k] in vowels:
                    currCount -= 1
            if s[i] in vowels:
                currCount += 1
            ans = max(currCount, ans)
        return ans
```

# **C++ Code**
```
class Solution {
public:
    int maxVowels(string s, int k) {
        
        // Store current count of vowels in currCount
        // and maximum currCount occurred in ans
        int currCount = 0;
        int ans = 0;
        
        // An array to mark all the vowels\' position to true
        // for checking whether the char is vowel or not
        bool vowels[26] = {false};
        vowels[0] = vowels[4] = vowels[8] = vowels[14] = vowels[20] = true;
        
        // Sliding window implementation
        for (int i=0; i < s.length(); i++) {
            
            // if any vowel is removed from left-> decrement
            if (i >= k and vowels[s[i-k]-\'a\']) {
                currCount -= 1;
            }
            
            // if any vowel is inserted from right-> increment
            if (vowels[s[i]-\'a\']) {
                currCount += 1;
            }
            
            // Store maximum occurence of currCount in ans
            ans = max (ans, currCount);
        }
        
        return ans ;
    }
};
```
# **Complexity Analysis**:
Time Complexity: **O(n)**
Space : O(1)

*Plz Upvote if you like it.*
</p>


