---
title: "Campus Bikes II"
weight: 976
#id: "campus-bikes-ii"
---
## Description
<div class="description">
<p>On a campus represented as a 2D grid, there are <code>N</code> workers and <code>M</code> bikes, with <code>N &lt;= M</code>. Each worker and bike is a 2D coordinate on this grid.</p>

<p>We assign one unique bike to each worker so that the sum of the Manhattan distances between each worker and their assigned bike is minimized.</p>

<p>The Manhattan distance between two points <code>p1</code> and <code>p2</code> is <code>Manhattan(p1, p2) = |p1.x - p2.x| + |p1.y - p2.y|</code>.</p>

<p>Return <em>the minimum possible sum of Manhattan distances between each worker and their assigned bike</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/03/06/1261_example_1_v2.png" style="width: 376px; height: 366px;" />
<pre>
<strong>Input:</strong> workers = [[0,0],[2,1]], bikes = [[1,2],[3,3]]
<strong>Output:</strong> 6
<strong>Explanation:</strong> 
We assign bike 0 to worker 0, bike 1 to worker 1. The Manhattan distance of both assignments is 3, so the output is 6.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/03/06/1261_example_2_v2.png" style="width: 376px; height: 366px;" />
<pre>
<strong>Input:</strong> workers = [[0,0],[1,1],[2,0]], bikes = [[1,0],[2,2],[2,1]]
<strong>Output:</strong> 4
<strong>Explanation: </strong>
We first assign bike 0 to worker 0, then assign bike 1 to worker 1 or worker 2, bike 2 to worker 2 or worker 1. Both assignments lead to sum of the Manhattan distances as 4.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> workers = [[0,0],[1,0],[2,0],[3,0],[4,0]], bikes = [[0,999],[1,999],[2,999],[3,999],[4,999]]
<strong>Output:</strong> 4995
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>N == workers.length</code></li>
	<li><code>M == bikes.length</code></li>
	<li><code>1 &lt;= N &lt;= M &lt;= 10</code></li>
	<li><code>workers[i].length == 2</code></li>
	<li><code>bikes[i].length == 2</code></li>
	<li><code>0 &lt;= workers[i][0], workers[i][1], bikes[i][0], bikes[i][1] &lt; 1000</code></li>
	<li>All the workers and the bikes locations are <strong>unique</strong>.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Backtracking (backtracking)

## Companies
- Google - 2 (taggedByAdmin: true)
- Amazon - 4 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Priority Queue
- Author: lee215
- Creation Date: Sun Jun 02 2019 01:31:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 01:31:35 GMT+0800 (Singapore Standard Time)

<p>
**Python:**
```
    def assignBikes(self, workers, bikes):
        def dis(i, j):
            return abs(workers[i][0] - bikes[j][0]) + abs(workers[i][1] - bikes[j][1])
        h = [[0, 0, 0]]
        seen = set()
        while True:
            cost, i, taken = heapq.heappop(h)
            if (i, taken) in seen: continue
            seen.add((i, taken))
            if i == len(workers):
                return cost
            for j in xrange(len(bikes)):
                if taken & (1 << j) == 0:
                    heapq.heappush(h, [cost + dis(i, j), i + 1, taken | (1 << j)])
```

</p>


### DFS + Pruning And DP Solution
- Author: shuoguoyangguang
- Creation Date: Tue Jun 04 2019 11:52:20 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 04 2019 11:52:20 GMT+0800 (Singapore Standard Time)

<p>
The DFS solution is pretty straight forward, try assign each bike to each worker. 
  Time Complexy: O(n * m !), n  is  number of workers, m is number of bikes
  
Ususally, when input size <= 10, O(n!) can be accepeted. When input size <= 12, we probably need do some pruning. if the test case is not strong, or problem designer wants to allow this techonolgy (dfs + pruning) to pass. we can luckly get a AC.(For my experenice in LeetCode, when problem is tagged as Medium, this kind solution can be passed)

For this problem, we add a very simple but effective condition:
```
	if (distance > min) return ;
```
```
	int min = Integer.MAX_VALUE;
    public int assignBikes(int[][] workers, int[][] bikes) {
        dfs(new boolean[bikes.length], workers, 0, bikes, 0);
        return min;
    }
    public void dfs(boolean[] visit, int[][] workers, int i, int[][] bikes, int distance) {
        if (i >= workers.length) {
            min = Math.min(distance, min);
            return ;
        }
        if (distance > min) {
            return ;
        }
        for (int j = 0; j < bikes.length; j++) {
            if (visit[j]) {
                continue;
            }
            visit[j] = true;
            dfs(visit, workers, i + 1, bikes, distance + dis(bikes[j], workers[i]));
            visit[j] = false;
        }
        
    }
    public int dis(int[] p1, int[] p2) {
        return Math.abs(p1[0] - p2[0]) + Math.abs(p1[1] - p2[1]);
    }
```

Actually the Brute Force Solution is a Permutation Problem\'s solution.  
One possible optimization is transfering Permutation Problem to Combination Problem(subset problem )\uFF0Cfrom **n!** to **2^n**. In Leetcode, I think when n < 15 , it can be accepted.

Assuming we have 3 bikes, 3 workers, and [ (b1,w1), (b2,w2),(b3,w3)] is our solution. In the Above DFS solution, we have caculated [(b2,w2),(b1,w1),(b3,w3)], [(b1,w1),(b3,w3),(b2,w2)] and so on. The distance of them are exactly same, However we only need one. In another word, we only need to know the distance of set, not list :{(b1,w1),(b2,w2),(b3,w3)}, now the problem change from permutation problem to combination. 

As long as we assign b3 to w3, b2 to w2, b1 to w1, we are good.  The assigning order is userless. 
For ith worker, the min distance = ith worker  uses jth bike + min distance all i - 1 workers to use i -1  others bike from m. so this is dp problem .

Here we use bit to represent jth bike is used or not
state : dp[i][s] = the min distance for first i workers to build the state s ,
transit: dp[i][s] = min(dp[i][s], dp[i - 1][prev] + dis(w[i -1], bike[j)) | 0 < j <m, prev = s ^ (1 << j)
init:dp[0][0] = 0; 
result: dp[n][s] s should have n bit

the code should be easy to understand.
```
  public int assignBikes(int[][] workers, int[][] bikes) {
        int n = workers.length;
        int m = bikes.length;
        int[][] dp = new int[n + 1][1 << m];
        for (int[] d : dp) {
            Arrays.fill(d, Integer.MAX_VALUE / 2);
        }
        dp[0][0] = 0;
        int min = Integer.MAX_VALUE;
        for (int i = 1; i <= n; i++) {
            for (int s = 1; s < (1 << m); s++) {
                for (int j = 0; j < m; j++) {
                    if ((s & (1 << j)) == 0) {
                        continue;
                    }
                    int prev = s ^ (1 << j);
                    dp[i][s] = Math.min(dp[i - 1][prev] + dis(workers[i - 1], bikes[j]), dp[i][s]) ;
                    if (i == n) {
                        min = Math.min(min, dp[i][s]);
                    }
                }
            }
        }
        return min;
    }
  
    public int dis(int[] p1, int[] p2) {
        return Math.abs(p1[0] - p2[0]) + Math.abs(p1[1] - p2[1]);
    }
```
Honestly, All the words are my thinking processing and code is my first accepted submission, so not perfect

I think this problem may have a existing algorithm, But I don\'t know.
</p>


### Step by Step solution from 400ms to 1ms (beating 100%)
- Author: qwerjkl112
- Creation Date: Sat Aug 17 2019 02:12:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 17 2019 02:16:06 GMT+0800 (Singapore Standard Time)

<p>
After spending some time going through some great answers in the discussion, I present a little summary of my own attempt and optimization. 

When solving this question naiively, you can see it is backtracking question. Take every path possible to find the shortest combined distance for all pairs of bikes and workes 
```
class Solution {
    private int max;
    public int assignBikes(int[][] workers, int[][] bikes) {
        max = Integer.MAX_VALUE;
        dfs(workers, bikes, new boolean[bikes.length], 0, 0);
        return max;
        
    }
    
    private void dfs(int[][] workers, int[][] bikes, boolean[] visited, int sum, int count) {
        if (count >= workers.length) {
            if (max > sum) max = sum;
            return;
        }
        
        for (int i = 0; i < bikes.length; i++) {
            if (!visited[i]) {
                visited[i] = true;
                dfs(workers, bikes, visited, sum + dist(workers[count], bikes[i]), count+1);
                visited[i] = false;
            }
        }
    }
    
    private int dist(int[] worker, int[] bike) {
        return Math.abs(worker[0] - bike[0]) + Math.abs(worker[1] - bike[1]);
    }
    
}
```

Looking at the complexity of this algorithm, we try to identify the points for optimization. 
Lets say x is a worker, y is bike
for 4 workers and 4 bikes, we expect the algorithm to go through paths like

x0y0, x1y1, **x2y2, x3y3**
x0y0, x1y1, **x2y3, x3y2**
x0y0, x1y2, x2y1, x3y3
x0y0, x1y2, x2y3, x3y1
x0y0, x1y3, x2y1, x3y2
x0y0, x1y3, x2y2, x3y2

x0y1, x1y0, **x2y2, x3y3**
x0y1, x1y0, **x2y3, x3y2**
x0y1, x1y2, x2y0, x3y1
x0y1, x1y2, x2y1, x3y0
...

Notice the bolded paths here repeats themself. We can prevent calculating this path again with a memoization technique. 
We can reword the pattern above as:

x0yn, x1yn, x2y2, x3y3
x0yn, x1yn, x2y3, x3y2

Since the nature of this problem asks for the shortest distance, so whenever we encounter the above 2 possible paths, we will always return it as **Math.min(f(x2y2, x3y3), f(x2y3 ,x3y2))**

Now the quesiton becomes how do we store Math.min(f(x2y2, x3y3), f(x2y3 ,x3y2))
Since the dfs function iterates through each worker from 0-nth, we can map them to an boolean array of boolean[workers.length]
Meaning if we ever see a **boolean array** of **{[true], [true], [false], [false]}** it will always refer to {x0yn, x1yn, x2y2, x3y3} or {x0yn, x1yn, x2y3, x3y2}. 
Converting this to a key by using a bitmasking technique to convert the boolean array into a bit array that results in **an integer**. 

```
class Solution {
    private int max;
    private Map<Integer, Integer> map;
    public int assignBikes(int[][] workers, int[][] bikes) {
        max = Integer.MAX_VALUE;
        map = new HashMap<>();
        int res = dfs(workers, bikes, new boolean[bikes.length], 0);
        return res;
    }
    
    private int dfs(int[][] workers, int[][] bikes, boolean[] visited, int count) {
        if (count == workers.length) return 0;
        int key = createKey(visited);
        if (map.containsKey(key)) return map.get(key);
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < bikes.length; i++) {
            if (!visited[i]) {
                visited[i] = true;
                int val = dist(workers[count], bikes[i]) + dfs(workers, bikes, visited, count+1);
                if (val < min) {
                    min = val;
                }
                visited[i] = false;
            }
        }
        
        map.put(key, min);
        return min;
    }
    
    private int dist(int[] worker, int[] bike) {
        return Math.abs(worker[0] - bike[0]) + Math.abs(worker[1] - bike[1]);
    }
    
    private int createKey(boolean[] visited) {
        int key = 0;
        for (int i = 0; i < visited.length; i++) {
            if (!visited[i]) {
                key |= 1 << i;
            }
        }
        return key;
    }
}
```

This reduces the runtime to about 12ms. 
I saw some great answers in the discussion and decided to further simplify my code by using a integer from the beginning to capture the status of my unused bikes. 
```
class Solution {
    public int assignBikes(int[][] workers, int[][] bikes) {
        int[] dp = new int[1 << bikes.length];
        int res = dfs(workers, bikes, 0, 0, dp);
        return res;
    }
    
    private int dfs(int[][] workers, int[][] bikes, int used, int count, int[] dp) {
        if (count == workers.length) return 0;
        if (dp[used] > 0) return dp[used];
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < bikes.length; i++) {
            if ((used & (1 << i)) == 0) {
                used |= (1 << i); //set i th bit
                min = Math.min(min, dist(workers[count], bikes[i]) + dfs(workers, bikes, used, count+1, dp));
                used &= ~(1 << i); //unset i th bit
            }
            
        }
        return dp[used] = min;
    }
```

Which ultimately reduced it to 1ms

There is also another great answer by the great wizard himself (@lee215) which I recommend checking out
https://leetcode.com/problems/campus-bikes-ii/discuss/303422/Python-Priority-Queue Using a dijstra algorithm
</p>


