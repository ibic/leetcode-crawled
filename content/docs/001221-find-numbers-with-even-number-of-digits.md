---
title: "Find Numbers with Even Number of Digits"
weight: 1221
#id: "find-numbers-with-even-number-of-digits"
---
## Description
<div class="description">
Given an array <code>nums</code> of integers, return how many of them contain an <strong>even number</strong> of digits.
<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [12,345,2,6,7896]
<strong>Output:</strong> 2
<strong>Explanation: 
</strong>12 contains 2 digits (even number of digits).&nbsp;
345 contains 3 digits (odd number of digits).&nbsp;
2 contains 1 digit (odd number of digits).&nbsp;
6 contains 1 digit (odd number of digits).&nbsp;
7896 contains 4 digits (even number of digits).&nbsp;
Therefore only 12 and 7896 contain an even number of digits.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [555,901,482,1771]
<strong>Output:</strong> 1 
<strong>Explanation: </strong>
Only 1771 contains an even number of digits.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 500</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Quora - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### JAVA solution with 100% better space and Time
- Author: adarshverma1901
- Creation Date: Tue Dec 24 2019 13:08:57 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 24 2019 13:08:57 GMT+0800 (Singapore Standard Time)

<p>
Very Simple 
10-99 ---- EVEN digit
1000-9999 --- EVEN digit
```
class Solution {
    public int findNumbers(int[] nums) {
        
        int count=0;
        
        for(int i =0 ; i< nums.length; i++){
            
            if((nums[i]>9 && nums[i]<100) || (nums[i]>999 && nums[i]<10000)){
                count++;
            }
        }
        
        return count;
        
    }
}
```

</p>


### C++ solution 100% runtime
- Author: huankimtran
- Creation Date: Fri Dec 27 2019 06:37:03 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jan 31 2020 11:42:43 GMT+0800 (Singapore Standard Time)

<p>
Only numbers between 10 and 99 or 1000 and 9999  or 100000 have even number of digits.
Taking advantage of nums[i] <= 10^5
```C++
class Solution {
public:
    int findNumbers(vector<int>& nums) {
        int n,count=0;
        for(int i=0;i<nums.size();i++)
        {
            n=nums[i];
            if(( 10<=n && n<=99) || (1000<=n && n<=9999 ) || ( n==100000 ))
            {
               count++;
            }
        }
        return count;
    }
};
</p>


### [java/Python 3] 1 liners.
- Author: rock
- Creation Date: Sun Dec 22 2019 12:03:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jan 25 2020 17:42:32 GMT+0800 (Singapore Standard Time)

<p>
```java
    public int findNumbers(int[] nums) {
        int cnt = 0;
        for (int n : nums)
            cnt += 1 - Integer.toString(n).length() % 2;
        return cnt;
    }
```
```java
    public int findNumbers(int[] nums) {
        return Arrays.stream(nums).map(i -> 1 - Integer.toString(i).length() % 2).sum();
    }
```
```python
    def findNumbers(self, nums: List[int]) -> int:
        return sum(len(str(n)) % 2 == 0 for n in nums) 
```
Thanks for **@apoorvwatsky**\'s contribution for the following method and **@ManuelP** suggests use `log10(n)`  instead of `log(n, 10)`:
```python
    def findNumbers(self, nums: List[int]) -> int:
        return sum(int(math.log10(n)) % 2 for n in nums) # log10(n) + 1 is the # of digits.
```
**Note: credit to @ManuelP**
log10(n) is more accurate than log(n, 10), for more details, please refer to [math.log(x, 10) gives different result than math.log10(x)](https://bugs.python.org/issue3724). Both have only limited accuracy, as @ManuelP provides tests as follows:
```
>>> for e in range(30):
	for x in range(10**e-1, 10**e+2):
		if x == 0: continue
		a = len(str(x))
		b = int(log10(x)) + 1
		if a != b:
			print(x, a, b)

999999999999999 15 16
9999999999999999 16 17
99999999999999999 17 18
999999999999999999 18 19
9999999999999999999 19 20
99999999999999999999 20 21
999999999999999999999 21 22
9999999999999999999999 22 23
99999999999999999999999 23 24
999999999999999999999999 24 25
9999999999999999999999999 25 26
99999999999999999999999999 26 27
999999999999999999999999999 27 28
9999999999999999999999999999 28 29
99999999999999999999999999999 29 30
```
</p>


