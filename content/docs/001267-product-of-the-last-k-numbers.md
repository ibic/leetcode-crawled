---
title: "Product of the Last K Numbers"
weight: 1267
#id: "product-of-the-last-k-numbers"
---
## Description
<div class="description">
<p>Implement the class <code>ProductOfNumbers</code>&nbsp;that supports two methods:</p>

<p>1.<code>&nbsp;add(int num)</code></p>

<ul>
	<li>Adds the number <code>num</code> to the back of the current list of numbers.</li>
</ul>

<p>2.<code> getProduct(int k)</code></p>

<ul>
	<li>Returns the product of the last <code>k</code> numbers in the current list.</li>
	<li>You can assume that always the current list has <strong>at least</strong> <code>k</code> numbers.</li>
</ul>

<p>At any time, the product of any contiguous sequence of numbers will fit into a single 32-bit integer without overflowing.</p>

<p>&nbsp;</p>
<p><strong>Example:</strong></p>

<pre>
<strong>Input</strong>
[&quot;ProductOfNumbers&quot;,&quot;add&quot;,&quot;add&quot;,&quot;add&quot;,&quot;add&quot;,&quot;add&quot;,&quot;getProduct&quot;,&quot;getProduct&quot;,&quot;getProduct&quot;,&quot;add&quot;,&quot;getProduct&quot;]
[[],[3],[0],[2],[5],[4],[2],[3],[4],[8],[2]]

<strong>Output</strong>
[null,null,null,null,null,null,20,40,0,null,32]

<strong>Explanation</strong>
ProductOfNumbers productOfNumbers = new ProductOfNumbers();
productOfNumbers.add(3);        // [3]
productOfNumbers.add(0);        // [3,0]
productOfNumbers.add(2);        // [3,0,2]
productOfNumbers.add(5);        // [3,0,2,5]
productOfNumbers.add(4);        // [3,0,2,5,4]
productOfNumbers.getProduct(2); // return 20. The product of the last 2 numbers is 5 * 4 = 20
productOfNumbers.getProduct(3); // return 40. The product of the last 3 numbers is 2 * 5 * 4 = 40
productOfNumbers.getProduct(4); // return 0. The product of the last 4 numbers is 0 * 2 * 5 * 4 = 0
productOfNumbers.add(8);        // [3,0,2,5,4,8]
productOfNumbers.getProduct(2); // return 32. The product of the last 2 numbers is 4 * 8 = 32 
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>There will be at most <code>40000</code>&nbsp;operations considering both <code>add</code> and <code>getProduct</code>.</li>
	<li><code>0 &lt;= num&nbsp;&lt;=&nbsp;100</code></li>
	<li><code>1 &lt;= k &lt;= 40000</code></li>
</ul>

</div>

## Tags
- Array (array)
- Design (design)

## Companies
- Google - 2 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Prefix Product
- Author: lee215
- Creation Date: Sun Feb 16 2020 12:04:19 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 03 2020 14:41:53 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
Similar to prefix sum. We can record the prefix product.
<br>

# **Explanation**
If we meet 0, the product including this 0 will always be 0.
We only need to record the prefix product after it.
So I clear the `A` and reinitilise it as `[1]`,
where 1 is the neutral element of multiplication.
<br>

# **Complexity**
Time `O(1)` each
Space `O(N)`
<br>

**Java:**
```java
    ArrayList<Integer> A = new ArrayList(){{
        add(1);
    }};

    public void add(int a) {
        if (a > 0)
            A.add(A.get(A.size() - 1) * a);
        else {
            A = new ArrayList();
            A.add(1);
        }
    }

    public int getProduct(int k) {
        int n = A.size();
        return k < n ? A.get(n - 1) / A.get(n - k - 1)  : 0;
    }
```

**C++:**
```cpp
    vector<int> A = {1};
    void add(int a) {
        if (a)
            A.push_back(A.back() * a);
        else
            A = {1};
    }

    int getProduct(int k) {
        return k < A.size() ? A.back() / A[A.size() - k - 1]  : 0;
    }
```

**Python:**
```python
    def __init__(self):
        self.A = [1]

    def add(self, a):
        if a == 0:
            self.A = [1]
        else:
            self.A.append(self.A[-1] * a)

    def getProduct(self, k):
        if k >= len(self.A): return 0
        return self.A[-1] / self.A[-k - 1]
```
</p>


### C++ Prefix Array
- Author: votrubac
- Creation Date: Sun Feb 16 2020 12:04:34 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 06 2020 11:47:09 GMT+0800 (Singapore Standard Time)

<p>
We maintain the runnig product in the prefix array. To get the product for the last `k` elements, we divide the last running product by the product that was `k - 1` steps ago. 

Important observation: if we add zero, we reset the prefix array. All products that includes zero will be zero. So, we can just check if `k` is greater than the size of our previx array, and return zero in that case.

```CPP
vector<long long> pr = { 1 };
void add(int num) {
    if (num == 0) pr = { 1 };
    else pr.push_back(pr.back() * num);
}
int getProduct(int k) {
    if (k >= pr.size()) return 0;
    return pr.back() / pr[pr.size() - k - 1];
}
```
</p>


### [Java] Maintain a Prefix Product (Handle the case when Element is 0)
- Author: manrajsingh007
- Creation Date: Sun Feb 16 2020 12:01:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 16 2020 12:01:07 GMT+0800 (Singapore Standard Time)

<p>
```
class ProductOfNumbers {
    static List<Integer> prod;
    static int p;
    public ProductOfNumbers() {
        prod = new ArrayList<>();
        p = 1;
    }
    public void add(int num) {
        if(num == 0) {
            prod = new ArrayList<>();
            p = 1;
            return;
        }
        p *= num;
        prod.add(p);
    }
    public int getProduct(int k) {
        if(prod.size() < k) return 0;
        int ans = prod.get(prod.size() - 1);
        if(prod.size() == k) return ans;
        return (ans / prod.get(prod.size() - 1 - k));
    }
}
</p>


