---
title: "Design Hit Counter"
weight: 345
#id: "design-hit-counter"
---
## Description
<div class="description">
<p>Design a hit counter which counts the number of hits received in the past 5 minutes.</p>

<p>Each function accepts a timestamp parameter (in seconds granularity) and you may assume that calls are being made to the system in chronological order (ie, the timestamp is monotonically increasing). You may assume that the earliest timestamp starts at 1.</p>

<p>It is possible that several hits arrive roughly at the same time.</p>

<p><b>Example:</b></p>

<pre>
HitCounter counter = new HitCounter();

// hit at timestamp 1.
counter.hit(1);

// hit at timestamp 2.
counter.hit(2);

// hit at timestamp 3.
counter.hit(3);

// get hits at timestamp 4, should return 3.
counter.getHits(4);

// hit at timestamp 300.
counter.hit(300);

// get hits at timestamp 300, should return 4.
counter.getHits(300);

// get hits at timestamp 301, should return 3.
counter.getHits(301); 
</pre>

<p><b>Follow up:</b><br />
What if the number of hits per second could be very large? Does your design scale?</p>
</div>

## Tags
- Design (design)

## Companies
- Amazon - 7 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: true)
- Atlassian - 4 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Netflix - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Pinterest - 5 (taggedByAdmin: false)
- Quip (Salesforce) - 3 (taggedByAdmin: false)
- Indeed - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Dropbox - 2 (taggedByAdmin: true)
- Affirm - 2 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Booking.com - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Super easy design O(1) hit()  O(s) getHits() no fancy data structure is needed!
- Author: xuyirui
- Creation Date: Tue Jun 21 2016 10:27:31 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 03:21:32 GMT+0800 (Singapore Standard Time)

<p>
O(s) s is total seconds in given time interval, in this case 300.
basic ideal is using buckets. 1 bucket for every second because we only need to keep the recent hits info for 300 seconds.  hit[] array is wrapped around by mod operation. Each hit bucket is associated with times[] bucket which record current time. If it is not current time, it means it is 300s or 600s... ago and need to reset to 1.




    public class HitCounter {
        private int[] times;
        private int[] hits;
        /** Initialize your data structure here. */
        public HitCounter() {
            times = new int[300];
            hits = new int[300];
        }
        
        /** Record a hit.
            @param timestamp - The current timestamp (in seconds granularity). */
        public void hit(int timestamp) {
            int index = timestamp % 300;
            if (times[index] != timestamp) {
                times[index] = timestamp;
                hits[index] = 1;
            } else {
                hits[index]++;
            }
        }
        
        /** Return the number of hits in the past 5 minutes.
            @param timestamp - The current timestamp (in seconds granularity). */
        public int getHits(int timestamp) {
            int total = 0;
            for (int i = 0; i < 300; i++) {
                if (timestamp - times[i] < 300) {
                    total += hits[i];
                }
            }
            return total;
        }
    }
</p>


### Simple Java Solution with explanation
- Author: cyqz1993
- Creation Date: Tue Jun 21 2016 09:47:46 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 04:47:14 GMT+0800 (Singapore Standard Time)

<p>
 In this problem, I use a queue to record the information of all the hits. Each time we call the function getHits( ), we have to delete the elements which hits beyond 5 mins (300). The result would be the length of the queue : )

  

    public class HitCounter {
            Queue<Integer> q = null;
            /** Initialize your data structure here. */
            public HitCounter() {
                q = new LinkedList<Integer>();
            }
            
            /** Record a hit.
                @param timestamp - The current timestamp (in seconds granularity). */
            public void hit(int timestamp) {
                q.offer(timestamp);
            }
            
            /** Return the number of hits in the past 5 minutes.
                @param timestamp - The current timestamp (in seconds granularity). */
            public int getHits(int timestamp) {
                while(!q.isEmpty() && timestamp - q.peek() >= 300) {
                    q.poll();
                }
                return q.size();
            }
        }
</p>


### Python solution with detailed explanation
- Author: gabbu
- Creation Date: Sun Jan 29 2017 06:25:34 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 24 2018 06:39:27 GMT+0800 (Singapore Standard Time)

<p>
**Solutions**

**Design Hit Counter** https://leetcode.com/problems/design-hit-counter/

**Deque Based Solution**
* Use deque as container. Deque stores the value of the timestamp.
* hit: append the timestamp to the deque. O(1)
* get_hit: O(N). During get_hit, pop out all elements from the left of the queue which do not serve any purpose and are stale.Return the length of the deque.
* How do we find the elenments which are stale? Condition we want to use:* timestamp-t >= 300*
* Example: timestamp = 301 and t = 1. Valid Range: [1 to 300], [2 to 301], [3,303]. So 301-1 >= 300. Hence 1 should be popped since it doesnt belong to range 2 to 301.
* Poor scalability of the solution since we store all timestamps.
```
from collections import deque
class HitCounter(object):
    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.counter = deque()

    def hit(self, timestamp):
        """
        Record a hit.
        @param timestamp - The current timestamp (in seconds granularity).
        :type timestamp: int
        :rtype: void
        """
        self.counter.append(timestamp)

    def getHits(self, timestamp):
        """
        Return the number of hits in the past 5 minutes.
        @param timestamp - The current timestamp (in seconds granularity).
        :type timestamp: int
        :rtype: int
        """
        while self.counter and timestamp -self.counter[0] >= 300:
            self.counter.popleft()
        return len(self.counter)
```

**Dictionary based Solution**
* The above solution doesnt scale since we add an element to the deque for every hit. 
* Use a dictionary instead and prune the dictionary. 
* Atleast in the dictionary solution, same timestamps will not be repeated. But the dictionary can grow unbounded.

```
from collections import defaultdict
class HitCounter(object):
    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.counter = defaultdict(int)

    def get(self, timestamp):
        return self.prune(timestamp)       

    def hit(self, timestamp):
        """
        Record a hit.
        @param timestamp - The current timestamp (in seconds granularity).
        :type timestamp: int
        :rtype: void
        """
        self.counter[timestamp] = self.counter[timestamp] + 1

    def getHits(self, timestamp):
        """
        Return the number of hits in the past 5 minutes.
        @param timestamp - The current timestamp (in seconds granularity).
        :type timestamp: int
        :rtype: int
        """
        cnt = 0
        for k in self.counter.keys():
            if timestamp - k >= 300:
                del self.counter[k]
            else:
                cnt = cnt + self.counter[k] 
        return cnt
```
**Optimized and Scalable Solution**
* The third solution creates an array of 300 elements. Every element of the array comprises of [frequency, timestamp].
* Timestamp 1 maps to index 0. Timestamp 100 maps to index 99.
* Use modulo mathematics to update it. hit: O(1). get_hit: O(300). This solution will scale perfectly!

```
class HitCounter(object):
    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.counter = [[0,i+1] for i in range(300)]
        return

    def hit(self, timestamp):
        """
        Record a hit.
        @param timestamp - The current timestamp (in seconds granularity).
        :type timestamp: int
        :rtype: void
        """
        # ts = 301 means (301-1)%300
        idx = int((timestamp - 1)%300)
        if self.counter[idx][1] == timestamp:
            self.counter[idx][0] += 1
        else:
            self.counter[idx][0] = 1            
            self.counter[idx][1] = timestamp            

    def getHits(self, timestamp):
        """
        Return the number of hits in the past 5 minutes.
        @param timestamp - The current timestamp (in seconds granularity).
        :type timestamp: int
        :rtype: int
        """
        cnt = 0
        for x in self.counter:
            c,t = x[0],x[1]
            if timestamp - t < 300:
                cnt += c
        return cnt
```

https://discuss.leetcode.com/topic/48752/simple-java-solution-with-explanation/9
</p>


