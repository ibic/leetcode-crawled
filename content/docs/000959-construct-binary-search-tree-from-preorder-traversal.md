---
title: "Construct Binary Search Tree from Preorder Traversal"
weight: 959
#id: "construct-binary-search-tree-from-preorder-traversal"
---
## Description
<div class="description">
<p>Return the root node of a binary <strong>search</strong> tree that matches the given <code>preorder</code> traversal.</p>

<p><em>(Recall that a binary search tree&nbsp;is a binary tree where for every <font face="monospace">node</font>, any descendant of <code>node.left</code> has a value <code>&lt;</code>&nbsp;<code>node.val</code>, and any descendant of <code>node.right</code> has a value <code>&gt;</code>&nbsp;<code>node.val</code>.&nbsp; Also recall that a preorder traversal&nbsp;displays the value of the&nbsp;<code>node</code> first, then traverses <code>node.left</code>, then traverses <code>node.right</code>.)</em></p>

<p>It&#39;s guaranteed that for the given test cases there is always possible to find a binary search tree with the given requirements.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[8,5,1,7,10,12]</span>
<strong>Output: </strong><span id="example-output-1">[8,5,10,1,7,null,12]
<img alt="" src="https://assets.leetcode.com/uploads/2019/03/06/1266.png" style="height: 200px; width: 306px;" /></span>
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= preorder.length &lt;= 100</code></li>
	<li><code>1 &lt;= preorder[i]&nbsp;&lt;= 10^8</code></li>
	<li>The values of <code>preorder</code> are distinct.</li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- Amazon - 5 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Construct binary tree from preorder and inorder traversal

**Intuition**

This approach is not the optimal one because of $$\mathcal{O}(N \log N)$$ time complexity,
but very straightforward. 

Let's use here two facts:

- [Binary tree could be constructed from preorder and inorder traversal](https://leetcode.com/articles/construct-binary-tree-from-preorder-and-inorder-tr/).

- [Inorder traversal of BST is an array sorted in the ascending order](https://leetcode.com/articles/delete-node-in-a-bst/).

**Algorithm**

- Construct inorder traversal by sorting the preorder array.

- [Construct binary tree from preorder and inorder traversal](https://leetcode.com/articles/construct-binary-tree-from-preorder-and-inorder-tr/):
the idea is to peek the elements one by one from the preorder array and
try to put them as a left or as a right child if it's possible. 
If it's impossible - just put `null` as a child and proceed further.
The possibility to use an element as a child is checked by an inorder array: 
if it contains no elements for this subtree, then the element couldn't be used here,
and one should use `null` as a child instead.

**Implementation**

![bla](../Figures/1008/preorder_inorder.png)

<iframe src="https://leetcode.com/playground/aWQrkasi/shared" frameBorder="0" width="100%" height="500" name="aWQrkasi"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N \log N)$$. $$\mathcal{O}(N \log N)$$ to sort preorder array
and $$\mathcal{O}(N)$$ to construct the binary tree.
* Space complexity : $$\mathcal{O}(N)$$ the inorder traversal and the tree.

<br />
<br />


---
#### Approach 2: Recursion

**Intuition**

It's quite obvious that the best possible time complexity for this problem 
is $$\mathcal{O}(N)$$ and hence the approach 1 is not the best one.
 
Basically, the inorder traversal above was used only to check if the element
could be placed in this subtree. 
Since one deals with a BST here, this could be verified with the help of lower and 
upper limits for each element as for the [validate BST problem](https://leetcode.com/articles/validate-binary-search-tree/).
This way there is no need in inorder traversal and the time
complexity is $$\mathcal{O}(N)$$.

**Algorithm**

- Initiate the lower and upper limits as negative and positive infinity because
one could always place the root.

- Start from the first element in the preorder array `idx = 0`.

- Return `helper(lower, upper)`:

    - If the preorder array is used up `idx = n` then the tree is
    constructed, return null.
    
    - If current value `val = preorder[idx]` is smaller then lower limit,
    or larger than upper limit, return null.
    
    - If the current value is in the limits, place it here `root = 
    TreeNode(val)`
    and proceed to construct recursively left and right subtrees:
    `root.left = helper(lower, val)` and `root.right = helper(val, upper)`.

    - Return `root`.

**Implementation**

![bla](../Figures/1008/recursion2.png)

<iframe src="https://leetcode.com/playground/Fbh9XLNQ/shared" frameBorder="0" width="100%" height="500" name="Fbh9XLNQ"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since we visit each node exactly once. 
* Space complexity : $$\mathcal{O}(N)$$ to keep the entire tree.
<br />
<br />


---
#### Approach 3: Iteration

**Algorithm**

The recursion above could be converted into the iteration 
with the help of stack.

- Pick the first preorder element as a root `root = new TreeNode(preorder[0])`
and push it into stack.

- Use `for` loop to iterate along the elements of preorder array :

    - Pick the last element of the stack as a parent node, and the 
    the current element of preorder as a child node.
    
    - Adjust the parent node : pop out of stack all elements
    with the value smaller than the child value.
    Change the parent node at each pop `node = stack.pop()`.
    
    - If `node.val < child.val` - put the child as a right 
    child of the node : `node.right = child`.
    
    - Else - as a left child : `node.left = child`.
    
    - Push child node into the stack.
    
- Return `root`.


**Implementation**

!?!../Documents/1008_LIS.json:1000,440!?!

<iframe src="https://leetcode.com/playground/8YhoVg3D/shared" frameBorder="0" width="100%" height="500" name="8YhoVg3D"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since we visit each node exactly once.
 
* Space complexity : $$\mathcal{O}(N)$$ to keep the stack and the tree.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] O(N) Solution
- Author: lee215
- Creation Date: Sun Mar 10 2019 12:03:30 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 25 2020 11:11:10 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**:
Find the left part and right part,
then recursively construct the tree.
<br>

# **Solution 1**:
Binary search

**Python, `O(N^2)`**
```py
    def bstFromPreorder(self, A):
        if not A: return None
        root = TreeNode(A[0])
        i = bisect.bisect(A, A[0])
        root.left = self.bstFromPreorder(A[1:i])
        root.right = self.bstFromPreorder(A[i:])
        return root
```

**Python, `O(NlogN)`**
```py
    def bstFromPreorder(self, A):
        def helper(i, j):
            if i == j: return None
            root = TreeNode(A[i])
            mid = bisect.bisect(A, A[i], i + 1, j)
            root.left = helper(i + 1, mid)
            root.right = helper(mid, j)
            return root
        return helper(0, len(A))
```
<br>


# **Solution 2**

Give the function a bound the maximum number it will handle.
The left recursion will take the elements smaller than `node.val`
The right recursion will take the remaining elements smaller than `bound`


**Complexity**
`bstFromPreorder` is called exactly `N` times.
It\'s same as a preorder traversal.
Time `O(N)`
Space `O(H)`

**Java**
```java
    int i = 0;
    public TreeNode bstFromPreorder(int[] A) {
        return bstFromPreorder(A, Integer.MAX_VALUE);
    }

    public TreeNode bstFromPreorder(int[] A, int bound) {
        if (i == A.length || A[i] > bound) return null;
        TreeNode root = new TreeNode(A[i++]);
        root.left = bstFromPreorder(A, root.val);
        root.right = bstFromPreorder(A, bound);
        return root;
    }
```
**C++**
```cpp
    int i = 0;
    TreeNode* bstFromPreorder(vector<int>& A, int bound = INT_MAX) {
        if (i == A.size() || A[i] > bound) return NULL;
        TreeNode* root = new TreeNode(A[i++]);
        root->left = bstFromPreorder(A, root->val);
        root->right = bstFromPreorder(A, bound);
        return root;
    }
```
**Python**
```python
    i = 0
    def bstFromPreorder(self, A, bound=float(\'inf\')):
        if self.i == len(A) or A[self.i] > bound:
            return None
        root = TreeNode(A[self.i])
        self.i += 1
        root.left = self.bstFromPreorder(A, root.val)
        root.right = self.bstFromPreorder(A, bound)
        return root
```
<br>

# Solution 2.1
Some may don\'t like the global variable `i`.
Well, I first reused the function in python,
so I had to use it, making it a "stateful" function.

I didn\'t realize there would be people who care about it.
If it\'s really matters,
We can discard the usage of global function.

**C++**
```cpp
    TreeNode* bstFromPreorder(vector<int>& A) {
        int i = 0;
        return build(A, i, INT_MAX);
    }

    TreeNode* build(vector<int>& A, int& i, int bound) {
        if (i == A.size() || A[i] > bound) return NULL;
        TreeNode* root = new TreeNode(A[i++]);
        root->left = build(A, i, root->val);
        root->right = build(A, i, bound);
        return root;
    }
```
**Java**
```java
    public TreeNode bstFromPreorder(int[] A) {
        return bstFromPreorder(A, Integer.MAX_VALUE, new int[]{0});
    }

    public TreeNode bstFromPreorder(int[] A, int bound, int[] i) {
        if (i[0] == A.length || A[i[0]] > bound) return null;
        TreeNode root = new TreeNode(A[i[0]++]);
        root.left = bstFromPreorder(A, root.val, i);
        root.right = bstFromPreorder(A, bound, i);
        return root;
    }
```
**Python**
```python
    def bstFromPreorder(self, A):
        return self.buildTree(A[::-1], float(\'inf\'))

    def buildTree(self, A, bound):
        if not A or A[-1] > bound: return None
        node = TreeNode(A.pop())
        node.left = self.buildTree(A, node.val)
        node.right = self.buildTree(A, bound)
        return node
```
</p>


### Python stack solution beats 100% on runtime and memory
- Author: cenkay
- Creation Date: Sun Mar 10 2019 23:09:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 10 2019 23:09:58 GMT+0800 (Singapore Standard Time)

<p>
* Idea is simple.
* First item in preorder list is the root to be considered.
* For next item in preorder list, there are 2 cases to consider:
	* If value is less than last item in stack, it is the left child of last item.
	* If value is greater than last item in stack, pop it.
		* The last popped item will be the parent and the item will be the right child of the parent.
```
class Solution:
    def bstFromPreorder(self, preorder: List[int]) -> TreeNode:
        root = TreeNode(preorder[0])
        stack = [root]
        for value in preorder[1:]:
            if value < stack[-1].val:
                stack[-1].left = TreeNode(value)
                stack.append(stack[-1].left)
            else:
                while stack and stack[-1].val < value:
                    last = stack.pop()
                last.right = TreeNode(value)
                stack.append(last.right)
        return root
```
</p>


### Java Stack Iterative Solution
- Author: Aoi-silent
- Creation Date: Mon Mar 11 2019 00:37:18 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 11 2019 00:37:18 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public TreeNode bstFromPreorder(int[] preorder) {
        if (preorder == null || preorder.length == 0) {
            return null;
        }
        Stack<TreeNode> stack = new Stack<>();
        TreeNode root = new TreeNode(preorder[0]);
        stack.push(root);
        for (int i = 1; i < preorder.length; i++) {
            TreeNode node = new TreeNode(preorder[i]);
            if (preorder[i] < stack.peek().val) {                
                stack.peek().left = node;                
            } else {
                TreeNode parent = stack.peek();
                while (!stack.isEmpty() && preorder[i] > stack.peek().val) {
                    parent = stack.pop();
                }
                parent.right = node;
            }
            stack.push(node);            
        }
        return root;
    }
}
```
</p>


