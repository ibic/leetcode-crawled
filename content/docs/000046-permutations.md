---
title: "Permutations"
weight: 46
#id: "permutations"
---
## Description
<div class="description">
<p>Given a collection of <strong>distinct</strong> integers, return all possible permutations.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> [1,2,3]
<strong>Output:</strong>
[
  [1,2,3],
  [1,3,2],
  [2,1,3],
  [2,3,1],
  [3,1,2],
  [3,2,1]
]
</pre>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- Amazon - 17 (taggedByAdmin: false)
- Facebook - 8 (taggedByAdmin: false)
- Microsoft - 7 (taggedByAdmin: true)
- Apple - 6 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- LinkedIn - 5 (taggedByAdmin: true)
- Oracle - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- GoDaddy - 2 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Garena - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Backtracking

[Backtracking](https://en.wikipedia.org/wiki/Backtracking) 
is an algorithm for finding all
solutions by exploring all potential candidates.
If the solution candidate turns to be _not_ a solution 
(or at least not the _last_ one), 
backtracking algorithm discards it by making some changes 
on the previous step, *i.e.* _backtracks_ and then try again.

Here is a backtrack function 
which takes the index of the first integer to consider 
as an argument `backtrack(first)`.

* If the first integer to consider has index `n` that means 
that the current permutation is done.
* Iterate over the integers from index `first` to index `n - 1`.
    * Place `i`-th integer first in the permutation, 
    i.e. `swap(nums[first], nums[i])`.
    * Proceed to create all permutations which starts from 
    `i`-th integer : `backtrack(first + 1)`.
    * Now backtrack, i.e. `swap(nums[first], nums[i])` back.
        
!?!../Documents/46_LIS.json:1000,548!?!

<iframe src="https://leetcode.com/playground/cCeKUsLu/shared" frameBorder="0" width="100%" height="500" name="cCeKUsLu"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(\sum_{k = 1}^{N}{P(N, k)})$$ where $$P(N, k) = \frac{N!}{(N - k)!} = N (N - 1) ... (N - k + 1)$$
is so-called [_k-permutations_of_n_, or _partial permutation_](https://en.wikipedia.org/wiki/Permutation#k-permutations_of_n). 

Here $$first + 1 = k$$ for the expression simplicity. 
The formula is easy to understand : for each $$k$$ (each $$first$$) 
one performs $$N(N - 1) ... (N - k + 1)$$ operations, 
and $$k$$ is going through the range of values from $$1$$ to $$N$$ (and $$first$$ from $$0$$ to $$N - 1$$). 

Let's do a rough estimation of the result : 
$$N! \le \sum_{k = 1}^{N}{\frac{N!}{(N - k)!}} = \sum_{k = 1}^{N}{P(N, k)} \le N \times N!$$,
i.e. the algorithm performs better than $$\mathcal{O}(N \times N!)$$ and 
a bit slower than $$\mathcal{O}(N!)$$.
 
* Space complexity : $$\mathcal{O}(N!)$$ since one has to keep
`N!` solutions.

## Accepted Submission (python)
```python
class Solution(object):
	def permute(self, nums):
		"""
		:type nums: List[int]
		:rtype: List[List[int]]
		"""
		r = []
		ln = len(nums)
		for i in range(ln):
			r.append([nums[i]])
		for i in range(ln - 1):
			nr = []
			for perm in r:
				rps = [e for e in nums if e not in perm]
				for rp in rps:
					onp = []
					for op in perm:
						onp.append(op)
					onp.append(rp)
					nr.append(onp)
			r = nr
		return r

```

## Top Discussions
### A general approach to backtracking questions in Java (Subsets, Permutations, Combination Sum, Palindrome Partioning)
- Author: issac3
- Creation Date: Mon May 23 2016 23:56:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 09 2019 09:40:57 GMT+0800 (Singapore Standard Time)

<p>
This structure might apply to many other backtracking questions, but here I am just going to demonstrate Subsets, Permutations, and Combination Sum.

Subsets : [https://leetcode.com/problems/subsets/][1]

    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, 0);
        return list;
    }
    
    private void backtrack(List<List<Integer>> list , List<Integer> tempList, int [] nums, int start){
        list.add(new ArrayList<>(tempList));
        for(int i = start; i < nums.length; i++){
            tempList.add(nums[i]);
            backtrack(list, tempList, nums, i + 1);
            tempList.remove(tempList.size() - 1);
        }
    }


Subsets II (contains duplicates) : [https://leetcode.com/problems/subsets-ii/][2]

    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, 0);
        return list;
    }
    
    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums, int start){
        list.add(new ArrayList<>(tempList));
        for(int i = start; i < nums.length; i++){
            if(i > start && nums[i] == nums[i-1]) continue; // skip duplicates
            tempList.add(nums[i]);
            backtrack(list, tempList, nums, i + 1);
            tempList.remove(tempList.size() - 1);
        }
    } 


----------

Permutations : [https://leetcode.com/problems/permutations/][3]

    public List<List<Integer>> permute(int[] nums) {
       List<List<Integer>> list = new ArrayList<>();
       // Arrays.sort(nums); // not necessary
       backtrack(list, new ArrayList<>(), nums);
       return list;
    }
    
    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums){
       if(tempList.size() == nums.length){
          list.add(new ArrayList<>(tempList));
       } else{
          for(int i = 0; i < nums.length; i++){ 
             if(tempList.contains(nums[i])) continue; // element already exists, skip
             tempList.add(nums[i]);
             backtrack(list, tempList, nums);
             tempList.remove(tempList.size() - 1);
          }
       }
    } 

Permutations II (contains duplicates) : [https://leetcode.com/problems/permutations-ii/][4]

    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, new boolean[nums.length]);
        return list;
    }
    
    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums, boolean [] used){
        if(tempList.size() == nums.length){
            list.add(new ArrayList<>(tempList));
        } else{
            for(int i = 0; i < nums.length; i++){
                if(used[i] || i > 0 && nums[i] == nums[i-1] && !used[i - 1]) continue;
                used[i] = true; 
                tempList.add(nums[i]);
                backtrack(list, tempList, nums, used);
                used[i] = false; 
                tempList.remove(tempList.size() - 1);
            }
        }
    }


----------

Combination Sum : [https://leetcode.com/problems/combination-sum/][5]

    public List<List<Integer>> combinationSum(int[] nums, int target) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, target, 0);
        return list;
    }
    
    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums, int remain, int start){
        if(remain < 0) return;
        else if(remain == 0) list.add(new ArrayList<>(tempList));
        else{ 
            for(int i = start; i < nums.length; i++){
                tempList.add(nums[i]);
                backtrack(list, tempList, nums, remain - nums[i], i); // not i + 1 because we can reuse same elements
                tempList.remove(tempList.size() - 1);
            }
        }
    }

Combination Sum II (can't reuse same element) : [https://leetcode.com/problems/combination-sum-ii/][6]

    public List<List<Integer>> combinationSum2(int[] nums, int target) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(list, new ArrayList<>(), nums, target, 0);
        return list;
        
    }
    
    private void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums, int remain, int start){
        if(remain < 0) return;
        else if(remain == 0) list.add(new ArrayList<>(tempList));
        else{
            for(int i = start; i < nums.length; i++){
                if(i > start && nums[i] == nums[i-1]) continue; // skip duplicates
                tempList.add(nums[i]);
                backtrack(list, tempList, nums, remain - nums[i], i + 1);
                tempList.remove(tempList.size() - 1); 
            }
        }
    } 


Palindrome Partitioning : [https://leetcode.com/problems/palindrome-partitioning/][7]

    public List<List<String>> partition(String s) {
       List<List<String>> list = new ArrayList<>();
       backtrack(list, new ArrayList<>(), s, 0);
       return list;
    }
    
    public void backtrack(List<List<String>> list, List<String> tempList, String s, int start){
       if(start == s.length())
          list.add(new ArrayList<>(tempList));
       else{
          for(int i = start; i < s.length(); i++){
             if(isPalindrome(s, start, i)){
                tempList.add(s.substring(start, i + 1));
                backtrack(list, tempList, s, i + 1);
                tempList.remove(tempList.size() - 1);
             }
          }
       }
    }
    
    public boolean isPalindrome(String s, int low, int high){
       while(low < high)
          if(s.charAt(low++) != s.charAt(high--)) return false;
       return true;
    } 



  [1]: https://leetcode.com/problems/subsets/
  [2]: https://leetcode.com/problems/subsets-ii/
  [3]: https://leetcode.com/problems/permutations/
  [4]: https://leetcode.com/problems/permutations-ii/
  [5]: https://leetcode.com/problems/combination-sum/
  [6]: https://leetcode.com/problems/combination-sum-ii/
  [7]: https://leetcode.com/problems/palindrome-partitioning/
</p>


### My elegant recursive C++ solution with inline explanation
- Author: xiaohui7
- Creation Date: Wed Dec 10 2014 23:49:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 08:07:00 GMT+0800 (Singapore Standard Time)

<p>
This recursive solution is the my first response for this problem. I was surprised when I found no similar solution posted here. It is much easier to understand than DFS-based ones, at least in my opinion. Please find more explanations [here][1]. All comments are welcome.

    class Solution {
    public:
        vector<vector<int> > permute(vector<int> &num) {
    	    vector<vector<int> > result;
    	    
    	    permuteRecursive(num, 0, result);
    	    return result;
        }
        
        // permute num[begin..end]
        // invariant: num[0..begin-1] have been fixed/permuted
    	void permuteRecursive(vector<int> &num, int begin, vector<vector<int> > &result)	{
    		if (begin >= num.size()) {
    		    // one permutation instance
    		    result.push_back(num);
    		    return;
    		}
    		
    		for (int i = begin; i < num.size(); i++) {
    		    swap(num[begin], num[i]);
    		    permuteRecursive(num, begin + 1, result);
    		    // reset
    		    swap(num[begin], num[i]);
    		}
        }
    };


  [1]: http://xiaohuiliucuriosity.blogspot.com/2014/12/permutations.html
</p>


### My AC simple iterative java/python solution
- Author: cbmbbz
- Creation Date: Fri Dec 26 2014 10:59:39 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 09:58:22 GMT+0800 (Singapore Standard Time)

<p>
the basic idea is, to permute n numbers, we can add the nth number into the resulting `List<List<Integer>>` from the n-1 numbers, in every possible position. 

For example, if the input num[] is {1,2,3}: First, add 1 into the initial `List<List<Integer>>` (let's call it "answer"). 

Then, 2 can be added in front or after 1. So we have to copy the List<Integer> in answer (it's just {1}), add 2 in position 0 of {1}, then copy the original {1} again, and add 2 in position 1. Now we have an answer of {{2,1},{1,2}}. There are 2 lists in the current answer.

Then we have to add 3. first copy {2,1} and {1,2}, add 3 in position 0; then copy {2,1} and {1,2}, and add 3 into position 1, then do the same thing for position 3. Finally we have 2*3=6 lists in answer, which is what we want.

    public List<List<Integer>> permute(int[] num) {
        List<List<Integer>> ans = new ArrayList<List<Integer>>();
        if (num.length ==0) return ans;
        List<Integer> l0 = new ArrayList<Integer>();
        l0.add(num[0]);
        ans.add(l0);
        for (int i = 1; i< num.length; ++i){
            List<List<Integer>> new_ans = new ArrayList<List<Integer>>(); 
            for (int j = 0; j<=i; ++j){            
               for (List<Integer> l : ans){
            	   List<Integer> new_l = new ArrayList<Integer>(l);
            	   new_l.add(j,num[i]);
            	   new_ans.add(new_l);
               }
            }
            ans = new_ans;
        }
        return ans;
    }

-------------------------------------------------------------------------
python version is more concise:

    def permute(self, nums):
        perms = [[]]   
        for n in nums:
            new_perms = []
            for perm in perms:
                for i in xrange(len(perm)+1):   
                    new_perms.append(perm[:i] + [n] + perm[i:])   ###insert n
            perms = new_perms
        return perms
</p>


