---
title: "Gray Code"
weight: 89
#id: "gray-code"
---
## Description
<div class="description">
<p>The gray code is a binary numeral system where two successive values differ in only one bit.</p>

<p>Given a non-negative integer <em>n</em> representing the total number of bits in the code, print the sequence of gray code. A gray code sequence must begin with 0.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong>&nbsp;2
<strong>Output:</strong>&nbsp;<code>[0,1,3,2]</code>
<strong>Explanation:</strong>
00 - 0
01 - 1
11 - 3
10 - 2

For a given&nbsp;<em>n</em>, a gray code sequence may not be uniquely defined.
For example, [0,2,3,1] is also a valid gray code sequence.

00 - 0
10 - 2
11 - 3
01 - 1
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong>&nbsp;0
<strong>Output:</strong>&nbsp;<code>[0]
<strong>Explanation:</strong> We define the gray code sequence to begin with 0.
&nbsp;            A gray code sequence of <em>n</em> has size = 2<sup>n</sup>, which for <em>n</em> = 0 the size is 2<sup>0</sup> = 1.
&nbsp;            Therefore, for <em>n</em> = 0 the gray code sequence is [0].</code>
</pre>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- Microsoft - 3 (taggedByAdmin: false)
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (python3)
```python3
class Solution:
    def genGrayCode(self, n):
        v = 0
        i = 0
        while (n >> i) > 0:
            bit = 0 if (n + 2**i) // 2**(i+1) % 2 == 0 else 1
            v += (bit << i)
            i += 1
        return v

    def grayCode(self, n):
        """
        :type n: int
        :rtype: List[int]
        """
        # metho 1. generative way
        # 0 1 0 1
        #--------
        # 1 2 2 2 -> 0th bit
        # 2 4 4 4 -> 1st bit
        # 4 8 8 8 -> 2nd bit
        # ...
        r = []
        for i in range(pow(2,n)):
            r.append(self.genGrayCode(i))
        return r
```

## Top Discussions
### Share my solution
- Author: yuyibestman
- Creation Date: Fri Aug 29 2014 10:41:58 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 06:31:35 GMT+0800 (Singapore Standard Time)

<p>
My idea is to generate the sequence iteratively. For example, when n=3, we can get the result based on n=2. 
00,01,11,10 -> (000,001,011,010 ) (110,111,101,100). The middle two numbers only differ at their highest bit, while the rest numbers of part two are exactly symmetric of part one. It is easy to see its correctness.
Code is simple:

----------

    public List<Integer> grayCode(int n) {
        List<Integer> rs=new ArrayList<Integer>();
        rs.add(0);
        for(int i=0;i<n;i++){
            int size=rs.size();
            for(int k=size-1;k>=0;k--)
                rs.add(rs.get(k) | 1<<i);
        }
        return rs;
    }
</p>


### An accepted three line solution in JAVA
- Author: jinrf
- Creation Date: Sun Feb 08 2015 02:37:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 10:04:04 GMT+0800 (Singapore Standard Time)

<p>
    public List<Integer> grayCode(int n) {
        List<Integer> result = new LinkedList<>();
        for (int i = 0; i < 1<<n; i++) result.add(i ^ i>>1);
        return result;
    }

The idea is simple. G(i) = i^ (i/2).
</p>


### One-liner Python solution (with demo in comments)
- Author: byronyi
- Creation Date: Sat Nov 08 2014 07:07:43 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 08:30:56 GMT+0800 (Singapore Standard Time)

<p>
All you need is a bit of careful thought.

Btw, it's extremely useful to write down your thought/demo in comments before you actually start to write the code, especially during interview. 

Even if you do not solve the problem finally, the interviewer at least get to know what you're thinking. 

And if you don't get the problem right, he/she will have a chance to correct you.

    class Solution:
        # @return a list of integers
        '''
        from up to down, then left to right
        
        0   1   11  110
                10  111
                    101
                    100
                    
        start:      [0]
        i = 0:      [0, 1]
        i = 1:      [0, 1, 3, 2]
        i = 2:      [0, 1, 3, 2, 6, 7, 5, 4]
        '''
        def grayCode(self, n):
            results = [0]
            for i in range(n):
                results += [x + pow(2, i) for x in reversed(results)]
            return results
</p>


