---
title: "Bank Account Summary"
weight: 1593
#id: "bank-account-summary"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Users</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| user_id      | int     |
| user_name    | varchar |
| credit       | int     |
+--------------+---------+
user_id is the primary key for this table.
Each row of this table contains the current credit information for each user.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Transactions</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| trans_id      | int     |
| paid_by       | int     |
| paid_to       | int     |
| amount        | int     |
| transacted_on | date    |
+---------------+---------+
trans_id is the primary key for this table.
Each row of this table contains the information about the transaction in the bank.
User with id (paid_by) transfer money to user with id (paid_to).
</pre>

<p>&nbsp;</p>

<p>Leetcode&nbsp;Bank (LCB) helps its coders in making virtual&nbsp;payments. Our bank records all transactions in the table <em>Transaction</em>, we want&nbsp;to find out the current balance of all users and check wheter they have breached their credit limit (If their current credit is less than 0).</p>

<p>Write an SQL query to report.</p>

<ul>
	<li><code>user_id</code></li>
	<li><code>user_name</code></li>
	<li><code>credit</code>, current balance after performing transactions.&nbsp;&nbsp;</li>
	<li><code>credit_limit_breached</code>, check&nbsp;credit_limit&nbsp;(&quot;Yes&quot; or &quot;No&quot;)</li>
</ul>

<p>Return the result table in <strong>any</strong> order.</p>

<p>The query result format is in the following example.</p>

<p>&nbsp;</p>

<pre>
<code>Users</code> table:
+------------+--------------+-------------+
| user_id    | user_name    | credit      |
+------------+--------------+-------------+
| 1          | Moustafa     | 100         |
| 2          | Jonathan     | 200         |
| 3          | Winston      | 10000       |
| 4          | Luis         | 800         | 
+------------+--------------+-------------+

<code>Transactions</code> table:
+------------+------------+------------+----------+---------------+
| trans_id   | paid_by    | paid_to    | amount   | transacted_on |
+------------+------------+------------+----------+---------------+
| 1          | 1          | 3          | 400      | 2020-08-01    |
| 2          | 3          | 2          | 500      | 2020-08-02    |
| 3          | 2          | 1          | 200      | 2020-08-03    |
+------------+------------+------------+----------+---------------+

Result table:
+------------+------------+------------+-----------------------+
| <code>user_id </code>   | <code>user_name</code>  | <code>credit </code>    | <code>credit_limit_breached</code> |
+------------+------------+------------+-----------------------+
| 1          | Moustafa   | -100       | Yes                   | 
| 2          | Jonathan   | 500        | No                    |
| 3          | Winston    | 9900       | No                    |
| 4          | Luis       | 800        | No                    |
+------------+------------+------------+-----------------------+
Moustafa paid $400 on &quot;2020-08-01&quot; and received $200 on &quot;2020-08-03&quot;, credit (100 -400 +200) = -$100
Jonathan received $500 on &quot;2020-08-02&quot; and paid $200 on &quot;2020-08-08&quot;, credit (200 +500 -200) = $500
Winston received $400 on &quot;2020-08-01&quot; and paid $500 on &quot;2020-08-03&quot;, credit (10000 +400 -500) = $9990
Luis didn&#39;t received any transfer, credit = $800</pre>

</div>

## Tags


## Companies
- Optum - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL easy-to-follow and faster than 100% neat solution
- Author: vwang0
- Creation Date: Fri Aug 21 2020 23:51:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 22 2020 07:14:57 GMT+0800 (Singapore Standard Time)

<p>
create an in-flow table and an out-flow table, and update credit as (old credit + in flow - out flow)

optimized solution:

```
SELECT user_id,
       user_name,
       (credit - IFNULL(out_cash, 0) + IFNULL(in_cash, 0)) AS credit,
       IF((credit - IFNULL(out_cash, 0) + IFNULL(in_cash, 0)) < 0, \'Yes\', \'No\') AS credit_limit_breached
FROM Users U
LEFT JOIN
    (SELECT paid_by,
            SUM(amount) AS out_cash
     FROM Transaction
     GROUP BY paid_by) out_tmp
ON U.user_id = out_tmp.paid_by
LEFT JOIN
    (SELECT paid_to,
            SUM(amount) AS in_cash
     FROM Transaction
     GROUP BY paid_to) in_tmp 
ON U.user_id = in_tmp.paid_to
```

2nd solution 

```
SELECT U.user_id,
       user_name,
       (credit - out_cash + in_cash) AS credit,
       IF((credit - out_cash + in_cash) < 0, \'Yes\', \'No\') AS credit_limit_breached
FROM Users U,

    (SELECT U1.user_id,
            IFNULL(SUM(amount), 0) AS out_cash
     FROM Users U1
     LEFT JOIN Transaction T1 ON U1.user_id = T1.paid_by
     GROUP BY user_id) out_tmp,

    (SELECT U2.user_id,
            IFNULL(SUM(amount), 0) AS in_cash
     FROM Users U2
     LEFT JOIN Transaction T2 ON U2.user_id = T2.paid_to
     GROUP BY user_id) in_tmp
WHERE U.user_id = out_tmp.user_id
    AND U.user_id = in_tmp.user_id
```

1st solution
```
SELECT U.user_id,
       user_name,
       (credit - out_cash + in_cash) AS credit,
       IF((credit - out_cash + in_cash) < 0, \'Yes\', \'No\') AS credit_limit_breached
FROM Users U
JOIN
    (SELECT U1.user_id,
            IFNULL(SUM(amount), 0) AS out_cash
     FROM Users U1
     LEFT JOIN Transaction T1 ON U1.user_id = T1.paid_by
     GROUP BY user_id) out_tmp ON U.user_id = out_tmp.user_id
JOIN
    (SELECT U2.user_id,
            IFNULL(SUM(amount), 0) AS in_cash
     FROM Users U2
     LEFT JOIN Transaction T2 ON U2.user_id = T2.paid_to
     GROUP BY user_id) in_tmp ON U.user_id = in_tmp.user_id
```

</p>


### MySQL solution using SUM and CASE WHEN without subquery
- Author: eminem18753
- Creation Date: Sat Aug 22 2020 01:09:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 13 2020 11:52:00 GMT+0800 (Singapore Standard Time)

<p>
```
# Write your MySQL query statement below
SELECT user_id,user_name,
IFNULL(SUM(CASE WHEN a.user_id=b.paid_by THEN -amount ELSE amount END),0)+a.credit as credit,
CASE WHEN IFNULL(SUM(CASE WHEN a.user_id=b.paid_by THEN -amount ELSE amount END),0)>=-a.credit THEN "No" ELSE "Yes" END as credit_limit_breached
FROM Users as a
LEFT JOIN Transactions as b
ON a.user_id=b.paid_by OR a.user_id=b.paid_to
GROUP BY a.user_id;
```
</p>


### MSSQL does not work
- Author: liurenjie
- Creation Date: Sat Aug 22 2020 23:22:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 05 2020 22:16:15 GMT+0800 (Singapore Standard Time)

<p>
**Leetcode has fixed the issue**

Table name is changed to Transactions, to avoid using keyword ``` Transaction``` .

Cheers.


***Original Post***
It seems the MSSQL is not working for this problem.

It will not run with below simple query.

`select * from [Transaction]`
 
I would expect the result to be "Wrong Answer", rather than "Runtime Error"

![image](https://assets.leetcode.com/users/images/43894763-5d3a-46b9-b961-7842f415dc87_1598109718.7922757.png)

Expected behavior from MySQL side -

![image](https://assets.leetcode.com/users/images/c5b0a333-f595-433a-87c5-8e8016325003_1598109739.1109982.png)

</p>


