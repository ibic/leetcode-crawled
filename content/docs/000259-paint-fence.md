---
title: "Paint Fence"
weight: 259
#id: "paint-fence"
---
## Description
<div class="description">
<p>There is a fence with n posts, each post can be painted with one of the k colors.</p>

<p>You have to paint all the posts such that no more than two adjacent fence posts have the same color.</p>

<p>Return the total number of ways you can paint the fence.</p>

<p><b>Note:</b><br />
n and k are non-negative integers.</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b> n = 3, k = 2
<b>Output:</b> 6
<strong>Explanation: </strong>Take c1 as color 1, c2 as color 2. All possible ways are:

&nbsp;           post1  post2  post3      
 -----      -----  -----  -----       
   1         c1     c1     c2 
&nbsp;  2         c1     c2     c1 
&nbsp;  3         c1     c2     c2 
&nbsp;  4         c2     c1     c1&nbsp; 
   5         c2     c1     c2
&nbsp;  6         c2     c2     c1
</pre>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 3 (taggedByAdmin: true)
- Expedia - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### The only solution you need to read
- Author: nm2812
- Creation Date: Sat Oct 06 2018 06:56:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 01 2019 10:14:33 GMT+0800 (Singapore Standard Time)

<p>
If you are painting the ith post, you have two options:
1. make it different color as i-1th post 
2. make it same color as i-1 th post (if you are allowed!)

simply add these for your answer:
`num_ways(i) = num_ways_diff(i) + num_ways_same(i)`

Now just think of how to calculate each of those functions.

The first one is easy. If you are painting the ith post, how many ways can you paint it to make it different from the i-1 th post? `k-1` 

`num_ways_diff(i) = num_ways(i-1) * (k-1)`

The second one is hard, but not so hard when you think about it.

If you are painting the ith post, how many ways can you paint it to make it the same as the i-1th post? At first, we should think the answer is 1 -- it must be whatever color the last one was.

`num_ways_same(i) = num_ways(i-1) * 1`

But no! This will fail in the cases where painting the last post the same results in three adjacent posts of the same color! We need to consider ONLY the cases where the last two colors were different. But we can do that!

`num_ways_diff(i-1)` <- all the cases where the i-1th and i-2th are different. 

THESE are the cases where can just plop the same color to the end, and no longer worry about causing three in a row to be the same.

`num_ways_same(i) = num_ways_diff(i-1) * 1`

We sum these for our answer, like I said before:

`num_ways(i) = num_ways_diff(i) + num_ways_same(i)` 
`= num_ways(i-1) * (k-1) + num_ways_diff(i-1)`

We know how to compute num_ways_diff, so we can substitute:
`num_ways(i) = num_ways(i-1) * (k-1) +  num_ways(i-2) * (k-1)`

We can even simplify a little more:

`num_ways(i) = (num_ways(i-1) +  num_ways(i-2)) * (k-1)`

As a note, trying to intuitively understand that last line is impossible. If you think you understand it intuitively, you are fooling yourself. Only the original equation makes intuitive sense.

Once you have this, the code is trivial (but overall, this problem is not an easy problem, despite the leetcode tag!):

```
class Solution {
    public int numWays(int n, int k) {
        // if there are no posts, there are no ways to paint them
        if (n == 0) return 0;
        
        // if there is only one post, there are k ways to paint it
        if (n == 1) return k;
        
        // if there are only two posts, you can\'t make a triplet, so you 
        // are free to paint however you want.
        // first post, k options. second post, k options
        if (n == 2) return k*k;
        
        int table[] = new int[n+1];
        table[0] = 0;
        table[1] = k;
        table[2] = k*k;
        for (int i = 3; i <= n; i++) {
            // the recursive formula that we derived
            table[i] = (table[i-1] + table[i-2]) * (k-1);
        }
        return table[n];
    }
}
```





</p>


### O(n) time java solution, O(1) space
- Author: JennyShaw
- Creation Date: Sat Sep 05 2015 04:53:58 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 07:01:17 GMT+0800 (Singapore Standard Time)

<p>
    public int numWays(int n, int k) {
        if(n == 0) return 0;
        else if(n == 1) return k;
        int diffColorCounts = k*(k-1);
        int sameColorCounts = k;
        for(int i=2; i<n; i++) {
            int temp = diffColorCounts;
            diffColorCounts = (diffColorCounts + sameColorCounts) * (k-1);
            sameColorCounts = temp;
        }
        return diffColorCounts + sameColorCounts;
    }

We divided it into two cases. 

1. the last two posts have the same color, the number of ways to paint in this case is *sameColorCounts*.

2. the last two posts have different colors, and the number of ways in this case is *diffColorCounts*.

The reason why we have these two cases is that we can easily compute both of them, and that is all I do. When adding a new post, we can use the same color as the last one (if allowed) or different color. If we use different color, there're *k-1* options, and the outcomes shoule belong to the *diffColorCounts* category. If we use same color, there's only one option, and we can only do this when the last two have different colors (which is the diffColorCounts). There we have our induction step.

Here is an example, let's say we have 3 posts and 3 colors. The first two posts we have 9 ways to do them, (1,1), (1,2), (1,3), (2,1), (2,2), (2,3), (3,1), (3,2), (3,3). Now we know that

    diffColorCounts = 6;
And

    sameColorCounts = 3;

Now for the third post, we can compute these two variables like this: 

If we use different colors than the last one (the second one), these ways can be added into *diffColorCounts*, so if the last one is 3, we can use 1 or 2, if it's 1, we can use 2 or 3, etc. Apparently there are `(diffColorCounts + sameColorCounts) * (k-1)` possible ways. 

If we use the same color as the last one, we would trigger a violation in these three cases (1,1,1), (2,2,2) and (3,3,3). This is because they already used the same color for the last two posts. So is there a count that rules out these kind of cases? YES, the *diffColorCounts*. So in cases within *diffColorCounts*, we can use the same color as the last one without worrying about triggering the violation. And now as we append a same-color post to them, the former *diffColorCounts* becomes the current *sameColorCounts*.

Then we can keep going until we reach the n. And finally just sum up these two variables as result.

Hope this would be clearer.
</p>


### Python solution with explanation
- Author: orbuluh
- Creation Date: Fri Sep 18 2015 14:11:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 06 2018 04:58:30 GMT+0800 (Singapore Standard Time)

<p>
If n == 1, there would be k-ways to paint.

if n == 2, there would be two situations:

 - 2.1 You paint same color with the previous post: k*1 ways to paint, named it as `same`
 - 2.2 You paint differently with the previous post: k*(k-1) ways to paint this way, named it as `dif`

So, you can think, if n >= 3, you can always maintain these two situations, 
`You either paint the same color with the previous one, or differently`.

Since there is a rule: "no more than two adjacent fence posts have the same color."

We can further analyze: 

 - from 2.1, since previous two are in the same color, next one you could only paint differently, and it would form one part of "paint differently" case in the n == 3 level, and the number of ways to paint this way would equal to `same*(k-1)`.
 - from 2.2, since previous two are not the same, you can either paint the same color this time (`dif*1`) ways to do so, or stick to paint differently (`dif*(k-1)`) times.

Here you can conclude, when seeing back from the next level, ways to paint the same, or variable `same` would equal to `dif*1 = dif`, and ways to paint differently, variable `dif`, would equal to `same*(k-1)+dif*(k-1) = (same + dif)*(k-1)`

So we could write the following codes:

            
        if n == 0:
            return 0
        if n == 1:
            return k
        same, dif = k, k*(k-1)
        for i in range(3, n+1):
            same, dif = dif, (same+dif)*(k-1)
        return same + dif
</p>


