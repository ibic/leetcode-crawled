---
title: "Managers with at Least 5 Direct Reports"
weight: 1484
#id: "managers-with-at-least-5-direct-reports"
---
## Description
<div class="description">
<p>The <code>Employee</code> table holds all employees including their managers. Every employee has an Id, and there is also a column for the manager Id.</p>

<pre>
+------+----------+-----------+----------+
|Id    |Name 	  |Department |ManagerId |
+------+----------+-----------+----------+
|101   |John 	  |A 	      |null      |
|102   |Dan 	  |A 	      |101       |
|103   |James 	  |A 	      |101       |
|104   |Amy 	  |A 	      |101       |
|105   |Anne 	  |A 	      |101       |
|106   |Ron 	  |B 	      |101       |
+------+----------+-----------+----------+
</pre>

<p>Given the <code>Employee</code> table, write a SQL query that finds out managers with at least 5 direct report. For the above table, your SQL query should return:</p>

<pre>
+-------+
| Name  |
+-------+
| John  |
+-------+
</pre>

<p><b>Note:</b><br />
No one would report to himself.</p>

</div>

## Tags


## Companies
- Amazon - 2 (taggedByAdmin: false)
- Bloomberg - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using `JOIN` and a temporary table [Accepted]

**Algorithm**

First, we can get the Id of the manager having more than 5 direct reports just using this *ManagerId* column.

Then, we can get the name of this manager by join that table with the **Employee** table.

**MySQL**

```sql
SELECT
    Name
FROM
    Employee AS t1 JOIN
    (SELECT
        ManagerId
    FROM
        Employee
    GROUP BY ManagerId
    HAVING COUNT(ManagerId) >= 5) AS t2
    ON t1.Id = t2.ManagerId
;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accepted and easy-to-understand solution
- Author: crystal_huangjin
- Creation Date: Mon Jul 03 2017 03:41:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 06:45:54 GMT+0800 (Singapore Standard Time)

<p>
```
select name from employee 
where id in 
(select managerId from Employee
group by managerId
having count(managerId)>=5) 
```
</p>


### MS SQL beats 100%
- Author: akhil1311
- Creation Date: Fri Jul 31 2020 21:56:51 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 31 2020 21:56:51 GMT+0800 (Singapore Standard Time)

<p>
```
/* Write your T-SQL query statement below */
select e.name
from employee m left join employee e
on m.managerid = e.id
group by e.name
having count(e.name) > 4
;
```
</p>


### My solution 91.62% fast
- Author: sheldon1990
- Creation Date: Thu Jan 04 2018 08:26:35 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jan 04 2018 08:26:35 GMT+0800 (Singapore Standard Time)

<p>
SELECT Name
FROM EMPLOYEE 
WHERE Id in (
             SELECT ManagerID
             FROM EMPLOYEE
             WHERE ManagerID IS NOT NULL
             GROUP BY ManagerId
             HAVING COUNT(ManagerID) >= 5
            );
</p>


