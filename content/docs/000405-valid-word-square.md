---
title: "Valid Word Square"
weight: 405
#id: "valid-word-square"
---
## Description
<div class="description">
<p>Given a sequence of words, check whether it forms a valid word square.</p>

<p>A sequence of words forms a valid word square if the <i>k</i><sup>th</sup> row and column read the exact same string, where 0 &le; <i>k</i> &lt; max(numRows, numColumns).</p>

<p><b>Note:</b><br />
<ol>
<li>The number of words given is at least 1 and does not exceed 500.</li>
<li>Word length will be at least 1 and does not exceed 500.</li>
<li>Each word contains only lowercase English alphabet <code>a-z</code>.</li>
</ol>
</p>

<p><b>Example 1:</b>
<pre>
<b>Input:</b>
[
  "abcd",
  "bnrt",
  "crmy",
  "dtye"
]

<b>Output:</b>
true

<b>Explanation:</b>
The first row and first column both read "abcd".
The second row and second column both read "bnrt".
The third row and third column both read "crmy".
The fourth row and fourth column both read "dtye".

Therefore, it is a valid word square.
</pre>
</p>

<p><b>Example 2:</b>
<pre>
<b>Input:</b>
[
  "abcd",
  "bnrt",
  "crm",
  "dt"
]

<b>Output:</b>
true

<b>Explanation:</b>
The first row and first column both read "abcd".
The second row and second column both read "bnrt".
The third row and third column both read "crm".
The fourth row and fourth column both read "dt".

Therefore, it is a valid word square.
</pre>
</p>

<p><b>Example 3:</b>
<pre>
<b>Input:</b>
[
  "ball",
  "area",
  "read",
  "lady"
]

<b>Output:</b>
false

<b>Explanation:</b>
The third row reads "read" while the third column reads "lead".

Therefore, it is <b>NOT</b> a valid word square.
</pre>
</p>
</div>

## Tags


## Companies
- Bloomberg - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java AC Solution Easy to Understand
- Author: star1993
- Creation Date: Sun Oct 16 2016 12:49:13 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 23:48:56 GMT+0800 (Singapore Standard Time)

<p>
Simply check for each row and columns, return false if not match.
```
public class Solution {
    public boolean validWordSquare(List<String> words) {
        if(words == null || words.size() == 0){
            return true;
        }
        int n = words.size();
        for(int i=0; i<n; i++){
            for(int j=0; j<words.get(i).length(); j++){
                if(j >= n || words.get(j).length() <= i || words.get(j).charAt(i) != words.get(i).charAt(j))
                    return false;
            }
        }
        return true;
    }
}
````
</p>


### 1-liner Python
- Author: StefanPochmann
- Creation Date: Sun Oct 16 2016 18:35:26 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 23 2018 10:50:40 GMT+0800 (Singapore Standard Time)

<p>
**Solution:**

    def validWordSquare(self, words):
        return map(None, *words) == map(None, *map(None, *words))

Or saving some work but taking two lines:

    def validWordSquare(self, words):
        t = map(None, *words)
        return t == map(None, *t)

**Explanation:**

The `map(None, ...)` transposes the "matrix", filling missing spots with `None`. For example:
```
["abc",           [('a', 'd', 'f'),
 "de",     =>      ('b', 'e', None),
 "f"]              ('c', None, None)]
```
And then I just need to check whether transposing it once more changes it.
</p>


### Only three false conditions: too short, too long, letter not equal
- Author: zhugejunwei
- Creation Date: Mon Nov 07 2016 00:03:34 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 23:48:13 GMT+0800 (Singapore Standard Time)

<p>
```
public boolean validWordSquare(List<String> words) {
        if (words.size() == 0 || words == null) return true;
        int n = words.size();
        for (int i = 0; i < n; i++) {
            String tmp = words.get(i);
            for (int j = 0; j < tmp.length(); j++) {
                // too long
                if (j >= n)
                    return false;
                // too short
                if (words.get(j).length() <= i)
                    return false;
                // letter not equal
                if (tmp.charAt(j) != words.get(j).charAt(i))
                    return false;
            }
        }
        return true;
    }
```
</p>


