---
title: "Find Lucky Integer in an Array"
weight: 1293
#id: "find-lucky-integer-in-an-array"
---
## Description
<div class="description">
<p>Given an array of integers <code>arr</code>, a lucky integer is an integer which has a frequency in the array equal to its value.</p>

<p>Return <i>a lucky integer</i>&nbsp;in the array. If there are multiple lucky integers return the <strong>largest</strong> of them. If there is no lucky&nbsp;integer return <strong>-1</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [2,2,3,4]
<strong>Output:</strong> 2
<strong>Explanation:</strong> The only lucky number in the array is 2 because frequency[2] == 2.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,2,3,3,3]
<strong>Output:</strong> 3
<strong>Explanation:</strong> 1, 2 and 3 are all lucky numbers, return the largest of them.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [2,2,2,3,3]
<strong>Output:</strong> -1
<strong>Explanation:</strong> There are no lucky numbers in the array.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arr = [5]
<strong>Output:</strong> -1
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> arr = [7,7,7,7,7,7,7]
<strong>Output:</strong> 7
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 500</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 500</code></li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Brute Force

**Intuition**

The simplest approach is to iterate through each number in the input array. Then for each number, iterate over the array again to count how many times that number occurs to determine whether or not it's a *lucky number*. We'll also need to keep track of the largest *lucky number* we've seen so far.

```text
define function find_lucky(arr):
    largest_lucky_number = -1
    for each num in arr:
        occurances_of_num = count_occurances(arr, num)
        if occurances_of_num equal to num AND num is larger than largest_lucky_number:
            largest_lucky_number = num
    return largest_lucky_number

define function count_occurances(arr, candidate_num):
    count = 0
    for each num in arr:
        if num equal to candidate_num:
            count = count + 1
    return count
```

Many programming languages have a built-in function that we can use instead of the `count_occurances(...)` function we've defined here. However, keep in mind that this operation will always be $$O(n)$$, regardless of whether you implemented it yourself or used a built-in function.

**Algorithm**

<iframe src="https://leetcode.com/playground/9uxtaDGw/shared" frameBorder="0" width="100%" height="412" name="9uxtaDGw"></iframe>


**Complexity Analysis**

Let $$n$$ be the the length of the input array.

- Time complexity : $$O(n^2)$$.

    For each of the $$n$$ numbers in the input array, we're counting how many times that number appears in the array. The count operation is $$O(n)$$, and so we get $$n \cdot O(n) = O(n^2)$$.

- Space complexity : $$O(1)$$.

    This approach only uses a constant number of variables to keep track of where we're up to. Therefore, it has a $$O(1)$$ space complexity.

<br/>

---

#### Approach 2: Sorting

**Intuition**

The crux of this problem is counting how many times each number occurs, so that we can then determine whether or not a particular number is lucky. It would be easier to do this if the array was sorted. So, how about we just sort the array and then go from there?

For example, if the input array was as follows:

```py
[2, 7, 8, 2, 1, 2, 3, 4, 5, 2, 2, 3, 4, 4, 7, 4, 3, 7, 8, 7, 7, 7]
```

After sorting, we would have the following:

```py
[1, 2, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 7, 7, 7, 7, 7, 7, 8, 8]
```

From here, we can now search for the largest lucky number in a single pass of the array. We need to step through the array, keeping track of the length of the current streak. Once we're on the last number of the current streak, we should check whether or not that number is lucky.

To avoid needing to keep track of the largest lucky number seen, we should work from right-to-left (largest to smallest), as then when we encounter a lucky number, we know it's the largest that could possibly be in the array. Even better, we could reverse-sort instead of forward-sort. The best option will depend on which programming language you're using.

```text
define function findLucky(arr):
    sort arr from largest to smallest
    current_streak = 0
    for each num in arr:
        current_streak = current_streak + 1
        if the next number in arr is different from num:
            if num equal to current_streak:
                return num
            current_streak = 0
    return -1
```

In practice, this approach will perform well, especially for a short input array length.

**Algorithm**

<iframe src="https://leetcode.com/playground/p5UJndXn/shared" frameBorder="0" width="100%" height="412" name="p5UJndXn"></iframe>

For those who are familiar with the `Heap` data-structure, there is a neat variant of this approach that will have the same time complexity, but a $$O(1)$$ space complexity. Here is that approach in Python. This could be done in other languages, but you'd need to confirm the "heapify" functionality really is $$O(1)$$ space, or implement heapify yourself.

<iframe src="https://leetcode.com/playground/kt2gWyCM/shared" frameBorder="0" width="100%" height="500" name="kt2gWyCM"></iframe>

**Complexity Analysis**

- Time complexity : $$O(n \, \log \, n)$$.

    This approach was split into 2 parts; sorting the array, and then searching for the largest lucky number in the sorted array.

    Sorting an array using a built-in sorting algorithm requires $$O(n \, \log \, n)$$ time. 

    Searching for the largest lucky number in the sorted array requires stepping through each of the $$n$$ elements in the array. Therefore, the cost of this part is $$O(n)$$.
    
    In big-oh notation, we drop insignificant added parts from the time complexity. Seeing as we know $$O(n \, \log \, n)$$ will always be larger than $$O(n)$$, we get a final time complexity of $$O(n + n \, \log \, n) = O(n \, \log \, n)$$. 

- Space complexity : Varies from $$O(n)$$ to $$O(1)$$, depending on sorting implementation.

    Most programming languages use a built-in sorting algorithm that requires $$O(n)$$ auxillary space. A few are $$O(\log \, n)$$ or even $$O(1)$$, but these seem to be the minority from what I've read.

    Searching for the largest lucky number in the sorted array requires only $$O(1)$$ additional space, and so can be ignored.

    The `Heap` variant of this approach has an $$O(1)$$ space complexity. Heapify requires only $$O(1)$$ extra space, as does removing and processing items from the heap.

<br/>

---

#### Approach 3: Counter

**Intuition**

If you were finding the largest lucky number of an array by hand, you would probably just make a tally chart.

![Table of sample check in times and stations](../Figures/1394/tally_chart.png)

We can do this in code, using a `HashMap` where the keys are the unique integers in the array, and the values are how many times each appeared. This is a very common usage for a `HashMap` in the programming interview world. Some programming languages have a special `HashMap` data structure specifically for this purpose, although Java does not. In the languages that have it, it might be called a `Counter`, `Bag`, or `MultiSet`.

Here is some pseudocode for making the `HashMap` of `counts.`

```text
counts = a new HashMap (number -> count)
for each num in arr:
    if num is not in counts:
        counts.put(num, 1)
    else:
        counts.put(num, counts.get(num) + 1)
```

This code can be written more concisely in Java, using the `getOrDefault(...)` method of `Map`. See the Algorithm section for details on how this works (and see how awesome it is!).

The rest of the code is straightforward; we simply need to check each key/ value entry in `counts`, and if they are equal, then we know that we've found a lucky number. We don't know whether or not it's the largest one though, because `HashMap` keys are *not sorted*. Therefore, we'll need to use the pattern we used in Approach 1 of keeping track of the largest we've seen so far, until we get to the end.

```text
largest_lucky_number = -1
for each number, count in counts.entries():
    if number equal to count AND number is larger than largest_lucky_number:
        largest_lucky_number = number
return largest_lucky_number
```

**Algorithm**

<iframe src="https://leetcode.com/playground/LwMgUV5y/shared" frameBorder="0" width="100%" height="412" name="LwMgUV5y"></iframe>

**Complexity Analysis**

- Time complexity : $$O(n)$$.

    There are two steps to this approach; building a `HashMap` of counts, and looking through the `HashMap` for the largest lucky number. 

    Inserting an item into a `HashMap` has a cost of $$O(1)$$. Therefore, inserting the $$n$$ items into the `HashMap` has a cost of $$n \cdot O(1) = O(n)$$. 

    Iterating over all the entries in a `HashMap` has a cost of $$O(m)$$, where $$m$$ is the number of entries in the `HashMap`. In the worst case, $$m = n$$ (all numbers were unique), and therefore the cost of this is $$O(n)$$.

    We get a final time complexity of $$O(n) + O(n) = O(n)$$.

- Space complexity : $$O(n)$$.

    In the worst case, all numbers in the `HashMap` are unique, and therefore it will take $$O(n)$$ space to store it.

Approach 2 and Approach 3 are both good approaches in practice, each with its own pros and cons. 

<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 3 Approaches (+2 Variations)
- Author: votrubac
- Creation Date: Tue Mar 31 2020 04:10:47 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Apr 03 2020 07:17:08 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**
We do not care about numbers greater than the size of the input array.

> Note: see approach 3A and 3B for a O(1) memory solution.

#### Approach 1: Counting Sort
We could use a hash map or an array (since the nuber will not exceed `n`) to count the occurences. Then go from the highest number down, and return the first one that matches our criteria.
```cpp
int findLucky(vector<int>& arr) {
    int m[501] = {};
    for (auto n : arr)
        ++m[n];
    for (auto n = arr.size(); n > 0; --n)
        if (n == m[n])
            return n;
    return -1;
}
```
**Complexity Analysis**
- Time: O(n)
- Memory: O(n)

#### Approach 2A: Sort
We can sort our array, and do a one-pass from largest element to the smallest. We return the first element which value equals it\'s count.

```cpp
int findLucky(vector<int>& arr) {
    sort(begin(arr), end(arr), greater<int>());
    int cnt = 1;
    for (int i = 1; i < arr.size(); ++i) {
        if (arr[i] == arr[i - 1])
            ++cnt;
        else {
            if (arr[i - 1] == cnt)
                return cnt;
            cnt = 1;
        }
    }
    return arr.back() == cnt ? cnt : - 1;
}
```
**Complexity Analysis**
- Time: O(n log n)
- Memory: O(log n), assuming we can modify the input array. Internally, a typical sort algorithm uses *log n* memory for the stack, but it can be greater than that based on the implementation.

#### Approach 2B: Heap
The benefit of a heap is that it does not sort elements upfront, but as you are popping them. When you find the lucky integer, you do not need to sort the rest of the heap.

C++ allows \'heapifying\' the existing array, so we do not need an extra space for a priority queue or something.

```cpp
int findLucky(vector<int>& arr) {
    make_heap(begin(arr), end(arr));
    int cnt = 1, last = -1;
    for (int i = 0; i < arr.size(); ++i) {
        if (arr.front() == last)
            ++cnt;
        else {
            if (last == cnt)
                return cnt;
            last = arr.front();
            cnt = 1;
        }
        pop_heap(arr.begin(), arr.end() - i);
    }
    return arr.front() == cnt ? cnt : - 1;    
}
```
**Complexity Analysis**
- Time: O(n log n) in the worst case, less if the lucky integer is large. 
- Memory: O(log n), assuming we can modify the input array. I am assuming that, internally, the `pop_heap` algorithm uses log n memory for the stack.

#### Approach 3A: Linked List (Beast Mode)
The idea is to use the input array to store counts. This approach, however, is quite tricky to implement correctly!

We can think of our array as a linked list, where `arr[i]` points to `arr[arr[i] - 1]` and so on, until the element that points to itself, or its outside of the array (and we do not care about that elements, per the intuition above).

After we visit `arr[arr[i] - 1]`, we can use that element to track the count of `arr[i]`. For the count, we will use a negative value to distinguish between pointers and counts.

```cpp
#define BEAST_MODE
```
```cpp
int findLucky(vector<int>& arr) {
    for (auto i = 0; i < arr.size(); ++i) {
        auto p = i, val = arr[i];
        while (val > 0 && val <= arr.size()) {
            auto n_val = arr[val - 1];
            arr[val - 1] = min(0, arr[val - 1]) - 1;
            if (val - 1 <= i || val - 1 == p)
                break;
            p = val - 1;
            val = n_val;
        }      
    }
    for (auto i = arr.size(); i > 0; --i)
        if (-arr[i - 1] == i)
            return i;
    return -1;
}
```
```cpp
#undef BEAST_MODE
```
These lines may need some explanation.
```cpp
            if (val - 1 <= i || val - 1 == p)
                break;
```
Here, we break when the element points to itself, or points to a node that we visited earlier.

**Complexity Analysis**
- Time: O(n)
- Memory: O(1), assuming we can modify the input array.

#### Approach 3B: Input Array
Here, we will also use the input array to store the count. Since values are limited, we can just use the upper part of the integer to store counts.

```cpp
int findLucky(vector<int>& arr) {
    for (auto n : arr) {
        n &= 0xFFFF;
        if (n <= arr.size())
            arr[n - 1] += 0x10000;
    }
    for (auto i = arr.size(); i > 0; --i)
        if ((arr[i - 1] >> 16) == i)
            return i;
    return -1;
}
```
**Complexity Analysis**
- Time: O(n)
- Memory: O(1); we can restore the input array to the original state, if needed.
</p>


### [Java/Python 3] Two similar clean codes: array and HashMap/Counter.
- Author: rock
- Creation Date: Sun Mar 29 2020 12:04:36 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 21 2020 13:06:09 GMT+0800 (Singapore Standard Time)

<p>
**Method 1: array** 
```java
    public int findLucky(int[] arr) {
        int[] cnt = new int[501];
        for (int a : arr) {
            ++cnt[a];
        }
        for (int i = 500; i > 0; --i) {
            if (cnt[i] == i) {
                return i;
            }
        }
        return -1;
    }
```
```python
    def findLucky(self, arr: List[int]) -> int:
        cnt = [0] * 501
        for a in arr:
            cnt[a] += 1
        for i in range(500, 0, -1):
            if cnt[i] == i:
                return i    
        return -1
```
or by **@chr1sC0d1ng**\'s suggestion of optimization:
```python
    def findLucky(self, arr: List[int]) -> int:
        n = len(arr)
        cnt = [0] * (n + 1)
        for a in arr:
            if a <= n:
                cnt[a] += 1
        for i in range(n, 0, -1):
            if cnt[i] == i:
                return i
        return -1
```

----

**Method 2**
```java
    public int findLucky(int[] arr) {
        Map<Integer, Integer> freq = new HashMap<>();
        for (int a : arr) {
            freq.put(a, 1 + freq.getOrDefault(a, 0)); // Accumulate the occurrence of a.
        }
        int ans = -1;
        for (Map.Entry<Integer, Integer> e : freq.entrySet()) {
            if (e.getKey() == e.getValue()) {
                ans = Math.max(ans, e.getKey());
            }
        }
        return ans;
    }
```
```python
    def findLucky(self, arr: List[int]) -> int:
        cnt = collections.Counter(arr)
        return max([k for k, v in cnt.items() if k == v] + [-1])
```
</p>


### Java clean O(n) solution with explanations
- Author: guytal
- Creation Date: Sun Mar 29 2020 13:26:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 29 2020 22:35:28 GMT+0800 (Singapore Standard Time)

<p>
```
public int findLucky(int[] arr) {
	Map<Integer, Integer> numToFrequency = new HashMap<>();
	for (int num : arr) { // calculate frequency for each number
		numToFrequency.put(num, numToFrequency.getOrDefault(num, 0) + 1);
	}
	int max = -1;
	for (int num : numToFrequency.keySet()) { // go over all of the numbers in the map
		if (num == numToFrequency.get(num)) { // number which has a frequency equal to its value
			max = Math.max(max, num); // keep the max value
		}
	}
	return max; // if no such number is found return -1
}
```
</p>


