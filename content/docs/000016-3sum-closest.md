---
title: "3Sum Closest"
weight: 16
#id: "3sum-closest"
---
## Description
<div class="description">
<p>Given an array <code>nums</code> of <em>n</em> integers and an integer <code>target</code>, find three integers in <code>nums</code>&nbsp;such that the sum is closest to&nbsp;<code>target</code>. Return the sum of the three integers. You may assume that each input would have exactly one solution.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [-1,2,1,-4], target = 1
<strong>Output:</strong> 2
<strong>Explanation:</strong> The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>3 &lt;= nums.length &lt;= 10^3</code></li>
	<li><code>-10^3&nbsp;&lt;= nums[i]&nbsp;&lt;= 10^3</code></li>
	<li><code>-10^4&nbsp;&lt;= target&nbsp;&lt;= 10^4</code></li>
</ul>

</div>

## Tags
- Array (array)
- Two Pointers (two-pointers)

## Companies
- Facebook - 4 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 6 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Adobe - 4 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: true)
- Yandex - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

This problem is a variation of [3Sum](https://leetcode.com/articles/3sum/). The main difference is that the sum of a triplet is not necessarily equal to the target. Instead, the sum is in some *relation* with the target, which is *closest* to the target for this problem. In that sense, this problem shares similarities with [3Sum Smaller](https://leetcode.com/articles/3sum-smaller/).

Before jumping in, let's check solutions for the similar problems:

1. [3Sum](https://leetcode.com/articles/3sum/) fixes one number and uses either the two pointers pattern or a hash set to find complementary pairs. Thus, the time complexity is $$\mathcal{O}(n^2)$$.

2. [3Sum Smaller](https://leetcode.com/articles/3sum-smaller/), similarly to 3Sum, uses the two pointers pattern to enumerate smaller pairs. Note that we cannot use a hash set here because we do not have a specific value to look up.

For the same reason as for 3Sum Smaller, we cannot use a hash set approach here. So, we will focus on the two pointers pattern and shoot for $$\mathcal{O}(n^2)$$ time complexity as the best conceivable runtime (BCR).

---

#### Approach 1: Two Pointers

The two pointers pattern requires the array to be sorted, so we do that first. As our BCR is $$\mathcal{O}(n^2)$$, the sort operation would not change the overall time complexity.

In the sorted array, we process each value from left to right. For value `v`, we need to find a pair which sum, ideally, is equal to `target - v`. We will follow the same two pointers approach as for 3Sum, however, since this 'ideal' pair may not exist, we will track the smallest absolute difference between the sum and the target. The two pointers approach naturally enumerates pairs so that the sum moves toward the target.

!?!../Documents/16_3Sum_Closest.json:1600,502!?!

**Algorithm**

1. Initialize the minimum difference `diff` with a large value.
2. Sort the input array `nums`.
3. Iterate through the array:
    - For the current position `i`, set `lo` to `i + 1`, and `hi` to the last index.
    - While the `lo` pointer is smaller than `hi`:
        - Set `sum` to `nums[i] + nums[lo] + nums[hi]`.
        - If the absolute difference between `sum` and `target` is smaller than the absolute value of `diff`:
            - Set `diff` to `target - sum`.
        - If `sum` is less than `target`, increment `lo`.
        - Else, decrement `hi`.
    - If `diff` is zero, break from the loop.
4. Return the value of the closest triplet, which is `target - diff`.

<iframe src="https://leetcode.com/playground/dbpzTKmh/shared" frameBorder="0" width="100%" height="344" name="dbpzTKmh"></iframe>

**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(n^2)$$. We have outer and inner loops, each going through $$n$$ elements.

    Sorting the array takes $$\mathcal{O}(n\log{n})$$, so overall complexity is $$\mathcal{O}(n\log{n} + n^2)$$. This is asymptotically equivalent to $$\mathcal{O}(n^2)$$.

- Space Complexity: from $$\mathcal{O}(\log{n})$$ to $$\mathcal{O}(n)$$, depending on the implementation of the sorting algorithm.

---

#### Approach 2: Binary Search

We can adapt the [3Sum Smaller: Binary Search](https://leetcode.com/articles/3sum-smaller/#approach-2-binary-search-accepted) approach to this problem.

In the two pointers approach, we fix one number and use two pointers to enumerate pairs. Here, we fix two numbers, and use a binary search to find the third complement number. This is less efficient than the two pointers approach, however, it could be more intuitive to come up with.

Note that we may not find the exact complement number, so we check the difference between the complement and two numbers: the next higher and the previous lower. For example, if the complement is `42`, and our array is `[-10, -4, 15, 30, 60]`, the next higher is `60` (so the difference is `-18`), and the previous lower is `30` (and the difference is `12`).

**Algorithm**

1. Initialize the minimum difference `diff` with a large value.
2. Sort the input array `nums`.
3. Iterate through the array (outer loop):
    - For the current position `i`, iterate through the array starting from `j = i + 1` (inner loop):
        - Binary-search for `complement` (`target - nums[i] - nums[j]`) in the rest of the array.
        - For the next higher value, check its absolute difference with `complement` against `diff`.
        - For the previous lower value, check its absolute difference with `complement` against `diff`.
        - Update `diff` based on the smallest absolute difference.
    - If `diff` is zero, break from the loop.
4. Return the value of the closest triplet, which is `target - diff`.

<iframe src="https://leetcode.com/playground/RsPcKjuz/shared" frameBorder="0" width="100%" height="327" name="RsPcKjuz"></iframe>

**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(n^2\log{n})$$. Binary search takes $$\mathcal{O}(\log{n})$$, and we do it $$n$$ times in the inner loop. Since we are going through $$n$$ elements in the outer loop, the overall complexity is $$\mathcal{O}(n^2\log{n})$$.

- Space Complexity: from $$\mathcal{O}(\log{n})$$ to $$\mathcal{O}(n)$$, depending on the implementation of the sorting algorithm.

---

#### Further Thoughts

3Sum is a well-known problem with many variations and its own [Wikipedia page](https://en.wikipedia.org/wiki/3SUM).

For an interview, we recommend focusing on the Two Pointers approach above. It's easier to get it right and adapt for other variations of 3Sum. Interviewers love asking follow-up problems like [3Sum Smaller](https://leetcode.com/articles/3sum-smaller/)!

If an interviewer asks you whether you can achieve $$\mathcal{O}(1)$$ memory complexity, you can use the selection sort instead of a built-in sort in the Two Pointers approach. It will make it a bit slower, though the overall time complexity will be still $$\mathcal{O}(n^2)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution with O(n2) for reference
- Author: chase1991
- Creation Date: Mon Nov 17 2014 01:16:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 14:35:22 GMT+0800 (Singapore Standard Time)

<p>
Similar to 3 Sum problem, use 3 pointers to point current element, next element and the last element. If the sum is less than target, it means we have to add a larger element so next element move to the next. If the sum is greater, it means we have to add a smaller element so last element move to the second last element. Keep doing this until the end. Each time compare the difference between sum and target, if it is less than minimum difference so far, then replace result with it, otherwise keep iterating.

    public class Solution {
        public int threeSumClosest(int[] num, int target) {
            int result = num[0] + num[1] + num[num.length - 1];
            Arrays.sort(num);
            for (int i = 0; i < num.length - 2; i++) {
                int start = i + 1, end = num.length - 1;
                while (start < end) {
                    int sum = num[i] + num[start] + num[end];
                    if (sum > target) {
                        end--;
                    } else {
                        start++;
                    }
                    if (Math.abs(sum - target) < Math.abs(result - target)) {
                        result = sum;
                    }
                }
            }
            return result;
        }
    }
</p>


### C++ solution O(n^2) using sort
- Author: asbear
- Creation Date: Sat Jun 27 2015 07:03:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 08:37:33 GMT+0800 (Singapore Standard Time)

<p>
Sort the vector and then no need to run *O(N^3)* algorithm as each index has a direction to move.

The code starts from this formation.

    ----------------------------------------------------
    ^  ^                                               ^
    |  |                                               |
    |  +- second                                     third
    +-first

if  *nums[first] + nums[second] + nums[third]* is smaller than the *target*, we know we have to increase the sum. so only choice is moving the second index forward.

    ----------------------------------------------------
    ^    ^                                             ^
    |    |                                             |
    |    +- second                                   third
    +-first


if the *sum* is bigger than the *target*, we know that we need to reduce the *sum*. so only choice is moving '*third*' to backward. of course if the *sum* equals to *target*, we can immediately return the *sum*.

    ----------------------------------------------------
    ^    ^                                          ^
    |    |                                          |
    |    +- second                                third
    +-first


when *second* and *third* cross, the round is done so start next round by moving '*first*' and resetting *second* and *third*.

    ----------------------------------------------------
      ^    ^                                           ^
      |    |                                           |
      |    +- second                                 third
      +-first

while doing this, collect the *closest sum* of each stage by calculating and comparing delta. Compare *abs(target-newSum)* and *abs(target-closest)*. At the end of the process the three indexes will eventually be gathered at the end of the array.

    ----------------------------------------------------
                                             ^    ^    ^
                                             |    |    `- third
                                             |    +- second
                                             +-first

if no exactly matching *sum* has been found so far, the value in *closest* will be the answer.


    int threeSumClosest(vector<int>& nums, int target) {
        if(nums.size() < 3) return 0;
        int closest = nums[0]+nums[1]+nums[2];
        sort(nums.begin(), nums.end());
        for(int first = 0 ; first < nums.size()-2 ; ++first) {
            if(first > 0 && nums[first] == nums[first-1]) continue;
            int second = first+1;
            int third = nums.size()-1;            
            while(second < third) {
                int curSum = nums[first]+nums[second]+nums[third];
                if(curSum == target) return curSum;
                if(abs(target-curSum)<abs(target-closest)) {
                    closest = curSum;
                }
                if(curSum > target) {
                    --third;
                } else {
                    ++second;
                }
            }
        }
        return closest;
    }
</p>


### Python O(N^2) solution
- Author: Google
- Creation Date: Sun Mar 08 2015 13:45:04 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 22:57:39 GMT+0800 (Singapore Standard Time)

<p>
    class Solution:
        # @return an integer
        def threeSumClosest(self, num, target):
            num.sort()
            result = num[0] + num[1] + num[2]
            for i in range(len(num) - 2):
                j, k = i+1, len(num) - 1
                while j < k:
                    sum = num[i] + num[j] + num[k]
                    if sum == target:
                        return sum
                    
                    if abs(sum - target) < abs(result - target):
                        result = sum
                    
                    if sum < target:
                        j += 1
                    elif sum > target:
                        k -= 1
                
            return result
</p>


