---
title: "Rearrange Words in a Sentence"
weight: 1336
#id: "rearrange-words-in-a-sentence"
---
## Description
<div class="description">
<p>Given a sentence&nbsp;<code>text</code> (A&nbsp;<em>sentence</em>&nbsp;is a string of space-separated words) in the following format:</p>

<ul>
	<li>First letter is in upper case.</li>
	<li>Each word in <code>text</code> are separated by a single space.</li>
</ul>

<p>Your task is to rearrange the words in text such that&nbsp;all words are rearranged in an increasing order of their lengths. If two words have the same length, arrange them in their original order.</p>

<p>Return the new text&nbsp;following the format shown above.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;Leetcode is cool&quot;
<strong>Output:</strong> &quot;Is cool leetcode&quot;
<strong>Explanation: </strong>There are 3 words, &quot;Leetcode&quot; of length 8, &quot;is&quot; of length 2 and &quot;cool&quot; of length 4.
Output is ordered by length and the new first word starts with capital letter.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;Keep calm and code on&quot;
<strong>Output:</strong> &quot;On and keep calm code&quot;
<strong>Explanation: </strong>Output is ordered as follows:
&quot;On&quot; 2 letters.
&quot;and&quot; 3 letters.
&quot;keep&quot; 4 letters in case of tie order by position in original text.
&quot;calm&quot; 4 letters.
&quot;code&quot; 4 letters.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> text = &quot;To be or not to be&quot;
<strong>Output:</strong> &quot;To be or to be not&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>text</code> begins with a capital letter and then contains lowercase letters and single space between words.</li>
	<li><code>1 &lt;= text.length &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- String (string)
- Sort (sort)

## Companies
- VMware - 2 (taggedByAdmin: false)
- Expedia - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 4 line
- Author: mayank12559
- Creation Date: Sun May 17 2020 12:01:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 12:01:06 GMT+0800 (Singapore Standard Time)

<p>
```
public String arrangeWords(String text) {
        String []s = text.toLowerCase().split(" ");
        Arrays.sort(s, (a,b) ->  a.length() - b.length());
        String ans = String.join(" ", s);
        return Character.toUpperCase(ans.charAt(0)) + ans.substring(1);
    }
```
</p>


### STL sort video explanation to Comparator in sort
- Author: white_shadow_
- Creation Date: Sun May 17 2020 12:05:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 18:27:45 GMT+0800 (Singapore Standard Time)

<p>

link:-https://youtu.be/W10o0xDDOE0?t=2


		static bool cmp(pair<string,int>&a,pair<string,int>&b)
			{
				if(a.first.size()==b.first.size())
				{
					return a.second<b.second;
				}
				return a.first.size()<b.first.size();
			}
			string arrangeWords(string text)
			{
				vector<pair<string,int>>v;
				string temp;
				text[0]=tolower(text[0]);
				text+=\' \';
				for(int i=0;i<text.size();i++)
				{
					if(text[i]==\' \')
					{
						v.push_back({temp,i});
						temp="";
					}
					else
					{
						temp+=text[i];
					}
				}
				sort(v.begin(),v.end(),cmp);
				temp="";
				for(auto a:v)
				{
					temp+=a.first;
					temp+=\' \';
				}
				temp[0]=toupper(temp[0]);
				return temp.substr(0,temp.size()-1);
			}
			
</p>


### [Python3] one line
- Author: ye15
- Creation Date: Sun May 17 2020 12:04:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 12:39:52 GMT+0800 (Singapore Standard Time)

<p>
A few string operations chained together to get the job done. 

```
class Solution:
    def arrangeWords(self, text: str) -> str:
        return " ".join(sorted(text.split(" "), key=len)).capitalize()
```
</p>


