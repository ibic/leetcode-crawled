---
title: "Couples Holding Hands"
weight: 691
#id: "couples-holding-hands"
---
## Description
<div class="description">
<p>
N couples sit in 2N seats arranged in a row and want to hold hands.  We want to know the minimum number of swaps so that every couple is sitting side by side.  A <i>swap</i> consists of choosing <b>any</b> two people, then they stand up and switch seats. 
</p><p>
The people and seats are represented by an integer from <code>0</code> to <code>2N-1</code>, the couples are numbered in order, the first couple being <code>(0, 1)</code>, the second couple being <code>(2, 3)</code>, and so on with the last couple being <code>(2N-2, 2N-1)</code>.
</p><p>
The couples' initial seating is given by <code>row[i]</code> being the value of the person who is initially sitting in the i-th seat.

<p><b>Example 1:</b><br /><pre>
<b>Input:</b> row = [0, 2, 1, 3]
<b>Output:</b> 1
<b>Explanation:</b> We only need to swap the second (row[1]) and third (row[2]) person.
</pre></p>

<p><b>Example 2:</b><br /><pre>
<b>Input:</b> row = [3, 2, 0, 1]
<b>Output:</b> 0
<b>Explanation:</b> All couples are already seated side by side.
</pre></p>

<p>
<b>Note:</b>
<ol> 
<li> <code>len(row)</code> is even and in the range of <code>[4, 60]</code>.</li>
<li> <code>row</code> is guaranteed to be a permutation of <code>0...len(row)-1</code>.</li>
</ol>
</div>

## Tags
- Greedy (greedy)
- Union Find (union-find)
- Graph (graph)

## Companies
- Google - 3 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach Framework

**Observations**

First, instead of couples `(0, 1), (2, 3), (4, 5), ...`, we could just consider couples `(0, 0), (1, 1), (2, 2), ...` without changing the answer.  Also, we could imagine that we have `N` two-seat couches `0, 1, 2, ..., N-1`.  This is because the person sitting on the left-most seat of the row must be paired with the person sitting on the second-left-most seat, the third-left-most paired with the fourth-left-most, and so on.
Call a person happy if they are with their partner on the same couch.  Intuitively, a swap that keeps both persons swapped unhappy is not part of some optimal solution.  We'll call this the *happy swap assumption* (HSA), and we'll prove it in Approach #2.

---

#### Approach #1: Backtracking [Time Limit Exceeded]

**Intuition**

We could guess without proof that a solution where we make the people on each couch happy in order is optimal.  This assumption is stronger than HSA, but it seems reasonable because at each move we are making at least 1 couple happy.  (See Approach #2 for a proof.)
Under such an assumption, for some couch with unhappy people X and Y, we either replace Y with X's partner, or replace X with Y's partner.
For each of the two possibilities, we can try both using a backtracking approach.

**Algorithm**

For each couch with two possibilities (ie. both people on the couch are unhappy), we will try the first possibility, find the answer as `ans1`, then undo our move and try the second possibility, find the associated answer as `ans2`, undo our move and then return the smallest answer.

<iframe src="https://leetcode.com/playground/9kcyhDWB/shared" frameBorder="0" width="100%" height="500" name="9kcyhDWB"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N * 2^N)$$, where $$N$$ is the number of couples, as for each couch we check up to two complete possibilities.  The $$N$$ factor is from searching for `jx` and `jy`; this factor can be removed with a more efficient algorithm that keeps track of where `pairs[j][k]` is `x` as we swap elements through pairs.

* Space Complexity: $$O(N)$$.

---
#### Approach #2: Cycle Finding [Accepted]

**Intuition**

If we take the HSA as true, it means that if a couple is on two separate couches, there are two possible moves to make this couple happy, depending on which partner visits their partner and which stays in place.
This leads to the following idea: for each couple sitting at couches X and Y (possibly the same), draw an undirected edge from X to Y.  Call such a graph the couples graph.  This graph is 2-regular (every node has degree 2), and it is easy to see that every connected component of this graph must be a cycle.

<br />
<center>
    <img src="../Figures/765/couples_cycle_finding.png" alt="Diagram of cyclic list" width="350"/>
</center>
<br />

Then, making a swap for $$X_1$$ to meet their partner $$X_2$$ has a corresponding move on the couples graph equivalent to contracting the corresponding edge to $$X_1X_2$$ plus having a node with a single self edge.
Our goal is to have `N` connected components in the graph, one for each couch.  Every swap (allowed by the scheme above) always increases that number by exactly 1, so under HSA, the answer is just `N` minus the number of connected components in the couples graph.

Now to prove HSA, observe that it is impossible with *any* swap to create more than 1 additional connected component in the couples graph.  So any optimal solution must have at least the number of moves in the answer we've constructed.  (This also proves the ordering assumption made in Approach #1, as we can make edge contractions of a cycle in any order without changing the answer.)

**Algorithm**

We'll construct the graph: `adj[node]` will be the index of the two nodes that this `node` is adjacent to.
After, we'll find all connected components (which are also cycles.)  If at some couch (node) a person is unvisited, we will visit it and repeatedly visit some neighbor until we complete the cycle.

<iframe src="https://leetcode.com/playground/VdW6ig2M/shared" frameBorder="0" width="100%" height="500" name="VdW6ig2M"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of couples.

* Space Complexity: $$O(N)$$, the size of `adj` and associated data structures.

---
#### Approach #3: Greedy [Accepted]

**Intuition**

From guessing, or by the proof in *Approach #2*, our strategy is to resolve each couch in order.

To resolve a couch, fix the first person and have their partner swap into the second seat if they are not already there.

**Algorithm**

If a person is number `x`, their partner is `x ^ 1`, where `^` is the bitwise XOR operator.

For each first person `x = row[i]` on a couch who is unpartnered, let's find their partner at `row[j]` and have them swap seats with `row[i+1]`.

<iframe src="https://leetcode.com/playground/5um6b62S/shared" frameBorder="0" width="100%" height="361" name="5um6b62S"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2)$$, where $$N$$ is the number of couples.

* Space Complexity: $$O(1)$$ additional complexity: the swaps are in place.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java/C++ O(N) solution using cyclic swapping
- Author: fun4LeetCode
- Creation Date: Tue Jan 16 2018 09:22:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sat May 30 2020 00:31:20 GMT+0800 (Singapore Standard Time)

<p>
You may have encountered problems that can be solved using cyclic swapping before (like [lc41](https://leetcode.com/problems/first-missing-positive/description/) and [lc268](https://leetcode.com/problems/missing-number/description/)). In this post, I will use a simplified `N` integers problem to illustrate the cyclic swapping idea and then generalize it to this `N` couples problem.

---

`I -- N integers problem`

Assume we have an integer array `row` of length `N`, which contains integers from `0` up to `N-1` but in random order. You are free to choose any two numbers and swap them. What is the minimum number of swaps needed so that we have `i == row[i]` for `0 <= i < N` (or equivalently, to sort this integer array)?

**First**, to apply the cyclic swapping algorithm, we need to divide the `N` indices into mutually exclusive index groups, where indices in each group form a cycle: `i0 --> i1 --> ... --> ik --> i0`. Here we employ the notation `i --> j` to indicate that we expect the element at index `i` to appear at index `j` at the end of the swapping process.

Before we dive into the procedure for building the index groups, here is a simple example to illustrate the ideas, assuming we have the following integer array and corresponding indices:

```
row: 2, 3, 1, 0, 5, 4
idx: 0, 1, 2, 3, 4, 5
```

Starting from index `0`, what is the index that the element at index `0` is expected to appear? The answer is `row[0]`, which is `2`. Using the above notation, we have `0 --> 2`. Then starting from index `2`, what is the index that the element at index `2` is expected to appear? The answer will be `row[2]`, which is `1`, so we have `2 --> 1`. We can continue in this fashion until the indices form a cycle, which indicates an index group has been found: `0 --> 2 --> 1 --> 3 --> 0`. Then we choose another start index that has not been visited and repeat what we just did. This time we found another group: `4 --> 5 --> 4`, after which all indices are visited so we conclude there are two index groups.

Now for an arbitrary integer array, we can take similar approaches to build the index groups. Starting from some unvisited index `i0`, we compute the index `i1` at which the element at `i0` is expected to appear. In this case, `i1 = row[i0]`. We then continue from index `i1` and compute the index `i2` at which the element at `i1` is expected to appear. Similarly we have, `i2 = row[i1]`. We continue in this fashion to construct a list of indices: `i0 --> i1 --> i2 --> ... --> ik`. Next we will show that eventually the list will repeat itself from index `i0`, which has two implications:

1. Eventually the list will repeat itself.
2. It will repeat itself from index `i0`, not other indices.

Proof of implication **1**: Assume the list will never repeat itself. Then we can follow the above procedure to generate a list containing at least `(N + 1)` distinct indices. This implies at least one index will be out of the range `[0, N-1]`. However, we know that each index in the list should represent a valid index into the `row` array. Therefore, all indices should lie in the range `[0, N-1]`, a contradiction. So we conclude the list will eventually repeat itself.

Proof of implication **2**: Assume the list will repeat itself at some index `j` other than `i0`. Then we know there exists at least two different indices `p` and `q` such that we have `p --> j` and `q --> j`, which, according to the definition of the `-->` notation, implies at the end of the swapping process, we need to place two different elements at index `j` simultaneously, which is impossible. So we conclude the list must repeat itself at index `i0`.

**Next** suppose we have produced two such index groups, `g1` and `g2`, which are not identical (there exists at least one index contained in `g1` but not in `g2` and at least one index contained in `g2` but not in `g1`). We will show that all indices in `g1` cannot appear in `g2`, and vice versa - - `g1` and `g2` are mutually exclusive. The proof is straightforward: if there is some index `j` that is common to both `g1` and `g2`, then both `g1` and `g2` can be constructed by starting from index `j` and following the aforementioned procedure. Since each index is only dependent on its predecessor, the groups generated from the same start index will be identical to each other, contradicting the assumption. Therefore `g1` and `g2` will be mutually exclusive. This also implies the union of all groups will cover all the `N` indices exactly once.

**Lastly**, we will show that the minimum number of swaps needed to resolve an index group of size `k` is given by `k - 1`. Here we define the size of a group as the number of distinct indices contained in the group, for example:

1. Size **`1`** groups: `0 --> 0`, `2 --> 2`, etc.
2. Size **`2`** groups: `0 --> 3 --> 0`, `2 --> 1 --> 2`, etc.
......
3. Size **`k`** groups: `0 --> 1 --> 2 --> ... --> (k-1) --> 0`, etc.

And by saying "resolving a group", we mean placing the elements at each index contained in the group to their expected positions at the end of the swapping process. In this case, we want to put the element at index `i`, which is `row[i]`, to its expected position, which is `row[i]` again (the fact that the element itself coincides with its expected position is a result of the placement requirement `row[i] == i`).

The proof proceeds by using mathematical induction:

1. **Base case**: If we have a group of size `1`, what is the minimum number of swaps needed to resolve the group? First, what does it imply if a group has size `1`? A group of size `1` takes the form `j --> j`. From the definition of the `-->` notation, we know the element at index `j` is expected to appear at index `j` at the end of the swapping process. Since this is already the case (that is, element at index `j` is already placed at index `j`), we don\'t need to take any further actions, therefore the minimum number of swaps needed is `0`, which is the group size minus `1`. Hence the base case stands.

2. **Induction case**: Assume the minimum number of swaps needed to resolve a group of size `m` is given by `m - 1`, where `1 <= m <= k`. For a group of size `k + 1`, we can always use one swap to turn it into two mutually exclusive groups of size `k1` and `k2`, respectively, such that `k1 + k2 = k + 1` with `1 <= k1, k2 <= k`. This is done by first choosing any two different indices `i` and `j` in the group, then swapping the **element** at index `i` with that at index `j`. Before swapping, the group is visualized as: `... --> i --> p --> ... --> j --> q --> ...`. The swapping operation is equivalent to repointing index `i` to index `q` meanwhile repointing index `j` to index `p`. This is because the element originally at index `i` is now at index `j`, therefore the expected index for the element at index `j` **after swapping** will be the same as the one for the element at index `i` **before swapping**, which is `p`, so we have `j --> p` after swapping. Similarly we have `i --> q` after swapping. Therefore, the original group becomes two disjoint groups after swapping: `... --> i --> q --> ...` and `... --> j --> p --> ...`, where each group contains at least one index. Now by the induction assumption, the minimum number of swaps needed to resolve the above two groups are given by `k1 - 1` and `k2 - 1`, respectively. Therefore, the minimum number of swapping needed to resolve the original group of size `(k + 1)` is given by `(k1 - 1) + (k2 - 1) + 1 = k1 + k2 - 1 = k`. Hence the induction case also stands.

**In conclusion**, the minimum number of swaps needed to resolve the whole array can be obtained by summing up the minimum number of swaps needed to resolve each of the index groups. To resolve each index group, we are free to choose any two distinct indices in the group and swap them so as to reduce the group to two smaller disjoint groups. In practice, we can always choose a pivot index and continuously swap it with its expected index until the pivot index is the same as its expected index, meaning the entire group is resolved and all placement requirements within the group are satisfied.

The following is a sample implementation for this problem, where for each pivot index `i`, the expected index `j` is computed by taking the value of `row[i]`.

```
public int miniSwapsArray(int[] row) {
    int res = 0, N = row.length;

    for (int i = 0; i < N; i++) {
		for (int j = row[i]; i != j; j = row[i]) {
			swap(row, i, j);
			res++;
		}
    }

    return res;
}

private void swap(int[] arr, int i, int j) {
    int t = arr[i];
    arr[i] = arr[j];
    arr[j] = t;
}
```

---

`II -- N couples problem`

The `N` couples problem can be solved using exactly the same idea as the `N` integers problem, except now we have different placement requirements: instead of `i == row[i]`, we require `i == ptn[pos[ptn[row[i]]]]`, where we have defined two additional arrays `ptn` and `pos`:

1. `ptn[i]` denotes the partner of label `i` (`i` can be either a seat or a person) - - `ptn[i] = i + 1` if `i` is even; `ptn[i] = i - 1` if `i` is odd.

2. `pos[i]` denotes the index of the person with label `i` in the `row` array - - `row[pos[i]] == i`.

The meaning of `i == ptn[pos[ptn[row[i]]]]` is as follows:

1. The person sitting at seat `i` has a label `row[i]`, and we want to place him/her next to his/her partner.

2. So we first find the label of his/her partner, which is given by `ptn[row[i]]`.

3. We then find the seat of his/her partner, which is given by `pos[ptn[row[i]]]`.

4. Lastly we find the seat next to his/her partner\'s seat, which is given by `ptn[pos[ptn[row[i]]]]`.

Therefore, for each pivot index `i`, its expected index `j` is given by `ptn[pos[ptn[row[i]]]]`. As long as `i != j`, we swap the two elements at index `i` and `j`, and continue until the placement requirement is satisfied. A minor complication here is that for each swapping operation, we need to swap both the `row` and `pos` arrays. 

Here is a list of solutions for Java and C++. Both run at `O(N)` time with `O(N)` space. Note that there are several optimizations we can do, just to name a few:

1. The `ptn` array can be replaced with a simple function that takes an index `i` and returns `i + 1` or `i - 1` depending on whether `i` is even or odd.

2. We can check every other seat instead of all seats. This is because we are matching each person to his/her partners, so technically speaking there are always half of the people sitting at the right seats.

3. There is an alternative way for building the index groups which goes in backward direction, that is instead of building the cycle like `i0 --> i1 --> ... --> jk --> i0`, we can also build it like `i0 <-- i1 <-- ... <-- ik <-- i0`, where `i <-- j` means the element at index `j` is expected to appear at index `i`. In this case, the pivot index will be changing along the cycle as the swapping operations are applied. The benefit is that we only need to do swapping on the `row` array.

**Java solution**:

```
public int minSwapsCouples(int[] row) {
    int res = 0, N = row.length;
    
    int[] ptn = new int[N];    
    int[] pos = new int[N];
    
    for (int i = 0; i < N; i++) {
        ptn[i] = (i % 2 == 0 ? i + 1 : i - 1);
        pos[row[i]] = i;
    }
    
    for (int i = 0; i < N; i++) {
        for (int j = ptn[pos[ptn[row[i]]]]; i != j; j = ptn[pos[ptn[row[i]]]]) {
			swap(row, i, j);
			swap(pos, row[i], row[j]);
			res++;
		}
    }
    
    return res;
}

private void swap(int[] arr, int i, int j) {
    int t = arr[i];
    arr[i] = arr[j];
    arr[j] = t;
}
```

**C++ solution**:

```
int minSwapsCouples(vector<int>& row) {
    int res = 0, N = row.size();
        
    vector<int> ptn(N, 0);
    vector<int> pos(N, 0);
        
    for (int i = 0; i < N; i++) {
        ptn[i] = (i % 2 == 0 ? i + 1 : i - 1);
        pos[row[i]] = i;
    }
    
    for (int i = 0; i < N; i++) {
        for (int j = ptn[pos[ptn[row[i]]]]; i != j; j = ptn[pos[ptn[row[i]]]]) {
			swap(row[i], row[j]);
            swap(pos[row[i]], pos[row[j]]);
			res++;
		}
    }
        
    return res;
}
```
</p>


### Java, union find, easy to understand, 5 ms
- Author: aCombray
- Creation Date: Sun Mar 04 2018 05:33:27 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 10:22:24 GMT+0800 (Singapore Standard Time)

<p>
Think about each couple as a vertex in the graph. So if there are N couples, there are N vertices. Now if in position *2i* and *2i +1* there are person from couple u and couple v sitting there, that means that the permutations are going to involve u and v. So we add an edge to connect u and v. The min number of swaps = N - number of connected components. This follows directly from the theory of permutations. Any permutation can be decomposed into a composition of cyclic permutations. If the cyclic permutation involve k elements, we need k -1 swaps. You can think about each swap as reducing the size of the cyclic permutation by 1. So in the end, if the graph has k connected components, we need N - k swaps to reduce it back to N disjoint vertices. 

Then there are many ways of doing this. We can use dfs for example to compute the number of connected components. The number of edges isn O(N). So this is an O(N) algorithm. We can also use union-find. I think a union-find is usually quite efficient. The following is an implementation.


```
class Solution {
    private class UF {
        private int[] parents;
        public int count;
        UF(int n) {
            parents = new int[n];
            for (int i = 0; i < n; i++) {
                parents[i] = i;
            }
            count = n;
        }
        
        private int find(int i) {
            if (parents[i] == i) {
                return i;
            }
            parents[i] = find(parents[i]);
            return parents[i];
        }
        
        public void union(int i, int j) {
            int a = find(i);
            int b = find(j);
            if (a != b) {
                parents[a] = b;
                count--;
            }
        }
    }
    public int minSwapsCouples(int[] row) {
        int N = row.length/ 2;
        UF uf = new UF(N);
        for (int i = 0; i < N; i++) {
            int a = row[2*i];
            int b = row[2*i + 1];
            uf.union(a/2, b/2);
        }
        return N - uf.count;
    }
}
```
</p>


### The general mathematical idea: permutation graph and graph decomposition.
- Author: jolyon129
- Creation Date: Wed Jul 17 2019 02:56:25 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 17 2019 03:02:18 GMT+0800 (Singapore Standard Time)

<p>
This question is similar to [854 K-Similar Strings](https://leetcode.com/problems/k-similar-strings/)
The solutions to these kinds of problems all come down to one mathematical idea, permutation graph, no matter you are using union-find, greedy, or cycle decomposition. And once you understand the concept of permutation graph, you will know how to think about it when you next time encountering them. So, I\'m going to detour a little bit to talk about the permutation graph.

## Permutation Graph and Graph Decomposition

The permutation graph theory clams that given a pair of permutation, e.g, `group1`:`a,b,a,c,a,d,c` and `group2`:`b,c,d,a,a,c,a`, if we connect the edge from group1 to group2, we can represent this pair of permutation by a graph. Based on observation, if at one position `i`, we have `group1[i]=group[i]`, then this vertex should have a self-connected edge.  

![](https://miro.medium.com/max/1400/1*isI1TOU8CFEsuz9s0n89cg.png)


To transform `group2` into `group1`, we need to make swapping so that we can remove edges between two vertices(in other words, let them become self-connected edges). 

Then, another observation you need is that for any cycle in our graph, the minimal steps to remove all non-self-connected edges for a cycle is the number of edges minus 1,` #edges-1`. Hence, the total number of swap:
$\#swap = \sum(C_k-1)$ where $C_1, ..., C_k$ are the lengths of the cycles(connected components) in our graph. 

If we want to minimize the total `#swap`, we need to find a way to decompose our graph into different cycles so that each cycle is as small as possible(Because the number of edges is fixed, but if the more cycles we ended with, the more times `#swap` is minus by 1). 

The more general visual example is given in this post: (854. K-Similar Strings)[https://medium.com/@jolyon129/854-k-similar-strings-7b68772217d0]. Actually, in this question, we don\'t need to decompose the graph because each vertex only has 2 degrees and there is only one way to decompose the graph. I\'ll talk this later. 

For more detailed information about cycle decomposition, you can check this [video](https://www.youtube.com/watch?v=6CwiYoEsEPI)

Those are all the prerequisite knowledge to solve this question. Let\'s back to our problem.

## Union-Find

The problem defines a bunch of couples, e.g, `[0,1]` , `[1,2]`, `[3,4]`. Without loss of generality, Let\'s name `[0,1]` couple `A`,  `[2,3]` couple `B` and so on. To smooth our understanding, let\'s say we have a function named \'coupleOf()\'. According to our definition, we have `coupleOf(0)=A`, `coupleOf(1)=A`, \'coupleOf(2)=B\', `coupleOf(3)=B`. `A`,`B` can denote vertices in the graph and for each pair in the row, row[2*i] and row[2*i+1] (i=0,1,2,3), we connect the edge from `coupleOf(row[2*i])` to `coupleOf(row[2*i+1])`.

Given a row = `[0,2,3,4,5,1,6,8,7,9]`, we can visualize them as follows:


![](https://miro.medium.com/max/1400/1*ZfO9A8Fi_UhHGbPVaSIIpg.png)

According to permutation graph theory, the minimal step of optimal swapping(the swap which at least help match one couple) is $\#swap = \sum(C_k-1)$ where $C_1, ..., C_k$ are the lengths of the cycles(connected components) in our graph. 

Notice that in this question, every vertex only has 2 degrees, and there is only one way to decompose our graph into cycles(which is very obvious) so that the problem could be reduced into `#swap = #(vertex) - #num(components)`. In the end, we only need to count the number of connected components, and this can be solved by using `DFS` or `Union-Find`.

## Greedy Policy

So far, we have already found a way to solve this question in `O(n)` time. If you look at the graph above more carefully, there is a greedy policy. Since there is only one way to decompose graph, any swap that helps match at least one couple is a greedy action. 

Specifically, we can use one loop to precompute a hash map, `position`, which used to store the position of each person. Then we loop over the `row` again, whenever we find a pair of people not match, we can find where the corresponding mate is in `position`  and swap them. We keep doing these greedy actions and count how many swapping we need. 

```
class Solution(){
    private int count = 0;
    private HashMap<Integer, Integer> position = new HashMap<>();

    public int minSwapsCouples(int[] row) {
        for (int i = 0; i < row.length; i++) {
            this.position.put(row[i], i);
        }
        for (int g = 0; g < row.length / 2; g++) {
            int c1 = row[2*g];
            int c2 = row[2*g+1];
            if((c1%2==0 && c2!=c1+1) || (c1%2!=0 &&c2!=c1-1)){
                int source_int = row[2 * g];
                int target_pos = source_int
                        % 2 == 0 ? position.get(source_int + 1) : position.get(source_int - 1);
                swap(row, 2 * g+1, target_pos);
                count++;
            }
        }
        return count;
    }

    private void swap(int[] row, int i, int j) {
        int tmp = row[i];
        row[i] = row[j];
        row[j] = tmp;
        this.position.put(row[i], i);
        this.position.put(row[j], j);
    }
}
```








</p>


