---
title: "Employee Importance"
weight: 622
#id: "employee-importance"
---
## Description
<div class="description">
<p>You are given a data structure of employee information, which includes the employee&#39;s <b>unique id</b>, their&nbsp;<b>importance value</b> and their&nbsp;<b>direct</b> subordinates&#39; id.</p>

<p>For example, employee 1 is the leader of employee 2, and employee 2 is the leader of employee 3. They have importance value 15, 10 and 5, respectively. Then employee 1 has a data structure like [1, 15, [2]], and employee 2 has [2, 10, [3]], and employee 3 has [3, 5, []]. Note that although employee 3 is also a subordinate of employee 1, the relationship is <b>not direct</b>.</p>

<p>Now given the employee information of a company, and an employee id, you need to return the total importance value of this employee and all their&nbsp;subordinates.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> [[1, 5, [2, 3]], [2, 3, []], [3, 3, []]], 1
<b>Output:</b> 11
<b>Explanation:</b>
Employee 1 has importance value 5, and he has two direct subordinates: employee 2 and employee 3. They both have importance value 3. So the total importance value of employee 1 is 5 + 3 + 3 = 11.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>One employee has at most one <b>direct</b> leader and may have several subordinates.</li>
	<li>The maximum number of employees won&#39;t exceed 2000.</li>
</ol>

</div>

## Tags
- Hash Table (hash-table)
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)

## Companies
- Google - 7 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Uber - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Depth-First Search [Accepted]

**Intuition and Algorithm**

Let's use a hashmap `emap = {employee.id -> employee}` to query employees quickly.

Now to find the total importance of an employee, it will be the importance of that employee, plus the total importance of each of that employee's subordinates.  This is a straightforward depth-first search.

<iframe src="https://leetcode.com/playground/Y9QW7TMB/shared" frameBorder="0" width="100%" height="310" name="Y9QW7TMB"></iframe>


**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of employees.  We might query each employee in `dfs`.

* Space Complexity: $$O(N)$$, the size of the implicit call stack when evaluating `dfs`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java HashMap bfs dfs
- Author: xuyirui
- Creation Date: Fri Sep 29 2017 14:29:37 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 03:44:23 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int getImportance(List<Employee> employees, int id) {
        int total = 0;
        Map<Integer, Employee> map = new HashMap<>();
        for (Employee employee : employees) {
            map.put(employee.id, employee);
        }
        Queue<Employee> queue = new LinkedList<>();
        queue.offer(map.get(id));
        while (!queue.isEmpty()) {
            Employee current = queue.poll();
            total += current.importance;
            for (int subordinate : current.subordinates) {
                queue.offer(map.get(subordinate));
            }
        }
        return total;
    }
}




class Solution {
    public int getImportance(List<Employee> employees, int id) {
        Map<Integer, Employee> map = new HashMap<>();
        for (Employee employee : employees) {
            map.put(employee.id, employee);
        }
        return getImportanceHelper(map, id);
    }
    
    private int getImportanceHelper(Map<Integer, Employee> map, int rootId) {
        Employee root = map.get(rootId);
        int total = root.importance;
        for (int subordinate : root.subordinates) {
            total += getImportanceHelper(map, subordinate);
        }
        return total;
    }
}
</p>


### I literally didn't understand the data type of `employees` from the question
- Author: lizzy0127
- Creation Date: Mon Nov 06 2017 00:33:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 11:34:40 GMT+0800 (Singapore Standard Time)

<p>
The question says `type employees: Employee`, which seems like `employees` is a node of class `Employee`. However, it's obvious that the class `Employee` only contain information of 1 employee, and `employees` should contain information of ALL employees. So I had a hard time figuring out how to extract every employee's information from `employees` because the question doesn't really say it. I had to read others' solution to figure out what it actually means. I don't know whether this is a usual way of describing data type, but I think this is very confusing. Even `List[Employee]` or `map(Employee)` or whatever would be a better description.
</p>


### 3-liner Python Solution (beats 99%)
- Author: yangshun
- Creation Date: Sun Oct 01 2017 17:30:06 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 16 2019 07:40:16 GMT+0800 (Singapore Standard Time)

<p>
Convert the employees array/list into a map for easy lookup and then do a DFS traversal from the "root" employee node.

*- Yangshun*

```
class Solution(object):
    def getImportance(self, employees, id):
        """
        :type employees: Employee
        :type id: int
        :rtype: int
        """
        # Time: O(n)
        # Space: O(n)
        emps = {employee.id: employee for employee in employees}
        dfs = lambda id: sum([dfs(sub_id) for sub_id in emps[id].subordinates]) + emps[id].importance
        return dfs(id)         
```

If you wanna make it more readable, it is 5-lines:

```
class Solution(object):
    def getImportance(self, employees, id):
        """
        :type employees: Employee
        :type id: int
        :rtype: int
        """
        # Time: O(n)
        # Space: O(n)
        emps = {employee.id: employee for employee in employees}
        def dfs(id):
            subordinates_importance = sum([dfs(sub_id) for sub_id in emps[id].subordinates])
            return subordinates_importance + emps[id].importance
        return dfs(id)
```
</p>


