---
title: "Lowest Common Ancestor of Deepest Leaves"
weight: 1098
#id: "lowest-common-ancestor-of-deepest-leaves"
---
## Description
<div class="description">
<p>Given a rooted binary tree, return the lowest common ancestor of its deepest leaves.</p>

<p>Recall that:</p>

<ul>
	<li>The node of a binary tree is a <em>leaf</em> if and only if it has no children</li>
	<li>The <em>depth</em> of the root of the tree is 0, and if the depth of a node is <code>d</code>, the depth of each of its children&nbsp;is&nbsp;<code>d+1</code>.</li>
	<li>The <em>lowest common ancestor</em> of a set <code>S</code> of nodes is the node <code>A</code> with the largest depth such that every node in S is in the subtree with root <code>A</code>.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> root = [1,2,3]
<strong>Output:</strong> [1,2,3]
<strong>Explanation:</strong> 
The deepest leaves are the nodes with values 2 and 3.
The lowest common ancestor of these leaves is the node with value 1.
The answer returned is a TreeNode object (not an array) with serialization &quot;[1,2,3]&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = [1,2,3,4]
<strong>Output:</strong> [4]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [1,2,3,4,5]
<strong>Output:</strong> [2,4,5]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The given tree will have between 1 and 1000 nodes.</li>
	<li>Each node of the tree will have a distinct value between 1 and 1000.</li>
</ul>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Facebook - 4 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Two Recursive Solution
- Author: lee215
- Creation Date: Sun Jul 14 2019 12:02:51 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 17 2019 12:59:38 GMT+0800 (Singapore Standard Time)

<p>
# **Complexity**
Both solution are one pass.
Time `O(N)` for one pass
Space `O(H)` for recursion management
<br>

# Solution 1: Get Subtree Height and LCA
`helper` function return the subtree `height` and `lca` and  at the same time.
`null` node will return depth 0,
leaves will return depth 1.


<br>

**C++**
```cpp
    pair<TreeNode*, int> helper(TreeNode* root) {
        if (!root) return {NULL, 0};
        auto left = helper(root->left);
        auto right = helper(root->right);
        if (left.second > right.second)
            return {left.first, left.second + 1};
        if (left.second < right.second)
            return {right.first, right.second + 1};
        return {root, left.second + 1};

    }
    TreeNode* lcaDeepestLeaves(TreeNode* root) {
        return helper(root).first;
    }
```

**Python:**
```python
    def lcaDeepestLeaves(self, root):
        def helper(root):
            if not root: return 0, None
            h1, lca1 = helper(root.left)
            h2, lca2 = helper(root.right)
            if h1 > h2: return h1 + 1, lca1
            if h1 < h2: return h2 + 1, lca2
            return h1 + 1, root
        return helper(root)[1]
```

**Java**
from @timmy2
```java
    class Pair {
        TreeNode node;
        int d;
        Pair(TreeNode node, int d) {
            this.node = node;
            this.d = d;
        }
    }
    public TreeNode lcaDeepestLeaves(TreeNode root) {
        Pair p = getLca(root, 0);
        return p.node;
    }
    private Pair getLca(TreeNode root, int d) {
        if (root == null) return new Pair(null, d);
        Pair l = getLca(root.left, d + 1);
        Pair r = getLca(root.right, d + 1);
        if (l.d == r.d) {
            return new Pair(root, l.d);
        } else {
            return  l.d > r.d ? l : r;
        }
    }
```
<br>

# Solution 2: Get Subtree Deepest Depth
`helper` function return the deepest depth of descendants.
`deepest` represent the deepest depth.
We use a global variable `lca` to represent the result.
One pass, Time `O(N)` Space `O(H)`
<br>

**Java:**
inspired by @Brahms
```java
    int deepest = 0;
    TreeNode lca;

    public TreeNode lcaDeepestLeaves(TreeNode root) {
        helper(root, 0);
        return lca;
    }

    private int helper(TreeNode node, int depth) {
        deepest = Math.max(deepest, depth);
        if (node == null) {
            return depth;
        }
        int left = helper(node.left, depth + 1);
        int right = helper(node.right, depth + 1);
        if (left == deepest && right == deepest) {
            lca = node;
        }
        return Math.max(left, right);
    }
```

**C++:**
```cpp
    TreeNode* lca;
    int deepest = 0;
    TreeNode* lcaDeepestLeaves(TreeNode* root) {
        helper(root, 0);
        return lca;
    }

    int helper(TreeNode* node, int depth) {
        deepest = max(deepest, depth);
        if (!node) {
            return depth;
        }
        int left = helper(node->left, depth + 1);
        int right = helper(node->right, depth + 1);
        if (left == deepest && right == deepest) {
            lca = node;
        }
        return max(left, right);
    }
```

**Python:**
```python
    def lcaDeepestLeaves(self, root):
        self.lca, self.deepest = None, 0
        def helper(node, depth):
            self.deepest = max(self.deepest, depth)
            if not node:
                return depth
            left = helper(node.left, depth + 1)
            right = helper(node.right, depth + 1)
            if left == right == self.deepest:
                self.lca = node
            return max(left, right)
        helper(root, 0)
        return self.lca
```

</p>


### I don't understand what this question is talking about...
- Author: DonaldTrump
- Creation Date: Sun Jul 14 2019 12:07:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 14 2019 12:07:20 GMT+0800 (Singapore Standard Time)

<p>
Shouldn\'t the lca of bunch of nodes just 1 tree node? Why [1,2,3] returns [1,2,3]?
</p>


### [C++] beats 100% time and space complexity
- Author: Izat_Khamiyev
- Creation Date: Sun Jul 14 2019 14:28:03 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 13 2020 19:10:15 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    TreeNode* lcaDeepestLeaves(TreeNode* root) {
        if(!root)
            return NULL;
        int l = countDepth(root->left);
        int r = countDepth(root->right);
        if(l == r)
            return root;
        else if(l < r)
            return lcaDeepestLeaves(root->right);
        else
            return lcaDeepestLeaves(root->left);
    }
    
    int countDepth(TreeNode* root){
        if(!root)
            return 0;
        if(!depth.count(root))
            depth[root] = 1 + max(countDepth(root->left), countDepth(root->right));
        return depth[root];
		
	private:
		unordered_map<TreeNode*, int> depth;
    }
};
```
</p>


