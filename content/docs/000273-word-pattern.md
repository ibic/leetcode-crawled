---
title: "Word Pattern"
weight: 273
#id: "word-pattern"
---
## Description
<div class="description">
<p>Given a <code>pattern</code> and a string <code>s</code>, find if <code>s</code>&nbsp;follows the same pattern.</p>

<p>Here <b>follow</b> means a full match, such that there is a bijection between a letter in <code>pattern</code> and a <b>non-empty</b> word in <code>s</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> pattern = &quot;abba&quot;, s = &quot;dog cat cat dog&quot;
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> pattern = &quot;abba&quot;, s = &quot;dog cat cat fish&quot;
<strong>Output:</strong> false
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> pattern = &quot;aaaa&quot;, s = &quot;dog cat cat dog&quot;
<strong>Output:</strong> false
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> pattern = &quot;abba&quot;, s = &quot;dog dog dog dog&quot;
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= pattern.length &lt;= 300</code></li>
	<li><code>pattern</code> contains only lower-case English letters.</li>
	<li><code>1 &lt;= s.length &lt;= 3000</code></li>
	<li><code>s</code> contains only lower-case English letters and spaces <code>&#39; &#39;</code>.</li>
	<li><code>s</code> <strong>does not contain</strong> any leading or trailing spaces.</li>
	<li>All the words in <code>s</code> are separated by a <strong>single space</strong>.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Capital One - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Dropbox - 0 (taggedByAdmin: true)
- Uber - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

This problem is similar to [Isomorphic Strings](https://leetcode.com/problems/isomorphic-strings/).

---

#### Approach 1: Two Hash Maps

**Intuition**

The most naive way to start thinking about this problem is to have a single hash map, tracking which character (in `pattern`) maps to what word (in `s`). As you scan each character-word pair, update this hash map for characters which are not in the mapping. If you see a character which already is one of the keys in mapping, check whether the current word matches with the word the character maps to. If they do not match, you can immediately return `False`, otherwise, just keep on scanning until the end.

This type of check will work well for cases such as:

* "abba" and "dog cat cat dog"  -> Returns `True`.
* "abba" and "dog cat cat fish" -> Returns `False`.

But it will fail for:

* "abba" and "dog dog dog dog"  -> Returns `True` (Expected `False`).

A fix for this is to have two hash maps, one for mapping characters to words and the other for mapping words to characters. While scanning each character-word pair,

* If the character is **NOT** in the character to word mapping, you additionally check whether that word is also in the word to character mapping.
    * If that word is already in the word to character mapping, then you can return `False` immediately since it has been mapped with some other character before.
    * Else, update both mappings.
* If the character **IS IN** in the character to word mapping, you just need to check whether the current word matches with the word which the character maps to in the character to word mapping. If not, you can return `False` immediately.

**Implementation**

<iframe src="https://leetcode.com/playground/nXfj9GmV/shared" frameBorder="0" width="100%" height="500" name="nXfj9GmV"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N)$$ where $$N$$ represents the number of words in `s` or the number of characters in `pattern`.

* Space complexity : $$O(M)$$ where $$M$$ represents the number of unique words in `s`. Even though we have two hash maps, the character to word hash map has space complexity of $$O(1)$$ since there can at most be 26 keys.

*Addendum:* Rather than keeping two hash maps, we can only keep character to word mapping and whenever we find a character that is not in the mapping, you can check whether the word in current character-word pair is already **one of the values** in the character to word mapping. However, this is trading time off for better space since checking for values in a hash map is a $$O(M)$$ operation where $$M$$ is the number of key value pairs in the hash map. Thus, if we decide to go this way, our time complexity will be $$O(NM)$$ where $$N$$ is the number of unique characters in `pattern`.

Another similar approach to Approach 1 would be using hash set to keep track of words which have been encountered. Instead of checking whether the word is already in the word to character mapping, you just need to check whether the word is in the encountered word hash set. And, rather than updating the word to character mapping, you just need to add the word to the encountered word hash set. Hash set would have a better practical space complexity even though the big-O space complexity for hash set and hash map is the same.

---

#### Approach 2: Single Index Hash Map

**Intuition**

Rather than having two hash maps, we can have a single index hash map which keeps track of the first occurrences of each character in `pattern` and each word in `s`. As we go through each character-word pair, we insert unseen characters from `pattern` and unseen words from `s`.

The goal is to make sure that the indices of each character and word match up. As soon as we find a mismatch, we can return `False`.

Let's go through some examples.

- `pattern`: 'abba'
- `s`: 'dog cat cat dog'

1. 'a' and 'dog' -> map_index = `{'a': 0, 'dog': 0}`
    * Index of 'a' and index of 'dog' are the same.
2. 'b' and 'cat' -> map_index = `{'a': 0, 'dog': 0, 'b': 1, 'cat': 1}`
    * Index of 'b' and index of 'cat' are the same.
3. 'b' and 'cat' -> map_index = `{'a': 0, 'dog': 0, 'b': 1, 'cat': 1}`
    * 'b' is already in the mapping, no need to update.
    * 'cat' is already in the mapping, no need to update.
    * Index of 'b' and index of 'cat' are the same.
4. 'a' and 'dog' -> map_index = `{'a': 0, 'dog': 0, 'b': 1,  'cat': 1}`
    * 'a' is already in the mapping, no need to update.
    * 'dog' is already in the mapping, no need to update.
    * Index of 'a' and index of 'dog' are the same.


- `pattern`: 'abba'
- `s`: 'dog cat fish dog'

1. 'a' and 'dog' -> map_index = `{'a': 0, 'dog': 0}`
    * Index of 'a' and index of 'dog' are the same.
2. 'b' and 'cat' -> map_index = `{'a': 0, 'dog': 0, 'b': 1, 'cat': 1}`
    * Index of 'b' and index of 'cat' are the same.
3. 'b' and 'fish' -> map_index = `{'a': 0, 'dog': 0, 'b': 1, 'cat': 1, 'fish': 2}`
    * 'b' is already in the mapping, no need to update.
    * Index of 'b' and index of 'fish' are NOT the same. Returns `False`.

**Implementation**

*Differentiating between character and string:* In Python there is no separate `char` type. And for cases such as:

- `pattern`: 'abba'
- `s`: 'b a a b'

Using the same hash map will not work properly. A workaround is to prefix each character in `pattern` with "char_" and each word in `s` with "word_".

<iframe src="https://leetcode.com/playground/H8oSksgW/shared" frameBorder="0" width="100%" height="480" name="H8oSksgW"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N)$$ where $$N$$ represents the number of words in the `s` or the number of characters in the `pattern`.

* Space complexity : $$O(M)$$ where $$M$$ is the number of unique characters in `pattern` and words in `s`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 8 lines simple Java
- Author: StefanPochmann
- Creation Date: Tue Oct 06 2015 00:32:59 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 00:08:09 GMT+0800 (Singapore Standard Time)

<p>
    public boolean wordPattern(String pattern, String str) {
        String[] words = str.split(" ");
        if (words.length != pattern.length())
            return false;
        Map index = new HashMap();
        for (Integer i=0; i<words.length; ++i)
            if (index.put(pattern.charAt(i), i) != index.put(words[i], i))
                return false;
        return true;
    }

I go through the pattern letters and words in parallel and compare the indexes where they last appeared.

**Edit 1:** Originally I compared the **first** indexes where they appeared, using `putIfAbsent` instead of `put`. That was based on [mathsam's solution](https://leetcode.com/discuss/36438/1-liner-in-python?show=39066#a39066) for the old [Isomorphic Strings](https://leetcode.com/problems/isomorphic-strings/) problem. But then [czonzhu's answer](https://leetcode.com/discuss/62374/9-lines-simple-java?show=62383#a62383) below made me realize that `put` works as well and why.

**Edit 2:** Switched from

        for (int i=0; i<words.length; ++i)
            if (!Objects.equals(index.put(pattern.charAt(i), i),
                                index.put(words[i], i)))
                return false;

to the current version with `i` being an `Integer` object, which allows to compare with just `!=` because there's no autoboxing-same-value-to-different-objects-problem anymore. Thanks to lap_218 for somewhat pointing that out in the comments.
</p>


### Short C++, read words on the fly
- Author: StefanPochmann
- Creation Date: Tue Oct 06 2015 10:00:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 05:40:48 GMT+0800 (Singapore Standard Time)

<p>
I think all previous C++ solutions read all words into a vector at the start. Here I read them on the fly.

    bool wordPattern(string pattern, string str) {
        map<char, int> p2i;
        map<string, int> w2i;
        istringstream in(str);
        int i = 0, n = pattern.size();
        for (string word; in >> word; ++i) {
            if (i == n || p2i[pattern[i]] != w2i[word])
                return false;
            p2i[pattern[i]] = w2i[word] = i + 1;
        }
        return i == n;
    }
</p>


### Short in Python
- Author: StefanPochmann
- Creation Date: Mon Oct 05 2015 22:13:39 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 03:06:30 GMT+0800 (Singapore Standard Time)

<p>
This problem is pretty much equivalent to [Isomorphic Strings](https://leetcode.com/problems/isomorphic-strings/). Let me reuse two old solutions.

From [here](https://leetcode.com/discuss/36438/1-liner-in-python?show=39070#c39070):

    def wordPattern(self, pattern, str):
        s = pattern
        t = str.split()
        return map(s.find, s) == map(t.index, t)

Improved version also from there:

    def wordPattern(self, pattern, str):
        f = lambda s: map({}.setdefault, s, range(len(s)))
        return f(pattern) == f(str.split())

From [here](https://leetcode.com/discuss/41379/1-line-in-python?show=41382#a41382):
        
    def wordPattern(self, pattern, str):
        s = pattern
        t = str.split()
        return len(set(zip(s, t))) == len(set(s)) == len(set(t)) and len(s) == len(t)

Thanks to zhang38 for pointing out the need to check len(s) == len(t) here.
</p>


