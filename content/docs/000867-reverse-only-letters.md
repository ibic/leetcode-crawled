---
title: "Reverse Only Letters"
weight: 867
#id: "reverse-only-letters"
---
## Description
<div class="description">
<p>Given a string <code>S</code>, return the &quot;reversed&quot; string where all characters that are not a letter&nbsp;stay in the same place, and all letters reverse their positions.</p>

<p>&nbsp;</p>

<div>
<div>
<div>
<ol>
</ol>
</div>
</div>
</div>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;ab-cd&quot;</span>
<strong>Output: </strong><span id="example-output-1">&quot;dc-ba&quot;</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">&quot;a-bC-dEf-ghIj&quot;</span>
<strong>Output: </strong><span id="example-output-2">&quot;j-Ih-gfE-dCba&quot;</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">&quot;Test1ng-Leet=code-Q!&quot;</span>
<strong>Output: </strong><span id="example-output-3">&quot;Qedo1ct-eeLg=ntse-T!&quot;</span>
</pre>

<p>&nbsp;</p>

<div>
<p><strong><span>Note:</span></strong></p>

<ol>
	<li><code>S.length &lt;= 100</code></li>
	<li><code>33 &lt;= S[i].ASCIIcode &lt;= 122</code>&nbsp;</li>
	<li><code>S</code> doesn&#39;t contain <code>\</code> or <code>&quot;</code></li>
</ol>
</div>
</div>
</div>
</div>
</div>

## Tags
- String (string)

## Companies
- Microsoft - 7 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Stack of Letters

**Intuition and Algorithm**

Collect the letters of `S` separately into a stack, so that popping the stack reverses the letters.  (Alternatively, we could have collected the letters into an array and reversed the array.)

Then, when writing the characters of `S`, any time we need a letter, we use the one we have prepared instead.

<iframe src="https://leetcode.com/playground/Ex9pP4wc/shared" frameBorder="0" width="100%" height="361" name="Ex9pP4wc"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `S`.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Reverse Pointer

**Intuition**

Write the characters of `S` one by one.  When we encounter a letter, we want to write the next letter that occurs if we iterated through the string backwards.

So we do just that: keep track of a pointer `j` that iterates through the string backwards.  When we need to write a letter, we use it.

<iframe src="https://leetcode.com/playground/ZCjokDYU/shared" frameBorder="0" width="100%" height="344" name="ZCjokDYU"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `S`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Two Pointers, One pass
- Author: lee215
- Creation Date: Sun Oct 07 2018 11:05:28 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 27 2020 14:43:34 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
Two pointers,
from the begin and the of the string.
find two letters and swap them.
<br>

# **Time Complexity**:
Time `O(N)`
<br>

# Use if clause inside while loop
**Java:**
```java
    public String reverseOnlyLetters(String S) {
        StringBuilder sb = new StringBuilder(S);
        for (int i = 0, j = S.length() - 1; i < j;) {
            if (!Character.isLetter(sb.charAt(i))) {
                ++i;
            } else if (!Character.isLetter(sb.charAt(j))) {
                --j;
            } else {
                sb.setCharAt(i, S.charAt(j));
                sb.setCharAt(j--, S.charAt(i++));
            }
        }
        return sb.toString();
    }
```

**C++:**
```cpp
    string reverseOnlyLetters(string S) {
        for (int i = 0, j = S.length() - 1; i < j;) {
            if (!isalpha(S[i]))
                ++i;
            else if (!isalpha(S[j]))
                --j;
            else
                swap(S[i++], S[j--]);
        }
        return S;
    }
```

**Python:**
```python
    def reverseOnlyLetters(self, S):
        S, i, j = list(S), 0, len(S) - 1
        while i < j:
            if not S[i].isalpha():
                i += 1
            elif not S[j].isalpha():
                j -= 1
            else:
                S[i], S[j] = S[j], S[i]
                i, j = i + 1, j - 1
        return "".join(S)
```
<br><br>

# Use while clause inside while loop
**C++:**
```cpp
    string reverseOnlyLetters(string S) {
        for (int i = 0, j = S.length() - 1; i < j; ++i, --j) {
            while (i < j && !isalpha(S[i])) ++i;
            while (i < j && !isalpha(S[j])) --j;
            swap(S[i], S[j]);
        }
        return S;
    }
```

**Java:**
```java
    public String reverseOnlyLetters(String S) {
        StringBuilder sb = new StringBuilder(S);
        for (int i = 0, j = S.length() - 1; i < j; ++i, --j) {
            while (i < j && !Character.isLetter(sb.charAt(i))) ++i;
            while (i < j && !Character.isLetter(sb.charAt(j))) --j;
            sb.setCharAt(i, S.charAt(j));
            sb.setCharAt(j, S.charAt(i));
        }
        return sb.toString();
    }
```

**Python:**
```python
    def reverseOnlyLetters(self, S):
        i, j = 0, len(S) - 1
        S = list(S)
        while i < j:
            while i < j and not S[i].isalpha(): i += 1
            while i < j and not S[j].isalpha(): j -= 1
            S[i], S[j] = S[j], S[i]
            i, j = i + 1, j - 1
        return "".join(S)
```

</p>


### My 2-liner Python stack solution 
- Author: cenkay
- Creation Date: Mon Oct 08 2018 18:27:49 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 16:58:08 GMT+0800 (Singapore Standard Time)

<p>
* Idea is very simple. 
* Go over the string and construct new string:
	*  by adding non-alphabetic characters or 
	*  dumping from "to be reversed" r stack.
```
class Solution:
    def reverseOnlyLetters(self, S):
        r = [s for s in S if s.isalpha()]
        return "".join(S[i] if not S[i].isalpha() else r.pop() for i in range(len(S)))
```
</p>


### Java Two Pointers
- Author: FLAGbigoffer
- Creation Date: Sun Oct 07 2018 11:47:11 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 01:11:24 GMT+0800 (Singapore Standard Time)

<p>
Thoughts: Usually how do we reverse string? Use two pointers and swap those characters that were pointed to. Two pointers solution is still working in this case.

1. Two pointers solution:
```
class Solution {
    public String reverseOnlyLetters(String S) {
        char[] chars = S.toCharArray();
        int lo = 0, hi = S.length() - 1;
        
        while (lo < hi) {
            while (lo < hi && !Character.isLetter(chars[lo])) {
                lo++;
            }
            while (lo < hi && !Character.isLetter(chars[hi])) {
                hi--;
            }
            swap(chars, lo, hi);
            lo++;
            hi--;
        }
        
        return String.valueOf(chars);
    }
    
    private void swap(char[] chars, int i, int j) {
        char temp = chars[i];
        chars[i] = chars[j];
        chars[j] = temp;
    }
}
````

2. Two Pass Straightforward Solution (Not good)
```
class Solution {
    public String reverseOnlyLetters(String S) {
        int len = S.length();
        char[] chars = new char[len];
        
        for (int i = 0; i < len; i++) {
            if (!Character.isLetter(S.charAt(i))) {
                chars[i] = S.charAt(i);
            }
        }
        
        int j = 0;
        for (int i = len - 1; i >= 0; i--) {
            if (Character.isLetter(S.charAt(i))) {
                while (chars[j] != \'\u0000\') j++;
                chars[j] = S.charAt(i);
                j++;
            }
        }
        
        return String.valueOf(chars);
    }
}
````
</p>


