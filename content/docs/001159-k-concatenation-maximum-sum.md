---
title: "K-Concatenation Maximum Sum"
weight: 1159
#id: "k-concatenation-maximum-sum"
---
## Description
<div class="description">
<p>Given an integer array <code>arr</code>&nbsp;and an integer <code>k</code>, modify the array by repeating it <code>k</code> times.</p>

<p>For example, if <code>arr&nbsp;= [1, 2]</code> and <code>k = 3 </code>then the modified array will be <code>[1, 2, 1, 2, 1, 2]</code>.</p>

<p>Return the maximum sub-array sum in the modified array. Note that the length of the sub-array can be <code>0</code>&nbsp;and its sum in that case is <code>0</code>.</p>

<p>As the answer can be very large, return the answer&nbsp;<strong>modulo</strong>&nbsp;<code>10^9 + 7</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2], k = 3
<strong>Output:</strong> 9
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,-2,1], k = 5
<strong>Output:</strong> 2
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [-1,-2], k = 7
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= k &lt;= 10^5</code></li>
	<li><code>-10^4 &lt;= arr[i] &lt;= 10^4</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution(Kadens Algo) with Explanation
- Author: Poorvank
- Creation Date: Sun Sep 15 2019 12:01:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 15 2019 12:20:34 GMT+0800 (Singapore Standard Time)

<p>
Explanation:
 The problem is to find the maximum sub array of *concatenatedarr*. 
 Maximum SubArray of an array A is a continuous SubArray within the array A that has the largest Sum. 
 The best method for finding Maximum SubArray is Kadanae\'s algorithm.
 
 Here you have to find the Maximum SubArray for an array concatenated_arr which is a k-times repetitive array of A. For e.g.. if A is {3, 2, -1} and K is 3 then B will be {3, 2, -1, 3, 2, -1, 3, 2, -1}. Method:
The maximum SubArray of concatenated_arr can be the sum of all its elements. 
For e.g.. if A is {3, 2, -1} and K is 3, then B will be {3, 2, -1, 3, 2, -1, 3, 2, -1}. 
The sum of all the elements in concatenated_arr will give us 12. To find this one we don\'t need to create the array concatenated_arr. 
We can simply find the sum of all the elements in array A and we can mutilply it with K. 
 But wait, we can omit the last term in it so that the sum will become 13. 
For this one we can use prefix and suffix calculations. 
Eg:
 A is repeated k times in *concatenatedarr*. 
Consider the first repetition of A is A1, second is A2 and so on. So now our B array(if K=8) will be {A1, A2, A3, A4, A5, A6, A7, A8}. If you omit the first two elements in A1 and the last two elements in A8, you might also get the maxsub array.
So here we can check whether it is possible to omit some initial elements in A1 and some Final elements in A8. We use prefix and suffix variables for that to calculate the sum of A1 and A10 specifically and he adds the remaining elements i.e answer = {prefix + sum_of_elements(A2) + sum_of_elements(A3) + sum_of_elements(A4) + sum_of_elements(A5) + sum_of_elements(A6) + sum_of_elements(A7) + suffix} , which in simplified form becomes {prefix + sum_of_elements(A)*(k-2) + suffix}. 

```
 int mod  = (int) Math.pow(10,9)+7;
    public int kConcatenationMaxSum(int[] ar, int k) {
        long kadanes= kadanesAlgo(ar);
        if(k==1){
            return (int)kadanes;
        }
        long prefixSum= prefixSum(ar);
        long suffixSum=suffixSum(ar);
        long sum=0;
        for (int i1 : ar) {
            sum += i1;
        }
        if(sum>0){
            return (int)(Math.max(((sum*(k-2))%mod+suffixSum%mod+prefixSum%mod)%mod,kadanes%mod));
        }
        else{
            return (int)(Math.max((prefixSum%mod+suffixSum%mod)%mod,kadanes%mod));
        }

    }
    public  long kadanesAlgo(int[] ar){

        long currentSum=0;
        long maxSum=Integer.MIN_VALUE;
        for(int i=0;i<ar.length;i++){
            currentSum=currentSum>0?(currentSum+ar[i])%mod:ar[i];
            maxSum=Math.max(currentSum,maxSum);
        }
        return maxSum<0?0:maxSum%mod;

    }


    public  long prefixSum(int[] ar){

        long currentSum=0;
        long maxSum=Integer.MIN_VALUE;
        for(int i=0;i<ar.length;i++){
            currentSum= (currentSum+ar[i])%mod;
            maxSum=Math.max(maxSum,currentSum);
        }
        return maxSum;


    }

    public  long suffixSum(int[] ar){


        long currentSum=0;
        long maxSum=Integer.MIN_VALUE;
        for(int i=ar.length-1;i>=0;i--){
            currentSum=(currentSum+ar[i])%mod;
            maxSum=Math.max(currentSum,maxSum);
        }
        return maxSum;

    }
```
</p>


### Short and concise O(N) C++ solution
- Author: mzchen
- Creation Date: Mon Sep 16 2019 01:16:46 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 01 2019 06:03:55 GMT+0800 (Singapore Standard Time)

<p>
We only need to consider  `k = 1` and `k = 2` then it becomes a classic DP problem - Maximum Subarray Sum.
```
int kConcatenationMaxSum(vector<int>& arr, int k) {
    int n = arr.size(), sum = arr[0], mx = arr[0];
    int64_t total = accumulate(arr.begin(), arr.end(), 0), mod = 1e9+7;
    for (int i = 1; i < n * min(k, 2); i++) {
        sum = max(arr[i % n], sum + arr[i % n]);
        mx = max(mx, sum);
    }
    return max<int64_t>({0, mx, total * max(0, k - 2) + mx}) % mod;
}
```
</p>


### [Python3] 6-liner Kadane
- Author: cenkay
- Creation Date: Sun Sep 15 2019 22:58:02 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 16 2019 13:59:12 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def kConcatenationMaxSum(self, arr: List[int], k: int, mod = 10 ** 9 + 7) -> int:
        def Kadane(arr, res = 0, cur = 0):
            for num in arr:
                cur = max(num, num + cur)
                res = max(res, cur)
            return res
        return ((k - 2) * max(sum(arr), 0) + Kadane(arr * 2)) % mod if k > 1 else Kadane(arr) % mod
```
</p>


