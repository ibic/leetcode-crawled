---
title: "User Purchase Platform"
weight: 1533
#id: "user-purchase-platform"
---
## Description
<div class="description">
<p>Table: <code>Spending</code></p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| user_id     | int     |
| spend_date  | date    |
| platform    | enum    | 
| amount      | int     |
+-------------+---------+
The table logs the spendings history of users that make purchases from an online shopping website which has a desktop and a mobile application.
(user_id, spend_date, platform) is the primary key of this table.
The platform column is an ENUM type of (&#39;desktop&#39;, &#39;mobile&#39;).
</pre>

<p>Write an SQL query to find the total number of users and the total amount spent&nbsp;using mobile <strong>only</strong>, desktop <strong>only</strong> and <strong>both</strong> mobile and desktop together for each date.</p>

<p>The query result format is in the following example:</p>

<pre>
<code>Spending</code> table:
+---------+------------+----------+--------+
| user_id | spend_date | platform | amount |
+---------+------------+----------+--------+
| 1       | 2019-07-01 | mobile   | 100    |
| 1       | 2019-07-01 | desktop  | 100    |
| 2       | 2019-07-01 | mobile   | 100    |
| 2       | 2019-07-02 | mobile   | 100    |
| 3       | 2019-07-01 | desktop  | 100    |
| 3       | 2019-07-02 | desktop  | 100    |
+---------+------------+----------+--------+

Result table:
+------------+----------+--------------+-------------+
| spend_date | platform | total_amount | total_users |
+------------+----------+--------------+-------------+
| 2019-07-01 | desktop  | 100          | 1           |
| 2019-07-01 | mobile   | 100          | 1           |
| 2019-07-01 | both     | 200          | 1           |
| 2019-07-02 | desktop  | 100          | 1           |
| 2019-07-02 | mobile   | 100          | 1           |
| 2019-07-02 | both     | 0            | 0           |
+------------+----------+--------------+-------------+ 
On 2019-07-01, user 1 purchased using <strong>both</strong> desktop and mobile, user 2 purchased using mobile <strong>only</strong> and user 3 purchased using desktop <strong>only</strong>.
On 2019-07-02, user 2 purchased using mobile <strong>only</strong>, user 3 purchased using desktop <strong>only</strong> and no one purchased using <strong>both</strong> platforms.</pre>

</div>

## Tags


## Companies
- LinkedIn - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL Solution With Explanations (Faster Than 100%)
- Author: zac4
- Creation Date: Fri Jul 19 2019 16:34:09 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 19 2019 22:29:35 GMT+0800 (Singapore Standard Time)

<p>
Let\'s start with a simple preprocess:
```SQL
SELECT
  spend_date,
  user_id,
  SUM(CASE platform WHEN \'mobile\' THEN amount ELSE 0 END) mobile_amount,
  SUM(CASE platform WHEN \'desktop\' THEN amount ELSE 0 END) desktop_amount
FROM Spending
GROUP BY spend_date, user_id
```

For each user in each day, we fetch its `mobile_amount` and `desktop_amount` respectively and output them into a single row. In this form, we can see a user belongs to which platform very clearly:
| spend_date | user_id | mobile_amount | desktop_amount | ->_(platform)_ |
|------------|---------|---------------|----------------|----------------|
| 2019-07-01 | 1       | 100           | 100            | -> _(both)_    |
| 2019-07-01 | 2       | 100           | 0              | -> _(mobile)_  |
| 2019-07-01 | 3       | 0             | 100            | -> _(desktop)_ |
| 2019-07-02 | 2       | 100           | 0              | -> _(mobile)_  |
| 2019-07-02 | 3       | 0             | 100            | ->_(desktop)_  |

Based on the above table, we use the following SQL to bind users to their platforms and calculate the amounts spent:
```SQL
SELECT
    spend_date,
    user_id,
    IF(mobile_amount > 0, IF(desktop_amount > 0, \'both\', \'mobile\'), \'desktop\') platform,
    (mobile_amount + desktop_amount) amount
FROM (
	...
) o
```
Result table:
| spend_date | user_id | platform | amount |
|------------|---------|----------|--------|
| 2019-07-01 | 1       | both     | 200    |
| 2019-07-01 | 2       | mobile   | 100    |
| 2019-07-01 | 3       | desktop  | 100    |
| 2019-07-02 | 2       | mobile   | 100    |
| 2019-07-02 | 3       | desktop  | 100    |

We don\'t wanna miss any record which has ZERO `total_amount` and `total_users`. So we need to get all combinations of `spend_date` and `platform`:
```SQL
SELECT DISTINCT(spend_date), \'desktop\' platform FROM Spending
UNION
SELECT DISTINCT(spend_date), \'mobile\' platform FROM Spending
UNION
SELECT DISTINCT(spend_date), \'both\' platform FROM Spending
```
The output:
| spend_date | platform |
|------------|----------|
| 2019-07-01 | desktop  |
| 2019-07-01 | mobile  |
| 2019-07-01 | both   |
| 2019-07-02 | desktop  |
| 2019-07-02 | mobile  |
| 2019-07-02 | both   |

After joinning this table to the previous one, we have our __final answer__:
```SQL
SELECT 
    p.spend_date,
    p.platform,
    IFNULL(SUM(amount), 0) total_amount,
    COUNT(user_id) total_users
FROM 
(
    SELECT DISTINCT(spend_date), \'desktop\' platform FROM Spending
    UNION
    SELECT DISTINCT(spend_date), \'mobile\' platform FROM Spending
    UNION
    SELECT DISTINCT(spend_date), \'both\' platform FROM Spending
) p 
LEFT JOIN (
    SELECT
        spend_date,
        user_id,
        IF(mobile_amount > 0, IF(desktop_amount > 0, \'both\', \'mobile\'), \'desktop\') platform,
        (mobile_amount + desktop_amount) amount
    FROM (
        SELECT
          spend_date,
          user_id,
          SUM(CASE platform WHEN \'mobile\' THEN amount ELSE 0 END) mobile_amount,
          SUM(CASE platform WHEN \'desktop\' THEN amount ELSE 0 END) desktop_amount
        FROM Spending
        GROUP BY spend_date, user_id
    ) o
) t
ON p.platform=t.platform AND p.spend_date=t.spend_date
GROUP BY spend_date, platform
```

</p>


### MySQL simplest solution
- Author: HanboSun
- Creation Date: Thu Jul 25 2019 12:03:28 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 25 2019 12:03:28 GMT+0800 (Singapore Standard Time)

<p>
```
# Write your MySQL query statement below

select c.spend_date, c.platform, sum(coalesce(amount,0)) total_amount, sum(case when amount is null then 0 else 1 end) total_users 
    from
    
    (select distinct spend_date, \'desktop\' platform from spending 
    union all
    select distinct spend_date, \'mobile\' platform from spending 
    union all
    select distinct spend_date, \'both\' platform from spending) c
    
    left join
    
    (select user_id, spend_date, case when count(*)=1 then platform else \'both\' end platform, sum(amount) amount 
        from spending group by user_id, spend_date) v
    
    on c.spend_date=v.spend_date and c.platform=v.platform
    group by spend_date, platform;

```
</p>


### MS SQL CTE + UNION ALL
- Author: serpol
- Creation Date: Wed Oct 23 2019 21:46:01 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 24 2019 16:16:15 GMT+0800 (Singapore Standard Time)

<p>
```
WITH CTE AS (SELECT user_id, spend_date, 
    SUM(CASE platform WHEN \'mobile\' THEN amount ELSE 0 END) ma,
    SUM(CASE platform WHEN \'desktop\' THEN amount ELSE 0 END) da
FROM Spending
GROUP BY user_id, spend_date)
SELECT spend_date, \'desktop\' platform, 
    SUM(CASE WHEN da > 0 AND ma = 0 THEN da ELSE 0 END) total_amount, 
    SUM(CASE WHEN da > 0 AND ma = 0 THEN 1 ELSE 0 END) total_users
FROM CTE
GROUP BY spend_date
UNION ALL
SELECT spend_date, \'mobile\' platform, 
	 SUM(CASE WHEN ma > 0 AND da = 0 THEN ma ELSE 0 END) total_amount, 
	 SUM(CASE WHEN ma > 0 AND da = 0 THEN 1 ELSE 0 END) total_users
FROM CTE
GROUP BY spend_date
UNION ALL
SELECT spend_date, \'both\' platform, 
    SUM(CASE WHEN da > 0 AND ma > 0 THEN ma + da ELSE 0 END) total_amount, 
    SUM(CASE WHEN da > 0 AND ma > 0 THEN 1 ELSE 0 END) total_users
FROM CTE
GROUP BY spend_date
```
</p>


