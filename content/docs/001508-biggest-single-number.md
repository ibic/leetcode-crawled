---
title: "Biggest Single Number"
weight: 1508
#id: "biggest-single-number"
---
## Description
<div class="description">
<p>Table <code>my_numbers</code> contains many numbers in column <b>num</b> including duplicated ones.<br />
Can you write a SQL query to find the biggest number, which only appears once.</p>

<pre>
+---+
|num|
+---+
| 8 |
| 8 |
| 3 |
| 3 |
| 1 |
| 4 |
| 5 |
| 6 | 
</pre>
For the sample data above, your query should return the following result:

<pre>
+---+
|num|
+---+
| 6 |
</pre>
<b>Note:</b><br />
If there is no such number, just output <b>null</b>.

<p>&nbsp;</p>

</div>

## Tags


## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach: Using **subquery** and `MAX()` function [Accepted]

**Algorithm**

Use subquery to select all the numbers appearing just one time.

<iframe src="https://leetcode.com/playground/gCbEq9kC/shared" frameBorder="0" name="gCbEq9kC" width="100%" height="156"></iframe>

Then choose the biggest one using `MAX()`.
<iframe src="https://leetcode.com/playground/rYpoAo97/shared" frameBorder="0" name="rYpoAo97" width="100%" height="224"></iframe>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Concise MySQL solution with reference
- Author: zhutou7
- Creation Date: Mon Jun 12 2017 10:55:47 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 17:38:42 GMT+0800 (Singapore Standard Time)

<p>
```
# Refer to: https://stackoverflow.com/questions/17250243/how-to-return-null-when-result-is-empty
select(
  select num
  from number
  group by num
  having count(*) = 1
  order by num desc limit 1
) as num;
```
</p>


### Why this is not right?
- Author: JocelynLee
- Creation Date: Sun Jun 25 2017 05:01:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 25 2017 05:01:23 GMT+0800 (Singapore Standard Time)

<p>
1. 

select max(num) from number 
group by num
having count(num)=1
order by num desc

Output:
{"headers": ["max(num)"], "values": [[1], [4], [5], [6]]}

why this is not right?? 

2. 

select num from number 
group by num
having count(num)=1
order by num desc limit 1


{"headers": ["num"], "values": []}

why this couldn't return NULL?
</p>


### So many ways to do it. Used If function
- Author: sarayamato
- Creation Date: Fri Jun 16 2017 12:20:48 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 12:14:20 GMT+0800 (Singapore Standard Time)

<p>
Since no one has posted this one.
```
select if(count(*) =1, num, null) as num from number 
group by num order by count(*), num desc limit 1
```
</p>


