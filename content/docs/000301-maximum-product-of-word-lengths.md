---
title: "Maximum Product of Word Lengths"
weight: 301
#id: "maximum-product-of-word-lengths"
---
## Description
<div class="description">
<p>Given a string array <code>words</code>, find the maximum value of <code>length(word[i]) * length(word[j])</code> where the two words do not share common letters. You may assume that each word will contain only lower case letters. If no such two words exist, return 0.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> <code>[&quot;abcw&quot;,&quot;baz&quot;,&quot;foo&quot;,&quot;bar&quot;,&quot;xtfn&quot;,&quot;abcdef&quot;]</code>
<b>Output: </b><code>16 
<strong>Explanation: </strong></code>The two words can be <code>&quot;abcw&quot;, &quot;xtfn&quot;</code><span style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;">.</span></pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> <code>[&quot;a&quot;,&quot;ab&quot;,&quot;abc&quot;,&quot;d&quot;,&quot;cd&quot;,&quot;bcd&quot;,&quot;abcd&quot;]</code>
<b>Output: </b><code>4 
<strong>Explanation: </strong></code>The two words can be <code>&quot;ab&quot;, &quot;cd&quot;</code><span style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;">.</span></pre>

<p><b>Example 3:</b></p>

<pre>
<b>Input:</b> <code>[&quot;a&quot;,&quot;aa&quot;,&quot;aaa&quot;,&quot;aaaa&quot;]</code>
<b>Output: </b><code>0 
<strong>Explanation: </strong></code>No such pair of words.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= words.length &lt;= 10^3</code></li>
	<li><code>0 &lt;= words[i].length &lt;= 10^3</code></li>
	<li><code>words[i]</code> consists only of lowercase English letters.</li>
</ul>

</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Google - 4 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

Let's start from the naive straightforward solution.

> Compare each word with all the following words one by one.
If two words have no common letters, update the maximum product `maxProd`.

Let's omit for the moment the implementation of 
`noCommonLetters` function.

<iframe src="https://leetcode.com/playground/aeBWrwcF/shared" frameBorder="0" width="100%" height="344" name="aeBWrwcF"></iframe>

The number of operations performed in the nested loops is
 
$$(N - 1) + (N - 2) + ... + 2 + 1 = \frac{N(N - 1)}{2}$$

that results in $$\mathcal{O}(N^2 \times f(L_1, L_2))$$ time
complexity. Here $$f(L_1, L_2)$$ is a complexity 
of function `noCommonLetters(String s1, String s2)`,
i.e. the price to compare two words of lengths $$L_1$$
and $$L_2$$.

> What could be done here?

![fig](../Figures/318/methods2.png)

- Approach 1: minimize time complexity $$f(L_1, L_2)$$ of 
`noCommonLetters` function.

- Approach 2: minimize the number of word comparisons.
There is no need to always perform $$\mathcal{O}(N^2)$$
comparisons. Among all the strings with the same set of letters
($$ab$$, $$aaaaabaabaaabb$$, $$bbabbabba$$) 
it's enough to keep the longest one ($$aaaaabaabaaabb$$). 
<br /> 
<br />


---
#### Approach 1: Optimize noCommonLetters function : Bitmasks + Precomputation

The idea is to minimize first the time complexity 
$$f(L_1, L_2)$$ of word comparison.

![fig](../Figures/318/methods.png)

**Naive Solution : $$\mathcal{O}(L_1 \times L_2)$$ time**

This naive solution is simple but not optimal.
Check the characters in the first word one by one. 
For each character ensure that this character is _not_ in the second word.

<iframe src="https://leetcode.com/playground/aTbSjZMF/shared" frameBorder="0" width="100%" height="140" name="aTbSjZMF"></iframe>
 
**Bitmasks : $$\mathcal{O}(L_1 + L_2)$$ time**

More elegant and fast solution would be to use bitmasks.

Words contain only lower case letters and hence 
an absence or presence of each letter in a word could be encoded 
with a bitmask of 26 elements. 
Let's set bit number 0 equal to 1 if character `a` is present in 
the word, and to 0 otherwise.
Now bit number 1. Let's set it equal to 1 if character `b` is present in 
the word, and to 0 otherwise. And so on and so forth, till
the bit number 26 which is equal to 1 if `z` is present in the word.

![fig](../Figures/318/n_th.png)

> How to set n-th bit? Use standard bitwise trick : `n_th_bit = 1 << n`.

> How to compute bitmask for a word? Iterate over the word, letter by letter,
compute bit number corresponding to that letter `n = (int)ch - (int)'a'`,
and add this n-th bit `n_th_bit = 1 << n` into bitmask 
`bitmask |= n_th_bit`.

![fig](../Figures/318/bitmask.png)

This way one could compute two bitmasks, character by character, in 
$$\mathcal{O}(L_1 + L_2)$$ time. Then the word comparison itself could be done
in one operation and in a constant time.

<iframe src="https://leetcode.com/playground/NJco7YsM/shared" frameBorder="0" width="100%" height="276" name="NJco7YsM"></iframe>

**Bitmasks + Precomputation : Comparison in $$\mathcal{O}(1)$$ time**

In the previous approach one computes a bitmask of
each word N times. In fact, each bitmask could be precomputed just once, 
memorised and then used for the runtime comparison 
in a constant time.

Let's use two integer arrays to store bitmasks and string lengths. 
That's Java-optimised way, since in general Java works faster with 
arrays than with hashmaps. 

**Algorithm**

- Precompute bitmasks for all words and save them in the array `masks`.
Use array `lens` to keep the lengths for all words.

- Compare each word with all the following words one by one.
If two words have no common letters, update the maximum product `maxProd`.
Perform "no common letters" check in a constant time with the help of
precomputed `masks` array: 
`(masks[i] & masks[j]) == 0`.

- Return `maxProd`.

**Implementation**

<iframe src="https://leetcode.com/playground/b8Hwy9oH/shared" frameBorder="0" width="100%" height="500" name="b8Hwy9oH"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N^2 + L)$$ where $$N$$ is a number of words
and $$L$$ is a total length of all words together. The precomputation takes
$$\mathcal{O}(L)$$ time because we iterate over all characters
in all words. 
The runtime word comparison takes $$\mathcal{O}(N^2)$$ time.
In total, that results in $$\mathcal{O}(N^2 + L)$$ time complexity.

* Space complexity : $$\mathcal{O}(N)$$ to keep two arrays of N elements.
<br /> 
<br />


---
#### Approach 2: Optimise Number of Comparisons : Bitmasks + Precomputation + Hashmap

Now, when the comparison itself is optimised, one could optimise the number 
of comparisons.
There is no need to always perform $$\mathcal{O}(N^2)$$
comparisons. Among all the strings with the same set of letters
($$ab$$, $$aaaaabaabaaabb$$, $$bbabbabba$$) 
it's enough to keep the longest one ($$aaaaabaabaaabb$$). 

For that, instead of two arrays of length $$N$$ as in the Approach 1, one
could use a hashmap : bitmask -> max word length with that bitmask.

![fig](../Figures/318/same.png)

This way the total number of word comparisons could be reduced,
that speeds up the solution in Python.
Note, that for Java this way is not the optimal one 
because of known problems with 
[HashMap performance](https://github.com/vavr-io/vavr/issues/571).  

**Algorithm**

- Precompute bitmasks for all words and save them in the hashmap
bitmask -> max word length with such a bitmask. (There could be
several words with the same bitmask, for example, "a" and "aaaaaaa").

- Compare each word with all the following words one by one.
If two words have no common letters, update the maximum product `maxProd`.
Perform "no common letters" check in a constant time with the help of
precomputed hashmap of bitmasks: 
`(x & y) == 0`.

- Return `maxProd`.

**Implementation**

<iframe src="https://leetcode.com/playground/qHJiK5Yx/shared" frameBorder="0" width="100%" height="500" name="qHJiK5Yx"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N^2 + L)$$ where N is a number of words
and L is a total length of all words together. If you want to have some fun,
here is a [bloody discussion](https://leetcode.com/problems/maximum-product-of-word-lengths/discuss/76976/Bit-shorter-C++/80869) 
that all this is for "small" N only, when 
$$N < 2^{26}$$. The idea is that the number of bitmasks is not more than
$$2^{26}$$ and hence for $$N > 2^{26}$$ the complexity is 
$$\mathcal{O}(L)$$. 

* Space complexity : $$\mathcal{O}(N)$$ to keep a hashmap of N elements
if $$N < 2^{26}$$. Otherwise, it's $$\mathcal{O}(2^{26})$$ = $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### JAVA----------Easy Version To Understand!!!!!!!!!!!!!!!!!
- Author: HelloWorld123456
- Creation Date: Thu Jan 28 2016 00:37:06 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 16:23:15 GMT+0800 (Singapore Standard Time)

<p>
    	public static int maxProduct(String[] words) {
		if (words == null || words.length == 0)
			return 0;
		int len = words.length;
		int[] value = new int[len];
		for (int i = 0; i < len; i++) {
			String tmp = words[i];
			value[i] = 0;
			for (int j = 0; j < tmp.length(); j++) {
				value[i] |= 1 << (tmp.charAt(j) - 'a');
			}
		}
		int maxProduct = 0;
		for (int i = 0; i < len; i++)
			for (int j = i + 1; j < len; j++) {
				if ((value[i] & value[j]) == 0 && (words[i].length() * words[j].length() > maxProduct))
					maxProduct = words[i].length() * words[j].length();
			}
		return maxProduct;
	}
</p>


### Bit shorter C++
- Author: StefanPochmann
- Creation Date: Wed Dec 16 2015 08:32:46 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 16:23:33 GMT+0800 (Singapore Standard Time)

<p>
Same algorithm as most, just written a bit shorter.

    int maxProduct(vector<string>& words) {
        vector<int> mask(words.size());
        int result = 0;
        for (int i=0; i<words.size(); ++i) {
            for (char c : words[i])
                mask[i] |= 1 << (c - 'a');
            for (int j=0; j<i; ++j)
                if (!(mask[i] & mask[j]))
                    result = max(result, int(words[i].size() * words[j].size()));
        }
        return result;
    }

**Update:** Here's an O(n+N) variation, where n is the number of words and N is the total number of characters in all words. Thanks to junhuangli for the suggestion.

    int maxProduct(vector<string>& words) {
        unordered_map<int,int> maxlen;
        for (string word : words) {
            int mask = 0;
            for (char c : word)
                mask |= 1 << (c - 'a');
            maxlen[mask] = max(maxlen[mask], (int) word.size());
        }
        int result = 0;
        for (auto a : maxlen)
            for (auto b : maxlen)
                if (!(a.first & b.first))
                    result = max(result, a.second * b.second);
        return result;
    }

**Or:** (thanks to junhuangli's further comment)

    int maxProduct(vector<string>& words) {
        unordered_map<int,int> maxlen;
        int result = 0;
        for (string word : words) {
            int mask = 0;
            for (char c : word)
                mask |= 1 << (c - 'a');
            maxlen[mask] = max(maxlen[mask], (int) word.size());
            for (auto maskAndLen : maxlen)
                if (!(mask & maskAndLen.first))
                    result = max(result, (int) word.size() * maskAndLen.second);
        }
        return result;
    }
</p>


### 32ms Java AC solution
- Author: cc9208
- Creation Date: Wed Dec 16 2015 09:52:09 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 17:26:24 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public int maxProduct(String[] words) {
            int max = 0;

            Arrays.sort(words, new Comparator<String>(){
                public int compare(String a, String b){
                    return b.length() - a.length();
                }
            });
    
            int[] masks = new int[words.length]; // alphabet masks

            for(int i = 0; i < masks.length; i++){
                for(char c: words[i].toCharArray()){
                    masks[i] |= 1 << (c - 'a');
                }
            }
        
            for(int i = 0; i < masks.length; i++){
                if(words[i].length() * words[i].length() <= max) break; //prunning
                for(int j = i + 1; j < masks.length; j++){
                    if((masks[i] & masks[j]) == 0){
                        max = Math.max(max, words[i].length() * words[j].length());
                        break; //prunning
                    }
                }
            }

            return max;
        }
    }
</p>


