---
title: "Distribute Candies to People"
weight: 1083
#id: "distribute-candies-to-people"
---
## Description
<div class="description">
<p>We distribute some&nbsp;number of <code>candies</code>, to a row of <strong><code>n =&nbsp;num_people</code></strong>&nbsp;people in the following way:</p>

<p>We then give 1 candy to the first person, 2 candies to the second person, and so on until we give <code>n</code>&nbsp;candies to the last person.</p>

<p>Then, we go back to the start of the row, giving <code>n&nbsp;+ 1</code> candies to the first person, <code>n&nbsp;+ 2</code> candies to the second person, and so on until we give <code>2 * n</code>&nbsp;candies to the last person.</p>

<p>This process repeats (with us giving one more candy each time, and moving to the start of the row after we reach the end) until we run out of candies.&nbsp; The last person will receive all of our remaining candies (not necessarily one more than the previous gift).</p>

<p>Return an array (of length <code>num_people</code>&nbsp;and sum <code>candies</code>) that represents the final distribution of candies.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> candies = 7, num_people = 4
<strong>Output:</strong> [1,2,3,1]
<strong>Explanation:</strong>
On the first turn, ans[0] += 1, and the array is [1,0,0,0].
On the second turn, ans[1] += 2, and the array is [1,2,0,0].
On the third turn, ans[2] += 3, and the array is [1,2,3,0].
On the fourth turn, ans[3] += 1 (because there is only one candy left), and the final array is [1,2,3,1].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> candies = 10, num_people = 3
<strong>Output:</strong> [5,2,3]
<strong>Explanation: </strong>
On the first turn, ans[0] += 1, and the array is [1,0,0].
On the second turn, ans[1] += 2, and the array is [1,2,0].
On the third turn, ans[2] += 3, and the array is [1,2,3].
On the fourth turn, ans[0] += 4, and the final array is [5,2,3].
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>1 &lt;= candies &lt;= 10^9</li>
	<li>1 &lt;= num_people &lt;= 1000</li>
</ul>

</div>

## Tags
- Math (math)

## Companies


## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Sum of Arithmetic Progression

**Intuition**

That sort of "Math" questions is to check on how far one 
could simplify the problem even before starting to code.

Naive idea here is to jump into code and start to give candies 
in a loop till the end of candies. Time complexity of such a 
solution would be $$\mathcal{O}(\max(G, N))$$, where G is a number of gifts 
and N is a number of people.

More elegant way would be to notice that candies distribution 
could be described by a simple formula. Using that formula 
one could solve the problem in $$\mathcal{O}(N)$$ time
by the straightforward generation of final distribution array.

Let's derive that formula step by step.

**Number of persons with complete gifts**

Candies gifts, except the last gift which contains the remaining,
represent the arithmetic progression of natural numbers. 

![fig](../Figures/1103/arithmeti.png)

Let's assume that the progression has `p` elements,
then the remaining is just a difference between number of candies $$C$$
and sum of the progression elements

$$
\textrm{remaining} = C - \sum\limits_{k = 0}^{k = p}{k}
$$

Sum of the natural numbers progression is a 
[school knowledge](https://en.wikipedia.org/wiki/1_%2B_2_%2B_3_%2B_4_%2B_%E2%8B%AF),
and remaining could be rewritten as

$$
\textrm{remaining} = C - \frac{p(p + 1)}{2}
$$

It's known that remaining is larger or equal to 0 and smaller than 
the next progression number $$p + 1$$.

$$
0 \le C - \frac{p(p + 1)}{2} < p + 1
$$

Simple calculations results in 

$$
\sqrt{2C + \frac{1}{4}} - \frac{3}{2} < p \le \sqrt{2C + \frac{1}{4}} - \frac{1}{2}
$$

There is only one integer in this interval, and hence now
one knows the number of elements in the arithmetic progression

$$
p = \textrm{floor}\left(\sqrt{2C + \frac{1}{4}} - \frac{1}{2}\right)
$$

![fig](../Figures/1103/number.png)

**Candies gain during the complete turns**

Now one could compute a number of complete turns when all N persons
received a gift : `rows = p / N`. 

During complete turns person number `i` receives in total 

$$
d[i] = i + (i + N) + (i + 2N) + ... (i + (\textrm{rows} - 1) N) = 
i \times \textrm{rows} + N \frac{\textrm{rows}(\textrm{rows} - 1)}{2}
$$

![fig](../Figures/1103/complete.png)

**Candies gain during the incomplete turn**

The last turn could be incomplete, i.e. not all persons receive 
their gifts.

One could compute a number of persons which received a complete
gift : `cols = p % N`. These persons will receive one turn more candies 

$$
d[i] += i + N \times \textrm{rows}
$$

And the last person with a gift will receive all remaining candies

$$
d[\textrm{cols} + 1] += \textrm{remaining}
$$

![fig](../Figures/1103/incomplete.png)

That's all, all distributed candies are computed.

**Algorithm**

- Compute number of persons with complete gifts

$$
p = \textrm{floor}\left(\sqrt{2C + \frac{1}{4}} - \frac{1}{2}\right)
$$

and the last gift $$\textrm{remaining} = C - \frac{p(p + 1)}{2}$$.

- Compute the number of complete turns, when all persons receive their 
gifts : `rows = p // n`, and candies gain from these turns :
$$
d[i] = i \times \textrm{rows} + N \frac{\textrm{rows}(\textrm{rows} - 1)}{2}
$$

- Add one turn more candies to first `p % N` persons participated in 
the last incomplete turn : $$d[i] += i + N \times \textrm{rows}$$.

- Add `remaining` to the person after the first `p % N` persons.

- Return candies distribution `d`. 

**Implementation**

<iframe src="https://leetcode.com/playground/5BAFw8ED/shared" frameBorder="0" width="100%" height="395" name="5BAFw8ED"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ to create N elements 
of the output array.
* Space complexity : $$\mathcal{O}(N)$$ to keep the output.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python3] Easy code w/ explanation and analysis.
- Author: rock
- Creation Date: Sun Jun 30 2019 12:05:33 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 19 2020 01:47:52 GMT+0800 (Singapore Standard Time)

<p>
1. Use `give % num_people` to determine the current index of the people, where `give` is the `give-th` giving of candy;
2. Increase each giving amount by 1 till run out of candies.

**Java:**
```
    public int[] distributeCandies(int candies, int num_people) {
        int[] people = new int[num_people];
        for (int give = 0; candies > 0; candies -= give) {
            people[give % num_people] +=  Math.min(candies, ++give);
        }
        return people;
    }
```
----
**Python3:**
```
    def distributeCandies(self, candies: int, num_people: int) -> List[int]:
        people = num_people * [0]
        give = 0
        while candies > 0:
            people[give % num_people] += min(candies, give + 1)
            give += 1
            candies -= give
        return people
```
**Analysis:**
Assume there are `give` times distribution such that `1 + 2 + ... + give >= candies`. Therefore,
`(1 + give) * give / 2 >= candies`, and when `give` is big enough, `(give + 1) * give /2 ~ candies`. We have: 
```
1/2 * give ^ 2 < 1/2 * (give ^ 2 + give)  < 1/ 2 * (give + 1) ^ 2
```
then 
```
1/2 * give ^ 2 < candies < 1/ 2 * (give + 1) ^ 2
```
so
```
give < sqrt(2 * candies) ~ O(sqrt(candies))
```
Time: O(sqrt(candies)), space: O(num_people) - including return array.
</p>


### [Java/C++/Python] O(sqrt(candies)) with explanation
- Author: lee215
- Creation Date: Sun Jun 30 2019 12:18:56 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 01 2019 11:41:49 GMT+0800 (Singapore Standard Time)

<p>
## **Math**
Math solution is `O(N)`, but personally not really like it.
```python
    def distributeCandies(self, candies, n):
        x = int(math.sqrt(candies * 2 + 0.25) - 0.5)
        res = [0] * n
        for i in xrange(n):
            m = x / n + (x % n > i)
            res[i] = m * (i + 1) + m * (m - 1) / 2 * n
        res[x % n] += candies - x * (x + 1) / 2
        return res
```

## **Intuition**
Brute force of simulation seems to be easy.
But how is the time complexity?
<br>

## **Explanation**
The `i-th` distribution,
we will distribute `i + 1` candies to `(i % n)`th people.
We just simulate the process of distribution until we ran out of candies.

## **Complexity**
Time `O(sqrt(candies))`
Space `O(N)` for result

The number of given candies is `i + 1`, which is an increasing sequence.
The total number distributed candies is `c * (c + 1) / 2` until it\'s bigger than `candies`.
So the time it takes is `O(sqrt(candies))`
<br>

**Java:**
```java
    public int[] distributeCandies(int candies, int n) {
        int[] res = new int[n];
        for (int i = 0; candies > 0; ++i) {
            res[i % n] += Math.min(candies, i + 1);
            candies -= i + 1;
        }
        return res;
    }
```

**C++:**
```cpp
    vector<int> distributeCandies(int candies, int n) {
        vector<int> res(n);
        for (int i = 0; candies > 0; ++i) {
            res[i % n] += min(candies, i + 1);
            candies -= i + 1;
        }
        return res;
    }
```
**Python**
```python
    def distributeCandies(self, candies, n):
        res = [0] * n
        i = 0
        while candies > 0:
            res[i % n] += min(candies, i + 1)
            candies -= i + 1
            i += 1
        return res
```
</p>


### C++ brute-force
- Author: votrubac
- Creation Date: Sun Jun 30 2019 12:03:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 30 2019 12:32:05 GMT+0800 (Singapore Standard Time)

<p>
I was going down the mathematical path and it felt a bit tricky. Since the problem is \'Easy\', I guessed the brute-force should do for the contest.
```
vector<int> distributeCandies(int c, int num) {
  vector<int> res(num);
  for (auto i = 0; c > 0; c -= ++i) 
    res[i % num] += min(i + 1, c);
  return res;
}
```
</p>


