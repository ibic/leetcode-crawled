---
title: "Number of Substrings With Only 1s"
weight: 1386
#id: "number-of-substrings-with-only-1s"
---
## Description
<div class="description">
<p>Given a binary string&nbsp;<code>s</code>&nbsp;(a string consisting only of &#39;0&#39; and &#39;1&#39;s).</p>

<p>Return the number of substrings with all characters 1&#39;s.</p>

<p>Since the answer&nbsp;may be too large,&nbsp;return it modulo&nbsp;10^9 + 7.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;0110111&quot;
<strong>Output:</strong> 9
<strong>Explanation: </strong>There are 9 substring in total with only 1&#39;s characters.
&quot;1&quot; -&gt; 5 times.
&quot;11&quot; -&gt; 3 times.
&quot;111&quot; -&gt; 1 time.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;101&quot;
<strong>Output:</strong> 2
<strong>Explanation: </strong>Substring &quot;1&quot; is shown 2 times in s.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;111111&quot;
<strong>Output:</strong> 21
<strong>Explanation: </strong>Each substring contains only 1&#39;s characters.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;000&quot;
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>s[i] == &#39;0&#39;</code> or <code>s[i] == &#39;1&#39;</code></li>
	<li><code>1 &lt;= s.length &lt;= 10^5</code></li>
</ul>
</div>

## Tags
- Math (math)
- String (string)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Count
- Author: lee215
- Creation Date: Sun Jul 12 2020 12:02:13 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 13 2020 23:40:22 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
`count` the current number of consecutive "1".
For each new element,
there will be more `count` substring,
with all characters 1\'s.
<br>

## **Complexity**
Time `O(N)`
Space `O(1)`
<br>

**Java:**
```java
    public int numSub(String s) {
        int res = 0, count = 0, mod = (int)1e9 + 7;
        for (int i = 0; i < s.length(); ++i) {
            count = s.charAt(i) == \'1\' ? count + 1 : 0;
            res = (res + count) % mod;
        }
        return res;
    }
```

**C++:**
```cpp
    int numSub(string s) {
        int res = 0, count = 0, mod = 1e9 + 7;
        for (char c: s) {
            count = c == \'1\' ? count + 1 : 0;
            res = (res + count) % mod;
        }
        return res;
    }
```

**Python:**
O(n) space though
```py
    def numSub(self, s):
        return sum(len(a) * (len(a) + 1) / 2 for a in s.split(\'0\')) % (10**9 + 7)
```

</p>


### [Python] Easy readable code! (TOTALLY DIFFERENT APPROACH)
- Author: akshit0699
- Creation Date: Sun Jul 12 2020 12:01:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 12 2020 13:43:04 GMT+0800 (Singapore Standard Time)

<p>
The example would be self explanatoy:
"0110111"
"0120123
Now if we sum up all these digits:
1+2+1+2+3 = 9 is the result!

```
class Solution(object):
    def numSub(self, s):
        res, currsum = 0,0
        for digit in s:
            if digit == \'0\':
                currsum = 0
            else:
                currsum += 1 
                res+=currsum 
        return res % (10**9+7)
```
</p>


### C++/Java sliding window
- Author: votrubac
- Creation Date: Sun Jul 12 2020 12:05:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 12 2020 12:14:30 GMT+0800 (Singapore Standard Time)

<p>
Well, it\'s easy - \'0\' cuts the previous string. So, when we see \'0\', we calculate how many strings can be formed between `i - 1` and `j`.

The formula is just sum of arithmetic progression (n * (n + 1) /2, or (i - j) * (i - j + 1) /2).

**C++**
```cpp
int numSub(string s) {
    long res = 0;
    for (long i = 0, j = 0; i <= s.size(); ++i)
        if (i == s.size() || s[i] == \'0\') {
            res = (res + (i - j) * (i - j + 1) / 2) % 1000000007;
            j = i + 1;
        }
    return res;
}
```

**Java**
```java
public int numSub(String s) {
    long res = 0;
    for (long i = 0, j = 0; i <= s.length(); ++i)
        if (i == s.length() || s.charAt((int)i) == \'0\') {
            res = (res + (i - j) * (i - j + 1) / 2) % 1000000007;
            j = i + 1;
        }
    return (int)res;
}
```
</p>


