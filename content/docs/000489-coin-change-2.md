---
title: "Coin Change 2"
weight: 489
#id: "coin-change-2"
---
## Description
<div class="description">
<p>You are given coins of different denominations and a total amount of money. Write a function to compute the number of combinations that make up that amount. You may assume that you have infinite number of each kind of coin.</p>

<ul>
</ul>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> amount = 5, coins = [1, 2, 5]
<b>Output:</b> 4
<b>Explanation:</b> there are four ways to make up the amount:
5=5
5=2+2+1
5=2+1+1+1
5=1+1+1+1+1
</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> amount = 3, coins = [2]
<b>Output:</b> 0
<b>Explanation:</b> the amount of 3 cannot be made up just with coins of 2.
</pre>

<p><b>Example 3:</b></p>

<pre>
<b>Input:</b> amount = 10, coins = [10] 
<b>Output:</b> 1
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<p>You can assume that</p>

<ul>
	<li>0 &lt;= amount &lt;= 5000</li>
	<li>1 &lt;= coin &lt;= 5000</li>
	<li>the number of coins is less than 500</li>
	<li>the answer is guaranteed to fit into signed 32-bit integer</li>
</ul>

</div>

## Tags


## Companies
- Amazon - 7 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- IXL - 2 (taggedByAdmin: false)
- Alation - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Dynamic Programming

**Template**

This is a classical dynamic programming problem. 

Here is a template one could use:

- Define the base cases for which the answer is obvious. 

- Develop the strategy to compute more complex case from more simple one. 

- Link the answer to base cases with this strategy.

**Example**

Let's pic up an example: amount = 11, available coins - 2 cent,
5 cent and 10 cent. Note, that coins are unlimited.

![fig](../Figures/518/example.png)

**Base Cases: No Coins or Amount = 0**

If the total amount of money is zero, there is only one combination: to take zero coins.

Another base case is no coins: zero combinations for `amount > 0` and one combination 
for `amount == 0`.

![fig](../Figures/518/base_cases.png)

**2 Cent Coins**

Let's do one step further and consider the situation with 
one kind of available coins: 2 cent.

![fig](../Figures/518/2_cents.png)

It's quite evident that there could be 1 or 0 combinations here, 
1 combination for even amount and 0 combinations for the odd one.

> The same answer could be received in a recursive way, by 
computing the number of combinations for all amounts of money,
from 0 to 11. 

First, that's quite obvious that all amounts less than 2 
are _not_ impacted by the presence of 2 cent coins. Hence 
for `amount = 0` and for `amount = 1` one could reuse the results
from the figure 2. 

Starting from `amount = 2`, one could use 2 cent coins in the combinations.
Since the amounts are considered gradually from 2 to 11, at each given
moment one could be sure to add not more than one coin to the previously 
known combinations. 

So let's pick up 2 cent coin, and use it 
to make up `amount = 2`. The number of combinations with 
this 2 cent coin is a number combinations for `amount = 0`,
i.e. 1.  

![fig](../Figures/518/2_cents_amount_2_end.png)

Now let's pick up 2 cent coin, and use it 
to make up `amount = 3`. The number of combinations with 
this 2 cent coin is a number combinations for `amount = 1`,
i.e. 0.

![fig](../Figures/518/2_cents_amount_3_end.png)

That leads to DP formula for number of combinations to make up 
the `amount = x`: `dp[x] = dp[x] + dp[x - coin]`, where `coin = 2 cents` 
is a value of coins we're currently adding.

![fig](../Figures/518/2cents_amount3_total2.png)

**2 Cent Coins + 5 Cent Coins + 10 Cent Coins**

Now let's add 5 cent coins. The formula is the same, 
but do not forget to add `dp[x]`, number of combinations with 
2 cent coins.

![fig](../Figures/518/2_5_cents.png) 

The story is the same for 10 cent coins.

![fig](../Figures/518/10_cent2.png) 

Now the strategy is here: 

- Add coins one-by-one, starting from base case "no coins".

- For each added coin, 
compute recursively the number of combinations 
for each amount of money from 0 to `amount`. 

**Algorithm**

- Initiate number of combinations array with the base case "no coins":
`dp[0] = 1`, and all the rest = 0.

- Loop over all coins:

    - For each coin, loop over all amounts from 0 to `amount`:
    
        - For each amount x, compute the number of combinations:
        `dp[x] += dp[x - coin]`.
        
- Return `dp[amount]`. 

**Implementation**

<iframe src="https://leetcode.com/playground/wqqfP4nY/shared" frameBorder="0" width="100%" height="276" name="wqqfP4nY"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N \times \textrm{amount})$$, where 
N is a length of coins array. 

* Space complexity: $$\mathcal{O}(\textrm{amount})$$ to keep dp array.

## Accepted Submission (java)
```java
import java.util.Arrays;

public class Solution {
    public int change(int amount, int[] coins) {
        int[] dp = new int[amount + 1];
        dp[0] = 1;
        for (int coin : coins) {
            for (int i = coin; i < amount + 1; i++) {
                dp[i] += dp[i - coin];
            }
        }
        return dp[amount];
    }
}

```

## Top Discussions
### Knapsack problem - Java solution with thinking process O(nm) Time and O(m) Space
- Author: tankztc
- Creation Date: Mon Feb 13 2017 05:39:45 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 03:11:05 GMT+0800 (Singapore Standard Time)

<p>
This is a classic knapsack problem. Honestly, I\'m not good at knapsack problem, it\'s really tough for me.

```dp[i][j]``` : the number of combinations to make up amount ```j``` by using the first ```i``` types of coins
```State transition```:
1. not using the ```i```th coin, only using the first ```i-1``` coins to make up amount ```j```, then we have  ```dp[i-1][j]``` ways.
2. using the ```i```th coin, since we can use unlimited same coin, we need to know how many ways to make up amount ```j - coins[i-1]``` by using first ```i``` coins(including ```i```th), which is ```dp[i][j-coins[i-1]]```

```Initialization```: ```dp[i][0] = 1```

Once you figure out all these, it\'s easy to write out the code:

```
    public int change(int amount, int[] coins) {
        int[][] dp = new int[coins.length+1][amount+1];
        dp[0][0] = 1;
        
        for (int i = 1; i <= coins.length; i++) {
            dp[i][0] = 1;
            for (int j = 1; j <= amount; j++) {
                dp[i][j] = dp[i-1][j] + (j >= coins[i-1] ? dp[i][j-coins[i-1]] : 0);
            }
        }
        return dp[coins.length][amount];
    }
```

Now we can see that ```dp[i][j]``` only rely on ```dp[i-1][j]``` and ```dp[i][j-coins[i]]```, then we can optimize the space by only using one-dimension array.
```
    public int change(int amount, int[] coins) {
        int[] dp = new int[amount + 1];
        dp[0] = 1;
        for (int coin : coins) {
            for (int i = coin; i <= amount; i++) {
                dp[i] += dp[i-coin];
            }
        }
        return dp[amount];
    }
```
</p>


### Unbounded Knapsack
- Author: GraceMeng
- Creation Date: Thu Jun 21 2018 19:58:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 02:13:47 GMT+0800 (Singapore Standard Time)

<p>
This is a Unbounded Knapsack problem: for each coin, we can put as many times as we want.

A Brute-Force solution is to try all combinations of the given `coins` to select the ones that give a total sum of `amount`.

With memoization, we can overcome overlapping subproblems involved.
```
    private Integer[][] dp;
    public int change(int amount, int[] coins) {
        if (amount == 0)
            return 1;
        if (coins.length == 0)
            return 0;
        dp = new Integer[coins.length][amount + 1];
        return changeFrom(amount, coins, 0);
    }
    
    private int changeFrom(int amount, int[] coins, int currentIndex) {
        if (amount == 0)
          return 1;

        if (amount < 0 || currentIndex == coins.length)
          return 0;

        if (dp[currentIndex][amount] != null)
            return dp[currentIndex][amount];
        
        // Recursive call after selecting the coin at the currentIndex
        int sum1 = changeFrom(amount - coins[currentIndex], coins, currentIndex);

        // Recursive call after excluding the coin at the currentIndex
        int sum2 = changeFrom(amount, coins, currentIndex + 1);

        dp[currentIndex][amount] = sum1 + sum2;
        return dp[currentIndex][amount];
    }
```
Or we can implement Bottom-up Dynamic Programming as below.
```
    public int change(int amount, int[] coins) {
        int[][] dp = new int[coins.length + 1][amount + 1];
        dp[0][0] = 1;        
        for (int j = 1; j <= coins.length; j++) {
            dp[j][0] = 1;
            for (int i = 1; i <= amount; i++) {
                dp[j][i] = dp[j - 1][i];
                if (i - coins[j - 1] >= 0) {
                    dp[j][i] += dp[j][i - coins[j - 1]];
                }
            }
        }
        return dp[coins.length][amount];
    }
```
Which can be reduced to one-dimensional dp array:
```
    public int change(int amount, int[] coins) {
        int[] dp = new int[amount + 1];
        dp[0] = 1;        
        for (int j = 0; j < coins.length; j++) {
            for (int i = 1; i <= amount; i++) {
                if (i - coins[j] >= 0) {
                    dp[i] += dp[i - coins[j]];
                }
            }
        }
        return dp[amount];
    }
```
Time complexity: O(length of `coins` * `amount`)
Space complexity: O(length of `coins` * `amount`)
</p>


### Beginner Mistake: Why an inner loop for coins doensn't work? [Java Soln]
- Author: ssonkar
- Creation Date: Tue Oct 02 2018 15:32:59 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 09:40:14 GMT+0800 (Singapore Standard Time)

<p>
Example of code giving wrong answer for outer loop iterating over amount and inner loop iterating over coins:
```
class Solution {
    public int change(int amount, int[] coins) {
        if(amount<=0)
            return 0;
        int ways[] = new int[amount+1];
        
        for(int i =0; i<amount+1; i++){
            for(int coin: coins){
                if(coin+i < amount+1 ){
                    ways[i+coin] = ways[i]+ways[i+coin];
                }
                if(i==0){
                    ways[i]=1;
                }
            }
        }
        for(int i=0; i< ways.length; i++){
            
            System.out.print(i+":"+ways[i]+" ");
        }
        return ways[amount];
    }
}
```

Correct Solution:
```
class Solution {
    public int change(int amount, int[] coins) {
        int [] combi = new int[amount+1];
        combi[0] = 1;
        for(int i = 0; i < coins.length; i++){
            for(int j=1; j< amount+1; j++){
                if(j-coins[i]>=0)
                    combi[j] = combi[j]+combi[j-coins[i]];
            }
        }
        for(int a: combi)
            System.out.print(a+" ");
        return combi[amount];
    }
}
```
The two codes on superificial comparison look equal, but the Code 1 gives a higher number of solutions that the correct answer.
The reason for this is when we create an amount array from 0...Amount, if we iterate over all the coins a solution
can be added twice. For example to create 7:
1. When amount is 2 and the coin value is 5, it would be counted as 1 way.
2. When amount is 5 and the coin value is 2, the number of ways become 2.

The set is either case remains 1 coin of 2 and 1 coin of 5. But the first method adds it twice.
So we create use an outer loop of coins so that a combination once used cannot be used again.

I hope this helps someone stuck at the same issue.
</p>


