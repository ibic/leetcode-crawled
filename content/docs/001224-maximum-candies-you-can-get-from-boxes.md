---
title: "Maximum Candies You Can Get from Boxes"
weight: 1224
#id: "maximum-candies-you-can-get-from-boxes"
---
## Description
<div class="description">
<p>Given <code>n</code> boxes, each box is given in the format <code>[status, candies, keys, containedBoxes]</code> where:</p>

<ul>
	<li><code>status[i]</code>: an integer which is <strong>1</strong> if&nbsp;<code>box[i]</code> is open and <strong>0</strong> if&nbsp;<code>box[i]</code> is closed.</li>
	<li><code>candies[i]</code>:&nbsp;an integer representing the number of candies in <code>box[i]</code>.</li>
	<li><code>keys[i]</code>: an array contains the indices of the boxes you can open with the key in <code>box[i]</code>.</li>
	<li><code>containedBoxes[i]</code>: an array contains the indices of the boxes found in <code>box[i]</code>.</li>
</ul>

<p>You will start with some boxes given in <code>initialBoxes</code> array. You can take all the candies in any open&nbsp;box and you can use the keys in it to open new boxes and you also can use the boxes you find in it.</p>

<p>Return <em>the maximum number of candies</em> you can get following the rules above.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> status = [1,0,1,0], candies = [7,5,4,100], keys = [[],[],[1],[]], containedBoxes = [[1,2],[3],[],[]], initialBoxes = [0]
<strong>Output:</strong> 16
<strong>Explanation:</strong> You will be initially given box 0. You will find 7 candies in it and boxes 1 and 2. Box 1 is closed and you don&#39;t have a key for it so you will open box 2. You will find 4 candies and a key to box 1 in box 2.
In box 1, you will find 5 candies and box 3 but you will not find a key to box 3 so box 3 will remain closed.
Total number of candies collected = 7 + 4 + 5 = 16 candy.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> status = [1,0,0,0,0,0], candies = [1,1,1,1,1,1], keys = [[1,2,3,4,5],[],[],[],[],[]], containedBoxes = [[1,2,3,4,5],[],[],[],[],[]], initialBoxes = [0]
<strong>Output:</strong> 6
<strong>Explanation:</strong> You have initially box 0. Opening it you can find boxes 1,2,3,4 and 5 and their keys. The total number of candies will be 6.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> status = [1,1,1], candies = [100,1,100], keys = [[],[0,2],[]], containedBoxes = [[],[],[]], initialBoxes = [1]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> status = [1], candies = [100], keys = [[]], containedBoxes = [[]], initialBoxes = []
<strong>Output:</strong> 0
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> status = [1,1,1], candies = [2,3,2], keys = [[],[],[]], containedBoxes = [[],[],[]], initialBoxes = [2,1,0]
<strong>Output:</strong> 7
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= status.length &lt;= 1000</code></li>
	<li><code>status.length == candies.length == keys.length == containedBoxes.length == n</code></li>
	<li><code>status[i]</code> is <code>0</code> or <code>1</code>.</li>
	<li><code>1 &lt;= candies[i] &lt;= 1000</code></li>
	<li><code><font face="monospace">0 &lt;= keys[i].length &lt;= status.length</font></code></li>
	<li><code>0 &lt;= keys[i][j] &lt; status.length</code></li>
	<li>All values in <code>keys[i]</code> are unique.</li>
	<li><code><font face="monospace">0 &lt;= </font>containedBoxes<font face="monospace">[i].length &lt;= status.length</font></code></li>
	<li><code>0 &lt;= containedBoxes[i][j] &lt; status.length</code></li>
	<li>All values in <code>containedBoxes[i]</code> are unique.</li>
	<li>Each box is contained in one box at most.</li>
	<li><code>0 &lt;= initialBoxes.length&nbsp;&lt;= status.length</code></li>
	<li><code><font face="monospace">0 &lt;= initialBoxes[i] &lt; status.length</font></code></li>
</ul>
</div>

## Tags
- Breadth-first Search (breadth-first-search)

## Companies
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] Simulation using BFS
- Author: PhoenixDD
- Creation Date: Sun Dec 22 2019 12:00:52 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 23 2019 06:04:06 GMT+0800 (Singapore Standard Time)

<p>
**Observation**

Try and visualize how you\'d proceed if this was a real life scenario
1) We would first open all the initial boxes.
2) Open all the boxes who\'s keys are present in these boxes, add all the candies in these opened boxes and queue up all the boxes that were inside these boxes.
3) We now move to the new boxes that we just got and repeat the same process for these with only difference that if these are closed and we don\'t have the keys to it we store it for opening later just in case we find the keys for these in a box later.
4) We stop when all the boxes are either opened or all the boxes left are closed and we don\'t have the keys for any of them.

We can thus use a simple queue to simulate the process in a BFS fashion.

**Solution**
Inspired by comment from [@StefanPochmann](https://leetcode.com/stefanpochmann) below.
This is updated to avoid the worst case when keys of `i` are contained in `i+1`. However due to the keys which can be duplicate inside each box we still might look at all the keys again and again after opening every box, thus not reducing the time complexity.

```c++
class Solution {
public:
    int maxCandies(vector<int>& status, vector<int>& candies, vector<vector<int>>& keys, vector<vector<int>>& containedBoxes, vector<int>& initialBoxes) 
    {
        queue<int> q;
        int result=0;
        vector<bool> reachableClosedBoxes(status.size(),false);  //Only used to store boxes that are reached but closed.
        for(int &i:initialBoxes)            //Push initial boxes that we can open in the queue.
            if(status[i])
                q.push(i);
            else
                reachableClosedBoxes[i]=true;
        while(!q.empty())                   //Continue until no more boxes are left that can be opened.
        {
            result+=candies[q.front()];			//Add candies we got.
            for(int &i:keys[q.front()])				//Open the box whose keys are found.
            {
                if(!status[i]&&reachableClosedBoxes[i])//If the box was previously closed and we already reached it,use it as an open box
                    q.push(i);
                status[i]=1;
            }
            for(int &i:containedBoxes[q.front()])		//Push all the boxes within this box for then next cycle in the queue.
                if(status[i])
                    q.push(i);
                else
                    reachableClosedBoxes[i]=true;      //The box is closed, wait until we get the keys for this box.
            q.pop();
        }
        return result;
    }
};
```
Space: `O(n)`.
Time: `O(n^2)`. Since we can have duplicate keys in different boxes.
</p>


### Easy way using Queue [JAVA]
- Author: JatinYadav96
- Creation Date: Sun Dec 22 2019 12:14:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 22 2019 12:14:19 GMT+0800 (Singapore Standard Time)

<p>
```
    public static int maxCandies(int[] status, int[] candies, int[][] keys, int[][] containedBoxes, int[] initialBoxes) {
        int n = status.length; // count of boxes
        boolean[] usedBoxes = new boolean[n]; // this are used once
        boolean[] boxFound = new boolean[n];// new box we found
        Queue<Integer> q = new LinkedList<>();
        for (int v : initialBoxes) {
            q.add(v);
            boxFound[v] = true; // initial boxes
        }
        int candy = 0;
        while (!q.isEmpty()) {
            int cur = q.poll();
            if (status[cur] == 1 && !usedBoxes[cur]) { // not used and box open
                candy += candies[cur];
                usedBoxes[cur] = true;
                for (int k : keys[cur]) { // all keys in that box
                    status[k] = 1;
                    if (boxFound[k]) q.add(k);// box was found and we have the key
                }
                for (int k : containedBoxes[cur]) {// all boxes in cur box
                    boxFound[k] = true;
                    q.add(k);
                }
            }
        }
        return candy;
    }
```
</p>


### [Java/C++/Python] Accumulate LeeCode Coins
- Author: lee215
- Creation Date: Fri Dec 27 2019 21:22:39 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jan 10 2020 14:18:28 GMT+0800 (Singapore Standard Time)

<p>
Skied last week, sorry for late.

## **Intuition**
While `initialBoxes` tells the initial boxes that you have,
`status` actually represents the initial keys that you have.

To open a box, you need both box and key with the same number.
The usual way is to record the boxed you have and also the keys you have.

The intuition is that, box is more important than the key.
Since you have unique box in the world, but it may have multiple keys for it.

This recalled me the rule of the LeetCode contests:
There are thousands participates, each time you earn 5 leetcode coins.
But there is only one champion and they earns 5000 leetcode coinss.
To open a LeetCode package box, you need about 6000+ leetcode coins.

Yes, we can solve this prolem with this **imagination**.
<br>

## **Explanation**
If you have a box, you got `5000` points.
If you have a key, you got `5` point.
We say thay you need `5000+` points to redeem a box.
We accumulate the points you have in `status`

Note: `1 <= status.length <= 1000 <= 5000`
<br>

## **Complexity**
Time `O(B + K)`
Space `O(B)`
where `B` is the number of boxes, and `K` is the number of keys.
<br>

**Java**
```java
    public int maxCandies(int[] status, int[] candies, int[][] keys, int[][] containedBoxes, int[] initialBoxes) {
        int res = 0;
        Queue<Integer> q = new LinkedList<>();
        for (int i : initialBoxes)
            if ((status[i] += 5000) > 5000)
                q.add(i);
        while (q.size() > 0) {
            int b = q.remove();
            res += candies[b];
            for (int i : keys[b])
                if ((status[i] += 5) == 5005)
                    q.add(i);
            for (int i : containedBoxes[b])
                if ((status[i] += 5000) > 5000)
                    q.add(i);
        }
        return res;
    }
```
**C++**
```cpp
    int maxCandies(vector<int>& status, vector<int>& candies, vector<vector<int>>& keys, vector<vector<int>>& containedBoxes, vector<int>& initialBoxes) {
        int res = 0, i;
        queue<int> q;
        for (int i : initialBoxes)
            if ((status[i] += 5000) > 5000)
                q.push(i);
        while (!q.empty()) {
            i = q.front(), q.pop();
            res += candies[i];
            for (int j : keys[i])
                if ((status[j] += 5) == 5005)
                    q.push(j);
            for (int j : containedBoxes[i])
                if ((status[j] += 5000) > 5000)
                    q.push(j);
        }
        return res;
    }
```

**Python:**
As we care the boxes,
and don\'t really care the candies in this process. (candies = reputation?)
We can count the total number of candies after open all boxes.
```python
    def maxCandies(self, status, candies, keys, containedBoxes, initialBoxes):
        boxes = set(initialBoxes)
        bfs = [i for i in boxes if status[i]]
        for i in bfs:
            for j in containedBoxes[i]:
                boxes.add(j)
                if status[j]:
                    bfs.append(j)
            for j in keys[i]:
                if status[j] == 0 and j in boxes:
                    bfs.append(j)
                status[j] = 1
        return sum(candies[i] for i in bfs)
```

</p>


