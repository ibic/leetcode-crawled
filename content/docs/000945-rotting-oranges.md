---
title: "Rotting Oranges"
weight: 945
#id: "rotting-oranges"
---
## Description
<div class="description">
<p>In a given grid, each cell can have one of three&nbsp;values:</p>

<ul>
	<li>the value <code>0</code> representing an empty cell;</li>
	<li>the value <code>1</code> representing a fresh orange;</li>
	<li>the value <code>2</code> representing a rotten orange.</li>
</ul>

<p>Every minute, any fresh orange that is adjacent (4-directionally) to a rotten orange becomes rotten.</p>

<p>Return the minimum number of minutes that must elapse until no cell has a fresh orange.&nbsp; If this is impossible, return <code>-1</code> instead.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/02/16/oranges.png" style="width: 712px; height: 150px;" /></strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[2,1,1],[1,1,0],[0,1,1]]</span>
<strong>Output: </strong><span id="example-output-1">4</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[[2,1,1],[0,1,1],[1,0,1]]</span>
<strong>Output: </strong><span id="example-output-2">-1</span>
<strong>Explanation: </strong> The orange in the bottom left corner (row 2, column 0) is never rotten, because rotting only happens 4-directionally.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[[0,2]]</span>
<strong>Output: </strong><span id="example-output-3">0</span>
<strong>Explanation: </strong> Since there are already no fresh oranges at minute 0, the answer is just 0.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= grid.length &lt;= 10</code></li>
	<li><code>1 &lt;= grid[0].length &lt;= 10</code></li>
	<li><code>grid[i][j]</code> is only <code>0</code>, <code>1</code>, or <code>2</code>.</li>
</ol>
</div>
</div>
</div>

</div>

## Tags
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 43 (taggedByAdmin: true)
- Microsoft - 5 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Flipkart - 3 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Breadth-First Search (BFS)

**Intuition**

This is yet another 2D traversal problem. As we know, the common algorithmic strategies to deal with these problems would be **_Breadth-First Search_** (BFS) and **_Depth-First Search_** (DFS).

As suggested by its name, the BFS strategy prioritizes the **breadth** over depth, _i.e._ it goes wider before it goes deeper. On the other hand, the DFS strategy prioritizes the **depth** over breadth.

The choice of strategy depends on the nature of the problem. Though sometimes, they are both applicable for the same problem. In addition to 2D grids, these two algorithms are often applied to problems associated with _tree_ or _graph_ data structures as well.

In this problem, one can see that BFS would be a better fit. 
>Because the process of rotting could be explained perfectly with the BFS procedure, _i.e._ the rotten oranges will contaminate their neighbors first, before the contamination propagates to other fresh oranges that are farther away.

If one is not familiar with the algorithm of BFS, one can refer to our [Explore card of Queue & Stack](https://leetcode.com/explore/learn/card/queue-stack/231/practical-application-queue/) which covers this subject.

However, it would be more intuitive to visualize the rotting process with a _graph_ data structure, where each node represents a cell and the edge between two nodes indicates that the given two cells are adjacent to each other.

![Grid to Graph](../Figures/994/994_grid_graph.png)

In the above graph (pun intended), as we can see, starting from the top rotten orange, the contamination would propagate _**layer by layer**_ (or level by level), until it reaches the farthest fresh oranges. The number of minutes that are elapsed would be equivalent to the number of levels in the graph that we traverse during the propagation.

!?!../Documents/994_LIS.json:1000,619!?!

**Algorithm**

One of the most distinguished code patterns in BFS algorithms is that often we use a _**queue**_ data structure to keep track of the candidates that we need to visit during the process.

The main algorithm is built around a loop iterating through the queue. At each iteration, we _pop_ out an element from the head of the queue. Then we do some particular process with the popped element. More importantly, we then _append_ neighbors of the popped element into the queue, to keep the BFS process running.

Here are some sample implementations.

<iframe src="https://leetcode.com/playground/xhpYDDNS/shared" frameBorder="0" width="100%" height="500" name="xhpYDDNS"></iframe>

In the above implementations, we applied some tricks to further optimize both the time and space complexities.

- Usually in BFS algorithms, we keep a `visited` table which records the visited candidates. The `visited` table helps us to avoid repetitive visits.
<br/>

- But as one notices, rather than using the `visited` table, we _reuse_ the input grid to keep track of our visits, _i.e._ we were altering the _status_ of the input grid **in-place**.
<br/>

- This _in-place_ technique reduces the memory consumption of our algorithm. Also, it has a constant time complexity to check the current status (_i.e._ array access, `grid[row][col]`), rather than referring to the `visited` table which might be of constant time complexity as well (_e.g._ hash table) but in reality could be slower than array access. 
<br/>

- We use a _**delimiter**_ (_i.e._ `(row=-1, col=-1)`) in the queue to separate cells on different levels. In this way, we only need one queue for the iteration. As an alternative, one can *create* a queue for each level and alternate between the queues, though technically the initialization and the assignment of each queue could consume some extra time.

**Complexity**

- Time Complexity: $$\mathcal{O}(N)$$, where $$N$$ is the size of the grid.

    First, we scan the grid to find the initial values for the queue, which would take $$\mathcal{O}(N)$$ time.
    <br/>

    Then we run the BFS process on the queue, which in the worst case would enumerate all the cells in the grid once and only once. Therefore, it takes $$\mathcal{O}(N)$$ time.
    <br/>

    Thus combining the above two steps, the overall time complexity would be $$\mathcal{O}(N) + \mathcal{O}(N) = \mathcal{O}(N)$$


- Space Complexity: $$\mathcal{O}(N)$$, where $$N$$ is the size of the grid.

    In the worst case, the grid is filled with rotten oranges. As a result, the queue would be initialized with all the cells in the grid.
    <br/>
    
    By the way, normally for BFS, the main space complexity lies in the process rather than the initialization. For instance, for a BFS traversal in a tree, at any given moment, the queue would hold no more than 2 levels of tree nodes. Therefore, the space complexity of BFS traversal in a tree would depend on the _**width**_ of the input tree.

<br/>
<br/>

---
#### Approach 2: In-place BFS

**Intuition**

Although there is no doubt that the best strategy for this problem is BFS, some users in the Discussion forum have proposed different implementations of BFS with constant space complexity $$\mathcal{O}(1)$$. To name just a few, one can see the posts from [@manky](https://leetcode.com/problems/rotting-oranges/discuss/569248/Alternate-approach-BFS-O(N-*-Height)-but-constant-space-easy-to-understand-and-modular-code) and [@votrubac](https://leetcode.com/problems/rotting-oranges/discuss/238579/C%2B%2BJava-with-picture-BFS).

As one might recall from the previous BFS implementation, its space complexity is mainly due to the `queue` that we were using to keep the order for the visits of cells. In order to achieve $$\mathcal{O}(1)$$ space complexity, we then need to eliminate the queue in the BFS.

>The secret in doing BFS traversal without a queue lies in the technique called [_in-place algorithm_](https://en.wikipedia.org/wiki/In-place_algorithm), which transforms input to solve the problem without using auxiliary data structure.

Actually, we have already had a taste of _in-place algorithm_ in the previous implementation of BFS, where we directly modified the input grid to mark the oranges that turn rotten, rather than using an additional `visited` table.

How about we apply the in-place algorithm again, but this time for the _role_ of the `queue` variable in our previous BFS implementation?

>The idea is that at each **_round_** of the BFS, we mark the cells to be visited in the input grid with a specific `timestamp`.

By _round_, we mean a snapshot in time where a group of oranges turns rotten.

**Algorithm**

![Grid Snapshot I](../Figures/994/994_timestamp_I.png)

![Grid Snapshot II](../Figures/994/994_timestamp_II.png)

In the above graph, we show how we manipulate the values in the input grid _in-place_ in order to run the BFS traversal.

- 1). Starting from the beginning (with `timestamp=2`), the cells that are marked with the value `2` contain rotten oranges. From this moment on, we adopt a **_rule_** stating as "the cells that have the value of the current timestamp (_i.e._ `2`) should be visited at this round of BFS.".
<br/>

- 2). For each of the cell that is marked with the current timestamp, we then go on to mark its neighbor cells that hold a fresh orange with the _**next**_ timestamp (_i.e._ `timestamp += 1`). This _**in-place**_ modification serves the same purpose as the `queue` variable in the previous BFS implementation, which is to select the candidates to visit for the next round.
<br/>

- 3). At this moment, we should have `timestamp=3`, and meanwhile we also have the cells to be visited at this round marked out. We then repeat the above step **(2)** until there is no more new candidates generated in the step (2) (_i.e._ the end of BFS traversal).
<br/>

To summarize, the above algorithm is still a BFS traversal in a 2D grid. But rather than using a queue data structure to keep track of the visiting order, we applied an _in-place algorithm_ to serve the same purpose as a queue in a more classic BFS implementation.

<iframe src="https://leetcode.com/playground/2sjoSda5/shared" frameBorder="0" width="100%" height="500" name="2sjoSda5"></iframe>


**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(N^2)$$ where $$N$$ is the size of the input grid.
<br/>

    In the in-place BFS traversal, for each round of BFS, we would have to iterate through the entire grid.
    <br/>

    The contamination propagates in 4 different directions. If the orange is well adjacent to each other, the chain of propagation would continue until all the oranges turn rotten. 
    <br/>

    In the worst case, the rotten and the fresh oranges might be arranged in a way that we would have to run the BFS loop over and over again, which could amount to $$\frac{N}{2}$$ times which is the **_longest_** propagation chain that we might have, _i.e._ the zigzag walk in a 2D grid as shown in the following graph.
    ![Grid Zigzag](../Figures/994/994_zigzag.png) 
    <br/>

    As a result, the overall time complexity of the in-place BFS algorithm is $$\mathcal{O}(N \cdot \frac{N}{2}) = \mathcal{O}(N^2)$$.

- Space Complexity: $$\mathcal{O}(1)$$, the memory usage is constant regardless the size of the input. This is the very point of applying in-place algorithm. Here we trade the time complexity with the space complexity, which is a common scenario in many algorithms.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Clean BFS Solution with comments
- Author: irodov
- Creation Date: Sun Feb 17 2019 13:32:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 17 2019 13:32:38 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int orangesRotting(int[][] grid) {
        if(grid == null || grid.length == 0) return 0;
        int rows = grid.length;
        int cols = grid[0].length;
        Queue<int[]> queue = new LinkedList<>();
        int count_fresh = 0;
        //Put the position of all rotten oranges in queue
        //count the number of fresh oranges
        for(int i = 0 ; i < rows ; i++) {
            for(int j = 0 ; j < cols ; j++) {
                if(grid[i][j] == 2) {
                    queue.offer(new int[]{i , j});
                }
                else if(grid[i][j] == 1) {
                    count_fresh++;
                }
            }
        }
        //if count of fresh oranges is zero --> return 0 
        if(count_fresh == 0) return 0;
        int count = 0;
        int[][] dirs = {{1,0},{-1,0},{0,1},{0,-1}};
        //bfs starting from initially rotten oranges
        while(!queue.isEmpty()) {
            ++count;
            int size = queue.size();
            for(int i = 0 ; i < size ; i++) {
                int[] point = queue.poll();
                for(int dir[] : dirs) {
                    int x = point[0] + dir[0];
                    int y = point[1] + dir[1];
                    //if x or y is out of bound
                    //or the orange at (x , y) is already rotten
                    //or the cell at (x , y) is empty
                        //we do nothing
                    if(x < 0 || y < 0 || x >= rows || y >= cols || grid[x][y] == 0 || grid[x][y] == 2) continue;
                    //mark the orange at (x , y) as rotten
                    grid[x][y] = 2;
                    //put the new rotten orange at (x , y) in queue
                    queue.offer(new int[]{x , y});
                    //decrease the count of fresh oranges by 1
                    count_fresh--;
                }
            }
        }
        return count_fresh == 0 ? count-1 : -1;
    }
}
```
</p>


### Python Clean & Well Explained (faster than > 90%)
- Author: c0deman
- Creation Date: Sun Apr 05 2020 02:47:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 22 2020 23:23:13 GMT+0800 (Singapore Standard Time)

<p>
```
from collections import deque

# Time complexity: O(rows * cols) -> each cell is visited at least once
# Space complexity: O(rows * cols) -> in the worst case if all the oranges are rotten they will be added to the queue

class Solution:
    def orangesRotting(self, grid: List[List[int]]) -> int:
        
        # number of rows
        rows = len(grid)
        if rows == 0:  # check if grid is empty
            return -1
        
        # number of columns
        cols = len(grid[0])
        
        # keep track of fresh oranges
        fresh_cnt = 0
        
        # queue with rotten oranges (for BFS)
        rotten = deque()
        
        # visit each cell in the grid
        for r in range(rows):
            for c in range(cols):
                if grid[r][c] == 2:
                    # add the rotten orange coordinates to the queue
                    rotten.append((r, c))
                elif grid[r][c] == 1:
                    # update fresh oranges count
                    fresh_cnt += 1
        
        # keep track of minutes passed.
        minutes_passed = 0
        
        # If there are rotten oranges in the queue and there are still fresh oranges in the grid keep looping
        while rotten and fresh_cnt > 0:

            # update the number of minutes passed
            # it is safe to update the minutes by 1, since we visit oranges level by level in BFS traversal.
            minutes_passed += 1
            
            # process rotten oranges on the current level
            for _ in range(len(rotten)):
                x, y = rotten.popleft()
                
                # visit all the adjacent cells
                for dx, dy in [(1,0), (-1,0), (0,1), (0,-1)]:
                    # calculate the coordinates of the adjacent cell
                    xx, yy = x + dx, y + dy
                    # ignore the cell if it is out of the grid boundary
                    if xx < 0 or xx == rows or yy < 0 or yy == cols:
                        continue
                    # ignore the cell if it is empty \'0\' or visited before \'2\'
                    if grid[xx][yy] == 0 or grid[xx][yy] == 2:
                        continue
                        
                    # update the fresh oranges count
                    fresh_cnt -= 1
                    
                    # mark the current fresh orange as rotten
                    grid[xx][yy] = 2
                    
                    # add the current rotten to the queue
                    rotten.append((xx, yy))

        
        # return the number of minutes taken to make all the fresh oranges to be rotten
        # return -1 if there are fresh oranges left in the grid (there were no adjacent rotten oranges to make them rotten)
        return minutes_passed if fresh_cnt == 0 else -1
```
</p>


### Is this an easy question?
- Author: hzg1981
- Creation Date: Wed May 15 2019 19:39:55 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 15 2019 19:39:55 GMT+0800 (Singapore Standard Time)

<p>
I think it should be medium.
</p>


