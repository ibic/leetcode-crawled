---
title: "Complex Number Multiplication"
weight: 505
#id: "complex-number-multiplication"
---
## Description
<div class="description">
<p>
Given two strings representing two <a href = "https://en.wikipedia.org/wiki/Complex_number">complex numbers</a>.</p>

<p>
You need to return a string representing their multiplication. Note i<sup>2</sup> = -1 according to the definition.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> "1+1i", "1+1i"
<b>Output:</b> "0+2i"
<b>Explanation:</b> (1 + i) * (1 + i) = 1 + i<sup>2</sup> + 2 * i = 2i, and you need convert it to the form of 0+2i.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> "1+-1i", "1+-1i"
<b>Output:</b> "0+-2i"
<b>Explanation:</b> (1 - i) * (1 - i) = 1 + i<sup>2</sup> - 2 * i = -2i, and you need convert it to the form of 0+-2i.
</pre>
</p>

<p><b>Note:</b>
<ol>
<li>The input strings will not have extra blank.</li>
<li>The input strings will be given in the form of <b>a+bi</b>, where the integer <b>a</b> and <b>b</b> will both belong to the range of [-100, 100]. And <b>the output should be also in this form</b>.</li>
</ol>
</p>
</div>

## Tags
- Math (math)
- String (string)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Simple Solution[Accepted]

**Algorithm**

Multiplication of two complex numbers can be done as:

$$
(a+ib) \times (x+iy)=ax+i^2by+i(bx+ay)=ax-by+i(bx+ay)
$$

We simply split up the real and the imaginary parts of the given complex strings based on the '+' and the 'i' symbols. We store the real parts of the two strings $$a$$ and $$b$$ as $$x[0]$$ and $$y[0]$$ respectively and the imaginary parts as $$x[1]$$ and $$y[1]$$ respectively. Then, we multiply the real and the imaginary parts as required after converting the extracted parts into integers. Then, we again form the return string in the required format and return the result.

<iframe src="https://leetcode.com/playground/jgLSUzDc/shared" frameBorder="0" name="jgLSUzDc" width="100%" height="309"></iframe>

**Complexity Analysis**

* Time complexity : $$O(1)$$. Here splitting takes constant time as length of the string is very small $$(<20)$$.

* Space complexity : $$O(1)$$. Constant extra space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### c++ using stringstream
- Author: beetlecamera
- Creation Date: Mon Mar 27 2017 03:44:25 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 19:18:40 GMT+0800 (Singapore Standard Time)

<p>
stringstream is very useful to extract num from a string

```
class Solution {
public:
    string complexNumberMultiply(string a, string b) {
        int ra, ia, rb, ib;
        char buff;
        stringstream aa(a), bb(b), ans;
        aa >> ra >> buff >> ia >> buff;
        bb >> rb >> buff >> ib >> buff;
        ans << ra*rb - ia*ib << "+" << ra*ib + rb*ia << "i";
        return ans.str();
    }
};
</p>


### Java 3-liner
- Author: compton_scatter
- Creation Date: Sun Mar 26 2017 11:01:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 26 2017 11:01:49 GMT+0800 (Singapore Standard Time)

<p>
This solution relies on the fact that (a+bi)(c+di) = (ac - bd) + (ad+bc)i.

```
public String complexNumberMultiply(String a, String b) {
    int[] coefs1 = Stream.of(a.split("\\+|i")).mapToInt(Integer::parseInt).toArray(), 
          coefs2 = Stream.of(b.split("\\+|i")).mapToInt(Integer::parseInt).toArray();
    return (coefs1[0]*coefs2[0] - coefs1[1]*coefs2[1]) + "+" + (coefs1[0]*coefs2[1] + coefs1[1]*coefs2[0]) + "i";
}
```
</p>


### Java - (a1+b1)*(a2+b2) = (a1a2 + b1b2 + (a1b2+b1a2))
- Author: earlme
- Creation Date: Sun Mar 26 2017 11:19:49 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 05:39:32 GMT+0800 (Singapore Standard Time)

<p>
    public String complexNumberMultiply(String a, String b) {
        String result = "";
        String[] A = a.split("\\+");
        String[] B = b.split("\\+");
        int a1 = Integer.parseInt(A[0]);
        int b1 = Integer.parseInt(A[1].replace("i",""));

        int a2 = Integer.parseInt(B[0]);
        int b2 = Integer.parseInt(B[1].replace("i",""));

        int a1a2 = a1 * a2;
        int b1b2 = b1 * b2;
        int a1b2a2b1 = (a1 * b2) + (b1 * a2);

        String afinal = (a1a2 + (-1 * b1b2)) + "";
        String bfinal = a1b2a2b1 + "i";
        result = afinal+"+"+bfinal;
        return result;
    }
</p>


