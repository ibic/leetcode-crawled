---
title: "Convert Sorted Array to Binary Search Tree"
weight: 108
#id: "convert-sorted-array-to-binary-search-tree"
---
## Description
<div class="description">
<p>Given an array where elements are sorted in ascending order, convert it to a height balanced BST.</p>

<p>For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees of <em>every</em> node never differ by more than 1.</p>

<p><strong>Example:</strong></p>

<pre>
Given the sorted array: [-10,-3,0,5,9],

One possible answer is: [0,-3,9,-10,null,5], which represents the following height balanced BST:

      0
     / \
   -3   9
   /   /
 -10  5
</pre>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 5 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Yahoo - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### How to Traverse the Tree. DFS: Preorder, Inorder, Postorder; BFS.

There are two general strategies to traverse a tree:
     
- *Depth First Search* (`DFS`)

    In this strategy, we adopt the `depth` as the priority, so that one
    would start from a root and reach all the way down to certain leaf,
    and then back to root to reach another branch.

    The DFS strategy can further be distinguished as
    `preorder`, `inorder`, and `postorder` depending on the relative order
    among the root node, left node and right node.
    
- *Breadth First Search* (`BFS`)

    We scan through the tree level by level, following the order of height,
    from top to bottom. The nodes on higher level would be visited before
    the ones with lower levels.
    
On the following figure the nodes are enumerated in the order you visit them,
please follow `1-2-3-4-5` to compare different strategies.

![postorder](../Figures/108/bfs_dfs.png)
<br /> 
<br />


---
#### Construct BST from Inorder Traversal: Why the Solution is _Not_ Unique 

It's known that [inorder traversal of BST is an array sorted in
the ascending order](https://leetcode.com/articles/delete-node-in-a-bst/).

Having sorted array as an input, we could rewrite the problem as
_Construct Binary Search Tree from Inorder Traversal_.

> Does this problem have a unique solution, i.e. could inorder traversal 
be used as a unique identifier to encore/decode BST? The answer is _no_. 

Here is the funny thing about BST. 
Inorder traversal is _not_ a unique identifier of BST.
At the same time both preorder and postorder traversals 
_are_ unique identifiers of BST. [From these traversals 
one could restore the inorder one](https://leetcode.com/articles/construct-bst-from-preorder-traversal/): 
`inorder = sorted(postorder) = sorted(preorder)`, 
and [inorder + postorder or inorder + preorder 
are both unique identifiers of whatever binary tree](https://leetcode.com/articles/construct-binary-tree-from-inorder-and-postorder-t/).

So, the problem "sorted array -> BST" has multiple solutions.

![postorder](../Figures/108/bst2.png)

Here we have an additional condition: _the tree should be height-balanced_, i.e.
the depths of the two subtrees of every node never differ by more than 1. 

> Does it make the solution to be unique? Still no.

![postorder](../Figures/108/height.png)

Basically, the height-balanced restriction means that at each step one has to 
pick up the number in the middle as a root. 
That works fine with arrays containing odd number of elements but 
there is no predefined choice for arrays with even number of elements.

![postorder](../Figures/108/pick.png)

One could choose left middle element, or right middle one, and both choices will
lead to _different_ height-balanced BSTs. Let's see that in practice: 
in Approach 1 we will always pick up left middle element, 
in Approach 2 - right middle one. 
That will generate _different_ BSTs but both solutions will be accepted.
<br /> 
<br />


---
#### Approach 1: Preorder Traversal: Always Choose Left Middle Node as a Root

**Algorithm**

![postorder](../Figures/108/left.png)

- Implement helper function `helper(left, right)`, 
which constructs BST from nums elements between indexes `left` and `right`:

    - If left > right, then there is no elements available for that subtree.
    Return None.
    
    - Pick left middle element: `p = (left + right) // 2`.
    
    - Initiate the root: `root = TreeNode(nums[p])`. 
    
    - Compute recursively left and right subtrees:
    `root.left = helper(left, p - 1)`, `root.right = helper(p + 1, right)`.
    
- Return `helper(0, len(nums) - 1)`.  

**Implementation**

<iframe src="https://leetcode.com/playground/PJk5Svbb/shared" frameBorder="0" width="100%" height="412" name="PJk5Svbb"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since we visit each node exactly once.

* Space complexity: $$\mathcal{O}(N)$$. 
$$\mathcal{O}(N)$$ to keep the output, and $$\mathcal{O}(\log N)$$ for the recursion
stack. 
<br /> 
<br />


---
#### Approach 2: Preorder Traversal: Always Choose Right Middle Node as a Root

**Algorithm**

![postorder](../Figures/108/right.png)

- Implement helper function `helper(left, right)`, 
which constructs BST from nums elements between indexes `left` and `right`:

    - If left > right, then there is no elements available for that subtree.
    Return None.
    
    - Pick right middle element: 
        
        - `p = (left + right) // 2`.
    
        - If `left + right` is odd, add 1 to p-index.
    
    - Initiate the root: `root = TreeNode(nums[p])`. 
    
    - Compute recursively left and right subtrees:
    `root.left = helper(left, p - 1)`, `root.right = helper(p + 1, right)`.
    
- Return `helper(0, len(nums) - 1)`. 

**Implementation**

<iframe src="https://leetcode.com/playground/29KweQq8/shared" frameBorder="0" width="100%" height="429" name="29KweQq8"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since we visit each node exactly once.

* Space complexity: $$\mathcal{O}(N)$$. 
$$\mathcal{O}(N)$$ to keep the output, and $$\mathcal{O}(\log N)$$ for the recursion
stack. 
<br /> 
<br />


---
#### Approach 3: Preorder Traversal: Choose Random Middle Node as a Root

This one is for fun. Instead of predefined choice we will pick 
randomly left or right middle node at each step. Each run will result in 
different solution and they all will be accepted. 

![postorder](../Figures/108/random.png)

**Algorithm**

- Implement helper function `helper(left, right)`, 
which constructs BST from nums elements between indexes `left` and `right`:

    - If left > right, then there is no elements available for that subtree.
    Return None.
    
    - Pick random middle element: 
        
        - `p = (left + right) // 2`.
    
        - If `left + right` is odd, add randomly 0 or 1 to p-index.
    
    - Initiate the root: `root = TreeNode(nums[p])`. 
    
    - Compute recursively left and right subtrees:
    `root.left = helper(left, p - 1)`, `root.right = helper(p + 1, right)`.
    
- Return `helper(0, len(nums) - 1)`. 

**Implementation**

<iframe src="https://leetcode.com/playground/gGZP2B7k/shared" frameBorder="0" width="100%" height="446" name="gGZP2B7k"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since we visit each node exactly once.

* Space complexity: $$\mathcal{O}(N)$$. 
$$\mathcal{O}(N)$$ to keep the output, and $$\mathcal{O}(\log N)$$ for the recursion
stack.

## Accepted Submission (java)
```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public TreeNode sortedArrayToBSTRecursive(int[] nums, int start, int stop) {
        int dist = stop - start;
        TreeNode root;
        switch (dist) {
        case 1:
            return new TreeNode(nums[start]);
        case 2:
            root = new TreeNode(nums[start]);
            root.right = new TreeNode(nums[start + 1]);
            return root;
        case 3:
            root = new TreeNode(nums[start + 1]);
            root.left = new TreeNode(nums[start]);
            root.right = new TreeNode(nums[start + 2]);
            return root;
        default:
            int mid = (start + stop) / 2;
            root = new TreeNode(nums[mid]);
            root.left = sortedArrayToBSTRecursive(nums, start, mid);
            root.right = sortedArrayToBSTRecursive(nums, mid + 1, stop);
            return root;
        }
    }
    public TreeNode sortedArrayToBST(int[] nums) {
        if (nums.length <= 0) {
            return null;
        } else {
            return sortedArrayToBSTRecursive(nums, 0, nums.length);
        }
    }
}
```

## Top Discussions
### My Accepted Java Solution
- Author: jiaming2
- Creation Date: Sat Sep 06 2014 12:39:31 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 12:49:39 GMT+0800 (Singapore Standard Time)

<p>
Hi everyone, this is my accepted recursive Java solution. I get overflow problems at first because I didn't use mid - 1 and mid + 1 as the bound. Hope this helps :)

    public TreeNode sortedArrayToBST(int[] num) {
        if (num.length == 0) {
            return null;
        }
        TreeNode head = helper(num, 0, num.length - 1);
        return head;
    }
    
    public TreeNode helper(int[] num, int low, int high) {
        if (low > high) { // Done
            return null;
        }
        int mid = (low + high) / 2;
        TreeNode node = new TreeNode(num[mid]);
        node.left = helper(num, low, mid - 1);
        node.right = helper(num, mid + 1, high);
        return node;
    }
</p>


### An easy Python solution
- Author: Google
- Creation Date: Sat Mar 21 2015 13:41:20 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 01:25:23 GMT+0800 (Singapore Standard Time)

<p>
The idea is to find the root first, then recursively build each left and right subtree

    # Definition for a  binary tree node
    # class TreeNode:
    #     def __init__(self, x):
    #         self.val = x
    #         self.left = None
    #         self.right = None
    
    class Solution:
        # @param num, a list of integers
        # @return a tree node
        # 12:37
        def sortedArrayToBST(self, num):
            if not num:
                return None
    
            mid = len(num) // 2
    
            root = TreeNode(num[mid])
            root.left = self.sortedArrayToBST(num[:mid])
            root.right = self.sortedArrayToBST(num[mid+1:])
    
            return root
</p>


### Python optimal solution
- Author: yangshun
- Creation Date: Mon Oct 16 2017 20:21:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 20 2019 23:56:34 GMT+0800 (Singapore Standard Time)

<p>
For a sorted array, the left half will be in the left subtree, middle value as the root, right half in the right subtree. This holds true for every node:

`[1, 2, 3, 4, 5, 6, 7]` -> left: `[1, 2, 3]`, root: `4`, right: `[5, 6, 7]`
`[1, 2, 3]` -> left: `[1]`, root: `2`, right: `[3]`
`[5, 6, 7]` -> left: `[5]`, root: `6`, right: `[7]`

Many of the approaches here suggest slicing an array recursively and passing them. However, slicing the array is expensive. It is better to pass the left and right bounds into recursive calls instead.

*- Yangshun*

```
class Solution(object):
    def sortedArrayToBST(self, nums):
        # Time: O(n)
        # Space: O(n) in the case of skewed binary tree.
        def convert(left, right):
            if left > right:
                return None
            mid = (left + right) // 2
            node = TreeNode(nums[mid])
            node.left = convert(left, mid - 1)
            node.right = convert(mid + 1, right)
            return node
        return convert(0, len(nums) - 1)
```
</p>


