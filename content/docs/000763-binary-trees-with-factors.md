---
title: "Binary Trees With Factors"
weight: 763
#id: "binary-trees-with-factors"
---
## Description
<div class="description">
<p>Given an array of unique integers, each integer is strictly greater than 1.</p>

<p>We make a binary tree using these integers&nbsp;and each number may be used for any number of times.</p>

<p>Each non-leaf node&#39;s&nbsp;value should be equal to the product of the values of it&#39;s children.</p>

<p>How many binary trees can we make?&nbsp; Return the answer <strong>modulo 10 ** 9 + 7</strong>.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> <code>A = [2, 4]</code>
<strong>Output:</strong> 3
<strong>Explanation:</strong> We can make these trees: <code>[2], [4], [4, 2, 2]</code></pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> <code>A = [2, 4, 5, 10]</code>
<strong>Output:</strong> <code>7</code>
<strong>Explanation:</strong> We can make these trees: <code>[2], [4], [5], [10], [4, 2, 2], [10, 2, 5], [10, 5, 2]</code>.</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;=&nbsp;1000</code>.</li>
	<li><code>2 &lt;=&nbsp;A[i]&nbsp;&lt;=&nbsp;10 ^ 9</code>.</li>
</ol>

</div>

## Tags


## Companies


## Official Solution
[TOC]

---
#### Approach #1: Dynamic Programming [Accepted]

**Intuition**

The largest value `v` used must be the root node in the tree.  Say `dp(v)` is the number of ways to make a tree with root node `v`.

If the root node of the tree (with value `v`) has children with values `x` and `y` (and `x * y == v`), then there are `dp(x) * dp(y)` ways to make this tree.

In total, there are $$\sum_{\substack{x * y = v}} \text{dp}(x) * \text{dp}(y)$$ ways to make a tree with root node `v`.

**Algorithm**

Actually, let `dp[i]` be the number of ways to have a root node with value `A[i]`.

Since in the above example we always have `x < v` and `y < v`, we can calculate the values of `dp[i]` in increasing order using dynamic programming.

For some root value `A[i]`, let's try to find candidates for the children with values `A[j]` and `A[i] / A[j]` (so that evidently `A[j] * (A[i] / A[j]) = A[i]`).  To do this quickly, we will need `index` which looks up this value: if `A[k] = A[i] / A[j]`, then index[A[i] / A[j]] = k`.

After, we'll add all possible `dp[j] * dp[k]` (with `j < i, k < i`) to our answer `dp[i]`.  In our Java implementation, we carefully used `long` so avoid overflow issues.

```java
class Solution {
    public int numFactoredBinaryTrees(int[] A) {
        int MOD = 1_000_000_007;
        int N = A.length;
        Arrays.sort(A);
        long[] dp = new long[N];
        Arrays.fill(dp, 1);

        Map<Integer, Integer> index = new HashMap();
        for (int i = 0; i < N; ++i)
            index.put(A[i], i);

        for (int i = 0; i < N; ++i)
            for (int j = 0; j < i; ++j) {
                if (A[i] % A[j] == 0) { // A[j] is left child
                    int right = A[i] / A[j];
                    if (index.containsKey(right)) {
                        dp[i] = (dp[i] + dp[j] * dp[index.get(right)]) % MOD;
                    }
                }
            }

        long ans = 0;
        for (long x: dp) ans += x;
        return (int) (ans % MOD);
    }
}
```

```python
class Solution(object):
    def numFactoredBinaryTrees(self, A):
        MOD = 10 ** 9 + 7
        N = len(A)
        A.sort()
        dp = [1] * N
        index = {x: i for i, x in enumerate(A)}
        for i, x in enumerate(A):
            for j in xrange(i):
                if x % A[j] == 0: #A[j] will be left child
                    right = x / A[j]
                    if right in index:
                        dp[i] += dp[j] * dp[index[right]]
                        dp[i] %= MOD

        return sum(dp) % MOD
```

</playground>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the length of `A`.  This comes from the two for-loops iterating `i` and `j`.

* Space Complexity: $$O(N)$$, the space used by `dp` and `index`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] DP solution
- Author: lee215
- Creation Date: Sun Apr 22 2018 11:02:54 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 14 2019 13:19:16 GMT+0800 (Singapore Standard Time)

<p>
Sort the list `A` at first. Scan `A` from small element to bigger.

DP equation:
`dp[i] = sum(dp[j] * dp[i / j])`
`res  = sum(dp[i])`
with `i, j, i / j` in the list `L`


**C++**
```cpp
    int numFactoredBinaryTrees(vector<int>& A) {
        long res = 0, mod = pow(10, 9) + 7;
        sort(A.begin(), A.end());
        unordered_map<int, long> dp;
        for (int i = 0; i < A.size(); ++i) {
            dp[A[i]] = 1;
            for (int j = 0; j < i; ++j)
                if (A[i] % A[j] == 0)
                    dp[A[i]] = (dp[A[i]] + dp[A[j]] * dp[A[i] / A[j]]) % mod;
            res = (res + dp[A[i]]) % mod;
        }
        return res;
    }
```
**Java**
```java
    public int numFactoredBinaryTrees(int[] A) {
        long res = 0L, mod = (long)1e9 + 7;
        Arrays.sort(A);
        HashMap<Integer, Long> dp = new HashMap<>();
        for (int i = 0; i < A.length; ++i) {
            dp.put(A[i], 1L);
            for (int j = 0; j < i; ++j)
                if (A[i] % A[j] == 0)
                    dp.put(A[i], (dp.get(A[i]) + dp.get(A[j]) * dp.getOrDefault(A[i] / A[j], 0L)) % mod);
            res = (res + dp.get(A[i])) % mod;
        }
        return (int) res;
    }
```

**Python**
```py
    def numFactoredBinaryTrees(self, A):
        dp = {}
        for a in sorted(A):
            dp[a] = sum(dp[b] * dp.get(a / b, 0) for b in dp if a % b == 0) + 1
        return sum(dp.values()) % (10**9 + 7)
```

</p>


### Concise Java solution using HashMap with detailed explanation. Easily understand!!!
- Author: Self_Learner
- Creation Date: Wed Apr 25 2018 15:46:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 30 2018 07:09:23 GMT+0800 (Singapore Standard Time)

<p>
```
/**sort the array
 * and use HashMap to record each Integer, and the number of trees with that Integer as root
 * (1) each integer A[i] will always have one tree with only itself
 * (2) if A[i] has divisor (a) in the map, and if A[i]/a also in the map
 *     then a can be the root of its left subtree, and A[i]/a can be the root of its right subtree;
 *     the number of such tree is map.get(a) * map.get(A[i]/a)
 * (3) sum over the map
 */
class Solution {    
    public int numFactoredBinaryTrees(int[] A) {
        Arrays.sort(A);
        Map<Integer, Long> map = new HashMap();
        long count = 1;
        map.put(A[0], count);
        for (int i = 1; i < A.length; i++) {
            count = 1;
            for (Integer n : map.keySet()) {
                if (A[i] % n == 0 && map.containsKey(A[i] / n)) {
                    count += map.get(n) * map.get(A[i] / n);
                }
            }
            map.put(A[i], count);
        }
        long sum = 0;
        for (Integer n : map.keySet()) {
            sum = (sum + map.get(n)) % ((int) Math.pow(10, 9) + 7) ;
        }
        return (int) sum;
    }        
}
</p>


### Short simple Python
- Author: StefanPochmann
- Creation Date: Mon Apr 23 2018 17:36:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 00:15:09 GMT+0800 (Singapore Standard Time)

<p>
My `t[a]` tells the number of trees with root value `a`. The `1` is for making a leaf (there\'s one way to make a leaf with that value), the `sum` is for non-leaves (go over every possible left child value `b`).

    def numFactoredBinaryTrees(self, A):
        t = {}
        for a in sorted(A):
            t[a] = 1 + sum(t[b] * t.get(a/b, 0) for b in A if b < a)
        return sum(t.values()) % (10**9 + 7)

</p>


