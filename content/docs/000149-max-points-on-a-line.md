---
title: "Max Points on a Line"
weight: 149
#id: "max-points-on-a-line"
---
## Description
<div class="description">
<p>Given <em>n</em> points on a 2D plane, find the maximum number of points that lie on the same straight line.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> [[1,1],[2,2],[3,3]]
<strong>Output:</strong> 3
<strong>Explanation:</strong>
^
|
| &nbsp; &nbsp; &nbsp; &nbsp;o
| &nbsp; &nbsp; o
| &nbsp;o &nbsp;
+-------------&gt;
0 &nbsp;1 &nbsp;2 &nbsp;3  4
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> [[1,1],[3,2],[5,3],[4,1],[2,3],[1,4]]
<strong>Output:</strong> 4
<strong>Explanation:</strong>
^
|
|  o
| &nbsp;&nbsp;&nbsp;&nbsp;o&nbsp;&nbsp;      o
| &nbsp;&nbsp;&nbsp;&nbsp;   o
| &nbsp;o &nbsp;      o
+-------------------&gt;
0 &nbsp;1 &nbsp;2 &nbsp;3 &nbsp;4 &nbsp;5 &nbsp;6
</pre>

<p><strong>NOTE:</strong>&nbsp;input types have been changed on April 15, 2019. Please reset to default code definition to get new method signature.</p>

</div>

## Tags
- Hash Table (hash-table)
- Math (math)

## Companies
- Google - 3 (taggedByAdmin: false)
- LinkedIn - 8 (taggedByAdmin: true)
- Microsoft - 4 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: true)
- TripAdvisor - 4 (taggedByAdmin: false)
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Enumeration

**Intuition**

Let's simplify the problem and search the maximum number of points
on a line passing through the point `i`. 

One could immediately notice that it's interesting to consider only
the next points `i + 1 .. N - 1` because the maximum number of points
containing, for example, the point `i - 2` was already found
during the search of the maximum number of points
on a line passing through the point `i - 2`. 

<img src="../Figures/149/149_lines_i.png" width="500">

The idea is very simple : draw the lines 
passing through the point `i` and each of the next points.
Save these lines is a hash table with a counter `2` = two points
on this line.  
Let's imagine now that points `i < i + k < i + l` are on the same line.
Then drawing a line through `i` and `i + l` one would discover that this 
line is already tracked and hence one has to update a counter 
of points on this line `count++`.

>How to save a line? 

If the line is horizontal, i.e. `y = c`,
one could use this constant `c` as a line key in a hash table of horizontal lines.

The other lines could be represented as `y = slope * x + c`. 

The equation for the line passing through two points `1` and `2` 
[could be written through their coordinates](https://en.wikipedia.org/wiki/Line_(geometry)#On_the_Cartesian_plane)
as

$$
\frac{x - x_1}{x_2 - x_1} = \frac{y - y_1}{y_2 - y_1}
$$

that for the representation $$y = \text{slope} \times x + c$$ means 

$$
\text{slope} = \frac{y_2 - y_1}{x_2 - x_1}
$$

>Since we are drawing a line between the starting point `i` and each of the following points, if all these lines share the same `slope` value, then we can be sure that all these points are aligned on the same line.

Hence, a `slope` value is sufficient to represent a unique line starting from a specific point `i`.

One might go ahead and use a float (or double) value to represent each unique slope.
Indeed, this could work for most of the cases, but not all.


**Slope Representation**

One of the cases that a float value would not cut for the slope variable is that when two points form a vertical line, (_i.e._ $$x_1 == x_2$$).
As we can see from the formula to calculate the slope value, we would encounter a divide-by-zero error.

One might argument we could treat this as a special case, and assign a special value (say, zero) to represent the horizontal slope.

However, a bigger problem is that the float and double values are intrinsically **_inaccurate_**, due to how these values are [represented in the computer](https://en.wikipedia.org/wiki/Floating-point_arithmetic).
A simple fact to comprehend this limitation is that we could have infinite number of digits for a fraction number (_i.e._ $$\frac{1}{3}$$), we could only keep a limited number of digits as its float value in the computer.

One can run a fun experiment to calculate the result for the operation of $$1.2 - 1.0$$ in the Python shell.
(_spoiler alert_: we would get the value of `0.19999999999999996` as the result.)

Therefore, it is not wise to use the float/double value to represent a unique slope, since they are not accurate.

>To circumvent the above issue, one could use a pair of [co-prime integers](https://en.wikipedia.org/wiki/Coprime_integers) to represent unique slope.

As a reminder, two integers are co-primes, if and only if their greatest common divisor is 1.

As one can see, due to the property of co-prime numbers, they can be used to represent the slope values of different lines.
For example, for the slope values of $$\frac{1}{3}, \frac{2}{6}, \frac{3}{9}$$, they all can be represented with the co-prime numbers of $$(1, 3)$$.

**Algorithm**

We now have the idea and even some important details (co-primes) to implement the algorithm:

* Initiate the maximum number of points `max_count = 1`.

* Iterate over all points `i` from `0` to `N - 2`.

    * For each point `i` find a maximum number of points 
    `max_count_i`
    on a line passing through the point `i` :
        * Initiate the maximum number of points 
        on a line passing through the point `i` : `count = 1`.
        * Iterate over next points `j` from `i + 1` to `N - 1`.
            * If `j` is a duplicate of `i`, 
            update a number of duplicates for point `i`.
            * If not:
                * Save the line passing through the points `i` and `j`.
                * Update `count` at each step.
        * Return `max_count_i = count + duplicates`.
            
    * Update the result `max_count = max(max_count, max_count_i)`
        
!?!../Documents/149_LIS.json:1000,581!?!

<iframe src="https://leetcode.com/playground/5NsCvrbQ/shared" frameBorder="0" width="100%" height="500" name="5NsCvrbQ"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N^2)$$ since one draws 
not more than `N - 1` lines passing through the point `0`,
not more than `N - 2` lines for the point `1`, and
the only one line for the point `N - 2`. That results in
`(N - 1) + (N - 2) + .. + 1 = N(N - 1)/2` operations, _i.e._ $$\mathcal{O}(N^2)$$ time complexity.

* Space complexity : $$\mathcal{O}(N)$$ to track down not more than `N - 1` lines.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A java solution with notes
- Author: reeclapple
- Creation Date: Wed Aug 27 2014 10:08:22 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 11:17:16 GMT+0800 (Singapore Standard Time)

<p>
  

      /*
         *  A line is determined by two factors,say y=ax+b
         *  
         *  If two points(x1,y1) (x2,y2) are on the same line(Of course). 

         *  Consider the gap between two points.

         *  We have (y2-y1)=a(x2-x1),a=(y2-y1)/(x2-x1) a is a rational, b is canceled since b is a constant

         *  If a third point (x3,y3) are on the same line. So we must have y3=ax3+b

         *  Thus,(y3-y1)/(x3-x1)=(y2-y1)/(x2-x1)=a

         *  Since a is a rational, there exists y0 and x0, y0/x0=(y3-y1)/(x3-x1)=(y2-y1)/(x2-x1)=a

         *  So we can use y0&x0 to track a line;
         */
        
        public class Solution{
            public int maxPoints(Point[] points) {
            	if (points==null) return 0;
            	if (points.length<=2) return points.length;
            	
            	Map<Integer,Map<Integer,Integer>> map = new HashMap<Integer,Map<Integer,Integer>>();
            	int result=0;
            	for (int i=0;i<points.length;i++){ 
            		map.clear();
            		int overlap=0,max=0;
            		for (int j=i+1;j<points.length;j++){
            			int x=points[j].x-points[i].x;
            			int y=points[j].y-points[i].y;
            			if (x==0&&y==0){
            				overlap++;
            				continue;
            			}
            			int gcd=generateGCD(x,y);
            			if (gcd!=0){
            				x/=gcd;
            				y/=gcd;
            			}
            			
            			if (map.containsKey(x)){
            				if (map.get(x).containsKey(y)){
            					map.get(x).put(y, map.get(x).get(y)+1);
            				}else{
            					map.get(x).put(y, 1);
            				}   					
            			}else{
            				Map<Integer,Integer> m = new HashMap<Integer,Integer>();
            				m.put(y, 1);
            				map.put(x, m);
            			}
            			max=Math.max(max, map.get(x).get(y));
            		}
            		result=Math.max(result, max+overlap+1);
            	}
            	return result;
            	
            	
            }
            private int generateGCD(int a,int b){
        
            	if (b==0) return a;
            	else return generateGCD(b,a%b);
            	
            }
        }
</p>


### Sharing my simple solution with explanation
- Author: zxyperfect
- Creation Date: Mon Dec 15 2014 06:05:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 10:08:20 GMT+0800 (Singapore Standard Time)

<p>
    int maxPoints(vector<Point> &points) {
        int result = 0;
        for(int i = 0; i < points.size(); i++){
            int samePoint = 1;
            unordered_map<double, int> map;
            for(int j = i + 1; j < points.size(); j++){
                if(points[i].x == points[j].x && points[i].y == points[j].y){
                    samePoint++;
                }
                else if(points[i].x == points[j].x){
                    map[INT_MAX]++;
                }
                else{
                    double slope = double(points[i].y - points[j].y) / double(points[i].x - points[j].x);
                    map[slope]++;
                }
            }
            int localMax = 0;
            for(auto it = map.begin(); it != map.end(); it++){
                localMax = max(localMax, it->second);
            }
            localMax += samePoint;
            result = max(result, localMax);
        }
        return result;
    }

First, let's talk about mathematics.

How to determine if three points are on the same line?

The answer is to see if slopes of arbitrary two pairs are the same.

Second, let's see what the minimum time complexity can be.

Definitely, O(n^2). It's because you have to calculate all slopes between any two points. 

Then let's go back to the solution of this problem.

In order to make this discussion simpler, let's pick a random point A as an example.

Given point A, we need to calculate all slopes between A and other points. There will be three cases:

1. Some other point is the same as point A.

2. Some other point has the same x coordinate as point A, which will result to a positive infinite slope.

3. General case. We can calculate slope.

We can store all slopes in a hash table. And we find which slope shows up mostly. Then add the number of same points to it. Then we know the maximum number of points on the same line for point A.

We can do the same thing to point B, point C...

Finally, just return the maximum result among point A, point B, point C...
</p>


### C++ O(n^2) solution for your reference
- Author: Yoursong
- Creation Date: Tue Aug 12 2014 04:14:50 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 11:40:44 GMT+0800 (Singapore Standard Time)

<p>
Hint by @stellari

"For each point pi, calculate the slope of each line it forms with all other points with greater indices, i.e. pi+1, pi+2, ..., and use a map to record how many lines have the same slope (If two lines have the same slope and share a common point, then the two lines must be the same one). By doing so, you can easily find how many points are on the same line that ends at pi in O(n). Thus the amortized running time of the whole algorithm is O(n^2)."

In order to avoid using double type(the slope k) as map key, I used pair (int a, int b) as the key where a=pj.x-pi.x, b=pj.y-pi.y, and k=b/a. Using greatest common divider of a and b to divide both a, b ensures that lines with same slope have the same key. 

I also handled two special cases: (1) when two points are on a vertical line (2) when two points are the same.

    class Solution {
    public:
        int maxPoints(vector<Point> &points) {
            
            if(points.size()<2) return points.size();
            
            int result=0;
            
            for(int i=0; i<points.size(); i++) {
                
                map<pair<int, int>, int> lines;
                int localmax=0, overlap=0, vertical=0;
                
                for(int j=i+1; j<points.size(); j++) {
                    
                    if(points[j].x==points[i].x && points[j].y==points[i].y) {
                        
                        overlap++;
                        continue;
                    }
                    else if(points[j].x==points[i].x) vertical++;
                    else {
                        
                        int a=points[j].x-points[i].x, b=points[j].y-points[i].y;
                        int gcd=GCD(a, b);
                        
                        a/=gcd;
                        b/=gcd;
                        
                        lines[make_pair(a, b)]++;
                        localmax=max(lines[make_pair(a, b)], localmax);
                    }
    
                    localmax=max(vertical, localmax);
                }
                
                result=max(result, localmax+overlap+1);
            }
            
            return result;
        }
    
    private:
        int GCD(int a, int b) {
            
            if(b==0) return a;
            else return GCD(b, a%b);
        }
    };
</p>


