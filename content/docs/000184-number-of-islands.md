---
title: "Number of Islands"
weight: 184
#id: "number-of-islands"
---
## Description
<div class="description">
<p>Given a 2d grid map of <code>&#39;1&#39;</code>s (land) and <code>&#39;0&#39;</code>s (water), count the number of islands. An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. You may assume all four edges of the grid are all surrounded by water.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> grid = [
  [&quot;1&quot;,&quot;1&quot;,&quot;1&quot;,&quot;1&quot;,&quot;0&quot;],
  [&quot;1&quot;,&quot;1&quot;,&quot;0&quot;,&quot;1&quot;,&quot;0&quot;],
  [&quot;1&quot;,&quot;1&quot;,&quot;0&quot;,&quot;0&quot;,&quot;0&quot;],
  [&quot;0&quot;,&quot;0&quot;,&quot;0&quot;,&quot;0&quot;,&quot;0&quot;]
]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> grid = [
  [&quot;1&quot;,&quot;1&quot;,&quot;0&quot;,&quot;0&quot;,&quot;0&quot;],
  [&quot;1&quot;,&quot;1&quot;,&quot;0&quot;,&quot;0&quot;,&quot;0&quot;],
  [&quot;0&quot;,&quot;0&quot;,&quot;1&quot;,&quot;0&quot;,&quot;0&quot;],
  [&quot;0&quot;,&quot;0&quot;,&quot;0&quot;,&quot;1&quot;,&quot;1&quot;]
]
<strong>Output:</strong> 3
</pre>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)
- Union Find (union-find)

## Companies
- Amazon - 276 (taggedByAdmin: true)
- Bloomberg - 39 (taggedByAdmin: false)
- Facebook - 22 (taggedByAdmin: true)
- Microsoft - 21 (taggedByAdmin: true)
- Apple - 14 (taggedByAdmin: false)
- Google - 8 (taggedByAdmin: true)
- Goldman Sachs - 8 (taggedByAdmin: false)
- eBay - 5 (taggedByAdmin: false)
- Adobe - 4 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- Roblox - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Robinhood - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Palantir Technologies - 4 (taggedByAdmin: false)
- Snapchat - 4 (taggedByAdmin: false)
- Expedia - 4 (taggedByAdmin: false)
- Tableau - 4 (taggedByAdmin: false)
- Audible - 4 (taggedByAdmin: false)
- Qualtrics - 3 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- DoorDash - 2 (taggedByAdmin: false)
- Evernote - 2 (taggedByAdmin: false)
- Samsung - 2 (taggedByAdmin: false)
- JPMorgan - 2 (taggedByAdmin: false)
- Arista Networks - 2 (taggedByAdmin: false)
- Mathworks - 2 (taggedByAdmin: false)
- Electronic Arts - 2 (taggedByAdmin: false)
- Lyft - 12 (taggedByAdmin: false)
- Twitter - 11 (taggedByAdmin: false)
- Atlassian - 8 (taggedByAdmin: false)
- Cruise Automation - 7 (taggedByAdmin: false)
- Affirm - 6 (taggedByAdmin: false)
- Twitch - 6 (taggedByAdmin: false)
- Hulu - 4 (taggedByAdmin: false)
- Houzz - 4 (taggedByAdmin: false)
- Cisco - 4 (taggedByAdmin: false)
- VMware - 4 (taggedByAdmin: false)
- Nutanix - 3 (taggedByAdmin: false)
- Spotify - 3 (taggedByAdmin: false)
- Zulily - 3 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- Citrix - 2 (taggedByAdmin: false)
- AppDynamics - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Nvidia - 2 (taggedByAdmin: false)
- Sumologic - 2 (taggedByAdmin: false)
- Square - 2 (taggedByAdmin: false)
- LiveRamp - 2 (taggedByAdmin: false)
- Intel - 2 (taggedByAdmin: false)
- BlackRock - 2 (taggedByAdmin: false)
- GoDaddy - 2 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)
- Splunk - 2 (taggedByAdmin: false)
- Cohesity - 2 (taggedByAdmin: false)
- Karat - 2 (taggedByAdmin: false)
- Zenefits - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1 DFS [Accepted]

**Intuition**

Treat the 2d grid map as an undirected graph and there is an edge
between two horizontally or vertically adjacent nodes of value '1'.

**Algorithm**

Linear scan the 2d grid map, if a node contains a '1', then it is a root node
that triggers a Depth First Search. During DFS, every visited node should be
set as '0' to mark as visited node. Count the number of root nodes that trigger
DFS, this number would be the number of islands since each DFS starting at some
root identifies an island.

The algorithm can be better illustrated by the animation below:
!?!../Documents/200_number_of_islands_dfs.json:1024,768!?!

<iframe src="https://leetcode.com/playground/eVGw9zoX/shared" frameBorder="0" width="100%" height="500" name="eVGw9zoX"></iframe>


**Complexity Analysis**

* Time complexity : $$O(M \times N)$$ where $$M$$ is the number of rows and
  $$N$$ is the number of columns.

* Space complexity : worst case $$O(M \times N)$$ in case that the grid map
  is filled with lands where DFS goes by $$M \times N$$ deep.

---


#### Approach #2: BFS [Accepted]

**Algorithm**

Linear scan the 2d grid map, if a node contains a '1', then it is a root node
that triggers a Breadth First Search. Put it into a queue and set its value
as '0' to mark as visited node. Iteratively search the neighbors of enqueued
nodes until the queue becomes empty.

<iframe src="https://leetcode.com/playground/jXzqritJ/shared" frameBorder="0" width="100%" height="500" name="jXzqritJ"></iframe>

**Complexity Analysis**

* Time complexity : $$O(M \times N)$$ where $$M$$ is the number of rows and
  $$N$$ is the number of columns.

* Space complexity : $$O(min(M, N))$$ because in worst case where the
  grid is filled with lands, the size of queue can grow up to min($$M,N$$).

---


#### Approach #3: Union Find (aka Disjoint Set) [Accepted]

**Algorithm**

Traverse the 2d grid map and union adjacent lands horizontally or vertically,
at the end, return the number of connected components maintained in the UnionFind
data structure.

For details regarding to Union Find, you can refer to this [article](https://leetcode.com/articles/redundant-connection/).

The algorithm can be better illustrated by the animation below:
!?!../Documents/200_number_of_islands_unionfind.json:1024,768!?!

<iframe src="https://leetcode.com/playground/k6Xzhgt6/shared" frameBorder="0" width="100%" height="500" name="k6Xzhgt6"></iframe>


**Complexity Analysis**

* Time complexity : $$O(M \times N)$$ where $$M$$ is the number of rows and
  $$N$$ is the number of columns. Note that Union operation takes essentially constant
  time[^1] when UnionFind is implemented with both path compression and union by rank.

* Space complexity : $$O(M \times N)$$ as required by UnionFind data structure.


---

**Footnotes**

[^1]: [https://en.wikipedia.org/wiki/Disjoint-set_data_structure](https://en.wikipedia.org/wiki/Disjoint-set_data_structure)

## Accepted Submission (java)
```java
class Solution {
    public int numIslands(char[][] grid) {
        int r = 0;
        int width = grid.length;
        if (width == 0) {
            return r;
        }
        int height = grid[0].length;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                if (grid[x][y] == '1') {
                    dfs(grid, x, y);
                    r++;
                }
            }
        }
        return r;
    }

    private void dfs(char[][] grid, int x, int y) {
        grid[x][y] = '0';
        List<int[]> neighbours = getNeighbours(grid, x, y);
        for (int[] neighbour: neighbours) {
            dfs(grid, neighbour[0], neighbour[1]);
        }
    }

    private List<int[]> getNeighbours(char[][] grid, int x, int y) {
        return Arrays.stream(new int[][]{
                new int[]{x, y - 1},
                new int[]{x, y + 1},
                new int[]{x - 1, y},
                new int[]{x + 1, y},
        }).filter(a -> a[0] >= 0 && a[0] < grid.length && a[1] >= 0 && a[1] < grid[0].length && grid[a[0]][a[1]] == '1')
                .collect(Collectors.toList());
    }
}

```

## Top Discussions
### Very concise Java AC solution
- Author: wcyz666
- Creation Date: Tue May 05 2015 00:27:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 01:23:32 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
    
    private int n;
    private int m;
    
    public int numIslands(char[][] grid) {
        int count = 0;
        n = grid.length;
        if (n == 0) return 0;
        m = grid[0].length;
        for (int i = 0; i < n; i++){
            for (int j = 0; j < m; j++)
                if (grid[i][j] == '1') {
                    DFSMarking(grid, i, j);
                    ++count;
                }
        }    
        return count;
    }
    
    private void DFSMarking(char[][] grid, int i, int j) {
        if (i < 0 || j < 0 || i >= n || j >= m || grid[i][j] != '1') return;
        grid[i][j] = '0';
        DFSMarking(grid, i + 1, j);
        DFSMarking(grid, i - 1, j);
        DFSMarking(grid, i, j + 1);
        DFSMarking(grid, i, j - 1);
    }
}
</p>


### Python Simple DFS Solution
- Author: girikuncoro
- Creation Date: Mon Feb 01 2016 20:49:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 00:50:52 GMT+0800 (Singapore Standard Time)

<p>
Iterate through each of the cell and if it is an island, do dfs to mark all adjacent islands, then increase the counter by 1.

    def numIslands(self, grid):
        if not grid:
            return 0
            
        count = 0
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] == '1':
                    self.dfs(grid, i, j)
                    count += 1
        return count
    
    def dfs(self, grid, i, j):
        if i<0 or j<0 or i>=len(grid) or j>=len(grid[0]) or grid[i][j] != '1':
            return
        grid[i][j] = '#'
        self.dfs(grid, i+1, j)
        self.dfs(grid, i-1, j)
        self.dfs(grid, i, j+1)
        self.dfs(grid, i, j-1)
</p>


### 7 lines Python, ~14 lines Java
- Author: StefanPochmann
- Creation Date: Mon Jun 22 2015 05:15:36 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 04:13:15 GMT+0800 (Singapore Standard Time)

<p>
Sink and count the islands.

---

**Python Solution**

    def numIslands(self, grid):
        def sink(i, j):
            if 0 <= i < len(grid) and 0 <= j < len(grid[i]) and grid[i][j] == '1':
                grid[i][j] = '0'
                map(sink, (i+1, i-1, i, i), (j, j, j+1, j-1))
                return 1
            return 0
        return sum(sink(i, j) for i in range(len(grid)) for j in range(len(grid[i])))

---

**Java Solution 1**

    public class Solution {
        char[][] g;
        public int numIslands(char[][] grid) {
            int islands = 0;
            g = grid;
            for (int i=0; i<g.length; i++)
                for (int j=0; j<g[i].length; j++)
                    islands += sink(i, j);
            return islands;
        }
        int sink(int i, int j) {
            if (i < 0 || i == g.length || j < 0 || j == g[i].length || g[i][j] == '0')
                return 0;
            g[i][j] = '0';
            sink(i+1, j); sink(i-1, j); sink(i, j+1); sink(i, j-1);
            return 1;
        }
    }

---

**Java Solution 2**

    public class Solution {
        public int numIslands(char[][] grid) {
            int islands = 0;
            for (int i=0; i<grid.length; i++)
                for (int j=0; j<grid[i].length; j++)
                    islands += sink(grid, i, j);
            return islands;
        }
        int sink(char[][] grid, int i, int j) {
            if (i < 0 || i == grid.length || j < 0 || j == grid[i].length || grid[i][j] == '0')
                return 0;
            grid[i][j] = '0';
            for (int k=0; k<4; k++)
                sink(grid, i+d[k], j+d[k+1]);
            return 1;
        }
        int[] d = {0, 1, 0, -1, 0};
    }
</p>


