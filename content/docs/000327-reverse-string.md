---
title: "Reverse String"
weight: 327
#id: "reverse-string"
---
## Description
<div class="description">
<p>Write a function that reverses a string. The input string is given as an array of characters <code>char[]</code>.</p>

<p>Do not allocate extra space for another array, you must do this by <strong>modifying the input array&nbsp;<a href="https://en.wikipedia.org/wiki/In-place_algorithm" target="_blank">in-place</a></strong> with O(1) extra memory.</p>

<p>You may assume all the characters consist of <a href="https://en.wikipedia.org/wiki/ASCII#Printable_characters" target="_blank">printable ascii characters</a>.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[&quot;h&quot;,&quot;e&quot;,&quot;l&quot;,&quot;l&quot;,&quot;o&quot;]</span>
<strong>Output: </strong><span id="example-output-1">[&quot;o&quot;,&quot;l&quot;,&quot;l&quot;,&quot;e&quot;,&quot;h&quot;]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[&quot;H&quot;,&quot;a&quot;,&quot;n&quot;,&quot;n&quot;,&quot;a&quot;,&quot;h&quot;]</span>
<strong>Output: </strong><span id="example-output-2">[&quot;h&quot;,&quot;a&quot;,&quot;n&quot;,&quot;n&quot;,&quot;a&quot;,&quot;H&quot;]</span>
</pre>
</div>
</div>

</div>

## Tags
- Two Pointers (two-pointers)
- String (string)

## Companies
- Apple - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)
- Adobe - 4 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Bloomberg - 6 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- GoDaddy - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

_Life is short, use Python._ (c) 

<iframe src="https://leetcode.com/playground/AzW3B9hy/shared" frameBorder="0" width="100%" height="106" name="AzW3B9hy"></iframe>

Speaking seriously, let's use this problem to discuss two things:

- Does _in-place_ mean constant space complexity?

- Two pointers approach. 
<br /> 
<br />


---
#### Approach 1: Recursion, In-Place, $$\mathcal{O}(N)$$ Space

**Does _in-place_ mean constant space complexity?**

No. [By definition](https://en.wikipedia.org/wiki/In-place_algorithm), 
an in-place algorithm is an algorithm 
which transforms input using no auxiliary data structure. 

The tricky part is that space is used by many actors, not only by 
data structures. The classical example is to use recursive function without
any auxiliary data structures. 

Is it in-place? Yes. 

Is it constant space? No, because of recursion stack.

![fig](../Figures/344/stack2.png) 

**Algorithm**

Here is an example. Let's implement recursive function `helper` which 
receives two pointers, left and right, as arguments. 

- Base case: if `left >= right`, do nothing.

- Otherwise, swap `s[left]` and `s[right]` and call `helper(left + 1, right - 1)`.

To solve the problem, call helper function passing the head and tail indexes as 
arguments: `return helper(0, len(s) - 1)`.

**Implementation**

<iframe src="https://leetcode.com/playground/cLDBmvWy/shared" frameBorder="0" width="100%" height="276" name="cLDBmvWy"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ time to perform $$N/2$$ swaps. 

* Space complexity : $$\mathcal{O}(N)$$ to keep the recursion stack.
<br /> 
<br />


---
#### Approach 2: Two Pointers, Iteration, $$\mathcal{O}(1)$$ Space

**Two Pointers Approach**

In this approach, two pointers are used to process two array elements
at the same time. Usual implementation is to set one pointer in the 
beginning and one at the end and then to move them until they both meet.  

Sometimes one needs to generalize this approach in order to use three pointers, 
like for classical [Sort Colors problem](https://leetcode.com/articles/sort-colors/).

**Algorithm**

- Set pointer left at index 0, and pointer right at index `n - 1`,
where n is a number of elements in the array.

- While left < right:

    - Swap `s[left]` and `s[right]`.
    
    - Move left pointer one step right, and right pointer one step left.
    
![fig](../Figures/344/two.png)    

**Implementation**

<iframe src="https://leetcode.com/playground/mryJCRru/shared" frameBorder="0" width="100%" height="225" name="mryJCRru"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ to swap $$N/2$$ element. 

* Space complexity : $$\mathcal{O}(1)$$, it's a constant space solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [JAVA] Simple and Clean with Explanations [6 Solutions]
- Author: ratchapongt
- Creation Date: Fri Apr 22 2016 12:27:28 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 09:05:03 GMT+0800 (Singapore Standard Time)

<p>
https://www.ratchapong.com/algorithm-practice/leetcode/reverse-string [Full solutions]
```
public class Solution {
    public String reverseString(String s) {
        char[] word = s.toCharArray();
        int i = 0;
        int j = s.length() - 1;
        while (i < j) {
            char temp = word[i];
            word[i] = word[j];
            word[j] = temp;
            i++;
            j--;
        }
        return new String(word);
    }
}
```
<div class="margin-bottom-20">
    <h4 class="text-uppercase margin-bottom-10">Complexity Analysis</h4>
    <p>
        <b>Time Complexity:</b>
        `O(n)` (Average Case) and `O(n)` (Worst Case) where `n` is the total number character in the input string.
        The algorithm need to reverse the whole string.
    </p>
    <p>
        <b>Auxiliary Space:</b>
        `O(n)` space is used where `n` is the total number character in the input string. Space is needed to transform
        string to character array.
    </p>
</div>
<div class="margin-bottom-20">
    <h4 class="text-uppercase margin-bottom-10">Algorithm</h4>
    <p>
        <b>Approach:</b>
        Iterative Swapping Using Two Pointers
    </p>
    <p>
        One pointer is pointing at the start of the string while the other pointer is pointing at the end of the string.
        Both pointers will keep swapping its element and travel towards each other. The algorithm basically simulating
        rotation of a string with respect to its midpoint.
    </p>
</div>

```
public class Solution {
    public String reverseString(String s) {
        byte[] bytes = s.getBytes();
        int i = 0;
        int j = s.length() - 1;
        while (i < j) {
            byte temp = bytes[i];
            bytes[i] = bytes[j];
            bytes[j] = temp;
            i++;
            j--;
        }
        return new String(bytes);
    }
}
```
<div class="margin-bottom-20">
    <h4 class="text-uppercase margin-bottom-10">Complexity Analysis</h4>
    <p>
        <b>Time Complexity:</b>
        `O(n)` (Average Case) and `O(n)` (Worst Case) where `n` is the total number character in the input string.
        The algorithm need to reverse the whole string. Each character is `1` byte.
    </p>
    <p>
        <b>Auxiliary Space:</b>
        `O(n)` space is used where `n` is the total number character in the input string. Space is needed to transform
        string to byte array.
    </p>
</div>
<div class="margin-bottom-20">
    <h4 class="text-uppercase margin-bottom-10">Algorithm</h4>
    <p>
        <b>Approach:</b>
        Iterative Swapping Using Two Pointers
    </p>
    <p>
        One pointer is pointing at the start of the byte array while the other pointer is pointing at the end of the
        byte array.
        Both pointers will keep swapping its element and travel towards each other. The algorithm basically simulating
        rotation of a string with respect to its midpoint.
    </p>
    <p>
        Note that this assume that the input string is encoded using
        ASCII format. This will not work with Unicode value where one character may be more than 1 byte.
    </p>
</div>

```
public class Solution {
    public String reverseString(String s) {
        char[] word = s.toCharArray();
        int i = 0;
        int j = s.length() - 1;
        while (i < j) {
            word[i] = (char) (word[i] ^ word[j]);
            word[j] = (char) (word[i] ^ word[j]);
            word[i] = (char) (word[i] ^ word[j]);
            i++;
            j--;
        }
        return new String(bytes);
    }
}
```
<div class="margin-bottom-20">
    <h4 class="text-uppercase margin-bottom-10">Complexity Analysis</h4>
    <p>
        <b>Time Complexity:</b>
        `O(n)` (Average Case) and `O(n)` (Worst Case) where `n` is the total number character in the input string.
        The algorithm need to reverse the whole string.
    </p>
    <p>
        <b>Auxiliary Space:</b>
        `O(n)` space is used where `n` is the total number character in the input string. Space is needed to transform
        string to character array.
    </p>
</div>
<div class="margin-bottom-20">
    <h4 class="text-uppercase margin-bottom-10">Algorithm</h4>
    <p>
        <b>Approach:</b>
        Iterative Swapping Using Two Pointers
    </p>
    <p>
        One pointer is pointing at the start of the string while the other pointer is pointing at the end of the string.
        Both pointers will keep swapping its element and travel towards each other. The algorithm basically simulating
        rotation of a string with respect to its midpoint. The swapping is done by using <code>XOR</code> swapping
        algorithm.
    </p>
    <div align="center" class="margin-bottom-10 margin-top-10">
        <table class="table table-bordered" style="width: 60%">
            <thead>
                <tr>
                    <th class="text-center">Operation</th>
                    <th class="text-center">Result</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center">`a = a \oplus b`</td>
                    <td align="left">`a = a \oplus b`</td>
                </tr>
                <tr>
                    <td align="center">`b = a \oplus b`</td>
                    <td align="left">`b = (a \oplus b) \oplus b = a \oplus b \oplus b = a`</td>
                </tr>
                <tr>
                    <td align="center">`a = a \oplus b`</td>
                    <td align="left">`a = (a \oplus b) \oplus a = a \oplus b \oplus a = b`</td>
                </tr>
            </tbody>
        </table>
    </div>
    <p>
        Note that this assume that the input string is encoded using
        ASCII format. This will not work with Unicode value where one character may be more than 1 byte.
    </p>
</div>

```
public class Solution {
    public String reverseString(String s) {
        byte[] bytes = s.getBytes();
        int i = 0;
        int j = s.length() - 1;
        while (i < j) {
            bytes[i] = (byte) (bytes[i] ^ bytes[j]);
            bytes[j] = (byte) (bytes[i] ^ bytes[j]);
            bytes[i] = (byte) (bytes[i] ^ bytes[j]);
            i++;
            j--;
        }
        return new String(bytes);
    }
}
```
<div class="margin-bottom-20">
    <h4 class="text-uppercase margin-bottom-10">Complexity Analysis</h4>
    <p>
        <b>Time Complexity:</b>
        `O(n)` (Average Case) and `O(n)` (Worst Case) where `n` is the total number character in the input string.
        The algorithm need to reverse the whole string. Each character is `1` byte.
    </p>
    <p>
        <b>Auxiliary Space:</b>
        `O(n)` space is used where `n` is the total number character in the input string. Space is needed to transform
        string to byte array.
    </p>
</div>
<div class="margin-bottom-20">
    <h4 class="text-uppercase margin-bottom-10">Algorithm</h4>
    <p>
        <b>Approach:</b>
        Iterative Swapping Using Two Pointers
    </p>
    <p>
        One pointer is pointing at the start of the byte array while the other pointer is pointing at the end of the
        byte array.
        Both pointers will keep swapping its element and travel towards each other. The algorithm basically simulating
        rotation of a string with respect to its midpoint. The swapping is done by using <code>XOR</code> swapping
        algorithm.
    </p>
    <div align="center" class="margin-bottom-10 margin-top-10">
        <table class="table table-bordered" style="width: 60%">
            <thead>
                <tr>
                    <th class="text-center">Operation</th>
                    <th class="text-center">Result</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center">`a = a \oplus b`</td>
                    <td align="left">`a = a \oplus b`</td>
                </tr>
                <tr>
                    <td align="center">`b = a \oplus b`</td>
                    <td align="left">`b = (a \oplus b) \oplus b = a \oplus b \oplus b = a`</td>
                </tr>
                <tr>
                    <td align="center">`a = a \oplus b`</td>
                    <td align="left">`a = (a \oplus b) \oplus a = a \oplus b \oplus a = b`</td>
                </tr>
            </tbody>
        </table>
    </div>
    <p>
        Note that this assume that the input string is encoded using
        ASCII format. This will not work with Unicode value where one character may be more than 1 byte.
    </p>
</div>

```
public class Solution {
    public String reverseString(String s) {
        return new StringBuilder(s).reverse().toString();
    }
}
```
<div class="margin-bottom-20">
    <h4 class="text-uppercase margin-bottom-10">Complexity Analysis</h4>
    <p>
        <b>Time Complexity:</b>
        `O(n)` (Average Case) and `O(n)` (Worst Case) where `n` is the total number character in the input string.
        Depending on the implementation. However, it is not possible to reverse string in less than `O(n)`.
    </p>
    <p>
        <b>Auxiliary Space:</b>
        `O(n)` space is used where `n` is the total number character in the input string. Space is needed to transform
        immutable string into character buffer in <code>StringBuilder</code>.
    </p>
</div>
<div class="margin-bottom-20">
    <h4 class="text-uppercase margin-bottom-10">Algorithm</h4>
    <p>
        <b>Approach:</b>
        Using Java Library
    </p>
    <p>
        Java's library is probably slower that direct implementation due to extra overhead in check various edge cases
        such as surrogate pairs.
    </p>
</div>

```
public class Solution {
    public String reverseString(String s) {
        int length = s.length();
        if (length <= 1) return s;
        String leftStr = s.substring(0, length / 2);
        String rightStr = s.substring(length / 2, length);
        return reverseString(rightStr) + reverseString(leftStr);
    }
}
```
<div class="margin-bottom-20">
    <h4 class="text-uppercase margin-bottom-10">Complexity Analysis</h4>
    <p>
        <b>Time Complexity:</b>
        `O(n log(n))` (Average Case) and `O(n * log(n))` (Worst Case) where `n` is the total number character in the
        input string.
        The recurrence equation is `T(n) = 2 * T(n/2) + O(n)`. `O(n)` is due to the fact that concatenation function
        takes linear time.
        The recurrence equation can be solved to get `O(n * log(n))`.
    </p>
    <p>
        <b>Auxiliary Space:</b>
        `O(h)` space is used where `h` is the depth of recursion tree generated which is `log(n)`. Space is needed for
        activation stack during recursion calls.
    </p>
</div>
<div class="margin-bottom-20">
    <h4 class="text-uppercase margin-bottom-10">Algorithm</h4>
    <p>
        <b>Approach:</b>
        Divide and Conquer (Recursive)
    </p>
    <p>
        The string is split into half. Each substring will be further divided. This process continues until the string
        can no longer be divided (length `<= 1`). The conquering process will take they previously split strings and
        concatenate them in reverse order.
    </p>
</div>
</p>


### Python2.7 (3 solutions: Recursive, Classic, Pythonic)
- Author: viakondratiuk
- Creation Date: Wed Sep 14 2016 01:12:23 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 08:08:38 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def reverseString(self, s):
        l = len(s)
        if l < 2:
            return s
        return self.reverseString(s[l/2:]) + self.reverseString(s[:l/2])


class SolutionClassic(object):
    def reverseString(self, s):
        r = list(s)
        i, j  = 0, len(r) - 1
        while i < j:
            r[i], r[j] = r[j], r[i]
            i += 1
            j -= 1

        return "".join(r)

class SolutionPythonic(object):
    def reverseString(self, s):
        return s[::-1]
```
</p>


### Simple C++ solution
- Author: xz2210
- Creation Date: Fri Apr 22 2016 12:32:06 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 03 2018 01:22:16 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        string reverseString(string s) {
            int i = 0, j = s.size() - 1;
            while(i < j){
                swap(s[i++], s[j--]); 
            }
            
            return s;
        }
    };
</p>


