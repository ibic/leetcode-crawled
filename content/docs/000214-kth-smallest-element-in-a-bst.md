---
title: "Kth Smallest Element in a BST"
weight: 214
#id: "kth-smallest-element-in-a-bst"
---
## Description
<div class="description">
<p>Given a binary search tree, write a function <code>kthSmallest</code> to find the <b>k</b>th smallest element in it.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> root = [3,1,4,null,2], k = 1
   3
  / \
 1   4
  \
&nbsp;  2
<strong>Output:</strong> 1</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = [5,3,6,2,4,null,null,1], k = 3
       5
      / \
     3   6
    / \
   2   4
  /
 1
<strong>Output:</strong> 3
</pre>

<p><b>Follow up:</b><br />
What if the BST is modified (insert/delete operations) often and you need to find the kth smallest frequently? How would you optimize the kthSmallest routine?</p>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of elements of the BST is between <code>1</code> to <code>10^4</code>.</li>
	<li>You may assume <code>k</code> is always valid, <code>1 &le; k &le; BST&#39;s total elements</code>.</li>
</ul>

</div>

## Tags
- Binary Search (binary-search)
- Tree (tree)

## Companies
- Amazon - 8 (taggedByAdmin: false)
- Facebook - 5 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: true)
- Oracle - 7 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- TripleByte - 2 (taggedByAdmin: false)
- Hulu - 2 (taggedByAdmin: false)
- Microsoft - 6 (taggedByAdmin: false)
- Uber - 5 (taggedByAdmin: true)
- VMware - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### How to traverse the tree

There are two general strategies to traverse a tree:
     
- *Depth First Search* (`DFS`)

    In this strategy, we adopt the `depth` as the priority, so that one
    would start from a root and reach all the way down to certain leaf,
    and then back to root to reach another branch.

    The DFS strategy can further be distinguished as
    `preorder`, `inorder`, and `postorder` depending on the relative order
    among the root node, left node and right node.
    
- *Breadth First Search* (`BFS`)

    We scan through the tree level by level, following the order of height,
    from top to bottom. The nodes on higher level would be visited before
    the ones with lower levels.
    
On the following figure the nodes are numerated in the order you visit them,
please follow `1-2-3-4-5` to compare different strategies.

![postorder](../Figures/230/bfs_dfs.png)

> To solve the problem, one could use the property of BST : inorder traversal of BST
is an array sorted in the ascending order. 

<br /> 
<br />


---
#### Approach 1: Recursive Inorder Traversal

It's a very straightforward approach with $$\mathcal{O}(N)$$ 
time complexity.
The idea is to build an inorder traversal of BST which is 
an array sorted in the ascending order. 
Now the answer is the `k - 1`th element of this array. 

![bla](../Figures/230/inorder.png)

<iframe src="https://leetcode.com/playground/3HqKP9gQ/shared" frameBorder="0" width="100%" height="293" name="3HqKP9gQ"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ to build a traversal. 
* Space complexity : $$\mathcal{O}(N)$$ to keep an inorder traversal.
<br />
<br />


---
#### Approach 2: Iterative Inorder Traversal

The above recursion could be converted into iteration, 
with the help of stack. This way one could speed up the solution 
because there is no need to build the entire inorder traversal,
and one could stop after the kth element.

![bla](../Figures/230/iteration.png)

<iframe src="https://leetcode.com/playground/BThpnsFo/shared" frameBorder="0" width="100%" height="361" name="BThpnsFo"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(H + k)$$, where $$H$$ is a tree height.
This complexity is defined by the stack, which contains at least $$H + k$$ elements, since before
starting to pop out one has to go down to a leaf. This results in 
$$\mathcal{O}(\log N + k)$$ for the balanced tree and 
$$\mathcal{O}(N + k)$$ for completely unbalanced tree with all
 the nodes in the left subtree. 
* Space complexity: $$\mathcal{O}(H)$$ to keep the stack, 
where $$H$$ is a tree height. That makes 
$$\mathcal{O}(N)$$ in the worst case of the skewed tree,
and $$\mathcal{O}(\log N)$$ in the average case of the balanced tree.
<br />
<br />


---
#### Follow up

> What if the BST is modified (insert/delete operations) 
often and you need to find the kth smallest frequently? 
How would you optimize the kthSmallest routine?

[Insert](https://leetcode.com/articles/insert-into-a-bst/) 
and [delete](https://leetcode.com/articles/delete-node-in-a-bst/) 
in a BST were discussed last week, the time complexity of these 
operations is $$\mathcal{O}(H)$$, where $$H$$ is a height of binary tree,
and $$H = \log N$$ for the balanced tree.

Hence without any optimisation insert/delete + search of kth element has 
$$\mathcal{O}(2H + k)$$ complexity. 
How to optimise that? 

That's a design question, 
basically we're asked to implement a structure 
which contains a BST inside and
optimises the following operations :

- Insert

- Delete

- Find kth smallest

Seems like a database description, isn't it? 
Let's use here the same logic as for [LRU cache](https://leetcode.com/articles/lru-cache/)
design, and combine an indexing structure (we could keep BST here)
with a double linked list. 

Such a structure would provide:

- $$\mathcal{O}(H)$$ time for the insert and delete.

- $$\mathcal{O}(k)$$ for the search of kth smallest.

![bla](../Figures/230/linked_list2.png)

The overall time complexity for insert/delete + search of kth smallest
is $$\mathcal{O}(H + k)$$ instead of $$\mathcal{O}(2H + k)$$. 

**Complexity Analysis**

* Time complexity for insert/delete + search of kth smallest: 
$$\mathcal{O}(H + k)$$, where $$H$$ is a tree height.
$$\mathcal{O}(\log N + k)$$ in the average case,
$$\mathcal{O}(N + k)$$ in the worst case.

* Space complexity : $$\mathcal{O}(N)$$ to keep the linked list.

## Accepted Submission (golang)
```golang
func inorder(root* TreeNode, k int, sa []int, ra []int) {
    if root == nil {
        return
    }
    inorder(root.Left, k, sa, ra)
    if sa[0] + 1 == k {
        sa[0]++
        ra[0] = root.Val
    } else if sa[0] + 1 < k {
        sa[0]++
        inorder(root.Right, k, sa, ra)
    }
}
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func kthSmallest(root *TreeNode, k int) int {
    sa := []int{0}
    ra := []int{0}
    inorder(root, k, sa, ra)
    return ra[0]
}
```

## Top Discussions
### 3 ways implemented in JAVA (Python): Binary Search, in-order iterative & recursive
- Author: angelvivienne
- Creation Date: Sun Jul 05 2015 10:06:24 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 18:49:57 GMT+0800 (Singapore Standard Time)

<p>
Binary Search (dfs): (edited 1/2019) this is NOT preferrable as in performance but since the quesiton is categorized with Binary Search tag, I was trying to solve it in that way.
- time complexity: O(N) best, O(N^2) worst

      public int kthSmallest(TreeNode root, int k) {
            int count = countNodes(root.left);
            if (k <= count) {
                return kthSmallest(root.left, k);
            } else if (k > count + 1) {
                return kthSmallest(root.right, k-1-count); // 1 is counted as current node
            }
            
            return root.val;
        }
        
        public int countNodes(TreeNode n) {
            if (n == null) return 0;
            
            return 1 + countNodes(n.left) + countNodes(n.right);
        }


DFS in-order recursive:
- time complexity: O(N)

        // better keep these two variables in a wrapper class
        private static int number = 0;
        private static int count = 0;

        public int kthSmallest(TreeNode root, int k) {
            count = k;
            helper(root);
            return number;
        }
        
        public void helper(TreeNode n) {
            if (n.left != null) helper(n.left);
            count--;
            if (count == 0) {
                number = n.val;
                return;
            }
            if (n.right != null) helper(n.right);
        }

DFS in-order iterative:
- time complexity: O(N) best

      public int kthSmallest(TreeNode root, int k) {
            Stack<TreeNode> st = new Stack<>();
            
            while (root != null) {
                st.push(root);
                root = root.left;
            }
                
            while (k != 0) {
                TreeNode n = st.pop();
                k--;
                if (k == 0) return n.val;
                TreeNode right = n.right;
                while (right != null) {
                    st.push(right);
                    right = right.left;
                }
            }
            
            return -1; // never hit if k is valid
      }

(edited: 7/2017)
**note: requirement has been changed a bit since last time I visited that the counting could be looked up frequently and BST itself could be altered (inserted/deleted) by multiple times, so that\'s the main reason that I stored them in an array.** 
```
class Solution(object):
    def kthSmallest(self, root, k):
        """
        :type root: TreeNode
        :type k: int
        :rtype: int
        """
        count = []
        self.helper(root, count)
        return count[k-1]
        
    def helper(self, node, count):
        if not node:
            return
        
        self.helper(node.left, count)
        count.append(node.val)
        self.helper(node.right, count)
```

DFS recursive, stop early when meet kth

```
def findNode(node, res):
            if len(res) > 1:
                return

            if node.left:
                findNode(node.left, res)

            res[0] -= 1
            if res[0] == 0:
                res.append(node.val)
                return
            
            if node.right:
                findNode(node.right, res)
                
        res = [k]
        findNode(root, res)
        return res[1]
```


Thanks again!
</p>


### What if you could modify the BST node's structure?
- Author: WHJ425
- Creation Date: Fri Jul 03 2015 05:55:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 11:54:24 GMT+0800 (Singapore Standard Time)

<p>
If we could add a count field in the BST node class, it will take O(n) time when we calculate the count value for the whole tree, but after that, it will take O(logn) time when insert/delete a node or calculate the kth smallest element.

       public class Solution {
            public int kthSmallest(TreeNode root, int k) {
                TreeNodeWithCount rootWithCount = buildTreeWithCount(root);
                return kthSmallest(rootWithCount, k);
            }
            
            private TreeNodeWithCount buildTreeWithCount(TreeNode root) {
                if (root == null) return null;
                TreeNodeWithCount rootWithCount = new TreeNodeWithCount(root.val);
                rootWithCount.left = buildTreeWithCount(root.left);
                rootWithCount.right = buildTreeWithCount(root.right);
                if (rootWithCount.left != null) rootWithCount.count += rootWithCount.left.count;
                if (rootWithCount.right != null) rootWithCount.count += rootWithCount.right.count;
                return rootWithCount;
            }
            
            private int kthSmallest(TreeNodeWithCount rootWithCount, int k) {
                if (k <= 0 || k > rootWithCount.count) return -1;
                if (rootWithCount.left != null) {
                    if (rootWithCount.left.count >= k) return kthSmallest(rootWithCount.left, k);
                    if (rootWithCount.left.count == k-1) return rootWithCount.val;
                    return kthSmallest(rootWithCount.right, k-1-rootWithCount.left.count);
                } else {
                    if (k == 1) return rootWithCount.val;
                    return kthSmallest(rootWithCount.right, k-1);
                }
            }
            
            class TreeNodeWithCount {
                int val;
                int count;
                TreeNodeWithCount left;
                TreeNodeWithCount right;
                TreeNodeWithCount(int x) {val = x; count = 1;};
            }
        }
</p>


### Python Easy Iterative and Recursive Solution
- Author: girikuncoro
- Creation Date: Mon Feb 15 2016 23:57:50 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 15:25:14 GMT+0800 (Singapore Standard Time)

<p>
Recursive:

    def kthSmallest(self, root, k):
        self.k = k
        self.res = None
        self.helper(root)
        return self.res
    
    def helper(self, node):
        if not node:
            return
        self.helper(node.left)
        self.k -= 1
        if self.k == 0:
            self.res = node.val
            return
        self.helper(node.right)


Iterative:

    def kthSmallest(root, k):
        stack = []
        while root or stack:
            while root:
                stack.append(root)
                root = root.left
            root = stack.pop()
            k -= 1
            if k == 0:
                return root.val
            root = root.right
</p>


