---
title: "Number of Students Doing Homework at a Given Time"
weight: 1335
#id: "number-of-students-doing-homework-at-a-given-time"
---
## Description
<div class="description">
<p>Given two integer arrays <code>startTime</code> and <code>endTime</code> and given an integer <code>queryTime</code>.</p>

<p>The <code>ith</code> student started doing their homework at the time <code>startTime[i]</code> and finished it at time <code>endTime[i]</code>.</p>

<p>Return <em>the number of students</em> doing their homework at time <code>queryTime</code>. More formally, return the number of students where <code>queryTime</code>&nbsp;lays in the interval <code>[startTime[i], endTime[i]]</code> inclusive.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> startTime = [1,2,3], endTime = [3,2,7], queryTime = 4
<strong>Output:</strong> 1
<strong>Explanation:</strong> We have 3 students where:
The first student started doing homework at time 1 and finished at time 3 and wasn&#39;t doing anything at time 4.
The second student started doing homework at time 2 and finished at time 2 and also wasn&#39;t doing anything at time 4.
The third student started doing homework at time 3 and finished at time 7 and was the only student doing homework at time 4.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> startTime = [4], endTime = [4], queryTime = 4
<strong>Output:</strong> 1
<strong>Explanation:</strong> The only student was doing their homework at the queryTime.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> startTime = [4], endTime = [4], queryTime = 5
<strong>Output:</strong> 0
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> startTime = [1,1,1,1], endTime = [1,3,2,4], queryTime = 7
<strong>Output:</strong> 0
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> startTime = [9,8,7,6,5,4,3,2,1], endTime = [10,10,10,10,10,10,10,10,10], queryTime = 5
<strong>Output:</strong> 5
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>startTime.length == endTime.length</code></li>
	<li><code>1 &lt;= startTime.length &lt;= 100</code></li>
	<li><code>1 &lt;= startTime[i] &lt;= endTime[i] &lt;= 1000</code></li>
	<li><code>1 &lt;=&nbsp;queryTime &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] simple code.
- Author: rock
- Creation Date: Sun May 17 2020 12:04:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 13:57:05 GMT+0800 (Singapore Standard Time)

<p>
```java
    public int busyStudent(int[] startTime, int[] endTime, int queryTime) {
        int res = 0;
        for (int i = 0; i < startTime.length; ++i) {
            res += startTime[i] <= queryTime && queryTime <= endTime[i] ? 1 : 0;
        }
        return res;        
    }
```
```python
    def busyStudent(self, startTime: List[int], endTime: List[int], queryTime: int) -> int:
        return sum(s <= queryTime <= e for s, e in zip(startTime, endTime))
```
</p>


### [c++] Easyst as You Like 4 line sol
- Author: suman_buie
- Creation Date: Sun May 17 2020 12:07:09 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 29 2020 19:28:39 GMT+0800 (Singapore Standard Time)

<p>
```
    int busyStudent(vector<int>& startTime, vector<int>& endTime, int queryTime) {
        int ans = 0;
        for(int i=0;i<startTime.size();i++)
            if(startTime[i]<=queryTime&&endTime[i]>=queryTime)ans++;
        return ans;
    }
```

</p>


### [JavaScript] Easy to understand - 3 solutions - 1 line
- Author: poppinlp
- Creation Date: Mon May 18 2020 11:55:06 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 18 2020 11:55:25 GMT+0800 (Singapore Standard Time)

<p>
## SOLUTION 1

Straight forward solution. We just traversal the `startTime` and `endTime` and check the `queryTime`.

```js
const busyStudent = (startTime, endTime, queryTime) => {
  let ret = 0;
  for (let i = 0; i < startTime.length; ++i) {
    startTime[i] <= queryTime && endTime[i] >= queryTime && ++ret;
  }
  return ret;
};
```

## SOLUTION 2

We could use the `filter` method on `Array.prototype` to do it.

```js
const busyStudent = (startTime, endTime, queryTime) => startTime.filter((t, i) => queryTime >= t && queryTime <= endTime[i]).length;
```

## SOLUTION 3

We could also use the `reduce` method on `Array.prototype` to do it.

```js
const busyStudent = (startTime, endTime, queryTime) => startTime.reduce((prev, t, i) => queryTime >= t && queryTime <= endTime[i] ? prev + 1 : prev, 0);
```
</p>


