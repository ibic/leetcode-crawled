---
title: "Find Longest Awesome Substring"
weight: 1414
#id: "find-longest-awesome-substring"
---
## Description
<div class="description">
<p>Given a string <code>s</code>. An <em>awesome</em> substring is a non-empty substring of <code>s</code> such that we can make any number of swaps in order to make it palindrome.</p>

<p>Return the length of the maximum length <strong>awesome substring</strong> of <code>s</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;3242415&quot;
<strong>Output:</strong> 5
<strong>Explanation:</strong> &quot;24241&quot; is the longest awesome substring, we can form the palindrome &quot;24142&quot; with some swaps.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;12345678&quot;
<strong>Output:</strong> 1
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;213123&quot;
<strong>Output:</strong> 6
<strong>Explanation:</strong> &quot;213123&quot; is the longest awesome substring, we can form the palindrome &quot;231132&quot; with some swaps.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;00&quot;
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 10^5</code></li>
	<li><code>s</code> consists only of digits.</li>
</ul>

</div>

## Tags
- String (string)
- Bit Manipulation (bit-manipulation)

## Companies
- Directi - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java/Python3 with picture (similar to 1371)
- Author: votrubac
- Creation Date: Sun Aug 09 2020 00:01:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 09 2020 01:50:42 GMT+0800 (Singapore Standard Time)

<p>
#### Intuition
This is similar to [1371. Find the Longest Substring Containing Vowels in Even Counts](https://leetcode.com/problems/find-the-longest-substring-containing-vowels-in-even-counts/discuss/534135/C%2B%2BJava-with-picture).

We can form a palindrome if the count of each included number (expcept maybe one) is even, and this is the property of an awesome string.

A bit in our `mask` is `0` if the count for the corresponding number is even, and `1` if it\'s odd.  As we go through the string, we track odd/even counts in our mask. If we see the same `mask` again, the subarray between first poistion (exclusive) and the current position (inclusive) with the same `mask` has all numbers with the even count. 

We will use the `dp` array to track the smallest (first) position of each `mask`. We can get the size of the substring by substracting it from the current poisition. 

> Note that position for zero mask is `-1`, as we need to include the very first character.

Now, the difference from #1371 is that a palindrome can have up to one number with the odd count. Therefore, we need to also check all masks that are different from the current one by one bit. In other words, if two masks are different by one bit, that means that there is one odd count in the substring.

![image](https://assets.leetcode.com/users/images/4fb7c48e-699d-4303-ade3-52300e87abf6_1596907985.4008424.png)

**C++**
```cpp
int longestAwesome(string s) {
    vector<int> dp(1024, s.size());
    int res = 0, mask = 0;
    dp[0] = -1;
    for (auto i = 0; i < s.size(); ++i) {
        mask ^= 1 << (s[i] - \'0\');
        res = max(res, i - dp[mask]);
        for (auto j = 0; j <= 9; ++j)
            res = max(res, i - dp[mask ^ (1 << j)]);
        dp[mask] = min(dp[mask], i);
    }
    return res;
}
```
**Java**
```java
public int longestAwesome(String s) {
    int dp[] = new int[1024];
    Arrays.fill(dp, s.length());
    int res = 0, mask = 0;
    dp[0] = -1;
    for (int i = 0; i < s.length(); ++i) {
        mask ^= 1 << (s.charAt(i) - \'0\');
        res = Math.max(res, i - dp[mask]);
        for (int j = 0; j <= 9; ++j)
            res = Math.max(res, i - dp[mask ^ (1 << j)]);
        dp[mask] = Math.min(dp[mask], i);
    }
    return res;
}
```

**Python**
```python
class Solution:
    def longestAwesome(self, s: str) -> int:
        mask, res = 0, 0
        dp = [-1] + [len(s)] * 1023
        for i in range(len(s)):
            mask ^= 1 << (ord(s[i]) - 48)
            for j in range(11):
                check_mask = 1023 & (mask ^ (1 << j))
                res = max(res, i - dp[check_mask])
            dp[mask] = min(dp[mask], i)
        return res
```

#### Complexity Analysis
- Time: O(nk), where k is the number of unique characters we track. We go through the string once, and do k checks for each character.
- Memory: O(2^ k), where k is the number of unique characters to track.
</p>


### Example Input "3242415" Explanation with BitMask
- Author: mstr_shifu
- Creation Date: Tue Aug 11 2020 13:40:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 22 2020 03:00:55 GMT+0800 (Singapore Standard Time)

<p>
**Definition of Awesomeness**
String S is a palindrome if S equals to its reversed version: "1112111"
Any string is awesome if we can reorder its characters to make it a palindrome: 
"24241" can be reordered to "24142".

**Properties of Awesome string**
There are 2 cases that a string can be reordered to be a palindrome:
**Case 1**. All of its characters have even occurrences: 
		"2424" -> there are two \'2\'s and two \'4\'s.
**Case 2**. All of its characters have even occurrences except one: 
		"24241" -> there are two \'2\'s, two \'4\'s and one \'1\'
		
**Brute force solution**
The most straighforward solution would be keeping track of all counters while looping through characters. Once we are at position i, we go back and subtract all our previous counters from our current counter to check if any of them is awesome. However, its time complexity is O(10*N^2): 10 digits * N chars * N comparisons which is not desirable

**BitMask solution**
We don\'t have to keep track of counters, we are only interested in odd counts in a substring. We can use one bit to say if some digit has even or odd count at any point.
Let 0 represent "even" and 1 represent "odd". 
For example, for an input of "233":
1. i = 0, char = \'2\', xor 2nd bit from right:
	mask = "100"
2. i = 1, char = \'3\', xor 3rd bit from right:
	mask = "1100"
3. i = 2, char = \'3\', xor 3rd bit from right:
	mask = "0100"
	
The last mask is "0100" which says it has only one number with odd count, so, the input can be rearranged to make it a palindrome: "233" => "323".

Even simpler, if the input is "22", we set and unset 2nd bit, then the remaining mask would be "000", which says we have numbers with all having even counts.

Now, let\'s analyze the example input "3242415" from problem definition.
1. i = 0, char = \'3\', xor 3rd bit from right:

		mask = \'0000001000\'
2. i = 1, char = \'2\', xor 2nd bit from right:
	
		mask = \'0000001100\'
3. i = 2, char = \'4\', xor 4th bit from right:
    
		mask = \'0000011100\'
4. i = 3, char = \'2\', xor 2nd bit from right:
	
		mask = \'0000011000\'
5. i = 4, char = \'4\', xor 4th bit from right:
	
		mask = \'0000001000\'
6. i = 5, char = \'1\', xor 1st bit from right:
	
		mask = \'0000001010\'
7. i = 6, char = \'5\', xor 5th bit from right:
	
		mask = \'0000101010\'
	
The problem asks to find the longest awesome substring, it can be anywhere between i and j such that i <= j <= lenth of input. For this, on every step above, we need to memoize the current mask and check if we have seen similar mask before.
Update the answer if:
		a. We have seen similar mask before.
		b. We have seen a mask such that it differs from the current mask by one bit being different.

```
class Solution:
    def longestAwesome(self, s: str) -> int:
        n = len(s)
        ans, mask = 0, 0
        
        memo = [n] * 1024
        memo[0] = -1
        
        for i in range(n):
            mask ^= 1 << int(s[i])
			
			# Case 1. Check if we have seen similar mask
            ans = max(ans, i - memo[mask])
            
			# Case 2. Check for masks that differ by one bit
            for j in range(10):
                test_mask = mask ^ (1 << j)
                ans = max(ans, i - memo[test_mask])
                
			# save the earliest position
            memo[mask] = min(memo[mask], i)    
        
        return ans
```
There are number of questions arise from the above solution:
**Question 1. Why 1024?**
Since the input only contains 10 digits ("0123456789"), we can only see 2^10 or 1024 variations of bit masks: 0000000000, 0000000001, .... , 1111111110, 1111111111
**Question 2. What is 1 << int(s[i])**
It shifts 1 by s[i] times, for ex: 1 << 3 gives us \'1000\'.
And it updates the current mask: 0000 ^ 1000 = 1000 or 1110 ^ 1000 = 0110 
If you didn\'t get it, please, read bit operations in depth.
**Question 3. What is the test mask**
The test mask in above solution alters one of the bits of the current mask to check for Case 2.
**Question 4. Why do we check for similar masks?**
That means between the similar mask and the current mask, we have seen such digits that xored themselves along the way and disappeared from the mask. Only if the number of digits is even, they can disappear from the current mask: for ex: "223", the current mask evolution is: "100" -> "000" -> "1000". 
**Question 5. Why do we check for masks that differ by one bit?**
Since it is allowed to have one number with odd count, we can check for masks that differ by one bit from the current mask.

I think, you got this, let me know if you have any other questions.

</p>


### [Java/C++/Python] Prefix + BitMask
- Author: lee215
- Creation Date: Sun Aug 09 2020 00:02:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 09 2020 00:14:48 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Similar to [1371. Find the Longest Substring Containing Vowels in Even Counts](https://leetcode.com/problems/find-the-longest-substring-containing-vowels-in-even-counts/discuss/531840/JavaC++Python-One-Pass).
<br>

## **Explanation**
We have 10 + 1 types of palindromes.
Use any number as the middle character, or don\'t have a middle character.
<br>

## **Complexity**
Time `O(10N)`
Space `O(1024)`
<br>

**Java:**
```java
    public int longestAwesome(String s) {
        int res = 0, cur = 0, n = s.length(), seen[] = new int[1024];
        Arrays.fill(seen, n);
        seen[0] = -1;
        for (int i = 0; i < n; ++i) {
            cur ^= 1 << (s.charAt(i) - \'0\');
            for (int a = 0; a < 10; ++a)
                res = Math.max(res, i - seen[cur ^ (1 << a)]);
            res = Math.max(res, i - seen[cur]);
            seen[cur] = Math.min(seen[cur], i);
        }
        return res;
    }
```

**C++:**
```cpp
    int longestAwesome(string s) {
        int res = 0, cur = 0, n = s.size();
        vector<int> seen(1024, n);
        seen[0] = -1;
        for (int i = 0; i < n; ++i) {
            cur ^= 1 << (s[i] - \'0\');
            for (int a = 0; a < 10; ++a)
                res = max(res, i - seen[cur ^ (1 << a)]);
            res = max(res, i - seen[cur]);
            seen[cur] = min(seen[cur], i);
        }
        return res;
    }
```

**Python:**
```py
    def longestAwesome(self, s):
        res, cur, n = 0, 0, len(s)
        seen = [-1] + [n] * 1024
        for i, c in enumerate(s):
            cur ^= 1 << int(c)
            for a in xrange(10):
                res = max(res, i - seen[cur ^ (1 << a)])
            res = max(res, i - seen[cur])
            seen[cur] = min(seen[cur], i)
        return res
```

</p>


