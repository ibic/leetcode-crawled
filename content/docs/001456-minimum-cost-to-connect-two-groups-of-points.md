---
title: "Minimum Cost to Connect Two Groups of Points"
weight: 1456
#id: "minimum-cost-to-connect-two-groups-of-points"
---
## Description
<div class="description">
<p>You are given two groups of points&nbsp;where the first group&nbsp;has <code><font face="monospace">size<sub>1</sub></font></code>&nbsp;points,&nbsp;the second group&nbsp;has <code><font face="monospace">size<sub>2</sub></font></code>&nbsp;points,&nbsp;and&nbsp;<code><font face="monospace">size<sub>1</sub></font> &gt;= <font face="monospace">size<sub>2</sub></font></code>.</p>

<p>The <code>cost</code> of the connection between any two points&nbsp;are given in an&nbsp;<code><font face="monospace">size<sub>1</sub></font> x <font face="monospace">size<sub>2</sub></font></code>&nbsp;matrix where <code>cost[i][j]</code> is the cost of connecting point <code>i</code> of the first group and point&nbsp;<code>j</code> of the second group. The groups are connected if <strong>each point in both&nbsp;groups is&nbsp;connected to one or more points in the opposite&nbsp;group</strong>. In other words, each&nbsp;point in the first group must be connected to at least one point in the second group, and each&nbsp;point in the second group must be connected to at least one point in the first group.</p>

<p>Return&nbsp;<em>the minimum cost it takes to connect the two groups</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/03/ex1.jpg" style="width: 322px; height: 243px;" />
<pre>
<strong>Input:</strong> cost = [[15, 96], [36, 2]]
<strong>Output:</strong> 17
<strong>Explanation</strong>: The optimal way of connecting the groups is:
1--A
2--B
This results in a total cost of 17.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/03/ex2.jpg" style="width: 322px; height: 403px;" />
<pre>
<strong>Input:</strong> cost = [[1, 3, 5], [4, 1, 1], [1, 5, 3]]
<strong>Output:</strong> 4
<strong>Explanation</strong>: The optimal way of connecting the groups is:
1--A
2--B
2--C
3--A
This results in a total cost of 4.
Note that there are multiple points connected to point 2 in the first group and point A in the second group. This does not matter as there is no limit to the number of points that can be connected. We only care about the minimum total cost.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> cost = [[2, 5, 1], [3, 4, 7], [8, 1, 2], [6, 2, 4], [3, 8, 8]]
<strong>Output:</strong> 10
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code><font face="monospace">size<sub>1</sub></font> == cost.length</code></li>
	<li><code><font face="monospace">size<sub>2</sub></font> == cost[i].length</code></li>
	<li><code>1 &lt;= <font face="monospace">size<sub>1</sub></font>, <font face="monospace">size<sub>2</sub></font> &lt;= 12</code></li>
	<li><code><font face="monospace">size<sub>1</sub></font> &gt;=&nbsp;<font face="monospace">size<sub>2</sub></font></code></li>
	<li><code>0 &lt;= cost[i][j] &lt;= 100</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Graph (graph)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Python DP using mask
- Author: votrubac
- Creation Date: Sun Sep 20 2020 12:01:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 21 2020 14:29:34 GMT+0800 (Singapore Standard Time)

<p>
#### Intuition
As usual, it pays to analyze problem constraints. Since we only have up to 12 points, we can track which ones are connected using a bit mask.



#### Solution
Staightforward top-down DP for the first group. At the same time, we track which elements from the second group were connected in `mask`.

After finishing with the first group, we detect elements in group 2 that are still disconnected, and connect them with the "cheapest" node in the first group.

**Python**
```python
def connectTwoGroups(self, cost: List[List[int]]) -> int:
    sz1, sz2 = len(cost), len(cost[0])
    min_sz2 = [min([cost[i][j] for i in range(sz1)]) for j in range(sz2)]
    @lru_cache(None)
    def dfs(i: int, mask: int):
        res = 0 if i >= sz1 else float(\'inf\')
        if i >= sz1:
            for j in range(sz2):
                if mask & (1 << j) == 0:
                    res += min_sz2[j]
        else:
            for j in range(sz2):
                res = min(res, cost[i][j] + dfs(i + 1, mask | (1 << j)))
        return res
    return dfs(0, 0)
```
**C++**
```cpp
int dp[13][4096] = {};
int dfs(vector<vector<int>>& cost, vector<int> &min_sz2, int i, int mask) {
    if (dp[i][mask])
        return dp[i][mask] - 1;    
    int res = i >= cost.size() ? 0 : INT_MAX;
    if (i >= cost.size())
        for (auto j = 0; j < cost[0].size(); ++j)
            res += min_sz2[j] * ((mask & (1 << j)) == 0);
    else
        for (auto j = 0; j < cost[0].size(); ++j)
            res = min(res, cost[i][j] + dfs(cost, min_sz2, i + 1, mask | (1 << j)));
    dp[i][mask] = res + 1;
    return res;
}
int connectTwoGroups(vector<vector<int>>& cost) {
    vector<int> min_sz2(cost[0].size(), INT_MAX);
    for (int j = 0; j < min_sz2.size(); ++j)
        for (int i = 0; i < cost.size(); ++i)
            min_sz2[j] = min(min_sz2[j], cost[i][j]);
    return dfs(cost, min_sz2, 0, 0);
}
```
**Complexity Analysis**
- Time:  O((n * 2 ^ m) * m) - it takes `m` to compute one state.
- Memory: O(n * 2 ^ m) states to compute
</p>


### [C++ - 4ms - beats 100%] Bipartite graph maximum weight matching
- Author: EulerianDescent
- Creation Date: Sun Sep 20 2020 17:14:29 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 21 2020 14:19:37 GMT+0800 (Singapore Standard Time)

<p>
I just found most people solve this problem via bit mask DP, which runs in *expoential* time. Here, I give a *polynomial* time solution.

**Explanation**:
This problem is called *minimum-cost edge cover* for bipartite graph. We show how to reduce this to a matching problem.
We note that some edge will cover both of endpoints, and some edge will only cover of their endpoints. If the edge only covers one of their endpoints, then this edge must be the edge with minimum weight incident to that particular endpoint. Let c_v be the cost of the minimum weight edge incident to vertex v, and w_(u,v) be the weight of edge (u,v), then the cost of a solution is 
![image](https://assets.leetcode.com/users/images/e04f65c8-0d40-4262-81ce-3ff85efd75e9_1600669162.3114378.png)


We note that the first sum is fixed, and the second we want maximum the second sum. Hence, the problem becomes find a maximum weight matching with edge weight c_v+c_u-w_(u,v). Using Hungarian algorithm, we can find such matching for bipartite graph in O(n^3).

**Implementation**:
```
class Solution {
public:
   #define getL(x) (x+2)
   #define getR(x) (2+Lmin.size()+x) 

    int connectTwoGroups(vector<vector<int>>& cost) {
        vector<int> Lmin(cost.size());
        vector<int> Rmin(cost[0].size());
        int ans = 0;
        for(int i = 0; i < cost.size();++i) {
            Lmin[i] = INT_MAX;
            for(int j = 0; j < Rmin.size(); ++j) {
                Lmin[i] = min(Lmin[i], cost[i][j]);
            }
            ans += Lmin[i];
        }
        for(int j = 0; j < Rmin.size();++j) {
            Rmin[j] = INT_MAX;
            for(int i = 0; i < Lmin.size(); ++i) {
                Rmin[j] = min(Rmin[j],cost[i][j]);
            }
            ans += Rmin[j];
        }
        hungarian<int> H(Lmin.size(),Rmin.size());
        for(int i = 0; i < Lmin.size(); ++i) {
            for(int j = 0; j < Rmin.size(); ++j) {
                H.addEdge(i,j,-1*(cost[i][j]-Lmin[i]-Rmin[j]));
            }
        }
        int ret = H.solve();
        ans = ans - ret;
        return ans;
    }
};
```

Template for Hungarian Algorithm (Kuhn\u2013Munkres algorithm):
```
template <typename T>
struct hungarian
{ // km
    int n;
    vector<int> matchx;
    vector<int> matchy;
    vector<int> pre;
    vector<bool> visx;
    vector<bool> visy;
    vector<T> lx;
    vector<T> ly;
    vector<vector<T>> g;
    vector<T> slack;
    T inf;
    T res;
    queue<int> q;
    int org_n;
    int org_m;

    hungarian(int _n, int _m)
    {
        org_n = _n;
        org_m = _m;
        n = max(_n, _m);
        inf = numeric_limits<T>::max();
        res = 0;
        g = vector<vector<T>>(n, vector<T>(n));
        matchx = vector<int>(n, -1);
        matchy = vector<int>(n, -1);
        pre = vector<int>(n);
        visx = vector<bool>(n);
        visy = vector<bool>(n);
        lx = vector<T>(n, -inf);
        ly = vector<T>(n);
        slack = vector<T>(n);
    }

    void addEdge(int u, int v, int w)
    {
        g[u][v] = max(w, 0); 
    }

    bool check(int v)
    {
        visy[v] = true;
        if (matchy[v] != -1)
        {
            q.push(matchy[v]);
            visx[matchy[v]] = true;
            return false;
        }
        while (v != -1)
        {
            matchy[v] = pre[v];
            swap(v, matchx[pre[v]]);
        }
        return true;
    }

    void bfs(int i)
    {
        while (!q.empty())
        {
            q.pop();
        }
        q.push(i);
        visx[i] = true;
        while (true)
        {
            while (!q.empty())
            {
                int u = q.front();
                q.pop();
                for (int v = 0; v < n; v++)
                {
                    if (!visy[v])
                    {
                        T delta = lx[u] + ly[v] - g[u][v];
                        if (slack[v] >= delta)
                        {
                            pre[v] = u;
                            if (delta)
                            {
                                slack[v] = delta;
                            }
                            else if (check(v))
                            {
                                return;
                            }
                        }
                    }
                }
            }
            T a = inf;
            for (int j = 0; j < n; j++)
            {
                if (!visy[j])
                {
                    a = min(a, slack[j]);
                }
            }
            for (int j = 0; j < n; j++)
            {
                if (visx[j])
                { // S
                    lx[j] -= a;
                }
                if (visy[j])
                { // T
                    ly[j] += a;
                }
                else
                { // T\'
                    slack[j] -= a;
                }
            }
            for (int j = 0; j < n; j++)
            {
                if (!visy[j] && slack[j] == 0 && check(j))
                {
                    return;
                }
            }
        }
    }

    int solve()
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                lx[i] = max(lx[i], g[i][j]);
            }
        }

        for (int i = 0; i < n; i++)
        {
            fill(slack.begin(), slack.end(), inf);
            fill(visx.begin(), visx.end(), false);
            fill(visy.begin(), visy.end(), false);
            bfs(i);
        }

        for (int i = 0; i < n; i++)
        {
            if (g[i][matchx[i]] > 0)
            {
                res += g[i][matchx[i]];
            }
            else
            {
                matchx[i] = -1;
            }
        }
        return res;
    }
};
```
</p>


### [Python] Clean DP + Bitmask solution with explaination
- Author: alanlzl
- Creation Date: Sun Sep 20 2020 12:16:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 20 2020 12:30:51 GMT+0800 (Singapore Standard Time)

<p>
**Idea**

There are basically two steps:
1. Connect all group1 nodes to group2. Each group1 node only sends out 1 edge.
2. For all the unconnected nodes in group2, connect them with their min-cost counterparty in group1.

We use DP + Bitmask to achieve this. The step 1 above is the DP transition step and the step 2 above is the DP base case.

<br />

**Python**
```Python
class Solution:
    def connectTwoGroups(self, cost: List[List[int]]) -> int:
        m, n = len(cost), len(cost[0])
        min_arr = [min(x) for x in zip(*cost)]
        
        @lru_cache(None)
        def dp(i, mask):
            if i == m:
                ans = 0
                for j in range(n):
                    if not mask & (1 << j):
                        ans += min_arr[j]
                return ans
            
            ans = float(\'inf\')
            for j in range(n):
                ans = min(ans, cost[i][j] + dp(i + 1, mask | (1 << j)))
            return ans
        
        return dp(0, 0)
```
</p>


