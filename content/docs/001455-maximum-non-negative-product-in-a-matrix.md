---
title: "Maximum Non Negative Product in a Matrix"
weight: 1455
#id: "maximum-non-negative-product-in-a-matrix"
---
## Description
<div class="description">
<p>You are given a&nbsp;<code>rows x cols</code>&nbsp;matrix&nbsp;<code>grid</code>.&nbsp;Initially, you&nbsp;are located at the top-left&nbsp;corner <code>(0, 0)</code>,&nbsp;and in each step, you can only <strong>move right&nbsp;or&nbsp;down</strong> in the matrix.</p>

<p>Among all possible paths starting from the top-left corner&nbsp;<code>(0, 0)</code>&nbsp;and ending in the bottom-right corner&nbsp;<code>(rows - 1, cols - 1)</code>, find the path with the&nbsp;<strong>maximum non-negative product</strong>. The product of a path is the product of all integers in the grid cells visited along the path.</p>

<p>Return the&nbsp;<em>maximum non-negative product&nbsp;<strong>modulo</strong>&nbsp;</em><code>10<sup>9</sup>&nbsp;+ 7</code>.&nbsp;<em>If the maximum product is <strong>negative</strong> return&nbsp;</em><code>-1</code>.</p>

<p><strong>Notice that the modulo is performed after getting the maximum product.</strong></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> grid = [[-1,-2,-3],
&nbsp;              [-2,-3,-3],
&nbsp;              [-3,-3,-2]]
<strong>Output:</strong> -1
<strong>Explanation:</strong> It&#39;s not possible to get non-negative product in the path from (0, 0) to (2, 2), so return -1.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> grid = [[<strong>1</strong>,-2,1],
&nbsp;              [<strong>1</strong>,<strong>-2</strong>,1],
&nbsp;              [3,<strong>-4</strong>,<strong>1</strong>]]
<strong>Output:</strong> 8
<strong>Explanation:</strong> Maximum non-negative product is in bold (1 * 1 * -2 * -4 * 1 = 8).
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> grid = [[<strong>1</strong>, 3],
&nbsp;              [<strong>0</strong>,<strong>-4</strong>]]
<strong>Output:</strong> 0
<strong>Explanation:</strong> Maximum non-negative product is in bold (1 * 0 * -4 = 0).
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> grid = [[ <strong>1</strong>, 4,4,0],
&nbsp;              [<strong>-2</strong>, 0,0,1],
&nbsp;              [ <strong>1</strong>,<strong>-1</strong>,<strong>1</strong>,<strong>1</strong>]]
<strong>Output:</strong> 2
<strong>Explanation:</strong> Maximum non-negative product is in bold (1 * -2 * 1 * -1 * 1 * 1 = 2).
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= rows, cols &lt;= 15</code></li>
	<li><code>-4 &lt;= grid[i][j] &lt;= 4</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Greedy (greedy)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] Dynamic Programming With Comments
- Author: jojonicho
- Creation Date: Sun Sep 20 2020 12:03:20 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 21 2020 21:20:54 GMT+0800 (Singapore Standard Time)

<p>
Similiar problem: 
[152. Maximum Product Subarray](https://leetcode.com/problems/maximum-product-subarray/)
[64. Minimum Path Sum](https://leetcode.com/problems/minimum-path-sum/)

Main idea:
1. Keep track of two DP vectors, mx and mn
2. update mx and mn based on the current number in grid (positive or negative)
3. if current number is positive, use mx. else use mn
```
    int maxProductPath(vector<vector<int>>& grid) {
        int m=grid.size(), n=grid[0].size(), MOD = 1e9+7;
        // we use long long to avoid overflow
        vector<vector<long long>>mx(m,vector<long long>(n)), mn(m,vector<long long>(n));
        mx[0][0]=mn[0][0]=grid[0][0];
        
        // initialize the top and left sides
        for(int i=1; i<m; i++){
            mn[i][0] = mx[i][0] = mx[i-1][0] * grid[i][0];
        }
        for(int j=1; j<n; j++){
            mn[0][j] = mx[0][j] = mx[0][j-1] * grid[0][j];
        }

        for(int i=1; i<m; i++){
            for(int j=1; j<n; j++){
 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0if(grid[i][j] < 0){ // minimum product * negative number = new maximum product
 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0mx[i][j] = (min(mn[i-1][j], mn[i][j-1]) * grid[i][j]);
                    mn[i][j] = (max(mx[i-1][j], mx[i][j-1]) * grid[i][j]);
                }
 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0else{ // maximum product * positive number = new maximum product
 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0 \xA0mx[i][j] = (max(mx[i-1][j], mx[i][j-1]) * grid[i][j]);
                    mn[i][j] = (min(mn[i-1][j], mn[i][j-1]) * grid[i][j]);
                }
            }
        }

        int ans = mx[m-1][n-1] % MOD;
        return ans < 0 ? -1 : ans;
    }
```
</p>


### Java Simple DP, beat 100%
- Author: hobiter
- Creation Date: Sun Sep 20 2020 12:05:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 27 2020 10:30:26 GMT+0800 (Singapore Standard Time)

<p>
pitfalls: Please do mod on the destination only, or it will return wrong result.

 dp[i][j][1] is the minimum value you can get when you reach g[i][j],  dp[i][j][0] is the max, similarly.
 see this similar problem for detail: https://leetcode.com/problems/maximum-product-subarray/
```
    public int maxProductPath(int[][] g) {
        int m = g.length, n = g[0].length, mod = 1_000_000_007;
        long dp[][][] = new long[m][n][2];
        dp[0][0] = new long[]{g[0][0], g[0][0]};
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 && j == 0) continue;
                long a = 0, b = 0;
                if (i == 0) {
                    dp[i][j][0] = dp[i][j][1] = g[i][j] * dp[i][j - 1][0];
                } else if (j == 0) { 
                    dp[i][j][0] = dp[i][j][1] = g[i][j] * dp[i - 1][j][0];
                } else {
                    a = g[i][j] * Math.max(dp[i][j - 1][0], dp[i - 1][j][0]);
                    b = g[i][j] * Math.min(dp[i][j - 1][1], dp[i - 1][j][1]);
                    dp[i][j][0] = Math.max(a, b);
                    dp[i][j][1] = Math.min(a, b);
                }
            }
        }
        if (dp[m - 1][n - 1][0] < 0) return -1;
        return (int) ((dp[m - 1][n - 1][0]) % mod);
    }
```
</p>


### Java DFS(Easy to understand) and DP(Best time complexity)
- Author: sujarosejohn
- Creation Date: Sun Sep 20 2020 12:13:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 20 2020 21:40:52 GMT+0800 (Singapore Standard Time)

<p>
**DFS Solution:**
```
class Solution {
    int mod = 1000000007;
    long product = -1;
    
    public int maxProductPath(int[][] grid) {
        dfs(grid,0,0,grid[0][0]);    
        return (int)(product%mod);
    }
    
    public void dfs(int[][] grid, int i, int j, long curr){
        if(i==grid.length-1 && j==grid[0].length-1){
            product=Math.max(product,curr);
            return;
        }
        if(grid[i][j]==0){
            product=Math.max(product,0);
            return;
        }
        if(i+1<grid.length)
            dfs(grid,i+1,j,curr*grid[i+1][j]);
        if(j+1<grid[0].length)
            dfs(grid,i,j+1,curr*grid[i][j+1]);
    }
}
```
Please note that the DFS solution is very easy to understand, but it doesn\'t have the best time complexity. **Update:** Thank you @zhuragat, I have updated the product variable above as long instead of double.


**DP Solution:**
A much better dynamic programming solution that beats 100% time complexity. Logic:
1. at every cell, we store the current overall min and max product paths in anticipation for a future negative/positive number
2. (current min negative number * future negative number) could be greater than (current max positive number * future positive number)

```
class Solution {    
    //define a Pair object to store min and max path product at every cell in the grid
    class Pair{
        Long min;
        Long max;
        Pair(Long min, Long max){
            this.min=min;
            this.max=max;
        }
    }
    
    public int maxProductPath(int[][] grid) {
        if(grid.length==0)
            return 0;
        
        int r = grid.length;
        int c = grid[0].length;
        int mod = 1000000007;
        
        Pair[][] dp = new Pair[r][c];
        
        //initialize first cell as (grid[0][0],grid[0][0]) since min and max are the same here
        dp[0][0]= new Pair((long)grid[0][0],(long)grid[0][0]);
        
        //initialize first row, min=max= prev row product* grid[i][j]
        for(int i=1;i<r;i++)
            dp[i][0]= new Pair(dp[i-1][0].min*grid[i][0],dp[i-1][0].max*grid[i][0]);
       
        //initialize first column, min=max= prev col product* grid[i][j]
        for(int j=1;j<c;j++)
            dp[0][j]= new Pair(dp[0][j-1].min*grid[0][j],dp[0][j-1].max*grid[0][j]);
        
        //from grid[1][1] to grid[r][c]
        for(int i=1;i<r;i++){
            for(int j=1;j<c;j++){
                long min = grid[i][j] * Math.min(dp[i-1][j].min,dp[i][j-1].min);
                long max = grid[i][j] * Math.max(dp[i-1][j].max,dp[i][j-1].max);
                dp[i][j] = new Pair(Math.min(min,max), Math.max(min,max));
            }
        }
        return dp[r-1][c-1].max<0?-1:(int)(dp[r-1][c-1].max%mod);       
    }
}
```
</p>


