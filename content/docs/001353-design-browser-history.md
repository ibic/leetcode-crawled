---
title: "Design Browser History"
weight: 1353
#id: "design-browser-history"
---
## Description
<div class="description">
<p>You have a <strong>browser</strong> of one tab where you start on the <code>homepage</code> and you can visit another <code>url</code>, get back in the history number of <code>steps</code> or move forward in the history number of <code>steps</code>.</p>

<p>Implement the <code>BrowserHistory</code> class:</p>

<ul>
	<li><code>BrowserHistory(string homepage)</code> Initializes the object with the <code>homepage</code>&nbsp;of the browser.</li>
	<li><code>void visit(string url)</code>&nbsp;Visits&nbsp;<code>url</code> from the current page. It clears up all the forward history.</li>
	<li><code>string back(int steps)</code>&nbsp;Move <code>steps</code> back in history. If you can only return <code>x</code> steps in the history and <code>steps &gt; x</code>, you will&nbsp;return only <code>x</code> steps. Return the current <code>url</code>&nbsp;after moving back in history <strong>at most</strong> <code>steps</code>.</li>
	<li><code>string forward(int steps)</code>&nbsp;Move <code>steps</code> forward in history. If you can only forward <code>x</code> steps in the history and <code>steps &gt; x</code>, you will&nbsp;forward only&nbsp;<code>x</code> steps. Return the current <code>url</code>&nbsp;after forwarding in history <strong>at most</strong> <code>steps</code>.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example:</strong></p>

<pre>
<b>Input:</b>
[&quot;BrowserHistory&quot;,&quot;visit&quot;,&quot;visit&quot;,&quot;visit&quot;,&quot;back&quot;,&quot;back&quot;,&quot;forward&quot;,&quot;visit&quot;,&quot;forward&quot;,&quot;back&quot;,&quot;back&quot;]
[[&quot;leetcode.com&quot;],[&quot;google.com&quot;],[&quot;facebook.com&quot;],[&quot;youtube.com&quot;],[1],[1],[1],[&quot;linkedin.com&quot;],[2],[2],[7]]
<b>Output:</b>
[null,null,null,null,&quot;facebook.com&quot;,&quot;google.com&quot;,&quot;facebook.com&quot;,null,&quot;linkedin.com&quot;,&quot;google.com&quot;,&quot;leetcode.com&quot;]

<b>Explanation:</b>
BrowserHistory browserHistory = new BrowserHistory(&quot;leetcode.com&quot;);
browserHistory.visit(&quot;google.com&quot;);       // You are in &quot;leetcode.com&quot;. Visit &quot;google.com&quot;
browserHistory.visit(&quot;facebook.com&quot;);     // You are in &quot;google.com&quot;. Visit &quot;facebook.com&quot;
browserHistory.visit(&quot;youtube.com&quot;);      // You are in &quot;facebook.com&quot;. Visit &quot;youtube.com&quot;
browserHistory.back(1);                   // You are in &quot;youtube.com&quot;, move back to &quot;facebook.com&quot; return &quot;facebook.com&quot;
browserHistory.back(1);                   // You are in &quot;facebook.com&quot;, move back to &quot;google.com&quot; return &quot;google.com&quot;
browserHistory.forward(1);                // You are in &quot;google.com&quot;, move forward to &quot;facebook.com&quot; return &quot;facebook.com&quot;
browserHistory.visit(&quot;linkedin.com&quot;);     // You are in &quot;facebook.com&quot;. Visit &quot;linkedin.com&quot;
browserHistory.forward(2);                // You are in &quot;linkedin.com&quot;, you cannot move forward any steps.
browserHistory.back(2);                   // You are in &quot;linkedin.com&quot;, move back two steps to &quot;facebook.com&quot; then to &quot;google.com&quot;. return &quot;google.com&quot;
browserHistory.back(7);                   // You are in &quot;google.com&quot;, you can move back only one step to &quot;leetcode.com&quot;. return &quot;leetcode.com&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= homepage.length &lt;= 20</code></li>
	<li><code>1 &lt;= url.length &lt;= 20</code></li>
	<li><code>1 &lt;= steps &lt;= 100</code></li>
	<li><code>homepage</code> and <code>url</code> consist of&nbsp; &#39;.&#39; or lower case English letters.</li>
	<li>At most <code>5000</code>&nbsp;calls will be made to <code>visit</code>, <code>back</code>, and <code>forward</code>.</li>
</ul>

</div>

## Tags
- Design (design)

## Companies
- Bloomberg - 14 (taggedByAdmin: false)
- Roblox - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Two Stacks vs. List
- Author: votrubac
- Creation Date: Sun Jun 07 2020 12:01:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 07 2020 13:21:59 GMT+0800 (Singapore Standard Time)

<p>
Two stacks (first solution) are natural fit for this problem. It\'s easier to implement during the contest. Using a single list (second solution) is faster, but it\'s a bit trickier to maintain the current position.

> One thing that is unclear is what should we do if we visit exactly the same url. In the real browser, that would not create a history item. But for this problem we are supposed to do. They should better clarify that int the description!

**Two Stacks**
The top of the first stack is the first element in the back history. And the top of the other stack - first element of the forward history. Move urls from one stack to another as you go back and forward.

```cpp
stack<string> h_back, h_forward;
string cur;
BrowserHistory(string homepage) { cur = homepage; }
void visit(string url) {
    h_forward = stack<string>();
    h_back.push(cur);
    cur = url;
}
string back(int steps) {
    while (--steps >= 0 && !h_back.empty()) {
        h_forward.push(cur);
        cur = h_back.top();
        h_back.pop();
    }
    return cur;
}
string forward(int steps) {
    while (--steps >= 0 && !h_forward.empty()) {
        h_back.push(cur);
        cur = h_forward.top();
        h_forward.pop();
    }
    return cur;
}
```
**List**
```cpp
vector<string> history;
int pos = 0;
BrowserHistory(string homepage) { history.push_back(homepage); }
void visit(string url) {
    ++pos;
    history.resize(pos);
    history.push_back(url);
}
string back(int steps) {
    pos = max(0, pos - steps);
    return history[pos];
}
string forward(int steps) {
    pos = min((int)history.size() - 1, pos + steps);
    return history[pos];
}
```
</p>


### Java use dual LinkedList
- Author: hobiter
- Creation Date: Sun Jun 07 2020 12:02:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 07 2020 12:11:33 GMT+0800 (Singapore Standard Time)

<p>
I cannot think of a better data structure than dual-linked list to solve this problem.
Looks like DDL is born to solve it.

```
class BrowserHistory {
    
    public class Node{
        String url;
        Node next, prev;
        public Node(String url) {
            this.url = url;
            next = null;
            prev = null;
        }
    }
    
    Node head, curr;
    public BrowserHistory(String homepage) {
        head = new Node(homepage);
        curr = head;
    }
    
    public void visit(String url) {
        Node node = new Node(url);
        curr.next = node;
        node.prev = curr;
        curr = node;
    }
    
    public String back(int steps) {
        while (curr.prev != null && steps-- > 0) {
            curr = curr.prev;
        }
        return curr.url;
    }
    
    public String forward(int steps) {
        while (curr.next != null && steps-- > 0) {
            curr = curr.next;
        }
        return curr.url;
    }
}
```
</p>


### Two Stacks, Pretty code.
- Author: interviewrecipes
- Creation Date: Sun Jun 07 2020 12:07:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 08 2020 13:21:08 GMT+0800 (Singapore Standard Time)

<p>
It\u2019s not difficult to realize that we need a stack so that `back` can be implemented. However, what to do for `forward`. Because you want the ability to visit all pages in the backward as well as the forward direction, we need to store the URLs whenever we go back. If you see the order in which we want to visit, you will quickly realize that we need two stacks. One to store history and another to store the next URLs in case we go back.
![image](https://assets.leetcode.com/users/interviewrecipes/image_1591684752.png)

Two special cases:
You must always have at least one element in the history stack which is the page that you are currently at.
But for the forward stack, this condition is not necessary.

I am sure the following code will make it even clearer.

**Thanks**
Please upvote if you find this helpful, it encourages me to write.

**More?**
Kindly visit my blog.

```
class BrowserHistory {
public:
    stack<string> history;
    stack<string> future;
	
    BrowserHistory(string homepage) {
        history.push(homepage);
        future = stack<string>();           // Reset the forward stack.
    }
    
    void visit(string url) {
        history.push(url);
        future = stack<string>();           // Reset the forward stack.
    }
    
    string back(int steps) {
        while(steps > 0 && history.size() > 1) { // Always keep at least one element in the stack. 
            future.push(history.top());
            history.pop();
            steps--;
        }
        return history.top();
    }
    
    string forward(int steps) {
        while(steps > 0 && future.size() > 0) {
            history.push(future.top());
            future.pop();
            steps--;
        }
        return history.top();
    }
};

/**
 * Your BrowserHistory object will be instantiated and called as such:
 * BrowserHistory* obj = new BrowserHistory(homepage);
 * obj->visit(url);
 * string param_2 = obj->back(steps);
 * string param_3 = obj->forward(steps);
 */
```


</p>


