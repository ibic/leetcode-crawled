---
title: "Design Parking System"
weight: 1448
#id: "design-parking-system"
---
## Description
<div class="description">
<p>Design a parking system for a parking lot. The parking lot has three kinds of parking spaces: big, medium, and small, with a fixed number of slots for each size.</p>

<p>Implement the <code>ParkingSystem</code> class:</p>

<ul>
	<li><code>ParkingSystem(int big, int medium, int small)</code> Initializes object of the <code>ParkingSystem</code> class. The number of slots for each parking space are given as part of the constructor.</li>
	<li><code>bool addCar(int carType)</code> Checks whether there is a parking space of <code>carType</code> for the car that wants to get into the parking lot. <code>carType</code> can be of three kinds: big, medium, or small, which are represented by <code>1</code>, <code>2</code>, and <code>3</code> respectively. <strong>A car can only park in a parking space of its </strong><code>carType</code>. If there is no space available, return <code>false</code>, else park the car in that size space and return <code>true</code>.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;ParkingSystem&quot;, &quot;addCar&quot;, &quot;addCar&quot;, &quot;addCar&quot;, &quot;addCar&quot;]
[[1, 1, 0], [1], [2], [3], [1]]
<strong>Output</strong>
[null, true, true, false, false]

<strong>Explanation</strong>
ParkingSystem parkingSystem = new ParkingSystem(1, 1, 0);
parkingSystem.addCar(1); // return true because there is 1 available slot for a big car
parkingSystem.addCar(2); // return true because there is 1 available slot for a medium car
parkingSystem.addCar(3); // return false because there is no available slot for a small car
parkingSystem.addCar(1); // return false because there is no available slot for a big car. It is already occupied.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= big, medium, small &lt;= 1000</code></li>
	<li><code>carType</code> is <code>1</code>, <code>2</code>, or <code>3</code></li>
	<li>At most <code>1000</code> calls will be made to <code>addCar</code></li>
</ul>

</div>

## Tags
- Design (design)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] 3 Lines
- Author: lee215
- Creation Date: Sun Oct 04 2020 00:27:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 00:27:25 GMT+0800 (Singapore Standard Time)

<p>
**Java:**
```java
    int[] count;
    public ParkingSystem(int big, int medium, int small) {
        count = new int[]{big, medium, small};
    }

    public boolean addCar(int carType) {
        return count[carType - 1]-- > 0;
    }
```

**C++:**
```cpp
    vector<int> count;
    ParkingSystem(int big, int medium, int small) {
        count = {big, medium, small};
    }

    bool addCar(int carType) {
        return count[carType - 1]-- > 0;
    }
```

**Python:**
```py
    def __init__(self, big, medium, small):
        self.A = [big, medium, small]

    def addCar(self, carType):
        self.A[carType - 1] -= 1
        return self.A[carType - 1] >= 0
```

</p>


### Self Explanatory Code
- Author: ngytlcdsalcp1
- Creation Date: Sun Oct 04 2020 00:15:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 00:15:33 GMT+0800 (Singapore Standard Time)

<p>
```
class ParkingSystem {

    int big, medium, small;
    public ParkingSystem(int big, int medium, int small) {
        this.big = big;
        this.medium = medium;
        this.small = small;
    }
    
    public boolean addCar(int carType) {
        if(carType == 1 && big == 0 || carType == 2 && medium == 0 || carType == 3 && small == 0)
            return false;
        if(carType == 1) big--;
        if(carType == 2) medium--;
        if(carType == 3) small--;
        return true;
    }
}

/**
 * Your ParkingSystem object will be instantiated and called as such:
 * ParkingSystem obj = new ParkingSystem(big, medium, small);
 * boolean param_1 = obj.addCar(carType);
 */
```
</p>


### [Java/Python 3] Use array to record remaining slots.
- Author: rock
- Creation Date: Sun Oct 04 2020 00:05:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 00:49:06 GMT+0800 (Singapore Standard Time)

<p>
```java
    private int[] count;
    public ParkingSystem(int big, int medium, int small) {
        count = new int[]{0, big, medium, small};
    }
    
    public boolean addCar(int carType) {
        return count[carType]-- > 0;
    }
```
```python
    def __init__(self, big: int, medium: int, small: int):
        self.count = [0, big, medium, small]
    def addCar(self, carType: int) -> bool:
        self.count[carType] -= 1
        return self.count[carType] >= 0
```
</p>


