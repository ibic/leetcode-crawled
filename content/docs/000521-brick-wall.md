---
title: "Brick Wall"
weight: 521
#id: "brick-wall"
---
## Description
<div class="description">
<p>There is a brick wall in front of you. The wall is rectangular and has several rows of bricks. The bricks have the same height but different width. You want to draw a vertical line from the <b>top</b> to the <b>bottom</b> and cross the <b>least</b> bricks.</p>

<p>The brick wall is represented by a list of rows. Each row is a list of integers representing the width of each brick in this row from left to right.</p>

<p>If your line go through the edge of a brick, then the brick is not considered as crossed. You need to find out how to draw the line to cross the least bricks and return the number of crossed bricks.</p>

<p><b>You cannot draw a line just along one of the two vertical edges of the wall, in which case the line will obviously cross no bricks. </b></p>

<p>&nbsp;</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b> [[1,2,2,1],
        [3,1,2],
        [1,3,2],
        [2,4],
        [3,1,2],
        [1,3,1,1]]

<b>Output:</b> 2

<b>Explanation:</b> 
<img src="https://assets.leetcode.com/uploads/2018/10/12/brick_wall.png" style="width: 100%; max-width: 350px" />
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>The width sum of bricks in different rows are the same and won&#39;t exceed INT_MAX.</li>
	<li>The number of bricks in each row is in range [1,10,000]. The height of wall is in range [1,10,000]. Total number of bricks of the wall won&#39;t exceed 20,000.</li>
</ol>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Oracle - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: true)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

In this approach, we consider the given wall as being made up of virtual bricks each of width 1. We traverse over the width of the wall only in terms of these virtual bricks.

Firstly, we need to determine the total number of virtual bricks. For this, we determine the width of the given wall by summing up the widths of the bricks in the first row. This width is stored in $$sum$$. Thus, we need to traverse over the widthe $$sum$$ times now in terms of 1 unit in each iteration.

We traverse over the virtual bricks in a column by column fashion. For keeping a track of the actual position at which we are currently in any row, we make use of a $$pos$$ array. $$pos[i]$$ refers to the index of the brick in the $$i^{th}$$ row, which is being treated as the virtual brick in the current column's traversal. Further, we maintain a $$count$$ variable to keep a track of the number of bricks cut if we draw a line down at the current position.

For every row considered during the column-by-column traversal, we need to check if we've hit an actual brick boundary. This is done by updating the brick's width after the traversal. If we don't hit an actual brick boundary, we need to increment $$count$$ to reflect that drawing a line at this point leads to cutting off 1 more brick. But, if we hit an actual brick boundary, we increment the value of $$pos[i]$$, with $$i$$ referring to the current row's index. But, now if we draw a line down through this boundary, we need not increment the $$count$$.

We repeat the same process for every column of width equal to a virtual brick, and determine the minimum value of $$count$$ obtained from all such traversals.

The following animation makes the process clearer:

<!--![Brick_Wall](../Figures/554_Brick_Wall_1.gif)-->
!?!../Documents/554_Brick_Wall1.json:866,487!?!



<iframe src="https://leetcode.com/playground/fruhzBr8/shared" frameBorder="0" name="fruhzBr8" width="100%" height="428"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n*m)$$. We traverse over the width($$n$$) of the wall $$m$$ times, where $$m$$ is the height of the wall.

* Space complexity : $$O(m)$$. $$pos$$ array of size $$m$$ is used, where $$m$$ is the height of the wall.

---
#### Approach #2 Better Brute Force[Time LImit Exceeded]

**Algorithm**

In this approach, instead of traversing over the columns in terms of 1 unit each time, we traverse over the columns in terms of the width of the smallest brick encountered while traversing the current column. Thus, we update $$pos$$ array and $$sums$$ appropriately depending on the width of the smallest brick. Rest of the process remains the same as the first approach.

The optimization achieved can be viewed by considering this example:

```

[[100, 50, 50],
[50, 100],
[150]]

```

In this case, we directly jump over the columns in terms of widths of 50 units each time, rather than making traversals over widths incrementing by 1 unit each time.


<iframe src="https://leetcode.com/playground/e58vTtJR/shared" frameBorder="0" name="e58vTtJR" width="100%" height="496"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n*m)$$. In worst case, we traverse over the length($$n$$) of the wall $$m$$ times, where $$m$$ is the height of the wall.

* Space complexity : $$O(m)$$. $$pos$$ array of size $$m$$ is used, where $$m$$ is the height of the wall.

---

#### Approach #3 Using HashMap [Accepted]

**Algorithm**

In this approach, we make use of a HashMap $$map$$ which is used to store entries in the form: $$(sum, count)$$. Here, $$sum$$ refers to the cumulative sum of the bricks' widths encountered in the current row, and $$count$$ refers to the number of times the corresponding sum is obtained. Thus, $$sum$$ in a way, represents the positions of the bricks's boundaries relative to the leftmost boundary.

Let's look at the process first. We traverse over every row of the given wall. For every brick considered, we find the $$sum$$ corresponding to the sum of the bricks' widths encountered so far in the current row. If this $$sum$$'s entry doesn't exist in the $$map$$, we create a corresponding entry with an initial $$count$$ of 1. If the $$sum$$ already exists as a key, we increment its corresponding $$count$$ value.

This is done based on the following observation. We will never obtain the same value of $$sum$$ twice while traversing over a particular row. Thus, if the $$sum$$ value is repeated while traversing over the rows, it means some row's brick boundary coincides with some previous row's brick boundary. This fact is accounted for by incrementing the corresponding $$count$$ value.

But, for every row, we consider the sum only upto the second last brick, since the last boundary isn't a valid boundary for the solution.

At the end, we can obtain the maximum $$count$$ value to determine the minimum number of bricks that need to be cut to draw a vetical line through them.

The following animation makes the process clear:

<!-- ![Brick_Wall](../Figures/554_Brick_Wall_2.gif) -->
!?!../Documents/554_Brick_Wall2.json:866,487!?!

<iframe src="https://leetcode.com/playground/miHSNYBN/shared" frameBorder="0" name="miHSNYBN" width="100%" height="377"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n)$$. We traverse over the complete bricks only once. $$n$$ is the total number of bricks in a wall.

* Space complexity : $$O(m)$$. $$map$$ will contain atmost $$m$$ entries, where $$m$$ refers to the width of the wall.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Clear Python Solution
- Author: szt
- Creation Date: Sun Apr 09 2017 11:07:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 09 2017 11:07:44 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def leastBricks(self, wall):
        """
        :type wall: List[List[int]]
        :rtype: int
        """
        d = collections.defaultdict(int)
        for line in wall:
            i = 0
            for brick in line[:-1]:
                i += brick
                d[i] += 1
        # print len(wall), d
        return len(wall)-max(d.values()+[0])
```
</p>


### C++ 6 lines (hash map)
- Author: votrubac
- Creation Date: Sun Apr 09 2017 11:02:02 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 14 2018 05:50:56 GMT+0800 (Singapore Standard Time)

<p>
For each potential cut position - which is at the edge of any brick, I am counting the number of brick edges for all rows. Note that we need to use hash map to only track potential (not all) cuts. If bricks are very wide, you'll get MLE if you store all cut positions.
```
int leastBricks(vector<vector<int>>& wall) {
    unordered_map<int, int> edges;
    auto min_bricks = wall.size();
    for (auto row : wall)
        for (auto i = 0, width = 0; i < row.size() - 1; ++i) // skip last brick
            min_bricks = min(min_bricks, wall.size() - (++edges[width += row[i]]));
    return min_bricks;
}
```
</p>


### Java Map solution
- Author: tankztc
- Creation Date: Sun Jun 10 2018 01:36:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 23:29:29 GMT+0800 (Singapore Standard Time)

<p>
The idea is straightforward, since each brick length is positive, we can record all the edge index in the wall and figure out which edge index is the most common. We cut through that edge index, it will cross ```number of rows - most common edge count``` rows

```
class Solution {
    public int leastBricks(List<List<Integer>> wall) {
        Map<Integer, Integer> map = new HashMap();
        
        int count = 0;
        for (List<Integer> row : wall) {
            int sum = 0;
            for (int i = 0; i < row.size() - 1; i++) {
                sum += row.get(i);
                map.put(sum, map.getOrDefault(sum, 0) + 1);
                count = Math.max(count, map.get(sum));
            }
        }
        
        return wall.size() - count;
    }
}
```
</p>


