---
title: "Transpose File"
weight: 1602
#id: "transpose-file"
---
## Description
<div class="description">
<p>Given a text file <code>file.txt</code>, transpose its content.</p>

<p>You may assume that each row has the same number of columns and each field is separated by the <code>&#39; &#39;</code> character.</p>

<p><strong>Example:</strong></p>

<p>If <code>file.txt</code> has the following content:</p>

<pre>
name age
alice 21
ryan 30
</pre>

<p>Output the following:</p>

<pre>
name alice ryan
age 21 30
</pre>

<p>&nbsp;</p>

</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (bash)
```bash
# Read from the file file.txt and print its transposed content to stdout.
input=file.txt
declare -a cols

nrow=0
i=0
while read -r line; do
	for item in $line; do
		cols[$i]="$item"
		let i=i+1
	done
	let nrow=nrow+1
done < $input

if [ $nrow -eq 0 ]; then exit; fi

let ncol=i/nrow
j=0
k=0
while [ $j -lt $ncol ]; do
	k=$j
    line=""
	while [ $k -lt $i ]; do
        if [ -z "$line" ]
        then
            line="${cols[$k]}"
        else
            line="$line ${cols[$k]}"
        fi
		let k=k+ncol
	done
	echo "$line"
	let j=j+1
done


```

## Top Discussions
### AC solution using awk and statement just like C.
- Author: illuz
- Creation Date: Thu Mar 26 2015 16:51:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 08:44:59 GMT+0800 (Singapore Standard Time)

<p>
Just feel free to use `for` and `if`.  
You can append string easily, for example, `s = s a` to append `a` with `s`. 


    awk '
    {
        for (i = 1; i <= NF; i++) {
            if(NR == 1) {
                s[i] = $i;
            } else {
                s[i] = s[i] " " $i;
            }
        }
    }
    END {
        for (i = 1; s[i] != ""; i++) {
            print s[i];
        }
    }' file.txt
</p>


### Solution using AWK with explanations
- Author: XSLC
- Creation Date: Tue Jan 09 2018 13:08:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 20 2019 15:24:08 GMT+0800 (Singapore Standard Time)

<p>
I would definitely recommend this tutorial: http://www.grymoire.com/Unix/Awk.html if you'd like to know more about GAWK/NAWK. Similar to `sed`, `awk` is used to perform complex editing tasks to streams of text. The main difference is that `awk` is more suitable for larger editing tasks (those that may need some programming language features).

You may have already seen `awk` solutions similar to the one presented below:

```bash
awk '
{
    for (i = 1; i <= NF; i++) {
        if (FNR == 1) {
            t[i] = $i;
        } else {
            t[i] = t[i] " " $i
        }
    }
}
END {
    for (i = 1; t[i] != ""; i++) {
        print t[i]
    }
}
' file.txt
```

Here're some quick notes to help you understand:

1. The code block with an "END" prefix is only executed after the last line is read; similarly, a code block with a "BEGIN" prefix will be executed before any line reads.

2. AWK is line-based: the main code block (the code block without prefix) processes one line of input at a time.

3. `NR`: a variable indicating the number of records (i.e. current line number) that's accumulated across multiple files read. `FNR` is similar to `NR`, but is reset for each file read. Since we only need to deal with one file in this question, either is fine to use.

4. `NF`: a variable indicating the number of fields (i.e. number of "columns") on an input line.

5. `$i`: the i-th field of the input line.

6. `t[]`: an array for saving the transposed table. More on awk arrays here: http://www.grymoire.com/Unix/Awk.html#uh-22
</p>


### Simple BASH solution that OJ hates
- Author: slicer
- Creation Date: Sat Apr 11 2015 03:51:19 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 12:59:29 GMT+0800 (Singapore Standard Time)

<p>
My solution in BASH. It works fine on my computer and I think it's conceptually straightforward. OJ complains exceeding memory.

    ncol=`head -n1 file.txt | wc -w`
    
    for i in `seq 1 $ncol`
    do
        echo `cut -d' ' -f$i file.txt`
    done
</p>


