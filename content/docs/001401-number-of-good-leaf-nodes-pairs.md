---
title: "Number of Good Leaf Nodes Pairs"
weight: 1401
#id: "number-of-good-leaf-nodes-pairs"
---
## Description
<div class="description">
<p>Given the <code>root</code> of a binary tree and an integer <code>distance</code>. A pair of two different <strong>leaf</strong> nodes of a binary tree is said to be good if the length of <strong>the shortest path</strong> between them is less than or equal to <code>distance</code>.</p>

<p>Return <em>the number of good leaf node pairs</em> in the tree.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/09/e1.jpg" style="width: 321px; height: 321px;" />
<pre>
<strong>Input:</strong> root = [1,2,3,null,4], distance = 3
<strong>Output:</strong> 1
<strong>Explanation:</strong> The leaf nodes of the tree are 3 and 4 and the length of the shortest path between them is 3. This is the only good pair.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/07/09/e2.jpg" style="width: 441px; height: 321px;" />
<pre>
<strong>Input:</strong> root = [1,2,3,4,5,6,7], distance = 3
<strong>Output:</strong> 2
<strong>Explanation:</strong> The good pairs are [4,5] and [6,7] with shortest path = 2. The pair [4,6] is not good because the length of ther shortest path between them is 4.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [7,1,4,6,null,5,3,null,null,null,null,null,2], distance = 3
<strong>Output:</strong> 1
<strong>Explanation:</strong> The only good pair is [2,5].
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> root = [100], distance = 1
<strong>Output:</strong> 0
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> root = [1,1,1], distance = 2
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the&nbsp;<code>tree</code>&nbsp;is in the range&nbsp;<code>[1, 2^10].</code></li>
	<li>Each node&#39;s value is between&nbsp;<code>[1, 100]</code>.</li>
	<li><code>1 &lt;= distance &lt;= 10</code></li>
</ul>
</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] DFS Solution with a Twist ,100% Faster [Explained]
- Author: Shradha1994
- Creation Date: Sun Jul 26 2020 14:08:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 16:19:29 GMT+0800 (Singapore Standard Time)

<p>
Steps -
We have to do normal `post order tree traversal`.
The trick is to keep track of number of leaf nodes with a particular distance. The arrays are used for this purpose. 
For this we maintain an array of size = max distance. 
Example -
![image](https://assets.leetcode.com/users/images/0c6ac619-eb11-4fa0-8ab4-c6cbf20579b5_1595744736.265242.png)


In above example , assume maximum distance = 4. So we maintain an array of size 4.
1. For root node 1, 
left = `[ 0,0,1,0,0]`
right = `[0,1,0,0,0]`
Here, left[2] = 1, which denotes that there is one leaf node with distance 2 in left subtree of root node 1.
right[1] = 1, which denotes that there is one leaf node with distance 1 in right subtree of root node 1.

In this way, we have to recursively, calculate the left and right subtree of every root node.

 2. Once we have both left and right arrays for a particular root, we have to just calculate total number of good node pairs formed using `result += left[l]*right[r];` 

3. Before we bactrack to parent, we have to return the distance for parents by adding up left and right subtrees of current node. Note that we are doing -  `res[i+1] = left[i]+right[i];`
The intution is that, if a leaf node is at distance i from current node, it would be at distance i+1 from its parent. Hence, will building the res array, we are adding sum in i+1 th position and return to parent.
```
class Solution {
    int result = 0;
    public int countPairs(TreeNode root, int distance) {
        dfs(root,distance);
        return result;
    }
    
    int[] dfs(TreeNode root,int distance){
        if(root == null)
            return new int[distance+1];
        if(root.left == null && root.right == null){
            int res[] = new int[distance+1];
            res[1]++;
            return res;
        }
        int[] left = dfs(root.left,distance);
        int[] right = dfs(root.right,distance);
        for(int l=1;l<left.length;l++){
            for(int r = distance-1;r>=0;r--){
                if(l+r <=distance)
                result += left[l]*right[r];
            }
        }
        int res[] = new int[distance+1];
        //shift by 1
        for(int i=res.length-2;i>=1;i--){
            res[i+1] = left[i]+right[i];
        }
        
        return res;
    }
}
```

*Feel free to ask question in comments, do upvote if you understood the solution*
</p>


### [Java] Detailed Explanation - Post-Order - Cache in Array
- Author: frankkkkk
- Creation Date: Sun Jul 26 2020 12:01:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 26 2020 12:36:18 GMT+0800 (Singapore Standard Time)

<p>
**Key Notes:**
- Store distances of all leaf nodes, at each non-leaf node, find those leaf nodes satisfying the condition, and accumulate into res.
- Distance is from 0 - 10. We could use a size 11 array to count frequency, and **ignore** the leaf node with distance larger than 10.
- What helper function returns: the number of leaf nodes for each distance. Let\'s name the return array is arr for the current node: **arr[i]: the number of leaf nodes, i: distance to current node is i.**

```java
private int res;
    
public int countPairs(TreeNode root, int distance) {

	res = 0;
	helper(root, distance);
	return res;
}

private int[] helper(TreeNode node, int distance) {

	if (node == null) return new int[11];

	int[] left = helper(node.left, distance);
	int[] right = helper(node.right, distance);

	int[] A = new int[11];

	// node is leaf node, no child, just return
	if (node.left == null && node.right == null) {
		A[1] = 1;
		return A;
	}

	// find all nodes satisfying distance
	for (int i = 0; i <= 10; ++i) {
		for (int j = 0; j <= 10; ++j) {
			if (i + j <= distance) res += (left[i] * right[j]);
		}
	}

	// increment all by 1, ignore the node distance larger than 10
	for (int i = 0; i <= 9; ++i) {
		A[i + 1] += left[i];
		A[i + 1] += right[i];
	}

	return A;
}
```
</p>


### [Python] Postorder Traversal
- Author: smolarek9
- Creation Date: Sun Jul 26 2020 12:01:18 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 27 2020 09:05:14 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**
My idea was to traverse from bottom to top (postorder) and keep track of the distance of the leaf nodes to each node. Once those leaf nodes meet a Lowest Common Ancestor, we can immediately check whether they are good pairs by checking if `dist_from_leaf1_to_LCA + dist_from_leaf2_to_LCA <= distance`. If so, we can increment the count by 1. 

This works because for any leaf node pair, the shortest distance between those two nodes is always `dist_from_leaf1_to_LCA + dist_from_leaf2_to_LCA`. Each node will "pass up" the information of distances from all leaf nodes from the left to the current node and distances from all leaf nodes from the right to the current node in a list. 

We can ensure that each node has leaf nodes on both sides by checking if the list returned from both sides are non-empty. Once a node has the distance information from both sides, we can check how many pairs are good pairs by calculating the distance between every pair of nodes. `count += sum(l + r <= distance for l in left for r in right)`

**Code**

```
class Solution:
    def countPairs(self, root: TreeNode, distance: int) -> int:
        count = 0
        def dfs(node):
            nonlocal count
            if not node:
                return []
            if not node.left and not node.right:
                return [1]
            left = dfs(node.left)
            right = dfs(node.right)
            count += sum(l + r <= distance for l in left for r in right)
            return [n + 1 for n in left + right if n + 1 < distance]
        dfs(root)
        return count
```

**Edit**

- Added pruning `return [n + 1 for n in left + right]` --> `return [n + 1 for n in left + right if n + 1 < distance]` Thanks to @readonly_true
- `if left and right: count += sum(l + r <= distance for l in left for r in right)` --> removed `if left and right` since it won\'t do the summation if either of left and right is empty
</p>


