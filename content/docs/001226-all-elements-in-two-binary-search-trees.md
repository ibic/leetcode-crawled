---
title: "All Elements in Two Binary Search Trees"
weight: 1226
#id: "all-elements-in-two-binary-search-trees"
---
## Description
<div class="description">
<p>Given two binary search trees <code>root1</code> and <code>root2</code>.</p>

<p>Return a list containing <em>all the integers</em> from <em>both trees</em> sorted in <strong>ascending</strong> order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/12/18/q2-e1.png" style="width: 457px; height: 207px;" />
<pre>
<strong>Input:</strong> root1 = [2,1,4], root2 = [1,0,3]
<strong>Output:</strong> [0,1,1,2,3,4]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root1 = [0,-10,10], root2 = [5,1,7,0,2]
<strong>Output:</strong> [-10,0,0,1,2,5,7,10]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root1 = [], root2 = [5,1,7,0,2]
<strong>Output:</strong> [0,1,2,5,7]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> root1 = [0,-10,10], root2 = []
<strong>Output:</strong> [-10,0,10]
</pre>

<p><strong>Example 5:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/12/18/q2-e5-.png" style="width: 352px; height: 197px;" />
<pre>
<strong>Input:</strong> root1 = [1,null,8], root2 = [8,1]
<strong>Output:</strong> [1,1,8,8]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>Each tree has at most <code>5000</code> nodes.</li>
	<li>Each node&#39;s value is between <code>[-10^5, 10^5]</code>.</li>
</ul>

</div>

## Tags
- Sort (sort)
- Tree (tree)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Prerequisites

To solve this problem we will use recursive and iterative inorder traversals.
Here are prerequisites you might want to check:

1. There are three DFS ways to traverse the tree: preorder, postorder and inorder.
Please check two minutes picture explanation, if you don't remember them quite well:
[here is Python version](https://leetcode.com/problems/binary-tree-inorder-traversal/discuss/283746/all-dfs-traversals-preorder-inorder-postorder-in-python-in-1-line) 
and 
[here is Java version](https://leetcode.com/problems/binary-tree-inorder-traversal/discuss/328601/all-dfs-traversals-preorder-postorder-inorder-in-java-in-5-lines).

2. > Inorder traversal of BST is an array sorted in the ascending order.

3. To compute inorder traversal follow the direction `Left -> Node -> Right`.

<iframe src="https://leetcode.com/playground/cW39g7yk/shared" frameBorder="0" width="100%" height="174" name="cW39g7yk"></iframe>

![traversal](../Figures/1305/inorder.png)
<br />
<br />


---
#### Approach 1: Recursive Inorder Traversal + Sort, Linearithmic Time.

Let's start from the shortest possible solution: 

- Implement recursive inorder traversal, 1 line in Python, 5 lines in Java.

- Compute inorder traversal of each tree.

- Merge both lists and then sort the result.

![traversal](../Figures/1305/recursive_inorder.png)

This solution takes one minute to write, but the time complexity is
linearithmic. 

**Implementation**

<iframe src="https://leetcode.com/playground/kbooPurH/shared" frameBorder="0" width="100%" height="480" name="kbooPurH"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}((N + M)\log(M + N))$$, 
where $$M$$ and $$N$$ are node numbers. To build inorder traversals takes 
$$\mathcal{O}(N + M)$$, to merge and sort the resulting lists -
$$\mathcal{O}((N + M)\log(M + N))$$.
    
* Space complexity: $$\mathcal{O}(N + M)$$ to keep the output. 
<br />
<br />


---
#### Approach 2: Iterative Inorder Traversal, One Pass, Linear Time.

**Intuition**

Now let's optimise the first approach. 

First, since both inorder traversals are already sorted, 
[one could merge them into one sorted list in linear time](https://leetcode.com/articles/merged-two-sorted-lists/).
Though it's still two passes solution: first to build two inorder traversals 
and then to merge them.

More elegant way here is to build iteratively inorder traversals for both trees in parallel,
and at each step update the output list by the smallest value between both trees.
That will be one pass solution. Here is how it works:

![traversal](../Figures/1305/iterative.png)

**Algorithm**

- Do iterative inorder traversal of both trees in parallel.

    - At each step add the smallest available value in the output.
     
- Return output list.

**Implementation**

!?!../Documents/1305_LIS.json:1000,530!?!

<iframe src="https://leetcode.com/playground/tKhveJc7/shared" frameBorder="0" width="100%" height="500" name="tKhveJc7"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N + M)$$, where $$M$$ and $$N$$ are node numbers. 
It's one-pass approach along each tree. 
    
* Space complexity: $$\mathcal{O}(N + M)$$ to keep the output and both stacks. 
<br />
<br />


---
#### Further Reading 

Here we implemented _recursive_ and _iterative_ inorder traversals.
There is also _Morris_ inorder traversal, which is used for the problems 
which require constant space solution. 
Here is detailed comparison and implementation of all three 
inorder traversals: 
[Recover BST](https://leetcode.com/articles/recover-binary-search-tree/). 
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ One-Pass Traversal
- Author: votrubac
- Creation Date: Sun Dec 29 2019 14:10:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 05 2020 09:42:29 GMT+0800 (Singapore Standard Time)

<p>
**Update:** check below the generic solution for K binary search trees.

We are doing in-order traversal independently for two trees. When it\'s time to \'visit\' a node on the top of the stack, we are visiting the smallest of two.
> Nodes are visited in the ascending order during the in-order traversal.
```CPP
void pushLeft(stack<TreeNode*> &s, TreeNode* n) {
    while (n != nullptr) 
        s.push(exchange(n, n->left));
}
vector<int> getAllElements(TreeNode* root1, TreeNode* root2) {
    vector<int> res;
    stack<TreeNode*> s1, s2;
    pushLeft(s1, root1);
    pushLeft(s2, root2);
    while (!s1.empty() || !s2.empty()) {
        stack<TreeNode*> &s = s1.empty() ? s2 : s2.empty() ? s1 : 
            s1.top()->val < s2.top()->val ? s1 : s2;
        auto n = s.top(); s.pop();
        res.push_back(n->val);
        pushLeft(s, n->right);
    }
    return res;
}
```
**Complexity Analysis**
- Time: O(n), where n is the total number of nodes in both trees.
- Memory: O(n) for the stack - in the worst case, we may need to store the entire tree there.
#### Generic Solution
A generic version of this problem would be to combine elements from K binary search trees. We can solve it by  applying two existing solutions:
- [173. Binary Search Tree Iterator](https://leetcode.com/problems/binary-search-tree-iterator/)
- [23. Merge k Sorted Lists](https://leetcode.com/problems/merge-k-sorted-lists/)

Since we need a `peek` method, we can also use [284. Peeking Iterator](https://leetcode.com/problems/peeking-iterator/), as suggested by [@StefanPochmann](https://leetcode.com/stefanpochmann/) to keep the iterator interface prestine. That way, you can plug a different BST iterator implementation, e.g. Morris traversal for O(1) memory.

Below is my take on this combined solution. I am not doing the proper memory management here to not to obfuscate the code (and it\'s specific to C++).
```CPP
class BSTIterator {
  stack<TreeNode*> s;
  void pushLeft(TreeNode* n) {
    while (n != nullptr) s.push(exchange(n, n->left));
  }
public:
  BSTIterator(TreeNode* root) { pushLeft(root); }
  bool hasNext() const { return !s.empty(); }
  int next() {
    auto n = s.top(); s.pop();
    pushLeft(n->right);
    return n->val;
  }
};
class PeekingIterator : public BSTIterator {
  bool peeked = false;
  int last_val = 0;
public:
  PeekingIterator(TreeNode* root) : BSTIterator(root) { }
  int peek() {
    if (peeked) return last_val;
    peeked = true;
    return last_val = BSTIterator::next();
  }
  int next() {
    if (peeked) {
      peeked = false;
      return last_val;
    }
    return BSTIterator::next();
  }
  bool hasNext() const {
    return peeked || BSTIterator::hasNext();
  }
};
vector<int> mergeKTrees(vector<TreeNode*> trees) {
  vector<int> res;
  priority_queue <pair<int, PeekingIterator*>,
    vector<pair<int, PeekingIterator*>>, greater<pair<int, PeekingIterator*>> > q;
  for (auto t : trees) {
    auto it = new PeekingIterator(t);
    if (it->hasNext()) q.push({ it->peek(), it });
  }
  while (!q.empty()) {
    auto p = q.top(); q.pop();
    res.push_back(p.second->next());
    if (p.second->hasNext())
      q.push({ p.second->peek(), p.second });
  }
  return res;
}
vector<int> getAllElements(TreeNode* root1, TreeNode* root2) {
  return mergeKTrees({ root1, root2 });
}
```
**Complexity Analysis**
- Time: O(n log k), where n is the total number of nodes in all trees, and k is the number of trees.
- Memory: O(n + k) for the stack and priority queue.
</p>


### Short O(n) Python
- Author: StefanPochmann
- Creation Date: Sun Dec 29 2019 20:32:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 29 2019 20:36:44 GMT+0800 (Singapore Standard Time)

<p>
Before you scream "But sorting isn\'t linear!", know that it\'s [&rarr;Timsort](https://en.wikipedia.org/wiki/Timsort), which identifies the two already sorted runs as such and simply merges them. And since it\'s done in C, it\'s likely *faster* than if I tried merging myself in Python.
```
def getAllElements(self, root1, root2):
    values = []
    def collect(root):
        if root:
            collect(root.left)
            values.append(root.val)
            collect(root.right)
    collect(root1)
    collect(root2)
    return sorted(values)
```
</p>


