---
title: "Verify Preorder Sequence in Binary Search Tree"
weight: 239
#id: "verify-preorder-sequence-in-binary-search-tree"
---
## Description
<div class="description">
<p>Given an array of numbers, verify whether it is the correct preorder traversal sequence of a binary search tree.</p>

<p>You may assume each number in the sequence is unique.</p>

<p>Consider the following&nbsp;binary search tree:&nbsp;</p>

<pre>
     5
    / \
   2   6
  / \
 1   3</pre>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> [5,2,6,1,3]
<strong>Output:</strong> false</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> [5,2,1,3,6]
<strong>Output:</strong> true</pre>

<p><b>Follow up:</b><br />
Could you do it using only constant space complexity?</p>

</div>

## Tags
- Stack (stack)
- Tree (tree)

## Companies
- VMware - 2 (taggedByAdmin: false)
- Mathworks - 5 (taggedByAdmin: false)
- Walmart Labs - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Zenefits - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(n) and O(1) extra space
- Author: StefanPochmann
- Creation Date: Thu Aug 13 2015 06:21:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 12:29:01 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1**

Kinda simulate the traversal, keeping a stack of nodes (just their values) of which we're still in the left subtree. If the next number is smaller than the last stack value, then we're still in the left subtree of all stack nodes, so just push the new one onto the stack. But before that, pop all smaller ancestor values, as we must now be in their right subtrees (or even further, in the right subtree of an ancestor). Also, use the popped values as a lower bound, since being in their right subtree means we must never come across a smaller number anymore.

    public boolean verifyPreorder(int[] preorder) {
        int low = Integer.MIN_VALUE;
        Stack<Integer> path = new Stack();
        for (int p : preorder) {
            if (p < low)
                return false;
            while (!path.empty() && p > path.peek())
                low = path.pop();
            path.push(p);
        }
        return true;
    }

**Solution 2** ...  **O(1) extra space**

Same as above, but abusing the given array for the stack.

    public boolean verifyPreorder(int[] preorder) {
        int low = Integer.MIN_VALUE, i = -1;
        for (int p : preorder) {
            if (p < low)
                return false;
            while (i >= 0 && p > preorder[i])
                low = preorder[i--];
            preorder[++i] = p;
        }
        return true;
    }

**Solution 3** ...  **Python**

Same as solution 1, just in Python.

    def verifyPreorder(self, preorder):
        stack = []
        low = float('-inf')
        for p in preorder:
            if p < low:
                return False
            while stack and p > stack[-1]:
                low = stack.pop()
            stack.append(p)
        return True
</p>


### C++ easy to understand solution with thought process and detailed explanation
- Author: xz2210
- Creation Date: Tue Mar 29 2016 09:03:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 12:32:47 GMT+0800 (Singapore Standard Time)

<p>
**THOUGHT**: We first look at the property of preorder traversal: we print left child\u2019s value of current node all the way until we reached a leaf node (you will see numbers decreasing), then we start printing the value of a node (let it be rc) which is the right child of one of the nodes (let it be node p) we already traversed. When do you know it's a right child node's value? It's when you see a value greater than the last one. Also,till here we know, all the nodes in p\u2019s left subtree have been read in the serialized array, and this property is maintained: 

    left subtree \u2018s value < p \u2019s value < rc\u2019s value

Since all the nodes whose value is smaller than p are already read, all the nodes\u2019 value to be read after should have greater value than p\u2019s value, so p\u2019s value becomes the **lower bound** for any upcoming node.

    p \u2019s value < upcoming value in array

Otherwise, it\u2019s not valid. **So the key here is to find the lower bound for upcoming nodes, which equals to find p.** 

To translate this into code: looking for the trend of numbers, if it\u2019s decreasing, it\u2019s still traversing the left child node all the way down, we push the value into stack. When we read a value greater than the last one, we know the current value belongs to a right node (let it be rc: right child) of one of the previous nodes (let it be p) we pushed to stack, in other words, p is a parent node of the current node rc. Due to the property of preorder traversal, p\u2019s value is pushed to stack before its left subtree nodes, so to find the parent node, we pop all the nodes in its left subtree, and the last popped node whose value is smaller than rc is rc\u2019s parent p, whose value becomes the lower bound. Then we keep reading the serialized array, in any case we see any value not greater than the lower bound, we return false. Lower bound is updated whenever we read a right child node\u2019s value.

    class Solution {
    public:
        bool verifyPreorder(vector<int>& preorder) {
            stack<int> stk;
            int lower_bound = INT_MIN;
            for(int i = 0; i < preorder.size(); i++){
                if(stk.empty() || preorder[i] < preorder[i - 1]){
                    if(preorder[i] <= lower_bound) return false;
                    stk.push(preorder[i]);
                }else{
                    while(!stk.empty() && stk.top() < preorder[i]){
                        lower_bound = stk.top();
                        stk.pop();
                    }
                    stk.push(preorder[i]);
                }
            }
            
            return true;
        }
    };


Using this [image][1] as an example:

    Push 50
    Push 17 
    Push 9
    (read 14, 14 > 9)
    Pop 9 (lower bound = 9)
    Push 14
    Push 12
    (read 23, 23 > 12)
    Pop 12
    Pop 14
    Pop 17 (lower bound = 17)
    Push 23
    (read 76, 76 > 23)
    Pop 23
    Pop 50 (lowerbound = 50)
    Push 76
    Push 54
    (read 72, 72 > 54)
    Pop 54 (lower bound = 54)
    Push 72
    Push 67


  [1]: https://en.wikipedia.org/wiki/Self-balancing_binary_search_tree#/media/File:Unbalanced_binary_tree.svg
</p>


### AC Python O(n) time O(1) extra space
- Author: dietpepsi
- Creation Date: Thu Oct 22 2015 00:26:36 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 27 2018 12:15:09 GMT+0800 (Singapore Standard Time)

<p>
 A easy solution is O(n) time and O(n) space using a stack

    def verifyPreorder(self, preorder):
        stack = []
        lower = -1 << 31
        for x in preorder:
            if x < lower:
                return False
            while stack and x > stack[-1]:
                lower = stack.pop()
            stack.append(x)
        return True


    # 59 / 59 test cases passed.
    # Status: Accepted
    # Runtime: 100 ms
    # 95.31%

Then we realize that the preorder array can be reused as the stack thus achieve O(1) extra space, since the scanned items of preorder array is always more than or equal to the length of the stack.

    def verifyPreorder(self, preorder):
        # stack = preorder[:i], reuse preorder as stack
        lower = -1 << 31
        i = 0
        for x in preorder:
            if x < lower:
                return False
            while i > 0 and x > preorder[i - 1]:
                lower = preorder[i - 1]
                i -= 1
            preorder[i] = x
            i += 1
        return True


    # 59 / 59 test cases passed.
    # Status: Accepted
    # Runtime: 112 ms
    # 70.31%
</p>


