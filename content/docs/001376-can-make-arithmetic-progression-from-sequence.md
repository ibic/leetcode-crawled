---
title: "Can Make Arithmetic Progression From Sequence"
weight: 1376
#id: "can-make-arithmetic-progression-from-sequence"
---
## Description
<div class="description">
<p>Given an array of numbers <code>arr</code>.&nbsp;A sequence of numbers is called an arithmetic progression&nbsp;if the difference between any two consecutive elements is the same.</p>

<p>Return <code>true</code>&nbsp;if the array can be rearranged to form an arithmetic progression, otherwise, return <code>false</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [3,5,1]
<strong>Output:</strong> true
<strong>Explanation: </strong>We can reorder the elements as [1,3,5] or [5,3,1] with differences 2 and -2 respectively, between each consecutive elements.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,4]
<strong>Output:</strong> false
<strong>Explanation: </strong>There is no way to reorder the elements to obtain an arithmetic progression.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= arr.length &lt;= 1000</code></li>
	<li><code>-10^6 &lt;= arr[i] &lt;= 10^6</code></li>
</ul>

</div>

## Tags
- Array (array)
- Sort (sort)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Two Solution | Brute Force O(nlogn) | Optimize O(n) | AP Explained
- Author: rajmc
- Creation Date: Sun Jul 05 2020 12:12:39 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 10 2020 23:55:25 GMT+0800 (Singapore Standard Time)

<p>
**Appraoch 1:** **Brute Force**
Sort and and check difference in all pairs if not equal or not.

Can be further optimize?
**Appraoch 2:** **Optimized**

As per wikipedia https://en.wikipedia.org/wiki/Arithmetic_progression
AP = a, a + d, a + 2d, . . . , a + (n - 1)d
T1 = a - first term of AP also min value  in AP for d > 0
Tn = a + (n - 1)d - nth term of AP also the max value in AP for d > 0

Tn - T1 = (n - 1)d => d =  (max(arr) - min(arr)) / (n - 1) common difference of our AP.

Now we will  add  all elements of our array in HashSet why ? to fast lookup.

Now we know first term and the common difference of our AP we can generate n-terms and then check if any term is not in our set return false else continue.

If we found all terms return true.

********
**Approach 1:** Code

```
class Solution {
    public boolean canMakeArithmeticProgression(int[] arr) {
        Arrays.sort(arr);
        int d = arr[1] - arr[0];
        for(int i = 0; i < arr.length - 1; i++) 
            if(arr[i + 1] - arr[i] != d)
                return false;
        return true;
    }
}
```

`TC - O(nlogn)`
`SC - (1)`

********

**Approach 2:** Code

```
class Solution {
    public boolean canMakeArithmeticProgression(int[] arr) {
        int n = arr.length;
        int a = Integer.MAX_VALUE; // first term min of array
        int max = Integer.MIN_VALUE; // nth term max of array
        Set<Integer> set = new HashSet();

        for (int num : arr) {
            a = Math.min(num, a);
            max = Math.max(num, max);
            set.add(num);
        }

        int d = max - a; //(n - 1) times common difference of AP
        if (d % (n - 1) != 0) // check Tn = a + (n - 1)d or not
            return false;

        d /= n - 1; // common difference of AP
        int i = 0;
        while (i < n) {
            if (!set.contains(a))
                return false;
            a += d;
            i++;
        }

        return true;
    }
}
```

`TC - O(n)`
`SC - (n)`

********

If you have any question ask in comment. If it helps **upvote**.
</p>


### Clean Python 3, O(N)
- Author: lenchen1112
- Creation Date: Sun Jul 05 2020 12:11:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 05 2020 12:34:36 GMT+0800 (Singapore Standard Time)

<p>
Find gap by dividing distance between minimun and maximun number.
Then check all numbers are evenly distributed.
Time: `O(N)`
Space: `O(N)`
```
class Solution:
    def canMakeArithmeticProgression(self, arr: List[int]) -> bool:
        m = min(arr)
        gap = (max(arr) - m) / (len(arr) - 1)
        if gap == 0: return True
        s = set(num - m for num in arr)
        return len(s) == len(arr) and all(diff % gap == 0 for diff in s)
```
--
Refer to [coder206](https://leetcode.com/coder206/)\'s [solution](https://leetcode.com/problems/can-make-arithmetic-progression-from-sequence/discuss/720152/O(n)-time-O(1)-space), we can do it in place by using original `arr` list.
My implementation of this solution is here.
Time: `O(N)`
Space: auxiliary `O(1)` because we do it in place
```
class Solution:
    def canMakeArithmeticProgression(self, arr: List[int]) -> bool:
        m = min(arr)
        gap = (max(arr) - m) / (len(arr) - 1)
        if gap == 0: return True
        i = 0
        while i < len(arr):
            if arr[i] == m + i * gap:
                i += 1
            else:
                dis = arr[i] - m
                if dis % gap != 0: return False
                pos = int(dis / gap)
                if arr[pos] == arr[i]: return False
                arr[pos], arr[i] = arr[i], arr[pos]
        return True
```
</p>


### O(n) time, O(1) space
- Author: coder206
- Creation Date: Sun Jul 05 2020 12:05:47 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 06 2020 04:06:11 GMT+0800 (Singapore Standard Time)

<p>
Maximum and minimum are first and last elements of the progression. Using this fact we find the difference. Then we rearrange the elements in the input array.

The second cycle is O(n) because we look at every element no more then twice. First time to put it in the correct place and second time to skip over it.
```
class Solution
{
public:
    bool canMakeArithmeticProgression(vector<int>& arr)
    {
        if (arr.size() <= 2) return true;
        int min = INT_MAX, max = INT_MIN;
        for (int num : arr) {
            min = std::min(min, num);
            max = std::max(max, num);
        }
        if ((max - min) % (arr.size() - 1) != 0) return false;
        int d = (max - min) / (arr.size() - 1);

        int i = 0;
        while (i < arr.size()) {
            if (arr[i] == min + i * d) i++;
            else if ((arr[i] - min) % d != 0) return false;
            else {
                int pos = (arr[i] - min) / d;
                if (pos < i || arr[pos] == arr[i]) return false;
                std::swap(arr[i], arr[pos]);
            }
        }
        return true;
    }
};
```
**Update:**

The goal of the second loop is to reorder elements in such a way that arr[i] = min + i * d (if this is impossible false must be returned).
```
01 while (i < arr.size()) {
02     if (arr[i] == min + i * d) i++;
03     else if ((arr[i] - min) % d != 0) return false;
04     else {
05         int pos = (arr[i] - min) / d;
06         if (arr[pos] == arr[i]) return false;
07         std::swap(arr[i], arr[pos]);
08     }
09 }
```
Cycle invariant: in the line 01 all elements arr[k], for 0 <= k < i, are correctly reordered. This is true before first cycle execution and is kept true by the following statements.

In the line 02 we test next element of the array. If condition is true the value `i` can be incremented without breaking the cycle invariant. Otherwise the value in arr[i] must be moved to the correct position. In the line 03 we verify that arr[i] belongs to the progression (can be represented as a * d + min) and later in the line 05 calculate the correct position. Next in the line 06 (the part `pos < i` in the original post is useless) we ensure that the number of elements that are in correct positions increases in the line 07 (this is important for complexity analysis and relates to the actual exit condition of the loop) otherwise we have duplicate elements. In the lines 03-08 elements before arr[i] are unchanged. Thus, in the line 09 the cycle invariant is true.

In every iteration we increase `i` or increase the number of elements in correct positions. The actual exit condition of the loop is `min(i, number of elements in correct positions) < arr.size()` that can be simplified to `i < arr.size()`. Thus, total number of iterations is no more than `2 * arr.size()`.
</p>


