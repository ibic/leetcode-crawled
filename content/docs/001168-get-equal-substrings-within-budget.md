---
title: "Get Equal Substrings Within Budget"
weight: 1168
#id: "get-equal-substrings-within-budget"
---
## Description
<div class="description">
<p>You are given two strings <code>s</code> and <code>t</code> of the same length. You want to change <code>s</code> to <code>t</code>. Changing the <code>i</code>-th character of <code>s</code> to <code>i</code>-th character of <code>t</code> costs <code>|s[i] - t[i]|</code> that is, the absolute difference between the ASCII values of the characters.</p>

<p>You are also given an integer <code>maxCost</code>.</p>

<p>Return the maximum length of a substring of <code>s</code> that can be changed to be the same as the corresponding substring of <code>t</code>with a cost less than or equal to <code>maxCost</code>.</p>

<p>If there is no substring from&nbsp;<code>s</code> that can be changed to its corresponding substring from <code>t</code>, return <code>0</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abcd&quot;, t = &quot;bcdf&quot;, maxCost = 3
<strong>Output:</strong> 3
<strong>Explanation: </strong>&quot;abc&quot; of s can change to &quot;bcd&quot;. That costs 3, so the maximum length is 3.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abcd&quot;, t = &quot;cdef&quot;, maxCost = 3
<strong>Output:</strong> 1
<strong>Explanation: </strong>Each character in s costs 2 to change to charactor in <code>t, so the maximum length is 1.</code>
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abcd&quot;, t = &quot;acde&quot;, maxCost = 0
<strong>Output:</strong> 1
<strong>Explanation: </strong>You can&#39;t make any change, so the maximum length is 1.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length, t.length &lt;= 10^5</code></li>
	<li><code>0 &lt;= maxCost &lt;= 10^6</code></li>
	<li><code>s</code> and&nbsp;<code>t</code> only contain lower case English letters.</li>
</ul>

</div>

## Tags
- Array (array)
- Sliding Window (sliding-window)

## Companies
- Traveloka - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Sliding Window
- Author: lee215
- Creation Date: Sun Sep 29 2019 12:01:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 29 2019 15:05:53 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Change the input of string `s` and `t` into an array of difference.
Then it\'s a standard sliding window problem.
<br>

## **Complexity**
Time `O(N)` for one pass
Space `O(1)`
<br>

**Java:**
```java
    public int equalSubstring(String s, String t, int k) {
        int n = s.length(), i = 0, j;
        for (j = 0; j < n; ++j) {
            k -= Math.abs(s.charAt(j) - t.charAt(j));
            if (k < 0) {
                k += Math.abs(s.charAt(i) - t.charAt(i));
                ++i;
            }
        }
        return j - i;
    }
```

**C++:**
```cpp
    int equalSubstring(string s, string t, int k) {
        int n = s.length(), i = 0, j;
        for (j = 0; j < n; ++j) {
            if ((k -= abs(s[j] - t[j])) < 0)
                k += abs(s[i] - t[i++]);
        }
        return j - i;
    }
```

**Python:**
```python
    def equalSubstring(self, s, t, cost):
        i = 0
        for j in xrange(len(s)):
            cost -= abs(ord(s[j]) - ord(t[j]))
            if cost < 0:
                cost += abs(ord(s[i]) - ord(t[i]))
                i += 1
        return j - i + 1
```

</p>


### [C++] Sliding window O(n) & Prefix sum O(nlogn) implementations
- Author: PhoenixDD
- Creation Date: Sun Sep 29 2019 12:01:45 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 07 2019 04:12:21 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
Since we need to find the maximum substring that can be replaced we can actually breakdown this problem to an array of integers that represent the replacement cost of `s[i]` to `t[i]` and then find the maximum length of continuous integers in that array whose `sum <= maxCost`.
eg:
`s = "aabcd"`
`t =  "zbdag"`
The array to find the maximum length on comes out to` [1,1,2,2,3]`.

This problem can now be solved in many ways, two of which are descibed below.
Somewhat similar to `209. Minimum Size Subarray Sum`

**Sliding window solution**
In general a sliding window is when we keep increasing the size of the window by increasing the right end to fit our goal, if it increases the goal we reduce the window by  sliding the left end until it again fits the goal, this ensures that the maximum window size is attained.
```c++
class Solution {
public:
    int equalSubstring(string s, string t, int maxCost) 
    {
        int start=0,end=0,sum=0;
        while(end<s.length())
        {
            sum+=abs(s[end]-t[end++]);
            if(sum>maxCost)
                sum-=abs(s[start]-t[start++]);
        }
        return end-start;
    }
};
```

**Complexity**
Time: O(n). Since each element is added and removed once at max.
Space: O(1). Since we get the elemets of sum array on the fly.

**Prefix sum slolution**
Here we create an array of prefix sums that hold the sum of differences from `0` to `i`.
At every iteration of `i` we can find the continuous sum of `numbers <= maxCost` by binary searching the lowerbound of `prefixSum[i] - maxCost`.
```
class Solution {
public:
    int equalSubstring(string s, string t, int maxCost) 
    {
        vector<int> prefixDiffSum(s.length()+1,0);
        int Max=0;
        for(int i=1;i<s.length()+1;i++)
        {
            prefixDiffSum[i]=prefixDiffSum[i-1]+abs(s[i-1]-t[i-1]);   //Stores sum of differences from begining till i.
            Max=max(Max,i-(int)(lower_bound(prefixDiffSum.begin(),prefixDiffSum.begin()+i+1,prefixDiffSum[i]-maxCost)-prefixDiffSum.begin()));         //Binary search to find start of the window that holds sum<=maxCost and ends with i.
        }
        return Max;
    }
};
```
**Complexity**
Time: O(nlogn). 
Space: O(n). Storing prefix sums.
</p>


### Java Sliding Window with Clear Explanation
- Author: kkzeng
- Creation Date: Sun Sep 29 2019 12:26:40 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 30 2019 03:54:27 GMT+0800 (Singapore Standard Time)

<p>
**Explanation:**

The idea is that you want to convert one character to another in that same position i.e. s[i] to t[i] such that you form the longest contiguous converted String.

There is just one key realization that you must make to solve this problem:

You need to realize that this problem can be reduced to finding the longest subarray such that the sum of the elements in that subarray is less than maxCost.

This is because you can generate an array `diff` such that `diff[i] = Math.abs(s[i] - t[i])` which is the cost of converting one character to another. You also want to find the maximum length substring that can be converted given the maxCost so this is equivalent to finding the longest subarray in `diff` which has sum of elements less than cost.

For this we can use a sliding window. We slide the `end` of the window to the right with each step. If the total sum exceeds the maxCost, we slide the `start` of the window to the right until the total sum inside the window is less than maxCosts. With each eligible window, we take the length and keep track of the maximum length.

For more info on sliding window: [Explanation](https://www.geeksforgeeks.org/window-sliding-technique/)

**Code:**

```

class Solution {
    public int equalSubstring(String s, String t, int maxCost) {
        // Convert the problem into a min subarray problem
        int[] diff = new int[s.length()];
        for(int i = 0; i < s.length(); ++i) {
            int asciiS = s.charAt(i);
            int asciiT = t.charAt(i);
            diff[i] = Math.abs(asciiS - asciiT);
        }
        
        // Now find the longest subarray <= maxCost
        // all diff[i] >= 0 (non-negative)
        
        // Use sliding window?
        int maxLen = 0;
        int curCost = 0;
        int start = 0;
        
        for(int end = 0; end < diff.length; ++end) {
            curCost += diff[end];
            while(curCost > maxCost) {
                curCost -= diff[start];
                ++start;
            }
            maxLen = Math.max(maxLen, end - start + 1);
        }
        
        return maxLen;
    }
}
```

`Time Complexity: O(N) where N is the length of strings s, t`
`Space Complexity: O(N) for diff`
Note: O(1) space can be achieved if we don\'t create the diff array - we can just get the values on the fly.
</p>


