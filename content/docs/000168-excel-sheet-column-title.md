---
title: "Excel Sheet Column Title"
weight: 168
#id: "excel-sheet-column-title"
---
## Description
<div class="description">
<p>Given a positive integer, return its corresponding column title as appear in an Excel sheet.</p>

<p>For example:</p>

<pre>
    1 -&gt; A
    2 -&gt; B
    3 -&gt; C
    ...
    26 -&gt; Z
    27 -&gt; AA
    28 -&gt; AB 
    ...
</pre>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> 1
<strong>Output:</strong> &quot;A&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> 28
<strong>Output:</strong> &quot;AB&quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> 701
<strong>Output:</strong> &quot;ZY&quot;
</pre>
</div>

## Tags
- Math (math)

## Companies
- Apple - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: true)
- VMware - 2 (taggedByAdmin: false)
- FactSet - 6 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: true)
- Adobe - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Zenefits - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My 1 lines code in Java, C++, and Python
- Author: xcv58
- Creation Date: Sun Dec 21 2014 01:16:58 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 06:39:58 GMT+0800 (Singapore Standard Time)

<p>
Java:

    return n == 0 ? "" : convertToTitle(--n / 26) + (char)('A' + (n % 26));

C++:

    return n == 0 ? "" : convertToTitle(n / 26) + (char) (--n % 26 + 'A');

update: because the behavior of different compilers, the safe version should be:

    return n == 0 ? "" : convertToTitle((n - 1) / 26) + (char) ((n - 1) % 26 + 'A');

Python:

    return "" if num == 0 else self.convertToTitle((num - 1) / 26) + chr((num - 1) % 26 + ord('A'))
</p>


### Python solution with explanation
- Author: yuzhiqiang
- Creation Date: Mon Dec 22 2014 05:20:04 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 04 2018 05:39:06 GMT+0800 (Singapore Standard Time)

<p>
Let's see the relationship between the Excel sheet column title and the number:

    A   1     AA    26+ 1     BA  2\xd726+ 1     ...     ZA  26\xd726+ 1     AAA  1\xd726\xb2+1\xd726+ 1
    B   2     AB    26+ 2     BB  2\xd726+ 2     ...     ZB  26\xd726+ 2     AAB  1\xd726\xb2+1\xd726+ 2
    .   .     ..    .....     ..  .......     ...     ..  ........     ...  .............   
    .   .     ..    .....     ..  .......     ...     ..  ........     ...  .............
    .   .     ..    .....     ..  .......     ...     ..  ........     ...  .............
    Z  26     AZ    26+26     BZ  2\xd726+26     ...     ZZ  26\xd726+26     AAZ  1\xd726\xb2+1\xd726+26

Now we can see that ABCD\uff1dA\xd726\xb3\uff0bB\xd726\xb2\uff0bC\xd726\xb9\uff0bD\uff1d1\xd726\xb3\uff0b2\xd726\xb2\uff0b3\xd726\xb9\uff0b4

But how to get the column title from the number? We can't simply use the n%26 method because:

ZZZZ\uff1dZ\xd726\xb3\uff0bZ\xd726\xb2\uff0bZ\xd726\xb9\uff0bZ\uff1d26\xd726\xb3\uff0b26\xd726\xb2\uff0b26\xd726\xb9\uff0b26

We can use (n-1)%26 instead, then we get a number range from 0 to 25.

    class Solution:
        # @return a string
        def convertToTitle(self, num):
            capitals = [chr(x) for x in range(ord('A'), ord('Z')+1)]
            result = []
            while num > 0:
                result.append(capitals[(num-1)%26])
                num = (num-1) // 26
            result.reverse()
            return ''.join(result)
</p>


### Accepted Java solution
- Author: murat
- Creation Date: Mon Dec 22 2014 09:07:30 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 13:43:26 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public String convertToTitle(int n) {
            StringBuilder result = new StringBuilder();
    
            while(n>0){
                n--;
                result.insert(0, (char)('A' + n % 26));
                n /= 26;
            }
    
            return result.toString();
        }
    }
</p>


