---
title: "Stone Game V"
weight: 1428
#id: "stone-game-v"
---
## Description
<div class="description">
<p>There are several stones&nbsp;<strong>arranged in a row</strong>, and each stone has an associated&nbsp;value which is an integer given in the array&nbsp;<code>stoneValue</code>.</p>

<p>In each round of the game, Alice divides the row into <strong>two non-empty rows</strong> (i.e. left row and right row), then Bob calculates the value of each row which is the sum of the values of all the stones in this row. Bob throws away the row which has the maximum value, and&nbsp;Alice&#39;s score increases by the value of the remaining row. If the value of the two rows are equal, Bob lets Alice decide which row will be thrown away. The next round starts with the remaining row.</p>

<p>The game ends when there is only <strong>one stone remaining</strong>. Alice&#39;s is initially <strong>zero</strong>.</p>

<p>Return <i>the maximum score that Alice can obtain</i>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> stoneValue = [6,2,3,4,5,5]
<strong>Output:</strong> 18
<strong>Explanation:</strong> In the first round, Alice divides the row to [6,2,3], [4,5,5]. The left row has the value 11 and the right row has value 14. Bob throws away the right row and Alice&#39;s score is now 11.
In the second round Alice divides the row to [6], [2,3]. This time Bob throws away the left row and Alice&#39;s score becomes 16 (11 + 5).
The last round Alice has only one choice to divide the row which is [2], [3]. Bob throws away the right row and Alice&#39;s score is now 18 (16 + 2). The game ends because only one stone is remaining in the row.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> stoneValue = [7,7,7,7,7,7,7]
<strong>Output:</strong> 28
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> stoneValue = [4]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= stoneValue.length &lt;= 500</code></li>
	<li><code>1 &lt;=&nbsp;stoneValue[i] &lt;= 10^6</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] Prefix Sum + DP (Memoization)
- Author: PhoenixDD
- Creation Date: Sun Aug 23 2020 12:01:07 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 24 2020 02:05:13 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
1. We can simply do what\'s stated in the question and try all the possible partitions.
1. If the sum of left row is less recur on the left row.
1. If the sum of right row is less recur on the right row.
1. If the sum of both rows are equal we try recuring on both the partitions and chose the one with maximum result.
1. Now simply repeat the steps for the new row
1. Do this until there is only 1 stone left.
1. We can see that there are recalculations of selected row when we perform the previous 3 steps. Thus we memoize on the row represented by `i,j` i.e row start and row end.

**Solution**

```c++
class Solution {
public:
    vector<int> prefixSum;			//Stores prefixSums
    vector<vector<int>> memo;
    int dp(vector<int>& stoneValue,int i,int j)
    {
        if(i==j)
            return 0;
        if(memo[i][j]!=-1)
            return memo[i][j];
        memo[i][j]=0;
        for(int p=i+1;p<=j;p++)		//Try each partition.
        {
	    int l=prefixSum[p]-prefixSum[i],r=prefixSum[j+1]-prefixSum[p];
	    if(l<r)		//Left part is smaller
                memo[i][j]=max(memo[i][j],l+dp(stoneValue,i,p-1));
            else if(l>r)	//Right part is smaller
                memo[i][j]=max(memo[i][j],r+dp(stoneValue,p,j));
            else	//Both parts are equal
                memo[i][j]=max(memo[i][j],l+max(dp(stoneValue,p,j),dp(stoneValue,i,p-1)));
        }
        return memo[i][j];
    }
    int stoneGameV(vector<int>& stoneValue)
    {
        memo.resize(stoneValue.size(),vector<int>(stoneValue.size(),-1));
        prefixSum.resize(stoneValue.size()+1,0);
        for(int i=0;i<stoneValue.size();i++)		//Create prefix Sums
            prefixSum[i+1]=prefixSum[i]+stoneValue[i];
        return dp(stoneValue,0,stoneValue.size()-1);
    }
};
```

**Complexity**
Time: `O(n^3)`
Space: `O(n^2)`
</p>


### [Java] Detailed Explanation - Easy Understand DFS + Memo - Top-Down DP
- Author: frankkkkk
- Creation Date: Sun Aug 23 2020 12:00:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 23 2020 12:00:43 GMT+0800 (Singapore Standard Time)

<p>
**Key Notes:**
- Classic DP problem
- Top-Down (DFS + Memo) is **easy to understand**
- Try split array at every possible point:
	- get left sum and right sum, choose the smaller one and keep searching
	- to save some time, we can pre-compute a **prefix array** to quick calculate sum
	- if left == right: try both ways
	- stop searching when only one element left, which means **pointer l == r**
- Cache searched result in dp by using state: pointer l and r

```java
public int stoneGameV(int[] stoneValue) {
        
	int N = stoneValue.length;

	// cache prefix
	int[] prefix = new int[N + 1];
	for (int i = 0; i < N; ++i) {
		prefix[i + 1] = prefix[i] + stoneValue[i];
	}

	return helper(stoneValue, 0, N - 1, prefix, new Integer[N][N]);
}

private int helper(int[] stoneValue, int l, int r, int[] prefix, Integer[][] dp) {

	if (l == r) return 0;
	if (l + 1 == r) return Math.min(stoneValue[l], stoneValue[r]);

	if (dp[l][r] != null) return dp[l][r];

	int res = 0;

	// left: [l, i], right: [i + 1, r]
	for (int i = l; i < r; ++i) {

		int left = prefix[i + 1] - prefix[l];
		int right = prefix[r + 1] - prefix[i + 1];

		if (left > right) {  // go right
			res = Math.max(res, right + helper(stoneValue, i + 1, r, prefix, dp));
		} else if (left < right) {  // go left
			res = Math.max(res, left + helper(stoneValue, l, i, prefix, dp));
		} else {  // left == right: go whatever max
			res = Math.max(res, Math.max(helper(stoneValue, l, i, prefix, dp),
										 helper(stoneValue, i + 1, r, prefix, dp)) 
										 + left);
		}

	}

	return dp[l][r] = res;
}
```

</p>


### [Python] DP
- Author: lee215
- Creation Date: Sun Aug 23 2020 12:02:45 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 02 2020 22:34:26 GMT+0800 (Singapore Standard Time)

<p>
**Update 2020/09/01:**
Seems new test cases added and enforce it to be O(N^2).
This solution is TLE now.

## **Intuition**
We have done stone games many times.
Just dp it.
<br>

## **Complexity**
Time `O(N^3)`
Space `O(N^2)`
<br>


**Python:**
```py
    def stoneGameV(self, A):
        n = len(A)
        prefix = [0] * (n + 1)
        for i, a in enumerate(A):
            prefix[i + 1] = prefix[i] + A[i]

        @functools.lru_cache(None)
        def dp(i, j):
            if i == j: return 0
            res = 0
            for m in range(i, j):
                left = prefix[m + 1] - prefix[i]
                right = prefix[j + 1] - prefix[m + 1]
                if left <= right:
                    res = max(res, dp(i, m) + left)
                if left >= right:
                    res = max(res, dp(m + 1, j) + right)
            return res
        return dp(0, n - 1)
```

</p>


