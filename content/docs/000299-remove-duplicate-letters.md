---
title: "Remove Duplicate Letters"
weight: 299
#id: "remove-duplicate-letters"
---
## Description
<div class="description">
<p>Given a string <code>s</code>, remove duplicate letters so that every letter appears once and only once. You must make sure your result is <strong>the smallest in lexicographical order</strong> among all possible results.</p>

<p><strong>Note:</strong> This question is the same as 1081:&nbsp;<a href="https://leetcode.com/problems/smallest-subsequence-of-distinct-characters/" target="_blank">https://leetcode.com/problems/smallest-subsequence-of-distinct-characters/</a></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;bcabc&quot;
<strong>Output:</strong> &quot;abc&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;cbacdcbc&quot;
<strong>Output:</strong> &quot;acdb&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 10<sup>4</sup></code></li>
	<li><code>s</code> consists of lowercase English letters.</li>
</ul>

</div>

## Tags
- String (string)
- Stack (stack)
- Greedy (greedy)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- ByteDance - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Nutanix - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Intuition

First we should make sure we understand what "lexicographical order" means. Comparing strings doesn't work the same way as comparing numbers. Strings are compared from the first character to the last one. Which string is greater depends on the comparison between _the first unequal corresponding character_ in the two strings. As a result any string beginning with `a` will always be less than any string beginning with `b`, regardless of the ends of both strings. 

Because of this, the optimal solution will have _the smallest characters as early as possible_. We draw two conclusions that provide different methods of solving this problem in $$O(N)$$:

1. The leftmost letter in our solution will be the smallest letter such that the suffix from that letter contains every other. This is because we know that the solution must have one copy of every letter, and we know that the solution will have the lexicographically smallest leftmost character possible.

    If there are multiple smallest letters, then we pick the leftmost one simply because it gives us more options. We can always eliminate more letters later on, so the optimal solution will always remain in our search space.

2. As we iterate over our string, if character `i` is greater than character `i+1` and another occurrence of character `i` exists later in the string, deleting character `i` will **always** lead to the optimal solution. Characters that come later in the string `i` don't matter in this calculation because `i` is in a more significant spot. Even if character `i+1` isn't the best yet, we can always replace it for a smaller character down the line if possible.

Since we try to remove characters as early as possible, and picking the best letter at each step leads to the best solution, "greedy" should be going off like an alarm.  

---

#### Approach 1: Greedy - Solving Letter by Letter

**Algorithm**

We use idea number one from the intuition. In each iteration, we determine leftmost letter in our solution. This will be **the smallest character such that its suffix contains at least one copy of every character in the string**. We determine the rest our answer by recursively calling the function on the suffix we generate from the original string (leftmost letter is removed).


**Implementation**
<iframe src="https://leetcode.com/playground/xVwisP5z/shared" frameBorder="0" width="100%" height="361" name="xVwisP5z"></iframe>

Note that the code in this section is a translated / commented version of the code [in this post](https://leetcode.com/problems/remove-duplicate-letters/discuss/76768/A-short-O(n)-recursive-greedy-solution) originally written by [lixx2100](https://leetcode.com/lixx2100/).

**Complexity Analysis**

* Time complexity : $$O(N)$$. Each recursive call will take $$O(N)$$. The number of recursive calls is bounded by a constant (26 letters in the alphabet), so we have $$O(N) * C = O(N)$$.

* Space complexity : $$O(N)$$. Each time we slice the string we're creating a new one (strings are immutable). The number of slices is bound by a constant, so we have $$O(N) * C = O(N)$$.


---
#### Approach 2: Greedy - Solving with Stack

**Algorithm**

We use idea number two from the intuition. We will keep a stack to store the solution we have built as we iterate over the string, and we will delete characters off the stack whenever it is possible and it makes our string smaller.

Each iteration we add the current character to the solution if it hasn't already been used. We try to remove as many characters as possible off the top of the stack, and then add the current character

The conditions for deletion are:

1. The character is greater than the current characters
2. The character can be removed because it occurs later on

At each stage in our iteration through the string, we greedily keep what's on the stack as small as possible.

The following animation makes this more clear:

!?!../Documents/316_remove_duplicate_letters.json:960,540!?!

**Implementation**
<iframe src="https://leetcode.com/playground/tSSMZhL6/shared" frameBorder="0" width="100%" height="500" name="tSSMZhL6"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N)$$. Although there is a loop inside a loop, the time complexity is still $$O(N)$$. This is because the inner while loop is bounded by the total number of elements added to the stack (each time it fires an element goes). This means that the _total_ amount of time spent in the inner loop is bounded by $$O(N)$$, giving us a total time complexity of $$O(N)$$

* Space complexity : $$O(1)$$. At first glance it looks like this is $$O(N)$$, but that is not true! `seen` will only contain unique elements, so it's bounded by the number of characters in the alphabet (a constant). You can only add to `stack` if an element has not been seen, so `stack` also only consists of unique elements. This means that _both_ `stack` and `seen` are bounded by constant, giving us $$O(1)$$ space complexity.

## Accepted Submission (python3)
```python3
# class DLinkedList:
#     class Node:
#         def __init__(self, value):
#             sef.value = value
#             self.next = None
#             self.prev = None

#         def next(self):
#             return self.next

#         def prev(self):
#             return self.prev

#     def __init__(self, value):
#         self.head = Node(value)

#     def add(self, Node):
#         self.

class Solution:
    def removeDuplicateLetters(self, s: str) -> str:
        def offset(ch):
            return ord(ch) - ord('a')

        ls = len(s)
        if ls == 0:
            return ''
        count = [0] * 26
        for i in range(ls):
            oft = offset(s[i])
            count[oft] +=1
        pos = 0
        for i in range(ls):
            if s[i] < s[pos]:
                pos = i
            count[offset(s[i])] -= 1
            if count[offset(s[i])] == 0:
                break
        return s[pos] + self.removeDuplicateLetters(s[pos + 1:].replace(s[pos], ''))

    def removeDuplicateLettersNotWorking(self, s: str) -> str:
        NULL = chr(0)
        sa = [c for c in s]
        ls = len(sa)
        locMap = {}
        for i in range(ls):
            ch = sa[i]
            if ch in locMap:
                prevPos = locMap[ch]
                j = prevPos + 1
                while j < i and sa[j] == NULL:
                    j += 1
                nextCh = sa[j]
                if nextCh < ch:
                    locMap[ch] = i
                    sa[prevPos] = NULL
                else:
                    sa[i] = NULL
            else:
                locMap[ch] = i
            print(sa)
        return ''.join([e for e in sa if e != NULL])

# if __name__ == "__main__":
#     so = Solution()
#     print(so.removeDuplicateLetters("bcabc"))
#     print(so.removeDuplicateLetters("cbacdcbc"))
```

## Top Discussions
### A short O(n) recursive greedy solution
- Author: lixx2100
- Creation Date: Wed Dec 09 2015 12:20:01 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 08:09:16 GMT+0800 (Singapore Standard Time)

<p>
Given the string s, the greedy choice (i.e., the leftmost letter in the answer) is the smallest s[i], s.t.
the suffix s[i .. ] contains all the unique letters. (Note that, when there are more than one smallest s[i]'s, we choose the leftmost one. Why? Simply consider the example: "abcacb".)

After determining the greedy choice s[i], we get a new string s' from s by 

 1. removing all letters to the left of s[i],
 2. removing all s[i]'s from s.

We then recursively solve the problem w.r.t. s'. 

The runtime is O(26 * n) = O(n).

    public class Solution {
        public String removeDuplicateLetters(String s) {
            int[] cnt = new int[26];
            int pos = 0; // the position for the smallest s[i]
            for (int i = 0; i < s.length(); i++) cnt[s.charAt(i) - 'a']++;
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) < s.charAt(pos)) pos = i;
                if (--cnt[s.charAt(i) - 'a'] == 0) break;
            }
            return s.length() == 0 ? "" : s.charAt(pos) + removeDuplicateLetters(s.substring(pos + 1).replaceAll("" + s.charAt(pos), ""));
        }
    }
</p>


### Java solution using Stack with comments
- Author: dwaijam
- Creation Date: Thu Dec 24 2015 09:43:29 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 13:24:42 GMT+0800 (Singapore Standard Time)

<p>
    public String removeDuplicateLetters(String sr) {
    
        int[] res = new int[26]; //will contain number of occurences of character (i+'a')
        boolean[] visited = new boolean[26]; //will contain if character (i+'a') is present in current result Stack
        char[] ch = sr.toCharArray();
        for(char c: ch){  //count number of occurences of character 
            res[c-'a']++;
        }
        Stack<Character> st = new Stack<>(); // answer stack
        int index;
        for(char s:ch){ 
            index= s-'a';
            res[index]--;   //decrement number of characters remaining in the string to be analysed
            if(visited[index]) //if character is already present in stack, dont bother
                continue;
            //if current character is smaller than last character in stack which occurs later in the string again
            //it can be removed and  added later e.g stack = bc remaining string abc then a can pop b and then c
            while(!st.isEmpty() && s<st.peek() && res[st.peek()-'a']!=0){ 
                visited[st.pop()-'a']=false;
            }
            st.push(s); //add current character and mark it as visited
            visited[index]=true;
        }
    
        StringBuilder sb = new StringBuilder();
        //pop character from stack and build answer string from back
        while(!st.isEmpty()){
            sb.insert(0,st.pop());
        }
        return sb.toString();
    }
</p>


### Easy to understand iterative Java solution
- Author: WHJ425
- Creation Date: Wed Dec 09 2015 13:12:18 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 09:03:55 GMT+0800 (Singapore Standard Time)

<p>
The basic idea is to find out the smallest result letter by letter (one letter at a time). Here is the thinking process for input "cbacdcbc":

1. find out the last appeared position for each letter;
    c - 7
    b - 6
    a - 2
    d - 4
2. find out the smallest index from the map in step 1 (a - 2);
3. the first letter in the final result must be the smallest letter from index 0 to index 2;
4. repeat step 2 to 3 to find out remaining letters.

- the smallest letter from index 0 to index 2: a
- the smallest letter from index 3 to index 4: c
- the smallest letter from index 4 to index 4: d
- the smallest letter from index 5 to index 6: b

so the result is "acdb"

Notes:

- after one letter is determined in step 3, it need to be removed from the "last appeared position map", and the same letter should be ignored in the following steps
- in step 3, the beginning index of the search range should be the index of previous determined letter plus one

----

    public class Solution {
    
        public String removeDuplicateLetters(String s) {
            if (s == null || s.length() <= 1) return s;
    
            Map<Character, Integer> lastPosMap = new HashMap<>();
            for (int i = 0; i < s.length(); i++) {
                lastPosMap.put(s.charAt(i), i);
            }
    
            char[] result = new char[lastPosMap.size()];
            int begin = 0, end = findMinLastPos(lastPosMap);
    
            for (int i = 0; i < result.length; i++) {
                char minChar = 'z' + 1;
                for (int k = begin; k <= end; k++) {
                    if (lastPosMap.containsKey(s.charAt(k)) && s.charAt(k) < minChar) {
                        minChar = s.charAt(k);
                        begin = k+1;
                    }
                }
    
                result[i] = minChar;
                if (i == result.length-1) break;
    
                lastPosMap.remove(minChar);
                if (s.charAt(end) == minChar) end = findMinLastPos(lastPosMap);
            }
    
            return new String(result);
        }
    
        private int findMinLastPos(Map<Character, Integer> lastPosMap) {
            if (lastPosMap == null || lastPosMap.isEmpty()) return -1;
            int minLastPos = Integer.MAX_VALUE;
            for (int lastPos : lastPosMap.values()) {
                 minLastPos = Math.min(minLastPos, lastPos);
            }
            return minLastPos;
        }
    
    }
</p>


