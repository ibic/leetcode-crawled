---
title: "Subtree of Another Tree"
weight: 534
#id: "subtree-of-another-tree"
---
## Description
<div class="description">
<p>Given two <strong>non-empty</strong> binary trees <b>s</b> and <b>t</b>, check whether tree <b>t</b> has exactly the same structure and node values with a subtree of <b>s</b>. A subtree of <b>s</b> is a tree consists of a node in <b>s</b> and all of this node&#39;s descendants. The tree <b>s</b> could also be considered as a subtree of itself.</p>

<p><b>Example 1:</b><br />
Given tree s:</p>

<pre>
     3
    / \
   4   5
  / \
 1   2
</pre>
Given tree t:

<pre>
   4 
  / \
 1   2
</pre>
Return <b>true</b>, because t has the same structure and node values with a subtree of s.

<p>&nbsp;</p>

<p><b>Example 2:</b><br />
Given tree s:</p>

<pre>
     3
    / \
   4   5
  / \
 1   2
    /
   0
</pre>
Given tree t:

<pre>
   4
  / \
 1   2
</pre>
Return <b>false</b>.

<p>&nbsp;</p>

</div>

## Tags
- Tree (tree)

## Companies
- Amazon - 18 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)
- Google - 7 (taggedByAdmin: false)
- eBay - 0 (taggedByAdmin: true)

## Official Solution
[TOC]


## Solution

---
#### Approach #1 Using Preorder Traversal [Accepted]

**Algorithm**

We can find the preorder traversal of the given tree $$s$$ and $$t$$, given by, say $$s_{preorder}$$ and $$t_{preorder}$$ respectively(represented in the form of a string). Now, we can check if $$t_{preorder}$$ is a substring of $$s_{preorder}$$. 

But, in order to use this approach, we need to treat the given tree in a different manner. Rather than assuming a $$null$$ value for the childern of the leaf nodes, we need to treat the left and right child as a $$lnull$$ and $$rnull$$ value respectively. This is done to ensure that the $$t_{preorder}$$ doesn't become a substring of $$s_{preorder}$$ even in cases when $$t$$ isn't a subtree of $$s$$. 

You can also note that we've added a '#' before every considering every value. If this isn't done, the trees of the form `s:[23, 4, 5]` and `t:[3, 4, 5]` will also give a true result since the preorder string of the `t("23 4 lnull rull 5 lnull rnull")` will be a substring of the preorder string of `s("3 4 lnull rull 5 lnull rnull")`. Adding a '#' before the node's value solves this problem.

![Preorder_null](../Figures/572_Subtree_1.PNG)
{:align="center"}

![Preorder_lnull_rnull](../Figures/572_Subtree_2.PNG)
{:align="center"}


<iframe src="https://leetcode.com/playground/cagXWqSv/shared" frameBorder="0" name="cagXWqSv" width="100%" height="513"></iframe>

**Complexity Analysis**

* Time complexity : $$O(m^2+n^2+m*n)$$. A total of $$n$$ nodes of the tree $$s$$ and $$m$$ nodes of tree $$t$$ are traversed. Assuming string concatenation takes $$O(k)$$ time for strings of length $$k$$ and `indexOf` takes $$O(m*n)$$.

* Space complexity : $$O(max(m,n))$$. The depth of the recursion tree can go upto $$n$$ for tree $$t$$ and $$m$$ for tree $$s$$ in worst case.

---
#### Approach #2 By Comparison of Nodes  [Accepted]

**Algorithm**

Instead of creating an inorder traversal, we can treat every node of the given tree $$t$$ as the root, treat it as a subtree and compare the corresponding subtree with the given subtree $$s$$ for equality. For checking the equality, we can compare the all the nodes of the two subtrees. 

For doing this, we make use a function `traverse(s,t)` which traverses over the given tree $$s$$ and treats every node as the root of the subtree currently being considered. It also checks the two subtrees currently being considered for their equality. In order to check the equality of the two subtrees, we make use of `equals(x,y)` function, which takes $$x$$ and $$y$$, which are the roots of the two subtrees to be compared as the inputs and returns True or False depending on whether the two are equal or not. It compares all the nodes of the two subtrees for equality. Firstly, it checks whether the roots of the two trees for equality and then calls itself recursively for the left subtree and the right subtree.

The follwowing animation depicts an abstracted view of the process:

!?!../Documents/572_Subtree.json:1000,563!?!

<iframe src="https://leetcode.com/playground/A6ipy4aH/shared" frameBorder="0" name="A6ipy4aH" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(m*n)$$. In worst case(skewed tree) `traverse` function takes $$O(m*n)$$ time. 

* Space complexity : $$O(n)$$. The depth of the recursion tree can go upto $$n$$. $$n$$ refers to the number of nodes in $$s$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution, tree traversal
- Author: shawngao
- Creation Date: Sun May 07 2017 11:08:02 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 13:14:35 GMT+0800 (Singapore Standard Time)

<p>
For each node during pre-order traversal of ```s```, use a recursive function ```isSame``` to validate if sub-tree started with this node is the same with ```t```.
```
public class Solution {
    public boolean isSubtree(TreeNode s, TreeNode t) {
        if (s == null) return false;
        if (isSame(s, t)) return true;
        return isSubtree(s.left, t) || isSubtree(s.right, t);
    }
    
    private boolean isSame(TreeNode s, TreeNode t) {
        if (s == null && t == null) return true;
        if (s == null || t == null) return false;
        
        if (s.val != t.val) return false;
        
        return isSame(s.left, t.left) && isSame(s.right, t.right);
    }
}
```
</p>


### Python, Straightforward with Explanation (O(ST) and O(S+T) approaches)
- Author: awice
- Creation Date: Sun May 07 2017 11:13:47 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 12:41:26 GMT+0800 (Singapore Standard Time)

<p>
**Naive approach, O(|s| * |t|)**
For each node of ```s```, let's check if it's subtree equals ```t```.  We can do that in a straightforward way by an ```isMatch``` function: check if ```s``` and ```t``` match at the values of their roots, plus their subtrees match.  Then, in our main function, we want to check if ```s``` and ```t``` match, or if ```t``` is a subtree of a child of ```s```.

```
def isMatch(self, s, t):
    if not(s and t):
        return s is t
    return (s.val == t.val and 
            self.isMatch(s.left, t.left) and 
            self.isMatch(s.right, t.right))

def isSubtree(self, s, t):
    if self.isMatch(s, t): return True
    if not s: return False
    return self.isSubtree(s.left, t) or self.isSubtree(s.right, t)
```

**Advanced approach, O(|s| + |t|) (Merkle hashing):**
For each node in a tree, we can create ```node.merkle```, a hash representing it's subtree.
This hash is formed by hashing the concatenation of the merkle of the left child, the node's value, and the merkle of the right child.  Then, two trees are identical if and only if the merkle hash of their roots are equal (except when there is a hash collision.)  From there, finding the answer is straightforward: we simply check if any ```node``` in ```s``` has ```node.merkle == t.merkle```

```
def isSubtree(self, s, t):
    from hashlib import sha256
    def hash_(x):
        S = sha256()
        S.update(x)
        return S.hexdigest()
        
    def merkle(node):
        if not node:
            return '#'
        m_left = merkle(node.left)
        m_right = merkle(node.right)
        node.merkle = hash_(m_left + str(node.val) + m_right)
        return node.merkle
        
    merkle(s)
    merkle(t)
    def dfs(node):
        if not node:
            return False
        return (node.merkle == t.merkle or 
                dfs(node.left) or dfs(node.right))
                    
    return dfs(s)
```
</p>


### Short Python by converting into strings
- Author: realisking
- Creation Date: Sun May 07 2017 11:28:12 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 11:59:37 GMT+0800 (Singapore Standard Time)

<p>
Basically we convert our ```tree``` into ```string``` representation, then just check whether substring exists in target string.
```
class Solution(object):
    def isSubtree(self, s, t):
        """
        :type s: TreeNode
        :type t: TreeNode
        :rtype: bool
        """
        def convert(p):
            return "^" + str(p.val) + "#" + convert(p.left) + convert(p.right) if p else "$"
        
        return convert(t) in convert(s)
```
</p>


