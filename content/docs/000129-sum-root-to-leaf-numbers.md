---
title: "Sum Root to Leaf Numbers"
weight: 129
#id: "sum-root-to-leaf-numbers"
---
## Description
<div class="description">
<p>Given a binary tree containing digits from <code>0-9</code> only, each root-to-leaf path could represent a number.</p>

<p>An example is the root-to-leaf path <code>1-&gt;2-&gt;3</code> which represents the number <code>123</code>.</p>

<p>Find the total sum of all root-to-leaf numbers.</p>

<p><strong>Note:</strong>&nbsp;A leaf is a node with no children.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> [1,2,3]
    1
   / \
  2   3
<strong>Output:</strong> 25
<strong>Explanation:</strong>
The root-to-leaf path <code>1-&gt;2</code> represents the number <code>12</code>.
The root-to-leaf path <code>1-&gt;3</code> represents the number <code>13</code>.
Therefore, sum = 12 + 13 = <code>25</code>.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> [4,9,0,5,1]
    4
   / \
  9   0
&nbsp;/ \
5   1
<strong>Output:</strong> 1026
<strong>Explanation:</strong>
The root-to-leaf path <code>4-&gt;9-&gt;5</code> represents the number 495.
The root-to-leaf path <code>4-&gt;9-&gt;1</code> represents the number 491.
The root-to-leaf path <code>4-&gt;0</code> represents the number 40.
Therefore, sum = 495 + 491 + 40 = <code>1026</code>.</pre>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Facebook - 9 (taggedByAdmin: false)
- Amazon - 6 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Overview

**Prerequisites**

There are three DFS ways to traverse the tree: preorder, postorder and inorder.
Please check two minutes picture explanation, if you don't remember them quite well:
[here is Python version](https://leetcode.com/problems/binary-tree-inorder-traversal/discuss/283746/all-dfs-traversals-preorder-inorder-postorder-in-python-in-1-line) 
and 
[here is Java version](https://leetcode.com/problems/binary-tree-inorder-traversal/discuss/328601/all-dfs-traversals-preorder-postorder-inorder-in-java-in-5-lines).

**Optimal Strategy to Solve the Problem**

> Root-to-left traversal is so-called _DFS preorder traversal_. To implement it,
one has to follow straightforward strategy Root->Left->Right. 

Since one has to visit all nodes, the best possible time complexity here is 
linear. Hence all interest here is to improve the space complexity. 

> There are 3 ways to implement preorder traversal: iterative,
recursive and Morris. 

Iterative and recursive approaches here do the job in one pass, 
but they both need up to $$\mathcal{O}(H)$$ space to keep the stack, 
where $$H$$ is a tree height.

Morris approach is two-pass approach, but it's a constant-space one.

![diff](../Figures/129/preorder2.png)

<br />
<br />


---
#### Approach 1: Iterative Preorder Traversal.

**Intuition**

Here we implement standard iterative preorder traversal with the stack:

- Push root into stack.

- While stack is not empty:

    - Pop out a node from stack and update the current number.
    
    - If the node is a leaf, update root-to-leaf sum.
    
    - Push right and left child nodes into stack.
    
- Return root-to-leaf sum.  

!?!../Documents/129_LIS.json:1000,383!?!

**Implementation**

Note, that 
[Javadocs recommends to use ArrayDeque, and not Stack as a stack implementation](https://docs.oracle.com/javase/8/docs/api/java/util/ArrayDeque.html).

<iframe src="https://leetcode.com/playground/FvQpLYha/shared" frameBorder="0" width="100%" height="480" name="FvQpLYha"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since one has to visit each node. 
    
* Space complexity: up to $$\mathcal{O}(H)$$ to keep the stack,
where $$H$$ is a tree height.  
<br />
<br />


---
#### Approach 2: Recursive Preorder Traversal.

Iterative approach 1 could be converted into recursive one.

Recursive preorder traversal is extremely simple: 
follow Root->Left->Right direction, i.e. do all the business with the node 
(= update the current number and root-to-leaf sum), 
and then do the recursive calls for the left and right child nodes.

P.S.
Here is the difference between _preorder_ and the other DFS recursive traversals. 
On the following figure the nodes are numerated in the order you visit them, 
please follow `1-2-3-4-5` to compare different DFS strategies implemented as
recursion.

![diff](../Figures/129/ddfs2.png)

**Implementation**

<iframe src="https://leetcode.com/playground/8RUD5gud/shared" frameBorder="0" width="100%" height="395" name="8RUD5gud"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since one has to visit each node. 
    
* Space complexity: up to $$\mathcal{O}(H)$$ to keep the recursion stack,
where $$H$$ is a tree height.  
<br />
<br />


---
#### Approach 3: Morris Preorder Traversal.

We discussed already iterative and recursive preorder traversals, 
which both have great time complexity though use up to 
$$\mathcal{O}(H)$$ to keep the stack. We could trade in performance to save space.

The idea of Morris preorder traversal is simple: 
to use no space but to traverse the tree.

> How that could be even possible? At each node one has to decide where to go:
to the left or tj the right, traverse the left subtree or traverse the right subtree. 
How one could know that the left subtree is already done if no 
additional memory is allowed?  

The idea of [Morris](https://www.sciencedirect.com/science/article/pii/0020019079900681)
algorithm is to set the _temporary link_ between the node and its 
[predecessor](https://leetcode.com/articles/delete-node-in-a-bst/):
`predecessor.right = root`.
So one starts from the node, computes its predecessor and
verifies if the link is present.

- There is no link? Set it and go to the left subtree.

- There is a link? Break it and go to the right subtree.  

There is one small issue to deal with : what if there is no
left child, i.e. there is no left subtree? 
Then go straightforward to the right subtree.

**Implementation**

!?!../Documents/129_RES.json:1000,408!?!

<iframe src="https://leetcode.com/playground/yyto7hCt/shared" frameBorder="0" width="100%" height="500" name="yyto7hCt"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$.
    
* Space complexity: $$\mathcal{O}(1)$$. 
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Short Java solution. Recursion.
- Author: pavel-shlyk
- Creation Date: Sun Jan 04 2015 23:48:18 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 16:06:56 GMT+0800 (Singapore Standard Time)

<p>
\x01I use recursive solution to solve the problem.

    public int sumNumbers(TreeNode root) {
		return sum(root, 0);
	}
	
	public int sum(TreeNode n, int s){
		if (n == null) return 0;
		if (n.right == null && n.left == null) return s*10 + n.val;
		return sum(n.left, s*10 + n.val) + sum(n.right, s*10 + n.val);
	}
</p>


### Python solutions (dfs+stack, bfs+queue, dfs recursively).
- Author: OldCodingFarmer
- Creation Date: Sat Aug 15 2015 01:11:42 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 06:30:14 GMT+0800 (Singapore Standard Time)

<p>
        
    # dfs + stack
    def sumNumbers1(self, root):
        if not root:
            return 0
        stack, res = [(root, root.val)], 0
        while stack:
            node, value = stack.pop()
            if node:
                if not node.left and not node.right:
                    res += value
                if node.right:
                    stack.append((node.right, value*10+node.right.val))
                if node.left:
                    stack.append((node.left, value*10+node.left.val))
        return res
        
    # bfs + queue
    def sumNumbers2(self, root):
        if not root:
            return 0
        queue, res = collections.deque([(root, root.val)]), 0
        while queue:
            node, value = queue.popleft()
            if node:
                if not node.left and not node.right:
                    res += value
                if node.left:
                    queue.append((node.left, value*10+node.left.val))
                if node.right:
                    queue.append((node.right, value*10+node.right.val))
        return res
        
    # recursively 
    def sumNumbers(self, root):
        self.res = 0
        self.dfs(root, 0)
        return self.res
        
    def dfs(self, root, value):
        if root:
            #if not root.left and not root.right:
            #    self.res += value*10 + root.val
            self.dfs(root.left, value*10+root.val)
            #if not root.left and not root.right:
            #    self.res += value*10 + root.val
            self.dfs(root.right, value*10+root.val)
            if not root.left and not root.right:
                self.res += value*10 + root.val
</p>


### Can you improve this algorithm?
- Author: potpie
- Creation Date: Tue Dec 31 2013 03:58:00 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 31 2013 03:58:00 GMT+0800 (Singapore Standard Time)

<p>
    /**
     * Definition for binary tree
     * public class TreeNode {
     *     int val;
     *     TreeNode left;
     *     TreeNode right;
     *     TreeNode(int x) { val = x; }
     * }
     */
    public class Solution {
        public int sumNumbers(TreeNode root) {
            if (root == null)
                return 0;
            return sumR(root, 0);
        }
        public int sumR(TreeNode root, int x) {
            if (root.right == null && root.left == null)
                return 10 * x + root.val;
            int val = 0;
            if (root.left != null)
                val += sumR(root.left, 10 * x + root.val);
            if (root.right != null)
                val += sumR(root.right, 10 * x + root.val);
            return val;
        }
    }
</p>


