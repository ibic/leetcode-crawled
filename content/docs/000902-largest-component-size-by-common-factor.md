---
title: "Largest Component Size by Common Factor"
weight: 902
#id: "largest-component-size-by-common-factor"
---
## Description
<div class="description">
<p>Given a non-empty&nbsp;array of unique positive integers <code>A</code>, consider the following graph:</p>

<ul>
	<li>There are <code>A.length</code> nodes, labelled <code>A[0]</code> to <code>A[A.length - 1];</code></li>
	<li>There is an edge between <code>A[i]</code> and <code>A[j]</code>&nbsp;if and only if&nbsp;<code>A[i]</code> and <code>A[j]</code> share a common factor greater than 1.</li>
</ul>

<p>Return the size of the largest connected component in the graph.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[4,6,15,35]</span>
<strong>Output: </strong><span id="example-output-1">4</span>
<span><img alt="" src="https://assets.leetcode.com/uploads/2018/12/01/ex1.png" style="width: 257px; height: 50px;" /></span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[20,50,9,63]</span>
<strong>Output: </strong><span id="example-output-2">2</span>
<span><img alt="" src="https://assets.leetcode.com/uploads/2018/12/01/ex2.png" style="width: 293px; height: 50px;" /></span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[2,3,6,7,4,12,21,39]</span>
<strong>Output: </strong><span id="example-output-3">8</span>
<span><img alt="" src="https://assets.leetcode.com/uploads/2018/12/01/ex3.png" style="width: 346px; height: 180px;" /></span>
</pre>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 20000</code></li>
	<li><code>1 &lt;= A[i] &lt;= 100000</code></li>
</ol>
</div>
</div>
</div>

</div>

## Tags
- Math (math)
- Union Find (union-find)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution


---
#### Overview

We could consider this problem as a graph partition problem. 

>Each number represents a node in a graph.
We are asked to partition the nodes into several groups and find the largest one.

Suppose that we have a means to determine and assign each node to a proper group, then we could easily solve the problem with a single iteration.

We could summarize the algorithm with the following Python pseudo code, where literally we count the appearance of each group.

```python
    group_count = {}
    for num in number_list:
        group_id = group(num)
        group_count[group_id] += 1
    return max(group_count.values())
```

>The key to the above algorithm lies in the questions on how we define a group and how we assign an element to a group.

Given the above intuition, it might remind you one of the most well-known data structures in computer science called [Disjoint Set](https://en.wikipedia.org/wiki/Disjoint-set_data_structure), which tracks a set of elements partitioned into a number of *disjoint* (non-overlapping) subsets.

**Union-Find Algorithm**

Indeed, the Disjoint-Set data structure would be the essential building block to solve this problem.

The Disjoint-Set data structure is also known as the Union-Find data structure. Because it mainly consists of two operations: `Union()` and `Find()` defined as follows:

- `Find(x)`: get the identity of the group that the element `x` belongs to.

- `Union(x, y)`: merge the two groups that the two elements belong to respectively.

Here are the sample implementation of the Union-Find data structure, following the [pseudo-code](https://en.wikipedia.org/wiki/Disjoint-set_data_structure) presented on the wiki page.

<iframe src="https://leetcode.com/playground/ND2b5r4f/shared" frameBorder="0" width="100%" height="500" name="ND2b5r4f"></iframe>

As one can see, the code is actually surprisingly concise, yet powerful.
It could be even more concise, if we did not care about the load balancing during the merge of groups in the `Union()` function.

We would use the implementation of this Union-Find data structure in the following approaches.

---
#### Approach 1: Union-Find via Factors 

**Intuition**

Now that we are equipped with the Union-Find structure, which greatly facilitates the group identification and group merge operations,
we can now reformulate the problem with the help of Union-Find.

>As we stated before, the problem can be considered as a _graph partition_ problem where we group nodes into a list of subsets.

Each number in the input list is represented as a node in the graph.
The connection between the nodes (_i.e._ edge) can happen, if and only if the two nodes share a **common factor** greater than one.

One naive idea would be that we enumerate all pairs of nodes, in order to partition the nodes into groups, with the help of Union-Find data structure as we implemented. This could pass some test cases, though it would exceed the time limit for tougher cases, since the algorithm has a quadratic time complexity.

>A more efficient idea would be that we build groups led by each of the common factors of the numbers. This can be done in a single iteration over each of the number.

For each number, we enumerate all factors that can divide the number, and then we **attribute** the number to each group led by the factor, _i.e._ `Union(num, factor)`.

![Venn diagram](../Figures/952/952_venn_diagram_.png)

As one can see in the above example, essentially we build a [Venn diagram](https://en.wikipedia.org/wiki/Venn_diagram), where each subset contains a series of numbers as well as factors.
Take the input number `6` as an example, it can be divided both by the factors of `2` and `3`.
As a result, it can be attributed to both groups that has the factors respectively.
And thanks to the number `6`, eventually the groups led by `2` and `3` respectively can be merged together.
At the end, all the input numbers can be attributed to a single big group, thanks to all the joints among the subgroups.

**Algorithm**

With the above intuition, we could implement the algorithm in two general steps:

- Step 1). Attribute each number to a series of groups that are led by each of its factors. 

    - We iterate through each number, denoted as `num`. For each number, we iterate from 2 to `sqrt(num)` to find out all the factors.

    - For each `factor` of `num`, we merge the groups of that possess the element `num` and `factor` respectively, _i.e._ `Union(num, factor)`. 

    - In addition, we perform the same union operation on the complement factor as well, _i.e._ `Union(num, num / factor)`.  

- Step 2). Iterate through each number a second time, to find out the final group that the number belongs to.

    - With the mapping between the number and its group ID, _i.e._ `{num -> group_id`}, it is intuitive to find out the group that has the most elements.

<iframe src="https://leetcode.com/playground/C8dQYCoh/shared" frameBorder="0" width="100%" height="500" name="C8dQYCoh"></iframe>

**Note:** One might encounter *TLE* (Time Limit Exceeded) exception with the above solution (especially in Python), when the online judge is under load.
But under the normal circumstance, the solution would be accepted, which is even faster than 30% of submissions.

**Complexity Analysis**

Since we applied the Union-Find data structure in our algorithm, we would like to start with a statement on the time complexity of the data structure, as follows:

>**Statement**: If $$M$$ operations, either Union or Find, are applied to $$N$$ elements, the total run time is $$\mathcal{O}(M \cdot \log^{*}{N})$$, where $$\log^{*}$$ is the [iterated logarithm](https://en.wikipedia.org/wiki/Iterated_logarithm).

One can refer to the [proof of Union-Find complexity](https://en.wikipedia.org/wiki/Proof_of_O(log*n)_time_complexity_of_union%E2%80%93find) for more details.

In our case, the number of elements in the Union-Find data structure is equal to the maximum number of the input list, _i.e._ `max(A)`.

Let $$N$$ be the number of elements in the input list, and $$M$$ be the maximum value of the input list.

- Time Complexity: $$\mathcal{O}(N \cdot \log_{2}{M} \cdot \log^{*}{M})$$ 

    - The number of factors for a given number is bounded by $$\log_{2}{M}$$. Since the minimal valid factor is 2, we then could decompose the number into the binary form, _i.e._ $$2\cdot 2 \cdot 2 ...$$. Hence, the maximum number of factors that a number could have would be the maximum number of bits in its binary form.

    - In the first step, we iterate through each number (_i.e._ $$N$$ iterations), and for each number, we iterate through all its factors (_i.e._ up to $$\log_{2}{M}$$ iterations). As a result, the time complexity of this step would be $$\mathcal{O}(N \cdot \log_{2}{M} \cdot \log^{*}{M})$$.

    - In the second step, we iterate through each number again.
    But this time, for each iteration we perform only once the Union-Find operation.
    Hence, the time complexity for this step would be $$\mathcal{O}(N \cdot \log^{*}{M})$$.

    - To sum up, the overall complexity of the algorithm would be $$\mathcal{O}(N \cdot \log_{2}{M} \cdot \log^{*}{M}) + \mathcal{O}(N \cdot \log^{*}{M}) = \mathcal{O}(N \cdot \log_{2}{M} \cdot \log^{*}{M}) $$.

- Space Complexity: $$\mathcal{O}(M + N)$$

    - The space complexity of the Union-Find data structure is $$\mathcal{O}(M)$$.

    - In the main algorithm, we use a hash table to keep track of the account for each group. In the worst case, each number forms an individual group. Therefore, the space complexity of this hash table is $$\mathcal{O}(N)$$.

    - To sum up, the overall space complexity of the algorithm is $$\mathcal{O}(M) + \mathcal{O}(N) = \mathcal{O}(M + N)$$.
<br/>
<br/>

---
#### Approach 2: Union-Find on Prime Factors

**Intuition**

One might notice that in the above algorithm, we would enumerate through a series of **_non-essential_** factors for a number.

For instance, for the number 12, we have a number of factors as `[2, 3, 4, 6, 8]`.
In this case, the factors of `[4, 6, 8]` are not essential, since they have been _covered_ by the _prime factors_ of `[2, 3]`. 
If there is another number (say `30`) that has a common factor with the number `12`, then this common factor is either one of the prime factors of `[2, 3]` or it can be further divided by these prime factors.

>The intuition is that the prime factors of a number can represent all of its factors, _i.e._ the integer can be characterized by a series of prime factors.

Indeed, _"By the fundamental theorem of arithmetic, every positive integer has a unique [prime factorization](https://en.wikipedia.org/wiki/Prime_number#Unique_factorization)",_  as we quote from [wikipedia](https://en.wikipedia.org/wiki/Integer_factorization).

Each positive integer (except 1) can be decomposed into a series of prime numbers, _e.g._ $$ 12 = 2 * 2 * 3$$.

>With the above theories, rather than enumerating all the factors of a number, we just need to enumerate the **prime factors** of a number, in our Union-Find data structure.


**Sieve Method**

Before proceeding to the main algorithm for this problem, let us briefly list the algorithm to decompose a number into a series of prime factors, which itself is not an easy problem.

Here we apply the [sieve of Eratosthenes](https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes) (let's call it sieve method for short), an ancient algorithm to calculate all prime factors up to any given limit.

<iframe src="https://leetcode.com/playground/jb7JkZ6U/shared" frameBorder="0" width="100%" height="327" name="jb7JkZ6U"></iframe>

**Algorithm**

Now that we know how to decompose a number into a series of prime factors, we can simply replace common factors in the previous approach with the prime factors. This could work.

However, there is another arguably more efficient method, which is that rather than Union-Find on all numbers together with its prime factors, we do the Union-Find **solely** on the prime factors, excluding the numbers.

We could therefore have much smaller set of elements for the Union-Find operations.
We illustrate how it could work on the same example before in the following graph.

![Union-Find on prime factors](../Figures/952/952_prime_factors_.png)

Similar with the previous approach, we could implement the algorithm in two general steps:

- Step 1). Decompose each number into its prime factors and apply `Union()` operations on the series of prime factors.

    - We iterate through each number, denoted as `num`. For each number, we decompose it into prime factors.

    - We join all groups that possess these prime factors, by applying `Union()` operation on each adjacent pair of prime factors.
    
    - In addition, we use a hash table to keep the mapping between each number and its any of prime factors. Later, we would use this table to find out which group that each number belongs to. 


- Step 2). Iterate through each number a second time, to find out the final group that the number belongs to.

    - Since we build Union-Find sets solely on the prime factors, we could find out which group that each prime factor belongs to, _i.e._ `prime_factor -> group_id`.

    - Thanks to the mapping between the number and its prime factor, _i.e._ `{num -> prime_factor`}, we could now find out which group that each number belongs with the above Union-Find sets, _i.e._ `num -> prime_factor -> group_id`.


<iframe src="https://leetcode.com/playground/5LmcTkdS/shared" frameBorder="0" width="100%" height="500" name="5LmcTkdS"></iframe>



**Complexity Analysis**

Let $$N$$ be the number of elements in the input list, and $$M$$ be the maximum value of the input list.

- Time Complexity: $$\mathcal{O}\big(N \cdot (\log_{2}{M} \cdot \log^{*}{M} + \sqrt{M}) \big)$$ 

    - First of all, the time complexity of the sieve method to calculate the prime factors of is $$\mathcal{O}(\sqrt{M})$$.

    - It is hard to estimate the number of prime factors for a given number. Since the smallest prime number is 2, a coarse upper bound for the number of the prime factors is $$\log_{2}{M}$$, _e.g._ $$ 8 = 2 * 2 * 2$$.

    - In the first step, we iterate through each number (_i.e._ $$N$$ iterations), and for each number, we iterate through all its factors (_i.e._ up to $$\log_{2}{M}$$ iterations). As a result, together with the calculation of prime factors, the time complexity of this step would be $$\mathcal{O}(N \cdot \log_{2}{M} \cdot \log^{*}{M}) + \mathcal{O}(N \cdot \sqrt{M}) = \mathcal{O}\big(N \cdot (\log_{2}{M} \cdot \log^{*}{M} + \sqrt{M}) \big)$$.

    - In the second step, we iterate through each number again.
    But this time, for each iteration we perform only once the Union-Find operation, _i.e._ $$\mathcal{O}(N \cdot \log^{*}{M})$$.

    - To sum up, the overall complexity of the algorithm would be $$\mathcal{O}\big(N \cdot (\log_{2}{M} \cdot \log^{*}{M} + \sqrt{M}) \big)$$.

    - As one might notice that, the asymptotic complexity of this approach seems to be inferior than the previous approach, due to the calculation of prime factors. However, in reality, the saving we gain on the Union-Find operations could outweigh the cost of prime factor calculation.

- Space Complexity: $$\mathcal{O}(M + N)$$

    - The space complexity of the Union-Find data structure is $$\mathcal{O}(M)$$.

    - In the main algorithm, we use a hash table to keep track of the count for each group. In the worst case, each number forms an individual group. Therefore, the space complexity of this hash table is $$\mathcal{O}(N)$$.

    - In addition, we keep a map between each number and one of its prime factors. Hence the space complexity of this map is $$\mathcal{O}(N)$$.

    - To sum up, the overall space complexity of the algorithm is $$\mathcal{O}(M) + \mathcal{O}(N) + \mathcal{O}(N) = \mathcal{O}(M + N)$$.

<br/>

---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Union find solution, explained
- Author: DBabichev
- Creation Date: Sun Aug 30 2020 17:43:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 30 2020 17:43:18 GMT+0800 (Singapore Standard Time)

<p>
If you try to run usual dfs/bfs on this problem, you will get TLE, so we need to think something smarter. Note, that if two numbers has common factor more than `1`, it means that they have **prime** common factor. So, let us for example consider all even numbers: they all have prime common factor `2`, so we can immediatly say, that they should be in the same group. Consider all numbers which are divisible by `3`, they also can be put in the same group. There is special data stucture: Disjoint set (https://en.wikipedia.org/wiki/Disjoint-set_data_structure), which is very suitable for these type of problems.

Let us consider example `[2,3,6,7,4,12,21,39]` an go through my code:

1. `primes_set(self,n)` return set of unique prive divisors of number `n`, for example for `n = 12` it returns set `[2,3]` and for `n=39` it returns set `[3,13]`.
2. Now, what is `primes` defaultdict? For each found prime, we put indexes of numbers from `A`, which are divisible by this prime. For our example we have: `2: [0,2,4,5], 3:[1,2,5,6,7], 7:[3,6], 13:[7]`. So, what we need to do now? We need to union `0-2-4-5`, `1-2-5-6-7` and `3-6`.
3. That exaclty what we do on the next step: iterate over our `primes` and create connections with `UF.union(indexes[i], indexes[i+1])`
4. Finally, when we made all connections, we need to find the biggest group: so we find parent for each number and find the bigges frequency.

**Complexity**: this is interesting part. Let `n` be length of `A` and let `m` be the biggest possible number. Function `primes_set` is quite heavy: for each number we need to check all numbers before its square root (if number is prime we can not do better with this approach), so complexity of `primes_set` is `O(sqrt(m))`. We apply this function `n` times, so overall complexity of this function will be `O(n*sqrt(m))`. We also have other parts of algorithm, where we put prime numbers to dictionary, complexity will be `O(n*log(m))` at most, because each number has no more, than `log(m)` different prime divisors. We also have the same number of `union()` calls, which will work as `O(1)` if we use it with ranks, and slower if we use it without ranks as I did, but still fast enough to be better than our bottleneck: `O(n*sqrt(m))` complexity for prime factors generation. That is why I use Union Find without ranks: if you want to improve algorighm, you need to deal with `primes_set()` function.
Space complexity is `O(n*log(m))` to keep all prime factors and to keep our `DSU` data structure.

```
class DSU:
    def __init__(self, N):
        self.p = list(range(N))

    def find(self, x):
        if self.p[x] != x:
            self.p[x] = self.find(self.p[x])
        return self.p[x]

    def union(self, x, y):
        xr, yr = self.find(x), self.find(y)
        self.p[xr] = yr

class Solution:
    def primes_set(self,n):
        for i in range(2, int(math.sqrt(n))+1):
            if n % i == 0:
                return self.primes_set(n//i) | set([i])
        return set([n])

    def largestComponentSize(self, A):
        n = len(A)
        UF = DSU(n)
        primes = defaultdict(list)
        for i, num in enumerate(A):
            pr_set = self.primes_set(num)
            for q in pr_set: primes[q].append(i)

        for _, indexes in primes.items():
            for i in range(len(indexes)-1):
                UF.union(indexes[i], indexes[i+1])

        return max(Counter([UF.find(i) for i in range(n)]).values())
```

If you have any questions, feel free to ask. If you like solution and explanations, please **Upvote!**
</p>


### Simplest Solution (Union Find only) No Prime Calculation
- Author: optimisea
- Creation Date: Mon Dec 03 2018 06:09:22 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 03 2018 06:09:22 GMT+0800 (Singapore Standard Time)

<p>
Union Find template. The only additional stuff is one hashmap which is used to convert factor to the node index in A for union. 
HashMap: key is the factor, val is the index in A
Time complexity: O(N*sqrt(Max val of A[i]))
```
class Solution {
    class UF {
        int[] parent;
        int[] size;
        int max;
        public UF (int N){
            parent = new int[N];
            size = new int[N];
            max = 1;
            for (int i = 0; i < N; i++){
                parent[i] = i;
                size[i] = 1;
            }
        }
        public int find(int x){
            if (x == parent[x]){
                return x;
            }
            return parent[x] = find(parent[x]);
        }
        public void union(int x, int y){
            int rootX = find(x);
            int rootY = find(y);
            if (rootX != rootY){
                parent[rootX] = rootY;
                size[rootY] += size[rootX];
                max = Math.max(max, size[rootY]);
            }
        }
    }
    public int largestComponentSize(int[] A) {
        int N = A.length;
        Map<Integer, Integer> map = new HashMap<>();// key is the factor, val is the node index
        UF uf = new UF(N);
        for (int i = 0; i < N; i++){
            int a = A[i];
            for (int j = 2; j * j <= a; j++){
                if (a % j == 0){
                    if (!map.containsKey(j)){//this means that no index has claimed the factor yet
                        map.put(j, i);
                    }else{//this means that one index already claimed, so union that one with current
                        uf.union(i, map.get(j));
                    }
                    if (!map.containsKey(a/j)){
                        map.put(a/j, i);
                    }else{
                        uf.union(i, map.get(a/j));
                    }
                }
            }
            if (!map.containsKey(a)){//a could be factor too. Don\'t miss this
                map.put(a, i);
            }else{
                uf.union(i, map.get(a));
            }
        }
        return uf.max;
    }
}
```
</p>


### C++ 232ms, Union-Find Solution
- Author: LacticAcid
- Creation Date: Mon Dec 03 2018 10:00:21 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 03 2018 10:00:21 GMT+0800 (Singapore Standard Time)

<p>
[reference](https://www.youtube.com/watch?v=GTX0kw63Tn0)
### Intuition

(1) Union each number with all its factor;
(2) Count the most frequent parent.

```
// W: the largest number
// Time Complexity: O(n*sqrt(W))
// Space Complexity: O(W)

class UnionFindSet {
public:
    UnionFindSet(int n) : _parent(n) {
        for (int i=0; i<n; i++) {
            _parent[i] = i;
        }
    }
    
    void Union(int x, int y) {
        _parent[Find(x)] = _parent[Find(y)];
    }
    
    int Find(int x) {
        if (_parent[x] != x) {
            _parent[x] = Find(_parent[x]);
        }
        return _parent[x];
    }
    
private:
    vector<int> _parent;
};

class Solution {
public:
    int largestComponentSize(vector<int>& A) {
        int n = *max_element(A.begin(), A.end());
        UnionFindSet ufs(n+1);
        for (int &a : A) {
            for (int k = 2; k <= sqrt(a); k++) {
                if (a % k == 0) {
                    ufs.Union(a, k);
                    ufs.Union(a, a / k);
                }
            }
        }
        
        unordered_map<int, int> mp;
        int ans = 1;
        for (int &a : A) {
            ans = max(ans, ++mp[ufs.Find(a)]);
        }
        return ans;
    }
};
```
</p>


