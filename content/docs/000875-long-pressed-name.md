---
title: "Long Pressed Name"
weight: 875
#id: "long-pressed-name"
---
## Description
<div class="description">
<p>Your friend is typing his <code>name</code>&nbsp;into a keyboard.&nbsp; Sometimes, when typing a character <code>c</code>, the key might get <em>long pressed</em>, and the character will be typed 1 or more times.</p>

<p>You examine the <code>typed</code>&nbsp;characters of the keyboard.&nbsp; Return <code>True</code> if it is possible that it was your friends name, with some characters (possibly none) being long pressed.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> name = &quot;alex&quot;, typed = &quot;aaleex&quot;
<strong>Output:</strong> true
<strong>Explanation: </strong>&#39;a&#39; and &#39;e&#39; in &#39;alex&#39; were long pressed.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> name = &quot;saeed&quot;, typed = &quot;ssaaedd&quot;
<strong>Output:</strong> false
<strong>Explanation: </strong>&#39;e&#39; must have been pressed twice, but it wasn&#39;t in the typed output.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> name = &quot;leelee&quot;, typed = &quot;lleeelee&quot;
<strong>Output:</strong> true
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> name = &quot;laiden&quot;, typed = &quot;laiden&quot;
<strong>Output:</strong> true
<strong>Explanation: </strong>It&#39;s not necessary to long press any character.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= name.length &lt;= 1000</code></li>
	<li><code>1 &lt;= typed.length &lt;= 1000</code></li>
	<li>The characters of <code>name</code> and <code>typed</code> are lowercase letters.</li>
</ul>

</div>

## Tags
- Two Pointers (two-pointers)
- String (string)

## Companies
- Google - 5 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Group into Blocks

**Intuition and Algorithm**

For a string like `S = 'aabbbbccc'`, we can group it into blocks `groupify(S) = [('a', 2), ('b', 4), ('c', 3)]`, that consist of a *key* `'abc'` and a *count* `[2, 4, 3]`.

Then, the necessary and sufficient condition for `typed` to be a long-pressed version of `name` is that the keys are the same, and each entry of the count of `typed` is at least the entry for the count of `name`.

For example, `'aaleex'` is a long-pressed version of `'alex'`: because when considering the groups `[('a', 2), ('l', 1), ('e', 2), ('x', 1)]` and `[('a', 1), ('l', 1), ('e', 1), ('x', 1)]`, they both have the key `'alex'`, and the count `[2,1,2,1]` is at least `[1,1,1,1]` when making an element-by-element comparison `(2 >= 1, 1 >= 1, 2 >= 1, 1 >= 1)`.

<iframe src="https://leetcode.com/playground/3Ftsk2eB/shared" frameBorder="0" width="100%" height="500" name="3Ftsk2eB"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N+T)$$, where $$N, T$$ are the lengths of `name` and `typed`.

* Space Complexity:  $$O(N+T)$$.


---
#### Approach 2: Two Pointer

**Intuition**

As in *Approach 1*, we want to check the key and the count.  We can do this on the fly.

Suppose we read through the characters `name`, and eventually it doesn't match `typed`.

There are some cases for when we are allowed to skip characters of `typed`. Let's use a tuple to denote the case (`name`, `typed`):

* In a case like `('aab', 'aaaaab')`, we can skip the 3rd, 4th, and 5th `'a'` in `typed` because we have already processed an `'a'` in this block.

* In a case like `('a', 'b')`, we can't skip the 1st `'b'` in `typed` because we haven't processed anything in the current block yet.

**Algorithm**

This leads to the following algorithm:

* First, we run a loop to move the two pointers along the strings, until we reach the end of either string.

    * For each character in `name`, if there's a match with the next character in `typed`, we advance both pointers.

    * If they are a mismatch, and it's the first character of the block in `typed`, the answer is `False`.

    * Else, discard all similar characters of `typed` coming up.  The next (different) character coming must match.

* At the end of the loop, we would end up three cases:

    * case 1). if there is still some characters left unmatched in the `name` string, then we don't have a match.

    * case 2). if there is still some characters left in the `typed` string, and all the remaining characters are resulted from the long press, then we still have a match.

    * case 3). otherwise, if any of the remaining characters in the `typed` string is not redundant, then we don't have a match.

<iframe src="https://leetcode.com/playground/R5hsv7ug/shared" frameBorder="0" width="100%" height="500" name="R5hsv7ug"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N+T)$$, where $$N, T$$ are the lengths of `name` and `typed`.

* Space Complexity:

    - For the Python solution, we have $$O(1)$$ space complexity.

    - For the Java solution, we need additional space due to the conversion made by `.toCharArray()` function. Hence, the space complexity would be $$O(N+T)$$.

---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Two Pointers
- Author: lee215
- Creation Date: Sun Oct 21 2018 13:25:15 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 21:56:46 GMT+0800 (Singapore Standard Time)

<p>
**C++:**
```
    bool isLongPressedName(string name, string typed) {
        int i = 0, m = name.length(), n = typed.length();
        for (int j = 0; j < n; ++j)
            if (i < m && name[i] == typed[j])
                ++i;
            else if (!j || typed[j] != typed[j - 1])
                return false;
        return i == m;
    }
```

**Java:**
```
    public boolean isLongPressedName(String name, String typed) {
        int i = 0, m = name.length(), n = typed.length();
        for (int j = 0; j < n; ++j)
            if (i < m && name.charAt(i) == typed.charAt(j))
                ++i;
            else if (j == 0 || typed.charAt(j) != typed.charAt(j - 1))
                return false;
        return i == m;
    }
```
**Python:**
```
    def isLongPressedName(self, name, typed):
        i = 0
        for j in range(len(typed)):
            if i < len(name) and name[i] == typed[j]:
                i += 1
            elif j == 0 or typed[j] != typed[j - 1]:
                return False
        return i == len(name)
```

</p>


### Java one pass solution 4ms with O(1) extra space
- Author: Olsh
- Creation Date: Mon Oct 22 2018 20:31:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 11:42:40 GMT+0800 (Singapore Standard Time)

<p>
The idea evert letter in **typed** should be the same as **name**, but "shifted" (or spreaded to be more exact) to some number of positions.
That\'s why, we go through **typed** and trying to identify if we have a corrponding letter in **name**. At the same time, we calculate the difference in positions of these corresponding letters in **typed** and **name**. In other words, difference identicates how many "additional" letters contains **typed**. For example:
name: **AABCD**
typed: **AAAABBCDDDDDD**

At the beginning difference is 0.
We go through **AAAABBCDDDDDD**:
Compare first letter of **typed** and **name**:
![image](https://assets.leetcode.com/users/olsh/image_1540209344.png)


The are equal -> all is ok, go further, difference remains unchanged
![image](https://assets.leetcode.com/users/olsh/image_1540209466.png)


Again equal -> go next:
![image](https://assets.leetcode.com/users/olsh/image_1540210445.png)


Not equal, but we can link **A** (**typed**)  to  **A** (**name**) from the previous step. As a result, our difference was incremented:
![image](https://assets.leetcode.com/users/olsh/image_1540210721.png)


The same will do next: **A** (**typed**) != **B** (**name**), but we can link **A** (**typed**) to **A** (**name**) from the previous step -> the difference was incremented again:
![image](https://assets.leetcode.com/users/olsh/image_1540210926.png)



At the end of all these manipulations, we see the following:
![image](https://assets.leetcode.com/users/olsh/image_1540208693.png)
The whole implementation:
```
class Solution {
 public boolean isLongPressedName(String name, String typed) {
  int difference = 0;
  for (int i = 0; i < typed.length();) {
	//letters are equal -> go next
   if (difference <= i && i - difference < name.length() && typed.charAt(i) == name.charAt(i - difference)) {
    i++;
   } 
	 // letters are not equal,  but we can link typed letter to name letter from the previous iteration
	 else if (difference < i && i - difference - 1 < name.length() && typed.charAt(i) == name.charAt(i - difference - 1)) {
    difference++;
   } else return false;
  }
	// check that at the end of name there\'s no odd symbols
    return typed.length() - difference == name.length();
 }
}
```
</p>


### C++ 2 lines accepted and 5 lines accurate
- Author: votrubac
- Creation Date: Sun Oct 21 2018 11:50:39 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 02:46:38 GMT+0800 (Singapore Standard Time)

<p>
This very naive solution got accepted. It fails of course for any test case where ```typed``` contains extra letters.
```
bool isLongPressedName(string name, string typed, int i = 0) {
    for (auto c : typed) i += name[i] == c;
    return i == name.size();
}
```
Here is a bit more lengthy but accurate solution:
```
bool isLongPressedName(string name, string typed, int i = 0, int j = 0) {
    while (j < typed.size()) {
        if (i < name.size() && name[i] == typed[j]) ++i, ++j;
        else if (i > 0 && name[i - 1] == typed[j]) ++j;
        else return false;
    }
    return i == name.size();
}
```
</p>


