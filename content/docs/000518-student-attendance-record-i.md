---
title: "Student Attendance Record I"
weight: 518
#id: "student-attendance-record-i"
---
## Description
<div class="description">
You are given a string representing an attendance record for a student. The record only contains the following three characters:

<p>
<ol>
<li><b>'A'</b> : Absent. </li>
<li><b>'L'</b> : Late.</li>
<li> <b>'P'</b> : Present. </li>
</ol>
</p>

<p>
A student could be rewarded if his attendance record doesn't contain <b>more than one 'A' (absent)</b> or <b>more than two continuous 'L' (late)</b>.    </p>

<p>You need to return whether the student could be rewarded according to his attendance record.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> "PPALLP"
<b>Output:</b> True
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> "PPALLL"
<b>Output:</b> False
</pre>
</p>



</div>

## Tags
- String (string)

## Companies
- Google - 5 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Simple Solution [Accepted]

One simple way of solving this problem is to count number of $$A's$$ in the string and check whether the string $$LLL$$ is a substring of a given string. If number of $$A's$$ is less than $$2$$ and $$LLL$$ is not a subtring of a given string then return $$true$$, otherwise return $$false$$.

$$indexOf$$ method can be used to check substring in a string. It return the index within this string of the first occurrence of the specified character or -1, if the character does not occur.


<iframe src="https://leetcode.com/playground/KwRnWGmW/shared" frameBorder="0" name="KwRnWGmW" width="100%" height="241"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Single loop and indexOf method takes $$O(n)$$ time.
* Space complexity : $$O(1)$$. Constant space is used.

---
#### Approach #2 Better Solution [Accepted]

**Algorithm**

One optimization of above method is to break the loop when count of $$A's$$ becomes $$2$$.

<iframe src="https://leetcode.com/playground/Eh3SunKt/shared" frameBorder="0" name="Eh3SunKt" width="100%" height="224"></iframe>

**Complexity Analysis**


* Time complexity : $$O(n)$$. Single loop and indexOf method takes $$O(n)$$ time.
* Space complexity : $$O(1)$$. Constant space is used.

---
#### Approach #3 Single pass Solution (Without indexOf method) [Accepted]

**Algorithm**

We can solve this problem in a single pass without using indexOf method. In a single loop we can count number of $$A's$$ and also check the substring $$LLL$$ in a given string.

<iframe src="https://leetcode.com/playground/sZFGNHqX/shared" frameBorder="0" name="sZFGNHqX" width="100%" height="275"></iframe>
**Complexity Analysis**


* Time complexity : $$O(n)$$. Single loop upto string length is used.
* Space complexity : $$O(1)$$. Constant space is used.

---
#### Approach #4 Using Regex [Accepted]

**Algorithm**

One interesting solution is to use regex to match the string. Java provides the java.util.regex package for pattern matching with regular expressions. A regular expression is a special sequence of characters that helps you match or find other strings or sets of strings, using a specialized syntax held in a pattern.

Following are the regex's used in this solution:
```
. : Matches any single character except newline.

* : Matches 0 or more occurrences of the preceding expression.

.* : Matches any string

a|b : Matches either a or b
```
$$matches$$ method is used to check whether or not the string matches the given regular expression.

Regular Expression of the string containing two or more than two $$A's$$ will be $$.*A.*A.*$$ and the regular expression of the string containing substring $$LLL$$ will be $$.*LLL.*$$. We can merge this two regex using $$|$$ and form a regex of string containing either more than one $$A$$ or containing substring $$LLL$$. Then regex will look like:  $$.*(A.*A|LLL).*$$. We will return true only when the string doesn't matches this regex.


<iframe src="https://leetcode.com/playground/5J2tfTuD/shared" frameBorder="0" name="5J2tfTuD" width="100%" height="156"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. $$matches$$ method takes $$O(n)$$ time.

* Space complexity : $$O(1)$$. No Extra Space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 1-liner
- Author: compton_scatter
- Creation Date: Sun Apr 16 2017 11:00:51 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 21:33:49 GMT+0800 (Singapore Standard Time)

<p>
```
public boolean checkRecord(String s) {
    return !s.matches(".*LLL.*|.*A.*A.*");
}
```
</p>


### C++ very simple solution
- Author: MichaelZ
- Creation Date: Mon Apr 17 2017 05:32:42 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 21:33:58 GMT+0800 (Singapore Standard Time)

<p>
    bool checkRecord(string s) {
        int a=0, l=0;
        for(int i=0;i<s.size();i++) {
            if(s[i]=='A') a++;
            if(s[i]=='L') l++;
            else l=0;
            if(a>=2||l>2) return false;
        }
        return true;
    }
</p>


### Java Simple without Regex 3 lines
- Author: kisto4
- Creation Date: Mon Apr 17 2017 01:37:11 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 21:34:07 GMT+0800 (Singapore Standard Time)

<p>

Simple by using Java String Functions - 
```
public class Solution {
    public boolean checkRecord(String s) {
        if(s.indexOf("A") != s.lastIndexOf("A") || s.contains("LLL"))
            return false;
        return true;
    }
}
```
</p>


