---
title: "Minimize Rounding Error to Meet Target"
weight: 962
#id: "minimize-rounding-error-to-meet-target"
---
## Description
<div class="description">
<p>Given an array of <code>prices</code> <code>[p<sub>1</sub>,p<sub>2</sub>...,p<sub>n</sub>]</code> and a <code>target</code>, round each price <code>p<sub>i</sub></code> to <code>Round<sub>i</sub>(p<sub>i</sub>)</code> so that the rounded array <code>[Round<sub>1</sub>(p<sub>1</sub>),Round<sub>2</sub>(p<sub>2</sub>)...,Round<sub>n</sub>(p<sub>n</sub>)]</code> sums to the given <code>target</code>. Each operation <code>Round<sub>i</sub>(p<sub>i</sub>)</code> could be either <code>Floor(p<sub>i</sub>)</code> or <code>Ceil(p<sub>i</sub>)</code>.</p>

<p>Return the string <code>&quot;-1&quot;</code> if the rounded array is impossible to sum to <code>target</code>. Otherwise, return the smallest rounding error, which is defined as <code>&Sigma; |Round<sub>i</sub>(p<sub>i</sub>) - (p<sub>i</sub>)|</code> for <italic><code>i</code></italic> from <code>1</code> to <italic><code>n</code></italic>, as a string with three places after the decimal.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> prices = [&quot;0.700&quot;,&quot;2.800&quot;,&quot;4.900&quot;], target = 8
<strong>Output:</strong> &quot;1.000&quot;
<strong>Explanation:</strong>
Use Floor, Ceil and Ceil operations to get (0.7 - 0) + (3 - 2.8) + (5 - 4.9) = 0.7 + 0.2 + 0.1 = 1.0 .
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> prices = [&quot;1.500&quot;,&quot;2.500&quot;,&quot;3.500&quot;], target = 10
<strong>Output:</strong> &quot;-1&quot;
<strong>Explanation:</strong> It is impossible to meet the target.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> prices = [&quot;1.500&quot;,&quot;2.500&quot;,&quot;3.500&quot;], target = 9
<strong>Output:</strong> &quot;1.500&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= prices.length &lt;= 500</code></li>
	<li>Each string&nbsp;<code>prices[i]</code> represents a real number in the range <code>[0.0, 1000.0]</code> and has exactly 3 decimal places.</li>
	<li><code>0 &lt;= target &lt;= 10<sup>6</sup></code></li>
</ul>

</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)
- Greedy (greedy)

## Companies
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Clean Java Solution using PriorityQueue
- Author: twu54
- Creation Date: Thu Jul 18 2019 06:12:49 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 18 2019 06:12:49 GMT+0800 (Singapore Standard Time)

<p>
In order to meet a target by rounding, we can first floor every number and check out if sum is within range **[target-length, target]**. If so, we can switch the numbers from floored to ceiled one by one until we reach the target.

In order to **minimize** the rounding error, we can determine the optimal order to switch the elements. Ideally, the closer the value to ceiling, the sooner it should be switched.

Note integer representation\'s floor and ceil are the same, so don\'t have a choice to switch.


```
class Solution {
    public String minimizeError(String[] prices, int target) {
        float res = 0;
        PriorityQueue<Double> diffHeap = new PriorityQueue<>();
        
        for (String s : prices) {
            float f = Float.valueOf(s);
            double low = Math.floor(f);
            double high = Math.ceil(f);
            
            if (low != high)
                diffHeap.offer((high-f)-(f-low));
            
            res += f-low;
            target -= low;
        }
        
        if (target < 0 || target > diffHeap.size())
            return "-1";
        
        while (target-- > 0)
            res += diffHeap.poll();
        
        return String.format("%.3f", res);
    }
}
```


</p>


### Greedy Python.
- Author: attixZhang
- Creation Date: Sun Jun 02 2019 08:37:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 02 2019 08:38:24 GMT+0800 (Singapore Standard Time)

<p>
For each number, we have two choice: floor or ceil.
So the minimum sum we could get is sum each floored price.
And the maximum sum we could get is sum each ceiled price.
If target is larger or equal than the minimum one and small or equal to the maximum one, we could get such value. Otherwise we need to return a "-1".

If we can get target, we can do floor operation to each value first. 
Now we get a minimum value.
To get the target, we need select (target - minimum) numbers of price and do ceil operation on it instead of floor operation so that we can get target.(ceil(p) - floor(p) = 1)
We want to minimum the total change (which is sum of the abs change by doing operation(floor | ceil) to each price). 
So we can:
1. Calculate the change made by floor operation and ceil operation for each price. 
2. Calculate the reduce in the change by filp the floor operation to ceil operation.
3. Sort them in des order.
4. For the first (target - minimum) changes, we filp floor operation to ceil.(As we gain more reduces to the total changes by doing ceil operation to these prices.)
5. For the rest changes, we select floor operation.
6. Return the sum of all changes.
```
class Solution:
    def minimizeError(self, prices: List[str], target: int) -> str:
        diff = []
        low, high = 0, 0
        for i, p in enumerate(map(float, prices)):
            f, c = math.floor(p), math.ceil(p)
            low, high = low + f, high + c
            fDiff, cDiff = p - f, c - p
            diff.append((fDiff - cDiff, fDiff, cDiff))
        if not low <= target <= high:
            return "-1"
        ceilN = target - low
        diff = sorted(diff, reverse=True)
        return "{:.3f}".format(sum([d[2] for d in diff[:ceilN]]) + sum([d[1] for d in diff[ceilN:]]))
```
</p>


### Greedy Python with different explanation
- Author: dridon
- Creation Date: Sun Sep 15 2019 09:48:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 15 2019 09:48:51 GMT+0800 (Singapore Standard Time)

<p>
My solution is taken from mud2man and attixZhang. It took me some time to understand what they said so I wanted to try and break it down a little further. I want to offer my explanation to help others that may find themselves having trouble. 

I break this problem down in two three parts: 

1. Determine possibility of a solution  
2. Determine operations to reach target
3. Minimize Error 

The first one is straightforward, the other two take a little thinking. Lets talk about each step. 
# Determine possibility of a Solution 
Take the following two values: 
```python
minSum = sum(floor(p) for p in prices) 
maxSum = sum(ceil(p) for p in prices) 
```
These are the minimum and maximum possible summations possible with `Round(p)`. The former occurs by summing all the floored prices and the latter from summing all the ceiled prices. Now if target falls between these two values the solution is possible: 

```python
if minV <= target <= maxV: 
   #solution possible
else: 
    return "-1"
```

In fact, not only is the solution possible. It is guaranteed. To understand, let us take a look at the second part. 

# Determine operations to reach target
So first remember `minV`, `maxV` and `target` are integer values. So the difference between any of these is an integer value. Particularly the following: 
```python
K = target - minV
```
This is the number of ceiling operations we must conduct. To understand, let us imagine the prices array: 
```python
prices = [ p1, p2, p3, ..., pN]
```
Now for a `pi` label its `floor(pi)` as `fpi`.  This gives us the following array: 
```python
floored_prices = [ fp1, fp2, fp3, ..., fpN ] 
```

Remember, that `minV = sum(floored_prices)`. So basically if we could change `floored_prices` such that we have `K` added to `minV` then we a sum to the target. One way to have this would be to add `1` to any `K` elements in `floored_prices`: 
```python
floored_prices_plus_K = [ fp1+1, ..., fK+1, ..., fpN ]
```
We achieve this by ceiling the first K (or really any K) values instead of flooring them when generating the list. Remember `ceil(p) == floor(p) + 1`: 
```python
floored_prices_plus_K = [ ceil(p1), ..., ceil(pK),floor(p(K+1)), ..., floor(pN) ] 
```

This array will give us `minV + K == target`. But we are not at the answer yet. This is where the "sorting" or "k smallest errors" come in to play.

# Minimize Error 
Since we know that any `K` elements ceiled instead of floored will give us the summation of the target we only need to pick the ones that will give us the lowest error. This is not difficult. Lets call the flooring error `floorError(p) -> p - floor(p)`.  Now map prices using this error: 
```python 
floorErrors = [ floorError(p1], floorError(p2), ... floorError(pN) ] 
```

Now sort it and take the largest `K` errors. If we use the ceil error `ceilError(p) -> ceil(p) - p` instead of `floorError` for these values and sum all these values, get the minimum error. The intuition is easily understood by taking a look at two actual numbers. Take `3.6` and `5.7` (arbitrarily generated). The floor errors are `0.6` and `0.7` respectively but their ceil errors are `0.4` and `0.3` respectively. So the prices with the biggest `floorErrror` will have the smallest `ceilError` (unless all errors are 0.5). This is actually fairly easy to prove mathematically: 

```python 
0.0 <= fE1, fE2 <= 1.0  # floor errors 
fE1 > fE2
1.0 - fE1 < 1.0 - fE2
# but 1.0 - fE1 = cE1 (the ceil error)
cE1 < cE2
``` 

A bit pedantic but hopefully it helps those who don\'t get the other solutions right away: 
```python
class Solution:
    def minimizeError(self, prices: List[str], target: int) -> str:
        maxV, minV, errors = 0,0, [] 
        
        # gather minV, maxV and errors  
        for p in prices: 
            fp = float(p)
            f, c = math.floor(fp), math.ceil(fp)
            minV, maxV = minV + f, maxV + c 
            fError, cError = fp - f, c - fp 
            errors.append((fError, cError))
        
        # lets make sure this is actually possible 
        if target < minV or target > maxV: 
            return "-1"        
        
        # The number of prices that need to be rounded up (rest are rounded down)
        ceilCount = target - minV
        
        # Floor errors are enough to give us what we need 
        errors = sorted(errors, reverse=True)
        
        #min error 
        minError = sum(e[1] for e in errors[:ceilCount]) + sum(e[0] for e in errors[ceilCount:])
        
        # return the error
        return "{:.3f}".format(minError)
```
</p>


