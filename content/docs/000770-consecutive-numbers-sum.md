---
title: "Consecutive Numbers Sum"
weight: 770
#id: "consecutive-numbers-sum"
---
## Description
<div class="description">
<p>Given a positive integer&nbsp;<code>N</code>, how many ways can we write it as a sum of&nbsp;consecutive positive integers?</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>5
<strong>Output: </strong>2
<strong>Explanation: </strong>5 = 5 = 2 + 3</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>9
<strong>Output: </strong>3
<strong>Explanation: </strong>9 = 9 = 4 + 5 = 2 + 3 + 4</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>15
<strong>Output: </strong>4
<strong>Explanation: </strong>15 = 15 = 8 + 7 = 4 + 5 + 6 = 1 + 2 + 3 + 4 + 5</pre>

<p><strong>Note:</strong>&nbsp;<code>1 &lt;= N &lt;= 10 ^ 9</code>.</p>

</div>

## Tags
- Math (math)

## Companies
- Citadel - 68 (taggedByAdmin: false)
- Citrix - 5 (taggedByAdmin: false)
- Roblox - 5 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Visa - 16 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---

#### Overview

**Find Divisors / Integer Factorization Problem**

$$N$$ is a sum of $$k$$ consecutive numbers, i.e. 
$$N = (x + 1) + (x + 2) + ... + (x + k)$$. 

![simple](../Figures/829/solution2.png) 

Let's regroup the terms $$N = x k + (1 + 2 + ... + k)$$ and 
use well-known [formula for the sum of natural numbers](https://en.wikipedia.org/wiki/1_%2B_2_%2B_3_%2B_4_%2B_%E2%8B%AF)
$$1 + 2 + ... + k = \frac{k(k + 1)}{2}$$:

$$
N = x k + \frac{k (k + 1)}{2} \qquad (1)
$$

$$
2N = k (2x + k + 1) \qquad (2)
$$

That means, the problem is to figure out all possible pairs of $$k$$
and $$2x + k + 1$$ which are both _divisors_ of a number $$2N$$.
To find a number of ways to factor $$2N$$ is the so-called 
[Integer Factorisation](https://en.wikipedia.org/wiki/Integer_factorization) problem. 

> The best way to solve this problem is to run 
[Shor's Algorithm](https://en.wikipedia.org/wiki/Shor's_algorithm)
on the quantum computer, that requires $$\mathcal{O}(\log^2 N \log \log N 
\log \log \log N)$$ time. If there is no quantum computer in the interview
room, just use classical [GNFS Algorithm](https://en.wikipedia.org/wiki/General_number_field_sieve),
which runs in a decent $$\mathcal{O}(e^{\log^{1/3} N (\log \log N)^{2/3}})$$ time. 

Speaking seriously, one has to stay reasonable
during the interview and figure out the right balance between speed and simplicity.

**Time Complexity to Target During the Interview: $$\mathcal{O}(\sqrt{N})$$**

First, one could iterate from $$1$$ to $$N$$ and figure out
all divisors in a linear time.

![simple](../Figures/829/linear_time_divisors2.png) 

Second, the divisors are paired, i.e. if number $$i$$ is a divisor of $$N$$, then
number $$N / i$$ is a divisor of $$N$$ too. That means it's enough to iterate
up to $$\sqrt{N}$$.

![simple](../Figures/829/sqrt_divisors2.png) 

$$\mathcal{O}(\sqrt{N})$$ is definitely enough for the interview. 
In this article, we consider two simple solutions, 
which are good suite for the interview and keep the rest out of scope.
<br />
<br />


---
#### Approach 1: Mathematical: Sum of First $$k$$ Natural Numbers, $$\mathcal{O}(\sqrt{N})$$ time

**Intuition**

Let's use the formula we derived above

$$
N = x k + \frac{k (k + 1)}{2} \qquad (1)
$$

derive $$x$$

$$
x = \frac{N}{k} - \frac{k + 1}{2}  \qquad (3)
$$

and set two constraints:

- $$x$$ should be greater or equal to zero.

- $$x$$ should be an integer.

The first constraint is easy to apply using [completing the square technique](https://en.wikipedia.org/wiki/Completing_the_square)

$$
\frac{N}{k} \ge \frac{k + 1}{2}  \qquad (4)
$$

$$
2N + \frac{1}{4} \ge \left(k + \frac{1}{2}\right)^2  \qquad (5)
$$

$$
k \le \sqrt{2N + \frac{1}{4}} - \frac{1}{2} \qquad (6)
$$

The first constraint sets the upper boundary for $$k$$. 
The second constraint, $$x$$ should be an integer, can be verified during 
the iteration over $$k$$.

**Algorithm**

- Initiate the counter to zero.

- Iterate over $$k$$ from $$1$$ to $$\sqrt{2N + \frac{1}{4}} - \frac{1}{2}$$.

- If $$x = \frac{N}{k} - \frac{k + 1}{2}$$ is an integer, increase the counter by one.

- Return the counter.

**Implementation**

<iframe src="https://leetcode.com/playground/SZc6ZayZ/shared" frameBorder="0" width="100%" height="310" name="SZc6ZayZ"></iframe>

**Complexity Analysis**

* Time Complexity: $$\mathcal{O}(\sqrt{N})$$.

* Space Complexity: $$O(1)$$.
<br />
<br />


---
#### Approach 2: Mathematical: Decrease $$N$$ Gradually, $$\mathcal{O}(\sqrt{N})$$ time

**Intuition**

Now let's optimize Approach 1. 
The idea is to use the same iteration limits for $$k$$, 
and to decrease $$N$$ by $$k$$ at each step. 

![simple](../Figures/829/approach2.png) 

At each step, we check if $$N$$ can be composed by the sum of $$k$$ 
consecutive numbers, _i.e._ $$N = (x + 1) + (x + 2) + ... + (x + k) 
= xk + (1 + 2 + ... + k)$$.

By removing all the complementary terms (1, 2, ... k) one by one, 
we reduce the number $$N$$ from 
$$xk + (1 + 2 + ... + k)$$ down to $$N = x k$$.
Since $$x$$ should be an integer, $$k$$ should be a divisor of $$N$$, 
_i.e._ `N % k == 0`.

**Algorithm**

- Initiate the counter to zero.

- Iterate over $$k$$ from $$1$$ to $$\sqrt{2N + \frac{1}{4}} - \frac{1}{2}$$.

- Decrease $$N$$ by $$k$$, to keep $$x k$$ term only.

- Verify that $$x$$ is an integer: N % k == 0. If it's the case, increase
the counter by one.

- Return the counter.

**Implementation**

<iframe src="https://leetcode.com/playground/GD84TnYs/shared" frameBorder="0" width="100%" height="259" name="GD84TnYs"></iframe>

**Complexity Analysis**

* Time Complexity: $$\mathcal{O}(\sqrt{N})$$.

* Space Complexity: $$O(1)$$.
<br />
<br />


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 5 lines C++ solution with detailed mathematical explanation.
- Author: pavan5
- Creation Date: Sun May 06 2018 12:56:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 08:59:23 GMT+0800 (Singapore Standard Time)

<p>
The thought process goes like this- Given a number `N`, we can *possibly* write it as a sum of 2 numbers, 3 numbers, 4 numbers and so on. Let\'s assume the fist number in this series be `x`. Hence, we should have 
`x + (x+1) + (x+2)+...+ k terms = N`
`kx + k*(k-1)/2 = N` implies
`kx  = N - k*(k-1)/2`
So, we can calculate the RHS for every value of `k` and if it is a multiple of `k` then we can construct a sum of `N` using `k` terms starting from `x`.
Now, the question arises, till what value of `k` should we loop for? That\'s easy. In the worst case, RHS should be greater than 0. That is 
` N - k*(k-1)/2 > 0` which implies
`k*(k-1) < 2N` which can be approximated to
`k*k < 2N ==> k < sqrt(2N)`
Hence the overall complexity of the algorithm is `O(sqrt(N))`

**PS**: OJ expects the answer to be 1 greater than the number of possible ways because it considers `N` as being written as `N` itself. It\'s confusing since they ask for sum of consecutive integers which implies atleast 2 numbers. But to please OJ, we should start `count` from 1.

```
class Solution {
public:
    int consecutiveNumbersSum(int N) {
        int count = 1;
        for( int k = 2; k < sqrt( 2 * N ); k++ ) {
            if ( ( N - ( k * ( k - 1 )/2) ) % k == 0) count++;
        }
        return count;
    }
};
```
</p>


### [ Java/Python 3] 5 liners O(N ^ 0.5) - Math method w/ explanation and analysis.
- Author: rock
- Creation Date: Sun May 06 2018 11:06:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 19 2019 00:27:36 GMT+0800 (Singapore Standard Time)

<p>
N can be expressed as `k, k + 1, k + 2, ..., k + (i - 1)`, where `k` is a positive integer; therefore 

`N = k * i + (i - 1) * i / 2 => 
N - (i - 1) * i / 2 = k * i`, which implies that as long as `N - (i - 1) * i / 2` is `k` times of `i`, we get a solution corresponding to `i`; Hence iteration of all possible values of `i`, starting from `1`, will cover all cases of the problem.  

```
    public int consecutiveNumbersSum(int N) {
        int ans = 0;
        for (int i = 1; i * (i - 1) / 2 < N; ++i)
            if ((N - i * (i - 1) / 2) % i == 0)
                ++ans;
        return ans;
    }
```
```
    def consecutiveNumbersSum(self, N: int) -> int:
        i, ans = 1, 0
        while N > i * (i - 1) // 2:
            if (N - i * (i - 1) // 2) % i == 0:
                ans += 1
            i += 1
        return ans
```
**Analysis**

Solve the equation `i * (i - 1) / 2 = N`, we know `i ~ N ^ 0.5`
Loop runs at most `N ^ 0.5` times, so

Time: O(N ^ 0.5), space: O(1)
</p>


### [Java/C++/Python] Fastest, Count Odd Factors, O(logN)
- Author: lee215
- Creation Date: Sun May 06 2018 11:02:58 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jan 14 2020 01:18:47 GMT+0800 (Singapore Standard Time)

<p>
## **Basic Math**
`N = (x+1) + (x+2) + ... + (x+k)`
`N = kx + k(k+1)/2`
`N * 2 = k(2x + k + 1)`,where `x >= 0, k >= 1`

Either `k` or `2x + k + 1` is odd.

It means that, for each odd factor of `N`,
we can find a consecutive numbers solution.

Now this problem is only about counting odd numbers!
I believe you can find many solutions to do this.
Like `O(sqrt(N))` solution used in all other post.

Here I shared my official solution.
<br>

## **Some Math about Counting Factors**
If `N = 3^a * 5^b * 7*c * 11*d ....`, the number of factors that `N` has equals
`N_factors = (a + 1) * (b + 1) * (c + 1) * (d + 1) .....`
<br>

## **Explanantion**:
1. Discard all factors 2 from `N`.
2. Check all odd number `i` if `N` is divided by `i`.
3. Calculate the `count` of factors `i` that `N` has.
4. Update `res *= count`.
5. If `N==1`, we have found all primes and just return res.
Otherwise, `N` will be equal to `P` and we should do `res += count + 1` where `count = 1`.
<br>

## **Complexity:**
To be more accurate, it\'s `O(biggest prime factor)`.
Because every time I find a odd factor, we do `N /= i`.
This help reduce `N` faster.

Assume `P` is the biggest prime factor of a **odd** number `N` .
If `N = 3^x * 5^y ...*  P`, Loop on `i` will stop at `P^0.5`
If `N = 3^x * 5^y ...*  P^z`, Loop on `i` will stop at `P`.
In the best case, `N = 3^x` and our solution is `O(log3N)`
In the worst case, `N = P^2` and our solution is `O(P) = O(N^0.5)`

Though in my solution, we didn\'t cache our process of finding odd factor.
Moreover, if we prepare all prime between `[3\uFF0C10^4.5]`.
it will be super faster because there are only 3400 primes in this range.
This complexity will be `O(P/logP)` with `P < sqrt(N)`
<br>


**Java**
```java
    public int consecutiveNumbersSum(int N) {
        int res = 1, count;
        while (N % 2 == 0) N /= 2;
        for (int i = 3; i * i <= N; i += 2) {
            count = 0;
            while (N % i == 0) {
                N /= i;
                count++;
            }
            res *= count + 1;
        }
        return N == 1 ? res : res * 2;
    }
```
**Short C++/Java:**
```cpp
        int res = 1, count;
        while (N % 2 == 0) N /= 2;
        for (int i = 3; i * i <= N; res *= count + 1, i += 2)
            for (count = 0; N % i == 0; N /= i, count++);
        return N == 1 ? res : res * 2;
```
**Python**
```py
    def consecutiveNumbersSum(self, N):
        res = 1
        i = 3
        while N % 2 == 0:
            N /= 2
        while i * i <= N:
            count = 0
            while N % i == 0:
                N /= i
                count += 1
            res *= count + 1
            i += 2
        return res if N == 1 else res * 2
```


</p>


