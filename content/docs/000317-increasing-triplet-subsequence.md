---
title: "Increasing Triplet Subsequence"
weight: 317
#id: "increasing-triplet-subsequence"
---
## Description
<div class="description">
<p>Given an unsorted array return whether an increasing subsequence of length 3 exists or not in the array.</p>

<p>Formally the function should:</p>

<blockquote>Return true if there exists <i>i, j, k </i><br />
such that <i>arr[i]</i> &lt; <i>arr[j]</i> &lt; <i>arr[k]</i> given 0 &le; <i>i</i> &lt; <i>j</i> &lt; <i>k</i> &le; <i>n</i>-1 else return false.</blockquote>

<p><strong>Note: </strong>Your algorithm should run in O(<i>n</i>) time complexity and O(<i>1</i>) space complexity.</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,2,3,4,5]</span>
<strong>Output: </strong><span id="example-output-1">true</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[5,4,3,2,1]</span>
<strong>Output: </strong><span id="example-output-2">false</span>
</pre>
</div>
</div>
</div>

## Tags


## Companies
- Facebook - 4 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Linear Scan

**Intuition**

The idea is to keep track of the first two numbers in increasing order and find the last number which will be bigger than the first two numbers. Here, the first and second smallest numbers can be updated with conditional checks while scanning `nums`.

```code    
first_num = second_num = some very big number
for n in nums:
    if n is smallest number:
        first_num = n
    else if n is second smallest number:
        second_num = n
    else n is bigger than both first_num and second_num:
        # We have found our triplet, return True

# After loop has terminated
# If we have reached this point, there is no increasing triplet, return False
```

Let's take a look at two cases where `nums` is sorted:
    
*  If `nums` is sorted in descending order, you will always end up in this first `if` block (and thus, repeatedly updating `first_num`). Finally, `False` will be returned after the loop has been terminated. 
* If `nums` is sorted in ascending order, you will first update `first_num` as soon as you see the first number in `nums` and when you encounter another number in `nums`, you will update the `second_nums` since this new number would be bigger than the value stored in `first_num`. After these two variables have been populated, all you need to look for is another number which is bigger than `first_num` and `second_num`. As soon as you find that number, the first `if` and the second `else if` blocks will be skipped, and you will end up in the last `else` block and `True` will be returned immediately.

This works not only for sorted cases described above but also for cases where the numbers are unsorted. First, find the smallest number and store it in `first_num`, and then find the second smallest number and store it in `second_num`. However, there is no guarantee that another number you encounter in `nums` will be greater than `first_num` and `second_num`. This new number can even be smaller than then `first_num` (in that case, you will have to update `first_num` with this new value) or `second_num` (in that case, you will have to update `second_num` with this new value). As long as you encounter those cases, you keep on updating your `first_num` and `second_num`. As soon as you encounter a number which is greater than **both** `first_num` and `second_num`, you have found your last number to complete the *increasing triplet subsequence*. At that point, you can immediately return `True`.

However, there is an important logic that is quite important to grasp. Let's take `nums` =  `[10,20,3,2,1,1,2,0,4]` for an example:

!?!../Documents/334_increasing_triplet_subsequence.json:1200,600!?!

In the above example, the point where you have reached `0` and subsequently updating `first_num = 0` can make you confused as you might think that this no longer is a subsequence (since `second_num = 2` comes before `first_num = 0`). Of course, since we have updated the `first_num`, if we want to return the actual subsequence, we might need to have another placeholder variable that will hold the previously recorded `first_num` before it is updated. However, for this problem, we are only looking for the *existence of* a valid increasing triplet subsequence, even though `first_num` and `second_num` are out of order, we don't need to worry about it.

To make this more illustrative, observe the following example.

`nums` = `[1,2,0,3] # should return True`

1. `first_num = 1`
2. `second_num = 2`
3. `first_num = 0`
4. `return True`

The increasing triplet subsequence is `1, 2, and 3`. Even though `first_num` is updated in `Line 3`, `second_num` is never updated again. However, you can tell that **there exists another number before `second_num` which is definitely BIGGER than the last updated `first_num` but SMALLER than `second_num`**. This is a very important observation. Therefore, you can safely say that there exists an increasing triplet subsequence as soon as you see a number which is bigger than the *last updated* `first_num` and `second_num` even though that last updated `first_num` is not one of the actual numbers of the increasing triplet subsequence. 

**Implementation**

<iframe src="https://leetcode.com/playground/Ftp28h5K/shared" frameBorder="0" width="100%" height="327" name="Ftp28h5K"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N)$$ where $$N$$ is the size of `nums`. We are updating `first_num` and `second_num` as we are scanning `nums`.

* Space complexity : $$O(1)$$ since we are not consuming additional space other than variables for two numbers.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Concise Java solution with comments.
- Author: sim5
- Creation Date: Wed Feb 17 2016 16:26:55 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 11:52:22 GMT+0800 (Singapore Standard Time)

<p>
       public boolean increasingTriplet(int[] nums) {
            // start with two largest values, as soon as we find a number bigger than both, while both have been updated, return true.
            int small = Integer.MAX_VALUE, big = Integer.MAX_VALUE;
            for (int n : nums) {
                if (n <= small) { small = n; } // update small if n is smaller than both
                else if (n <= big) { big = n; } // update big only if greater than small but smaller than big
                else return true; // return if you find a number bigger than both
            }
            return false;
        }
</p>


### Clean and short, with comments, C++
- Author: alveko
- Creation Date: Tue Feb 16 2016 12:01:18 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 22:55:11 GMT+0800 (Singapore Standard Time)

<p>
    bool increasingTriplet(vector<int>& nums) {
        int c1 = INT_MAX, c2 = INT_MAX;
        for (int x : nums) {
            if (x <= c1) {
                c1 = x;           // c1 is min seen so far (it's a candidate for 1st element)
            } else if (x <= c2) { // here when x > c1, i.e. x might be either c2 or c3
                c2 = x;           // x is better than the current c2, store it
            } else {              // here when we have/had c1 < c2 already and x > c2
                return true;      // the increasing subsequence of 3 elements exists
            }
        }
        return false;
    }
</p>


### Python Easy O(n) Solution
- Author: girikuncoro
- Creation Date: Sun Mar 13 2016 13:54:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 06 2018 18:29:11 GMT+0800 (Singapore Standard Time)

<p>
Start with the maximum numbers for the first and second element. Then:
(1) Find the first smallest number in the 3 subsequence
(2) Find the second one greater than the first element, reset the first one if it's smaller

    def increasingTriplet(nums):
        first = second = float('inf')
        for n in nums:
            if n <= first:
                first = n
            elif n <= second:
                second = n
            else:
                return True
        return False
</p>


