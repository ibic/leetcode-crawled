---
title: "Maximum Length of Pair Chain"
weight: 578
#id: "maximum-length-of-pair-chain"
---
## Description
<div class="description">
<p>
You are given <code>n</code> pairs of numbers. In every pair, the first number is always smaller than the second number.
</p>

<p>
Now, we define a pair <code>(c, d)</code> can follow another pair <code>(a, b)</code> if and only if <code>b < c</code>. Chain of pairs can be formed in this fashion. 
</p>

<p>
Given a set of pairs, find the length longest chain which can be formed. You needn't use up all the given pairs. You can select pairs in any order.
</p>


<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> [[1,2], [2,3], [3,4]]
<b>Output:</b> 2
<b>Explanation:</b> The longest chain is [1,2] -> [3,4]
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The number of given pairs will be in the range [1, 1000].</li>
</ol>
</p>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 2 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Dynamic Programming [Accepted]

**Intuition**

If a chain of length `k` ends at some `pairs[i]`, and `pairs[i][1] < pairs[j][0]`, we can extend this chain to a chain of length `k+1`.

**Algorithm**

Sort the pairs by first coordinate, and let `dp[i]` be the length of the longest chain ending at `pairs[i]`.  When `i < j` and `pairs[i][1] < pairs[j][0]`, we can extend the chain, and so we have the candidate answer `dp[j] = max(dp[j], dp[i] + 1)`.

<iframe src="https://leetcode.com/playground/cBD958Bm/shared" frameBorder="0" width="100%" height="378" name="cBD958Bm"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2)$$ where $$N$$ is the length of `pairs`.  There are two for loops, and $$N^2$$ dominates the sorting step.

* Space Complexity: $$O(N)$$ for sorting and to store `dp`.

---
#### Approach #2: Greedy [Accepted]

**Intuition**

We can greedily add to our chain.  Choosing the next addition to be the one with the lowest second coordinate is at least better than a choice with a larger second coordinate.

**Algorithm**

Consider the pairs in increasing order of their *second* coordinate.  We'll try to add them to our chain.  If we can, by the above argument we know that it is correct to do so.

<iframe src="https://leetcode.com/playground/fZtpFsK8/shared" frameBorder="0" width="100%" height="242" name="fZtpFsK8"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N \log N)$$ where $$N$$ is the length of `S`.  The complexity comes from the sorting step, but the rest of the solution does linear work.

* Space Complexity: $$O(N)$$.  The additional space complexity of storing `cur` and `ans`, but sorting uses $$O(N)$$ space.  Depending on the implementation of the language used, sorting can sometimes use less space.

## Accepted Submission (java)
```java
import java.util.Arrays;
import java.util.Comparator;

public class Solution {
    public static String stringify2DArray(int[][] arr) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < arr.length; i++) {
            sb.append("[");
            for (int j = 0; j < arr[i].length; j++) {
                sb.append(String.valueOf(arr[i][j]));
                if (j < arr[i].length - 1) {
                    sb.append(",");
                }
            }
            sb.append("]");
        }
        sb.append("]");
        return sb.toString();
    }
    class SecondPairComparator implements Comparator<int[]> {
        public int compare(int[] o1, int[] o2) {
            return o1[1] - o2[1];
        }
    }
    public int findLongestChain(int[][] pairs) {
        Comparator<int[]> comp = new SecondPairComparator();
        Arrays.sort(pairs, comp);
        System.out.println(stringify2DArray(pairs));
        int n = 0;
        int prev = Integer.MIN_VALUE;
        for (int i = 0; i < pairs.length; i++) {
            if (pairs[i][0] > prev) {
                n++;
                prev = pairs[i][1];
            }
        }
        return n;
    }
    public static void main(String[] args) {
        Solution sol = new Solution();
        int n = sol.findLongestChain(new int[][]{{1,2}, {2,3}, {3,4}});
        System.out.println(n);
    }
}

```

## Top Discussions
### 4-Liner Python Greedy
- Author: lefeizzzzz
- Creation Date: Sun Jul 23 2017 15:24:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 23 2018 23:58:23 GMT+0800 (Singapore Standard Time)

<p>
    def findLongestChain(self, pairs):
        cur, res = float('-inf'), 0
        for p in sorted(pairs, key=lambda x: x[1]):
            if cur < p[0]: cur, res = p[1], res + 1
        return res
</p>


### easy dp
- Author: MSG_Yang
- Creation Date: Wed Aug 09 2017 21:20:39 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 17:25:16 GMT+0800 (Singapore Standard Time)

<p>
```
    public int findLongestChain(int[][] pairs) {
        if (pairs == null || pairs.length == 0) return 0;
        Arrays.sort(pairs, (a, b) -> (a[0] - b[0]));
        int[] dp = new int[pairs.length];
        Arrays.fill(dp, 1);
        for (int i = 0; i < dp.length; i++) {
            for (int j = 0; j < i; j++) {
                dp[i] = Math.max(dp[i], pairs[i][0] > pairs[j][1]? dp[j] + 1 : dp[j]);
            }
        }
        return dp[pairs.length - 1];
    }
```
</p>


### Proof of the greedy solution
- Author: yuxiong
- Creation Date: Sat Jan 26 2019 02:58:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jan 26 2019 02:58:56 GMT+0800 (Singapore Standard Time)

<p>
The optimal solution: greedy solution: beat 100%
Sort the pairs by pair[1] (tail).
Try add each pair from left to right.
Why this works?
Consider pairA and pairB, where pairA appears before pairB in the sorted pairs.
That implies that pairA[1] < pairB[1], but there is no constraint on pairA[0] and pairB[0].
Now, the greedy part is: I claim that it\'s always better to try to add pairA to the chain first.
Let\'s prove that:
1.     When pairA[1] < pairB[0], it\'s obvious that we should append pairA first.
1.     When pairA[1] >= pairB[0], we have to choose carefully, because that means:
            either we only append pairA to the chain,
            or we only append pairB to the chain.
        Append either pairA or pairB will increment the length of the chain by 1.
        However: (note: cur is the tail of the chain)
            appending pairA will have cur = pairA[1],
            appending pairB will have cur = pairB[1].
            And pairA[1] < pairB[1]
        Apparently, we shall append pairA first because that way we expose a smaller tail which has a better opportunity to append more future pairs.
```
class Solution:
    def findLongestChain(self, pairs):
        N = len(pairs)
        pairs.sort(key = lambda x: x[1])
        ans = 0
        cur = -math.inf
        for head, tail in pairs:
            if head > cur:
                cur = tail
                ans += 1
        return ans
```
</p>


