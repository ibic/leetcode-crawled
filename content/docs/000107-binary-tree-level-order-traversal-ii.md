---
title: "Binary Tree Level Order Traversal II"
weight: 107
#id: "binary-tree-level-order-traversal-ii"
---
## Description
<div class="description">
<p>Given a binary tree, return the <i>bottom-up level order</i> traversal of its nodes' values. (ie, from left to right, level by level from leaf to root).</p>

<p>
For example:<br />
Given binary tree <code>[3,9,20,null,null,15,7]</code>,<br />
<pre>
    3
   / \
  9  20
    /  \
   15   7
</pre>
</p>
<p>
return its bottom-up level order traversal as:<br />
<pre>
[
  [15,7],
  [9,20],
  [3]
]
</pre>
</p>
</div>

## Tags
- Tree (tree)
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Adobe - 4 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### How to traverse the tree

There are two general strategies to traverse a tree:
     
- *Depth First Search* (`DFS`)

    In this strategy, we adopt the `depth` as the priority, so that one
    would start from a root and reach all the way down to a certain leaf,
    and then back to root to reach another branch.

    The DFS strategy can further be distinguished as
    `preorder`, `inorder`, and `postorder` depending on the relative order
    among the root node, left node, and right node.
    
- *Breadth First Search* (`BFS`)

    We scan through the tree level by level, following the order of height,
    from top to bottom. The nodes on a higher level would be visited before the ones 
    on lower levels.
    
In the following figure the nodes are enumerated in the order you visit them,
please follow ```1-2-3-4-5``` to compare different strategies.

![postorder](../Figures/107/ddfs.png)

Here the problem is to implement split-level BFS traversal : `[[4, 5], [2, 3], [1]]`.
That means we could use one of the `Node->Left->Right` techniques: BFS or DFS Preorder.

We already discussed [three different ways](https://leetcode.com/articles/binary-tree-right-side-view/) 
to implement iterative BFS traversal with the queue, and compared 
[iterative BFS vs. iterative DFS](https://leetcode.com/problems/deepest-leaves-sum/solution/).
Let's use this article to discuss the two most simple and fast techniques:

- Recursive DFS.

- Iterative BFS with two queues.

> Note, that both approaches are root-to-bottom traversals, and we're asked to provide 
bottom-up output. To achieve that, the final result should be reversed. 

<br /> 
<br />


---
#### Approach 1: Recursion: DFS Preorder Traversal

**Intuition**

The first step is to ensure that the tree is not empty. 
The second step is to implement the recursive function 
`helper(node, level)`, which takes the current node and its level as the arguments.

**Algorithm for the Recursive Function**

Here is its implementation:

- Initialize the output list `levels`. 
The length of this list determines which level is currently updated.
You should compare this level `len(levels)` with a node level `level`, 
to ensure that you add the node on the correct level.
If you're still on the previous level - 
add the new level by adding a new list into `levels`.

- Append the node value to the last level in `levels`.

- Process recursively child nodes if they are not `None`: 
`helper(node.left / node.right, level + 1)`.

**Implementation**

!?!../Documents/107_LIS.json:1000,406!?!

<iframe src="https://leetcode.com/playground/H8Jue6Az/shared" frameBorder="0" width="100%" height="480" name="H8Jue6Az"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since each node is processed
exactly once.
 
* Space complexity: $$\mathcal{O}(N)$$ to keep the output structure 
which contains $$N$$ node values.
<br />
<br />


---
#### Approach 2: Iteration: BFS Traversal

**Algorithm**

The recursion above could be rewritten in the iteration form.

Let's keep each tree level in the _queue_ structure,
which typically orders elements in a FIFO (first-in-first-out) manner.
In Java one could use [`ArrayDeque` implementation of the `Queue` interface](https://docs.oracle.com/javase/8/docs/api/java/util/ArrayDeque.html).
In Python using [`Queue` structure](https://docs.python.org/3/library/queue.html)
would be an overkill since it's designed for a safe exchange between multiple threads
and hence requires locking which leads to a performance downgrade. 
In Python the queue implementation with a fast atomic `append()`
and `popleft()` is [`deque`](https://docs.python.org/3/library/collections.html#collections.deque).

**Algorithm**

- Initialize two queues: one for the current level, 
and one for the next. Add root into `nextLevel` queue.

- While `nextLevel` queue is not empty:

    - Initialize the current level `currLevel = nextLevel`,
    and empty the next level `nextLevel`. 
    
    - Iterate over the current level queue:

        - Append the node value to the last level in `levels`.
        
        - Add first _left_ and then _right_ child node into `nextLevel`
        queue.
    
- Return reversed `levels`.

**Implementation**

<iframe src="https://leetcode.com/playground/H5xJuMYH/shared" frameBorder="0" width="100%" height="500" name="H5xJuMYH"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ since each node is processed
exactly once.
 
* Space complexity: $$\mathcal{O}(N)$$ to keep the output structure which
contains $$N$$ node values.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My DFS and BFS java solution
- Author: SOY
- Creation Date: Tue Jan 20 2015 21:51:06 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 18 2019 14:25:21 GMT+0800 (Singapore Standard Time)

<p>
DFS solution:

    public class Solution {
        public List<List<Integer>> levelOrderBottom(TreeNode root) {
            Queue<TreeNode> queue = new LinkedList<TreeNode>();
            List<List<Integer>> wrapList = new LinkedList<List<Integer>>();
            
            if(root == null) return wrapList;
            
            queue.offer(root);
            while(!queue.isEmpty()){
                int levelNum = queue.size();
                List<Integer> subList = new LinkedList<Integer>();
                for(int i=0; i<levelNum; i++) {
                    if(queue.peek().left != null) queue.offer(queue.peek().left);
                    if(queue.peek().right != null) queue.offer(queue.peek().right);
                    subList.add(queue.poll().val);
                }
                wrapList.add(0, subList);
            }
            return wrapList;
        }
    }

BFS solution:

    public class Solution {
            public List<List<Integer>> levelOrderBottom(TreeNode root) {
                List<List<Integer>> wrapList = new LinkedList<List<Integer>>();
                levelMaker(wrapList, root, 0);
                return wrapList;
            }
            
            public void levelMaker(List<List<Integer>> list, TreeNode root, int level) {
                if(root == null) return;
                if(level >= list.size()) {
                    list.add(0, new LinkedList<Integer>());
                }
                levelMaker(list, root.left, level+1);
                levelMaker(list, root.right, level+1);
                list.get(list.size()-level-1).add(root.val);
            }
        }
</p>


### Is there any better idea than doing regular level order traversal and reverse the result?
- Author: stellari
- Creation Date: Sun May 18 2014 05:02:28 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 21:47:54 GMT+0800 (Singapore Standard Time)

<p>
The way I see this problem is that it is EXACTLY the same as "Level-Order Traversal I" except that we need to reverse the final container for output, which is trivial. Is there a better idea that fits this problem specifically?

The attached is my current recursive solution. In each function call, we pass in the current node and its level. If this level does not yet exist in the output container, then we should add a new empty level. Then, we add the current node to the end of the current level, and recursively call the function passing the two children of the current node at the next level. This algorithm is really a DFS, but it saves the level information for each node and produces the same result as BFS would.

    vector<vector<int> > res;

    void DFS(TreeNode* root, int level)
    {
        if (root == NULL) return;
        if (level == res.size()) // The level does not exist in output
        {
            res.push_back(vector<int>()); // Create a new level
        }
        
        res[level].push_back(root->val); // Add the current value to its level
        DFS(root->left, level+1); // Go to the next level
        DFS(root->right,level+1);
    }
    
    vector<vector<int> > levelOrderBottom(TreeNode *root) {
        DFS(root, 0);
        return vector<vector<int> > (res.rbegin(), res.rend());
    }
</p>


### Python solutions (dfs recursively, dfs+stack, bfs+queue).
- Author: OldCodingFarmer
- Creation Date: Thu Aug 13 2015 22:14:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 17:08:06 GMT+0800 (Singapore Standard Time)

<p>

    # dfs recursively
    def levelOrderBottom1(self, root):
        res = []
        self.dfs(root, 0, res)
        return res
    
    def dfs(self, root, level, res):
        if root:
            if len(res) < level + 1:
                res.insert(0, [])
            res[-(level+1)].append(root.val)
            self.dfs(root.left, level+1, res)
            self.dfs(root.right, level+1, res)
            
    # dfs + stack
    def levelOrderBottom2(self, root):
        stack = [(root, 0)]
        res = []
        while stack:
            node, level = stack.pop()
            if node:
                if len(res) < level+1:
                    res.insert(0, [])
                res[-(level+1)].append(node.val)
                stack.append((node.right, level+1))
                stack.append((node.left, level+1))
        return res
     
    # bfs + queue   
    def levelOrderBottom(self, root):
        queue, res = collections.deque([(root, 0)]), []
        while queue:
            node, level = queue.popleft()
            if node:
                if len(res) < level+1:
                    res.insert(0, [])
                res[-(level+1)].append(node.val)
                queue.append((node.left, level+1))
                queue.append((node.right, level+1))
        return res
		
	def levelOrderBottom(self, root):
        deque, ret = collections.deque(), []
        if root:
            deque.append(root)
        while deque:
            level, size = [], len(deque)
            for _ in range(size):
                node = deque.popleft()
                level.append(node.val)
                if node.left:
                    deque.append(node.left)
                if node.right:
                    deque.append(node.right)
            ret.append(level)
        return ret[::-1]
</p>


