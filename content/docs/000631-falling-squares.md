---
title: "Falling Squares"
weight: 631
#id: "falling-squares"
---
## Description
<div class="description">
<p>On an infinite number line (x-axis), we drop given squares in the order they are given.</p>

<p>The <code>i</code>-th square dropped (<code>positions[i] = (left, side_length)</code>) is a square with the left-most point being <code>positions[i][0]</code> and sidelength <code>positions[i][1]</code>.</p>

<p>The square is dropped with the bottom edge parallel to the number line, and from a higher height than all currently landed squares. We wait for each square to stick before dropping the next.</p>

<p>The squares are infinitely sticky on their bottom edge, and will remain fixed to any positive length surface they touch (either the number line or another square). Squares dropped adjacent to each other will not stick together prematurely.</p>
&nbsp;

<p>Return a list <code>ans</code> of heights. Each height <code>ans[i]</code> represents the current highest height of any square we have dropped, after dropping squares represented by <code>positions[0], positions[1], ..., positions[i]</code>.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> [[1, 2], [2, 3], [6, 1]]
<b>Output:</b> [2, 5, 5]
<b>Explanation:</b>
</pre>

<p>After the first drop of <code>positions[0] = [1, 2]: _aa _aa ------- </code>The maximum height of any square is 2.</p>

<p>After the second drop of <code>positions[1] = [2, 3]: __aaa __aaa __aaa _aa__ _aa__ -------------- </code>The maximum height of any square is 5. The larger square stays on top of the smaller square despite where its center of gravity is, because squares are infinitely sticky on their bottom edge.</p>

<p>After the third drop of <code>positions[1] = [6, 1]: __aaa __aaa __aaa _aa _aa___a -------------- </code>The maximum height of any square is still 5. Thus, we return an answer of <code>[2, 5, 5]</code>.</p>

<p>&nbsp;</p>
&nbsp;

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> [[100, 100], [200, 100]]
<b>Output:</b> [100, 100]
<b>Explanation:</b> Adjacent squares don&#39;t get stuck prematurely - only their bottom edge can stick to surfaces.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ul>
	<li><code>1 &lt;= positions.length &lt;= 1000</code>.</li>
	<li><code>1 &lt;= positions[i][0] &lt;= 10^8</code>.</li>
	<li><code>1 &lt;= positions[i][1] &lt;= 10^6</code>.</li>
</ul>

<p>&nbsp;</p>

</div>

## Tags
- Segment Tree (segment-tree)
- Ordered Map (ordered-map)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Uber - 0 (taggedByAdmin: true)
- Square - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach Framework

**Intuition**

Intuitively, there are two operations: `update`, which updates our notion of the board (number line) after dropping a square; and `query`, which finds the largest height in the current board on some interval.  We will work on implementing these operations.

**Coordinate Compression**

In the below approaches, since there are only up to `2 * len(positions)` critical points, namely the left and right edges of each square, we can use a technique called *coordinate compression* to map these critical points to adjacent integers, as shown in the code snippets below.  

For brevity, these snippets are omitted from the remaining solutions.

<iframe src="https://leetcode.com/playground/CbB5jkAp/shared" frameBorder="0" width="100%" height="242" name="CbB5jkAp"></iframe>

---
#### Approach 1: Offline Propagation

**Intuition**

Instead of asking the question "what squares affect this query?", lets ask the question "what queries are affected by this square?"

**Algorithm**

Let `qans[i]` be the maximum height of the interval specified by `positions[i]`.  At the end, we'll return a running max of `qans`.

For each square `positions[i]`, the maximum height will get higher by the size of the square we drop.  Then, for any future squares that intersect the interval `[left, right)` (where `left = positions[i][0], right = positions[i][0] + positions[i][1]`), we'll update the maximum height of that interval.

<iframe src="https://leetcode.com/playground/W5UReNvG/shared" frameBorder="0" width="100%" height="500" name="W5UReNvG"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2)$$, where $$N$$ is the length of `positions`.  We use two for-loops, each of complexity $$O(N)$$.

* Space Complexity: $$O(N)$$, the space used by `qans` and `ans`.
<br>
<br>

---
#### Approach 2: Brute Force with Coordinate Compression

**Intuition and Algorithm**

Let `N = len(positions)`.  After mapping the board to a board of length at most $$2* N \leq 2000$$, we can brute force the answer by simulating each square's drop directly.

Our answer is either the current answer or the height of the square that was just dropped, and we'll update it appropriately.

<iframe src="https://leetcode.com/playground/NgREHX2G/shared" frameBorder="0" width="100%" height="500" name="NgREHX2G"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2)$$, where $$N$$ is the length of `positions`.  We use two for-loops, each of complexity $$O(N)$$ (because of coordinate compression.)

* Space Complexity: $$O(N)$$, the space used by `heights`.
<br>
<br>

---
#### Approach 3: Block (Square Root) Decomposition

**Intuition**

Whenever we perform operations (like `update` and `query`) on some interval in a domain, we could segment that domain with size $$W$$ into blocks of size $$\sqrt{W}$$.  

Then, instead of a typical brute force where we update our array `heights` representing the board, we will also hold another array `blocks`, where `blocks[i]` represents the $$B = \lfloor \sqrt{W} \rfloor$$ elements `heights[B*i], heights[B*i + 1], ..., heights[B*i + B-1]`.  This allows us to write to the array in $$O(B)$$ operations.

**Algorithm**

Let's get into the details.  We actually need another array, `blocks_read`.  When we update some element `i` in block `b = i / B`, we'll also update `blocks_read[b]`.  If later we want to read the entire block, we can read from here (and stuff written to the whole block in `blocks[b]`.)

When we write to a block, we'll write in `blocks[b]`.  Later, when we want to read from an element `i` in block `b = i / B`, we'll read from `heights[i]` and `blocks[b]`.

Our process for managing `query` and `update` will be similar.  While `left` isn't a multiple of `B`, we'll proceed with a brute-force-like approach, and similarly for `right`.  At the end, `[left, right+1)` will represent a series of contiguous blocks: the interval will have length which is a multiple of `B`, and `left` will also be a multiple of `B`.

<iframe src="https://leetcode.com/playground/4ZssEbsQ/shared" frameBorder="0" width="100%" height="500" name="4ZssEbsQ"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N\sqrt{N})$$, where $$N$$ is the length of `positions`.  Each `query` and `update` has complexity $$O(\sqrt{N})$$.

* Space Complexity: $$O(N)$$, the space used by `heights`.
<br>
<br>

---
#### Approach 4: Segment Tree with Lazy Propagation

**Intuition**

If we were familiar with the idea of a segment tree (which supports queries and updates on intervals), we can immediately crack the problem.  

**Algorithm**

Segment trees work by breaking intervals into a disjoint sum of component intervals, whose number is at most `log(width)`.  The motivation is that when we change an element, we only need to change `log(width)` many intervals that aggregate on an interval containing that element.

When we want to update an interval all at once, we need to use *lazy propagation* to ensure good run-time complexity.  This topic is covered in more depth [here](https://leetcode.com/articles/a-recursive-approach-to-segment-trees-range-sum-queries-lazy-propagation/).

With such an implementation in hand, the problem falls out immediately.

<iframe src="https://leetcode.com/playground/HEvAFECn/shared" frameBorder="0" width="100%" height="500" name="HEvAFECn"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N \log N)$$, where $$N$$ is the length of `positions`.  This is the run-time complexity of using a segment tree.

* Space Complexity: $$O(N)$$, the space used by our tree.
<br>
<br>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy Understood O(n^2) Solution with explanation
- Author: leuction
- Creation Date: Sun Oct 15 2017 11:04:30 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 09:50:26 GMT+0800 (Singapore Standard Time)

<p>
The idea is quite simple, we use intervals to represent the square. the initial height we set to the square `cur` is `pos[1]`. That means we assume that all the square will fall down to the land. we iterate the previous squares, check is there any square `i` beneath my `cur` square. If we found that we have squares `i` intersect with us, which means my current square will go above to that square `i`. My target is to find the highest square and put square `cur` onto square `i`, and set the height of the square `cur` as 
```java
cur.height = cur.height + previousMaxHeight;
``` 
Actually, you do not need to use the interval class to be faster, I just use it to make my code cleaner

```java
class Solution {
    private class Interval {
        int start, end, height;
        public Interval(int start, int end, int height) {
            this.start = start;
            this.end = end;
            this.height = height;
        }
    }
    public List<Integer> fallingSquares(int[][] positions) {
        List<Interval> intervals = new ArrayList<>();
        List<Integer> res = new ArrayList<>();
        int h = 0;
        for (int[] pos : positions) {
            Interval cur = new Interval(pos[0], pos[0] + pos[1] - 1, pos[1]);
            h = Math.max(h, getHeight(intervals, cur));
            res.add(h);
        }
        return res;
    }
    private int getHeight(List<Interval> intervals, Interval cur) {
        int preMaxHeight = 0;
        for (Interval i : intervals) {
            // Interval i does not intersect with cur
            if (i.end < cur.start) continue;
            if (i.start > cur.end) continue;
            // find the max height beneath cur
            preMaxHeight = Math.max(preMaxHeight, i.height);
        }
        cur.height += preMaxHeight;
        intervals.add(cur);
        return cur.height;
    }
}
```
</p>


### Treemap solution and Segment tree (Java) solution with lazy propagation and coordinates compression
- Author: britishorthair
- Creation Date: Sun Jan 28 2018 08:42:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 30 2018 08:24:33 GMT+0800 (Singapore Standard Time)

<p>
Here is the link I refer to:
https://www.geeksforgeeks.org/lazy-propagation-in-segment-tree/
https://leetcode.com/articles/falling-squares/

I think the first link is pretty useful to help you understand the segment tree's basic and lazy propagation. The second link I really appreciate its systematic way to define the interface to solve this using different algorithm. However, the segment tree implementation is not well explained in the article (at least it's hard for me to understand).
Thus, I decided to post my Treemap solution, segment tree solution and segment tree solution with lazy propagation for future readers.

TreeMap Solution: The basic idea here is pretty simple, for each square i, I will find all the maximum height from previously dropped squares range from floorKey(i_start) (inclusive) to end (exclusive), then I will update the height and delete all the old heights.
```
import java.util.SortedMap;
class Solution {
    public List<Integer> fallingSquares(int[][] positions) {
        List<Integer> res = new ArrayList<>();
        TreeMap<Integer, Integer> startHeight = new TreeMap<>();
        startHeight.put(0, 0); 
        int max = 0;
        for (int[] pos : positions) {
            int start = pos[0], end = start + pos[1];
            Integer from = startHeight.floorKey(start);
            int height = startHeight.subMap(from, end).values().stream().max(Integer::compare).get() + pos[1];
            max = Math.max(height, max);
            res.add(max);
            // remove interval within [start, end)
            int lastHeight = startHeight.floorEntry(end).getValue();
            startHeight.put(start, height);
            startHeight.put(end, lastHeight);
            startHeight.keySet().removeAll(new HashSet<>(startHeight.subMap(start, false, end, false).keySet()));
        }
        return res;
    }
}
```
Segment tree with coordinates compression: It's easy to understand if go through the links above.
```
class Solution {
    public List<Integer> fallingSquares(int[][] positions) {
        int n = positions.length;
        Map<Integer, Integer> cc = coorCompression(positions);
        int best = 0;
        List<Integer> res = new ArrayList<>();
        SegmentTree tree = new SegmentTree(cc.size());
        for (int[] pos : positions) {
            int L = cc.get(pos[0]);
            int R = cc.get(pos[0] + pos[1] - 1);
            int h = tree.query(L, R) + pos[1];
            tree.update(L, R, h);
            best = Math.max(best, h);
            res.add(best);
        }
        return res;
    }
    
    private Map<Integer, Integer> coorCompression(int[][] positions) {
        Set<Integer> set = new HashSet<>();
        for (int[] pos : positions) {
            set.add(pos[0]);
            set.add(pos[0] + pos[1] - 1);
        }
        List<Integer> list = new ArrayList<>(set);
        Collections.sort(list);
        Map<Integer, Integer> map = new HashMap<>();
        int t = 0;
        for (int pos : list) map.put(pos, t++);
        return map;
    }
    
    class SegmentTree {
        int[] tree;
        int N;
        
        SegmentTree(int N) {
            this.N = N;
            int n = (1 << ((int) Math.ceil(Math.log(N) / Math.log(2)) + 1));
            tree = new int[n];
        }
        
        public int query(int L, int R) {
            return queryUtil(1, 0, N - 1, L, R);
        }
        
        private int queryUtil(int index, int s, int e, int L, int R) {
            // out of range
            if (s > e || s > R || e < L) {
                return 0;
            }
            // [L, R] cover [s, e]
            if (s >= L && e <= R) {
                return tree[index];
            }
            // Overlapped
            int mid = s + (e - s) / 2;
            return Math.max(queryUtil(2 * index, s, mid, L, R), queryUtil(2 * index + 1, mid + 1, e, L, R));
        }
        
        public void update(int L, int R, int h) {
            updateUtil(1, 0, N - 1, L, R, h);
        }
        
        private void updateUtil(int index, int s, int e, int L, int R, int h) {
            // out of range
            if (s > e || s > R || e < L) {
                return;
            }
            tree[index] = Math.max(tree[index], h);
            if (s != e) {
                int mid = s + (e - s) / 2;
                updateUtil(2 * index, s, mid, L, R, h);
                updateUtil(2 * index + 1, mid + 1, e, L, R, h);
            }
        }
    }
}
```
Segment tree with lazy propagation and coordinates compression: This code is easy to add if you already got the code above.
```
class Solution {
    public List<Integer> fallingSquares(int[][] positions) {
        int n = positions.length;
        Map<Integer, Integer> cc = coorCompression(positions);
        int best = 0;
        List<Integer> res = new ArrayList<>();
        SegmentTree tree = new SegmentTree(cc.size());
        for (int[] pos : positions) {
            int L = cc.get(pos[0]);
            int R = cc.get(pos[0] + pos[1] - 1);
            int h = tree.query(L, R) + pos[1];
            tree.update(L, R, h);
            best = Math.max(best, h);
            res.add(best);
        }
        return res;
    }
    
    private Map<Integer, Integer> coorCompression(int[][] positions) {
        Set<Integer> set = new HashSet<>();
        for (int[] pos : positions) {
            set.add(pos[0]);
            set.add(pos[0] + pos[1] - 1);
        }
        List<Integer> list = new ArrayList<>(set);
        Collections.sort(list);
        Map<Integer, Integer> map = new HashMap<>();
        int t = 0;
        for (int pos : list) map.put(pos, t++);
        return map;
    }
    
    class SegmentTree {
        int[] tree;
        int[] lazy;
        int N;
        
        SegmentTree(int N) {
            this.N = N;
            int n = (1 << ((int) Math.ceil(Math.log(N) / Math.log(2)) + 1));
            tree = new int[n];
            lazy = new int[n];
        }
        
        public int query(int L, int R) {
            return queryUtil(1, 0, N - 1, L, R);
        }
        
        private int queryUtil(int index, int s, int e, int L, int R) {
            if (lazy[index] != 0) {
                // apply lazy to node
                int update = lazy[index];
                lazy[index] = 0;
                tree[index] = Math.max(tree[index], update);
                // check if this is leaf
                if (s != e) {
                    lazy[2 * index] = Math.max(lazy[2 * index], update);
                    lazy[2 * index + 1] = Math.max(lazy[2 * index + 1], update);
                }
            }
            // out of range
            if (s > e || s > R || e < L) {
                return 0;
            }
            // [L, R] cover [s, e]
            if (s >= L && e <= R) {
                return tree[index];
            }
            // Overlapped
            int mid = s + (e - s) / 2;
            return Math.max(queryUtil(2 * index, s, mid, L, R), queryUtil(2 * index + 1, mid + 1, e, L, R));
        }
        
        public void update(int L, int R, int h) {
            updateUtil(1, 0, N - 1, L, R, h);
        }
        
        private void updateUtil(int index, int s, int e, int L, int R, int h) {
            if (lazy[index] != 0) {
                // apply lazy to node
                int update = lazy[index];
                lazy[index] = 0;
                tree[index] = Math.max(tree[index], update);
                // check if this is leaf
                if (s != e) {
                    lazy[2 * index] = Math.max(lazy[2 * index], update);
                    lazy[2 * index + 1] = Math.max(lazy[2 * index + 1], update);
                }
            }
            // out of range
            if (s > e || s > R || e < L) {
                return;
            }
            if (s >= L && e <= R) {
                tree[index] = Math.max(tree[index], h);
                if (s != e) {
                    lazy[2 * index] = Math.max(lazy[2 * index], h);
                    lazy[2 * index + 1] = Math.max(lazy[2 * index + 1], h);
                }
                return;
            }
            int mid = s + (e - s) / 2;
            updateUtil(2 * index, s, mid, L, R, h);
            updateUtil(2 * index + 1, mid + 1, e, L, R, h);
            tree[index] = Math.max(tree[2 * index], tree[2 * index + 1]);
        }
    }
}
```
</p>


### Easy and Concise Python Solution (97%)
- Author: lee215
- Creation Date: Sun Oct 15 2017 11:18:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 02:03:48 GMT+0800 (Singapore Standard Time)

<p>
I used two list to take note of the current state.
If you read question **218. The Skyline Problem**, you will easily understand how I do this.

```pos``` tracks the x-coordinate of start points.
```height``` tracks the y-coordinate of lines.

![0_1508239910449_skyline.png](/assets/uploads/files/1508239911437-skyline.png) 

````
def fallingSquares(self, positions):
        height = [0]
        pos = [0]
        res = []
        max_h = 0
        for left, side in positions:
            i = bisect.bisect_right(pos, left)
            j = bisect.bisect_left(pos, left + side)
            high = max(height[i - 1:j] or [0]) + side
            pos[i:j] = [left, left + side]
            height[i:j] = [high, height[j - 1]]
            max_h = max(max_h, high)
            res.append(max_h)
        return res
</p>


