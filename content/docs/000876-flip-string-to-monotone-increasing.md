---
title: "Flip String to Monotone Increasing"
weight: 876
#id: "flip-string-to-monotone-increasing"
---
## Description
<div class="description">
<p>A string of <code>&#39;0&#39;</code>s and <code>&#39;1&#39;</code>s is <em>monotone increasing</em> if it consists of some number of <code>&#39;0&#39;</code>s (possibly 0), followed by some number of <code>&#39;1&#39;</code>s (also possibly 0.)</p>

<p>We are given a string <code>S</code> of <code>&#39;0&#39;</code>s and <code>&#39;1&#39;</code>s, and we may flip any <code>&#39;0&#39;</code> to a <code>&#39;1&#39;</code> or a <code>&#39;1&#39;</code> to a <code>&#39;0&#39;</code>.</p>

<p>Return the minimum number of flips to make <code>S</code>&nbsp;monotone increasing.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;00110&quot;</span>
<strong>Output: </strong><span id="example-output-1">1</span>
<strong>Explanation: </strong>We flip the last digit to get 00111.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">&quot;010110&quot;</span>
<strong>Output: </strong><span id="example-output-2">2</span>
<strong>Explanation: </strong>We flip to get 011111, or alternatively 000111.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">&quot;00011000&quot;</span>
<strong>Output: </strong><span id="example-output-3">2</span>
<strong>Explanation: </strong>We flip to get 00000000.
</pre>

<p>&nbsp;</p>

<p><strong><span>Note:</span></strong></p>

<ol>
	<li><code>1 &lt;= S.length &lt;= 20000</code></li>
	<li><code>S</code> only consists of <code>&#39;0&#39;</code> and <code>&#39;1&#39;</code> characters.</li>
</ol>
</div>
</div>
</div>
</div>

## Tags
- Array (array)

## Companies
- Google - 4 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Prefix Sums

**Intuition**

For say a 5 digit string, the answer is either `'00000'`, `'00001'`, `'00011'`, `'00111'`, `'01111'`, or `'11111'`.  Let's try to calculate the cost of switching to that answer.  The answer has two halves, a left (zero) half, and a right (one) half.

Evidently, it comes down to a question of knowing, for each candidate half: how many ones are in the left half, and how many zeros are in the right half.

We can use prefix sums.  Say `P[i+1] = A[0] + A[1] + ... + A[i]`, where `A[i] = 1` if `S[i] == '1'`, else `A[i] = 0`.  We can calculate `P` in linear time.

Then if we want `x` zeros followed by `N-x` ones, there are `P[x]` ones in the start that must be flipped, plus `(N-x) - (P[N] - P[x])` zeros that must be flipped.  The last calculation comes from the fact that there are `P[N] - P[x]` ones in the later segment of length `N-x`, but we want the number of zeros.

**Algorithm**

For example, with `S = "010110"`:  we have `P = [0, 0, 1, 1, 2, 3, 3]`.  Now say we want to evaluate having `x=3` zeros.

There are `P[3] = 1` ones in the first 3 characters, and `P[6] - P[3] = 2` ones in the later `N-x = 3` characters.

So, there is `(N-x) - (P[N] - P[x]) = 1` zero in the later `N-x` characters.

We take the minimum among all candidate answers to arrive at the final answer.

<iframe src="https://leetcode.com/playground/6SNPpUzN/shared" frameBorder="0" width="100%" height="310" name="6SNPpUzN"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `S`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Prefix-Suffix Java O(N) One Pass Solution - Space O(1)
- Author: vicky_11
- Creation Date: Sun Oct 21 2018 11:26:43 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 20:29:41 GMT+0800 (Singapore Standard Time)

<p>
Algorithm:
1. Skip 0\'s until we encounter the first 1.
2. Keep track of number of 1\'s in onesCount (Prefix).
3. Any 0 that comes after we encounter 1 can be a potential candidate for flip. Keep track of it in flipCount.
4. If flipCount exceeds oneCount - (Prefix 1\'s flipped to 0\'s)
		a. Then we are trying to flip more 0\'s (suffix) than number of 1\'s (prefix) we have. 
		b. Its better to flip the 1\'s instead.
```
public int minFlipsMonoIncr(String S) {
	if(S == null || S.length() <= 0 )
		return 0;

	char[] sChars = S.toCharArray();
	int flipCount = 0;
	int onesCount = 0;

	for(int i=0; i<sChars.length; i++){
		if(sChars[i] == \'0\'){
			if(onesCount == 0) continue;
			else flipCount++;
		}else{
			onesCount++;
		}
		if(flipCount > onesCount){
			flipCount = onesCount;
		}
	}
	return flipCount;
}
```
</p>


### C++/Java 4 lines O(n) | O(1), DP
- Author: votrubac
- Creation Date: Sun Oct 21 2018 11:02:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 10:19:17 GMT+0800 (Singapore Standard Time)

<p>
We need to split the array into  left \'0\' and right \'1\' sub-arrays, so that sum of \'1\' -> \'0\' flips (left) and \'0\' -> \'1\' flips (right) is minimal.
- Count of \'0\' -> \'1\' flips going left to right, and store it in ```f0```.
- Count of \'1\' -> \'0\' flips going right to left, and store it in ```f1```.
- Find a the smallest ```f0[i] + f1[i]```.
```
int minFlipsMonoIncr(string S, int res = INT_MAX) {
    vector<int> f0(S.size() + 1), f1(S.size() + 1);
    for (int i = 1, j = S.size() - 1; j >= 0; ++i, --j) {
        f0[i] += f0[i - 1] + (S[i - 1] == \'0\' ? 0 : 1);
        f1[j] += f1[j + 1] + (S[j] == \'1\' ? 0 : 1);
    }
    for (int i = 0; i <= S.size(); ++i) res = min(res, f0[i] + f1[i]);
    return res;
}
```
![image](https://assets.leetcode.com/users/votrubac/image_1545866108.png)
This reminds me of [122. Best Time to Buy and Sell Stock III](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/description/). That problem has a DP solution with O(1) memory complexity, so we can try to apply it here. We can go left to right, and virtually \'move\' the split point every time we see that we need less \'0\' -> \'1\' than \'1\' -> \'0\' flips (``` f1 = min(f0, f1 + 1 - (c - \'0\'))```).
```
int minFlipsMonoIncr(string S, int f0 = 0, int f1 = 0) {
    for (auto c : S) {
        f0 += c - \'0\';
        f1 = min(f0, f1 + 1 - (c - \'0\'));
    }
    return f1;
}
```
Here is Java version. Note that I am using ```charAt(i)``` instead of ```toCharArray()``` to keep the memory complexity a constant.
```
public int minFlipsMonoIncr(String S) {
  int f0 = 0, f1 = 0;
  for (int i = 0; i < S.length(); ++i) {
    f0 += S.charAt(i) - \'0\';
    f1 = Math.min(f0, f1 + 1 - (S.charAt(i) - \'0\'));
  }
  return f1;
}
```
</p>


### C++ one-pass DP solution, 0ms O(n) | O(1), one line, with explaination.
- Author: LiamHuang
- Creation Date: Mon Nov 05 2018 11:45:46 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 05 2018 11:45:46 GMT+0800 (Singapore Standard Time)

<p>

This is a typical case of DP.

Let's see the sub-question of DP first.

Suppose that you have a string `s`, and the solution to the mono increase question is already solved. That is, for string `s`, `counter_flip` flips are required for the string, and there were `counter_one` `'1'`s in the original string `s`.

Let's see the next step of DP.

Within the string `s`, a new incoming character, say `ch`, is appended to the original string. The question is that, how should `counter_flip` be updated, based on the sub-question? We should discuss it case by case.

* When `'1'` comes, no more flip should be applied, since `'1'` is appended to the tail of the original string.
* When `'0'` comes, things become a little bit complicated. There are two options for us: flip the newly appended `'0'` to `'1'`, after `counter_flip` flips for the original string; or flip `counter_one` `'1'` in the original string to `'0'`. Hence, the result of the next step of DP, in the `'0'` case, is `std::min(counter_flip + 1, counter_one);`.

Based on these analysis, the solution comes.

```cpp
class Solution {
public:
    int minFlipsMonoIncr(const std::string& S, int counter_one  = 0, int counter_flip = 0) {
        for (auto ch : S) counter_flip = std::min(counter_one += ch - '0', counter_flip + '1' - ch);
        return counter_flip;
    }
};
```

If you find the above snippet of code is somewhat difficult to understand, try the below one.

```cpp
class Solution {
public:
    int minFlipsMonoIncr(const std::string& S, int counter_one  = 0, int counter_flip = 0) {
        for (auto ch : S) {
            if (ch == '1') {
                ++counter_one;
            } else {
                ++counter_flip;
            }
            counter_flip = std::min(counter_one, counter_flip);
        }
        return counter_flip;
    }
};
```

Enjoy coding!
</p>


