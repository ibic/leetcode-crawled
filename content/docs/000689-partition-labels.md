---
title: "Partition Labels"
weight: 689
#id: "partition-labels"
---
## Description
<div class="description">
<p>A string <code>S</code> of lowercase English letters is given. We want to partition this string into as many parts as possible so that each letter appears in at most one part, and return a list of integers representing the size of these parts.</p>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> S = &quot;ababcbacadefegdehijhklij&quot;
<b>Output:</b> [9,7,8]
<b>Explanation:</b>
The partition is &quot;ababcbaca&quot;, &quot;defegde&quot;, &quot;hijhklij&quot;.
This is a partition so that each letter appears in at most one part.
A partition like &quot;ababcbacadefegde&quot;, &quot;hijhklij&quot; is incorrect, because it splits S into less parts.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ul>
	<li><code>S</code> will have length in range <code>[1, 500]</code>.</li>
	<li><code>S</code> will consist of lowercase English&nbsp;letters (<code>&#39;a&#39;</code> to <code>&#39;z&#39;</code>) only.</li>
</ul>

<p>&nbsp;</p>

</div>

## Tags
- Two Pointers (two-pointers)
- Greedy (greedy)

## Companies
- Amazon - 205 (taggedByAdmin: true)
- Nutanix - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach 1: Greedy

**Intuition**

Let's try to repeatedly choose the smallest left-justified partition.
Consider the first label, say it's `'a'`.  The first partition must include it, and also the last occurrence of `'a'`.
However, between those two occurrences of `'a'`, there could be other labels that make the minimum size of this partition bigger.  For example, in `"abccaddbeffe"`, the minimum first partition is `"abccaddb"`. 
This gives us the idea for the algorithm:  For each letter encountered, process the last occurrence of that letter, extending the current partition `[anchor, j]` appropriately.

**Algorithm**

We need an array `last[char] -> index of S where char occurs last`.
Then, let `anchor` and `j` be the start and end of the current partition.
If we are at a label that occurs last at some index after `j`, we'll extend the partition `j = last[c]`.  If we are at the end of the partition (`i == j`) then we'll append a partition size to our answer, and set the start of our new partition to `i+1`.

<iframe src="https://leetcode.com/playground/LQRLounV/shared" frameBorder="0" width="100%" height="361" name="LQRLounV"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of $$S$$.

* Space Complexity: $$O(1)$$ to keep data structure `last` of not more
than 26 characters.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 2 pass O(n) time O(1) space, extending end pointer solution
- Author: Samuel_Ji
- Creation Date: Mon Jan 15 2018 03:27:40 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 05:46:37 GMT+0800 (Singapore Standard Time)

<p>
1. traverse the string record the last index of each char.
2. using pointer to record end of the current sub string.
 
```
public List<Integer> partitionLabels(String S) {
        if(S == null || S.length() == 0){
            return null;
        }
        List<Integer> list = new ArrayList<>();
        int[] map = new int[26];  // record the last index of the each char

        for(int i = 0; i < S.length(); i++){
            map[S.charAt(i)-'a'] = i;
        }
        // record the end index of the current sub string
        int last = 0;
        int start = 0;
        for(int i = 0; i < S.length(); i++){
            last = Math.max(last, map[S.charAt(i)-'a']);
            if(last == i){
                list.add(last - start + 1);
                start = last + 1;
            }
        }
        return list;
    }
```
</p>


### Python two pointer solution with explanation
- Author: mereck
- Creation Date: Sat May 25 2019 04:24:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sat May 25 2019 04:24:27 GMT+0800 (Singapore Standard Time)

<p>
Figure out the rightmost index first and use it to denote the start of the next section. 

Reset the left pointer at the start of each new section.

Store the difference of right and left pointers + 1 as in the result for each section.

![image](https://assets.leetcode.com/users/mereck/image_1558729275.png)

```
def partition_labels(S):

	rightmost = {c:i for i, c in enumerate(S)}
	left, right = 0, 0

	result = []
	for i, letter in enumerate(S):

		right = max(right,rightmost[letter])
	
		if i == right:
			result += [right-left + 1]
			left = i+1

	return result

```
</p>


### C++ 6 lines O(n) | O(1) - two simple passes
- Author: votrubac
- Creation Date: Sun Jan 14 2018 12:10:01 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 23:38:04 GMT+0800 (Singapore Standard Time)

<p>
First pass we record the last position for each letter. Second pass we keep the maximum position of the letters we have seen so far. If the pointer exceeds the maximum position, we found the part.
```
vector<int> partitionLabels(string S) {
  vector<int> res, pos(26, 0);  
  for (auto i = 0; i < S.size(); ++i) pos[S[i] - \'a\'] = i;
  for (auto i = 0, idx = INT_MIN, last_i = 0; i < S.size(); ++i) {
    idx = max(idx, pos[S[i] - \'a\']);
    if (idx == i) res.push_back(i - exchange(last_i, i + 1) + 1);
  }
  return res;
}
```
# Complexity Analysis
Runtime: O(n). We analyze each character twice.
Memory: O(1). We use 26 elements for the algorithm. Additional memory is used to store the result; the algorithm does not need it.
</p>


