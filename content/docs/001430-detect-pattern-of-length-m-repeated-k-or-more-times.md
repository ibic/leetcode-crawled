---
title: "Detect Pattern of Length M Repeated K or More Times"
weight: 1430
#id: "detect-pattern-of-length-m-repeated-k-or-more-times"
---
## Description
<div class="description">
<p>Given an array of positive integers <code>arr</code>,&nbsp; find a pattern of length <code>m</code> that is repeated <code>k</code> or more times.</p>

<p>A <strong>pattern</strong> is a subarray (consecutive sub-sequence) that consists of one or more values, repeated multiple times <strong>consecutively </strong>without overlapping. A pattern is defined by its length and the number of repetitions.</p>

<p>Return&nbsp;<code>true</code>&nbsp;<em>if there exists a pattern of length</em>&nbsp;<code>m</code>&nbsp;<em>that is repeated</em>&nbsp;<code>k</code>&nbsp;<em>or more times, otherwise return</em>&nbsp;<code>false</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,4,4,4,4], m = 1, k = 3
<strong>Output:</strong> true
<strong>Explanation: </strong>The pattern <strong>(4)</strong> of length 1 is repeated 4 consecutive times. Notice that pattern can be repeated k or more times but not less.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,1,2,1,1,1,3], m = 2, k = 2
<strong>Output:</strong> true
<strong>Explanation: </strong>The pattern <strong>(1,2)</strong> of length 2 is repeated 2 consecutive times. Another valid pattern <strong>(2,1) is</strong> also repeated 2 times.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,1,2,1,3], m = 2, k = 3
<strong>Output:</strong> false
<strong>Explanation: </strong>The pattern (1,2) is of length 2 but is repeated only 2 times. There is no pattern of length 2 that is repeated 3 or more times.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,3,1,2], m = 2, k = 2
<strong>Output:</strong> false
<strong>Explanation: </strong>Notice that the pattern (1,2) exists twice but not consecutively, so it doesn&#39;t count.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> arr = [2,2,2,2], m = 2, k = 3
<strong>Output:</strong> false
<strong>Explanation: </strong>The only pattern of length 2 is (2,2) however it&#39;s repeated only twice. Notice that we do not count overlapping repetitions.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= arr.length &lt;= 100</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 100</code></li>
	<li><code>1 &lt;= m&nbsp;&lt;= 100</code></li>
	<li><code>2 &lt;= k&nbsp;&lt;= 100</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- HRT - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple c++ solution(0ms) 100% fast
- Author: reema_mahabooba
- Creation Date: Sun Aug 30 2020 12:11:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 30 2020 12:18:47 GMT+0800 (Singapore Standard Time)

<p>
```
 bool containsPattern(vector<int>& arr, int m, int k) {

        int cnt=0;
        for(int i=0;i+m < arr.size(); i++){
            
            if(arr[i]!=arr[i+m]){
              cnt=0;  
            }
            cnt += (arr[i] == arr[i+m]);
            if(cnt == (k-1)*m)
                return true;
            
        }
        return false;
    }
</p>


### [Java] Time O(n) Space O(1)
- Author: heshan1234
- Creation Date: Sun Aug 30 2020 12:11:37 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 31 2020 01:56:31 GMT+0800 (Singapore Standard Time)

<p>
Compare with a shift of m. Related problem: [459. Repeated Substring Pattern].
```
    public boolean containsPattern(int[] arr, int m, int k) {
        for(int i = 0, j = i + m, count = 0; j < arr.length; ++i, ++j) {
            if (arr[i] != arr[j]) {
                count = 0;
            } else if (++count == (k - 1) * m) {
                return true;
            }
        }
        return false;
    }
```
</p>


### Python3 Iterative Solution
- Author: resotto
- Creation Date: Sun Aug 30 2020 12:01:55 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 31 2020 07:30:11 GMT+0800 (Singapore Standard Time)

<p>
```python
def containsPattern(self, arr: List[int], m: int, k: int) -> bool:
	i = 0
	while i <= len(arr)-1:
		p = arr[i:i+m]
		if p * k == arr[i:i+m*k]:
			return True

		i += 1

	return False
```

trungnguyen276\'s solution (optimized)

```python
def containsPattern(self, arr: List[int], m: int, k: int) -> bool:
    streak = 0
    for i in range(len(arr)-m):
        streak = streak + 1 if arr[i] == arr[i+m] else 0
        if streak == (k-1)*m: return True
    return False
```
</p>


