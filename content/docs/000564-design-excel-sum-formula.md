---
title: "Design Excel Sum Formula"
weight: 564
#id: "design-excel-sum-formula"
---
## Description
<div class="description">
<p>Your task is to design the basic function of Excel and implement the function of sum formula.  Specifically, you need to implement the following functions:</p>



<p><code>Excel(int H, char W):</code> This is the constructor. The inputs represents the height and width of the Excel form. <b>H</b> is a positive integer, range from 1 to 26. It represents the height. <b>W</b> is a character range from 'A' to 'Z'. It represents that the width is the number of characters from 'A' to <b>W</b>. The Excel form content is represented by a height * width 2D integer array <code>C</code>, it should be initialized to zero. You should assume that the first row of <code>C</code> starts from 1, and the first column of <code>C</code> starts from 'A'.</p>

<br>

<p><code>void Set(int row, char column, int val):</code> Change the value at <code>C(row, column)</code> to be val.</p>
<br>
<p><code>int Get(int row, char column):</code> Return the value at <code>C(row, column)</code>.</p>
<br>
<p><code>int Sum(int row, char column, List of Strings : numbers):</code> This function calculate and set the value at <code>C(row, column)</code>, where the value should be the sum of cells represented by <code>numbers</code>. This function return the sum result at <code>C(row, column)</code>. This sum formula should exist until this cell is overlapped by another value or another sum formula.</p>

<p><code>numbers</code> is a list of strings that each string represent a cell or a range of cells. If the string represent a single cell, then it has the following format : <code>ColRow</code>. For example, "F7" represents the cell at (7, F). </p>

<p>If the string represent a range of cells, then it has the following format : <code>ColRow1:ColRow2</code>. The range will always be a rectangle, and ColRow1 represent the position of the top-left cell, and ColRow2 represents the position of the bottom-right cell. </p>
<br>
<p><b>Example 1:</b><br />
<pre>
Excel(3,"C"); 
// construct a 3*3 2D array with all zero.
//   A B C
// 1 0 0 0
// 2 0 0 0
// 3 0 0 0

Set(1, "A", 2);
// set C(1,"A") to be 2.
//   A B C
// 1 2 0 0
// 2 0 0 0
// 3 0 0 0

Sum(3, "C", ["A1", "A1:B2"]);
// set C(3,"C") to be the sum of value at C(1,"A") and the values sum of the rectangle range whose top-left cell is C(1,"A") and bottom-right cell is C(2,"B"). Return 4. 
//   A B C
// 1 2 0 0
// 2 0 0 0
// 3 0 0 4

Set(2, "B", 2);
// set C(2,"B") to be 2. Note C(3, "C") should also be changed.
//   A B C
// 1 2 0 0
// 2 0 2 0
// 3 0 0 6
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>You could assume that there won't be any circular sum reference. For example, A1 = sum(B1) and B1 = sum(A1).</li>
<li> The test cases are using double-quotes to represent a character.</li>
<li>Please remember to <b>RESET</b> your class variables declared in class Excel, as static/class variables are <b>persisted across multiple test cases</b>. Please see <a href="https://leetcode.com/faq/#different-output">here</a> for more details.</li>
</ol>
</p>
</div>

## Tags
- Design (design)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Topological Sort

Before discussing the required design, we'll discuss some prerequisites to help ease the understanding of the solution. 

Firstly, we can note that once a formula is applied to any cell in excel, let's say $$C1 = C2 + C3$$, if any change is made to $$C2$$ or $$C3$$, the result to be put into $$C1$$ needs to be evaluated again based on the new values of $$C2$$ and $$C3$$. Further, suppose some other cell, say $$D2$$ is also dependent on $$C1$$ due to some prior formula applied to $$D2$$. Then, when any change is made to, say, $$C2$$, we re-evaluate $$C1$$'s value. Furhter, since $$D2$$ is dependent on $$C1$$, we need to re-evaluate $$D2$$'s value as well. 

Thus, whenever, we make any change to any cell, $$x$$,  we need to determine the cells which are dependent on $$x$$, and update these cells, and further determine the cells which are dependent on the changed cells and so on. We can assume that no cycles are present in the formulas, i.e. Any cell's value won't directly or indirectly be dependent on its own value. 

But, while doing these set of evaluations of the cells to determine their updated values, we need to update the cells in such an order that the cell on which some other cell is dependent is always evaluated prior to the cell which is dependent on the former cell.

In order to do so, we can view the dependence between the cells in the form of a dependency graph, which can be a Directed Graph. Since, no cycles are allowed between the formulas, the graph reduces to a Directed Acyclic Graph. Now, to solve the problem of evaluating the cells in the required order, we can make use of a very well known method specifically used for such problems in Directed Acyclic Graphs, known as the Topological Sorting. 

Topological sorting for Directed Acyclic Graph (DAG) is a linear ordering of vertices such that for every directed edge $$uv$$, vertex $$u$$ comes before $$v$$ in the ordering. For example, a topological sorting of the following graph is `5 4 2 3 1 0`. 

There can be more than one topological sorting for a graph. For example, another topological sorting of the following graph is `4 5 2 3 1 0`. The first vertex in topological sorting is always a vertex with in-degree as 0 (a vertex with no in-coming edges).

![Topological_Sort_Graph](../Figures/631/631_Design_Excel.PNG)
{:align="center"}

Topological Sorting can be done if we modify the Depth First Search to some extent.  In Depth First Search, we start from a vertex, we first print it and then recursively call DFS for its adjacent vertices. Thus, the DFS obtained for the graph above, starting from node 5, will be `5 2 3 1 0 4`. But, in the case of a topological sort, we can't print a node until all the nodes on which it is dependent have already been printed. 

To solve this problem, we make use of a temporary stack. We do the traversals in the same manner as in DFS, but  we don’t print the current node immediately. Instead, for the current node we do as follows:

* Recursively call topological sorting for all the nodes adjacent to the current node.

* Push the current node onto a stack. 

* Repeat the above process till all the nodes have been considered atleast once. 

* Print the contents of the stack. 

Note that a vertex is pushed to stack only when all of its adjacent(dependent) vertices (and their adjacent(dependent) vertices and so on) are already in stack. Thus, we obtain the correct ordering of the vertices. 

The following animation shows an example of topological sorting for the graph above.

!?!../Documents/631_Topological.json:1000,563!?!

We can make use of the same concept while evaluating the cell values to determine the order in which they need to be evaluated. 

Now, let's discuss how we implement the various required functions. We make use of a simple structure(Class), $$Formula$$, which contains two elements. First, the value of the cell which it represents, $$val$$, and a HashMap, $$cells$$. It is a list of cells on which the current cell's value is dependent. This $$cells$$ hashmap stores the data in the form $$(cellName, count)$$.  $$cellName$$ has the format $$ColRow$$. $$count$$ refers to the number of times the current cell directly or indirectly comes in the current cell's formulas. e.g. $$C1 = C2 + C3 + C2$$. In this case, the frequency of $$C3$$ is 1 and that of $$C2$$ is 2.

* `Excel(int H, char W)` : We simply need to initialize an array of $$Formula$$ with $$H$$ rows and the required number of columns corresponding to $$W$$.

* `set(int row, char column, int val)` : For setting the value of the cell corresponding to the given $$row$$ and $$column$$, we can simply change the value , $$val$$, in the $$Formulas$$ array at the indices corresponding to the current cell. Further, if any new formula is applied to a particular cell, we need to remove  the previously applied formulas on the same cell. This is because two formulas can't be used to determine the value of a cell simultaneously. Now, setting a cell to a particular value can also be seen as a formula e.g. $$C1 = 2$$. Thus, we remove all the $$cells$$ in the $$Formulas$$ for the current cell. Further, when the current cell's value is changed, all the other cells which are dependent on it also need to be evaluated in the correct order. Thus, we make use of Topological Sorting starting with the current cell. We make use of a function `topologicalSort(r, c)` for this purpose.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`topologicalSort(r, c)`: In every call to this function, we traverse over all the cells in the $$Formulas$$ array and further apply topological sorting to all the &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cells which are dependent on the current cell(row=r, column=c). To find these cells, we can check the $$cells$$ in the $$Formulas$$ associated with each cell &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;and check if the current cell lies in it. After applying Topological sorting to all these dependent cells, we put the current cell onto a $$stack$$. 

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;After doing the topological sorting, the cells on the $$stack$$ lie in the order in which their values should be evaluated given the current dependency chain &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;based on the formulas applied. Thus, we pick up these cells one by one, and evaluate their values. To do the evaluation, we make use of &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`calculate_sum(r, c, cells)`. In this function, we traverse over all the $$cells$$ in the $$Formulas$$ of the current cell(row=r, column=c), and keep on adding &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;their values. When this summing has been done, we update the current cell's value, $$val$$, to the sum just obtained. We keep on doing so till all the cells in &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the $$stack$$ have been evaluated.

* `get(int row, char column)` : We can simply obtain the value($$val$$) associated with the current cell's $$Formulas$$. If the cell has never been initialized previously, we can return a 0 value.

* `sum(int row, char column, List of Strings : numbers)` : To implement this function, firstly, we need to expand the given $$numbers$$ to obtain all the cells which need to be added in the current formula. We obtain them, by making use of a `convert` function, which extracts all these cells by doing appropriate expansions based on `:` values. We put all these cells in the $$cells$$ associated with the current cell's $$Formulas$$. We also need to set the current cell's value to a new value based on the current formula added. For this, we make use of `calculate_sum` function as discussed above. We also need to do the topological sorting and evaluate all the cells dependent on the current cell. This is done in the same manner as in the `set` function discussed above. We also need to return the value to which the current cell has been set.


<iframe src="https://leetcode.com/playground/KGKJEKW4/shared" frameBorder="0" width="100%" height="500" name="KGKJEKW4"></iframe>

**Performance Analysis**

* `set` takes $$O\big((r*c)^2\big)$$ time. Here, $$r$$ and $$c$$ refer to the number of rows and columns in the current Excel Form. There can be a maximum of $$O(r*c)$$ formulas for an Excel Form with $$r$$ rows and $$c$$ columns. For each formula, $$r*c$$ time will be needed to find the dependent nodes. Thus, in the worst case, a total of $$O\big((r*c)^2\big)$$ will be needed.

* `sum` takes $$O\big((r*c)^2 + 2*r*c*l\big)$$ time. Here, $$l$$ refers to the number of elements in the  the list of strings used for obtaining the cells required for the current sum. In the worst case, the expansion of each such element requires $$O(r*c)$$ time, leading to $$O(l*r*c)$$ time for expanding $$l$$ such elements. After doing the expansion, `calculate_sum` itself requires $$O(l*r*c)$$ time for traversing over the required elements for obtaining the sum. After this, we need to update all the dependent cells, which requires the use of `set` which itself requires $$O\big((r*c)^2\big)$$ time.

* `get` takes $$O(1)$$ time. 

* The space required will be $$O\big((r*c)^2\big)$$ in the worst case. $$O(r*c)$$ space will be required for the Excel Form itself. For each cell in this form, the $$cells$$ list can contain $$O(r*c)$$ cells.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java, implement the logic in Cell class, easy to understand
- Author: ydeng365
- Creation Date: Mon Jul 10 2017 11:32:29 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 06 2018 23:06:06 GMT+0800 (Singapore Standard Time)

<p>
O(1)  set() operation, only do the traverse in get()/sum() operation, 103ms.
Good for set() heavy system.

```
public class Excel {
    Cell[][] table;

    public Excel(int H, char W) {
        table = new Cell[H+1][W-'A'+1];
    }
    
    public void set(int r, char c, int v) {
        if(table[r][c-'A'] == null) table[r][c-'A'] = new Cell (v); 
        else table[r][c-'A'].setValue(v); 
    }
    
    public int get(int r, char c) {
        if( table[r][c-'A'] == null) return 0;
        else return table[r][c-'A'].getValue();  
    }
    
    public int sum(int r, char c, String[] strs) {
        if (table[r][c-'A'] == null) {
            table[r][c-'A'] = new Cell(strs);
        } else {
            table[r][c-'A'].setFormula(strs);
        }
        
        return table[r][c-'A'].getValue();
    }
    
    
    private class Cell{
        int val=0;
        HashMap<Cell, Integer> formula=new HashMap<>();//one cell might appear multiple times
        
        public Cell(int val){
            setValue(val); 
        }
        public Cell(String[] formulaStr){
            setFormula(formulaStr);
        }
        
        public void setValue(int val) {           
            formula.clear(); //will not be treated as a formula cell anymore        
            this.val = val;
        }
        
        public void setFormula(String[] formulaStr){
            formula.clear();            
            for(String str : formulaStr){
                if (str.indexOf(":")<0){
                    int[] pos = getPos(str);
                    addFormulaCell(pos[0], pos[1]);
                } else {
                    String[] pos = str.split(":");
                    int[] startPos = getPos(pos[0]);
                    int[] endPos = getPos(pos[1]);
                    for(int r = startPos[0]; r<=endPos[0]; r++){
                        for(int c = startPos[1]; c<=endPos[1]; c++){
                            addFormulaCell(r, c);
                        }
                    }
                }
            }
        }
        
        private int[] getPos(String str){
            int[] pos = new int[2];
            pos[1]=str.charAt(0)-'A';
            pos[0]=Integer.parseInt(str.substring(1));
            return pos;
        }
        
        private void addFormulaCell(int r, int c){
            if(table[r][c] == null) table[r][c] = new Cell(0);
            Cell rangeCell = table[r][c];                            
            formula.put(rangeCell, (formula.containsKey(rangeCell)? formula.get(rangeCell) : 0)+1);
        }
        
        //recursive method
        private int getValue(){
            if(this.formula.isEmpty()) return this.val;
            int sum = 0;
            for(Cell cell : formula.keySet()){
                sum+=cell.getValue()*formula.get(cell);
            }
            return sum;
        }
    }
}
```
</p>


### Python Solution
- Author: lee215
- Creation Date: Thu Jun 29 2017 02:43:20 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jun 29 2017 02:43:20 GMT+0800 (Singapore Standard Time)

<p>
`````
class Excel(object):

    def __init__(self, H, W):
        self.M = [[{'v': 0, 'sum': None} for i in range(H)] for j in range(ord(W) - 64)]

    def set(self, r, c, v):
        self.M[r - 1][ord(c) - 65] = {'v': v, 'sum': None}

    def get(self, r, c):
        cell = self.M[r - 1][ord(c) - 65]
        if not cell['sum']: return cell['v']
        return sum(self.get(*pos) * cell['sum'][pos] for pos in cell['sum'])

    def sum(self, r, c, strs):
        self.M[r - 1][ord(c) - 65]['sum'] = self.parse(strs)
        return self.get(r, c)

    def parse(self, strs):
        c = collections.Counter()
        for s in strs:
            s, e = s.split(':')[0], s.split(':')[1] if ':' in s else s
            for i in range(int(s[1:]), int(e[1:]) + 1):
                for j in range(ord(s[0]) - 64, ord(e[0]) - 64 + 1):
                    c[(i, chr(j + 64))] += 1
        return c
</p>


### C++, 3 ms, concise and easy to understand
- Author: zestypanda
- Creation Date: Mon Jun 26 2017 02:11:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 21:35:16 GMT+0800 (Singapore Standard Time)

<p>
I have put detailed explanation in the comments. A vector<vector<int>> is used for the Excel. Here I just want to highlight two things.
1) When a cell changes, all the sum related have to update. So a forward link from a cell to all the cells with it in the sum is required. Because a cell may be used for multiple times in the sum due to overlapping ranges, unordered_map<cell, unordered_map<cell, weight>> is used. The weight could improve the efficiency in the worst case.
2) When reset a cell's value, or reassign a new sum range to it, the cells contribute to its sum will change. So a backward link from this cell to all those cells is necessary. unordered_map<cell, unordered_set<cell>>  is sufficient.

The excel is small, at most 26 by 26. So we can use row*26+col as the key of a cell, which is int.
See the code below.
```
class Excel {
public:
    Excel(int H, char W) {
        // initialization. Because r starts from 1, I used H+1 instead of H.
        Exl = vector<vector<int>> (H+1, vector<int>(W-'A'+1, 0));
        fward.clear();
        bward.clear();
    }
    
    void set(int r, char c, int v) {
        int col = c-'A', key = r*26+col;
        // update its value and all the sum related
        update(r, col, v);
        // This is a reset, so break all the forward links if existing
        if (bward.count(key)) {
            for (int k:bward[key]) {
                fward[k].erase(key);
            }
            bward[key].clear();
        }
    }
    // update a cell and all the sum related, using BFS
    // weights are used to improve efficiency for overlapping ranges
    void update(int r, int col, int v) {
        int prev = Exl[r][col];
        Exl[r][col] = v;
        // myq is keys for cells in next level, and update is the increment for each cell
        queue<int> myq, update;
        myq.push(r*26+col);
        update.push(v-prev);
        while (!myq.empty()) {
            int key = myq.front(), dif = update.front();
            myq.pop();
            update.pop();
            if (fward.count(key)) {
                for (auto it = fward[key].begin(); it != fward[key].end(); it++) {
                    int k = it->first;
                    myq.push(k);
                    update.push(dif*(it->second));
                    Exl[k/26][k%26] += dif*(it->second);
                }
            }
        }
    }
    
    int get(int r, char c) {
        return Exl[r][c-'A'];
    }
    
    int sum(int r, char c, vector<string> strs) {
        // this is another reset, so break all the forward links
        int col = c-'A', key = r*26+col, ans = 0;
        if (bward.count(key)) {
            for (int k:bward[key]) {
                fward[k].erase(key);
            }
            bward[key].clear();
        }
        // get the sum over multiple ranges
        for (string str:strs) {
            int p = str.find(':'), left, right, top, bot;
            left = str[0]-'A';
            right = str[p+1]-'A';
            if (p == -1) 
                top = stoi(str.substr(1));
            else
                top = stoi(str.substr(1, p-1));
            bot = stoi(str.substr(p+2));
            for (int i = top; i <= bot; ++i) {
                for (int j = left; j <= right; ++j) {
                    ans += Exl[i][j];
                    fward[i*26+j][key]++;
                    bward[key].insert(i*26+j);
                }
            }
        }
        // update this cell and all the sum related
        update(r, col, ans);
        return ans;
    }
private:
    // The key of a cell is defined as 26*row+col, which is int;
    // fward links a cell to all the cells which use it for sum, and it may be used for 
    // multiple times due to overlap ranges, so another map is used for its weight
    // bward links a cell to all the cells that contribute to its sum. When reset its value,
    // or reassign a new sum range to it, we need disconnect the forward link of those cells to it. 
    unordered_map<int, unordered_map<int, int>> fward;
    unordered_map<int, unordered_set<int>> bward;
    vector<vector<int>> Exl;
};
```
</p>


