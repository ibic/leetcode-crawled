---
title: "Random Point in Non-overlapping Rectangles"
weight: 828
#id: "random-point-in-non-overlapping-rectangles"
---
## Description
<div class="description">
<p>Given a list of <strong>non-overlapping</strong>&nbsp;axis-aligned rectangles <code>rects</code>, write a function <code>pick</code> which randomly and uniformily picks an <strong>integer point</strong> in the space&nbsp;covered by the rectangles.</p>

<p>Note:</p>

<ol>
	<li>An <strong>integer point</strong>&nbsp;is a point that has integer coordinates.&nbsp;</li>
	<li>A point&nbsp;on the perimeter&nbsp;of a rectangle is&nbsp;<strong>included</strong> in the space covered by the rectangles.&nbsp;</li>
	<li><code>i</code>th rectangle = <code>rects[i]</code> =&nbsp;<code>[x1,y1,x2,y2]</code>, where <code>[x1, y1]</code>&nbsp;are the integer coordinates of the bottom-left corner, and <code>[x2, y2]</code>&nbsp;are the integer coordinates of the top-right corner.</li>
	<li>length and width of each rectangle does not exceed <code>2000</code>.</li>
	<li><code>1 &lt;= rects.length&nbsp;&lt;= 100</code></li>
	<li><code>pick</code> return a point as an array of integer coordinates&nbsp;<code>[p_x, p_y]</code></li>
	<li><code>pick</code> is called at most <code>10000</code>&nbsp;times.</li>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: 
</strong><span id="example-input-1-1">[&quot;Solution&quot;,&quot;pick&quot;,&quot;pick&quot;,&quot;pick&quot;]
</span><span id="example-input-1-2">[[[[1,1,5,5]]],[],[],[]]</span>
<strong>Output: 
</strong><span id="example-output-1">[null,[4,1],[4,1],[3,3]]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: 
</strong><span id="example-input-2-1">[&quot;Solution&quot;,&quot;pick&quot;,&quot;pick&quot;,&quot;pick&quot;,&quot;pick&quot;,&quot;pick&quot;]
</span><span id="example-input-2-2">[[[[-2,-2,-1,-1],[1,0,3,0]]],[],[],[],[],[]]</span>
<strong>Output: 
</strong><span id="example-output-2">[null,[-1,-2],[2,0],[-2,-1],[3,0],[-2,-2]]</span></pre>
</div>

<div>
<p><strong>Explanation of Input Syntax:</strong></p>

<p>The input is two lists:&nbsp;the subroutines called&nbsp;and their&nbsp;arguments.&nbsp;<code>Solution</code>&#39;s&nbsp;constructor has one argument, the array of rectangles <code>rects</code>. <code>pick</code>&nbsp;has no arguments.&nbsp;Arguments&nbsp;are&nbsp;always wrapped with a list, even if there aren&#39;t any.</p>
</div>
</div>

<div>
<div>&nbsp;</div>
</div>

</div>

## Tags
- Binary Search (binary-search)
- Random (random)

## Companies
- Google - 4 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---

#### Approach 1: Prefix Sum and Binary Search

**Intuition**

Some rectangles may be more likely to be sampled from than others, since some may contain more points than others, and each point has an equal chance of being sampled. Is there a way to select a rectangle to sample from, such that the probabilities are proportional to the number of points contained in each rectangle? Is there a way to do this using less than $$O(\text{total number of points})$$ space?

**Algorithm**

Create a weight array $$w$$, where $$w[i]$$ is the number of points in $$\text{rects}[i]$$.

Compute the prefix sum array $$p$$, where $$p[x] = \sum_{i=0}^{x}w[i]$$.

Generate a random integer $$\text{targ}$$ in the range $$[0, \sum_{i=0}^{n}w[i]]$$, where $$\sum_{i=0}^{n}w[i]$$ is the total number of points in all rectangles.

Use binary search to find the index $$x$$ where $$x$$ is the lowest index such that $$\text{targ} < p[x]$$. $$\text{rects}[x]$$ is the rectangle that we will sample from.

Note that for some index $$i$$, all integers $$v$$ where $$p[i] - w[i] \leq v < p[i]$$ map to this index. Therefore, rectangles will be sampled proportionally to the rectangle weights.

The only step remaining is to choose a random point in $$\text{rects}[x]$$. Generating random $$\text{x\_coordinate}$$ and $$\text{y\_coordinate}$$ within this rectangle area will suffice, but we can also reuse $$\text{targ}$$ by mapping it to the point

$$
\text{x\_coordinate} = x1 + (\text{targ}-p[i]+w[i])\ \%\  (x2-x1+1) \\
\text{y\_coordinate} = y1 + (\text{targ}-p[i]+w[i])\ /\ (x2-x1+1)
$$

This strategy is useful when calls to the random number generator are expensive.

<p align="center">
    <img src="../Figures/882/targToPoint.png" alt="Targ_To_Point" style="height: 300px;"/>

<br/>

Mapping from targ to x_coordinate and y_coordinate for rects = [[1, 1, 2, 4], [3, 2, 5, 4], [2, 5, 5, 6]]

</p>

<iframe src="https://leetcode.com/playground/ab7ERTMk/shared" frameBorder="0" width="100%" height="500" name="ab7ERTMk"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ preprocessing. $$O(\log N)$$ pick.
* Space Complexity: $$O(N)$$

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Trying to explain why the intuitive solution wont work
- Author: Murushierago
- Creation Date: Sat Jun 22 2019 11:06:54 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 31 2020 01:04:32 GMT+0800 (Singapore Standard Time)

<p>
### Problem
The intuitive solution is randomly pick one rectangle from the `rects` and then create a random point within it. But this approach wont work. It took me a while to understand, I am trying to explain it:

![image](https://assets.leetcode.com/users/dqi2/image_1561169986.png)

As shown in the picture above, you have two rectangles.  Lets declare `S1` to be the set of points within rectanle 1, and `S2` to be point set within rectable 2. So, mathematically we have:
```
S1 = {p11, p12, p13, ........................, p1n}
S2 = {p21, p22, p23, ......, p2m}
n > m
```
Obviously, you can see that `rectangle 1` is larger than `rectangle 2` and therefore `S1` has bigger size `(n > m)`. 
According to the problem, `"randomly and uniformily picks an integer point in the space covered by the rectangles"`. It is very difficult to understand,  lets translated it into another way of expression. 

It is saying that, now I am providing you a new set `S = S1 + S2`, where `S = {p11, p12, ............, p1n, p21, p22, ........., p2m}` , **within this new set of points** that are merged together, randomly pick a point from it. What do you think of the probability of getting `p1i` and `p2j` right now ?  They are exactly the same, which is `1 / (n + m)`.

Now, we can answer why the intuitive solution wont work. If you first randomy pick a rectangle, you have 50% to get either `S1` or `S2`,  how ever, once you select on rectangle, you have selected `S1` as your candidate point set, no matter how many time you try to pick you will never get the points in the second set `S2`. If size of S1 and S2 are the same, that would be ok, but if `S1` is bigger than `S2`, you will run into a problem. 

For example, the chance of getting `S1` and `S2` are both `1 / 2`, so far so good. How ever, within `S1` and `S2`, the chance of getting point `p1i` and `p2j` are `1 / n` and `1 / m`. So the final chance of getting `p1i` and `p2j` are:
```
probability_of_getting_p1i = 1 / (2 * n)
probability_of_getting_p2j = 1 / (2 * m)

Where n is the size of S1, and m is size of S2
```
The probability depends on the size of two rectangle.

### Solution
So how can we solve the problem that descibed in previous section ?  One way is to really merge all the point set of every rectangle and radnomly pick one from them, but this may  not be a good idea since it requires very hight space somplexity. What if we make the chance of getting reactangle`S1` higher than rectangle `S2` (from rects) base on the size of each of them.
for example, for simplification, lets assume:
```
n = size_of_s1 = 70
m = size_of_s2 = 30

n + m = 100
```

if we can have the chance of getting `S1` to be `70 %` and chance of getting `S2` to be `30 %`, the chance of getting `p1i` from `S1` is `1 / 70`, and chance of getting `p2j` from `S2` is `1 / 30`, we have:
```
probality_of_getting_p1i = (70 / 100) * (1 / 70) = 1 / 100

probability_of_getting_p2j = (30 / 100) * (1 / 30) = 1 / 100
```

problem sovled !

So, our mission is to implement an algorithm that allows us to have higher chance to pick the larger rectangle, and then generate a random point with it. Lets use code pseudo to make a rough design, still base on the previous example, imagine you have a map:

```

map = 
{
	30: S1,
	30 + 70: S2
}

or 

map = 
{
  30: S1,
  100: S2
}


randomNumer = random(0, 100)

if  30 < randomNumber <= 100:
	return S2
else
	return S1

```

If we can design an algorithm like that, we will have 70% chance gitting S2, and 30% chance hitting S1. With number of intervals already known, we can simply use `if .. else` block or `switch` to implement this, but the problem did not specify how many rects, so the popular solution uses `TreeMap` .



</p>


### [Python] Short solution with binary search, explained
- Author: DBabichev
- Creation Date: Sat Aug 22 2020 16:31:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 22 2020 16:31:13 GMT+0800 (Singapore Standard Time)

<p>
Basically, this problem is extention of problem **528. Random Pick with Weight**, let me explain why. Here we have several rectangles and we need to choose point from these rectangles. We can do in in two steps:

1. Choose rectangle. Note, that the bigger number of points in these rectangle the more should be our changes. Imagine, we have two rectangles with 10 and 6 points. Then we need to choose first rectangle with probability `10/16` and second with probability `6/16`.
2. Choose point inside this rectangle. We need to choose coordinate `x` and coordinate `y` uniformly.

When we `initailze` we count weights as `(x2-x1+1)*(y2-y1+1)` because we also need to use boundary. Then we evaluate cumulative sums and normalize.

For `pick` function, we use **binary search** to find the right place, using uniform distribution from `[0,1]` and then we use uniform discrete distribution to choose coordinates `x` and `y`. 

**Complexity**: Time and space complexity of `__init__` is `O(n)`, where `n` is number of rectangles. Time complexity of `pick` is `O(log n)`, because we use binary search. Space complexity of `pick` is `O(1)`.

**Remark**: Note, that there is solution with `O(1)` time/space complexity for `pick`, using smart mathematical trick, see my solution of problem **528**: https://leetcode.com/problems/random-pick-with-weight/discuss/671439/Python-Smart-O(1)-solution-with-detailed-explanation


```
class Solution:
    def __init__(self, rects):
        w = [(x2-x1+1)*(y2-y1+1) for x1,y1,x2,y2 in rects]
        self.weights = [i/sum(w) for i in accumulate(w)]
        self.rects = rects

    def pick(self):
        n_rect = bisect.bisect(self.weights, random.random())
        x1, y1, x2, y2 = self.rects[n_rect] 
        return [random.randint(x1, x2),random.randint(y1, y2)]
```

If you have any questions, feel free to ask. If you like solution and explanations, please **Upvote!**
</p>


### Java Solution. Randomly pick a rectangle then pick a point inside.
- Author: Uynait
- Creation Date: Fri Jul 27 2018 18:16:53 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 14:31:10 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    TreeMap<Integer, Integer> map;
    int[][] arrays;
    int sum;
    Random rnd= new Random();
    
    public Solution(int[][] rects) {
        arrays = rects;
        map = new TreeMap<>();
        sum = 0;
        
        for(int i = 0; i < rects.length; i++) {
            int[] rect = rects[i];
						
            // the right part means the number of points can be picked in this rectangle
            sum += (rect[2] - rect[0] + 1) * (rect[3] - rect[1] + 1);
			
            map.put(sum, i);
        }
    }
    
    public int[] pick() {
        // nextInt(sum) returns a num in [0, sum -1]. After added by 1, it becomes [1, sum]
        int c = map.ceilingKey( rnd.nextInt(sum) + 1);
        
        return pickInRect(arrays[map.get(c)]);
    }
    
    private int[] pickInRect(int[] rect) {
        int left = rect[0], right = rect[2], bot = rect[1], top = rect[3];
        
        return new int[]{left + rnd.nextInt(right - left + 1), bot + rnd.nextInt(top - bot + 1) };
    }
}
```
</p>


