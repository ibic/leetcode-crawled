---
title: "Generalized Abbreviation"
weight: 303
#id: "generalized-abbreviation"
---
## Description
<div class="description">
<p>Write a function to generate the generalized abbreviations of a word.&nbsp;</p>

<p><strong>Note:&nbsp;</strong>The order of the output does not matter.</p>

<p><b>Example:</b></p>

<pre>
<strong>Input:</strong> <code>&quot;word&quot;</code>
<strong>Output:</strong>
[&quot;word&quot;, &quot;1ord&quot;, &quot;w1rd&quot;, &quot;wo1d&quot;, &quot;wor1&quot;, &quot;2rd&quot;, &quot;w2d&quot;, &quot;wo2&quot;, &quot;1o1d&quot;, &quot;1or1&quot;, &quot;w1r1&quot;, &quot;1o2&quot;, &quot;2r1&quot;, &quot;3d&quot;, &quot;w3&quot;, &quot;4&quot;]
</pre>

<p>&nbsp;</p>

</div>

## Tags
- Backtracking (backtracking)
- Bit Manipulation (bit-manipulation)

## Companies
- ByteDance - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)

## Official Solution
[TOC]

## Summary
This article is for intermediate readers. It introduces the following ideas:
Backtracking and Bit Manipulation

## Solution
---
#### Approach #1 (Backtracking) [Accepted]

**Intuition**

How many abbreviations are there for a word of length $$n$$? The answer is $$2^n$$ because each character can either be abbreviated or not, resulting in different abbreviations.

**Algorithm**

The backtracking algorithm enumerates a set of partial candidates that, in principle, could be completed in several choices to give all the possible solutions to the problem. The completion is done incrementally, by extending the candidate in many steps. Abstractly, the partial candidates can be seen as nodes of a tree, the potential search tree. Each partial candidate is the parent of the candidates that derives from it by an extension step; the leaves of the tree are the partial candidates that cannot be extended any further.

In our problem, the partial candidates are incomplete abbreviations that can be extended by one of the two choices:

1. keep the next character;
2. abbreviate the next character.

We extend the potential candidate in a depth-first manner. We backtrack when we reach a leaf node in the search tree. All the leaves in the search tree are valid abbreviations and shall be put into a shared list which will be returned at the end.



<iframe src="https://leetcode.com/playground/DZ7F6eEQ/shared" frameBorder="0" name="DZ7F6eEQ" width="100%" height="496"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n 2^n)$$. For each call to `backtrack`, it either returns without branching, or it branches into two recursive calls. All these recursive calls form a complete binary recursion tree with $$2^n$$ leaves and $$2^n - 1$$ inner nodes. For each leaf node, it needs $$O(n)$$ time for converting builder to String; for each internal node, it needs only constant time. Thus, the total time complexity is dominated by the leaves. In total that is $$O(n2^n)$$.

* Space complexity : $$O(n)$$. If the return list doesn't count, we only need $$O(n)$$ auxiliary space to store the characters in `StringBuilder` and the $$O(n)$$ space used by system stack. In a recursive program, the space of system stack is linear to the maximum recursion depth which is $$n$$ in our problem.

---
#### Approach #2 (Bit Manipulation) [Accepted]

**Intuition**

If we use $$0$$ to represent a character that is not abbreviated and $$1$$ to represent one that is. Then each abbreviation is mapped to an $$n$$ bit binary number and vice versa.

**Algorithm**

To generate all the $$2^n$$ abbreviation with non-repetition and non-omission, we need to follow rules. In approach #1, the rules are coded in the backtracking process.
Here we introduce another way.

From the intuition section, each abbreviation has a one to one relationship to a $$n$$ bit binary number $$x$$. We can use these numbers as blueprints to build the corresponding abbreviations.

For example:

Given word = `"word"` and x = `0b0011`

Which means `'w'` and `'o'` are kept, `'r'` and `'d'` are abbreviated. Therefore, the result is "wo2".

Thus, for a number $$x$$, we just need to scan it bit by bit as if it is an array so that we know which character should be kept and which should be abbreviated.

To scan a number $$x$$ bit by bit, one could extract its last bit by `b = x & 1` and shift $$x$$ one bit to the right, i.e. `x >>= 1`.
Doing this repeatedly, one will get all the $$n$$ bits of $$x$$ from last bit to first bit.


<iframe src="https://leetcode.com/playground/s3sJrrGq/shared" frameBorder="0" name="s3sJrrGq" width="100%" height="513"></iframe>
**Complexity Analysis**

* Time complexity : $$O(n 2^n)$$. Building one abbreviation from the number $$x$$, we need scan all the $$n$$ bits. Besides the `StringBuilder::toString` function is also linear. Thus, to generate all the $$2^n$$, it costs $$O(n 2^n)$$ time.

* Space complexity : $$O(n)$$. If the return list doesn't count, we only need $$O(n)$$ auxiliary space to store the characters in `StringBuilder`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java backtracking solution
- Author: soymuybien
- Creation Date: Thu Dec 24 2015 11:08:02 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 03:59:04 GMT+0800 (Singapore Standard Time)

<p>
The idea is: for every character, we can keep it or abbreviate it. To keep it, we add it to the current solution and carry on backtracking. To abbreviate it, we omit it in the current solution, but increment the count, which indicates how many characters have we abbreviated. When we reach the end or need to put a character in the current solution, and count is bigger than zero, we add the number into the solution.



      public List<String> generateAbbreviations(String word){
            List<String> ret = new ArrayList<String>();
            backtrack(ret, word, 0, "", 0);
    
            return ret;
        }
    
        private void backtrack(List<String> ret, String word, int pos, String cur, int count){
            if(pos==word.length()){
                if(count > 0) cur += count;
                ret.add(cur);
            }
            else{
                backtrack(ret, word, pos + 1, cur, count + 1);
                backtrack(ret, word, pos+1, cur + (count>0 ? count : "") + word.charAt(pos), 0);
            }
        }
</p>


### Java 14ms beats 100%
- Author: yavinci
- Creation Date: Wed Dec 30 2015 21:47:30 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Dec 30 2015 21:47:30 GMT+0800 (Singapore Standard Time)

<p>
For each char `c[i]`, either abbreviate it or not.

 1. Abbreviate: count accumulate `num` of abbreviating chars, but don't append it yet. 
 2. Not Abbreviate: append accumulated `num` as well as current char `c[i]`.
 3. In the end append remaining `num`.
 4. Using `StringBuilder` can decrease `36.4%` time. 

This comes to the pattern I find powerful:

    int len = sb.length(); // decision point
    ... backtracking logic ...
    sb.setLength(len);     // reset to decision point

Similarly, check out [remove parentheses][1] and [add operators][2].

<hr>

    public List<String> generateAbbreviations(String word) {
        List<String> res = new ArrayList<>();
        DFS(res, new StringBuilder(), word.toCharArray(), 0, 0);
        return res;
    }
    
    public void DFS(List<String> res, StringBuilder sb, char[] c, int i, int num) {
        int len = sb.length();  
        if(i == c.length) {
            if(num != 0) sb.append(num);
            res.add(sb.toString());
        } else {
            DFS(res, sb, c, i + 1, num + 1);               // abbr c[i]

            if(num != 0) sb.append(num);                   // not abbr c[i]
            DFS(res, sb.append(c[i]), c, i + 1, 0);        
        }
        sb.setLength(len); 
    }


  [1]: https://leetcode.com/discuss/72208/easiest-9ms-java-solution
  [2]: https://leetcode.com/discuss/75308/java-simple-and-fast-solution-beats-96-56%25
</p>


### Simple Python Solution with Explanation
- Author: myliu
- Creation Date: Wed May 18 2016 09:40:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 26 2018 06:42:45 GMT+0800 (Singapore Standard Time)

<p>
    class Solution(object):
        def generateAbbreviations(self, word):
            """
            :type word: str
            :rtype: List[str]
            """
            def helper(word, pos, cur, count, result):
                if len(word) == pos:
                    # Once we reach the end, append current to the result
                    result.append(cur + str(count) if count > 0 else cur)
                else:
                    # Skip current position, and increment count
                    helper(word, pos + 1, cur, count + 1, result)
                    # Include current position, and zero-out count
                    helper(word, pos + 1, cur + (str(count) if count > 0 else '') + word[pos], 0, result)
    
            result = []
            helper(word, 0, '', 0, result)
            return result
</p>


