---
title: "Rank Scores"
weight: 1473
#id: "rank-scores"
---
## Description
<div class="description">
<p>Write a SQL query to rank scores. If there is a tie between two scores, both should have the same ranking. Note that after a tie, the next ranking number should be the next consecutive integer value. In other words, there should be no &quot;holes&quot; between ranks.</p>

<pre>
+----+-------+
| Id | Score |
+----+-------+
| 1  | 3.50  |
| 2  | 3.65  |
| 3  | 4.00  |
| 4  | 3.85  |
| 5  | 4.00  |
| 6  | 3.65  |
+----+-------+
</pre>

<p>For example, given the above <code>Scores</code> table, your query should generate the following report (order by highest score):</p>

<pre>
+-------+---------+
| score | Rank    |
+-------+---------+
| 4.00  | 1       |
| 4.00  | 1       |
| 3.85  | 2       |
|&nbsp;3.65  | 3       |
| 3.65  | 3       |
| 3.50  | 4       |
+-------+---------+
</pre>

<p><strong>Important Note:</strong>&nbsp;For MySQL solutions, to escape reserved words used as column names, you can use an apostrophe before and after the keyword. For example<strong>&nbsp;`Rank`</strong>.</p>

</div>

## Tags


## Companies
- Apple - 5 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Adobe - 4 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple, Short, Fast
- Author: StefanPochmann
- Creation Date: Sat Jun 13 2015 05:54:36 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 05:42:31 GMT+0800 (Singapore Standard Time)

<p>
These are four different solutions.

---
**With Variables:** 841 ms

First one uses two variables, one for the current rank and one for the previous score.

    SELECT
      Score,
      @rank := @rank + (@prev <> (@prev := Score)) Rank
    FROM
      Scores,
      (SELECT @rank := 0, @prev := -1) init
    ORDER BY Score desc

---

**Always Count:** 1322 ms

This one counts, for each score, the number of distinct greater or equal scores.

    SELECT
      Score,
      (SELECT count(distinct Score) FROM Scores WHERE Score >= s.Score) Rank
    FROM Scores s
    ORDER BY Score desc

---

**Always Count, Pre-uniqued:** 795 ms

Same as the previous one, but faster because I have a subquery that "uniquifies" the scores first. Not entirely sure *why* it's faster, I'm guessing MySQL makes `tmp` a temporary table and uses it for every outer Score.

    SELECT
      Score,
      (SELECT count(*) FROM (SELECT distinct Score s FROM Scores) tmp WHERE s >= Score) Rank
    FROM Scores
    ORDER BY Score desc

---

**Filter/count Scores^2:** 1414 ms

Inspired by the attempt in wangkan2001's answer. Finally `Id` is good for something :-)

    SELECT s.Score, count(distinct t.score) Rank
    FROM Scores s JOIN Scores t ON s.Score <= t.score
    GROUP BY s.Id
    ORDER BY s.Score desc
</p>


### MySQL Two Simple Solutions and Explanations for Beginners
- Author: sophiesu0827
- Creation Date: Sat Dec 21 2019 03:31:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Dec 21 2019 04:05:57 GMT+0800 (Singapore Standard Time)

<p>
# **1. MySQL Solution: (777ms)**
```
SELECT S.Score, COUNT(S2.Score) AS Rank FROM Scores S,
(SELECT DISTINCT Score FROM Scores) S2
WHERE S.Score<=S2.Score
GROUP BY S.Id 
ORDER BY S.Score DESC;
```
![image](https://assets.leetcode.com/users/sophiesu0827/image_1576871668.png)




# **2. Optional MySQL solution using Variables: (397ms)**
```
SELECT Score, convert(Rank,SIGNED) AS Rank FROM
    (SELECT Score, @rank:=CASE WHEN Score=@previous THEN @rank ELSE @rank+1 END AS Rank, @previous:=Score FROM Scores,
        (SELECT @previous:=-1,@rank:=0) AS initial
    ORDER BY Score DESC) A;  
```
**Intuition:** 
First we sort the table by descending scores. We check each score to see if it is equal to or lower than the previous score. If equal, keep the same rank; If lower, increase the rank by 1.

**Yes! We\'ve found that using Variables would save half of the running time.**

</p>


### Accepted solution using InnerJoin and GroupBy
- Author: dragonmigo
- Creation Date: Tue Jan 13 2015 06:59:21 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 22:39:32 GMT+0800 (Singapore Standard Time)

<p>
    SELECT Scores.Score, COUNT(Ranking.Score) AS RANK
      FROM Scores
         , (
           SELECT DISTINCT Score
             FROM Scores
           ) Ranking
     WHERE Scores.Score <= Ranking.Score
     GROUP BY Scores.Id, Scores.Score
     ORDER BY Scores.Score DESC;
</p>


