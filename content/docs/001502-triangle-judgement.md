---
title: "Triangle Judgement"
weight: 1502
#id: "triangle-judgement"
---
## Description
<div class="description">
A pupil Tim gets homework to identify whether three line segments could possibly form a triangle.
<p>&nbsp;</p>
However, this assignment is very heavy because there are hundreds of records to calculate.

<p>&nbsp;</p>
Could you help Tim by writing a query to judge whether these three sides can form a triangle, assuming table <code>triangle</code> holds the length of the three sides x, y and z.

<p>&nbsp;</p>

<pre>
| x  | y  | z  |
|----|----|----|
| 13 | 15 | 30 |
| 10 | 20 | 15 |
</pre>
For the sample data above, your query should return the follow result:

<pre>
| x  | y  | z  | triangle |
|----|----|----|----------|
| 13 | 15 | 30 | No       |
| 10 | 20 | 15 | Yes      |
</pre>

</div>

## Tags


## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach: Using `case...when...` [Accepted]

**Algorithm**

In Math, three segments can form a triangle only if the sum of any of the two segments is larger than the third one.
(In other words, the subtraction of any of the two segments are smaller than the third one.)

So, we can use this knowledge to judge with the help of the MySQL control statements [`case...when...`](https://dev.mysql.com/doc/refman/5.7/en/case.html).

**MySQL**

```sql
SELECT 
    x,
    y,
    z,
    CASE
        WHEN x + y > z AND x + z > y AND y + z > x THEN 'Yes'
        ELSE 'No'
    END AS 'triangle'
FROM
    triangle
;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Using CASE WHEN
- Author: tucaoneo
- Creation Date: Tue Jun 06 2017 11:30:47 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 21:26:03 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT x, y, z,
CASE WHEN x+y<=z OR
          x+z<=y OR
          y+z<=x
     THEN 'No'
     ELSE 'Yes'
END AS 'triangle'
FROM triangle;
```
</p>


### Simple Answer Using IF() Function
- Author: richarddia
- Creation Date: Sun Jun 04 2017 13:58:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 04 2017 13:58:10 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT *, IF(x+y>z and x+z>y and y+z>x, 'Yes', 'No') as triangle FROM triangle
```
</p>


### simple solution
- Author: buchireddy
- Creation Date: Wed Feb 20 2019 13:06:02 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Feb 20 2019 13:06:02 GMT+0800 (Singapore Standard Time)

<p>
```
# Write your MySQL query statement below
select x,y,z,
case when x+y <= z THEN \'No\'
    when y+z <= x THEN \'No\' 
    when z+x <= y then \'No\' 
    else \'Yes\' END as `triangle`
from triangle;
     ```
</p>


