---
title: "Snapshot Array"
weight: 1122
#id: "snapshot-array"
---
## Description
<div class="description">
<p>Implement a SnapshotArray that supports the following interface:</p>

<ul>
	<li><code>SnapshotArray(int length)</code> initializes an array-like data structure with the given length.&nbsp; <strong>Initially, each element equals 0</strong>.</li>
	<li><code>void set(index, val)</code> sets the element at the given <code>index</code> to be equal to <code>val</code>.</li>
	<li><code>int snap()</code>&nbsp;takes a snapshot of the array and returns the <code>snap_id</code>: the total number of times we called <code>snap()</code> minus <code>1</code>.</li>
	<li><code>int get(index, snap_id)</code>&nbsp;returns the value at the given <code>index</code>, at the time we took the snapshot with the given <code>snap_id</code></li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> [&quot;SnapshotArray&quot;,&quot;set&quot;,&quot;snap&quot;,&quot;set&quot;,&quot;get&quot;]
[[3],[0,5],[],[0,6],[0,0]]
<strong>Output:</strong> [null,null,0,null,5]
<strong>Explanation: </strong>
SnapshotArray snapshotArr = new SnapshotArray(3); // set the length to be 3
snapshotArr.set(0,5);  // Set array[0] = 5
snapshotArr.snap();  // Take a snapshot, return snap_id = 0
snapshotArr.set(0,6);
snapshotArr.get(0,0);  // Get the value of array[0] with snap_id = 0, return 5</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= length&nbsp;&lt;= 50000</code></li>
	<li>At most <code>50000</code>&nbsp;calls will be made to <code>set</code>, <code>snap</code>, and <code>get</code>.</li>
	<li><code>0 &lt;= index&nbsp;&lt;&nbsp;length</code></li>
	<li><code>0 &lt;=&nbsp;snap_id &lt;&nbsp;</code>(the total number of times we call <code>snap()</code>)</li>
	<li><code>0 &lt;=&nbsp;val &lt;= 10^9</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Google - 3 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python] Binary Search
- Author: lee215
- Creation Date: Sun Aug 04 2019 12:02:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 15 2020 22:38:02 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Instead of copy the whole array,
we can only record the changes of `set`.
<br>

## **Explanation**
The idea is, the whole array can be large,
and we may take the `snap` tons of times.
(Like you may always ctrl + S twice)

Instead of record the history of the whole array,
we will record the history of each cell.
And this is the minimum space that we need to record all information.

For each `A[i]`, we will record its history.
With a `snap_id` and a its value.

When we want to `get` the value in history, just binary search the time point.
<br>

## **Complexity**
Time `O(logS)`
Space `O(S)`
where `S` is the number of `set` called.

`SnapshotArray(int length)` is `O(N)` time
`set(int index, int val)` is O(1) in Python and `O(logSnap)` in Java
`snap()` is `O(1)`
`get(int index, int snap_id)` is `O(logSnap)`
<br>

**Java**
```java
class SnapshotArray {
    TreeMap<Integer, Integer>[] A;
    int snap_id = 0;
    public SnapshotArray(int length) {
        A = new TreeMap[length];
        for (int i = 0; i < length; i++) {
            A[i] = new TreeMap<Integer, Integer>();
            A[i].put(0, 0);
        }
    }

    public void set(int index, int val) {
        A[index].put(snap_id, val);
    }

    public int snap() {
        return snap_id++;
    }

    public int get(int index, int snap_id) {
        return A[index].floorEntry(snap_id).getValue();
    }
}
```
**C++**
from @vikas007
```
class SnapshotArray {
private:
    map<int, map<int, int>> snaps;
    int snapId = 0;
public:
    SnapshotArray(int length) {
        for (int i = 0; i < length; i++) {
            map<int, int> mp; mp[0] = 0;
            snaps[i] = mp;
        }
    }
    
    void set(int index, int val) {
        snaps[index][snapId] = val;
    }
    
    int snap() {
        return snapId++;
    }
    
    int get(int index, int snap_id) {
        auto it = snaps[index].upper_bound(snap_id); it--;
        return it->second;
    }
};
```

**Python:**
```python
class SnapshotArray(object):

    def __init__(self, n):
        self.A = [[[-1, 0]] for _ in xrange(n)]
        self.snap_id = 0

    def set(self, index, val):
        self.A[index].append([self.snap_id, val])

    def snap(self):
        self.snap_id += 1
        return self.snap_id - 1

    def get(self, index, snap_id):
        i = bisect.bisect(self.A[index], [snap_id + 1]) - 1
        return self.A[index][i][1]
```

</p>


### C++ Hash Map + Vector
- Author: votrubac
- Creation Date: Sun Aug 04 2019 12:24:26 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 05 2019 05:27:38 GMT+0800 (Singapore Standard Time)

<p>
It\'s similar to [981. Time Based Key-Value Store](https://leetcode.com/problems/time-based-key-value-store/discuss/226664/C%2B%2B-3-lines-hash-map-%2B-map).

For each index, we store values in the map for each snap id. We maintain the current snap id and use it in ```set```. When we do ```snap```, we just increase snap id. Therefore, our map will only contain snap ids when the value was set.

For ```get```, we use binary search to find the most recent value for the requested snap id.

> Note that we can achieve O(1) complexity for the get operation, but it won\'t be memory-efficient if we populate all indices and then make a lot of snaps (without modifying much).
```
unordered_map<int, map<int, int>> a;
int cur_snap = 0;
int snap() { return cur_snap++; }
void set(int index, int val) { 
  a[index][cur_snap] = val; 
}
int get(int index, int snap_id) {
  auto it = a[index].upper_bound(snap_id);
  return it == begin(a[index]) ? 0 : prev(it)->second;
}
```
Since our timestamps are only increasing, we can use a vector instead of a map, though it\'s not as concise.
```
unordered_map<int, vector<pair<int, int>>> m;
int cur_snap = 0;
int snap() { return cur_snap++; }
void set(int index, int val) {
  if (m[index].empty() || m[index].back().first != cur_snap)
    m[index].push_back({ cur_snap, val });
  else m[index].back().second = val;
}
int get(int index, int snap_id) {
  auto it = upper_bound(begin(m[index]), end(m[index]), pair<int, int>(snap_id, INT_MAX));
  return it == begin(m[index]) ? 0 : prev(it)->second;
}
```
# Complexity analysis
Assuming ```n``` is the number of ```set``` + ```snap``` operations, and ```m``` is the number of get operations:
> Note: for the complexity analysis, we consider  ```set``` + ```snap``` as one operation. Otherwise, neither ```set``` nor ```snap``` alone increases the number of values we need to consider for the ```get``` operation.

- Time Complexity: 
    - Set: ```O(1)``` single operation, and total ```O(n)```.
    - Get: ```O(log n)``` for a single operation, and total ```O(m log n)```.
- Space Complexity: ```O(n)```
</p>


### [Java] O(1) snap O(logN) get/set using TreeMap
- Author: seafmch
- Creation Date: Sun Aug 04 2019 12:22:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 04 2019 12:23:30 GMT+0800 (Singapore Standard Time)

<p>
```
class SnapshotArray {
    
    List<TreeMap<Integer, Integer>> arr;
    int currId = 0;

    public SnapshotArray(int length) {
        arr = new ArrayList();
        
        for (int i = 0; i < length; i++) {
            arr.add(i, new TreeMap<Integer, Integer>());
            arr.get(i).put(0, 0);
        }
    }
    
    public void set(int index, int val) {
        arr.get(index).put(currId, val);
    }
    
    public int snap() {
        return currId++;
    }
    
    public int get(int index, int snap_id) {
        return arr.get(index).floorEntry(snap_id).getValue();
    }
}

/**
 * Your SnapshotArray object will be instantiated and called as such:
 * SnapshotArray obj = new SnapshotArray(length);
 * obj.set(index,val);
 * int param_2 = obj.snap();
 * int param_3 = obj.get(index,snap_id);
 */
```
</p>


