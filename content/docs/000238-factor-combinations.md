---
title: "Factor Combinations"
weight: 238
#id: "factor-combinations"
---
## Description
<div class="description">
<p>Numbers can be regarded as product of its factors. For example,</p>

<pre>
8 = 2 x 2 x 2;
  = 2 x 4.
</pre>

<p>Write a function that takes an integer <i>n</i> and return all possible combinations of its factors.</p>

<p><b>Note:</b></p>

<ol>
	<li>You may assume that <i>n</i> is always positive.</li>
	<li>Factors should be greater than 1 and less than <i>n</i>.</li>
</ol>

<p><b>Example&nbsp;1: </b></p>

<pre>
<strong>Input:</strong> <code>1</code>
<strong>Output:</strong> []
</pre>

<p><b>Example&nbsp;2: </b></p>

<pre>
<strong>Input:</strong> <code>37</code>
<strong>Output:</strong>[]</pre>

<p><b>Example&nbsp;3: </b></p>

<pre>
<strong>Input:</strong> <code>12</code>
<strong>Output:</strong>
[
  [2, 6],
  [2, 2, 3],
  [3, 4]
]</pre>

<p><b>Example&nbsp;4: </b></p>

<pre>
<strong>Input:</strong> <code>32</code>
<strong>Output:</strong>
[
  [2, 16],
  [2, 2, 8],
  [2, 2, 2, 4],
  [2, 2, 2, 2, 2],
  [2, 4, 4],
  [4, 8]
]
</pre>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- LinkedIn - 9 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: false)
- Uber - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My Recursive DFS Java Solution
- Author: lc_cl_aaa
- Creation Date: Tue Aug 11 2015 06:54:32 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 13:45:45 GMT+0800 (Singapore Standard Time)

<p>
    public List<List<Integer>> getFactors(int n) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        helper(result, new ArrayList<Integer>(), n, 2);
        return result;
    }
    
    public void helper(List<List<Integer>> result, List<Integer> item, int n, int start){
        if (n <= 1) {
            if (item.size() > 1) {
                result.add(new ArrayList<Integer>(item));
            }
            return;
        }
        
        for (int i = start; i <= n; ++i) {
            if (n % i == 0) {
                item.add(i);
                helper(result, item, n/i, i);
                item.remove(item.size()-1);
            }
        }
    }
</p>


### A simple java solution
- Author: hahadaxiong
- Creation Date: Fri Sep 18 2015 06:42:09 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 07:35:26 GMT+0800 (Singapore Standard Time)

<p>
    public List<List<Integer>> getFactors(int n) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        if (n <= 3) return result;
        helper(n, -1, result, new ArrayList<Integer>());
        return result; 
    }
    
    public void helper(int n, int lower, List<List<Integer>> result, List<Integer> cur) {
        if (lower != -1) {
            cur.add(n);
            result.add(new ArrayList<Integer>(cur));
            cur.remove(cur.size() - 1);
        }
        int upper = (int) Math.sqrt(n);
        for (int i = Math.max(2, lower); i <= upper; ++i) {
            if (n % i == 0) {
                cur.add(i);
                helper(n / i, i, result, cur);
                cur.remove(cur.size() - 1);
            }
        }
    }
</p>


### Iterative and Recursive Python
- Author: StefanPochmann
- Creation Date: Tue Aug 11 2015 08:17:48 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 15:37:36 GMT+0800 (Singapore Standard Time)

<p>
**Iterative:**

    def getFactors(self, n):
        todo, combis = [(n, 2, [])], []
        while todo:
            n, i, combi = todo.pop()
            while i * i <= n:
                if n % i == 0:
                    combis += combi + [i, n/i],
                    todo += (n/i, i, combi+[i]),
                i += 1
        return combis

**Recursive:**

    def getFactors(self, n):
        def factor(n, i, combi, combis):
            while i * i <= n:
                if n % i == 0:
                    combis += combi + [i, n/i],
                    factor(n/i, i, combi+[i], combis)
                i += 1
            return combis
        return factor(n, 2, [], [])
</p>


