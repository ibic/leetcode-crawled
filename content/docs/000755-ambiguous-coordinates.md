---
title: "Ambiguous Coordinates"
weight: 755
#id: "ambiguous-coordinates"
---
## Description
<div class="description">
<p>We had some 2-dimensional coordinates, like <code>&quot;(1, 3)&quot;</code> or <code>&quot;(2, 0.5)&quot;</code>.&nbsp; Then, we removed&nbsp;all commas, decimal points, and spaces, and ended up with the string&nbsp;<code>S</code>.&nbsp; Return a list of strings representing&nbsp;all possibilities for what our original coordinates could have been.</p>

<p>Our original representation never had extraneous zeroes, so we never started with numbers like &quot;00&quot;, &quot;0.0&quot;, &quot;0.00&quot;, &quot;1.0&quot;, &quot;001&quot;, &quot;00.01&quot;, or any other number that can be represented with&nbsp;less digits.&nbsp; Also, a decimal point within a number never occurs without at least one digit occuring before it, so we never started with numbers like &quot;.1&quot;.</p>

<p>The final answer list can be returned in any order.&nbsp; Also note that all coordinates in the final answer&nbsp;have exactly one space between them (occurring after the comma.)</p>

<pre>
<strong>Example 1:</strong>
<strong>Input:</strong> &quot;(123)&quot;
<strong>Output:</strong> [&quot;(1, 23)&quot;, &quot;(12, 3)&quot;, &quot;(1.2, 3)&quot;, &quot;(1, 2.3)&quot;]
</pre>

<pre>
<strong>Example 2:</strong>
<strong>Input:</strong> &quot;(00011)&quot;
<strong>Output:</strong> &nbsp;[&quot;(0.001, 1)&quot;, &quot;(0, 0.011)&quot;]
<strong>Explanation:</strong> 
0.0, 00, 0001 or 00.01 are not allowed.
</pre>

<pre>
<strong>Example 3:</strong>
<strong>Input:</strong> &quot;(0123)&quot;
<strong>Output:</strong> [&quot;(0, 123)&quot;, &quot;(0, 12.3)&quot;, &quot;(0, 1.23)&quot;, &quot;(0.1, 23)&quot;, &quot;(0.1, 2.3)&quot;, &quot;(0.12, 3)&quot;]
</pre>

<pre>
<strong>Example 4:</strong>
<strong>Input:</strong> &quot;(100)&quot;
<strong>Output:</strong> [(10, 0)]
<strong>Explanation:</strong> 
1.0 is not allowed.
</pre>

<p>&nbsp;</p>

<p><strong>Note: </strong></p>

<ul>
	<li><code>4 &lt;= S.length &lt;= 12</code>.</li>
	<li><code>S[0]</code> = &quot;(&quot;, <code>S[S.length - 1]</code> = &quot;)&quot;, and the other elements in <code>S</code> are digits.</li>
</ul>

<p>&nbsp;</p>

</div>

## Tags
- String (string)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Cartesian Product [Accepted]

**Intuition and Algorithm**

For each place to put the comma, we separate the string into two fragments.  For example, with a string like `"1234"`, we could separate it into fragments `"1" and "234"`, `"12" and "34"`, or `"123"` and `"4"`.

Then, for each fragment, we have a choice of where to put the period, to create a list `make(...)` of choices.  For example, `"123"` could be made into `"1.23"`, `"12.3"`, or `"123"`.

Because of extranneous zeroes, we should ignore possibilities where the part of the fragment to the `left` of the decimal starts with `"0"` (unless it is exactly `"0"`), and ignore possibilities where the part of the fragment to the `right` of the decimal ends with `"0"`, as these are not allowed.

Note that this process could result in an empty answer, such as for the case `S = "(000)"`.

<iframe src="https://leetcode.com/playground/QTRoMo9y/shared" frameBorder="0" width="100%" height="463" name="QTRoMo9y"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^3)$$, where $$N$$ is the length `S`.  We evaluate the sum $$O(\sum_k k(N-k))$$.

* Space Complexity: $$O(N^3)$$, to store the answer.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Solution with Explanation
- Author: lee215
- Creation Date: Sun Apr 15 2018 11:10:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 14:48:30 GMT+0800 (Singapore Standard Time)

<p>
We can split S to two parts for two coordinates.
Then we use sub function ```f``` to find all possible strings for each coordinate.

**In sub functon f(S)**
if S == "": return []
if S == "0": return [S]
if S == "0XXX0": return []
if S == "0XXX": return ["0.XXX"]
if S == "XXX0": return [S]
return [S, "X.XXX", "XX.XX", "XXX.X"...]

Then we add the product of two lists to result.

**Time complexity**
O(N^3) with N <= 10

Provement:
![image](https://s3-lc-upload.s3.amazonaws.com/users/lee215/image_1523920967.png)


C++:
```
    vector<string> ambiguousCoordinates(string S) {
        int n = S.size();
        vector<string> res;
        for (int i = 1; i < n - 2; ++i) {
            vector<string> A = f(S.substr(1, i)), B = f(S.substr(i + 1, n - 2 - i));
            for (auto & a : A) for (auto & b : B) res.push_back("(" + a + ", " + b + ")");
        }
        return res;
    }
    vector<string> f(string S) {
        int n = S.size();
        if (n == 0 || (n > 1 && S[0] == \'0\' && S[n - 1] == \'0\')) return {};
        if (n > 1 && S[0] == \'0\') return {"0." + S.substr(1)};
        if (n == 1 || S[n - 1] == \'0\') return {S};
        vector<string> res = {S};
        for (int i = 1; i < n; ++i) res.push_back(S.substr(0, i) + \'.\' + S.substr(i));
        return res;
    }
```
**Java:**
```
    public List<String> ambiguousCoordinates(String S) {
        int n = S.length();
        List<String> res = new ArrayList();
        for (int i = 1; i < n - 2; ++i) {
            List<String> A = f(S.substring(1, i + 1)), B = f(S.substring(i + 1, n - 1));
            for (String a : A) for (String b : B) res.add("(" + a + ", " + b + ")");
        }
        return res;
    }
    public List<String> f(String S) {
        int n = S.length();
        List<String> res = new ArrayList();
        if (n == 0 || (n > 1 && S.charAt(0) == \'0\' && S.charAt(n - 1) == \'0\')) return res;
        if (n > 1 && S.charAt(0) == \'0\') {
            res.add("0." + S.substring(1));
            return res;
        }
        res.add(S);
        if (n == 1 || S.charAt(n - 1) == \'0\') return res;
        for (int i = 1; i < n; ++i) res.add(S.substring(0, i) + \'.\' + S.substring(i));
        return res;
    }
```
**Python:**
```
    def ambiguousCoordinates(self, S):
        S = S[1:-1]
        def f(S):
            if not S or len(S) > 1 and S[0] == S[-1] == \'0\': return []
            if S[-1] == \'0\': return [S]
            if S[0] == \'0\': return [S[0] + \'.\' + S[1:]]
            return [S] + [S[:i] + \'.\' + S[i:] for i in range(1, len(S))]
        return [\'(%s, %s)\' % (a, b) for i in range(len(S)) for a, b in itertools.product(f(S[:i]), f(S[i:]))]
```
</p>


### Really clear Java code
- Author: wangzi6147
- Creation Date: Sun Apr 15 2018 11:39:01 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 10 2018 16:30:40 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public List<String> ambiguousCoordinates(String S) {
        S = S.substring(1, S.length() - 1);
        List<String> result = new LinkedList<>();
        for (int i = 1; i < S.length(); i++) {
            List<String> left = allowed(S.substring(0, i));
            List<String> right = allowed(S.substring(i));
            for (String ls : left) {
                for (String rs : right) {
                    result.add("(" + ls + ", " + rs + ")");
                }
            }
        }
        return result;
    }
    private List<String> allowed(String s) {
        int l = s.length();
        char[] cs = s.toCharArray();
        List<String> result = new LinkedList<>();
        if (cs[0] == \'0\' && cs[l - 1] == \'0\') { // "0xxxx0" Invalid unless a single "0"
            if (l == 1) {
                result.add("0");
            }
            return result;
        }
        if (cs[0] == \'0\') { // "0xxxxx" The only valid result is "0.xxxxx"
            result.add("0." + s.substring(1));
            return result;
        }
        if (cs[l - 1] == \'0\') { // "xxxxx0" The only valid result is itself
            result.add(s);
            return result;
        }
        result.add(s); // Add itself
        for (int i = 1; i < l; i++) { // "xxxx" -> "x.xxx", "xx.xx", "xxx.x"
            result.add(s.substring(0, i) + \'.\' + s.substring(i));
        }
        return result;
    }
}
```
</p>


### Concise C++ solution with comments
- Author: mzchen
- Creation Date: Sun Apr 15 2018 11:52:48 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 06 2018 08:30:46 GMT+0800 (Singapore Standard Time)

<p>
```
vector<string> cases(string &&s) {
    if (s.size() == 1) // single digit
        return {s};
    if (s.front() == \'0\') { // 0xxx
        if (s.back() == \'0\') // 0xxx0
            return {};
        return {"0." + s.substr(1)}; // 0xxx9
    }
    if (s.back() == \'0\') // 9xxx0
        return {s};
    vector<string> res{s}; // 9xxx9
    for (int i = 1; i < s.size(); i++)
        res.emplace_back(s.substr(0, i) + "." + s.substr(i));
    return res;
}

vector<string> ambiguousCoordinates(string S) {
    vector<string> res;
    for (int i = 2; i < S.size() - 1; i++) // position of comma
    for (auto &x : cases(S.substr(1, i - 1)))
    for (auto &y : cases(S.substr(i, S.size() - i - 1)))
        res.emplace_back("(" + x + ", " + y + ")");
    return res;
}
```
</p>


