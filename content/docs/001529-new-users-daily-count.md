---
title: "New Users Daily Count"
weight: 1529
#id: "new-users-daily-count"
---
## Description
<div class="description">
<p>Table: <code>Traffic</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| user_id       | int     |
| activity      | enum    |
| activity_date | date    |
+---------------+---------+
There is no primary key for this table, it may have duplicate rows.
The activity column is an ENUM type of (&#39;login&#39;, &#39;logout&#39;, &#39;jobs&#39;, &#39;groups&#39;, &#39;homepage&#39;).
</pre>

<p>&nbsp;</p>

<p>Write an SQL query that reports for every date within at most&nbsp;<strong>90 days</strong> from today, the number of users that logged in for the first time on that date. Assume today is <strong>2019-06-30</strong>.</p>

<p>The query result format is in the following example:</p>

<pre>
Traffic table:
+---------+----------+---------------+
| user_id | activity | activity_date |
+---------+----------+---------------+
| 1       | login    | 2019-05-01    |
| 1       | homepage | 2019-05-01    |
| 1       | logout   | 2019-05-01    |
| 2       | login    | 2019-06-21    |
| 2       | logout   | 2019-06-21    |
| 3       | login    | 2019-01-01    |
| 3       | jobs     | 2019-01-01    |
| 3       | logout   | 2019-01-01    |
| 4       | login    | 2019-06-21    |
| 4       | groups   | 2019-06-21    |
| 4       | logout   | 2019-06-21    |
| 5       | login    | 2019-03-01    |
| 5       | logout   | 2019-03-01    |
| 5       | login    | 2019-06-21    |
| 5       | logout   | 2019-06-21    |
+---------+----------+---------------+

Result table:
+------------+-------------+
| login_date | user_count  |
+------------+-------------+
| 2019-05-01 | 1           |
| 2019-06-21 | 2           |
+------------+-------------+
Note that we only care about dates with non zero user count.
The user with id 5 first logged in on 2019-03-01 so he&#39;s not counted on 2019-06-21.
</pre>

</div>

## Tags


## Companies
- LinkedIn - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL Solution
- Author: bradcc
- Creation Date: Thu Jul 04 2019 10:47:31 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 04 2019 10:47:31 GMT+0800 (Singapore Standard Time)

<p>
```
select login_date, count(1) user_count
from
(select user_id, min(activity_date) login_date
from traffic
where activity = \'login\'
group by user_id) a
where login_date between date_add(\'2019-06-30\', interval -90 day) and \'2019-06-30\'
group by login_date
```
</p>


### Intuitive and fast MySQL solution
- Author: xiaozhouxiao
- Creation Date: Fri Aug 30 2019 09:30:33 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 30 2019 09:30:33 GMT+0800 (Singapore Standard Time)

<p>
* First find each user\'s original login date
* Then find dates within 90 days, and count user for each date
```
select login_date, count(user_id) as user_count
from
(select user_id, min(activity_date) as login_date
from Traffic
where activity = \'login\'
group by user_id) t
where datediff(\'2019-06-30\', login_date) <= 90
group by login_date
```
</p>


### SQL Server Clear Logic Solution
- Author: kevindincs
- Creation Date: Sat Jul 06 2019 11:13:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 06 2019 11:13:07 GMT+0800 (Singapore Standard Time)

<p>
with fir_log_user as 
(
select user_id, min(activity_date) as fir_date
from Traffic
where activity = \'login\' group by user_id
)

select fir_date as login_date, count(distinct user_id) as user_count
from fir_log_user
where fir_date>= \'2019-04-01\'
group by fir_date
order by fir_date
</p>


