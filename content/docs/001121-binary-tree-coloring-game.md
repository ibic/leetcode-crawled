---
title: "Binary Tree Coloring Game"
weight: 1121
#id: "binary-tree-coloring-game"
---
## Description
<div class="description">
<p>Two players play a turn based game on a binary tree.&nbsp; We are given&nbsp;the <code>root</code> of this binary tree, and the number of nodes <code>n</code>&nbsp;in the tree.&nbsp; <code>n</code> is odd, and&nbsp;each node has a distinct value from <code>1</code> to <code>n</code>.</p>

<p>Initially, the first player names a value <code>x</code> with <code>1 &lt;= x &lt;= n</code>, and the second player names a value <code>y</code> with <code>1 &lt;= y &lt;= n</code> and <code>y != x</code>.&nbsp; The first player colors the node with value <code>x</code> red, and the second player colors the node with value <code>y</code> blue.</p>

<p>Then, the players take turns starting with the first player.&nbsp; In each turn, that player chooses a node of their color (red if player 1, blue if player 2) and colors an <strong>uncolored</strong> neighbor of the chosen node (either the left child, right child, or parent of the chosen node.)</p>

<p>If (and only if)&nbsp;a player cannot choose such a node in this way, they must pass their turn.&nbsp; If both players pass their turn, the game ends, and the winner is the player that colored more nodes.</p>

<p>You are the second player.&nbsp; If it is possible to choose such a <code>y</code>&nbsp;to ensure you win the game, return <code>true</code>.&nbsp; If it is not possible, return <code>false</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/08/01/1480-binary-tree-coloring-game.png" style="width: 300px; height: 186px;" />
<pre>
<strong>Input:</strong> root = [1,2,3,4,5,6,7,8,9,10,11], n = 11, x = 3
<strong>Output:</strong> true
<strong>Explanation: </strong>The second player can choose the node with value 2.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>root</code> is the root of a binary tree with <code>n</code> nodes and distinct node values from <code>1</code> to <code>n</code>.</li>
	<li><code>n</code> is odd.</li>
	<li><code>1 &lt;= x &lt;= n&nbsp;&lt;= 100</code></li>
</ul>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Google - 2 (taggedByAdmin: true)
- Bloomberg - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Simple recursion and Follow-Up
- Author: lee215
- Creation Date: Sun Aug 04 2019 12:02:55 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 23 2019 01:23:33 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
The first player colors a node,
there are at most 3 nodes connected to this node.
Its left, its right and its parent.
Take this 3 nodes as the root of 3 subtrees.

The second player just color any one root,
and the whole subtree will be his.
And this is also all he can take,
since he cannot cross the node of the first player.
<br>

# **Explanation**
`count` will recursively count the number of nodes,
in the left and in the right.
`n - left - right` will be the number of nodes in the "subtree" of parent.
Now we just need to compare `max(left, right, parent)` and `n / 2`
<br>

# **Complexity**
Time `O(N)`
Space `O(height)` for recursion
<br>

**Java:**
```java
    int left, right, val;
    public boolean btreeGameWinningMove(TreeNode root, int n, int x) {
        val = x;
        count(root);
        return Math.max(Math.max(left, right), n - left - right - 1) > n / 2;
    }

    private int count(TreeNode node) {
        if (node == null) return 0;
        int l = count(node.left), r = count(node.right);
        if (node.val == val) {
            left = l;
            right = r;
        }
        return l + r + 1;
    }
```

**C++:**
```cpp
    int left, right, val;
    bool btreeGameWinningMove(TreeNode* root, int n, int x) {
        val = x, n = count(root);
        return max(max(left, right), n - left - right - 1) > n / 2;
    }

    int count(TreeNode* node) {
        if (!node) return 0;
        int l = count(node->left), r = count(node->right);
        if (node->val == val)
            left = l, right = r;
        return l + r + 1;
    }
```

**Python:**
```python
    def btreeGameWinningMove(self, root, n, x):
        c = [0, 0]
        def count(node):
            if not node: return 0
            l, r = count(node.left), count(node.right)
            if node.val == x:
                c[0], c[1] = l, r
            return l + r + 1
        return count(root) / 2 < max(max(c), n - sum(c) - 1)
```

# **Fun Moment of Follow-up**:
Alex and Lee are going to play this turned based game.
Alex draw the whole tree. `root` and `n` will be given.
Note the `n` will be odd, so no tie in the end.

Now Lee says that, this time he wants to color the node first.
1. Return `true` if Lee can ensure his win, otherwise return `false`

2. Could you find the set all the nodes,
that Lee can ensure he wins the game?
Return the size of this set.

3. What is the complexity of your solution?


# Solution to the follow up 1

Yes, similar to the solution [877. Stone Game](https://leetcode.com/problems/stone-game/discuss/154610/DP-or-Just-return-true)
Just return true.
But this time, Lee\'s turn to ride shotgun today! Bravo.

**Java/C++**
```java
    return true;
```
**Python:**
```python
    return True
```

# Solution to the follow up 2
There will one and only one node in the tree,
that can Lee\'s win.

**Java/C++**
```java
    return 1
```
**Python:**
```python
    return 1
```

# Solution to the follow up 3
It can be solve in `O(N)`.

</p>


### Easy to understand for everyone
- Author: mudin
- Creation Date: Sun Aug 04 2019 13:31:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 04 2019 13:31:27 GMT+0800 (Singapore Standard Time)

<p>
Count left and right children\'s nodes of the player 1\'s initial node with value `x`. Lets call `countLeft` and `countRight`.
1. if  `countLeft` or `countRight` are bigger than `n/2`, player 2 chooses this child of the node and will win.
2. If `countLeft + countRight + 1` is smaller than `n/2`, player 2 chooses the parent of the node and will win;
3. otherwise, player 2 has not chance to win.

```
var btreeGameWinningMove = function(root, n, x) {
    
    let leftCount = 0;
    let rightCount = 0;
    
    function countNodes(t) {
        if(!t) return 0;
        let l = countNodes(t.left);
        let r = countNodes(t.right);
        if (t.val == x) {
            leftCount = l;
            rightCount = r;
        }
        return l + r + 1;
    }
    
    countNodes(root); // calculate node count
    
    // if player2 chooses player1\'s parent node and payer1 node\'s count is smaller than n/2, playr2 will win
    if(leftCount+rightCount+1<n/2) return true;
    
    // if player2 chooses player1\'s left or right node and its count is bigger than n/2, playr2 will win
    if(leftCount>n/2 || rightCount>n/2) return true;
    
    return false;
};
```
</p>


### c++,0ms, modular, beats 100% (both time and memory) with algo and image
- Author: goelrishabh5
- Creation Date: Sun Aug 04 2019 12:51:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 04 2019 13:23:19 GMT+0800 (Singapore Standard Time)

<p>
The second player will pick y as either left child, right child or parent (depending on which one has max nodes in their vicinity) of the node picked by first player.
If the no of nodes available for second player is greater than first, he wins 
Note : Equal case will never arise since n is odd

For example : 
![image](https://assets.leetcode.com/users/goelrishabh5/image_1564894633.png) ![image](https://assets.leetcode.com/users/goelrishabh5/image_1564894011.png) **parent(b:4)** ![image](https://assets.leetcode.com/users/goelrishabh5/image_1564894063.png) **right child (b:3)** ![image](https://assets.leetcode.com/users/goelrishabh5/image_1564894297.png) **left child (b:9)**    

Here, blue wins since it will pick left child. Thus, 9 will be left for blue and for red , 8 will be left

```
TreeNode* findNode(TreeNode* root,int x)
    {
        TreeNode* temp = NULL;
        if(root)
        {
            if(root->val==x)
                temp = root;
            else
            {
            temp = findNode(root->left,x);
            if(!temp)
            temp = findNode(root->right,x);
            }
        }
        return temp;
    }
    
    void countChildren(TreeNode* root, int &no)
    {
        if(root)
        {
            no++;
            countChildren(root->left,no);
            countChildren(root->right,no);    
        }
    }
    
    bool btreeGameWinningMove(TreeNode* root, int n, int x) {
        
        int left=0,right=0,parent=0,blue=0;
        TreeNode* temp = findNode(root,x); // find node
        countChildren(temp->left,left);  // count no of nodes in left subtree
        countChildren(temp->right,right); // count no of nodes in right subtree
        blue=max(left,right);
        if(temp->val!=root->val)
            parent = n-(left+right+1); // count no of nodes apart from given node\'s subtree
        blue=max(blue,parent);
        
        return (blue)>(n-blue);
    }
```
</p>


