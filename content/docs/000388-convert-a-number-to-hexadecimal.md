---
title: "Convert a Number to Hexadecimal"
weight: 388
#id: "convert-a-number-to-hexadecimal"
---
## Description
<div class="description">
<p>
Given an integer, write an algorithm to convert it to hexadecimal. For negative integer, <a href="https://en.wikipedia.org/wiki/Two%27s_complement" target="_blank">two’s complement</a> method is used.
</p>

<p><b>Note:</b>
<ol>
<li>All letters in hexadecimal (<code>a-f</code>) must be in lowercase.</li>
<li>The hexadecimal string must not contain extra leading <code>0</code>s. If the number is zero, it is represented by a single zero character <code>'0'</code>; otherwise, the first character in the hexadecimal string will not be the zero character.</li>
<li>The given number is guaranteed to fit within the range of a 32-bit signed integer.</li>
<li>You <b>must not use <i>any</i> method provided by the library</b> which converts/formats the number to hex directly.</li>
</ol>
</p>

<p><b>Example 1:</b>
<pre>
Input:
26

Output:
"1a"
</pre>
</p>

<p><b>Example 2:</b>
<pre>
Input:
-1

Output:
"ffffffff"
</pre>
</p>
</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java solution with comment
- Author: DonaldTrump
- Creation Date: Sun Sep 25 2016 12:05:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 17:54:32 GMT+0800 (Singapore Standard Time)

<p>
```
/*
Basic idea: each time we take a look at the last four digits of
            binary verion of the input, and maps that to a hex char
            shift the input to the right by 4 bits, do it again
            until input becomes 0.

*/

public class Solution {
    
    char[] map = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
    
    public String toHex(int num) {
        if(num == 0) return "0";
        String result = "";
        while(num != 0){
            result = map[(num & 15)] + result; 
            num = (num >>> 4);
        }
        return result;
    }
    
    
}````
</p>


### easy 10-line python solution with inline explanation
- Author: cbmbbz
- Creation Date: Thu Oct 06 2016 08:40:48 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 11 2018 12:18:46 GMT+0800 (Singapore Standard Time)

<p>
```
    def toHex(self, num):
        if num==0: return '0'
        mp = '0123456789abcdef'  # like a map
        ans = ''
        for i in range(8):
            n = num & 15       # this means num & 1111b
            c = mp[n]          # get the hex char 
            ans = c + ans
            num = num >> 4
        return ans.lstrip('0')  #strip leading zeroes
```
</p>


### Concise C++ Solution
- Author: lookbackinanger
- Creation Date: Mon Sep 26 2016 01:07:08 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 19 2018 20:36:16 GMT+0800 (Singapore Standard Time)

<p>
```
const string HEX = "0123456789abcdef";
class Solution {
public:
    string toHex(int num) {
        if (num == 0) return "0";
        string result;
        int count = 0;
        while (num && count++ < 8) {
            result = HEX[(num & 0xf)] + result;
            num >>= 4;
        }
        return result;
    }
};
```
</p>


