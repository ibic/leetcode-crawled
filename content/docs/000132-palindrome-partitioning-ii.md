---
title: "Palindrome Partitioning II"
weight: 132
#id: "palindrome-partitioning-ii"
---
## Description
<div class="description">
<p>Given a string <code>s</code>, partition <code>s</code> such that every substring of the partition is a palindrome.</p>

<p>Return <em>the minimum cuts needed</em> for a palindrome partitioning of <code>s</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aab&quot;
<strong>Output:</strong> 1
<strong>Explanation:</strong> The palindrome partitioning [&quot;aa&quot;,&quot;b&quot;] could be produced using 1 cut.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;a&quot;
<strong>Output:</strong> 0
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;ab&quot;
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 2000</code></li>
	<li><code>s</code> consists of lower-case English letters only.</li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My solution does not need a table for palindrome, is it right ? It uses only O(n) space.
- Author: tqlong
- Creation Date: Wed Aug 20 2014 10:21:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 00:20:23 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        int minCut(string s) {
            int n = s.size();
            vector<int> cut(n+1, 0);  // number of cuts for the first k characters
            for (int i = 0; i <= n; i++) cut[i] = i-1;
            for (int i = 0; i < n; i++) {
                for (int j = 0; i-j >= 0 && i+j < n && s[i-j]==s[i+j] ; j++) // odd length palindrome
                    cut[i+j+1] = min(cut[i+j+1],1+cut[i-j]);
    
                for (int j = 1; i-j+1 >= 0 && i+j < n && s[i-j+1] == s[i+j]; j++) // even length palindrome
                    cut[i+j+1] = min(cut[i+j+1],1+cut[i-j+1]);
            }
            return cut[n];
        }
    };
</p>


### Easiest Java DP Solution (97.36%)
- Author: yavinci
- Creation Date: Mon Dec 28 2015 21:50:02 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 14:11:10 GMT+0800 (Singapore Standard Time)

<p>
This can be solved by two points:

 1. `cut[i]` is the minimum of `cut[j - 1] + 1 (j <= i)`, if `[j, i]` is palindrome.
 2. If  `[j, i]` is palindrome, `[j + 1, i - 1]` is palindrome, and `c[j]  == c[i]`.

The 2nd point reminds us of using dp (caching).

    a   b   a   |   c  c
                    j  i
           j-1  |  [j, i] is palindrome
       cut(j-1) +  1


Hope it helps!

    public int minCut(String s) {
        char[] c = s.toCharArray();
        int n = c.length;
        int[] cut = new int[n];
        boolean[][] pal = new boolean[n][n];
        
        for(int i = 0; i < n; i++) {
            int min = i;
            for(int j = 0; j <= i; j++) {
                if(c[j] == c[i] && (j + 1 > i - 1 || pal[j + 1][i - 1])) {
                    pal[j][i] = true;  
                    min = j == 0 ? 0 : Math.min(min, cut[j - 1] + 1);
                }
            }
            cut[i] = min;
        }
        return cut[n - 1];
    }
</p>


### My DP Solution ( explanation and code)
- Author: heiyanbin
- Creation Date: Thu Jun 26 2014 18:00:36 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 12:33:15 GMT+0800 (Singapore Standard Time)

<p>
Calculate and maintain 2 DP states: 

 1. pal[i][j] , which is whether s[i..j] forms a pal
 
 2.  d[i], which
        is the minCut for s[i..n-1]

Once we comes to a pal[i][j]==true:

 - if j==n-1, the string s[i..n-1] is a Pal, minCut is 0, d[i]=0; 
 - else: the current cut num (first cut s[i..j] and then cut the rest
   s[j+1...n-1]) is 1+d[j+1], compare it to the exisiting minCut num
   d[i], repalce if smaller.

d[0] is the answer.

     class Solution {
        public:
            int minCut(string s) {
                if(s.empty()) return 0;
                int n = s.size();
                vector<vector<bool>> pal(n,vector<bool>(n,false));
                vector<int> d(n);
                for(int i=n-1;i>=0;i--)
                {
                    d[i]=n-i-1;
                    for(int j=i;j<n;j++)
                    {
                        if(s[i]==s[j] && (j-i<2 || pal[i+1][j-1]))
                        {
                           pal[i][j]=true;
                           if(j==n-1)
                               d[i]=0;
                           else if(d[j+1]+1<d[i])
                               d[i]=d[j+1]+1;
                        }
                    }
                }
                return d[0];
            }
        };
</p>


