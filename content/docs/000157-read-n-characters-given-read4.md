---
title: "Read N Characters Given Read4"
weight: 157
#id: "read-n-characters-given-read4"
---
## Description
<div class="description">
<p>Given a file and assume that you can only read the file using a given method&nbsp;<code>read4</code>, implement a method to read <em>n</em> characters.</p>

<p>&nbsp;</p>

<p><b>Method read4: </b></p>

<p>The API&nbsp;<code>read4</code> reads 4 consecutive characters from the file, then writes those characters into the buffer array <code>buf</code>.</p>

<p>The return value is the number of actual characters read.</p>

<p>Note that&nbsp;<code>read4()</code> has its own file pointer, much like <code>FILE *fp</code> in C.</p>

<p><b>Definition of read4:</b></p>

<pre>
    Parameter:  char[] buf4
    Returns:    int

Note: buf4[] is destination not source, the results from read4 will be copied to buf4[]
</pre>

<p>Below is a high level example of how <code>read4</code> works:</p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/07/01/157_example.png" style="width: 600px; height: 403px;" /></p>

<pre>
<code>File file(&quot;</code>abcde<code>&quot;); // File is &quot;</code>abcde<code>&quot;, initially file pointer (fp) points to &#39;a&#39;
char[] buf4 = new char[4]; // Create buffer with enough space to store characters
read4(buf4); // read4 returns 4. Now buf = &quot;abcd&quot;, fp points to &#39;e&#39;
read4(buf4); // read4 returns 1. Now buf = &quot;e&quot;, fp points to end of file
read4(buf4); // read4 returns 0. Now buf = &quot;&quot;, fp points to end of file</code>
</pre>

<p>&nbsp;</p>

<p><strong>Method read:</strong></p>

<p>By using the <code>read4</code> method, implement the method&nbsp;<code>read</code> that reads <i>n</i> characters from the file and store it in the&nbsp;buffer array&nbsp;<code>buf</code>. Consider that you <strong>cannot</strong> manipulate the file directly.</p>

<p>The return value is the number of actual characters read.</p>

<p><b>Definition of read: </b></p>

<pre>
    Parameters:	char[] buf, int n
    Returns:	int

Note: buf[] is destination not source, you will need to write the results to buf[]
</pre>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>file = &quot;abc&quot;, n = 4
<strong>Output: </strong>3
<strong>Explanation:</strong>&nbsp;After calling your read method, buf should contain &quot;abc&quot;. We read a total of 3 characters from the file, so return 3. Note that &quot;abc&quot; is the file&#39;s content, not buf. buf is the destination buffer that you will have to write the results to.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>file = &quot;abcde&quot;, n = 5
<strong>Output: </strong>5
<strong>Explanation: </strong>After calling your read method, buf should contain &quot;abcde&quot;. We read a total of 5 characters from the file, so return 5.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>file = &quot;abcdABCD1234&quot;, n = 12
<strong>Output: </strong>12
<strong>Explanation: </strong>After calling your read method, buf should contain &quot;abcdABCD1234&quot;. We read a total of 12 characters from the file, so return 12.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong>file = &quot;leetcode&quot;, n = 5
<strong>Output: </strong>5
<strong>Explanation: </strong>After calling your read method, buf should contain &quot;leetc&quot;. We read a total of 5 characters from the file, so return 5.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li>Consider that you <strong>cannot</strong> manipulate the file directly, the file is only accesible for <code>read4</code> but&nbsp;<strong>not</strong> for <code>read</code>.</li>
	<li>The <code>read</code> function will only be called once for each test case.</li>
	<li>You may assume the destination buffer array,&nbsp;<code>buf</code>,&nbsp;is guaranteed to have enough&nbsp;space for storing&nbsp;<em>n</em>&nbsp;characters.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Facebook - 6 (taggedByAdmin: true)
- Google - 4 (taggedByAdmin: false)
- Lyft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Overview

**Interview Tendencies: Google and Facebook**

A long time ago, long ago, so long ago that no one can remember,
algorithm interview questions were less popular. 
Ten years ago big companies mainly filtered the candidates by the university ranks, 
and the interview questions were like 
[please describe how DDR memory works](https://hexus.net/tech/tech-explained/ram/702-ddr-ii-how-it-works/).

Nowadays there are some tendencies to merge this "old-style interview" with the
modern algorithm questions interview. The idea is to ask a question which 
sounds like algorithmic but checks your knowledge of how do computers 
work: [Round-robin CPU scheduling](https://en.wikipedia.org/wiki/Round-robin_scheduling),
[C10k problem first solved by nginx](https://en.wikipedia.org/wiki/C10k_problem), etc. 

> Is it good or bad? That's a reality to deal with, 
especially if we speak about Google or Facebook interviews.

**Read N Characters Given Read4**

Back to the problem, the question is "how does the memory work":

- Because of the physical implementation, 
loading 4 bytes in DDR is faster than to load 1 byte 4 times.

- On the majority of computers today, collection of 4 bytes, 
or 32 bits, is called a _word_.
Most modern CPUs are optimized for the operations with _words_. 

To sum up, the problem is a practical low-level question. 
The standard approach (Approach 1) to solve it using 
the _internal_ buffer of 4 characters:

File -> Internal Buffer of 4 Characters -> Buffer of N Characters.

![img](../Figures/157/internal.png)
*Figure 1. Approach 1: solution with internal buffer.*
{:align="center"}

Once it's done, and you show your understanding of memory operations,
the follow-up question is how to speed up.
The answer (Approach 2) is quite straightforward. 
If it's possible, do not use the internal buffer of 4 characters 
to avoid the double copy:

File -> Buffer of N Characters.

![img](../Figures/157/no_internal2.png)
*Figure 2. Approach 2: solution without internal buffer.*
{:align="center"}

<br />
<br />


---
#### Approach 1: Use Internal Buffer of 4 Characters

![img](../Figures/157/internal.png)
*Figure 3. Solution with internal buffer.*
{:align="center"}

**Algorithm**

Let's use an internal buffer of 4 characters to solve this problem:

File -> Internal Buffer of 4 Characters -> Buffer of N Characters.

- Initialize the number of copied characters `copiedChars = 0`,
and the number of read characters: `readChars = 4`. It's convenient 
initialize `readChars` to `4` and then use `readChars != 4` as EOF marker. 

- Initialize an internal buffer of 4 characters: `buf4`.

- While number of copied characters is less than N: `copiedChars < n` 
and there are still characters in the file: `readChars == 4`:

    - Read from file into internal buffer: `readChars = read4(buf4)`.
    
    - Copy the characters from internal buffer `buf4` into main buffer
    `buf` one by one. Increase `copiedChars` after each character. 
    In the number of copied characters is equal to N: `copiedChars == n`, 
    interrupt the copy process and return `copiedChars`.

**Implementation**

<iframe src="https://leetcode.com/playground/5mgwJ8N7/shared" frameBorder="0" width="100%" height="378" name="5mgwJ8N7"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ to copy N characters.

* Space complexity: $$\mathcal{O}(1)$$ to keep `buf4` of 4 elements.

<br />
<br />


---
#### Approach 2: Speed Up: No Internal Buffer

![img](../Figures/157/one_buf.png)
*Figure 4. Solution without internal buffer.*
{:align="center"}

This solution is mainly suitable for the languages (C, C++, Golang) 
where pointers allow to append directly to the primary buffer `buf`. 

**Algorithm**

- Initialize the number of copied characters `copiedChars = 0`,
and the number of read characters: `readChars = 4`.

- While number of copied characters is less than N: `copiedChars < n` 
and there are still characters in the file: `readChars == 4`:

    - Read from file directly into buffer: `read4(buf + copiedChars)`.
    
    - Increase `copiedChars`: `copiedChars += readChars`.
    
- Now `buf` contains at least N characters. 
Return `min(n, copiedChars)`.

**Implementation**

<iframe src="https://leetcode.com/playground/LdcUryBA/shared" frameBorder="0" width="100%" height="259" name="LdcUryBA"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ to copy N characters.

* Space complexity: $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### What is the objective of this question
- Author: mlblount45
- Creation Date: Thu Sep 03 2015 13:31:38 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 22:28:57 GMT+0800 (Singapore Standard Time)

<p>
This question is very unclear to me. what are we supposed to accomplish in this problem. Our read function returns an int but the expected result looks like its a String. if we are forced to read 4 chars at a time how do we ever read n chars that are not a factor of 4 if we actually use the read4 method? is 'n' the max we can return or must we return exactly n assuming at least n items exist other wise return the number of items. clearly I'm missing a lot here can someone please explain this problem to me.
</p>


### Another accepted Java solution
- Author: jeantimex
- Creation Date: Fri Jul 10 2015 07:13:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 12:26:04 GMT+0800 (Singapore Standard Time)

<p>
    public int read(char[] buf, int n) {
      boolean eof = false;      // end of file flag
      int total = 0;            // total bytes have read
      char[] tmp = new char[4]; // temp buffer
      
      while (!eof && total < n) {
        int count = read4(tmp);
        
        // check if it's the end of the file
        eof = count < 4;
        
        // get the actual count
        count = Math.min(count, n - total);
        
        // copy from temp buffer to buf
        for (int i = 0; i < count; i++) 
          buf[total++] = tmp[i];
      }
      
      return total;
    }
</p>


### Python solution with explainations and comments
- Author: zhuyinghua1203
- Creation Date: Sun Dec 20 2015 04:30:49 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 13:30:58 GMT+0800 (Singapore Standard Time)

<p>
Simplified from solution to #158 in this [post][1]

i.e., easy to extend to multi-call case

    def read(self, buf, n):
        idx = 0
        while n > 0:
            # read file to buf4
            buf4 = [""]*4
            l = read4(buf4)
            # if no more char in file, return
            if not l:
                return idx
            # write buf4 into buf directly
            for i in range(min(l, n)):
                buf[idx] = buf4[i]
                idx += 1
                n -= 1
        return idx

  [1]: https://leetcode.com/discuss/75081/python-solution-with-explainations-and-comments
</p>


