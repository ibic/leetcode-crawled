---
title: "Positions of Large Groups"
weight: 771
#id: "positions-of-large-groups"
---
## Description
<div class="description">
<p>In a string <code><font face="monospace">s</font></code>&nbsp;of lowercase letters, these letters form consecutive groups of the same character.</p>

<p>For example, a string like <code>s = &quot;abbxxxxzyy&quot;</code> has the groups <code>&quot;a&quot;</code>, <code>&quot;bb&quot;</code>, <code>&quot;xxxx&quot;</code>, <code>&quot;z&quot;</code>, and&nbsp;<code>&quot;yy&quot;</code>.</p>

<p>A group is identified by an interval&nbsp;<code>[start, end]</code>, where&nbsp;<code>start</code>&nbsp;and&nbsp;<code>end</code>&nbsp;denote the start and end&nbsp;indices (inclusive) of the group. In the above example,&nbsp;<code>&quot;xxxx&quot;</code>&nbsp;has the interval&nbsp;<code>[3,6]</code>.</p>

<p>A group is considered&nbsp;<strong>large</strong>&nbsp;if it has 3 or more characters.</p>

<p>Return&nbsp;<em>the intervals of every <strong>large</strong> group sorted in&nbsp;<strong>increasing order by start index</strong></em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abbxxxxzzy&quot;
<strong>Output:</strong> [[3,6]]
<strong>Explanation</strong>: <code>&quot;xxxx&quot; is the only </code>large group with start index 3 and end index 6.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abc&quot;
<strong>Output:</strong> []
<strong>Explanation</strong>: We have groups &quot;a&quot;, &quot;b&quot;, and &quot;c&quot;, none of which are large groups.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abcdddeeeeaabbbcd&quot;
<strong>Output:</strong> [[3,5],[6,9],[12,14]]
<strong>Explanation</strong>: The large groups are &quot;ddd&quot;, &quot;eeee&quot;, and &quot;bbb&quot;.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aba&quot;
<strong>Output:</strong> []
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 1000</code></li>
	<li><code>s</code> contains lower-case English letters only.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Google - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Two Pointer [Accepted]

**Intuition**

We scan through the string to identify the start and end of each group.  If the size of the group is at least 3, we add it to the answer.

**Algorithm**

Maintain pointers `i, j` with `i <= j`.  The `i` pointer will represent the start of the current group, and we will increment `j` forward until it reaches the end of the group.

We know that we have reached the end of the group when `j` is at the end of the string, or `S[j] != S[j+1]`.  At this point, we have some group `[i, j]`; and after, we will update `i = j+1`, the start of the next group.

<iframe src="https://leetcode.com/playground/fwBwKAme/shared" frameBorder="0" width="100%" height="327" name="fwBwKAme"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `S`.

* Space Complexity: $$O(N)$$, the space used by the answer.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Straight Forward
- Author: lee215
- Creation Date: Sun May 06 2018 11:06:03 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 09 2020 15:57:47 GMT+0800 (Singapore Standard Time)

<p>
# Explanation
For every groups, 
find its start index `i` and end index `j - 1`.

Group length is `j - i`, 
if it\'s no less than 3, add `(i, j)` to result.
<br>

# Complexity
Time `O(N)`
Space `O(N)`
<br>

**Java:**
```java
    public List<List<Integer>> largeGroupPositions(String S) {
        List<List<Integer>> res = new ArrayList<>();
        for (int i = 0, j = 0; i < S.length(); i = j) {
            while (j < S.length() && S.charAt(j) == S.charAt(i)) ++j;
            if (j - i >= 3)
                res.add(Arrays.asList(i, j - 1));
        }
        return res;
    }
```


**C++:**
```cpp
    vector<vector<int>> largeGroupPositions(string S) {
        vector<vector<int>> res;
        for (int i = 0, j = 0; i < S.size(); i = j) {
            while (j < S.size() && S[j] == S[i]) ++j;
            if (j - i >= 3)
                res.push_back({i, j - 1});
        }
        return res;
    }
```

**Python:**
```py
    def largeGroupPositions(self, S):
        i, j, N = 0, 0, len(S)
        res = []
        while i < N:
            while j < N and S[j] == S[i]: j += 1
            if j - i >= 3: res.append([i, j - 1])
            i = j
        return res
```

</p>


### Oneline Python using Regex
- Author: wpanther
- Creation Date: Tue May 08 2018 08:14:59 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 22:11:55 GMT+0800 (Singapore Standard Time)

<p>
Using finditer, first thing that came to my mind:
```
        return [[r.start(), r.end() - 1] for r in re.finditer(r\'(\w)\1{2,}\', S)]
```

</p>


### Ambiguous: "the final answer should be in lexicographic order."
- Author: aaronferrucci
- Creation Date: Mon May 07 2018 01:46:10 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 22:11:21 GMT+0800 (Singapore Standard Time)

<p>
I found the lexicographic order requirement ambiguous. That\'s fine in an interview setting - the requirement might be underspecified on purpose, making the candidate request clarification - but for a contest it seems sloppy.

Fortunately, one natural way to solve the problem produces results in sorted order by start index, and that (apparently) meets the order requirement.
</p>


