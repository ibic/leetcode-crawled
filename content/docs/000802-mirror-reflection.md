---
title: "Mirror Reflection"
weight: 802
#id: "mirror-reflection"
---
## Description
<div class="description">
<p>There is&nbsp;a special square room with mirrors on each of the four&nbsp;walls.&nbsp; Except for the southwest&nbsp;corner, there are receptors on each of the remaining corners, numbered <code>0</code>, <code>1</code>, and <code>2</code>.</p>

<p>The square room has walls of length <code>p</code>, and a laser ray from the southwest corner&nbsp;first meets the east wall at a distance <code>q</code>&nbsp;from the <code>0</code>th receptor.</p>

<p>Return the number of the receptor that the ray meets first.&nbsp; (It is guaranteed that the ray will meet&nbsp;a receptor eventually.)</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>p = <span id="example-input-1-1">2</span>, q = <span id="example-input-1-2">1</span>
<strong>Output: </strong><span id="example-output-1">2</span>
<strong>Explanation: </strong>The ray meets receptor 2 the first time it gets reflected back to the left wall.
<img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/06/18/reflection.png" style="width: 218px; height: 217px;" />
</pre>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= p &lt;= 1000</code></li>
	<li><code>0 &lt;= q &lt;= p</code></li>
</ol>
</div>

</div>

## Tags
- Math (math)

## Companies
- Facebook - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Simulation

**Intuition**

The initial ray can be described as going from an origin `(x, y) = (0, 0)` in the direction `(rx, ry) = (p, q)`.  From this, we can figure out which wall it will meet and where, and what the appropriate new ray will be (based on reflection.)  We keep simulating the ray until it finds it's destination.

**Algorithm**

The parameterized position of the laser after time `t` will be `(x + rx * t, y + ry * t)`.  From there, we know when it will meet the east wall (if `x + rx * t == p`), and so on.  For a positive (and nonnegligible) time `t`, it meets the next wall.

We can then calculate how the ray reflects.  If it hits an east or west wall, then `rx *= -1`, else `ry *= -1`.

In Java, care must be taken with floating point operations.

<iframe src="https://leetcode.com/playground/9B6fwsA2/shared" frameBorder="0" width="100%" height="500" name="9B6fwsA2"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(p)$$.  We can prove (using *Approach #2*) that the number of bounces is bounded by this.

* Space Complexity:  $$O(1)$$.
<br />
<br />


---
#### Approach 2: Mathematical

**Intuition and Algorithm**

Instead of modelling the ray as a bouncing line, model it as a straight line through reflections of the room.

For example, if `p = 2, q = 1`, then we can reflect the room horizontally, and draw a straight line from `(0, 0)` to `(4, 2)`.  The ray meets the receptor `2`, which was reflected from `(0, 2)` to `(4, 2)`.

In general, the ray goes to the first integer point `(kp, kq)` where `k` is an integer, and `kp` and `kq` are multiples of `p`.  Thus, the goal is just to find the smallest `k` for which `kq` is a multiple of `p`.

The mathematical answer is `k = p / gcd(p, q)`.

<iframe src="https://leetcode.com/playground/PVJs949s/shared" frameBorder="0" width="100%" height="327" name="PVJs949s"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\log P)$$, the complexity of the `gcd` operation.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java short solution with a sample drawing
- Author: wangzi6147
- Creation Date: Sun Jun 24 2018 11:02:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 05:06:17 GMT+0800 (Singapore Standard Time)

<p>
![image](https://s3-lc-upload.s3.amazonaws.com/users/wangzi6147/image_1529813576.png)

I draw this graph to help. In this example, `p / q = 5 / 2`. In this way, we can find some patterns between pq and the result.

I use the extending blocks to simulate the reflection. As well as the reflection of the corners `0, 1, 2`. Then the laser could go straightly to the top right of the extending blocks until it reaches any of the corners. Then we can use `p/q` to represents the extending blocks `x/y` ratio which is `5/2` in the example above. Then the destination corner could be calculated using this `x/y` ratio.
The reason why we used modulo 2 is that if `x/y=10/4` for example, the laser will firstly reach the corner `0` when `x/y=5/2` before `x/y=10/4`. You can draw a similar graph to simulate it.
In other words, when the laser first reaches a corner, the `x` and `y` cannot both be even. So we keep using modulo 2 to let `x` and `y` go down. 

```
class Solution {
    public int mirrorReflection(int p, int q) {
        while (p % 2 == 0 && q % 2 == 0) {
            p /= 2;
            q /= 2;
        }
        if (p % 2 == 0) {
            return 2;
        } else if (q % 2 == 0) {
            return 0;
        } else {
            return 1;
        }
    }
}
```
</p>


### Java solution with an easy-to-understand explanation
- Author: motorix
- Creation Date: Sat Jul 07 2018 12:56:27 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 14:59:22 GMT+0800 (Singapore Standard Time)

<p>
The idea comes from this post: https://leetcode.com/problems/mirror-reflection/discuss/141773, and here I add some explaination.
```
    public int mirrorReflection(int p, int q) {
        int m = 1, n = 1;
        while(m * p != n * q){
            n++;
            m = n * q / p;
        }
        if (m % 2 == 0 && n % 2 == 1) return 0;
        if (m % 2 == 1 && n % 2 == 1) return 1;
        if (m % 2 == 1 && n % 2 == 0) return 2;
        return -1;
    }
```
First, think about the case p = 3 & q = 2.
![image](https://s3-lc-upload.s3.amazonaws.com/users/motorix/image_1529877876.png)
So, this problem can be transformed into finding ```m * p = n * q```, where 
```m```  = the number of room extension + 1. 
```n``` = the number of light reflection + 1. 


1. If the number of light reflection is odd (which means ```n``` is even), it means the corner is on the left-hand side. The possible corner is ```2```.
Otherwise, the corner is on the right-hand side. The possible corners are ```0``` and ```1```.
2. Given the corner is on the right-hand side. 
If the number of room extension is even (which means ```m``` is odd), it means the corner is ```1```. Otherwise, the corner is ```0```.

So, we can conclude:
```
m is even & n is odd => return 0.
m is odd & n is odd => return 1.
m is odd & n is even => return 2.
```
Note: The case ```m is even & n is even``` is impossible. Because in the equation ``` m * q = n * p```, if ```m``` and ```n``` are even, we can divide both ```m``` and ```n``` by 2. Then, ```m``` or ```n``` must be odd.

--
Because we want to find ```m * p = n * q```, where either ```m``` or ```n``` is odd, we can do it this way.
```
    public int mirrorReflection(int p, int q) {
        int m = q, n = p;
        while(m % 2 == 0 && n % 2 == 0){
            m /= 2;
            n /= 2;
        }
        if (m % 2 == 0 && n % 2 == 1) return 0;
        if (m % 2 == 1 && n % 2 == 1) return 1;
        if (m % 2 == 1 && n % 2 == 0) return 2;
        return -1;
    }
```
</p>


### [C++/Java/Python] 1-line without using any package or ✖️➗%
- Author: lee215
- Creation Date: Sun Jun 24 2018 11:04:31 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 14:59:14 GMT+0800 (Singapore Standard Time)

<p>
Here is my first solution. When I did it, I just code without any thinking.
```
    def mirrorReflection(self, p, q):
        k = 1
        while (q * k % p): k += 1
        if q * k / p % 2 and k % 2: return 1
        if q * k / p % 2 == 0 and k % 2: return 0
        if q * k / p % 2 and k % 2 == 0: return 2
        return -1
```
When I reviewed the problems for my discuss article, I finaly realised only odd or even matter.

Divide `p,q` by `2` until at least one odd.

If `p = odd, q = even`: return 0
If `p = even, q = odd`:  return 2
If `p = odd, q = odd`: return 1
I summary it as `return 1 - p % 2 + q % 2`

<img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/06/18/reflection.png" style="width: 218px; height: 217px;" />

**C++:**
```
    int mirrorReflection(int p, int q) {
        while (p % 2 == 0 && q % 2 == 0) p >>= 1, q >>= 1;
        return 1 - p % 2 + q % 2;
    }
```

**Java:**
```
    public int mirrorReflection(int p, int q) {
        while (p % 2 == 0 && q % 2 == 0) {p >>= 1; q >>= 1;}
        return 1 - p % 2 + q % 2;
    }
```
**Python:**
```
    def mirrorReflection(self, p, q):
        while p % 2 == 0 and q % 2 == 0: p, q = p / 2, q / 2
        return 1 - p % 2 + q % 2
```

Based on this idea, I did this 1-line solutions without any package.
**C++/Java**
```
        return (p & -p) > (q & -q) ? 2 : (p & -p) < (q & -q) ? 0:1;
```

**Python**
```
        return ((p & -p) >= (q & -q)) + ((p & -p) > (q & -q))
```


**Time Complexity**
Time for 4 bit manipulations and 2 comparations.
</p>


