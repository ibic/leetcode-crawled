---
title: "Valid Square"
weight: 545
#id: "valid-square"
---
## Description
<div class="description">
<p>Given the coordinates of four points in 2D space, return whether the four points could construct a square.</p>

<p>The coordinate (x,y) of a point is represented by an integer array with two integers.</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b> p1 = [0,0], p2 = [1,1], p3 = [1,0], p4 = [0,1]
<b>Output:</b> True
</pre>

<p>&nbsp;</p>

<p>Note:</p>

<ol>
	<li>All the input integers are in the range [-10000, 10000].</li>
	<li>A valid square has four equal sides with positive length and four equal angles (90-degree angles).</li>
	<li>Input points have no order.</li>
</ol>

<p>&nbsp;</p>

</div>

## Tags
- Math (math)

## Companies
- Pure Storage - 4 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force [Accepted]

The idea behind determining whether 4 given set of points constitute a valid square or not is really simple. Firstly, we need to determine if the sides of the qaudrilateral formed by these 4 points are equal. But checking only this won't suffice. Since, this condition will be satisfied even in the case of a rhombus, where all the four sides are equal but the adjacent sides aren't perpendicular to each other. Thus, we also need to check if the lengths of the diagonals formed between the corners of the quadrilateral are equal. If both the conditions are satisfied, then only the given set of points can be deemed appropriate for constituting a square.

Now, the problem arises in determining which pairs of points act as the adjacent points on the square boundary. So, the simplest method is to consider every possible case. For the given 4 points, $$[p_0, p_1, p_2, p_3]$$, there are a total of 4! ways in which these points can be arranged to be considered as the square's boundaries. We can generate every possible permutation and check if any permutation leads to the valid square arrangement of points.

<iframe src="https://leetcode.com/playground/kR62YSDY/shared" frameBorder="0" name="kR62YSDY" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(1)$$. Constant number of permutations($$4!$$) are generated.

* Space complexity : $$O(1)$$. Constant space is required.

---
#### Approach #2 Using Sorting [Accepted]

Instead of considering all the permutations of arrangements possible, we can make use of maths to simplify this problem a bit. If we sort the given set of points based on their x-coordinate values, and in the case of a tie, based on their y-coordinate value, we can obtain an arrangement, which directly reflects the arrangement of points on a valid square boundary possible.

Consider the only possible cases as shown in the figure below:

![Valid_Square](../Figures/593_Valid_Square_1.PNG)

In each case, after sorting, we obtain the following conclusion regarding the connections of the points:

1. $$p_0p_1$$, $$p_1p_3$$, $$p_3p_2$$ and $$p_2p_0$$ form the four sides of any valid square.

2. $$p_0p_3$$ and $$p_1p_2$$ form the diagonals of the square.

Thus, once the sorting of the points is done, based on the above knowledge, we can directly compare $$p_0p_1$$, $$p_1p_3$$, $$p_3p_2$$ and $$p_2p_0$$ for equality of lengths(corresponding to the sides); and $$p_0p_3$$ and $$p_1p_2$$ for equality of lengths(corresponding to the diagonals).

<iframe src="https://leetcode.com/playground/xp6gv2NM/shared" frameBorder="0" name="xp6gv2NM" width="100%" height="241"></iframe>

**Complexity Analysis**

* Time complexity : $$O(1)$$. Sorting 4 points takes constant time.

* Space complexity : $$O(1)$$. Constant space is required.

---
#### Approach #3 Checking every case [Accepted]

**Algorithm**

If we consider all the permutations descripting the arrangement of points as in the brute force approach, we can come up with the following set of 24 arrangements:

![Valid_Square](../Figures/593_Valid_Square_2.PNG)

In this figure, the rows with the same shaded color indicate that the corresponding arrangements lead to the same set of edges and diagonals. Thus, we can see that only three unique cases exist. Thus, instead of generating all the 24 permutations, we check for the equality of edges and diagonals for only the three distinct cases.

<iframe src="https://leetcode.com/playground/7wt6ZUJR/shared" frameBorder="0" name="7wt6ZUJR" width="100%" height="258"></iframe>

**Complexity Analysis**

* Time complexity : $$O(1)$$. A fixed number of comparisons are done.

* Space complexity : $$O(1)$$. No extra space required.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ 3 lines (unordered_set)
- Author: votrubac
- Creation Date: Sun May 21 2017 11:01:04 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 20:36:40 GMT+0800 (Singapore Standard Time)

<p>
If we calculate all distances between 4 points, 4 smaller distances should be equal (sides), and 2 larger distances should be equal too (diagonals). As an optimization, we can compare squares of the distances, so we do not have to deal with the square root and precision loss.

Therefore, our set will only contain 2 unique distances in case of square (beware of the zero distance though).
```
int d(vector<int>& p1, vector<int>& p2) {
    return (p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]);
}
bool validSquare(vector<int>& p1, vector<int>& p2, vector<int>& p3, vector<int>& p4) {
    unordered_set<int> s({ d(p1, p2), d(p1, p3), d(p1, p4), d(p2, p3), d(p2, p4), d(p3, p4) });
    return !s.count(0) && s.size() == 2;
}
```
</p>


### Share my simple Python solution
- Author: aditya74
- Creation Date: Sun May 21 2017 11:08:34 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 08:27:37 GMT+0800 (Singapore Standard Time)

<p>
Number of unique distances should be 2. (4 for sides, and 2 for diagonals)
```
class Solution(object):
    def validSquare(self, p1, p2, p3, p4):
        points = [p1, p2, p3, p4]
        
        dists = collections.Counter()
        for i in range(len(points)):
            for j in range(i+1, len(points)):
                dists[self.getDistance(points[i], points[j])] += 1
        
        return len(dists.values())==2 and 4 in dists.values() and 2 in dists.values()
        
    def getDistance(self, p1, p2):
        return (p1[0] - p2[0])**2 + (p1[1] - p2[1])**2
```
</p>


### Simple Java Solution - Square distances
- Author: earlme
- Creation Date: Sun May 21 2017 11:03:26 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 04 2018 00:35:28 GMT+0800 (Singapore Standard Time)

<p>
Just find the square of lenghts, and validate that
1. There are only two equal longest lenghts. 
2. The non longest lengths are all equal.


    public boolean validSquare(int[] p1, int[] p2, int[] p3, int[] p4) {
        long[] lengths = {length(p1, p2), length(p2, p3), length(p3, p4),
                length(p4, p1), length(p1, p3),length(p2, p4)}; // all 6 sides

        long max = 0, nonMax = 0;
        for(long len : lengths) {
            max = Math.max(max, len);
        }
        int count = 0;
        for(int i = 0; i < lengths.length; i++) {
            if(lengths[i] == max) count++;
            else nonMax = lengths[i]; // non diagonal side.
        }
        if(count != 2) return false; // diagonals lenghts have to be same.

        for(long len : lengths) {
            if(len != max && len != nonMax) return false; // sides have to be same length
        }
        return true;
    }
    private long length(int[] p1, int[] p2) {
        return (long)Math.pow(p1[0]-p2[0],2) + (long)Math.pow(p1[1]-p2[1], 2);
    }
</p>


