---
title: "Sort Integers by The Number of 1 Bits"
weight: 1250
#id: "sort-integers-by-the-number-of-1-bits"
---
## Description
<div class="description">
<p>Given an integer array <code>arr</code>. You have to sort the integers in the array&nbsp;in ascending order by the number of <strong>1&#39;s</strong>&nbsp;in their binary representation and in case of two or more integers have the same number of <strong>1&#39;s</strong> you have to sort them in ascending order.</p>

<p>Return <em>the sorted array</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [0,1,2,3,4,5,6,7,8]
<strong>Output:</strong> [0,1,2,4,8,3,5,6,7]
<strong>Explantion:</strong> [0] is the only integer with 0 bits.
[1,2,4,8] all have 1 bit.
[3,5,6] have 2 bits.
[7] has 3 bits.
The sorted array by bits is [0,1,2,4,8,3,5,6,7]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1024,512,256,128,64,32,16,8,4,2,1]
<strong>Output:</strong> [1,2,4,8,16,32,64,128,256,512,1024]
<strong>Explantion:</strong> All integers have 1 bit in the binary representation, you should just sort them in ascending order.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [10000,10000]
<strong>Output:</strong> [10000,10000]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arr = [2,3,5,7,11,13,17,19]
<strong>Output:</strong> [2,3,5,17,7,11,13,19]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> arr = [10,100,1000,10000]
<strong>Output:</strong> [10,100,10000,1000]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 500</code></li>
	<li><code>0 &lt;= arr[i] &lt;= 10^4</code></li>
</ul>

</div>

## Tags
- Sort (sort)
- Bit Manipulation (bit-manipulation)

## Companies
- Adobe - 2 (taggedByAdmin: false)
- Mapbox - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] 1 liners.
- Author: rock
- Creation Date: Sun Feb 23 2020 00:04:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 13 2020 09:08:41 GMT+0800 (Singapore Standard Time)

<p>
**Explanation by @Be_Kind_To_One_Another**:
Comment regarding `i -> Integer.bitCount(i) * 10000 + i` to answer any potential question. It\'s essentially hashing the original numbers into another number generated from the count of bits and then sorting the newly generated numbers. so why `10000`? simply because of the input range is `0 <= arr[i] <= 10^4`.
For instance `[0,1,2,3,5,7]`, becomes something like this `[0, 10001, 10002, 20003, 20005, 30007]`.
```
0 has 0 number of bits  --> 0 * 10000 + 0 = 0
1,2 have 1 bit set      --> 1 * 10000 + 1 = 10001  &  1 * 10000 + 2 = 10002
3,5 have 2 bits set     --> 2 * 10000 + 3 = 20003  &  2 * 10000 + 5 = 20005
7 has 3 bits set        --> 3 * 10000 + 7 = 30007
```
In short, the bit length contribution to the value of hash code is always greater than the number itself. -- credit to **@wang2019**.

```java
    public int[] sortByBits(int[] arr) {
        Integer[] a = new Integer[arr.length];
        for (int i = 0; i < a.length; ++i)
            a[i] = arr[i];
     // Arrays.sort(a, (i, j) -> Integer.bitCount(i) != Integer.bitCount(j) ? Integer.bitCount(i) - Integer.bitCount(j) : i - j);
        Arrays.sort(a, Comparator.comparing(i -> Integer.bitCount(i) * 10000 + i));
        for (int i = 0; i < a.length; ++i)
            arr[i] = a[i];
        return arr;
    }
```
Java 8 1 liner:
```java
    public int[] sortByBits(int[] arr) {
        return Arrays.stream(arr)
                     .boxed()
                     .sorted(Comparator.comparingInt(i -> Integer.bitCount(i) * 10000 + i)).mapToInt(i -> i)
                     .toArray();
    }
```
Or
```java
    public int[] sortByBits(int[] arr) {
        return Arrays.stream(arr)
                     .boxed()
                     .sorted((i, j) -> Integer.bitCount(i) != Integer.bitCount(j) ? Integer.bitCount(i) - Integer.bitCount(j) : i - j)
                     .mapToInt(i -> i)
                     .toArray();
    }
```
```python
    def sortByBits(self, arr: List[int]) -> List[int]:
        return sorted(arr, key=lambda x: (bin(x).count(\'1\'), x))
```
</p>


### [Python] 1-line
- Author: lee215
- Creation Date: Sun Feb 23 2020 00:08:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 00:08:51 GMT+0800 (Singapore Standard Time)

<p>
**Python:**
```python
    def sortByBits(self, A):
        return sorted(A, key=lambda a: [bin(a).count(\'1\'), a])
```

</p>


### C++ in-place sort+ popcount
- Author: Mr_Fish
- Creation Date: Sun Feb 23 2020 00:15:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 00:15:12 GMT+0800 (Singapore Standard Time)

<p>
```
	vector<int> sortByBits(vector<int>& arr) {
        sort(arr.begin(), arr.end(),[](const int& a, const int& b){
            int countA = __builtin_popcount(a), countB = __builtin_popcount(b);
            return countA==countB ? a<b:countA<countB;});
        return arr;
    }
```

</p>


