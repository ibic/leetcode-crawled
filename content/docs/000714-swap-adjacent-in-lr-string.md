---
title: "Swap Adjacent in LR String"
weight: 714
#id: "swap-adjacent-in-lr-string"
---
## Description
<div class="description">
<p>In a string composed of <code>&#39;L&#39;</code>, <code>&#39;R&#39;</code>, and <code>&#39;X&#39;</code> characters, like <code>&quot;RXXLRXRXL&quot;</code>, a move consists of either replacing one occurrence of <code>&quot;XL&quot;</code> with <code>&quot;LX&quot;</code>, or replacing one occurrence of <code>&quot;RX&quot;</code> with <code>&quot;XR&quot;</code>. Given the starting string <code>start</code> and the ending string <code>end</code>, return <code>True</code> if and only if there exists a sequence of moves to transform one string to the other.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> start = &quot;RXXLRXRXL&quot;, end = &quot;XRLXXRRLX&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong> We can transform start to end following these steps:
RXXLRXRXL -&gt;
XRXLRXRXL -&gt;
XRLXRXRXL -&gt;
XRLXXRRXL -&gt;
XRLXXRRLX
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> start = &quot;X&quot;, end = &quot;L&quot;
<strong>Output:</strong> false
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> start = &quot;LLR&quot;, end = &quot;RRL&quot;
<strong>Output:</strong> false
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> start = &quot;XL&quot;, end = &quot;LX&quot;
<strong>Output:</strong> true
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> start = &quot;XLLR&quot;, end = &quot;LXLX&quot;
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= start.length&nbsp;&lt;= 10<sup>4</sup></code></li>
	<li><code>start.length == end.length</code></li>
	<li>Both <code>start</code> and <code>end</code> will only consist of characters in <code>&#39;L&#39;</code>, <code>&#39;R&#39;</code>, and&nbsp;<code>&#39;X&#39;</code>.</li>
</ul>

</div>

## Tags
- Brainteaser (brainteaser)

## Companies
- Google - 5 (taggedByAdmin: true)
- ByteDance - 4 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach 1: Invariant

**Intuition**

Let's think of `'L'` and `'R'` as people facing left and right standing on one line, and `'X'` as an empty space on that line.

We can ask: what invariants (conditions that remain true after making any move) there are.  This is natural for any question that involves transforming some state and asking whether a final state is possible.

**Algorithm**

One invariant is that `'L'` and `'R'` characters in the string can never cross each other - people walking on the line cannot pass through each other.  That means the starting and target strings must be the same when reading only the `'L'` and `'R'`s.  With respect to some starting string, let's call such a target string "*solid*" (since the people don't pass through each other).

Additionally, the `n`-th `'L'` can never go to the right of it's original position, and similarly the `n`-th `'R'` can never go to the left of it's original position.  We'll call this "*accessibility*".  Formally, if $$(i_1, i_2, \cdots )$$ and $$(i^{'}_1, i^{'}_2, \cdots )$$ are the positions of each `'L'` character in the source and target string, and similarly for $$(j_i \cdots ), (j^{'}_1 \cdots )$$ and positions of `'R'` characters, then we will say a target string is *accessible* if $$i_k \geq i^{'}_k$$ and $$j_k \leq j^{'}_k$$.

This shows being solid and accessible are necessary conditions.  With an induction on the number of people, we can show that they are sufficient conditions.  The basic idea of the proof is this: A person on the end either walks away from the others, or walks towards.  If they walk away, then let them walk first and it is true; if they walk towards, then let them walk last and it is true (by virtue of the target being solid).

With these ideas in hand, we'll investigate the two properties individually.  If they are both true, then the answer is `True`, otherwise it is `False`.

<iframe src="https://leetcode.com/playground/9XCQVMZH/shared" frameBorder="0" width="100%" height="429" name="9XCQVMZH"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `start` and `end`.

* Space Complexity:  $$O(N)$$.  The replacement operation is $$O(N)$$, while the remaining operations use $$O(1)$$ additional space.  We could amend the replace part of our algorithm to use pointers so as to reduce the total complexity to $$O(1)$$.

---

#### Approach 2: Two Pointers

**Intuition and Algorithm**

Following the explanation in *Approach #1*, the target string must be solid and accessible.

We use two pointers to solve it.  Each pointer `i`, `j` points to an index of `start`, `end` with `start[i] != 'X'`, `end[j] != 'X'`.

Then, if `start[i] != end[j]`, the target string isn't solid.  Also, if `start[i] == 'L' and i < j`, (or `start[i] == 'R' and i > j`), the string is not accessible.

In our Python implementation, we use generators to handle moving `i, j` to the next index where `start[i] != 'X', end[j] != 'X'`.

<iframe src="https://leetcode.com/playground/PgCzzLY9/shared" frameBorder="0" width="100%" height="500" name="PgCzzLY9"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `start` and `end`.

* Space Complexity:  $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java one pass O(n) solution with explaination
- Author: Samuel_Ji
- Creation Date: Mon Feb 05 2018 05:23:00 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 13:13:51 GMT+0800 (Singapore Standard Time)

<p>
The idea is simple. Just get the non-X characters and compare the positions of them.
```
class Solution {
    public boolean canTransform(String start, String end) {
        if (!start.replace("X", "").equals(end.replace("X", "")))
            return false;
        
        int p1 = 0;
        int p2 = 0;
        
        while(p1 < start.length() && p2 < end.length()){
            
            // get the non-X positions of 2 strings
            while(p1 < start.length() && start.charAt(p1) == 'X'){
                p1++;
            }
            while(p2 < end.length() && end.charAt(p2) == 'X'){
                p2++;
            }
            
            //if both of the pointers reach the end the strings are transformable
            if(p1 == start.length() && p2 == end.length()){
                return true;
            }
            // if only one of the pointer reach the end they are not transformable
            if(p1 == start.length() || p2 == end.length()){
                return false;
            }
            
            if(start.charAt(p1) != end.charAt(p2)){
                return false;
            }
            // if the character is 'L', it can only be moved to the left. p1 should be greater or equal to p2.
            if(start.charAt(p1) == 'L' && p2 > p1){
                return false;
            }
            // if the character is 'R', it can only be moved to the right. p2 should be greater or equal to p1.
            if(start.charAt(p1) == 'R' && p1 > p2){
                return false;
            }
            p1++;
            p2++;
        }
        return true;
    }
    
}
```
</p>


### Python using corresponding position - 中文解释
- Author: shenweihai
- Creation Date: Fri Jan 11 2019 15:19:41 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jan 11 2019 15:19:41 GMT+0800 (Singapore Standard Time)

<p>
![image](https://assets.leetcode.com/users/shenweihai/image_1547191167.png)

```
class Solution(object):
    def canTransform(self, start, end):
        """
        :type start: str
        :type end: str
        :rtype: bool
        """
        if len(start) != len(end): return False
        
        A = [(s, idx) for idx, s in enumerate(start) if s == \'L\' or s == \'R\']
        B = [(e, idx) for idx, e in enumerate(end) if e == \'L\' or e == \'R\']
        if len(A) != len(B): return False
        
        for (s, i), (e, j) in zip(A, B):
            if s != e: return False
            if s == \'L\':
                if i < j:
                    return False
            if s == \'R\':
                if i > j:
                    return False
            
        return True
```
</p>


### Python simple solution, 3 lines O(n)
- Author: yanzhan2
- Creation Date: Sun Feb 04 2018 16:54:16 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 01:02:48 GMT+0800 (Singapore Standard Time)

<p>
The idea it to compare 'L' position in start is greater than or equal to that in end, also 'R' position in start is less than or equal to that in end.

The reason is 'L' can move left when 'X' exists before, and 'R' can move right when 'X' exists after, and 'L' and 'R' cannot cross each other, so we do not care about 'X'.
```
class Solution:
    def canTransform(self, start, end):
        """
        :type start: str
        :type end: str
        :rtype: bool
        """
        s = [(c, i) for i, c in enumerate(start) if c == 'L' or c == 'R']
        e = [(c, i) for i, c in enumerate(end) if c == 'L' or c == 'R']
        return len(s) == len(e) and all(c1 == c2 and (i1 >= i2 and c1 == 'L' or i1 <= i2 and c1 == 'R') for (c1, i1), (c2, i2) in zip(s,e))
```
</p>


