---
title: "Minimum Moves to Move a Box to Their Target Location"
weight: 1203
#id: "minimum-moves-to-move-a-box-to-their-target-location"
---
## Description
<div class="description">
<p>Storekeeper is a&nbsp;game&nbsp;in which the player pushes boxes around in a warehouse&nbsp;trying to get them to target locations.</p>

<p>The game is represented by a <code>grid</code> of size&nbsp;<code>m x n</code>, where each element is a wall, floor, or a box.</p>

<p>Your task is move the box <code>&#39;B&#39;</code> to the target position <code>&#39;T&#39;</code> under the following rules:</p>

<ul>
	<li>Player is represented by character <code>&#39;S&#39;</code>&nbsp;and&nbsp;can move up, down, left, right in the <code>grid</code> if it is a floor (empy cell).</li>
	<li>Floor is represented by character <code>&#39;.&#39;</code> that means free cell to walk.</li>
	<li>Wall is represented by character <code>&#39;#&#39;</code> that means obstacle&nbsp;&nbsp;(impossible to walk there).&nbsp;</li>
	<li>There is only one box <code>&#39;B&#39;</code> and one&nbsp;target cell <code>&#39;T&#39;</code> in the <code>grid</code>.</li>
	<li>The box can be moved to an adjacent free cell by standing next to the box and then moving in the direction of the box. This is a <strong>push</strong>.</li>
	<li>The player cannot walk through the box.</li>
</ul>

<p>Return the minimum number of <strong>pushes</strong> to move the box to the target. If there is no way to reach the target, return&nbsp;<code>-1</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/11/06/sample_1_1620.png" style="width: 520px; height: 386px;" /></strong></p>

<pre>
<strong>Input:</strong> grid = [[&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;],
               [&quot;#&quot;,&quot;T&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;],
&nbsp;              [&quot;#&quot;,&quot;.&quot;,&quot;.&quot;,&quot;B&quot;,&quot;.&quot;,&quot;#&quot;],
&nbsp;              [&quot;#&quot;,&quot;.&quot;,&quot;#&quot;,&quot;#&quot;,&quot;.&quot;,&quot;#&quot;],
&nbsp;              [&quot;#&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;S&quot;,&quot;#&quot;],
&nbsp;              [&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;]]
<strong>Output:</strong> 3
<strong>Explanation: </strong>We return only the number of times the box is pushed.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> grid = [[&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;],
               [&quot;#&quot;,&quot;T&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;],
&nbsp;              [&quot;#&quot;,&quot;.&quot;,&quot;.&quot;,&quot;B&quot;,&quot;.&quot;,&quot;#&quot;],
&nbsp;              [&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;.&quot;,&quot;#&quot;],
&nbsp;              [&quot;#&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;S&quot;,&quot;#&quot;],
&nbsp;              [&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;]]
<strong>Output:</strong> -1
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> grid = [[&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;],
&nbsp;              [&quot;#&quot;,&quot;T&quot;,&quot;.&quot;,&quot;.&quot;,&quot;#&quot;,&quot;#&quot;],
&nbsp;              [&quot;#&quot;,&quot;.&quot;,&quot;#&quot;,&quot;B&quot;,&quot;.&quot;,&quot;#&quot;],
&nbsp;              [&quot;#&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;#&quot;],
&nbsp;              [&quot;#&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;S&quot;,&quot;#&quot;],
&nbsp;              [&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;]]
<strong>Output:</strong> 5
<strong>Explanation:</strong>  push the box down, left, left, up and up.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> grid = [[&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;],
&nbsp;              [&quot;#&quot;,&quot;S&quot;,&quot;#&quot;,&quot;.&quot;,&quot;B&quot;,&quot;T&quot;,&quot;#&quot;],
&nbsp;              [&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;,&quot;#&quot;]]
<strong>Output:</strong> -1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m ==&nbsp;grid.length</code></li>
	<li><code>n ==&nbsp;grid[i].length</code></li>
	<li><code>1 &lt;= m &lt;= 20</code></li>
	<li><code>1 &lt;= n &lt;= 20</code></li>
	<li><code>grid</code> contains only characters&nbsp;<code>&#39;.&#39;</code>, <code>&#39;#&#39;</code>,&nbsp; <code>&#39;S&#39;</code> , <code>&#39;T&#39;</code>,&nbsp;or <code>&#39;B&#39;</code>.</li>
	<li>There is only one character&nbsp;<code>&#39;S&#39;</code>, <code>&#39;B&#39;</code>&nbsp;<font face="sans-serif, Arial, Verdana, Trebuchet MS">and&nbsp;</font><code>&#39;T&#39;</code>&nbsp;in the <code>grid</code>.</li>
</ul>

</div>

## Tags
- Breadth-first Search (breadth-first-search)

## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A-star search
- Author: yorkshire
- Creation Date: Sun Nov 17 2019 12:01:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 17 2019 12:28:13 GMT+0800 (Singapore Standard Time)

<p>
Heuristic (an under-estimate of the remaining moves required) is the Manhattan distance between box and target.
A state consist of box and person locations together.

Repeatedly pop the state with the lowest heuristic + previous moves off the heap.
Attempt to move the person in all 4 directions.
If any direction moves the person to the box, check if the box can move to the nex position in the grid.

```
        rows, cols = len(grid), len(grid[0])
        for r in range(rows):
            for c in range(cols):
                if grid[r][c] == "T":
                    target = (r, c)
                if grid[r][c] == "B":
                    start_box = (r, c)
                if grid[r][c] == "S":
                    start_person = (r, c)
                    
        def heuristic(box):
            return abs(target[0] - box[0]) + abs(target[1] - box[1])
        
        def out_bounds(location):  # return whether the location is in the grid and not a wall
            r, c = location
            if r < 0 or r >= rows:
                return True
            if c < 0 or c >= cols:
                return True
            return grid[r][c] == "#"
                        
        heap = [[heuristic(start_box), 0, start_person, start_box]]
        visited = set()
        
        while heap:
            _, moves, person, box = heapq.heappop(heap)
            if box == target:
                return moves
            if (person, box) in visited: # do not visit same state again
                continue
            visited.add((person, box))
            
            for dr, dc in [[0, 1], [1, 0], [-1, 0], [0, -1]]:
                new_person = (person[0] + dr, person[1] + dc)
                if out_bounds(new_person):
                    continue
                    
                if new_person == box:
                    new_box = (box[0] + dr, box[1] + dc)
                    if out_bounds(new_box):
                        continue
                    heapq.heappush(heap, [heuristic(new_box) + moves + 1, moves + 1, new_person, new_box])
                else:
                    heapq.heappush(heap, [heuristic(box) + moves, moves, new_person, box]) # box remains same
        
        return -1
```
</p>


### Java straightforward BFS solution
- Author: CodeJoker
- Creation Date: Sun Nov 17 2019 18:53:27 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Nov 22 2019 15:14:41 GMT+0800 (Singapore Standard Time)

<p>
For me, this is a typical bfs question to find the minimum step, so the key point is to find out the state. For this problem, I choose the coordinates of box and storekeeper as the state. Because the grid length is from 1 to 20, we can use one 32 bit integer to present all the coordinates.
Every step, move storekeeper 1 step, if it meet the box, then the box move 1 step in the same direction. Because it want to know the minimum step the box move, so when the first time box on the target pocision, it may not be the minimum anwser. So we compare every one and find the minimum step.

```java
class Solution {
    int[][] moves = new int[][]{{0,-1}, {0,1}, {-1,0}, {1,0}};
    public int minPushBox(char[][] grid) {
        int[] box = null, target = null, storekeeper = null;
        int n = grid.length, m = grid[0].length;
        for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
            if (grid[i][j] == \'B\') box = new int[]{i, j};
            else if (grid[i][j] == \'T\') target = new int[]{i, j};
            else if (grid[i][j] == \'S\') storekeeper = new int[]{i, j};
        }
        Queue<Integer> q = new LinkedList<>();
        Map<Integer, Integer> dis = new HashMap<>();
        int start = encode(box[0], box[1], storekeeper[0], storekeeper[1]);
        dis.put(start, 0);
        q.offer(start);
        int ret = Integer.MAX_VALUE;
        while (!q.isEmpty()) {
            int u = q.poll();
            int[] du = decode(u);
            if (dis.get(u) >= ret) continue;
            if (du[0] == target[0] && du[1] == target[1]) {
                ret = Math.min(ret, dis.get(u));
                continue;
            }
            int[] b = new int[]{du[0], du[1]};
            int[] s = new int[]{du[2], du[3]};
			// move the storekeeper for 1 step
            for (int[] move : moves) {
                int nsx = s[0] + move[0];
                int nsy = s[1] + move[1];
                if (nsx < 0 || nsx >= n || nsy < 0 || nsy >= m || grid[nsx][nsy] == \'#\') continue;
				// if it meet the box, then the box move in the same direction
                if (nsx == b[0] && nsy == b[1]) {
                    int nbx = b[0] + move[0];
                    int nby = b[1] + move[1];
                    if (nbx < 0 || nbx >= n || nby < 0 || nby >= m || grid[nbx][nby] == \'#\') continue;
                    int v = encode(nbx, nby, nsx, nsy);
                    if (dis.containsKey(v) && dis.get(v) <= dis.get(u) + 1) continue;
                    dis.put(v, dis.get(u) + 1);
                    q.offer(v);
                } else { // if the storekeeper doesn\'t meet the box, the position of the box do not change
                    int v = encode(b[0], b[1], nsx, nsy);
                    if (dis.containsKey(v) && dis.get(v) <= dis.get(u)) continue;
                    dis.put(v, dis.get(u));
                    q.offer(v);
                }
            }
        }
        return ret == Integer.MAX_VALUE ? -1 : ret;
    }
    int encode(int bx, int by, int sx, int sy) {
        return (bx << 24) | (by << 16) | (sx << 8) | sy;
    }
    int[] decode(int num) {
        int[] ret = new int[4];
        ret[0] = (num >>> 24) & 0xff;
        ret[1] = (num >>> 16) & 0xff;
        ret[2] = (num >>> 8) & 0xff;
        ret[3] = num & 0xff;
        return ret;
    }
}
```
</p>


### Python Straightforward 2-stage BFS, Explained
- Author: davyjing
- Creation Date: Sun Nov 17 2019 12:04:05 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Nov 19 2019 03:45:52 GMT+0800 (Singapore Standard Time)

<p>
First define a function to check from current state, what are the possible neighbouring states (use BFS to check if we can move the player to required location). Notice that the state includes both the location of the box and the player.
Second BFS to see if we can reach the target location.
```
class Solution:
    def minPushBox(self, grid: List[List[str]]) -> int:
        dire = [(1,0),(0,1),(-1,0),(0,-1)]
		
        def can_get(cur_b,cur_p,tar):
            seen,cur = set([cur_p]),set([cur_p])
            while cur:
                tmp = []
                for loc in cur:
                    for x,y in dire:
                        if 0<= loc[0]+x < len(grid) and 0 <= loc[1] + y < len(grid[0]) and (loc[0]+x,loc[1] +y) != cur_b and grid[loc[0] +x][loc[1] +y] != \'#\' and (loc[0]+x,loc[1] +y) not in seen:
                            tmp += [(loc[0]+x,loc[1] +y)]
                cur = set(tmp)
                seen |= cur
                if tar in seen:
                    return True
            return False
			
        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] == \'B\': box = (i,j)
                if grid[i][j] == \'S\': player = (i,j)
                if grid[i][j] == \'T\': target = (i,j)
				
        seen,cur,res = set([(box,player)]), set([(box,player)]), 0
        while cur:
            tmp = []
            res += 1
            for b,p in cur:
                for x,y in dire:
                    if 0<= b[0]+x < len(grid) and 0 <= b[1] + y < len(grid[0]) and grid[b[0]+x][b[1]+y] != \'#\' and can_get(b,p,(b[0]-x,b[1]-y)) and ((b[0]+x,b[1]+y),b) not in seen:
                        tmp += [((b[0]+x,b[1]+y),b)]
            cur = set(tmp)
            seen |= cur
            for x,y in dire:
                if (target,(target[0]+x,target[1]+y)) in seen:
                    return res
        return -1
```
</p>


