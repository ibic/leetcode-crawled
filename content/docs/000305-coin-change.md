---
title: "Coin Change"
weight: 305
#id: "coin-change"
---
## Description
<div class="description">
<p>You are given coins of different denominations and a total amount of money <i>amount</i>. Write a function to compute the fewest number of coins that you need to make up that amount. If that amount of money cannot be made up by any combination of the coins, return <code>-1</code>.</p>

<p>You may assume that you have an infinite number of each kind of coin.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> coins = [1,2,5], amount = 11
<strong>Output:</strong> 3
<strong>Explanation:</strong> 11 = 5 + 5 + 1
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> coins = [2], amount = 3
<strong>Output:</strong> -1
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> coins = [1], amount = 0
<strong>Output:</strong> 0
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> coins = [1], amount = 1
<strong>Output:</strong> 1
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> coins = [1], amount = 2
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= coins.length &lt;= 12</code></li>
	<li><code>1 &lt;= coins[i] &lt;= 2<sup>31</sup> - 1</code></li>
	<li><code>0 &lt;= amount &lt;= 10<sup>4</sup></code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 17 (taggedByAdmin: false)
- Goldman Sachs - 17 (taggedByAdmin: false)
- Bloomberg - 9 (taggedByAdmin: false)
- ByteDance - 6 (taggedByAdmin: false)
- BlackRock - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Grab - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Capital One - 12 (taggedByAdmin: false)
- Microsoft - 7 (taggedByAdmin: false)
- VMware - 4 (taggedByAdmin: false)
- Oracle - 4 (taggedByAdmin: false)
- Zappos - 3 (taggedByAdmin: false)
- Airbnb - 3 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Affirm - 2 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- JPMorgan - 20 (taggedByAdmin: false)
- Yahoo - 4 (taggedByAdmin: false)
- Visa - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Summary

This article is for intermediate users. It introduces the following ideas:
Backtracking, Dynamic programming.

## Solution

---
#### Approach #1 (Brute force) [Time Limit Exceeded]

**Intuition**


The problem could be modeled as the following optimization problem :
$$
\min_{x} \sum_{i=0}^{n - 1} x_i \\
\text{subject to} \sum_{i=0}^{n - 1} x_i*c_i = S
$$

, where $$S$$ is the amount,    $$c_i$$ is the coin denominations, $$x_i$$  is the number of coins with denominations $$c_i$$ used in change of amount $$S$$. We could easily see that $$x_i = [{0, \frac{S}{c_i}}]$$.

A trivial solution is to enumerate all subsets of coin frequencies $$[x_0\dots\ x_{n - 1}]$$  that satisfy the constraints above, compute their sums and return the minimum among them.

**Algorithm**

To apply this idea, the algorithm uses backtracking technique to generate all combinations of coin frequencies $$[x_0\dots\ x_{n-1}]$$ in the range \([{0, \frac{S}{c_i}}]\) which satisfy the constraints above. It makes a sum of the combinations and returns their minimum or $$-1$$ in case there is no acceptable combination.

<iframe src="https://leetcode.com/playground/3APj2Aym/shared" frameBorder="0" width="100%" height="497" name="3APj2Aym"></iframe>

**Complexity Analysis**

* Time complexity : $$O(S^n)$$. In the worst case, complexity is exponential in the number of the coins $$n$$. The reason is that every coin denomination $$c_i$$ could have at most $$\frac{S}{c_i}$$ values. Therefore the number of possible combinations is :

$$
\frac{S}{c_1}*\frac{S}{c_2}*\frac{S}{c_3}\ldots\frac{S}{c_n} = \frac{S^{n}}{{c_1}*{c_2}*{c_3}\ldots{c_n}}
$$



* Space complexity : $$O(n)$$.
In the worst case the maximum depth of recursion is $$n$$. Therefore we need $$O( n)$$ space used by the system recursive stack.

---
#### Approach #2 (Dynamic programming - Top down) [Accepted]

**Intuition**

Could we improve the exponential solution above? Definitely! The problem could be solved with polynomial time using Dynamic programming technique. First, let's define:
> $$F(S)$$ - minimum number of coins needed to make change for amount $$S$$ using coin denominations $$[{c_0\ldots c_{n-1}}]$$


We note that this problem has an optimal substructure property, which is the key piece in solving any Dynamic Programming problems. In other words, the optimal solution can be constructed from optimal solutions of its subproblems.
How to split the problem into subproblems? Let's assume that we know $$F(S)$$ where some change $$val_1, val_2, \ldots$$ for $$S$$ which is optimal and the last coin's denomination is $$C$$.
Then the following equation should be true because of optimal substructure of the problem:

$$
F(S) = F(S - C) + 1
$$

But we don't know which is the denomination of the last coin $$C$$. We compute  $$F(S - c_i)$$ for each possible denomination $$c_0, c_1, c_2 \ldots c_{n -1}$$ and choose the minimum among them. The following recurrence relation holds:

$$
F(S) = \min_{i=0 ... n-1} { F(S - c_i) } + 1 \\
\text{subject to} \ \  S-c_i \geq 0 \\
$$

$$
F(S) = 0 \ , \text{when} \ S = 0 \\
F(S) = -1 \ , \text{when} \ n = 0
$$

![Recursion tree for finding coin change of amount 6 with coin denominations {1,2,3}.](https://leetcode.com/media/original_images/322_coin_change_tree.png){:width="100%"}
{:align="center"}


In the recursion tree above, we could see that a lot of subproblems were calculated multiple times.  For example the problem $$F(1)$$ was calculated $$13$$ times. Therefore we should cache the solutions to the subproblems in a table and access them in constant time when necessary

**Algorithm**

The idea of the algorithm is to build the solution of the problem from top to bottom. It applies the idea described above. It use backtracking and cut the partial solutions in the recursive tree, which doesn't lead to a viable solution. Тhis happens when we try to make a change of a coin with a value greater than the amount *$$S$$*. To improve  time complexity we should store the solutions of the already calculated subproblems in a table.

<iframe src="https://leetcode.com/playground/cFv7meyP/shared" frameBorder="0" width="100%" height="412" name="cFv7meyP"></iframe>

**Complexity Analysis**

* Time complexity : $$O(S*n)$$. where S is the amount, n is denomination count.
In the worst case the recursive tree of the algorithm has height of $$S$$ and the algorithm  solves only $$S$$ subproblems because it caches precalculated solutions in a table. Each subproblem is computed with  $$n$$ iterations, one by coin denomination. Therefore there is $$O(S*n)$$ time complexity.

* Space complexity : $$O(S)$$, where $$S$$ is the amount to change
We use extra space for the memoization table.

---
#### Approach #3 (Dynamic programming - Bottom up) [Accepted]

**Algorithm**

For the iterative solution, we think in bottom-up manner. Before calculating *$$F(i)$$*, we have to compute all minimum counts for amounts up to $$i$$. On each iteration $$i$$ of the algorithm *$$F(i)$$* is computed as $$\min_{j=0 \ldots n-1}{F(i -c_j)} + 1$$


![Bottom-up approach using a table to build up the solution to F6.](https://leetcode.com/media/original_images/322_coin_change_table.png){:width="539px"}
{:align="center"}

In the example above you can see that:

$$
\begin{align}
F(3) &= \min\{{F(3- c_1), F(3-c_2), F(3-c_3)}\} + 1 \\
&= \min\{{F(3- 1), F(3-2), F(3-3)}\} + 1 \\
&= \min\{{F(2), F(1), F(0)}\} + 1 \\
&= \min\{{1, 1, 0}\} + 1 \\
&= 1
\end{align}
$$


<iframe src="https://leetcode.com/playground/YFt8weFw/shared" frameBorder="0" width="100%" height="327" name="YFt8weFw"></iframe>

**Complexity Analysis**

* Time complexity : $$O(S*n)$$.
On each step the algorithm finds the next *$$F(i)$$* in $$n$$ iterations, where $$1\leq i \leq S$$. Therefore in total the iterations are $$S*n$$.
* Space complexity : $$O(S)$$.
We use extra space for the memoization table.

Analysis written by: @elmirap.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] O(n*amount) time O(amount) space DP solution
- Author: wyattliu
- Creation Date: Sun Dec 27 2015 09:32:05 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 17 2018 10:35:43 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        int coinChange(vector<int>& coins, int amount) {
            int Max = amount + 1;
            vector<int> dp(amount + 1, Max);
            dp[0] = 0;
            for (int i = 1; i <= amount; i++) {
                for (int j = 0; j < coins.size(); j++) {
                    if (coins[j] <= i) {
                        dp[i] = min(dp[i], dp[i - coins[j]] + 1);
                    }
                }
            }
            return dp[amount] > amount ? -1 : dp[amount];
        }
    };
</p>


### *Java* Both iterative and recursive solutions with explanations
- Author: ElementNotFoundException
- Creation Date: Sun Dec 27 2015 11:22:11 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 20:04:59 GMT+0800 (Singapore Standard Time)

<p>
#Recursive Method:#
The idea is very classic dynamic programming: think of the last step we take. Suppose we have already found out the best way to sum up to amount `a`, then for the last step, we can choose any coin type which gives us a remainder `r` where `r = a-coins[i]` for all `i`'s. For every remainder, go through exactly the same process as before until either the remainder is 0 or less than 0 (meaning not a valid solution). With this idea, the only remaining detail is to store the minimum number of coins needed to sum up to `r` so that we don't need to recompute it over and over again.

Code in Java:

    public class Solution {
    public int coinChange(int[] coins, int amount) {
        if(amount<1) return 0;
        return helper(coins, amount, new int[amount]);
    }
    
    private int helper(int[] coins, int rem, int[] count) { // rem: remaining coins after the last step; count[rem]: minimum number of coins to sum up to rem
        if(rem<0) return -1; // not valid
        if(rem==0) return 0; // completed
        if(count[rem-1] != 0) return count[rem-1]; // already computed, so reuse
        int min = Integer.MAX_VALUE;
        for(int coin : coins) {
            int res = helper(coins, rem-coin, count);
            if(res>=0 && res < min)
                min = 1+res;
        }
        count[rem-1] = (min==Integer.MAX_VALUE) ? -1 : min;
        return count[rem-1];
    }
    }



#Iterative Method:#
For the iterative solution, we think in bottom-up manner. Suppose we have already computed all the minimum counts up to `sum`, what would be the minimum count for `sum+1`?

Code in Java:

    public class Solution {
    public int coinChange(int[] coins, int amount) {
        if(amount<1) return 0;
        int[] dp = new int[amount+1];
        int sum = 0;
        
    	while(++sum<=amount) {
    		int min = -1;
        	for(int coin : coins) {
        		if(sum >= coin && dp[sum-coin]!=-1) {
        			int temp = dp[sum-coin]+1;
        			min = min<0 ? temp : (temp < min ? temp : min);
        		}
        	}
        	dp[sum] = min;
    	}
    	return dp[amount];
    }
    }

If you are interested in my other posts, please feel free to check my Github page here: [https://github.com/F-L-A-G/Algorithms-in-Java][1]


  [1]: https://github.com/F-L-A-G/Algorithms-in-Java
</p>


### Clean dp python code
- Author: lime66
- Creation Date: Wed Dec 30 2015 13:13:12 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 16:29:50 GMT+0800 (Singapore Standard Time)

<p>
Assume `dp[i]` is the fewest number of coins making up amount `i`, then for every `coin` in `coins`, dp[i] = min(dp[i - coin] + 1). 

The time complexity is `O(amount * coins.length)` and the space complexity is `O(amount)`

    class Solution(object):
        def coinChange(self, coins, amount):
            MAX = float('inf')
            dp = [0] + [MAX] * amount
    
            for i in xrange(1, amount + 1):
                dp[i] = min([dp[i - c] if i - c >= 0 else MAX for c in coins]) + 1
    
            return [dp[amount], -1][dp[amount] == MAX]
</p>


