---
title: "Find N Unique Integers Sum up to Zero"
weight: 1225
#id: "find-n-unique-integers-sum-up-to-zero"
---
## Description
<div class="description">
<p>Given an integer <code>n</code>, return <strong>any</strong> array containing <code>n</code> <strong>unique</strong>&nbsp;integers such that they add up to 0.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 5
<strong>Output:</strong> [-7,-1,1,3,4]
<strong>Explanation:</strong> These arrays also are accepted [-5,-1,1,2,3] , [-3,-1,2,-2,4].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 3
<strong>Output:</strong> [-1,0,1]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 1
<strong>Output:</strong> [0]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 1000</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Find the Rule
- Author: lee215
- Creation Date: Tue Dec 31 2019 01:11:21 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jan 10 2020 11:05:24 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Naive idea
`n = 1, [0]`
`n = 2, [-1, 1]`

Now write more based on this
`n = 3, [-2, 0, 2]`
`n = 4, [-3, -1, 1, 3]`
`n = 5, [-4, -2, 0, 2, 4]`

It spreads like the wave.
<br>

## **Explanation**
Find the rule
`A[i] = i * 2 - n + 1`
<br>

## **Math Observation**
@zzg_zzm helps explain in math.

Actually, this rule could be derived from constructing an arithmetic sequence.

(Note that any arithmetic sequence must have unique values if the common delta is non-zero)

We need the sequence sum, so that

`(a[0] + a[n-1]) * n / 2 = 0`, which means `a[0] + a[n-1] = 0`.

Note that `a[n-1] - a[0] = (n-1) * delta`, which is `-2 * a[0]`,

so we simply set `delta = 2, a[0] = 1 - n`
<br>


## **Note**
It\'s not bad to sum up `1 + 2 + 3 + ... + (N - 1)`.
Personally I don\'t really like it much.
What is the possible problem of this approach?
It doesn\'t work if `N` goes up to `10^5`
<br>

## **Complexity**
Time `O(N)`
Space `O(N)`
<br>

**Java:**
```java
    public int[] sumZero(int n) {
        int[] A = new int[n];
        for (int i = 0; i < n; ++i)
            A[i] = i * 2 - n + 1;
        return A;
    }
```

**C++:**
```cpp
    vector<int> sumZero(int n) {
        vector<int> A(n);
        for (int i = 0; i < n; ++i)
            A[i] = i * 2 - n + 1;
        return A;
    }
```

**Python:**
```python
    def sumZero(self, n):
        return range(1 - n, n, 2)
```

</p>


### Simple Java : Fill from both sides
- Author: anshu4intvcom
- Creation Date: Sun Dec 29 2019 12:14:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 29 2019 12:14:58 GMT+0800 (Singapore Standard Time)

<p>
- start filling up from left and right complementary values (so if we insert 1 from left, insert -1 from right, then insert 2 from left and insert -2 from right and so on) :

```
public int[] sumZero(int n) {
        int[] res = new int[n];
        int left = 0, right = n - 1, start = 1;
        while (left < right) {
            res[left++] = start;
            res[right--] = -start;
            start++;
        }
        return res;
    }
```
</p>


### Keep it simple. Add all values till n-1 and then balance it with -sum.
- Author: Natanu12
- Creation Date: Sun Dec 29 2019 12:56:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 29 2019 19:55:01 GMT+0800 (Singapore Standard Time)

<p>
Edited. As @StefanPochmann pointed out rightly. starting from i+1 instead of i to avoid the case of 0 and 0 when n = 2.
```
class Solution {
    public int[] sumZero(int n) {
        int arr[] = new int[n];
        int sum = 0;
        for(int i = 0; i < n-1; i++) {
            arr[i] = i+1;
            sum += arr[i];
        }
        arr[n-1] = -sum;
        return arr;
    }
}
</p>


