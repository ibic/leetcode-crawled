---
title: "Coin Path"
weight: 588
#id: "coin-path"
---
## Description
<div class="description">
<p>Given an array <code>A</code> (index starts at <code>1</code>) consisting of N integers: A<sub>1</sub>, A<sub>2</sub>, ..., A<sub>N</sub>&nbsp;and an integer <code>B</code>. The integer <code>B</code> denotes that from any place (suppose the index is <code>i</code>) in the array <code>A</code>, you can jump to any one of the place in the array <code>A</code> indexed <code>i+1</code>, <code>i+2</code>, &hellip;, <code>i+B</code> if this place can be jumped to. Also, if you step on the index <code>i</code>, you have to pay A<sub>i</sub>&nbsp;coins. If A<sub>i</sub>&nbsp;is -1, it means you can&rsquo;t jump to the place indexed <code>i</code> in the array.</p>

<p>Now, you start from the place indexed <code>1</code> in the array <code>A</code>, and your aim is to reach the place indexed <code>N</code> using the minimum coins. You need to return the path of indexes (starting from 1 to N) in the array you should take to get to the place indexed <code>N</code> using minimum coins.</p>

<p>If there are multiple paths with the same cost, return the lexicographically smallest such path.</p>

<p>If it&#39;s not possible to reach the place indexed N then you need to return an empty array.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> [1,2,4,-1,2], 2
<b>Output:</b> [1,3,5]
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> [1,2,4,-1,2], 1
<b>Output:</b> []
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>Path Pa<sub>1</sub>, Pa<sub>2</sub>, ..., Pa<sub>n</sub>&nbsp;is lexicographically smaller than Pb<sub>1</sub>, Pb<sub>2</sub>, ..., Pb<sub>m</sub>, if and only if at the first <code>i</code> where Pa<sub>i</sub>&nbsp;and Pb<sub>i</sub>&nbsp;differ, Pa<sub>i</sub>&nbsp;&lt; Pb<sub>i</sub>; when no such&nbsp;<code>i</code>&nbsp;exists, then&nbsp;<code>n</code> &lt; <code>m</code>.</li>
	<li>A<sub>1</sub> &gt;= 0. A<sub>2</sub>, ..., A<sub>N</sub> (if exist) will in the range of [-1, 100].</li>
	<li>Length of A is in the range of [1, 1000].</li>
	<li>B is in the range of [1, 100].</li>
</ol>

<p>&nbsp;</p>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]


## Solution

---
#### Approach #1 Brute Force[Time Limit Exceeded]

In this approach, we make use of a $$next$$ array of size $$n$$. Here, $$n$$ refers to the size of the given $$A$$ array. The array $$nums$$ is used such that $$nums[i]$$ is used to store the minimum number of coins needed to jump till the end of the array $$A$$, starting from the index $$i$$. 

We start by filling the $$next$$ array with all -1's. Then, in order to fill this $$next$$ array, we make use of a recursive function `jump(A, B, i, next)` which fills the $$next$$ array starting from the index $$i$$ onwards, given $$A$$ as the coins array and $$B$$ as the largest jump value.

With $$i$$ as the current index, we can consider every possible index from $$i+1$$ to $$i+B$$ as the next place to be jumped to. For every such next index, $$j$$, if this place can be jumped to, we determine the cost of reaching the end of the array starting from the index $$i$$, and with $$j$$ as the next index jumped from $$i$$, as  $$A[i] + jump(A, B, j, next)$$. If this cost is lesser than the minimum cost required till now, for the same starting index, we can update the minimum cost and the value of $$next[i]$$ as well. 

For every such function call, we also need to return this minimum cost.

At the end, we traverse over the $$next$$ array starting from the index 1. At every step, we add the current index to the $$res$$ list to be returned and also jump/move to the index pointed by $$next[i]$$, since this refers to the next index for the minimum cost. We continue in the same manner till we reach the end of the array $$A$$.

<iframe src="https://leetcode.com/playground/9tcSoGzo/shared" frameBorder="0" name="9tcSoGzo" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(B^n)$$. The size of the recursive tree can grow upto $$O(b^n)$$ in the worst case. This is because, we have $$B$$ possible branches at every step. Here, $$B$$ refers to the limit of the largest jump and $$n$$ refers to the size of the given $$A$$ array. 

* Space complexity : $$O(n)$$. The depth of the recursive tree can grow upto $$n$$. $$next$$ array of size $$n$$ is used.

---
#### Approach #2 Using Memoization [Accepted]

**Algorithm**

In the recursive solution just discussed, a lot of duplicate function calls are made, since we are considering the same index through multiple paths. To remove this redundancy, we can make use of memoization.

We keep a $$memo$$ array, such that $$memo[i]$$ is used to store the minimum cost of jumps to reach the end of the array $$A$$. Whenever the value for any index is calculated once, it is stored in its appropriate location. Thus, next time whenever the same function call is made, we can return the result directly from this $$memo$$ array, pruning the search space to a great extent.

<iframe src="https://leetcode.com/playground/mAVq4r5H/shared" frameBorder="0" name="mAVq4r5H" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(nB)$$. $$memo$$ array of size $$n$$ is filled only once. We also do a traversal over the $$next$$ array, which will go upto $$B$$ steps.  Here, $$n$$ refers to the number of nodes in the given tree.

* Space complexity : $$O(n)$$. The depth of the recursive tree can grow upto $$n$$. $$next$$ array of size $$n$$ is used.

---
#### Approach #3 Using Dynamic Programming [Accepted]

**Algorithm**

From the solutions discussed above, we can observe that the cost of jumping till the end of the array $$A$$ starting from the index $$i$$ is only dependent on the elements following the index $$i$$ and not the ones before it. This inspires us to make use of Dynamic Programming to solve the current problem.

We again make use of a $$next$$ array to store the next jump locations. We also make use of a $$dp$$ with the same size as that of the given $$A$$ array. $$dp[i]$$ is used to store the minimum cost of jumping till the end of the array $$A$$, starting from the index $$i$$. We start with the last index as the current index and proceed backwards for filling the $$next$$ and $$dp$$ array.

With $$i$$ as the current index, we consider all the next possible positions from $$i+1$$, $$i+2$$,..., $$i+B$$, and determine the position, $$j$$,  which leads to a minimum cost of reaching the end of $$A$$, which is given by $$A[i]+dp[j]$$. We update $$next[i]$$ with this corresponding index. We also update $$dp[i]$$ with the minimum cost, to be used by the previous indices' cost calculations.

At the end, we again jump over the indices as per the $$next$$ array and put these indices in the $$res$$ array to be returned.

<iframe src="https://leetcode.com/playground/BMCVq3jo/shared" frameBorder="0" name="BMCVq3jo" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(nB)$$. We need to consider all the possible $$B$$ positions for every current index considered in the $$A$$ array. Here, $$A$$ refers to the number of elements in $$A$$.

* Space complexity : $$O(n)$$. $$dp$$ and $$next$$ array of size $$n$$ are used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++, DP, O(nB) time O(n) space
- Author: zestypanda
- Creation Date: Sun Aug 06 2017 12:09:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 23:27:08 GMT+0800 (Singapore Standard Time)

<p>
This is a classic DP problem. dp[k] (starting from k = 0) is the minimum coins from A*k+1* to A*n*, and pos[k] is the next place to jump from A*k+1*.

If working backward from dp[n-1] to dp[0], and considering smaller index first, i.e. i+1 to i+B, there is no need to worry about lexicographical order. I argue pos[k] always holds the lexicographically smallest path from k to n-1, i.e. from A*k+1* to A*n*. The prove is as below. 

Clearly, when k = n-1, it is true because there is only 1 possible path, which is [n]. When k = i and i < n-1, we search for an index j, which has smallest cost or smallest j if the same cost. If there are >= 2 paths having the same minimum cost, for example,
P = [k+1, j+1, ..., n]
Q = [k+1, m+1, ..., n] (m > j)
The path P with smaller index j is always the lexicographically smaller path. 
So the argument is true by induction.    
```
class Solution {
public:
    vector<int> cheapestJump(vector<int>& A, int B) {
        vector<int> ans;
        if (A.empty() || A.back() == -1) return ans;
        int n = A.size();
        vector<int> dp(n, INT_MAX), pos(n, -1);
        dp[n-1] = A[n-1];
        // working backward
        for (int i = n-2; i >= 0; i--) {
            if (A[i] == -1) continue;
            for (int j = i+1; j <= min(i+B, n-1); j++) {
                if (dp[j] == INT_MAX) continue;
                if (A[i]+dp[j] < dp[i]) {
                    dp[i] = A[i]+dp[j];
                    pos[i] = j;
                }
            }
        }
        // cannot jump to An
        if (dp[0] == INT_MAX) return ans;
        int k = 0;
        while (k != -1) {
            ans.push_back(k+1);
            k = pos[k];
        }
        return ans;
    }
};
```
</p>


### Java 22 lines solution with proof
- Author: yuxiangmusic
- Creation Date: Sun Aug 06 2017 20:30:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 06 2017 20:30:59 GMT+0800 (Singapore Standard Time)

<p>
The following solution is based on that:

**If there are two path to reach `n`, and they have the same optimal cost, then the longer path is lexicographically smaller**.

**Proof by contradiction**:
Assume path `P` and `Q` have the same cost, and `P` is strictly shorter and `P` is lexicographically smaller. 
Since `P` is lexicographically smaller, `P` and `Q` must start to differ at some point.
In other words, there must be `i` in `P` and `j` in `Q` such that `i < j` and `len([1...i]) == len([1...j])`
`P = [1...i...n]`
`Q = [1...j...n]`
Since `i` is further away from `n` there need to be no less steps taken to jump from `i` to `n` **unless `j` to `n` is not optimal**
So `len([i...n]) >= len([j...n])`
So `len(P) >= len(Q)` which contradicts the assumption that `P` is strictly shorter.

For example:
Input: `[1, 4, 2, 2, 0], 2`
Path `P = [1, 2, 5]`
Path `Q = [1, 3, 4, 5]`
They both have the same cost `4` to reach `n`
They differ at `i = 2` in `P` and `j = 3` in `Q`
Here `Q` is longer but not lexicographically smaller.
Why? Because `j = 3` to `n = 5` is not optimal.
The optimal path should be `[1, 3, 5]` where the cost is only `2`

```
    public List<Integer> cheapestJump(int[] A, int B) {
        int n = A.length;
        int[] c = new int[n]; // cost
        int[] p = new int[n]; // previous index
        int[] l = new int[n]; // length
        Arrays.fill(c, Integer.MAX_VALUE);
        Arrays.fill(p, -1);
        c[0] = 0;
        for (int i = 0; i < n; i++) {
            if (A[i] == -1) continue;
            for (int j = Math.max(0, i - B); j < i; j++) {
                if (A[j] == -1) continue;
                int alt = c[j] + A[i];
                if (alt < c[i] || alt == c[i] && l[i] < l[j] + 1) {
                    c[i] = alt;
                    p[i] = j;
                    l[i] = l[j] + 1;
                }
            }
        }
        List<Integer> path = new ArrayList<>();
        for (int cur = n - 1; cur >= 0; cur = p[cur]) path.add(0, cur + 1);
        return path.get(0) != 1 ? Collections.emptyList() : path;
    }
```
</p>


### Python Straight Forward Solution
- Author: lee215
- Creation Date: Sun Aug 06 2017 12:43:12 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 20 2018 13:05:05 GMT+0800 (Singapore Standard Time)

<p>
I used DP. dp[i] represent the best path found to get to the place indexed ````i + 1```` and ````dp[i][0]```` is the cost of the path.
````dp[0]```` is initialized as ````[A[0], 1]```` and the others are initialized as ````[infinity]````.
````
def cheapestJump(self, A, B):
        if not A or A[0] == -1: return []
        dp = [[float('inf')] for _ in A]
        dp[0] = [A[0], 1]
        for j in range(1, len(A)):
            if A[j] == -1: continue
            dp[j] = min([dp[i][0] + A[j]] + dp[i][1:] + [j + 1] for i in range(max(0, j - B), j))
        return dp[-1][1:] if dp[-1][0] < float('inf') else []
</p>


