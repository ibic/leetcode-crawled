---
title: "Students and Examinations"
weight: 1556
#id: "students-and-examinations"
---
## Description
<div class="description">
<p>Table: <code>Students</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| student_id    | int     |
| student_name  | varchar |
+---------------+---------+
student_id is the primary key for this table.
Each row of this table contains the ID and the name of one student in the school.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Subjects</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| subject_name | varchar |
+--------------+---------+
subject_name is the primary key for this table.
Each row of this table contains the name of one subject in the school.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Examinations</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| student_id   | int     |
| subject_name | varchar |
+--------------+---------+
There is no primary key for this table. It may contain duplicates.
Each student from the Students table takes every course from Subjects table.
Each row of this table indicates that a student with ID student_id attended the exam of subject_name.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the number of times each student attended each exam.</p>

<p>Order the result table by <code>student_id</code> and <code>subject_name</code>.</p>

<p>The query result format is in the following example:</p>

<pre>
Students table:
+------------+--------------+
| student_id | student_name |
+------------+--------------+
| 1          | Alice        |
| 2          | Bob          |
| 13         | John         |
| 6          | Alex         |
+------------+--------------+
Subjects table:
+--------------+
| subject_name |
+--------------+
| Math         |
| Physics      |
| Programming  |
+--------------+
Examinations table:
+------------+--------------+
| student_id | subject_name |
+------------+--------------+
| 1          | Math         |
| 1          | Physics      |
| 1          | Programming  |
| 2          | Programming  |
| 1          | Physics      |
| 1          | Math         |
| 13         | Math         |
| 13         | Programming  |
| 13         | Physics      |
| 2          | Math         |
| 1          | Math         |
+------------+--------------+
Result table:
+------------+--------------+--------------+----------------+
| student_id | student_name | subject_name | attended_exams |
+------------+--------------+--------------+----------------+
| 1          | Alice        | Math         | 3              |
| 1          | Alice        | Physics      | 2              |
| 1          | Alice        | Programming  | 1              |
| 2          | Bob          | Math         | 1              |
| 2          | Bob          | Physics      | 0              |
| 2          | Bob          | Programming  | 1              |
| 6          | Alex         | Math         | 0              |
| 6          | Alex         | Physics      | 0              |
| 6          | Alex         | Programming  | 0              |
| 13         | John         | Math         | 1              |
| 13         | John         | Physics      | 1              |
| 13         | John         | Programming  | 1              |
+------------+--------------+--------------+----------------+
The result table should contain all students and all subjects.
Alice attended Math exam 3 times, Physics exam 2 times and Programming exam 1 time.
Bob attended Math exam 1 time, Programming exam 1 time and didn&#39;t attend the Physics exam.
Alex didn&#39;t attend any exam.
John attended Math exam 1 time, Physics exam 1 time and Programming exam 1 time.
</pre>

</div>

## Tags


## Companies
- Roblox - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple MySQL solution beats 100% with LEFT JOIN (no subquery)
- Author: eminem18753
- Creation Date: Fri Dec 06 2019 22:36:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 17 2020 11:59:17 GMT+0800 (Singapore Standard Time)

<p>
```
# Write your MySQL query statement below
SELECT student.student_id,student.student_name,subject.subject_name,COUNT(exam.subject_name) as attended_exams
FROM Students as student
JOIN Subjects as subject
LEFT JOIN Examinations as exam
ON student.student_id=exam.student_id AND subject.subject_name=exam.subject_name
GROUP BY student.student_id,subject.subject_name
ORDER BY student_id,subject_name;
```
</p>


### [MySQL] two solutions
- Author: ye15
- Creation Date: Thu Feb 06 2020 00:27:20 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Feb 06 2020 00:27:20 GMT+0800 (Singapore Standard Time)

<p>
JOIN (302ms, 99.60%): 
```
SELECT 
    student_id, 
    student_name, 
    subject_name, 
    IFNULL(attended_exams, 0) AS attended_exams
FROM 
    Students 
    CROSS JOIN Subjects
    LEFT JOIN (SELECT student_id, subject_name, COUNT(*) AS attended_exams 
               FROM Examinations 
               GROUP BY student_id, subject_name) a USING (student_id, subject_name)
ORDER BY student_id, subject_name;
```

Alternatively (1351ms, 14.91%): 
```
SELECT 
    student_id, 
    student_name, 
    subject_name, 
    COUNT(a.subject_name) AS attended_exams
FROM 
    Students 
    CROSS JOIN Subjects
    LEFT JOIN Examinations a USING (student_id, subject_name)
GROUP BY student_id, subject_name
ORDER BY student_id, subject_name;
```
</p>


### Simple SQL without join
- Author: Merciless
- Creation Date: Sat Dec 07 2019 05:14:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Dec 07 2019 05:14:31 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT student_id, student_name, subject_name, 
(
    SELECT ifnull(COUNT(*),0)
    FROM examinations e
    WHERE e.student_id = st.student_id and e.subject_name = s.subject_name
) AS attended_exams
FROM students st, subjects s
GROUP BY student_id, subject_name
</p>


