---
title: "Minimum Time to Collect All Apples in a Tree"
weight: 1329
#id: "minimum-time-to-collect-all-apples-in-a-tree"
---
## Description
<div class="description">
<p>Given an undirected tree consisting of <code>n</code> vertices numbered from 0 to <code>n-1</code>, which has some apples in their&nbsp;vertices. You spend 1 second to walk over one&nbsp;edge of the tree.&nbsp;<em>Return the minimum time in seconds&nbsp;you have to spend&nbsp;in order to collect all apples in the tree starting at <strong>vertex 0</strong> and coming back to this vertex.</em></p>

<p>The edges of the undirected tree are given in the array <code>edges</code>, where <code>edges[i] = [from<sub>i</sub>, to<sub>i</sub>]</code> means that exists an edge connecting the vertices <code>from<sub>i</sub></code> and <code>to<sub>i</sub></code>. Additionally, there is&nbsp;a boolean array <code>hasApple</code>, where <code>hasApple[i] = true</code>&nbsp;means that&nbsp;vertex <code>i</code> has an apple, otherwise, it does not have any apple.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/04/23/min_time_collect_apple_1.png" style="width: 300px; height: 212px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 7, edges = [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]], hasApple = [false,false,true,false,true,true,false]
<strong>Output:</strong> 8 
<strong>Explanation:</strong> The figure above represents the given tree where red vertices have an apple. One optimal path to collect all apples is shown by the green arrows.  
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/04/23/min_time_collect_apple_2.png" style="width: 300px; height: 212px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 7, edges = [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]], hasApple = [false,false,true,false,false,true,false]
<strong>Output:</strong> 6
<strong>Explanation:</strong> The figure above represents the given tree where red vertices have an apple. One optimal path to collect all apples is shown by the green arrows.  
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 7, edges = [[0,1],[0,2],[1,4],[1,5],[2,3],[2,6]], hasApple = [false,false,false,false,false,false,false]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^5</code></li>
	<li><code>edges.length == n-1</code></li>
	<li><code>edges[i].length == 2</code></li>
	<li><code>0 &lt;= from<sub>i</sub>, to<sub>i</sub> &lt;= n-1</code></li>
	<li><code>from<sub>i</sub>&nbsp;&lt; to<sub>i</sub></code></li>
	<li><code>hasApple.length == n</code></li>
</ul>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Concise explanation with a Picture for Visualization
- Author: interviewrecipes
- Creation Date: Sun May 10 2020 12:01:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 08 2020 13:23:26 GMT+0800 (Singapore Standard Time)

<p>

**Key idea**
Whenever you are at a node, say p, you will collect all apples in p\u2019s subtree before returning back to the original root. This will avoid traveling the same path multiple times. 
Say, root is where we start, p is a node in the tree and p has two children - child1, child2 - and both of them have an apple each.
`root -> p -> child1 -> p -> child2 -> p -> root `is always going to be better than
`root-> p -> child1 -> p -> root -> p -> child2 -> p -> root`.

So now it becomes a simple graph traversal problem, particularly DFS where we visit all children of the node first and then go back the other nodes.

**Algorithm**
1. Because you have the list of edges, construct the graph first to have a better representation of the graph.
2. For each node, check if any of its children have apples and find the total cost in terms of time in seconds of collecting those.
3. If there is at least one child with an apple, then we have to collect it. So we will also have to add the cost (time required) of reaching that node.
4. return the total time.

Following image should help in understanding better.

**Please upvote** if you find this helpful, it would be encouraging! 

Let me know if something is unclear and I will fix it. :)
Thanks.


![image](https://assets.leetcode.com/users/interviewrecipes/image_1591669743.png)




**[EDIT]**
It looks like some cases were failing because I assumed I won\'t need the bidirectional graph. As correctly pointed out by @hiepit, I was wrong. Fixed the code. :)

**C++**
```
class Solution {
public:
    unordered_map<int, vector<int>> g; // to store the graph
    unordered_map<int, bool> visited; // to stop exploring same nodes again and again.
	
    void createGraph(vector<vector<int>>& edges) {
      for (auto e: edges) {
        g[e[0]].push_back(e[1]); // adjecency list representation
		g[e[1]].push_back(e[0]); // adjecency list representation
      }
    }
  
    int dfs(int node, int myCost, vector<bool>& hasApple) {
	  if (visited[node]) {
		  return 0;
	  }
	  visited[node] = true;
	  
      int childrenCost = 0; // cost of traversing all children. 
      for (auto x: g[node]) { 
        childrenCost += dfs(x, 2, hasApple);  // check recursively for all apples.
      }

      if (childrenCost == 0 && hasApple[node] == false) {
	  // If no child has apples, then we won\'t traverse the subtree, so cost will be zero.
	  // similarly, if current node also does not have the apple, we won\'t traverse this branch at all, so cost will be zero.
        return 0;
      }
	  
	  // Children has at least one apple or the current node has an apple, so add those costs.
      return (childrenCost + myCost);
    }
  
    int minTime(int n, vector<vector<int>>& edges, vector<bool>& hasApple) {
      createGraph(edges); // construct the graph first.
      return dfs(0, 0, hasApple); // cost of reaching the root is 0. For all others, its 2.
    }
};

```

**Java**
```
class Solution {
    public int minTime(int n, int[][] edges, List<Boolean> hasApple) {
        
        Map<Integer, List<Integer>> graph = createGraph(edges); // to store the graph
        Map<Integer, Boolean> visited = new HashMap<>();
		
        return dfs(graph, 0, hasApple, 0, visited); // cost of reaching the root is 0. For all others, its 2.
      
    }
    
    private int dfs(Map<Integer, List<Integer>> graph, int node, List<Boolean> hasApple, int myCost, Map<Integer, Boolean> visited) {
        Boolean v = visited.getOrDefault(node, false);
		if (v) {
			return 0;
		}
		visited.put(node, true);
		
        int childrenCost = 0; // cost of traversing all children. 
      
        for(int n : graph.getOrDefault(node, new ArrayList<>())) {
            childrenCost += dfs(graph, n, hasApple, 2, visited); // check recursively for all apples in subtrees.
        }
      
        if (childrenCost == 0 && hasApple.get(node) == false) {
          // If no child has apples, then we won\'t traverse the subtree, so cost will be zero.
          // similarly, if current node also does not have the apple, we won\'t traverse this branch at all, so cost will be zero.
          return 0;
        }
      
        return childrenCost + myCost;
    }
    
    private Map<Integer, List<Integer>> createGraph(int[][] edges) {
        Map<Integer, List<Integer>> graph = new HashMap<>();
      
        for(int i = 0; i < edges.length; i++) {
            List<Integer> list = graph.getOrDefault(edges[i][0], new ArrayList<>()); // Adjecency list representation.
            list.add(edges[i][1]);
            graph.put(edges[i][0], list);
			
			list = graph.getOrDefault(edges[i][1], new ArrayList<>()); // Adjecency list representation.
            list.add(edges[i][0]);
            graph.put(edges[i][1], list);
        }
      
        return graph;
    }
}
```

**Python 3** (Thanks to @venetor for this Python 3 solution.)
```
def minTime(self, n: int, edges: List[List[int]], hasApple: List[bool]) -> int:
    adj = [[] for _ in range(n)]
    for u, v in edges:
        adj[u].append(v)
        adj[v].append(u)

    visited = set()
    def dfs(node):
        if node in visited:
            return 0
        visited.add(node)
        secs = 0
        for child in adj[node]:
            secs += dfs(child)
        if secs > 0:
            return secs + 2
        return 2 if hasApple[node] else 0

    return max(dfs(0) - 2, 0)
```

</p>


### [Java] Detailed Explanation - Build Tree + DFS
- Author: frankkkkk
- Creation Date: Sun May 10 2020 12:01:49 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 11 2020 00:46:06 GMT+0800 (Singapore Standard Time)

<p>
**[Updated]** Still need bi-direction graph and use visited set. Example test case for that: (Thanks @hiepit and @notebook, my bad)
*4
[[0,2],[0,3],[1,2]]
[false,true,false,false]
Expected Answer: 4*

Key Notes: 
* You need to consume 2 seconds to simply collect an apple node (come and go)
* Consider a node:
	* If none of descendant (including itself) has an apple, we don\'t need to waste time on this node
	* If any of descendant has an apple (no matter if it-self has an apple or not), we need to consume 2 seconds on this node anyway
* Collect node 0 does not need to consume any time

Then, we can have a helper dfs function meaning: time needs to waste on this node to collect all apples. (0 or > 0).

```java
public int minTime(int n, int[][] edges, List<Boolean> hasApple) {
        
	Map<Integer, List<Integer>> map = new HashMap<>();
	buildTree(edges, map);
	Set<Integer> visited = new HashSet<>();
	return helper(0, map, hasApple, visited);
}

private int helper(int node, Map<Integer, List<Integer>> map, List<Boolean> hasApple, Set<Integer> visited) {

	visited.add(node);

	int res = 0;

	for (int child : map.getOrDefault(node, new LinkedList<>())) {
		if (visited.contains(child)) continue;
		res += helper(child, map, hasApple, visited);
	}

	if ((res > 0 || hasApple.get(node)) && node != 0) res += 2;

	return res;
}

private void buildTree(int[][] edges, Map<Integer, List<Integer>> map) {

	for (int[] edge : edges) {
		int a = edge[0], b = edge[1];
		map.putIfAbsent(a, new LinkedList<>());
		map.putIfAbsent(b, new LinkedList<>());
		map.get(a).add(b);
		map.get(b).add(a);
	}

}
```
</p>


### [C++] Short and simple DFS
- Author: PhoenixDD
- Creation Date: Sun May 10 2020 12:01:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 11 2020 02:28:02 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
We know that graph is in a form of tree i.e no cycles and that the last node is supposed to be the root it self, this gives us an idea that we can use DFS to solve this.
At each node we can check if its children has any apples if it does we add the distance from current node to that child node and we return this distance else we return `0`.
The other key observation is that each time we collect the apple we have to walk back to the parent, thus the distance travelled is doubled.

**Solution**
We can return the distance as 0 if no apples found under the node and the current node also doesn\'t have an apple else we return the distance travelled.

```c++
class Solution {
public:
    vector<vector<int>> adjList;
    int dfs(vector<bool>& hasApple,int node,int d,int prev)
    {
        int result=0,temp;
        for(int &i:adjList[node])
	    if(i!=prev)
	    {
	        temp=dfs(hasApple,i,d+1,node);
	        if(temp)			//If child has apples it\'ll return a non zero result which is the distance traveled upto that node.
		    result+=temp-d;
	    }
        return result||hasApple[node]?result+d:0;  //If nothing is added to result and current node doesnt have apple return 0 else return distances of children + current distance from root.
        
    }
    int minTime(int n, vector<vector<int>>& edges, vector<bool>& hasApple) 
    {
        adjList.resize(n);
        for(vector<int> &e:edges)
            adjList[e[0]].push_back(e[1]),adjList[e[1]].push_back(e[0]);
        return dfs(hasApple,0,0,-1)*2;     //Result is doubled the distance travelled as per our observation.
    }
};
```
</p>


