---
title: "Number of Trusted Contacts of a Customer"
weight: 1568
#id: "number-of-trusted-contacts-of-a-customer"
---
## Description
<div class="description">
<p>Table: <code>Customers</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| customer_id   | int     |
| customer_name | varchar |
| email         | varchar |
+---------------+---------+
customer_id is the primary key for this table.
Each row of this table contains the name and the email of a customer of an online shop.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Contacts</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| user_id       | id      |
| contact_name  | varchar |
| contact_email | varchar |
+---------------+---------+
(user_id, contact_email) is the primary key for this table.
Each row of this table contains the name and email of one contact of customer with user_id.
This table contains information about people each customer trust. The contact may or may not exist in the Customers table.

</pre>

<p>&nbsp;</p>

<p>Table: <code>Invoices</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| invoice_id   | int     |
| price        | int     |
| user_id      | int     |
+--------------+---------+
invoice_id is the primary key for this table.
Each row of this table indicates that user_id has an invoice with invoice_id and a price.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the following for each <code>invoice_id</code>:</p>

<ul>
	<li><code>customer_name</code>: The name of the customer the invoice is related to.</li>
	<li><code>price</code>: The price of the invoice.</li>
	<li><code>contacts_cnt</code>: The number of contacts related to the customer.</li>
	<li><code>trusted_contacts_cnt</code>: The number of contacts related to the customer and at the same time they are customers to the shop. (i.e His/Her email exists in the Customers table.)</li>
</ul>

<p>Order the result table by <code><font face="monospace">invoice_id</font></code>.</p>

<p>The query result format is in the following example:</p>

<pre>
<code>Customers</code> table:
+-------------+---------------+--------------------+
| customer_id | customer_name | email              |
+-------------+---------------+--------------------+
| 1           | Alice         | alice@leetcode.com |
| 2           | Bob           | bob@leetcode.com   |
| 13          | John          | john@leetcode.com  |
| 6           | Alex          | alex@leetcode.com  |
+-------------+---------------+--------------------+
Contacts table:
+-------------+--------------+--------------------+
| user_id     | contact_name | contact_email      |
+-------------+--------------+--------------------+
| 1           | Bob          | bob@leetcode.com   |
| 1           | John         | john@leetcode.com  |
| 1           | Jal          | jal@leetcode.com   |
| 2           | Omar         | omar@leetcode.com  |
| 2           | Meir         | meir@leetcode.com  |
| 6           | Alice        | alice@leetcode.com |
+-------------+--------------+--------------------+
Invoices table:
+------------+-------+---------+
| invoice_id | price | user_id |
+------------+-------+---------+
| 77         | 100   | 1       |
| 88         | 200   | 1       |
| 99         | 300   | 2       |
| 66         | 400   | 2       |
| 55         | 500   | 13      |
| 44         | 60    | 6       |
+------------+-------+---------+
Result table:
+------------+---------------+-------+--------------+----------------------+
| invoice_id | customer_name | price | contacts_cnt | trusted_contacts_cnt |
+------------+---------------+-------+--------------+----------------------+
| 44         | Alex          | 60    | 1            | 1                    |
| 55         | John          | 500   | 0            | 0                    |
| 66         | Bob           | 400   | 2            | 0                    |
| 77         | Alice         | 100   | 3            | 2                    |
| 88         | Alice         | 200   | 3            | 2                    |
| 99         | Bob           | 300   | 2            | 0                    |
+------------+---------------+-------+--------------+----------------------+
Alice has three contacts, two of them are trusted contacts (Bob and John).
Bob has two contacts, none of them is a trusted contact.
Alex has one contact and it is a trusted contact (Alice).
John doesn&#39;t have any contacts.
</pre>

</div>

## Tags


## Companies
- Roblox - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy MS SQL
- Author: amuw
- Creation Date: Sat Apr 04 2020 06:41:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Apr 04 2020 06:41:18 GMT+0800 (Singapore Standard Time)

<p>
```
select
    i.invoice_id,
    c.customer_name,
    i.price,
    count(con.user_id) as contacts_cnt,
    count(c2.email) as trusted_contacts_cnt
from invoices i
join customers c on c.customer_id = i.user_id
left join contacts con on con.user_id = c.customer_id
left join customers c2 on c2.email = con.contact_email
group by i.invoice_id, c.customer_name, i.price
order by i.invoice_id
```
</p>


### mysql simple solution
- Author: shelley616
- Creation Date: Fri Feb 28 2020 01:05:27 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 03 2020 23:10:41 GMT+0800 (Singapore Standard Time)

<p>
select i.invoice_id, 
			c.customer_name,
			i.price ,
			count(cont.contact_email) contacts_cnt , 
			sum(if(cont.contact_name in (select distinct email from customers),1,0)) trusted_contacts_cnt     
from invoices i
join customers c on c.customer_id  = i.user_id 
left join  Contacts cont on cont.user_id = c.customer_id 
group by i.invoice_id 
order by i.invoice_id
</p>


### MySQL solution
- Author: 13024798256
- Creation Date: Wed Aug 19 2020 01:27:02 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 19 2020 01:27:02 GMT+0800 (Singapore Standard Time)

<p>
```
select i.invoice_id,
       c.customer_name,
       i.price,
       count(distinct t.contact_name) as contacts_cnt,
       sum(case when contact_name in (select distinct customer_name from Customers) then 1 else 0 end) as trusted_contacts_cnt
from Invoices as i
join Customers as c on c.customer_id = i.user_id
left join Contacts as t on c.customer_id = t.user_id
group by i.invoice_id
```
</p>


