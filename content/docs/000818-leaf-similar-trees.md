---
title: "Leaf-Similar Trees"
weight: 818
#id: "leaf-similar-trees"
---
## Description
<div class="description">
<p>Consider all the leaves of a binary tree, from&nbsp;left to right order, the values of those&nbsp;leaves form a <strong>leaf value sequence</strong><em>.</em></p>

<p><img alt="" src="https://s3-lc-upload.s3.amazonaws.com/uploads/2018/07/16/tree.png" style="width: 400px; height: 336px;" /></p>

<p>For example, in the given tree above, the leaf value sequence is <code>(6, 7, 4, 9, 8)</code>.</p>

<p>Two binary trees are considered <em>leaf-similar</em>&nbsp;if their leaf value sequence is the same.</p>

<p>Return <code>true</code> if and only if the two given trees with head nodes <code>root1</code> and <code>root2</code> are leaf-similar.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/03/leaf-similar-1.jpg" style="width: 750px; height: 297px;" />
<pre>
<strong>Input:</strong> root1 = [3,5,1,6,2,9,8,null,null,7,4], root2 = [3,5,1,6,7,4,2,null,null,null,null,null,null,9,8]
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root1 = [1], root2 = [1]
<strong>Output:</strong> true
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root1 = [1], root2 = [2]
<strong>Output:</strong> false
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> root1 = [1,2], root2 = [2,2]
<strong>Output:</strong> true
</pre>

<p><strong>Example 5:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/03/leaf-similar-2.jpg" style="width: 450px; height: 165px;" />
<pre>
<strong>Input:</strong> root1 = [1,2,3], root2 = [1,3,2]
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in each tree will be in the range <code>[1, 200]</code>.</li>
	<li>Both of the given trees will have values in the range <code>[0, 200]</code>.</li>
</ul>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Microsoft - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Depth First Search

**Intuition and Algorithm**

Let's find the leaf value sequence for both given trees.  Afterwards, we can compare them to see if they are equal or not.

To find the leaf value sequence of a tree, we use a depth first search.  Our `dfs` function writes the node's value if it is a leaf, and then recursively explores each child.  This is guaranteed to visit each leaf in left-to-right order, as left-children are fully explored before right-children.

<iframe src="https://leetcode.com/playground/Nr9hkmXx/shared" frameBorder="0" width="100%" height="378" name="Nr9hkmXx"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(T_1 + T_2)$$, where $$T_1, T_2$$ are the lengths of the given trees.

* Space Complexity:  $$O(T_1 + T_2)$$, the space used in storing the leaf values.
<br />
<br />

## Accepted Submission (python3)
```python3
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def getLeaves(self, root: TreeNode, li: []):
        if not root:
            return li
        if not root.left and not root.right:
            li.append(root)
        if root.left:
            self.getLeaves(root.left, li)
        if root.right:
            self.getLeaves(root.right, li)
        return li
    def leafSimilar(self, root1: TreeNode, root2: TreeNode) -> bool:
        leaves1 = self.getLeaves(root1, [])
        leaves2 = self.getLeaves(root2, [])
        #print(leaves1)
        #print(leaves2)
        l1 = len(leaves1)
        l2 = len(leaves2)
        if l1 != l2:
            return False
        for i in range(l1):
            if leaves1[i].val != leaves2[i].val:
                return False
        return True
```

## Top Discussions
### [C++/Java/Python] O(H) Space
- Author: lee215
- Creation Date: Sun Jul 22 2018 11:07:00 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 08 2019 15:28:20 GMT+0800 (Singapore Standard Time)

<p>
**General methode is that traverse DFS whole tree to a list and compare two lists.**

Here I share an idea of comparing node by node using `O(H)` space,
where `H` is the height of the tree.

Use a `stack<TreeNode>` to keep dfs path.
`dfs(stack)` will return next leaf.
Check leaves one by one, until the end or difference.
Only `O(H)` space for stack, no extra space for comparation.
`O(1)` is also possible if we can modify the tree.

**C++:**
```
    bool leafSimilar(TreeNode* root1, TreeNode* root2) {
        stack<TreeNode*> s1 , s2;
        s1.push(root1); s2.push(root2);
        while (!s1.empty() && !s2.empty())
            if (dfs(s1) != dfs(s2)) return false;
        return s1.empty() && s2.empty();
    }

    int dfs(stack<TreeNode*>& s) {
        while (true) {
            TreeNode* node = s.top(); s.pop();
            if (node->right) s.push(node->right);
            if (node->left) s.push(node->left);
            if (!node->left && !node->right) return node->val;
        }
    }
```

**Java:**
```
    public boolean leafSimilar(TreeNode root1, TreeNode root2) {
        Stack<TreeNode> s1 = new Stack<>(), s2 = new Stack<>();
        s1.push(root1); s2.push(root2);
        while (!s1.empty() && !s2.empty())
            if (dfs(s1) != dfs(s2)) return false;
        return s1.empty() && s2.empty();
    }

    public int dfs(Stack<TreeNode> s) {
        while (true) {
            TreeNode node = s.pop();
            if (node.right != null) s.push(node.right);
            if (node.left != null) s.push(node.left);
            if (node.left == null && node.right == null) return node.val;
        }
    }
```

**Python:**
```
    def leafSimilar(self, root1, root2):
        def dfs(node):
            if not node: return
            if not node.left and not node.right: yield node.val
            for i in dfs(node.left): yield i
            for i in dfs(node.right): yield i
        return all(a == b for a, b in itertools.izip_longest(dfs(root1), dfs(root2)))
```
</p>


### 4 line Python Solution
- Author: HeroKillerEver
- Creation Date: Mon Jul 23 2018 19:14:56 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 20:54:26 GMT+0800 (Singapore Standard Time)

<p>
This problem is very similar to the tree traversal problems, actually to me, they are the same, we just return the leaf node if `node.left == None and node.right == None`. Otherwise, we recursively find the left leaves and right leaves.
```
class Solution(object):
    def leafSimilar(self, root1, root2):
        """
        :type root1: TreeNode
        :type root2: TreeNode
        :rtype: bool
        """
        return self.findleaf(root1) == self.findleaf(root2)
        
    def findleaf(self, root):
        if not root: return []
        if not (root.left or root.right): return [root.val]
        return self.findleaf(root.left) + self.findleaf(root.right)
```
</p>


### C++ super easy 0ms short solution, beats 100%!
- Author: MichaelZ
- Creation Date: Tue Jul 24 2018 12:25:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 09:29:43 GMT+0800 (Singapore Standard Time)

<p>
```
    bool leafSimilar(TreeNode* root1, TreeNode* root2) {
        string t1, t2;
        DFS(root1, t1);
        DFS(root2, t2);
        return t1==t2;
    }
    
    void DFS(TreeNode* root, string& s) {
        if(root==NULL) return;
        if(root->left==NULL&&root->right==NULL) s+=to_string(root->val)+"#";
        DFS(root->left, s);
        DFS(root->right, s);
    }
</p>


