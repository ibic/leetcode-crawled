---
title: "Three Equal Parts"
weight: 877
#id: "three-equal-parts"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of <code>0</code>s and <code>1</code>s, divide the array into 3 non-empty parts such that all of these parts represent the same binary value.</p>

<p>If it is possible, return <strong>any</strong> <code>[i, j]</code>&nbsp;with <code>i+1 &lt; j</code>, such that:</p>

<ul>
	<li><code>A[0], A[1], ..., A[i]</code> is the first part;</li>
	<li><code>A[i+1], A[i+2], ..., A[j-1]</code> is the second part, and</li>
	<li><code>A[j], A[j+1], ..., A[A.length - 1]</code> is the third part.</li>
	<li>All three parts have equal binary value.</li>
</ul>

<p>If it is not possible, return <code>[-1, -1]</code>.</p>

<p>Note that the entire part is used when considering what binary value it represents.&nbsp; For example, <code>[1,1,0]</code>&nbsp;represents <code>6</code>&nbsp;in decimal,&nbsp;not <code>3</code>.&nbsp; Also, leading zeros are allowed, so&nbsp;<code>[0,1,1]</code> and <code>[1,1]</code> represent the same value.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,0,1,0,1]</span>
<strong>Output: </strong><span id="example-output-1">[0,3]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[1,1,0,1,1]</span>
<strong>Output: </strong><span id="example-output-2">[-1,-1]</span></pre>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>3 &lt;= A.length &lt;= 30000</code></li>
	<li><code>A[i] == 0</code>&nbsp;or <code>A[i] == 1</code></li>
</ol>

<div>
<div>&nbsp;</div>
</div>
</div>

## Tags
- Math (math)
- Binary Search (binary-search)
- Greedy (greedy)

## Companies
- Netflix - 2 (taggedByAdmin: false)
- Hotstar - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Equal Ones

**Intuition**

Each part has to have the same number of ones in their representation.  The algorithm given below is the natural continuation of this idea.

**Algorithm**

Say `S` is the number of ones in `A`.  Since every part has the same number of ones, they all should have `T = S / 3` ones.

If `S` isn't divisible by 3, the task is impossible.

We can find the position of the 1st, T-th, T+1-th, 2T-th, 2T+1-th, and 3T-th one.  The positions of these ones form 3 intervals: `[i1, j1], [i2, j2], [i3, j3]`.  (If there are only 3 ones, then the intervals are each length 1.)

Between them, there may be some number of zeros.  The zeros after `j3` must be included in each part: say there are `z` of them `(z = S.length - j3)`.

So the first part, `[i1, j1]`, is now `[i1, j1+z]`.  Similarly, the second part, `[i2, j2]`, is now `[i2, j2+z]`.

If all this is actually possible, then the final answer is `[j1+z, j2+z+1]`.

<iframe src="https://leetcode.com/playground/pnkgBkRn/shared" frameBorder="0" width="100%" height="500" name="pnkgBkRn"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `S`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] O(n) time, O(1) space, 12 ms with explanation & comments
- Author: primeNumber
- Creation Date: Sun Oct 21 2018 11:41:57 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 16:53:32 GMT+0800 (Singapore Standard Time)

<p>
Algorithm:
	1) Count no. of 1\'s in the given array, say ```countNumberOfOnes```.
	2) If no 1 is found ie. ```countNumberOfOnes == 0```, just return ```{0,size-1}```
	3) If ``` countNumberOfOnes % 3 != 0``` , then we cannot partition the given array for sure. This is because, there is no way to put equal no. of 1\'s in any partition and hence, we will get different binary representations.
	4) Let\'s try to find if there is a valid partition possible now. We find the first 1 in the given array and represent it\'s position by ```start```.
	5) Also, we know that each partition must have ``` countNumberOfOnes/3 ``` (for same reason as given in step 3). Therefore, after finding the first 1, leave  ```k = countNumberOfOnes/3 ``` 1\'s for the first partition.
	6) Assign this position as ```mid``` that denotes the beginning of a possible second partition.
	7) Further leave ```k = countNumberOfOnes/3 ``` 1\'s for this partition and assign the beginning of last partition as ```end```
	8) Now, all we need to do is verify whether all the partitions have same values in them. This can be done by iterating through to the end of the array.
	9) If ```end``` doesn\'t reach the end of the array, we find a mismatch and hence, we need to return ```{-1, -1}```
	10) Otherwise, we have found our partition, return ```{start-1,mid}```

Time Complexity: ```O(n)```
Space Complexity: ```O(1)```

```
static int x=[](){ios::sync_with_stdio(false); cin.tie(NULL); return 0;}();

class Solution {
public:
    vector<int> threeEqualParts(vector<int>& A) {
	    // Count no of 1\'s in the given array
        int countNumberOfOnes = 0;
        for(int c: A)
            if(c == 1)                  
                countNumberOfOnes++;
								
	    // If no 1 is found, that means we can return ans as {0, size-1}
        if(countNumberOfOnes == 0)      
            return {0, A.size()-1};
						
        // If no of 1\'s is not a multiple of 3, then we can never find a possible partition since
        // there will be atkeast one partition that will have different no of 1\'s and hence
        // different binary representation
        // For example,
        // Given :
        // 0000110  110  110 
        //       |  |    |
        //       i       j
        // Total no of ones = 6
        // 0000110         110      110
        //     |           |        |
        //     start       mid      end
        // Match starting from first 1, and putting 2 more pointers by skipping k 1\'s
        
        if(countNumberOfOnes % 3 != 0)            
            return {-1, -1};
						
        // find size of each partition
        int k = countNumberOfOnes/3;
        int i;
        
        // find the first 1 in the array
        for(i=0;i<A.size(); i++)
            if(A[i] == 1)
                break;
        int start = i;
        
        // find (k+1)th 1 in the array
        int count1 = 0;
        for(i=0;i<A.size(); i++)
        {
            if(A[i] == 1)
                count1++;
            if(count1 == k + 1)
                break;
        }
        int mid = i;
        
        //find (2*k +1)th 1 in the array
        count1= 0;
        for(i=0;i<A.size(); i++)
        {
            if(A[i] == 1)
                count1++;
            if(count1 == 2*k + 1)
                break;
        }
        int end = i;
        
        // Match all values till the end of the array
        while(end< A.size() && A[start] == A[mid] && A[mid] == A[end])
        {
            start++; mid++; end++;
        }
        
        // Return appropriate values if all the values have matched till the end
        if(end == A.size()) 
            return {start-1, mid};
        
        // otherwise, no such indices found
        return {-1, -1};
    }
};

```
Code written during contest, may be optimized further. :-)
</p>


### Java O(n) simple solution (don't know why official solution is that long)
- Author: vincexu
- Creation Date: Wed Jan 23 2019 03:43:37 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jan 23 2019 03:43:37 GMT+0800 (Singapore Standard Time)

<p>
Key obseration is that three parts much have same number and pattern of 1s except the leading part.  My idea is to:

1. count how many ones  (if num%3!=0 return [-1,-1])
2. search from right side to left, until we found num/3 1s.  This index is not final answer, but it defines patten of 1s
3. from feft, ignore leading 0s, and then match the pattern found in step 2, to get the first EndIndex
4. do another matching to found second EndIndex


```
public int[] threeEqualParts(int[] A) {
		int numOne = 0;
		for (int i: A){
			if (i==1) numOne++;
		}
        
        int[] noRes = {-1, -1};
        if (numOne == 0) return new int[]{0,2};
        if (numOne%3 != 0) return noRes;
        
        //find index of starting 1 of third string
        int idxThird=0;
		int temp = 0;
        for (int i = A.length-1; i>=0; i--){
            if (A[i]==1){
                temp++;
                if (temp == numOne / 3){
                    idxThird = i;
                    break;
                }
            } 
        }
        
        int res1 = findEndIdx(A, 0, idxThird);
        if (res1<0) return noRes;
        
        int res2 = findEndIdx(A, res1+1, idxThird);
        if (res2<0) return noRes;
        
        return new int[]{res1, res2+1};
    }
    
	//right is the pattern to compare to.  
	//return EndIdx of left pattern that matches right side.
    private int findEndIdx(int[] A, int left, int right){
        while (A[left]==0) left++;
        while (right < A.length){
            if (A[left]!=A[right]) return -1;
            left++;
            right++;            
        }
        return left-1;
    }
```
</p>


### [C++] O(N) time O(N) space, 40ms, 14 lines, 2 loops, easy understand with explanation
- Author: fangzzh
- Creation Date: Sun Oct 21 2018 13:21:21 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 10:41:17 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    vector<int> threeEqualParts(vector<int>& A) {
        vector<int> dp;
        for(int i = 0 ; i < A.size(); i++) // this loop is used to store the index of all 1s
            if(A[i]) dp.push_back(i);
        if(dp.size() % 3) return {-1, -1}; // if the number of 1s cannot be devided perfectly by 3, the input is invalid
	if(dp.empty()) return {0,2}; // if the number of 1 is zero, then it is natually valid, return {0, 2}
        int l1 = 0, l2 = dp.size() / 3, l3 = l2 * 2; //if we want to devide into 3 parts, the distribution pattern of 1s in three parts should be the same
        for(int i = 1; i < l2; i++ ) {
            int diff = dp[i] - dp[i-1];
            if(dp[l2+i] - dp[l2+i-1] != diff || dp[l3+i] - dp[l3+i-1] != diff) //unmatched pattern
                return {-1, -1};
	}
        int tail0 = A.size() - dp.back(); // calculate how many 0s tail
        if(dp[l3] - dp[l3-1] < tail0 ||   dp[l2] - dp[l2-1] < tail0) return {-1,-1};// all three parts should tail with the same number of 0s with that in the last part
        return {dp[l2-1] + tail0 - 1, dp[l3-1] + tail0};
    }
};
```
</p>


