---
title: "X of a Kind in a Deck of Cards"
weight: 864
#id: "x-of-a-kind-in-a-deck-of-cards"
---
## Description
<div class="description">
<p>In a deck of cards, each card has an integer written on it.</p>

<p>Return <code>true</code> if and only if you can choose&nbsp;<code>X &gt;= 2</code> such that&nbsp;it is possible to split the entire deck&nbsp;into 1 or more groups of cards, where:</p>

<ul>
	<li>Each group has exactly <code>X</code> cards.</li>
	<li>All the cards in each group have the same integer.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> deck = [1,2,3,4,4,3,2,1]
<strong>Output:</strong> true
<strong>Explanation</strong>: Possible partition [1,1],[2,2],[3,3],[4,4].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> deck = [1,1,1,2,2,2,3,3]
<strong>Output:</strong> false&acute;
<strong>Explanation</strong>: No possible partition.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> deck = [1]
<strong>Output:</strong> false
<strong>Explanation</strong>: No possible partition.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> deck = [1,1]
<strong>Output:</strong> true
<strong>Explanation</strong>: Possible partition [1,1].
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> deck = [1,1,2,2,2,2]
<strong>Output:</strong> true
<strong>Explanation</strong>: Possible partition [1,1],[2,2],[2,2].
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= deck.length &lt;= 10^4</code></li>
	<li><code>0 &lt;= deck[i] &lt;&nbsp;10^4</code></li>
</ul>

</div>

## Tags
- Array (array)
- Math (math)

## Companies
- Google - 5 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Brute Force

**Intuition**

We can try every possible `X`.  

**Algorithm**

Since we divide the deck of `N` cards into say, `K` piles of `X` cards each, we must have `N % X == 0`.

Then, say the deck has `C_i` copies of cards with number `i`.  Each group with number `i` has `X` copies, so we must have `C_i % X == 0`.  These are necessary and sufficient conditions.

<iframe src="https://leetcode.com/playground/o2sLAyHx/shared" frameBorder="0" width="100%" height="446" name="o2sLAyHx"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2 \log \log N)$$, where $$N$$ is the number of cards.  It is outside the scope of this article to prove that the number of divisors of $$N$$ is bounded by $$O(N \log \log N)$$.

* Space Complexity:  $$O(N)$$.
<br />
<br />


---
#### Approach 2: Greatest Common Divisor

**Intuition and Algorithm**

Again, say there are `C_i` cards of number `i`.  These must be broken down into piles of `X` cards each, ie. `C_i % X == 0` for all `i`.

Thus, `X` must divide the greatest common divisor of `C_i`.  If this greatest common divisor `g` is greater than `1`, then `X = g` will satisfy.  Otherwise, it won't.

<iframe src="https://leetcode.com/playground/GEJPyUs9/shared" frameBorder="0" width="100%" height="429" name="GEJPyUs9"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log^2 N)$$, where $$N$$ is the number of votes.  If there are $$C_i$$ cards with number $$i$$, then each `gcd` operation is naively $$O(\log^2 C_i)$$.  Better bounds exist, but are outside the scope of this article to develop.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Greatest Common Divisor
- Author: lee215
- Creation Date: Sun Sep 30 2018 11:02:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 17:40:37 GMT+0800 (Singapore Standard Time)

<p>
Counts all occurrence of all numbers.
Return if the greatest common divisor of counts > 1.

Time Complexity O(N).

**C++:**
```
    bool hasGroupsSizeX(vector<int>& deck) {
        unordered_map<int, int> count;
        int res = 0;
        for (int i : deck) count[i]++;
        for (auto i : count) res = __gcd(i.second, res);
        return res > 1;
    }
```

**Java:**
```
    public boolean hasGroupsSizeX(int[] deck) {
        Map<Integer, Integer> count = new HashMap<>();
        int res = 0;
        for (int i : deck) count.put(i, count.getOrDefault(i, 0) + 1);
        for (int i : count.values()) res = gcd(i, res);
        return res > 1;
    }

    public int gcd(int a, int b) {
        return b > 0 ? gcd(b, a % b) : a;
    }
```
**Python:**
```
    def hasGroupsSizeX(self, deck):
        def gcd(a, b):
            while b: a, b = b, a % b
            return a
        count = collections.Counter(deck).values()
        return reduce(gcd, count) > 1
```
**Python 1-line**
Idea from @gsk694
```
    def hasGroupsSizeX(self, deck):
        return reduce(fractions.gcd, collections.Counter(deck).values()) > 1
```
</p>


### Python Faster Than 97%. Without in-build GCD function
- Author: jobseekers
- Creation Date: Sat May 04 2019 03:42:11 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 18 2019 22:20:07 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def hasGroupsSizeX(self, deck: List[int]) -> bool:
        count = collections.Counter(deck)
        mini = min(count.values())
        
        if mini < 2:
            return False
        for i in range(mini+1,1,-1):
            res = all(value % i ==0 for value in count.values())
            if res: return True
        return False        
```
</p>


### [JavaScript - O(N)] Buckets and GCD w/Explanation
- Author: bundit
- Creation Date: Mon Mar 04 2019 04:34:33 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 04 2019 04:34:33 GMT+0800 (Singapore Standard Time)

<p>
```
In a deck of cards, each card has an integer written on it.

Return true if and only if you can choose X >= 2 such that it is possible to split the entire deck into 1 or more groups of cards, where:
* Each group has exactly X cards.
* All the cards in each group have the same integer.
```

1. First of all, we need to count how many of each card there is. We can do this with a map data structure. The benefit to using a map in this case is that it is O(1) lookup and O(1) insert.
2. After that we are left with the numerical count of each card. There\'s a few different ways we can solve this. One option is to use a greatest common diviser (gcd) function to find the gcd of all of the counts.
3. We use a variable to store the result of the gcd of the previous result and the next input. What happens is if there is any two counts where the gcd is 1, it will force the output to be 1. This is because the gcd of 1 and any positive number is 1. 
4. If the gcd of every count outputs 2, this tells us we can split our cards into groups of 2 to be a valid output. If the gcd of every count outputs 1, then we cannot make groups larger than size of 1 and the output is invalid. 

```
var hasGroupsSizeX = function(deck) {
    let buckets = new Map();
    
    deck.forEach ((card) => {
        let count = buckets.get(card);
        buckets.set(card, count ? count+1 : 1);
    });
    
    let res = 0;
    for (let bucket of buckets.values()) {
        res = gcd(res, bucket);
    }
    
    return res > 1;
};

function gcd (a, b) {
    return b > 0 ? gcd(b, a % b) : a;
}
```
</p>


