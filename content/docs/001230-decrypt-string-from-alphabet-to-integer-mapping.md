---
title: "Decrypt String from Alphabet to Integer Mapping"
weight: 1230
#id: "decrypt-string-from-alphabet-to-integer-mapping"
---
## Description
<div class="description">
<p>Given a string <code>s</code> formed by digits (<code>&#39;0&#39;</code> - <code>&#39;9&#39;</code>)&nbsp;and <code>&#39;#&#39;</code>&nbsp;.&nbsp;We want to map <code>s</code> to English lowercase characters as follows:</p>

<ul>
	<li>Characters (<code>&#39;a&#39;</code> to <code>&#39;i&#39;)</code> are&nbsp;represented by&nbsp;(<code>&#39;1&#39;</code> to&nbsp;<code>&#39;9&#39;</code>)&nbsp;respectively.</li>
	<li>Characters (<code>&#39;j&#39;</code> to <code>&#39;z&#39;)</code> are represented by (<code>&#39;10#&#39;</code>&nbsp;to&nbsp;<code>&#39;26#&#39;</code>)&nbsp;respectively.&nbsp;</li>
</ul>

<p>Return the string formed after mapping.</p>

<p>It&#39;s guaranteed that a unique mapping will always exist.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;10#11#12&quot;
<strong>Output:</strong> &quot;jkab&quot;
<strong>Explanation:</strong> &quot;j&quot; -&gt; &quot;10#&quot; , &quot;k&quot; -&gt; &quot;11#&quot; , &quot;a&quot; -&gt; &quot;1&quot; , &quot;b&quot; -&gt; &quot;2&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;1326#&quot;
<strong>Output:</strong> &quot;acz&quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;25#&quot;
<strong>Output:</strong> &quot;y&quot;
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;12345678910#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#&quot;
<strong>Output:</strong> &quot;abcdefghijklmnopqrstuvwxyz&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 1000</code></li>
	<li><code>s[i]</code> only contains digits letters (<code>&#39;0&#39;</code>-<code>&#39;9&#39;</code>) and <code>&#39;#&#39;</code>&nbsp;letter.</li>
	<li><code>s</code> will be valid string&nbsp;such that mapping is always possible.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Quip (Salesforce) - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] 1-line Regex
- Author: lee215
- Creation Date: Sun Jan 05 2020 12:41:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 05 2020 12:41:49 GMT+0800 (Singapore Standard Time)

<p>
**Python:**
```python
    def freqAlphabets(self, s):
        return \'\'.join(chr(int(i[:2]) + 96) for i in re.findall(r\'\d\d#|\d\', s))
```

</p>


### Check [i + 2]
- Author: votrubac
- Creation Date: Sun Jan 05 2020 12:01:09 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 05 2020 12:11:21 GMT+0800 (Singapore Standard Time)

<p>
We can simply check whehter \'#\' character appears at position `i + 2` to determine which decription rule to apply. 
```CPP
string freqAlphabets(string s) {
  string res;
  for (int i = 0; i < s.size(); ++i) {
    if (i < s.size() - 2 && s[i + 2] == \'#\') {
      res += \'j\' + (s[i] - \'1\') * 10 + s[i + 1] - \'0\';
      i += 2;
    }
    else res += \'a\' + (s[i] - \'1\');
  }
  return res;
}
```
</p>


### Python 3 (two lines) (beats 100%) (16 ms) (With Explanation)
- Author: junaidmansuri
- Creation Date: Sun Jan 05 2020 12:05:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 05 2020 13:00:18 GMT+0800 (Singapore Standard Time)

<p>
- Start from letter z (two digit numbers) and work backwords to avoid any confusion or inteference with one digit numbers. After replacing all two digit (hashtag based) numbers, we know that the remaining numbers will be simple one digit replacements.
- Note that ord(\'a\') is 97 which means that chr(97) is \'a\'. This allows us to easily create a character based on its number in the alphabet. For example, \'a\' is the first letter in the alphabet. It has an index of 1 and chr(96 + 1) is \'a\'.

```
class Solution:
    def freqAlphabets(self, s: str) -> str:
        for i in range(26,0,-1): s = s.replace(str(i)+\'#\'*(i>9),chr(96+i))
        return s
            
		
		
- Junaid Mansuri
- Chicago, IL
</p>


