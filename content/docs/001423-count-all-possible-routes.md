---
title: "Count All Possible Routes"
weight: 1423
#id: "count-all-possible-routes"
---
## Description
<div class="description">
<p>You are given an array of <strong>distinct</strong> positive integers locations&nbsp;where <code>locations[i]</code> represents the position of city <code>i</code>. You are also given&nbsp;integers&nbsp;<code>start</code>,&nbsp;<code>finish</code>&nbsp;and&nbsp;<code>fuel</code>&nbsp;representing the starting city, ending city, and the initial amount of fuel you have, respectively.</p>

<p>At each step, if you are at city&nbsp;<code>i</code>, you can pick any city&nbsp;<code>j</code>&nbsp;such that <code>j != i</code>&nbsp;and&nbsp;<code>0 &lt;= j &lt; locations.length</code>&nbsp;and move to city <code>j</code>.&nbsp;Moving from city <code>i</code> to city <code>j</code> reduces the amount of fuel you have by&nbsp;<code>|locations[i] - locations[j]|</code>.&nbsp;Please notice that <code>|x|</code>&nbsp;denotes the absolute value of <code>x</code>.</p>

<p>Notice that&nbsp;<code>fuel</code>&nbsp;<strong>cannot</strong> become negative at any point in time, and that you are <strong>allowed</strong> to visit any city more than once (including <code>start</code>&nbsp;and&nbsp;<code>finish</code>).</p>

<p>Return <em>the count of all possible routes from&nbsp;</em><code>start</code>&nbsp;<em>to</em>&nbsp;<code>finish</code>.</p>

<p>Since the answer&nbsp;may be too large,&nbsp;return it modulo&nbsp;<code>10^9 + 7</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> locations = [2,3,6,8,4], start = 1, finish = 3, fuel = 5
<strong>Output:</strong> 4
<strong>Explanation:</strong>&nbsp;The following are all possible routes, each uses 5 units of fuel:
1 -&gt; 3
1 -&gt; 2 -&gt; 3
1 -&gt; 4 -&gt; 3
1 -&gt; 4 -&gt; 2 -&gt; 3
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> locations = [4,3,1], start = 1, finish = 0, fuel = 6
<strong>Output:</strong> 5
<strong>Explanation: </strong>The following are all possible routes:
1 -&gt; 0, used fuel = 1
1 -&gt; 2 -&gt; 0, used fuel = 5
1 -&gt; 2 -&gt; 1 -&gt; 0, used fuel = 5
1 -&gt; 0 -&gt; 1 -&gt; 0, used fuel = 3
1 -&gt; 0 -&gt; 1 -&gt; 0 -&gt; 1 -&gt; 0, used fuel = 5
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> locations = [5,2,1], start = 0, finish = 2, fuel = 3
<strong>Output:</strong> 0
<b>Explanation: </b>It&#39;s impossible to get from 0 to 2 using only 3 units of fuel since the shortest route needs 4 units of fuel.</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> locations = [2,1,5], start = 0, finish = 0, fuel = 3
<strong>Output:</strong> 2
<strong>Explanation:</strong>&nbsp;There are two possible routes, 0 and 0 -&gt; 1 -&gt; 0.</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> locations = [1,2,3], start = 0, finish = 2, fuel = 40
<strong>Output:</strong> 615088286
<strong>Explanation: </strong>The total number of possible routes is 2615088300. Taking this number modulo 10^9 + 7 gives us 615088286.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= locations.length &lt;= 100</code></li>
	<li><code>1 &lt;= locations[i] &lt;= 10^9</code></li>
	<li>All integers in&nbsp;<code>locations</code>&nbsp;are&nbsp;<strong>distinct</strong>.</li>
	<li><code>0 &lt;= start, finish &lt;&nbsp;locations.length</code></li>
	<li><code><font face="monospace">1 &lt;= fuel &lt;= 200</font></code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- tsys - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Dynamic Programming | Simple Explanation
- Author: khaufnak
- Creation Date: Sun Sep 06 2020 00:00:27 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 09 2020 17:22:11 GMT+0800 (Singapore Standard Time)

<p>
1. Start from the `start` index, and visit all cities except `start`.
2. Continue this process recursively.
3. If you reach `end` index, do:
    - Now, we have atleast 1 way of reaching `end` so add this 1 possible way to the answer.
    - Continue recursion, since there might be more ways to get back from `end` to `end` using other cities.
4. If fuel < 0, there is no further way left.

If you look at this recursion, we can see that there are overlapping subproblems. If you reach a city `c` with fuel `f` multiple times from different paths, don\'t recompute, just use memoised result. 

```
class Solution {
    public int countRoutes(int[] locations, int start, int finish, int fuel) {
        int n = locations.length;
        long[][] dp = new long[n][fuel + 1];
        for (int i = 0; i < n; ++i) {
            Arrays.fill(dp[i], -1);
        }
        return (int) solve(locations, start, finish, dp, fuel);
    }
	// dp[curCity][fuel] = number of ways to reach finish, when we are at city `curCity` with fuel `fuel`
    private long solve(int[] locations, int curCity, int e, long[][] dp, int fuel) {
        // 4. There is no further way left.
        if (fuel < 0) return 0;
        if (dp[curCity][fuel] != -1) return dp[curCity][fuel];
        // 3. Now, if we have atleast 1 way of reaching `end`, add 1 to the answer. But don\'t stop right here, keep going, there might be more ways :)
        long ans = (curCity == e) ? 1 : 0;
        for (int nextCity = 0; nextCity < locations.length; ++nextCity) {
            // 1. Visit all cities except `curCity`.
            if (nextCity != curCity) {
                // 2. Continue this process recursively.
                ans = (ans + solve(locations, nextCity, e, dp, fuel - Math.abs(locations[curCity] - locations[nextCity]))) % 1000000007;
            }
        }
        return dp[curCity][fuel] = ans;
    }
}
```

Complexity: `O(fuel*n^2)`
</p>


### C++/Python #mimimalizm
- Author: votrubac
- Creation Date: Mon Sep 07 2020 07:27:08 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 08 2020 11:28:35 GMT+0800 (Singapore Standard Time)

<p>
**C++**
A usual #minimalizm trick is to add `1` to `dp[i][f]` to indicate an explored path. This helps saving lines :)
```cpp
int dp[101][201] = {};
int countRoutes(vector<int>& locs, int i, int finish, int f) {
    if (!dp[i][f]) {
        dp[i][f] = 1 + (i == finish);
        for (int j = 0; j < locs.size(); ++j)
            if (i != j && f >= abs(locs[i] - locs[j]))
                dp[i][f] = (dp[i][f] + countRoutes(locs, j, finish, f - abs(locs[i] - locs[j]))) % 1000000007;
    }
    return dp[i][f] - 1;
}
```

**Python**
Just a few lines in Python... that\'s an animal magnetism!
```python
def countRoutes(self, l: List[int], start: int, fin: int, fuel: int) -> int:
    @lru_cache(None)
    def dfs(i: int, f: int) -> int:
        return 0 if f < 0 else (1 if i == fin else 0) + sum(0 if i == j else dfs(j, f - abs(l[j] - l[i])) for j in range(len(l)))
    return dfs(start, fuel) % 1000000007
```
</p>


### [Java] Bottom Up
- Author: nihalanim9
- Creation Date: Sun Sep 06 2020 06:19:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 06 2020 06:19:52 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int countRoutes(int[] locations, int start, int finish, int fuel) {
        long[][] dp = new long[locations.length][fuel+1];
        long MOD = (long)(1e9 + 7);
        Arrays.fill(dp[finish],1);
        long ans = 0;
        for(int j = 0; j <= fuel; j++) {
            for(int i = 0; i < locations.length; i++) {
                for(int k = 0; k < locations.length; k++) {
                    if(k == i) continue;
                    if(Math.abs(locations[i] - locations[k]) <= j) {
                        dp[i][j] = (dp[i][j] + dp[k][j - Math.abs(locations[i] - locations[k])]) % MOD; 
                    }
                }
            }
        }
        return (int)dp[start][fuel];
    }
}
```
</p>


