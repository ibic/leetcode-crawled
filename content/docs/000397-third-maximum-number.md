---
title: "Third Maximum Number"
weight: 397
#id: "third-maximum-number"
---
## Description
<div class="description">
<p>Given a <b>non-empty</b> array of integers, return the <b>third</b> maximum number in this array. If it does not exist, return the maximum number. The time complexity must be in O(n).</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> [3, 2, 1]

<b>Output:</b> 1

<b>Explanation:</b> The third maximum is 1.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> [1, 2]

<b>Output:</b> 2

<b>Explanation:</b> The third maximum does not exist, so the maximum (2) is returned instead.
</pre>
</p>

<p><b>Example 3:</b><br />
<pre>
<b>Input:</b> [2, 2, 3, 1]

<b>Output:</b> 1

<b>Explanation:</b> Note that the third maximum here means the third maximum distinct number.
Both numbers with value 2 are both considered as second maximum.
</pre>
</p>
</div>

## Tags
- Array (array)

## Companies
- Facebook - 4 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: true)
- Google - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Use a Set and Delete Maximums

**Intuition**

Firstly, note that we can't simply sort the values and then select the third-to-end one, because the time complexity of sorting is $$O(n \, \log \, n)$$ (where $$n$$ is the length of the input `Array`). This problem clearly states our solution must have a time complexity of $$O(n)$$ though.

Also, note that there are 2 important pieces of information in the problem description that are easily overlooked:

1. If the third maximum doesn't exist, we must return the ***maximum*** (*never* the second maximum like one might assume!).
2. Duplicates should be ignored. We want the third maximum *distinct* value. i.e. for `[8, 8, 8, 3, 1]` the third maximum is `1`, despite the fact that `8` appears three times.

---

We'll work with the following example `Array`.

```
[12, 3, 8, 9, 12, 12, 7, 8, 12, 4, 3, 8, 1]
```

If there were no duplicates in the `Array`, then a logical strategy would be as follows:

```text
Find the maximum. Delete it.
Find the new maximum. Delete it.
Return the *new* maximum.
```

However, the input `Array` we're working with could have duplicates. To handle this, we can convert the input into a `Set` first to remove the duplicates.

Converting our input `Array` example into a `Set` gives us the following:

```
{12, 3, 8, 9, 7, 4, 1}
```

We then need to find the maximum in the `Set`. This can be done using a library function, or if necessary, your own function that loops through the list keeping track of the maximum seen so far, and then returns the maximum at the end.

The maximum from our example is `12`.

Now, we need to delete `12` from the `Set`. This leaves us with:

```
{3, 8, 9, 7, 4, 1}
```

We can then find and remove the second maximum, following the same process.

The second maximum is `9` (the maximum of what's left in the `Set`).

Removing it leaves us with the following:

```
{3, 8, 7, 4, 1}
```

Finally, we can return the maximum of what's left, which is `8`.

Remember that if the third maximum doesn't exist, then we need to return the *maximum* of the *original* `Array`. We can detect this situation as soon as we have converted the input `Array` into a `Set`, because it will contain less than `3` values.

**Algorithm**

<iframe src="https://leetcode.com/playground/BUTTFGvx/shared" frameBorder="0" width="100%" height="412" name="BUTTFGvx"></iframe>

**Complexity Analysis**

- Time Complexity : $$O(n)$$.

    Putting the input `Array` values into a `HashSet` has a cost of $$O(n)$$, as each value costs $$O(1)$$ to place, and there are $$n$$ of them.

    Finding the maximum in a `HashSet` has a cost of $$O(n)$$, as all the values need to be looped through. We do this $$3$$ times, giving $$O(3 \cdot n) = O(n)$$ as we drop constants in big-oh notation.

    Deleting a value from a `HashSet` has a cost of $$O(1)$$, so we can ignore this.

    In total, we're left with $$O(n) + O(n) = O(n)$$.

- Space Complexity : $$O(n)$$.

    In the worst case, the `HashSet` is the same size as the input `Array`, and so requires $$O(n)$$ space to store.

</br>

---


#### Approach 2: Seen-Maximums Set

**Intuition**

In the previous approach, we deleted the maximum and second maximum so that we could easily find the third maximum. We had to convert the input `Array` into a `Set` so that duplicates weren't super complicated to handle.

Instead of deleting items though, we could instead keep a `Set` of maximums we've already seen. Then when we are searching for a maximum, we can ignore any values that are already in the seen `Set`.

This will also handle duplicates elegantly—if for example we had the input set `[12, 12, 4, 2, 12, 1]`, then the first value we'd put into the seen maximums `Set` would be `12`. Then when we find the second maximum, the algorithm knows to ignore *all* the `12`s.

**Algorithm**

<iframe src="https://leetcode.com/playground/bJkLR4WY/shared" frameBorder="0" width="100%" height="500" name="bJkLR4WY"></iframe>


**Complexity Analysis**

- Time Complexity : $$O(n)$$.

    For each of the three times we find the next maximum, we need to perform an $$O(n)$$ scan. Because there are only, at most, three scans the total time complexity is just $$O(n)$$.

    The `Set` operations are all $$O(1)$$ because there are only at most `3` items in the `Set`.

- Space Complexity : $$O(1)$$.

    Because `seenMaximums` can contain at most `3` items, the space complexity is only $$O(1)$$.

</br>

---

#### Approach 3: Keep Track of 3 Maximums Using a Set

**Intuition**

So far, our approaches have required multiple parses through the input array. While this is still $$O(n)$$ in big-oh notation, it'd be good if we could solve it in a single parse. One way is to simply use a `Set` to keep track of the 3 maximum values we've seen so far. While you could achieve something similar using `3` variables (`maximum`, `secondMaximum`, and `thirdMaximum`), this is messy to work with and is poor programming practice.

For each number in the `Array`, we add it into the `Set` of maximums. If this causes there to be more than `3` numbers in the `Set`, then we evict the *smallest* number.

At the end, we check whether or not there are `3` numbers in the `Set`. If there are, this means the third maximum exists, and will be the *minimum* in the `Set`. If not, this means there was no third maximum, and so we should return the *maximum* of the `Set`, as per the problem requirements.

Here is an animation showing the approach.

!?!../Documents/414_approach_3.json:960,250!?!

**Algorithm**

<iframe src="https://leetcode.com/playground/TEyiDbzG/shared" frameBorder="0" width="100%" height="276" name="TEyiDbzG"></iframe>

**Complexity Analysis**

- Time Complexity : $$O(n)$$.

    For each of the $$n$$ values in the input `Array`, we insert it into a `Set` for a cost of $$O(1)$$. We then sometimes find and remove the minimum of the `Set`. Because there are never more than $$3$$ items in the `Set`, the time complexity of doing this is $$O(1)$$. 

    In total, we're left with $$O(n)$$.

- Space Complexity : $$O(1)$$.

    Because `maximums` never holds more than $$3$$ items at a time, it is considered to be constant $$O(1)$$.

</br>

---

#### Related Problem - Kth Largest Element in an Array

A related problem, which would possibly be used as a follow up question in an interview is [Kth Largest Element in an Array](https://leetcode.com/problems/kth-largest-element-in-an-array/).

</br>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java neat and easy understand solution, O(n) time, O(1) space
- Author: yuxiaofei2016
- Creation Date: Tue Oct 18 2016 22:01:03 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 09:36:10 GMT+0800 (Singapore Standard Time)

<p>
```
    public int thirdMax(int[] nums) {
        Integer max1 = null;
        Integer max2 = null;
        Integer max3 = null;
        for (Integer n : nums) {
            if (n.equals(max1) || n.equals(max2) || n.equals(max3)) continue;
            if (max1 == null || n > max1) {
                max3 = max2;
                max2 = max1;
                max1 = n;
            } else if (max2 == null || n > max2) {
                max3 = max2;
                max2 = n;
            } else if (max3 == null || n > max3) {
                max3 = n;
            }
        }
        return max3 == null ? max1 : max3;
    }
```
</p>


### Intuitive and Short Python solution
- Author: agave
- Creation Date: Sun Dec 04 2016 06:44:38 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 08:13:09 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def thirdMax(self, nums):
        v = [float('-inf'), float('-inf'), float('-inf')]
        for num in nums:
            if num not in v:
                if num > v[0]:   v = [num, v[0], v[1]]
                elif num > v[1]: v = [v[0], num, v[1]]
                elif num > v[2]: v = [v[0], v[1], num]
        return max(nums) if float('-inf') in v else v[2]
```
Time complexity is O(n), space complexity is O(1).
</p>


### Short easy C++ using set
- Author: StefanPochmann
- Creation Date: Thu Oct 20 2016 08:04:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 21:29:55 GMT+0800 (Singapore Standard Time)

<p>
Track the largest three values in a set.

    int thirdMax(vector<int>& nums) {
        set<int> top3;
        for (int num : nums) {
            top3.insert(num);
            if (top3.size() > 3)
                top3.erase(top3.begin());
        }
        return top3.size() == 3 ? *top3.begin() : *top3.rbegin();
    }

Alternatively (not sure which one I like better):

    int thirdMax(vector<int>& nums) {
        set<int> top3;
        for (int num : nums)
            if (top3.insert(num).second && top3.size() > 3)
                top3.erase(top3.begin());
        return top3.size() == 3 ? *top3.begin() : *top3.rbegin();
    }
</p>


