---
title: "Design HashSet"
weight: 737
#id: "design-hashset"
---
## Description
<div class="description">
<p>Design a HashSet&nbsp;without using any built-in hash table libraries.</p>

<p>To be specific, your design should include these functions:</p>

<ul>
	<li><code>add(value)</code>:&nbsp;Insert a value into the HashSet.&nbsp;</li>
	<li><code>contains(value)</code> : Return whether the value exists in the HashSet or not.</li>
	<li><code>remove(value)</code>: Remove a value in&nbsp;the HashSet. If the value does not exist in the HashSet, do nothing.</li>
</ul>

<p><br />
<strong>Example:</strong></p>

<pre>
MyHashSet hashSet = new MyHashSet();
hashSet.add(1); &nbsp; &nbsp; &nbsp; &nbsp; 
hashSet.add(2); &nbsp; &nbsp; &nbsp; &nbsp; 
hashSet.contains(1); &nbsp;&nbsp;&nbsp;// returns true
hashSet.contains(3); &nbsp;&nbsp;&nbsp;// returns false (not found)
hashSet.add(2); &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
hashSet.contains(2); &nbsp;&nbsp;&nbsp;// returns true
hashSet.remove(2); &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
hashSet.contains(2); &nbsp;&nbsp;&nbsp;// returns false (already removed)
</pre>

<p><br />
<strong>Note:</strong></p>

<ul>
	<li>All values will be in the range of <code>[0, 1000000]</code>.</li>
	<li>The number of operations will be in the range of&nbsp;<code>[1, 10000]</code>.</li>
	<li>Please do not use the built-in HashSet library.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Design (design)

## Companies
- Google - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Intuition

This is a classical question from textbook, which is intended to test one's knowledge on data structure. Therefore, needless to say, it is not desirable to solve the problem with any build-in HashSet data structure.

>There are two key questions that one should address, in order to implement the HashSet data structure, namely _**hash function**_ and _**collision handling**_.

- _**hash function**_: the goal of the hash function is to assign an address to store a given value. Ideally, each unique value should have an unique hash value. 

- _**collision handling**_: since the nature of a hash function is to map a value from a space `A` into a corresponding value in a __*smaller*__ space `B`, it could happen that multiple values from space `A` might be mapped to the _same_ value in space `B`. This is what we call __*collision*__. Therefore, it is indispensable for us to have a strategy to handle the collision. 

Overall, there are several strategy to resolve the collisions:

- [Separate Chaining](https://en.wikipedia.org/wiki/Hash_table#Separate_chaining): for values with the same hash key, we keep them in a _bucket_, and each bucket is independent from each other.

- [Open Addressing](https://en.wikipedia.org/wiki/Hash_table#Open_addressing): whenever there is a collision, we keep on _probing_ on the main space with certain strategy until a free slot is found.

- [2-Choice Hashing](https://en.wikipedia.org/wiki/2-choice_hashing): we use two hash functions rather than one, and we pick the generated address with fewer collision.

In this article, we focus on the strategy of _**separate chaining**_. Here is how it works overall.

- Essentially, the primary storage underneath a HashSet is a continuous memory as `Array`. Each element in this array corresponds to a `bucket` that stores the actual values.

- Given a `value`, first we generate a key for the value via the _hash function_. The generated key serves as the index to locate the bucket.

- Once the `bucket` is located, we then perform the desired operations on the bucket, such as `add`, `remove` and `contains`.

---
#### Approach 1: LinkedList as Bucket

**Intuition**


The common choice of hash function is the `modulo` operator, _i.e._ $$\text{hash} = \text{value} \mod \text{base}$$. Here, the $$\text{base}$$ of modulo operation would determine the number of buckets that we would have at the end in the HashSet.

Theoretically, the more buckets we have (hence the larger the space would be), the less likely that we would have _collisions_. The choice of $$\text{base}$$ is a tradeoff between the space and the collision. 

In addition, it is generally advisable to use a prime number as the base of modulo, _e.g._ $$769$$, in order to reduce the potential collisions. 

![pic](../Figures/705/705_linked_list.png)

As to the design of `bucket`, again there are several options. One could simply use another `Array` as bucket to store all the values.
However, one drawback with the Array data structure is that it would take $$\mathcal{O}(N)$$ time complexity to remove or insert an element, rather than the desired $$\mathcal{O}(1)$$.

Since for any update operation, we would need to scan the entire _bucket_ first to avoid any duplicate, a better choice for the implementation of _bucket_ would be the _**LinkedList**_, which has a constant time complexity for the _insertion_ as well as _deletion_, once we locate the position to update.

**Algorithm**

As we discussed in the above section, here we adopt the `LinkedList` to implement our _bucket_ within the HashSet.

>Essentially, we are implementing a _LinkedList_ that does not contain any duplicate.

For each of the functions of `add`, `remove` and `contains`, we first generate the bucket index with the hash function. Then, we simply pass down the operation to the underlying bucket.


<iframe src="https://leetcode.com/playground/bqwhM3Lp/shared" frameBorder="0" width="100%" height="500" name="bqwhM3Lp"></iframe>


***Implementation Notes***

In the Python implementation, we employed a sort of **_pseudo head_** to keep a reference to the _actual_ head of the LinkedList, which could _simplify_ a bit the logic by reducing the number of branchings.

For a value that was never seen before, we insert it to the **head** of the bucket, though we could also append it to the tail. It is a choice that we made, which could **fit better** the scenario where redundant values are operated in nearby time windows, since it is more likely that we spot the value at the head of the bucket rather than walking through the entire bucket.


**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(\frac{N}{K})$$ where $$N$$ is the number of all possible values and $$K$$ is the number of predefined buckets, which is `769`.

    - Assuming that the values are _evenly_ distributed, thus we could consider that the average size of bucket is $$\frac{N}{K}$$. 

    - Since for each operation, in the worst case, we would need to scan the entire bucket, hence the time complexity is $$\mathcal{O}(\frac{N}{K})$$.
<br/>

- Space Complexity: $$\mathcal{O}(K+M)$$ where $$K$$ is the number of predefined buckets, and $$M$$ is the number of unique values that have been inserted into the HashSet.
<br/>
<br/>

---
#### Approach 2: Binary Search Tree (BST) as Bucket

**Intuition**

In the above approach, one of the drawbacks is that we have to scan the entire linkedlist in order to verify if a value already exists in the bucket (_i.e._ the lookup operation).

To optimize the above process, one of the strategies could be that we maintain a _**sorted list**_ as the bucket. With the sorted list, we could obtain the $$\mathcal{O}(\log{N})$$ time complexity for the lookup operation, with the binary search algorithm, rather than a linear $$\mathcal{O}({N})$$ complexity as in the above approach.

On the other hand, if we implement the sorted list in a continuous space such as Array, it would incur a _linear_ time complexity for the update operations (_e.g._ _insert_ and _delete_), since we would need to shift the elements.

>So the question is can we have a data structure that have $$\mathcal{O}(\log{N})$$ time complexity, for the operations of _search_, _insert_ and _delete_ ?

Well. The answer is yes, with _**Binary Search Tree**_ (BST). Thanks to the properties of BST, we could optimize the time complexity of our first approach with LinkedList.

![pic](../Figures/705/705_BST.png)

As a result, now the problem is boiled down to the implementation of a standard Binary Search Tree that serves as the _bucket_ in the HashSet.

**Algorithm**

One could build upon the implementation of first approach for our second approach, by applying the [Façade design pattern](https://en.wikipedia.org/wiki/Facade_pattern).

>We have already defined a façade class (_i.e._ `bucket`) with three interfaces (`exists`, `insert` and `delete`), which hides all the underlying details from its users (_i.e._ HashSet).

So we can keep the bulk of the code, and simply modify the implementation of `bucket` class with BST. For each of the interfaces in `bucket`, it corresponds exactly to an operation in BST.

!?!../Documents/705_LIS.json:1000,456!?!

Actually, we have each of the BST operations listed as an independent problem in LeetCode, as follows:

- [Article 700. Search in a BST](https://leetcode.com/articles/search-in-a-bst/)
- [Article 701. Insert in a BST](https://leetcode.com/articles/insert-into-a-bst/)
- [Article 450. Delete in a BST](https://leetcode.com/articles/delete-node-in-a-bst)

One could try these exercises first, and then combine them together to get a full implementation of BST. 

<iframe src="https://leetcode.com/playground/LGKAwaGJ/shared" frameBorder="0" width="100%" height="500" name="LGKAwaGJ"></iframe>



**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(\log{\frac{N}{K}})$$ where $$N$$ is the number of all possible values and $$K$$ is the number of predefined buckets, which is `769`.

    - Assuming that the values are evenly distributed, we could consider that the average size of bucket is $$\frac{N}{K}$$. 

    - When we traverse the BST, we are conducting binary search, as a result, the final time complexity of each operation is $$\mathcal{O}(\log{\frac{N}{K}})$$.

- Space Complexity: $$\mathcal{O}(K+M)$$ where $$K$$ is the number of predefined buckets, and $$M$$ is the number of unique values that have been inserted into the HashSet.
<br/>
<br/>

---
#### Notes on Hash Function

In all the above approaches, the range of address is fixed, since the base of modulo operator is fixed.

Sometimes, it might be more desirable to have a __*dynamic space*__ that goes with the increase of elements in the HashSet. One could set up a threshold on the _load factor_ (_i.e._ ratio between the number of elements and the size of space) of the HashSet, and double the range of address, once the load factor exceeds the threshold.

The increase of address space could potentially **_reduce_** the collisions, therefore improve the overall performance of HashSet.
However, one should also take into account the cost of **_rehashing_** and redistributing the existing values. 

In another scenario, one could adopt the **_2-choice hashing_** as we mentioned at the beginning, which could help the values to be more _**evenly**_ distributed in the address space.
<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### can you guys think before doing?
- Author: fluency03
- Creation Date: Fri Sep 14 2018 14:39:30 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Nov 12 2019 15:04:18 GMT+0800 (Singapore Standard Time)

<p>
the purpose of this leetcode question is not to pass all the test.  it is for understanding how hashset or hashmap works under the hood. 

So please do not post any answers using built-in hashset/hashmap, or just use a single array. Ask yourself, are you going to do the same stupid thing if this is the question you are asked in a real interview?
</p>


### Real Python Solution, no cheating, open addressing
- Author: Tet
- Creation Date: Fri Dec 28 2018 19:58:52 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Dec 28 2018 19:58:52 GMT+0800 (Singapore Standard Time)

<p>
A lot of people don\'t follow the rules and aren\'t interested in learning.  This implementation was based on some data structures concepts and what I\'ve read about Python\'s dict implementation.

The underlying structure that my hash set uses is a list.  The list simply contains a key at the index the key is hashed to, unless there are multiple keys at the same index (ie a collision). More on that later...

My hashset is designed to use a limited amount of memory, but expands itself by a factor of 2 when the load factor (size divided by array spots) exceeds 2/3.  This will allow for O(1) operations on average.  The downside to this is that every time the hashset doubles, it has to create a new list and rehash every element which takes O(n) each time.  The average time will be O(1) for each add call as the cost of rehashing the set is *amortized* over the calls.

Open addressing and chaining each have their own advantages and disadvantages.  I\'m not going to explain the nuances of the methods, but know that chaining hash sets/tables have the head of a linked list of keys in each array spot, while open addressing just moves the key to an empty index.

I used open addressing for this problem.  If we add an element that is hashed to the same index as another key, then we apply a secondary operation on the index until we find an empty spot (or a previously removed spot).  This is called double hashing.  The formula I used is similar to that of the Python dict implementation, but without the perturb part.  This is better than linear probing since the hash function (which just mods the key by the capacity of the list) is likely to fill contiguous array spots and double hashing makes the probability of finding an empty spot more uniform in those cases than linear probing.

For removing, the removed element is replaced by a tombstone so that the contains function won\'t get messed up when the path to an existing element has an empty spot in it, causing the contains function to return false.  The add function will know that tombstones are able to be replaced by new elements.

```
class MyHashSet(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.capacity = 8
        self.size = 0
        self.s = [None]*8
        self.lf = float(2)/3
        
    def myhash(self, key): # can be modified to hash other hashable objects like built in python hash function
        return key%self.capacity
        

    def add(self, key):
        """
        :type key: int
        :rtype: void
        """
        if float(self.size)/self.capacity >= self.lf:
            self.capacity <<= 1
            ns = [None]*self.capacity
            for i in range(self.capacity >> 1):
                if self.s[i] and self.s[i] != "==TOMBSTONE==":
                    n = self.myhash(self.s[i])
                    while ns[n] is not None:
                        n = (5*n+1)%self.capacity
                    ns[n] = self.s[i]
            self.s = ns
        h = self.myhash(key)
        while self.s[h] is not None:
            if self.s[h] == key:
                return
            h = (5*h + 1) % self.capacity
            if self.s[h] == "==TOMBSTONE==":
                break
        self.s[h] = key
        self.size += 1
        
        

    def remove(self, key):
        """
        :type key: int
        :rtype: void
        """
        h = self.myhash(key)
        while self.s[h]:
            if self.s[h] == key:
                self.s[h] = "==TOMBSTONE=="
                self.size -= 1
                return
            h = (5*h+1)%self.capacity
        

    def contains(self, key):
        """
        Returns true if this set contains the specified element
        :type key: int
        :rtype: bool
        """
        h = self.myhash(key)
        while self.s[h] is not None:
            if self.s[h] == key:
                return True
            h = (5*h + 1)%self.capacity
        return False
```
</p>


### C++ 97.97% without a massive array or using a map, BST
- Author: groothedde
- Creation Date: Tue Oct 09 2018 05:12:57 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 09:32:34 GMT+0800 (Singapore Standard Time)

<p>
When I saw the solutions people are submitting I was wondering why I was going through the effort of creating a simple BST, but here goes. This solution should also manage its memory by traversing down the tree when a node is deleted. This means that deleting root will first delete the left and right subtree, before deleting root.

```cpp
// https://www.geeksforgeeks.org/fast-io-for-competitive-programming/
// https://codeforces.com/blog/entry/10297
// People are not actually implementing any form of hash set, so I use tricks
static const int _ = []() {
    ios::sync_with_stdio(false);
    cin.tie(nullptr);
    return 0;
}();

struct MyHashSetNode {
    int val;
    MyHashSetNode *left, *right;
    
    MyHashSetNode(int v) : val(v), left(nullptr), right(nullptr) {}
    ~MyHashSetNode() {
        if(left  != nullptr) delete left;
        if(right != nullptr) delete right;
    }
};

class MyHashSetTree {
private:
    MyHashSetNode *root;
public:
    MyHashSetTree() : root(nullptr) {}
    ~MyHashSetTree() {
        if(root != nullptr) 
            delete root; // traverses through destructors
    }
    
    MyHashSetNode *Insert(int x, MyHashSetNode *parent) {
        MyHashSetNode *result = parent;

        if (parent == nullptr) {
            result = new MyHashSetNode(x);
        } else if (x < parent->val) {
            parent->left = Insert(x, parent->left);
        } else if (x > parent->val) {
            parent->right = Insert(x, parent->right);
        } else {
            return nullptr; // duplicate, add should not comply
        }

        return result;
    }
    
    bool Insert(int x) {
        if(Find(x) != nullptr)
            return false;
        
        root = Insert(x, root);
        return true;
    }
    
    MyHashSetNode *Find(int x, MyHashSetNode *parent) {
        MyHashSetNode *current = parent;
        int currentValue;

        while (current != nullptr) {
            currentValue = current->val;
            if (x < currentValue) {
                current = current->left;
            } else if (x > currentValue) {
                current = current->right;
            } else {
                return current;
            }
        }

        return nullptr;
    }
    
    MyHashSetNode *Find(int x) {
        if (root != nullptr) 
            return Find(x, root);

        return nullptr;
    }
    
    MyHashSetNode *FindMin(MyHashSetNode *parent) {
        MyHashSetNode *current = parent;
        MyHashSetNode *left = nullptr;

        if (current != nullptr) {
            while ((left = current->left) != nullptr)
                current = left;
        }

        return current;
    }

    MyHashSetNode *RemoveMin(MyHashSetNode *parent) {
        if (parent == nullptr) {
            return nullptr;
        } else if (parent->left != nullptr) {
            parent->left = RemoveMin(parent->left);
            return parent;
        } else {
            MyHashSetNode *result = parent->right;

            parent->right = parent->left = nullptr;
            delete parent;
            return result;
        }
    }
    
    MyHashSetNode *Remove(int x, MyHashSetNode *parent) {
        MyHashSetNode *current = parent;
        MyHashSetNode *left = nullptr;
        MyHashSetNode *right = nullptr;
        int currentValue;

        if (current != nullptr) {
            left = current->left;
            right = current->right;
            currentValue = current->val;
        }

        if (current == nullptr) {
            return nullptr;
        } else if (x < currentValue) {
            current->left = Remove(x, left);
        } else if (x > currentValue) {
            current->right = Remove(x, right);
        } else if (left != nullptr && right != nullptr) {
            current->val = FindMin(right)->val;
            current->right = RemoveMin(right);
        } else {
            current = (left != nullptr) ? left : right;

            parent->right = parent->left = nullptr;
            delete parent;
        }

        return current;
    }
    
    bool Remove(int x) {
        if(Find(x) == nullptr)
            return false;
        
        root = Remove(x, root);
        return true;
    }
};

class MyHashSet {
private:
    MyHashSetTree tree;
public:
    /** Initialize your data structure here. */
    MyHashSet() {
        
    }
    
    void add(int key) {
        tree.Insert(key);
    }
    
    void remove(int key) {
        tree.Remove(key);
    }
    
    /** Returns true if this set contains the specified element */
    bool contains(int key) {
        return tree.Find(key) != nullptr;
    }
};
 ```
</p>


