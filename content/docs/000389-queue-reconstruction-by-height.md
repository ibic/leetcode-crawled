---
title: "Queue Reconstruction by Height"
weight: 389
#id: "queue-reconstruction-by-height"
---
## Description
<div class="description">
<p>Suppose you have a random list of people standing in a queue. Each person is described by a pair of integers <code>(h, k)</code>, where <code>h</code> is the height of the person and <code>k</code> is the number of people in front of this person who have a height greater than or equal to <code>h</code>. Write an algorithm to reconstruct the queue.</p>

<p><b>Note:</b><br />
The number of people is less than 1,100.</p>
&nbsp;

<p><b>Example</b></p>

<pre>
Input:
[[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]]

Output:
[[5,0], [7,0], [5,2], [6,1], [4,4], [7,1]]
</pre>

<p>&nbsp;</p>

</div>

## Tags
- Greedy (greedy)

## Companies
- Facebook - 5 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Apple - 3 (taggedByAdmin: false)
- ByteDance - 3 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Approach 1: Greedy

**Intuition**

The problem is to reconstruct the queue.

![pic](../Figures/406/input.png)

Let's start from the simplest case, 
when all guys (h, k) in the queue are of the same height h,
and differ by their k values only (the number of people in front 
who have a greater or the same height).
Then the solution is simple: each guy's index is equal to his k value.
The guy with zero people in front takes the place number 0,
the guy with 1 person in front takes the place number 1, and 
so on and so forth.  

![fig](../Figures/406/same.png)

This strategy could be used even in the case when not all people
are of the same height. The smaller persons are "invisible" for the 
taller ones, and hence one could first arrange the tallest guys as if 
there was no one else.

Let's now consider a queue with people of two different heights: 7 and 6.
For simplicity, let's have just one 6-height guy.
First follow the strategy above and arrange guys of height 7. Now it's 
time to find a place for the guy of height 6. 
Since he is "invisible" for the 7-height guys, he could take 
whatever place without disturbing 7-height guys order. 
However, for him the others are visible, and hence he should 
take the position equal to his k-value, in order to have his proper place. 
 
![fig](../Figures/406/two.png)

> This idea is easy to extend for the case of numerous guys of height 6.
Just sort them by k-values, as it was done before for 7-height guys,
and insert them one by one on the positions equal to their k-values. 

The following strategy could be continued recursively:

- Sort the tallest guys in the ascending order by k-values 
and then insert them one by one into output queue at the 
indexes equal to their k-values.

- Take the next height in the descending order. 
Sort the guys of that height in the ascending order by k-values 
and then insert them one by one into output queue at the 
indexes equal to their k-values.

- And so on and so forth.

!?!../Documents/406_LIS.json:1000,612!?!

**Algorithm**

- Sort people: 
    - In the descending order by height. 
    - Among the guys of the same height, in the ascending order by k-values.

- Take guys one by one, 
and place them in the output array at the indexes equal to their k-values.

- Return output array.

**Implementation**

<iframe src="https://leetcode.com/playground/p3kzy72R/shared" frameBorder="0" width="100%" height="378" name="p3kzy72R"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N^2)$$. To sort people takes 
$$\mathcal{O}(N \log N)$$ time. Then one proceeds to n insert
operations, and each takes up to $$\mathcal{O}(k)$$ time, 
where k is a current number of elements in the list. In total,
one needs up to 
$$\mathcal{O}\left({\sum\limits_{k = 0}^{N - 1}{k}}\right)$$ time,
i.e. up to $$\mathcal{O}(N^2)$$ time.
 
* Space complexity : $$\mathcal{O}(N)$$ to keep the output.

## Accepted Submission (golang)
```golang
//package main

import (
	"fmt"
	"math"
	"math/big"
	"sort"
	"strings"
	"strconv"
)

func unused() int {
	fmt.Println("no bugger")
	math.Abs(0)
	sort.Ints([]int{0})
	strings.Title("hello world")
	strconv.Atoi("0")
	fmt.Println(big.NewInt(0))
	return 0
}

func prl(a ...interface{}) (n int, err error) {
	//return fmt.Println(a)
	return 0, nil
}

func reconstructQueue(people [][]int) [][]int {
	//hpClone := append([][]int(nil), people...)
	hpClone := people
	sort.Slice(hpClone, func (i, j int) bool {
		if hpClone[i][0] < hpClone[j][0] {
			return true
		}
		if hpClone[i][0] == hpClone[j][0] {
			return hpClone[i][1] >= hpClone[j][1]
		}
		return false
	})
	prl(hpClone)
	seq := make([][]int, len(people))
	taken := make([]int, len(people))
	for _, hp := range hpClone {
		front := hp[1]
		pos := 0
		prl("front", front)
		for {
			for taken[pos] != 0 {
				pos++
			}
			front--
			if front < 0 {
				break
			} else {
				pos++
			}
		}  
		prl("pos", pos)
		seq[pos] = hp
		taken[pos] = 1
	}
	return seq
}

func mainly() {
}


```

## Top Discussions
### Easy concept with Python/C++/Java Solution
- Author: YJL1228
- Creation Date: Sun Sep 25 2016 19:52:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 05:42:00 GMT+0800 (Singapore Standard Time)

<p>
1. **Pick out tallest group of people and sort them** in a subarray (S). Since there's no other groups of people taller than them, therefore **each guy's index will be just as same as his k value**.
2. For 2nd tallest group (and the rest), insert each one of them into (S) by k value. So on and so forth.

E.g.
input: [[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]]
subarray after step 1: [**[7,0], [7,1]**]
subarray after step 2: [[7,0], **[6,1]**, [7,1]]
...

It's not the most concise code, but I think it well explained the concept.

```
class Solution(object):
    def reconstructQueue(self, people):
        if not people: return []

        # obtain everyone's info
        # key=height, value=k-value, index in original array
        peopledct, height, res = {}, [], []
        
        for i in xrange(len(people)):
            p = people[i]
            if p[0] in peopledct:
                peopledct[p[0]] += (p[1], i),
            else:
                peopledct[p[0]] = [(p[1], i)]
                height += p[0],

        height.sort()      # here are different heights we have

        # sort from the tallest group
        for h in height[::-1]:
            peopledct[h].sort()
            for p in peopledct[h]:
                res.insert(p[0], people[p[1]])

        return res

```

**EDIT:**
Please also check:
@tlhuang 's concise Python code.
@wsurvi 's 4 lines Python code.
@tonygogogo 's 8 lines C++ solution.
@zeller2 's Java version.
@hotpro 's Java 8 solution.
</p>


### Explanation of the neat Sort+Insert solution
- Author: StefanPochmann
- Creation Date: Thu Sep 29 2016 22:04:33 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 14:50:17 GMT+0800 (Singapore Standard Time)

<p>
Below is my explanation of the following neat solution where we sort people from tall to short (and by increasing k-value) and then just insert them into the queue using their k-value as the queue index:

    def reconstructQueue(self, people):
        people.sort(key=lambda (h, k): (-h, k))
        queue = []
        for p in people:
            queue.insert(p[1], p)
        return queue

I didn't come up with that myself, but here's my own explanation of it, as I haven't seen anybody explain it (and was asked to explain it):

People are only counting (in their k-value) taller or equal-height others standing in front of them. So a smallest person is **completely irrelevant** for all taller ones. And of all smallest people, the one standing most in the back is even completely irrelevant for **everybody** else. Nobody is counting that person. So we can first arrange everybody else, ignoring that one person. And then just insert that person appropriately. Now note that while this person is irrelevant for everybody else, everybody else is relevant for this person - this person counts exactly everybody in front of them. So their count-value tells you exactly the index they must be standing.

So you can first solve the sub-problem with all but that one person and then just insert that person appropriately. And you can solve that sub-problem the same way, first solving the sub-sub-problem with all but the last-smallest person of the subproblem. And so on. The base case is when you have the sub-...-sub-problem of zero people. You're then inserting the people in the reverse order, i.e., that overall last-smallest person in the very end and thus the first-tallest person in the very beginning. That's what the above solution does, Sorting the people from the first-tallest to the last-smallest, and inserting them one by one as appropriate.

Now that's **my** explanation. If you have a different one, I'm interested to see it :-)
</p>


### I don't understand the question. Reconstruct the queue to what?
- Author: stanley16
- Creation Date: Tue Jan 10 2017 01:03:02 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 23:38:41 GMT+0800 (Singapore Standard Time)

<p>
Is reconstructing a queue a keyword in programming? I am not familiar.

Thanks.
</p>


