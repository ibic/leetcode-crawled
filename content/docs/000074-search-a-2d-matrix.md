---
title: "Search a 2D Matrix"
weight: 74
#id: "search-a-2d-matrix"
---
## Description
<div class="description">
<p>Write an efficient algorithm that searches for a value in an <code>m x n</code> matrix. This matrix has the following properties:</p>

<ul>
	<li>Integers in each row are sorted from left to right.</li>
	<li>The first integer of each row is greater than the last integer of the previous row.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/10/05/mat.jpg" style="width: 322px; height: 242px;" />
<pre>
<strong>Input:</strong> matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,50]], target = 3
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/10/05/mat2.jpg" style="width: 322px; height: 242px;" />
<pre>
<strong>Input:</strong> matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,50]], target = 13
<strong>Output:</strong> false
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> matrix = [], target = 0
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m == matrix.length</code></li>
	<li><code>n == matrix[i].length</code></li>
	<li><code>0 &lt;= m, n &lt;= 100</code></li>
	<li><code>-10<sup>4</sup> &lt;= matrix[i][j], target &lt;= 10<sup>4</sup></code></li>
</ul>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Amazon - 13 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
   
#### Approach 1: Binary Search

**Intuition**

One could notice that the input matrix `m x n` could be considered 
as a sorted array of length `m x n`.

![bla](../Figures/74/matrix2.png)

Sorted array is a perfect candidate for the binary search
because the element index in this _virtual_ array (_for sure we're 
not going to construct it for real_) could be easily
transformed into the row and column in the initial matrix

> `row = idx // n` and `col = idx % n`.

**Algorithm**

The algorithm is a standard binary search : 

* Initialise left and right indexes 
`left = 0` and `right = m x n - 1`.

* While `left <= right` :

    * Pick up the index in the middle of the virtual array 
    as a pivot index : `pivot_idx = (left + right) / 2`.
    
    * The index corresponds to `row = pivot_idx // n` and
    `col = pivot_idx % n` in the initial matrix, and hence
    one could get the `pivot_element`.
    This element splits the virtual array in two parts. 
    
    * Compare `pivot_element` and `target` 
    to identify in which part one has to look for `target`.
        
**Implementation**

!?!../Documents/74_LIS.json:1000,436!?!

<iframe src="https://leetcode.com/playground/VXaabxWV/shared" frameBorder="0" width="100%" height="497" name="VXaabxWV"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(\log(m n))$$ since it's a standard binary search.

* Space complexity : $$\mathcal{O}(1)$$. 


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Don't treat it as a 2D matrix, just treat it as a sorted list
- Author: vaputa
- Creation Date: Wed Sep 10 2014 14:09:32 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 08:42:51 GMT+0800 (Singapore Standard Time)

<p>
Use binary search. 

n * m matrix convert to an array => matrix[x][y] => a[x * m + y]

an array convert to n * m matrix => a[x] =>matrix[x / m][x % m];

    class Solution {
    public:
        bool searchMatrix(vector<vector<int> > &matrix, int target) {
            int n = matrix.size();
            int m = matrix[0].size();
            int l = 0, r = m * n - 1;
            while (l != r){
                int mid = (l + r - 1) >> 1;
                if (matrix[mid / m][mid % m] < target)
                    l = mid + 1;
                else 
                    r = mid;
            }
            return matrix[r / m][r % m] == target;
        }
    };
</p>


### Binary search on an ordered matrix
- Author: liaison
- Creation Date: Fri Nov 07 2014 08:09:30 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 04:55:59 GMT+0800 (Singapore Standard Time)

<p>
	/**
	 *  Do binary search in this "ordered" matrix
	 */
	public boolean searchMatrix(int[][] matrix, int target) {
		
		int row_num = matrix.length;
		int col_num = matrix[0].length;
		
		int begin = 0, end = row_num * col_num - 1;
		
		while(begin <= end){
			int mid = (begin + end) / 2;
			int mid_value = matrix[mid/col_num][mid%col_num];
			
			if( mid_value == target){
				return true;
			
			}else if(mid_value < target){
				//Should move a bit further, otherwise dead loop.
				begin = mid+1;
			}else{
				end = mid-1;
			}
		}
		
		return false;
	}
</p>


### A Python binary search solution - O(logn)
- Author: Google
- Creation Date: Sun Mar 15 2015 09:32:17 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 00:41:50 GMT+0800 (Singapore Standard Time)

<p>
It is basically an advanced version of the binary search

    class Solution:
        # @param matrix, a list of lists of integers
        # @param target, an integer
        # @return a boolean
        # 8:21
        def searchMatrix(self, matrix, target):
            if not matrix or target is None:
                return False
    
            rows, cols = len(matrix), len(matrix[0])
            low, high = 0, rows * cols - 1
            
            while low <= high:
                mid = (low + high) / 2
                num = matrix[mid / cols][mid % cols]
    
                if num == target:
                    return True
                elif num < target:
                    low = mid + 1
                else:
                    high = mid - 1
            
            return False
</p>


