---
title: "Count Largest Group"
weight: 1286
#id: "count-largest-group"
---
## Description
<div class="description">
<p>Given an integer&nbsp;<code>n</code>.&nbsp;Each number from <code>1</code> to <code>n</code> is grouped according to the sum of its digits.&nbsp;</p>

<p>Return&nbsp;how many groups have the largest size.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 13
<strong>Output:</strong> 4
<strong>Explanation:</strong> There are 9 groups in total, they are grouped according sum of its digits of numbers from 1 to 13:
[1,10], [2,11], [3,12], [4,13], [5], [6], [7], [8], [9]. There are 4 groups with largest size.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 2
<strong>Output:</strong> 2
<strong>Explanation:</strong> There are 2 groups [1], [2] of size 1.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 15
<strong>Output:</strong> 6
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 24
<strong>Output:</strong> 5
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^4</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- mercari - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java Count
- Author: votrubac
- Creation Date: Sun Apr 05 2020 00:01:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 05 2020 00:01:56 GMT+0800 (Singapore Standard Time)

<p>
**C++**
```cpp
int dsum(int n) {
    return n == 0 ? 0 : n % 10 + dsum(n / 10);
}
int countLargestGroup(int n) {
    int cnt[37] = {}, mx = 0;
    for (auto i = 1; i <= n; ++i)
        mx = max(mx, ++cnt[dsum(i)]);
    return count_if(begin(cnt), end(cnt), [&mx](int n) { return n == mx; });
}
```
**Java**
```java
int dsum(int n) {
    return n == 0 ? 0 : n % 10 + dsum(n / 10);
}
public int countLargestGroup(int n) {
    ArrayList<Integer> cnt = new ArrayList<>(Collections.nCopies(37, 0));
    for (int i = 1; i <= n; ++i) {
        int c = dsum(i);
        cnt.set(c, cnt.get(c) + 1);
    }
    return Collections.frequency(cnt, Collections.max(cnt));
}
```
</p>


### Can someone please explain question. I find it hard to understand the question??? Need help!!!!!!
- Author: tenzindadhul
- Creation Date: Wed Jun 03 2020 16:12:25 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 03 2020 16:12:25 GMT+0800 (Singapore Standard Time)

<p>
Can someone please explain question. I find it hard to understand the question??? Need help!!!!!!
</p>


### 1 line Python
- Author: ManuelP
- Creation Date: Sun Apr 05 2020 00:06:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 05 2020 00:31:45 GMT+0800 (Singapore Standard Time)

<p>
Most frequent value is called [&rightarrow;mode](https://en.wikipedia.org/wiki/Mode_(statistics)), and [&rightarrow;`statistics.multimode`](https://docs.python.org/3/library/statistics.html#statistics.multimode) gives us all of them.
```
def countLargestGroup(self, n: int) -> int:
    return len(statistics.multimode(sum(map(int, str(i))) for i in range(1, n+1)))
```
</p>


