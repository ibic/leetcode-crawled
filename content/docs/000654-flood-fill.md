---
title: "Flood Fill"
weight: 654
#id: "flood-fill"
---
## Description
<div class="description">
<p>
An <code>image</code> is represented by a 2-D array of integers, each integer representing the pixel value of the image (from 0 to 65535).
</p><p>
Given a coordinate <code>(sr, sc)</code> representing the starting pixel (row and column) of the flood fill, and a pixel value <code>newColor</code>, "flood fill" the image.
</p><p>
To perform a "flood fill", consider the starting pixel, plus any pixels connected 4-directionally to the starting pixel of the same color as the starting pixel, plus any pixels connected 4-directionally to those pixels (also with the same color as the starting pixel), and so on.  Replace the color of all of the aforementioned pixels with the newColor.
</p><p>
At the end, return the modified image.
</p>
<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> 
image = [[1,1,1],[1,1,0],[1,0,1]]
sr = 1, sc = 1, newColor = 2
<b>Output:</b> [[2,2,2],[2,2,0],[2,0,1]]
<b>Explanation:</b> 
From the center of the image (with position (sr, sc) = (1, 1)), all pixels connected 
by a path of the same color as the starting pixel are colored with the new color.
Note the bottom corner is not colored 2, because it is not 4-directionally connected
to the starting pixel.
</pre>
</p>

<p><b>Note:</b>
<li>The length of <code>image</code> and <code>image[0]</code> will be in the range <code>[1, 50]</code>.</li>
<li>The given starting pixel will satisfy <code>0 <= sr < image.length</code> and <code>0 <= sc < image[0].length</code>.</li>
<li>The value of each color in <code>image[i][j]</code> and <code>newColor</code> will be an integer in <code>[0, 65535]</code>.</li>
</p>
</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 15 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Palantir Technologies - 7 (taggedByAdmin: false)
- Apple - 5 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Qualtrics - 2 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Depth-First Search [Accepted]

**Intuition**

We perform the algorithm explained in the problem description: paint the starting pixels, plus adjacent pixels of the same color, and so on.

**Algorithm**

Say `color` is the color of the starting pixel.  Let's floodfill the starting pixel: we change the color of that pixel to the new color, then check the 4 neighboring pixels to make sure they are valid pixels of the same `color`, and of the valid ones, we floodfill those, and so on.

We can use a function `dfs` to perform a floodfill on a target pixel.

<iframe src="https://leetcode.com/playground/mBGsyAup/shared" frameBorder="0" width="100%" height="327" name="mBGsyAup"></iframe>


**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of pixels in the image.  We might process every pixel.

* Space Complexity: $$O(N)$$, the size of the implicit call stack when calling `dfs`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 9 liner, DFS
- Author: shawngao
- Creation Date: Sun Nov 26 2017 12:14:28 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 21:56:05 GMT+0800 (Singapore Standard Time)

<p>
Time complexity: O(m*n), space complexity: O(1). m is number of rows, n is number of columns.
```
class Solution {
    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        if (image[sr][sc] == newColor) return image;
        fill(image, sr, sc, image[sr][sc], newColor);
        return image;
    }
    
    private void fill(int[][] image, int sr, int sc, int color, int newColor) {
        if (sr < 0 || sr >= image.length || sc < 0 || sc >= image[0].length || image[sr][sc] != color) return;
        image[sr][sc] = newColor;
        fill(image, sr + 1, sc, color, newColor);
        fill(image, sr - 1, sc, color, newColor);
        fill(image, sr, sc + 1, color, newColor);
        fill(image, sr, sc - 1, color, newColor);
    }
}
```
</p>


### Easy Python DFS (no need for visited)!!!
- Author: yangshun
- Creation Date: Sun Nov 26 2017 14:04:12 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 22 2018 05:15:44 GMT+0800 (Singapore Standard Time)

<p>
The idea is simple. Simply perform a DFS on the source cell. Continue the DFS if:

1. Next cell is within bounds.
2. Next cell is the same color as source cell.

There is a tricky case where the new color is the same as the original color and if the DFS is done on it, there will be an infinite loop. If new color is same as original color, there is nothing to be done and we can simply return the `image`.

*- Yangshun*

```
class Solution(object):
    def floodFill(self, image, sr, sc, newColor):
        rows, cols, orig_color = len(image), len(image[0]), image[sr][sc]
        def traverse(row, col):
            if (not (0 <= row < rows and 0 <= col < cols)) or image[row][col] != orig_color:
                return
            image[row][col] = newColor
            [traverse(row + x, col + y) for (x, y) in ((0, 1), (1, 0), (0, -1), (-1, 0))]
        if orig_color != newColor:
            traverse(sr, sc)
        return image
```
</p>


### Python nice & clean DFS & BFS solutions
- Author: cenkay
- Creation Date: Tue Oct 30 2018 21:03:16 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 30 2018 21:03:16 GMT+0800 (Singapore Standard Time)

<p>
* BFS
```
class Solution:
    def floodFill(self, image, sr, sc, newColor):
        old, m, n = image[sr][sc], len(image), len(image[0])
        if old != newColor: 
            q = collections.deque([(sr, sc)])
            while q:
                i, j = q.popleft()
                image[i][j] = newColor
                for x, y in ((i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)):
                    if 0 <= x < m and 0 <= y < n and image[x][y] == old: 
                        q.append((x, y))
        return image
```
* DFS
```
class Solution:
    def floodFill(self, image, sr, sc, newColor):
        def dfs(i, j):
            image[i][j] = newColor
            for x, y in ((i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)):
                if 0 <= x < m and 0 <= y < n and image[x][y] == old:
                    dfs(x, y)
        old, m, n = image[sr][sc], len(image), len(image[0])
        if old != newColor: 
            dfs(sr, sc)
        return image
```
</p>


