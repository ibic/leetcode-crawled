---
title: "Super Washing Machines"
weight: 488
#id: "super-washing-machines"
---
## Description
<div class="description">
<p>You have <b>n</b> super washing machines on a line. Initially, each washing machine has some dresses or is empty. 
</p>

<p>For each <b>move</b>, you could choose <b>any m</b> (1 &le; m &le; n) washing machines, and pass <b>one dress</b> of each washing machine to one of its adjacent washing machines <b> at the same time </b>.  </p>

<p>Given an integer array representing the number of dresses in each washing machine from left to right on the line, you should find the <b>minimum number of moves</b> to make all the washing machines have the same number of dresses. If it is not possible to do it, return -1.</p>

<p><b>Example1</b>
<pre>
<b>Input:</b> [1,0,5]

<b>Output:</b> 3

<b>Explanation:</b> 
1st move:    1     0 <-- 5    =>    1     1     4
2nd move:    1 <-- 1 <-- 4    =>    2     1     3    
3rd move:    2     1 <-- 3    =>    2     2     2   
</pre>

<p><b>Example2</b>
<pre>
<b>Input:</b> [0,3,0]

<b>Output:</b> 2

<b>Explanation:</b> 
1st move:    0 <-- 3     0    =>    1     2     0    
2nd move:    1     2 --> 0    =>    1     1     1     
</pre>

<p><b>Example3</b>
<pre>
<b>Input:</b> [0,2,0]

<b>Output:</b> -1

<b>Explanation:</b> 
It's impossible to make all the three washing machines have the same number of dresses. 
</pre>

</p>

<p><b>Note:</b><br>
<ol>
<li>The range of n is [1, 10000].</li>
<li>The range of dresses number in a super washing machine is [0, 1e5].</li>
</ol>
</p>
</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Greedy.

**Intuition**

This greedy problem is very similar to [Gas station problem](https://leetcode.com/articles/gas-station/),
and could be solved in linear time as well. 

> First of all - could the problem be solved or not? 

Yes, if the dresses could be divided into `N` equal parts where `N` is number 
of machines. In other words, `N` should be a divisor of the number of dresses `D`. 

![bla](../Figures/517/could_solve2.png)

Now it's easy to compute the number of dresses that each machine should have: `D / N`.
The starting numbers of dresses in the machines move around this `D / N` average value. 

![bla](../Figures/517/distribution.png)

The standard ML trick is to normalize the data, so that the average value would be zero. 
For that, one could replace the actual number of dresses in the machine by
 the number of dresses to be removed. This number could be negative, if one 
 actually needs _to add_ the dresses into the machine. 
 
![bla](../Figures/517/to_be_removed.png)

As for the [gas station problem](https://leetcode.com/articles/gas-station), one starts from the beginning 
and checks the standard set for such problems:
the current element, the current sum, and 
the maximum sum seen so far :

- `m`. Number of dresses to be removed from the current machine.

- `curr_sum`. Number of dresses to be passed on the right.

- `max_sum`. Maximum number of dresses one had to pass on the right
at this point or before.

It's quite obvious that the result at each point is a maximum 
between `max_sum` and `m`, i.e. one has to compare the cumulative and 
the local maximums.

Here are three different examples.

- `[1, 0, 5]`. The cumulative maximum is equal to the local one.

![bla](../Figures/517/table1.png)

- `[0, 3, 0]`. The local maximum wins over the cumulative one.

![bla](../Figures/517/table2.png)

- `[0, 0, 3, 5]`. The cumulative maximum wins over the local one.

![bla](../Figures/517/table3.png)

**Algorithm**

Here is the algorithm.

1. Check if the problem could be solved: `len(machines)` should be
a divisor of `sum(machines)`. Otherwise the answer is `-1`.

2. Compute the number of dresses each machine should finally have:
`dresses_per_machine = sum(machines)/len(machines)`.

3. [Normalize](https://en.wikipedia.org/wiki/Normalization#Technology_and_computer_science) 
the problem by replacing the _number of dresses_ in each machine 
by the _number of dresses to be removed_ from this machine (could be negative). 

4. Initiate `curr_sum`, `max_sum`, and `res` as zero.

5. Iterate over all machines `m in machines`:
    
    - Update `curr_sum` and `max_sum` at each step:
    `curr_sum += m`, `max_sum = max(max_sum, abs(curr_sum))`. 
    
    - Update result `res = max(res, max_sum, m)`.

6. Return `res`.

**Implementation**

<iframe src="https://leetcode.com/playground/3dz7v9TB/shared" frameBorder="0" width="100%" height="480" name="3dz7v9TB"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since it's a three
iterations over the input array.
 
* Space complexity : $$\mathcal{O}(1)$$ since it's a constant 
space solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Super Short & Easy Java O(n) Solution
- Author: chidong
- Creation Date: Sun Feb 19 2017 13:36:57 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 06:24:31 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public int findMinMoves(int[] machines) {
        int total = 0; 
        for(int i: machines) total+=i;
        if(total%machines.length!=0) return -1;
        int avg = total/machines.length, cnt = 0, max = 0;
        for(int load: machines){
            cnt += load-avg; //load-avg is "gain/lose"
            max = Math.max(Math.max(max, Math.abs(cnt)), load-avg);
        }
        return max;
    }
}
```

Let me use an example to briefly explain this. For example, your machines[] is [0,0,11,5]. So your total is 16 and the target value for each machine is 4. Convert the machines array to a kind of gain/lose array, we get: [-4,-4,7,1]. Now what we want to do is go from the first one and try to make all of them 0.
To make the 1st machines 0, you need to give all its "load" to the 2nd machines. 
So we get: [0,-8,7,1]
then: [0,0,-1,1]
lastly: [0,0,0,0], done. 
You don't have to worry about the details about how these machines give load to each other. In this process, the least steps we need to eventually finish this process is determined by the peak of abs(cnt) and the max of "gain/lose" array. In this case, the peak of abs(cnt) is 8 and the max of gain/lose array is 7. So the result is 8.


Some other example:
machines: [0,3,0]; gain/lose array: [-1,2,-1]; max = 2, cnt = 0, -1, 1, 0, its abs peak is 1. So result is 2. 
machines: [1,0,5]; gain/lose array: [-1,-2,3]; max = 3, cnt = 0, -1, -3, 0, its abs peak is 3. So result is 3.
</p>


### Very intuitive O(n) solution
- Author: mgispk
- Creation Date: Mon Feb 20 2017 02:50:37 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 01:42:20 GMT+0800 (Singapore Standard Time)

<p>
Instead of using some DP methodology to solve the problem, I have a very intuitive way to approach the solution. 

Think about the machine ```i```, after we make all machines have the same dresses, **how many dresses will be passed through machine ```i```**?
Let's denote the current sum of dresses of machines ```[0...i-1]``` as ```leftSums[i]```, and the current sum of dresses of machines ```[i+1...n-1]``` as rightSums[i].
Let's denote the expected sum of dresses of machines ```[0...i-1]``` as ```expLeft```, which means after all dresses are equally distributed, the sum of address in machines ```[0...i-1]``` should be ```expLeft```. The same logic applies to machines ```[i+1...n-1]```, denoted as ```expRight```.

Then the above question should be clearly answered. If ```expLeft``` is larger than ```leftSums[i]```, that means no matter how you move the dresses, there will be at least ```expLeft - leftSums[i]``` dresses being moved to left of machine ```i```, which means pass through machine ```i```. For the right machines of machine i, the logic remains the same. So we could conclude that the minimum dresses passed through machine ```i``` will be:
```
left = expLeft > leftSums[i] ? expLeft - leftSums[i] : 0;
right = expRight > rightSums[i] ? expRight - rightSums[i] : 0;
total = left + right;
```

With this answer in mind, we could know that the minimum moves is the maximum dresses that pass through for each single machine, because for each dress, it will require at least one move. Hence the following solution. The code could be more concise, but I will leave it here for purpose of explanation. 

If you have any doubts or suggestions for this solution, any comments are welcome.

```
public class Solution {
    public int findMinMoves(int[] machines) {
        int n = machines.length;
        int sum = 0;
        for (int num : machines) {
            sum += num;
        }
        if (sum % n != 0) {
            return -1;
        }
        int avg = sum / n;
        int[] leftSums = new int[n];
        int[] rightSums = new int[n];
        for (int i = 1; i < n; i ++) {
            leftSums[i] = leftSums[i-1] + machines[i-1];
        }
        for (int i = n - 2; i >= 0; i --) {
            rightSums[i] = rightSums[i+1] + machines[i+1];
        }
        int move = 0;
        for (int i = 0; i < n; i ++) {
            int expLeft = i * avg;
            int expRight = (n - i - 1) * avg;
            int left = 0;
            int right = 0;
            if (expLeft > leftSums[i]) {
                left = expLeft - leftSums[i];
            } 
            if (expRight > rightSums[i]) {
                right = expRight - rightSums[i];
            }
            move = Math.max(move, left + right);
        }
        return move;
    }
}
```
</p>


### C++ 16ms O(n) solution (with trivial proof)
- Author: Mrsuyi
- Creation Date: Sun Feb 19 2017 12:35:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 21:07:31 GMT+0800 (Singapore Standard Time)

<p>
First we check the sum of dresses in all machines. if that number cannot be divided by count of machines, there is no solution.

Otherwise, we can always transfer a dress from one machine to another, one at a time until every machines reach the same number, so there must be a solution. In this way, the total actions is sum of operations on every machine.

Since we can operate several machines at the same time, the minium number of moves is the maximum number of necessary operations on every machine.

For a single machine, necessary operations is to transfer dresses from one side to another until sum of both sides and itself reaches the average number. We can calculate (required dresses) - (contained dresses) of each side as L and R:

L > 0 && R > 0: both sides lacks dresses, and we can only export one dress from current machines at a time, so result is abs(L) + abs(R)
L < 0 && R < 0: both sides contains too many dresses, and we can import dresses from both sides at the same time, so result is max(abs(L), abs(R))
L < 0 && R > 0 or L >0 && R < 0: the side with a larger absolute value will import/export its extra dresses from/to current machine or other side, so result is max(abs(L), abs(R))

For example, [1, 0, 5], average is 2
for 1, L = 0 * 2 - 0 = 0, R = 2 * 2 - 5= -1, result = 1
for 0, L = 1 * 2 - 1= 1, R = 1 * 2 - 5 = -3, result = 3
for 5, L = 2 * 2 - 1= 3, R = 0 * 2 - 0= 0, result = 3
so minium moves is 3

```
class Solution {
public:
    int findMinMoves(vector<int>& machines) {
        int len = machines.size();
        vector<int> sum(len + 1, 0);
        for (int i = 0; i < len; ++i)
            sum[i + 1] = sum[i] + machines[i];

        if (sum[len] % len) return -1;

        int avg = sum[len] / len;
        int res = 0;
        for (int i = 0; i < len; ++i)
        {
            int l = i * avg - sum[i];
            int r = (len - i - 1) * avg - (sum[len] - sum[i] - machines[i]);

            if (l > 0 && r > 0)
                res = std::max(res, std::abs(l) + std::abs(r));
            else
                res = std::max(res, std::max(std::abs(l), std::abs(r)));
        }
        return res;
    }
};
```
</p>


