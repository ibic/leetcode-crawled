---
title: "IP to CIDR"
weight: 673
#id: "ip-to-cidr"
---
## Description
<div class="description">
<p>
Given a start IP address <code>ip</code> and a number of ips we need to cover <code>n</code>, return a representation of the range as a list (of smallest possible length) of CIDR blocks.
</p><p>
A CIDR block is a string consisting of an IP, followed by a slash, and then the prefix length.  For example: "123.45.67.89/20".  That prefix length "20" represents the number of common prefix bits in the specified range.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> ip = "255.0.0.7", n = 10
<b>Output:</b> ["255.0.0.7/32","255.0.0.8/29","255.0.0.16/32"]
<b>Explanation:</b>
The initial ip address, when converted to binary, looks like this (spaces added for clarity):
255.0.0.7 -> 11111111 00000000 00000000 00000111
The address "255.0.0.7/32" specifies all addresses with a common prefix of 32 bits to the given address,
ie. just this one address.

The address "255.0.0.8/29" specifies all addresses with a common prefix of 29 bits to the given address:
255.0.0.8 -> 11111111 00000000 00000000 00001000
Addresses with common prefix of 29 bits are:
11111111 00000000 00000000 00001000
11111111 00000000 00000000 00001001
11111111 00000000 00000000 00001010
11111111 00000000 00000000 00001011
11111111 00000000 00000000 00001100
11111111 00000000 00000000 00001101
11111111 00000000 00000000 00001110
11111111 00000000 00000000 00001111

The address "255.0.0.16/32" specifies all addresses with a common prefix of 32 bits to the given address,
ie. just 11111111 00000000 00000000 00010000.

In total, the answer specifies the range of 10 ips starting with the address 255.0.0.7 .

There were other representations, such as:
["255.0.0.7/32","255.0.0.8/30", "255.0.0.12/30", "255.0.0.16/32"],
but our answer was the shortest possible.

Also note that a representation beginning with say, "255.0.0.7/30" would be incorrect,
because it includes addresses like 255.0.0.4 = 11111111 00000000 00000000 00000100 
that are outside the specified range.
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li><code>ip</code> will be a valid IPv4 address.</li>
<li>Every implied address <code>ip + x</code> (for <code>x < n</code>) will be a valid IPv4 address.</li>
<li><code>n</code> will be an integer in the range <code>[1, 1000]</code>.</li>
</ol>
</p>
</div>

## Tags
- Bit Manipulation (bit-manipulation)

## Companies
- Airbnb - 9 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Direct [Accepted]

**Intuition**

This problem is about performing the steps directly as written.  The tricky part is managing the bit manipulations involved.

Let's ask the question: for a number `n` of ip addresses desired, and the starting address `ip` of that range, what is the CIDR block representing the most ip addresses in that range starting at `ip`?  Evidently, this greedy approach will work, and we can keep repeating this until we are done, so let's just focus on creating one largest block.

**Algorithm**

We'll need to be able to convert `ip` addresses back and forth to integers (`long`).  We can do this with some basic manipulations - see the code for more details.

Then, with an ip address like `255.0.0.24` converted to `start`, it ends in the binary `00011000`.  There are some cases.  If `n >= 8`, then we should use the entire block `255.0.0.24/29`.  Otherwise, we can only take a number of addresses equal to the largest power of 2 less than or equal to `n`.

In a more general setting, we use the bit lengths of both `n` and `start & -start` (the lowest bit of `start`) to compute the `mask` which represents $$2^{32 - \text{mask}}$$ ip addresses.  Then, we adjust `start` and `n` appropriately.

In Java and C++, we should be careful to use `long` data types to represent the converted ip addresses, since the number could exceed $$2^{31}$$.

<iframe src="https://leetcode.com/playground/9vQkZHua/shared" frameBorder="0" width="100%" height="500" name="9vQkZHua"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ where $$N$$ is the length of `nums`.

* Space Complexity: $$O(1)$$, the space used by our `int` variables.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Solution with Very Detailed Explanation 8ms
- Author: seanmsha
- Creation Date: Thu Jul 19 2018 09:29:46 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 12:49:28 GMT+0800 (Singapore Standard Time)

<p>
This question was not easy for me even though it\'s labeled as easy. The solution may be straightforward (just try values in order), but the implementation has multiple parts and a lot of places to get stuck.  There are a lot of ways to implement it, but these are the steps I used. 

The problem is asking to get IP Addresses starting with the given start IP Address that cover n IPs.  So we will take all the ips we can get at each IP Address (at each step this is either the maximum possible (2^number of trailing bits) or if 2^number of trailing bits will make us pick more than n it is the largest (2^amount) that won\'t pick over n.  

We get our next IP by adding the value of 2^bits we took in the result for the current IP Address value.  The reason this works, is that we advance out of the amount of values we added to the result\'s range.  (for example we found 2^3 values in the iteration so we add 8 because that\'s the next ip address that we didn\'t proccess).

For the problems example:
We start at 
255.0.0.7 -> 11111111 00000000 00000000 00000111  (IP form -> Integer form) n=10
We get "255.0.0.7/32" because there are 0 leading zeroes, the range is 32- 0 bit shifted=32.  We used 1 so n = 9
255.0.0.7 + 2^0 = 255.0.0.8        255.0.0.8 -> 11111111 00000000 00000000 00001000
We get "255.0.0.8/29". The number of leading zeros is 3 so we can use 2^3 digits from this range. We shifted 1 -> 2 -> 4 -> 8 three times so the range is 32-3 bits shifted=29. We used 2^3 =8 so n = 1.
255.0.0.8 + 2^3 = 255.0.0.16
255.0.0.16->11111111 00000000 00000000 00010000 
We get "255.0.0.16/32".Theres 4 leading zero\'s but we only have n=1 remaining so we can only take 2^0 (maximum power of two less than n and less than 2^leading zeroes).  the range is 32- 0 bit shifted =32.  We used 2^0 so n=0.
255.0.0.16+2^0 = 255.0.0.17  but n=0 so we\'re done

**1. Convert the IP string into an Integer**
  The first goal is to convert the IP Address to an int so we can have an easier time processing it.  First I split the string and convert the ip formatted string into an integer.  The way I do this is splitting the string by "."s and treating each block as a digit of base 256. Now I want to convert this base 256 number into base 10 similar to how you would convert a base 2 number into base ten by multiplying each digit with 2^digit places from the end, but instead base 256 by multiplying each block with 256^ places from the end.  So the last block is multiplied by 1 (256^0) the second to last block is multiplied by 2 ( 256^1), etc. and summed (I did it the other way around by multiplying the sum instead of remultiplying each digit, but either way works).  

**2. Greedily take the biggest block of IP Addresses at each step**
As mentioned before I get the number of trailing zeros. I know that this is the maximum number of bits I\'m allowed to advance.  I start the value at 1 just so it\'s easier to calculate the 32 - used. I keep shifting it while it is less than n or the number of bits shifted is less than the maximum allowed.  If I went over the n value I have to go back one shift, because I have to take a power of 2 less than the valid amount (any more is invalid).  If the number shifted is less than the maximum allowed shifting one more time will always make it = to the maximum allowed so the current amount is fine.  You can implement this however u like; however, make sure it works in these two cases.

**3. At each step convert back into String Form**
During part 2, I have to convert the number back into string form and add the range at the end.  The range is 32- the number of bits shifted from 1 for part 2\'s bits advanced. I shift each number in blocks of 255 and & it with a 255 mask to get each block, and append a . after.  I remove the last ., and append the range at the end using string builder.

```
class Solution {
    public List<String> ipToCIDR(String ip, int n) {
        int cur = toInt(ip);
        List<String> res = new ArrayList<>();
        while(n>0){
            int maxBits = Integer.numberOfTrailingZeros(cur);
            int maxAmount = 1<<maxBits;
            int bitVal = 1;
            int count = 0;
            while(bitVal< n && count< maxBits){
                bitVal<<=1;
                ++count;
            }
            if(bitVal>n){
                bitVal>>=1;
                --count;
            }
            res.add(toString(cur,32-count));
            n-= bitVal;
            cur+=(bitVal);
        }
        return res;
    }
    private String toString(int number, int range){
        //convert every 8 into an integer
        final int WORD_SIZE = 8;
        StringBuilder sb = new StringBuilder();
        for(int i=3; i>=0; --i){
           sb.append(Integer.toString(((number>>(i*WORD_SIZE))&255)));
           sb.append(".");
       }
        sb.setLength(sb.length()-1);
        sb.append("/");
        sb.append(Integer.toString(range));
        return sb.toString();
    }
    private int toInt(String ip){
        String [] sep = ip.split("\\.");
        int sum = 0;
        for(int i=0; i<sep.length;++i){
            sum*=256;
            sum+=Integer.parseInt(sep[i]);
        }
        return sum;
    }
}
```
</p>


### [Java] This question cannot be *easy*
- Author: DyXrLxSTAOadoD
- Creation Date: Tue Jan 29 2019 13:56:08 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 09 2020 06:55:00 GMT+0800 (Singapore Standard Time)

<p>

**I don\'t think this is an easy problem, it contains much fundamental knowledge we don\'t deal with on a daily basis.**

Read about what is CIDR
https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing

You should know about one\u2019s complement(\u53CD\u7801) and two\'s complement(\u8865\u7801)
https://stackoverflow.com/questions/2604296/twos-complement-why-the-name-two
(-x) is the two\u2019s complement of x. (-x) will be equal to one\u2019s complement of x plus 1. 
for example:
```
7  in binary              :  00000111
one\u2019s complement of 7     :  11111000
two\'s complement of 7     :  11111001
x & (-x) of 7             :  00000001
```
So the value of x & (-x) can mean for 255.0.0.7, how many more ips we can represent in CIDR. In this case it\'s only 1.(```because x & (-x) of 7 is 1```)
for example, 255.0.0.8, x & (-x) of it is 00001000, it\'s 8, we can represent 8 more ips which start from it.
for 255.0.0.9, x & (-x) of it is 00000001, 1 again.
So for input: 255.0.0.7 and 10 ips, we should have results:
```
255.0.0.7:1
255.0.0.8:8
255.0.0.9:1
```
But the specification of CIDR asks us to use a mask instead of count. So we transfer it to mask, it\'s the function ```oneCIDR``` has done.
I comment some code you need to pay more attention.
```
class Solution {
  public List<String> ipToCIDR(String ip, int n) {
    long x = 0;
    String[] parts = ip.split("\\."); // we need \\ here because \'.\' is a keyword in regex.
    for(int i = 0; i < 4; i++) {
      x = x * 256 + Long.parseLong(parts[i]);
    }
    
    List<String> res = new ArrayList();
    while(n > 0) {
      long count = x & -x; 
      // this count value here means if we don\'t change the current start ip, how many
      // more ips we can represent with CIDR
      
      while(count > n) { // incase the count(mask) is too larget. For example count is 7 but we only need 2 more ips
        count /= 2;
      }
      
      res.add(oneCIDR(x, count));
      n = n - (int)count;
      x = x + (int)count;
    }
    return res;
  }
  
  private String oneCIDR(long x, long count) {
    int d, c, b, a;
    d = (int) x & 255; // Compute right-most part of ip
    x >>= 8; // throw away the right-most part of ip
    c = (int) x & 255;
    x >>= 8;
    b = (int) x & 255;
    x >>= 8;
    a = (int) x & 255;
    
    int len = 0;
	 // this while loop to know how many digits of count is in binary
	 // for example, 00001000 here the len will be 4.
    while(count > 0) {
      count /= 2;
      len++;
    }
    int mask = 32 - (len - 1);
    // Think about 255.0.0.7 -> 11111111 00000000 00000000 00000111
    // x & -x of it is 00000001, the mask is 32. (which is 32 - (1 - 1))
    
    return new StringBuilder()
      .append(a)
      .append(".")
      .append(b)
      .append(".")
      .append(c)
      .append(".")
      .append(d)
      .append("/")
      .append(mask)
      .toString();
  }
}
```
</p>


### Very Simple Java Solution (beat 100%)
- Author: DemonSong
- Creation Date: Sun Dec 24 2017 15:49:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 24 2017 15:49:04 GMT+0800 (Singapore Standard Time)

<p>
The idea refer to @cuiaoxiang 


```java
	public List<String> ipToCIDR(String ip, int range) {
		long x = 0;
		String[] ips = ip.split("\\.");
		for (int i = 0; i < ips.length; ++i) {
			x = Integer.parseInt(ips[i]) + x * 256;
		}
		
		List<String> ans = new ArrayList<>();
		while (range > 0) {
			long step = x & -x;
			while (step > range) step /= 2;
			ans.add(longToIP(x, (int)step));
			x += step;
			range -= step;
		}
		
		return ans;
	}
	
	String longToIP(long x, int step) {
		int[] ans = new int[4];
		ans[0] = (int) (x & 255); x >>= 8;
		ans[1] = (int) (x & 255); x >>= 8;
		ans[2] = (int) (x & 255); x >>= 8;
		ans[3] = (int) x;
		int len = 33;
		while (step > 0) {
			len --;
			step /= 2;
		}
		return ans[3] + "." + ans[2] + "." + ans[1] + "." + ans[0] + "/" + len;
	}
```
</p>


