---
title: "Maximal Network Rank"
weight: 1615
#id: "maximal-network-rank"
---
## Description
<div class="description">
<p>There is an infrastructure of <code>n</code> cities with some number of <code>roads</code> connecting these cities. Each <code>roads[i] = [a<sub>i</sub>, b<sub>i</sub>]</code> indicates that there is a bidirectional road between cities <code>a<sub>i</sub></code> and <code>b<sub>i</sub></code>.</p>

<p>The <strong>network rank</strong><em> </em>of <strong>two different cities</strong> is defined as the total number of&nbsp;<strong>directly</strong> connected roads to <strong>either</strong> city. If a road is directly connected to both cities, it is only counted <strong>once</strong>.</p>

<p>The <strong>maximal network rank </strong>of the infrastructure is the <strong>maximum network rank</strong> of all pairs of different cities.</p>

<p>Given the integer <code>n</code> and the array <code>roads</code>, return <em>the <strong>maximal network rank</strong> of the entire infrastructure</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/09/21/ex1.png" style="width: 292px; height: 172px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 4, roads = [[0,1],[0,3],[1,2],[1,3]]
<strong>Output:</strong> 4
<strong>Explanation:</strong> The network rank of cities 0 and 1 is 4 as there are 4 roads that are connected to either 0 or 1. The road between 0 and 1 is only counted once.
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/09/21/ex2.png" style="width: 292px; height: 172px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 5, roads = [[0,1],[0,3],[1,2],[1,3],[2,3],[2,4]]
<strong>Output:</strong> 5
<strong>Explanation:</strong> There are 5 roads that are connected to cities 1 or 2.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 8, roads = [[0,1],[1,2],[2,3],[2,4],[5,6],[5,7]]
<strong>Output:</strong> 5
<strong>Explanation:</strong> The network rank of 2 and 5 is 5. Notice that all the cities do not have to be connected.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 100</code></li>
	<li><code>0 &lt;= roads.length &lt;= n * (n - 1) / 2</code></li>
	<li><code>roads[i].length == 2</code></li>
	<li><code>0 &lt;= a<sub>i</sub>, b<sub>i</sub>&nbsp;&lt;= n-1</code></li>
	<li><code>a<sub>i</sub>&nbsp;!=&nbsp;b<sub>i</sub></code></li>
	<li>Each&nbsp;pair of cities has <strong>at most one</strong> road connecting them.</li>
</ul>

</div>

## Tags
- Graph (graph)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java simple O(n^2)
- Author: hobiter
- Creation Date: Sun Oct 11 2020 12:02:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2020 12:42:26 GMT+0800 (Singapore Standard Time)

<p>
Use a boolean[][] to mark if connected directly.
Keep in mind if there is a line directly connects i and j, you will count roads[i, j] twice, so you have to remove 1;
Just use brutal force to check all pairs of i, j, where i != j, and find the max;


```
    public int maximalNetworkRank(int n, int[][] roads) {
        boolean[][] connected = new boolean[n][n];
        int[] cnts = new int[n];
        for (int[] r : roads) {
            cnts[r[0]]++;
            cnts[r[1]]++;
            connected[r[0]][r[1]] = true;
            connected[r[1]][r[0]] = true;  // cache if i and j directly connected
        }
        int res = 0;
        for (int i = 0; i < n; i++) 
            for (int j = i + 1; j < n; j++) 
                res = Math.max(res, cnts[i] + cnts[j] - (connected[i][j] ? 1 : 0));  // loop all pairs
        return res;
    }
```

</p>


### C++ O(n * n) -> O(m) average
- Author: votrubac
- Creation Date: Sun Oct 11 2020 12:09:13 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2020 12:47:15 GMT+0800 (Singapore Standard Time)

<p>
Count number of roads for each node. Also, track if two nodes are directly connected in `adj`.

Then, find two nodes that have the biggest sum of roads, minus one if they are adjacent.

```cpp
int maximalNetworkRank(int n, vector<vector<int>>& roads) {
    vector<int> cnt(n);
    size_t res = 0;
    set<pair<int, int>> adjacent;
    for (auto r : roads) {
        ++cnt[r[0]];
        ++cnt[r[1]];
        adjacent.insert({min(r[0], r[1]), max(r[0], r[1])});
    }
    for (auto i = 0; i < n; ++i)
        for (auto j = i + 1; j < n; ++j)
            res = max(res, cnt[i] + cnt[j] - adjacent.count({i, j}));
    return res;
}
```

We can optimize the solution for the *average* case by sorting the nodes by the number of roads, so we can exit the second loop as soon as the sum of roads drops below the current maximum. This will improve the runtime from quadratic to linearithmic. Based on the OJ test cases, we do not need more than 5 nodes with the biggest number of roads, so we can achieve O(m), where m is the number of roads, if using partial sort:
- Partial sort is O(n log 5) -> O(n)
- Finding maximum is O(5 * 5) -> O(1)

```cpp
int maximalNetworkRank(int n, vector<vector<int>>& roads) {
    vector<int> cnt(n), idx(n);
    iota(begin(idx), end(idx), 0);
    size_t res = 0;
    unordered_set<int> adj;
    for (auto r : roads) {
        ++cnt[r[0]];
        ++cnt[r[1]];
        adj.insert((min(r[0], r[1]) << 16) + max(r[0], r[1]));
    }
    partial_sort(begin(idx), begin(idx) + min(5, (int)idx.size()), end(idx), [&](int i, int j) {return cnt[i] > cnt[j]; });
    for (auto i = 0; i < n; ++i) {
        for (auto j = i + 1; j < n; ++j) {
            if (res > cnt[idx[i]] + cnt[idx[j]])
                break;
            res = max(res, cnt[idx[i]] + cnt[idx[j]] -
                adj.count((min(idx[i], idx[j]) << 16) + max(idx[i], idx[j])));
        }
    }
    return res;
}
```
</p>


### [C++] Straightforward (Graph) with Brief Explanation, O(n²) Runtime
- Author: amanmehara
- Creation Date: Sun Oct 11 2020 13:02:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2020 13:11:00 GMT+0800 (Singapore Standard Time)

<p>
**Brief Explanation**
* Build the graph (adjacency list).
* Compute network rank of all pairs of cities.
	* Simply compute the sum of neighbors (connected cities) of both cities.
	* Decrement 1 from the computed sum if the cities under consideration are connected to each other.
       *(Otherwise the road between those 2 cities will be double counted)*
	* Update the ***maximal network rank*** if the ***network rank*** computed is greater than the current value. 

***Note:***
Used `unordered_set` in the graph to track neighbors (connected cities) in order to speed up the look up.
(Required to check if the 2 cities under consideration are connected or not)

```
class Solution {
public:
    int maximalNetworkRank(int n, vector<vector<int>>& roads) {
        vector<unordered_set<int>> graph(n);
        
        // Building the graph (adjacency list). 
        for (const auto& road: roads) {
            graph[road[0]].insert(road[1]);
            graph[road[1]].insert(road[0]);
        }
        
        int maximal = 0;
        for(int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                // Sum of neighbors (connected cities) of both cities. 
                int network_rank = graph[i].size() + graph[j].size();
                
                // Reduce the rank by 1 in case the cities are connected to each other.
                if (graph[j].count(i)) {
                    --network_rank;
                }
                
                // Maximal network rank is the maximum network rank possible.
                maximal = max(maximal, network_rank);
            }
            
        }
        return maximal;
    }
};
```
</p>


