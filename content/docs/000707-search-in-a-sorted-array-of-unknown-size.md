---
title: "Search in a Sorted Array of Unknown Size"
weight: 707
#id: "search-in-a-sorted-array-of-unknown-size"
---
## Description
<div class="description">
<p>Given an&nbsp;integer array sorted in ascending order, write a function to search <code>target</code> in <code>nums</code>.&nbsp; If <code>target</code> exists, then return its index, otherwise return <code>-1</code>. <strong>However, the array size is unknown to you</strong>. You may only access the array using an <code>ArrayReader</code>&nbsp;interface, where&nbsp;<code>ArrayReader.get(k)</code> returns the element of the array at index <code>k</code>&nbsp;(0-indexed).</p>

<p>You may assume all integers in the array are less than&nbsp;<code>10000</code>, and if you access the array out of bounds, <code>ArrayReader.get</code> will return <code>2147483647</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> <code>array</code> = [-1,0,3,5,9,12], <code>target</code> = 9
<strong>Output:</strong> 4
<strong>Explanation:</strong> 9 exists in <code>nums</code> and its index is 4
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> <code>array</code> = [-1,0,3,5,9,12], <code>target</code> = 2
<strong>Output:</strong> -1
<strong>Explanation:</strong> 2 does not exist in <code>nums</code> so return -1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>You may assume that all elements in the array are unique.</li>
	<li>The value of each element in the array&nbsp;will be in the range <code>[-9999, 9999]</code>.</li>
	<li>The length of the array will be in the range <code>[1, 10^4]</code>.</li>
</ul>

</div>

## Tags
- Binary Search (binary-search)

## Companies
- Microsoft - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Binary Search 

**Split into Two Subproblems**

The array is sorted, i.e. one could 
try to fit into a logarithmic time complexity.
That means two subproblems, and both should be done
in a logarithmic time:

- Define search limits, i.e. left and right boundaries for the
search.

- Perform binary search in the defined boundaries.

![limits](../Figures/702/way.png)

**Define Search Boundaries**

This is a key subproblem here. 

The idea is quite simple.
Let's take two first indexes, 0 and 1, as left and right boundaries. 
If the target value is not among these zeroth and the first element, 
then it's outside the boundaries, on the right. 

That means that the left boundary could moved to the right,
and the right boundary should be extended. To keep logarithmic time
complexity, let's extend it twice as far: `right = right * 2`. 

![limits](../Figures/702/limits.png)

If the target now is less than the right element, we're done, 
the boundaries are set. If not, repeat these two steps till the
boundaries are established:

- Move the left boundary to the right: `left = right`.

- Extend the right boundary: `right = right * 2`.  

![limits](../Figures/702/done.png)

**Binary Search**

[Binary search](https://en.wikipedia.org/wiki/Binary_search_algorithm)
is a textbook algorithm with a logarithmic time complexity.
It's based on the idea to 
compare the target value to the middle element of the array.

- If the target value is equal to the middle element - we're done.

- If the target value is smaller - continue to search on the left.

- If the target value is larger - continue to search on the right.

![limits](../Figures/702/binary2.png)

**Prerequisites: left and right shifts**

To speed up, one could use here [bitwise shifts](https://wiki.python.org/moin/BitwiseOperators):

- Left shift: `x << 1`. The same as multiplying by 2: `x * 2`.

- Right shift: `x >> 1`. The same as dividing by 2: `x / 2`.

**Algorithm**

Define boundaries:

- Initiate `left = 0` and `right = 1`.

- While target is on the right to the right boundary: `reader.get(right) < target`:

    - Set left boundary equal to the right one: `left = right`.
    
    - Extend right boundary: `right *= 2`. To speed up, use right shift
    instead of multiplication: `right <<= 1`. 
    
- Now the target is between left and right boundaries. 

Binary Search:

- While `left <= right`:

    - Pick a pivot index in the middle: `pivot = (left + right) / 2`.
    To avoid overflow, use the form `pivot = left + ((right - left) >> 1)`
    instead of straightforward expression above. 
     
    - Retrieve the element at this index: `num = reader.get(pivot)`.  
    
    - Compare middle element `num` to the target value.
    
        - If the middle element _is_ the target `num == target`: 
        return `pivot`. 
        
        - If the target is not yet found: 
        
            - If `num > target`, continue to search on the left 
            `right = pivot - 1`.
            
            - Else continue to search on the right 
            `left = pivot + 1`.
    
- We're here because target is not found. Return -1. 
            
**Implementation**



<iframe src="https://leetcode.com/playground/DhHUeQWM/shared" frameBorder="0" width="100%" height="500" name="DhHUeQWM"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(\log T)$$, where $$T$$ is an index of 
target value. 

    There are two operations here: to define search boundaries and 
    to perform binary search. 
    
    Let's first find the number of steps k
    to setup the boundaries. On the first step, the boundaries are
    $$2^0 .. 2^{0 + 1}$$, on the second step $$2^1 .. 2^{1 + 1}$$, etc. 
    When everything is done, the boundaries are $$2^k .. 2^{k + 1}$$
    and $$2^k < T \le 2^{k + 1}$$. That means one needs $$k = \log T$$
    steps to setup the boundaries, that means $$\mathcal{O}(\log T)$$
    time complexity.  
    
    Now let's discuss the complexity of the binary search.
    There are $$2^{k + 1} - 2^k = 2^k$$ elements in the boundaries,
    i.e. $$2^{\log T} = T$$ elements. 
    [As discussed](https://leetcode.com/articles/binary-search/),
    binary search has logarithmic complexity, that results in
    $$\mathcal{O}(\log T)$$ time complexity.  
     
* Space complexity : $$\mathcal{O}(1)$$ since it's a constant space
solution.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Shortest and cleanest Java solution so far...
- Author: touchdown
- Creation Date: Fri Jul 20 2018 09:57:47 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 11:18:29 GMT+0800 (Singapore Standard Time)

<p>
To use binary search, we need to find the search space defined by `low` and `hi`. Find `hi` by moving `hi` exponentially. Once `hi` is found, `low` is previous `hi`. Then do binary search.
		
	public int search(ArrayReader reader, int target) {
        int hi = 1;
        while (reader.get(hi) < target) {
            hi = hi << 1;
        }
        int low = hi >> 1;
        while (low <= hi) {
            int mid = low+(hi-low)/2;
            if (reader.get(mid) > target) {
                hi = mid-1;
            } else if (reader.get(mid) < target) {
                low = mid+1;
            } else {
                return mid;
            }
        }
        return -1;
    }
</p>


### Straight forward binary search. 
- Author: jingjing_334
- Creation Date: Tue Sep 18 2018 18:34:14 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 26 2018 22:08:08 GMT+0800 (Singapore Standard Time)

<p>
```
public int search(ArrayReader reader, int target) {
        int s=0, e=Integer.MAX_VALUE;
        while (s<=e) {
            int mid=(s+e)/2; 
            int x = reader.get(mid);
            if (x==Integer.MAX_VALUE || x>target)
                e=mid-1; 
            else if (x<target) 
                s=mid+1; 
            else 
                return mid; 
        }
        return -1; 
    }
```
</p>


### Python Binary Search - How to design for a specific case
- Author: WangQiuc
- Creation Date: Wed Mar 20 2019 05:32:29 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 09 2019 12:39:31 GMT+0800 (Singapore Standard Time)

<p>
As the sorted array has an unknown size, first we need to find the search scope.
We can use a \'binary search\' to find the scope: ```while reader.get(hi) < target: hi <<= 1``` .

Here is the first decision, to choose ```reader.get(hi) < target``` or ```reader.get(hi) <= target```.
If we choose the first one, then search scope is ```(hi/2, hi]```, otherwise ```[hi/2, hi)```. I choose the 1st one since the search scope could be half size of that of the 2nd one if target == hi.

Then we can perform the binary search and there are 4 things to decide:
1. loop condition: ```lo < hi``` or ```lo <= hi```
2. ```lo``` and ```hi``` update: ```lo = mid + 1 or mid```, ```hi= mid - 1 or mid```
3. Should loop break when ```target == reader.get(mid)```?
4. When loop ends at ```lo < hi``` or ```lo <= hi```, what should we return?

In this problem, I want to finish my search in the loop. So if I leave loop without finding the target, I know target is not in the array and I can just return -1. This is for **decision #4**.

Because of decison #4 and search scope being ```(lo, hi]```, if we pick ```lo < hi``` for decision #1, when target is at ```hi,``` last loop would be ```lo = mid = hi-1```. So ```reader.get(mid) < target = reader.get(hi)```. Then I should update ```lo``` as ```lo = mid + 1``` otherwise it will be a dead loop. If we break out here as ```lo == hi```, we will miss checking index ```hi``` and return -1. Thus, we need to choose ```lo <= hi``` for **decision #1**. 
Besides, if we choose search scope as ```[lo, hi)```, we can end the loop when ```lo == hi```.

For **decision #2**, we need to update as ```lo = mid + 1```, ```hi= mid - 1``` because of **decision #1**. Otherwise when target is not in array, we would stuck in loop when ```lo == hi```.

Finally as all elements in the array are distinct so there is only one valid index to return, we could just return the index when ```target == reader.get(index)``` for **decision #3**. 
Such return, however, would give an incorrect ansewer for some problems, such as [#668](https://leetcode.com/problems/kth-smallest-number-in-multiplication-table/discuss/262279/Python-Binary-Search-Need-to-Return-the-Smallest-Candidate). In that problem, there is a range of valid indices and we need to return a border index of that range. For example, the array is ```[1,3,3,3,5] ```and target is ```3```. The valid index range is ```[1,3]``` and need to return the left border index which is ```1```. In such case, we can\'t return when ```target == array[mid]``` but update as ```hi = mid``` and keep searching until ```lo > hi```.

As you can see, there are quite a few tricky things in a binary search. You need to design carefully for each case. 
```
def search(reader, target):
	hi = 1
	while reader.get(hi) < target: hi <<= 1
	lo = hi >> 1
	while lo <= hi:
		mid = lo + hi >> 1
		if reader.get(mid) < target: lo = mid + 1
		elif reader.get(mid) > target: hi = mid - 1
		else: return mid
	return -1
```
</p>


