---
title: "Sort Transformed Array"
weight: 343
#id: "sort-transformed-array"
---
## Description
<div class="description">
<p>Given a <b>sorted</b> array of integers <i>nums</i> and integer values <i>a</i>, <i>b</i> and <i>c</i>. Apply a quadratic function of the form f(<i>x</i>) = <i>ax</i><sup>2</sup> + <i>bx</i> + <i>c</i> to each element <i>x</i> in the array.</p>

<p>The returned array must be in <b>sorted order</b>.</p>

<p>Expected time complexity: <b>O(<i>n</i>)</b></p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>nums = <span id="example-input-1-1">[-4,-2,2,4]</span>, a = <span id="example-input-1-2">1</span>, b = <span id="example-input-1-3">3</span>, c = <span id="example-input-1-4">5</span>
<strong>Output: </strong><span id="example-output-1">[3,9,15,33]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>nums = <span id="example-input-2-1">[-4,-2,2,4]</span>, a = <span id="example-input-2-2">-1</span>, b = <span id="example-input-2-3">3</span>, c = <span id="example-input-2-4">5</span>
<strong>Output: </strong><span id="example-output-2">[-23,-5,1,7]</span>
</pre>
</div>
</div>
</div>

## Tags
- Math (math)
- Two Pointers (two-pointers)
- Sort (sort)

## Companies
- Google - 3 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(n) incredibly short yet easy to understand AC solution
- Author: xuyirui
- Creation Date: Fri Jun 17 2016 11:49:33 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 08:19:19 GMT+0800 (Singapore Standard Time)

<p>
the problem seems to have many cases a>0, a=0,a<0, (when a=0, b>0, b<0). However, they can be combined into just 2 cases: a>0 or a<0

1.a>0, two ends in original array are bigger than center if you learned middle school math before.

2.a<0, center is bigger than two ends.

so use two pointers i, j and do a merge-sort like process. depending on sign of a, you may want to start from the beginning or end of the transformed array. For a==0 case, it does not matter what b's sign is.
The function is monotonically increasing or decreasing. you can start with either beginning or end.



    public class Solution {
        public int[] sortTransformedArray(int[] nums, int a, int b, int c) {
            int n = nums.length;
            int[] sorted = new int[n];
            int i = 0, j = n - 1;
            int index = a >= 0 ? n - 1 : 0;
            while (i <= j) {
                if (a >= 0) {
                    sorted[index--] = quad(nums[i], a, b, c) >= quad(nums[j], a, b, c) ? quad(nums[i++], a, b, c) : quad(nums[j--], a, b, c);
                } else {
                    sorted[index++] = quad(nums[i], a, b, c) >= quad(nums[j], a, b, c) ? quad(nums[j--], a, b, c) : quad(nums[i++], a, b, c);
                }
            }
            return sorted;
        }
        
        private int quad(int x, int a, int b, int c) {
            return a * x * x + b * x + c;
        }
    }
</p>


### Python O(n) Two-Pointers Solution
- Author: waigx
- Creation Date: Fri Jun 17 2016 16:19:25 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 23:08:39 GMT+0800 (Singapore Standard Time)

<p>
*d* is the increment of *i*.

    def sortTransformedArray(self, nums, a, b, c):
        nums = [x*x*a + x*b + c for x in nums]
        ret = [0] * len(nums)
        p1, p2 = 0, len(nums) - 1
        i, d = (p1, 1) if a < 0 else (p2, -1)
        while p1 <= p2:
            if nums[p1] * -d > nums[p2] * -d:
                ret[i] = nums[p1]
                p1 += 1
            else:
                ret[i] = nums[p2]
                p2 -=1
            i += d
        return ret
</p>


### My easy to understand Java AC solution using Two pointers
- Author: Tonkotsu_Ramen
- Creation Date: Fri Jul 08 2016 01:32:44 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 10 2018 10:55:18 GMT+0800 (Singapore Standard Time)

<p>
The idea is simple: 
For a parabola, 
if a >= 0, the min value is at its vertex. So our our sorting should goes from two end points towards the vertex, high to low.
if a < 0, the max value is at its vertex. So our sort goes the opposite way.

```
public class Solution {
    public int[] sortTransformedArray(int[] nums, int a, int b, int c) {
        int[] res = new int[nums.length];
        int start = 0;
        int end = nums.length - 1;
        int i = a >= 0 ? nums.length - 1 : 0;
        while(start <= end) {
            int startNum = getNum(nums[start], a, b, c);
            int endNum = getNum(nums[end], a, b, c);
            if(a >= 0) {
                if(startNum >= endNum) {
                    res[i--] = startNum;
                    start++;
                }
                else{
                    res[i--] = endNum;
                    end--;
                }
            }
            else{
                if(startNum <= endNum) {
                    res[i++] = startNum;
                    start++;
                }
                else{
                    res[i++] = endNum;
                    end--;
                }
            }
        }
        return res;
    }
    public int getNum(int x, int a, int b, int c) {
        return a * x * x + b * x + c;
    }
}
```
</p>


