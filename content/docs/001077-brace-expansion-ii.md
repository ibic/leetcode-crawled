---
title: "Brace Expansion II"
weight: 1077
#id: "brace-expansion-ii"
---
## Description
<div class="description">
<p>Under a grammar given below, strings can represent a set of lowercase words.&nbsp; Let&#39;s&nbsp;use <code>R(expr)</code>&nbsp;to denote the <strong>set</strong> of words the expression represents.</p>

<p>Grammar can best be understood through simple examples:</p>

<ul>
	<li>Single letters represent a singleton set containing that word.
	<ul>
		<li><code>R(&quot;a&quot;) = {&quot;a&quot;}</code></li>
		<li><code>R(&quot;w&quot;) = {&quot;w&quot;}</code></li>
	</ul>
	</li>
	<li>When we take a comma delimited list of 2 or more expressions, we take the union of possibilities.
	<ul>
		<li><code>R(&quot;{a,b,c}&quot;) = {&quot;a&quot;,&quot;b&quot;,&quot;c&quot;}</code></li>
		<li><code>R(&quot;{{a,b},{b,c}}&quot;) = {&quot;a&quot;,&quot;b&quot;,&quot;c&quot;}</code>&nbsp;(notice the final set only contains each word at most once)</li>
	</ul>
	</li>
	<li>When we concatenate two expressions, we take the set of possible concatenations between two words where the first word comes from the first expression and the second word comes from the second expression.
	<ul>
		<li><code>R(&quot;{a,b}{c,d}&quot;) = {&quot;ac&quot;,&quot;ad&quot;,&quot;bc&quot;,&quot;bd&quot;}</code></li>
		<li><code>R(&quot;a{b,c}{d,e}f{g,h}&quot;)&nbsp;= {&quot;abdfg&quot;, &quot;abdfh&quot;, &quot;abefg&quot;, &quot;abefh&quot;, &quot;acdfg&quot;, &quot;acdfh&quot;, &quot;acefg&quot;, &quot;acefh&quot;}</code></li>
	</ul>
	</li>
</ul>

<p>Formally, the 3 rules for our grammar:</p>

<ul>
	<li>For every lowercase letter <code>x</code>, we have <code>R(x) = {x}</code></li>
	<li>For expressions <code>e_1, e_2, ... , e_k</code>&nbsp;with <code>k &gt;= 2</code>, we have <code>R({e_1,e_2,...}) = R(e_1)&nbsp;&cup; R(e_2)&nbsp;&cup; ...</code></li>
	<li>For&nbsp;expressions <code>e_1</code> and <code>e_2</code>, we have <code>R(e_1 + e_2) = {a + b for (a, b) in&nbsp;R(e_1)&nbsp;&times; R(e_2)}</code>, where + denotes concatenation, and &times; denotes the cartesian product.</li>
</ul>

<p>Given an <code>expression</code> representing a set of words under the given grammar, return the&nbsp;sorted list of words that the expression represents.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;{a,b}{c,{d,e}}&quot;</span>
<strong>Output: </strong><span id="example-output-1">[&quot;ac&quot;,&quot;ad&quot;,&quot;ae&quot;,&quot;bc&quot;,&quot;bd&quot;,&quot;be&quot;]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span>&quot;{{a,z},a{b,c},{ab,z}}&quot;</span>
<strong>Output: </strong><span>[&quot;a&quot;,&quot;ab&quot;,&quot;ac&quot;,&quot;z&quot;]</span>
<strong>Explanation: </strong>Each distinct word is written only once in the final answer.
</pre>

<p>&nbsp;</p>

<p><strong>Constraints:</strong></p>

<ol>
	<li><code>1 &lt;= expression.length &lt;= 60</code></li>
	<li><code>expression[i]</code> consists of <code>&#39;{&#39;</code>, <code>&#39;}&#39;</code>, <code>&#39;,&#39;</code>or lowercase English letters.</li>
	<li>The given&nbsp;<code>expression</code>&nbsp;represents a set of words based on the grammar given in the description.</li>
</ol>
</div>
</div>

</div>

## Tags
- String (string)

## Companies
- Google - 2 (taggedByAdmin: true)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python3 Clear and Short Recursive Solution
- Author: jinjiren
- Creation Date: Sun Jun 23 2019 12:09:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 23 2019 13:00:42 GMT+0800 (Singapore Standard Time)

<p>
# The general idea
**Split expressions into groups separated by top level `\',\'`; for each top-level sub expression (substrings with braces), process it and add it to the corresponding group; finally combine the groups and sort.**
## Thought process
- in each call of the function, try to remove one level of braces
- maintain a list of groups separated by top level `\',\'`
	- when meets `\',\'`: create a new empty group as the current group
	- when meets `\'{\'`: check whether current `level` is 0, if so, record the starting index of the sub expression; 
		- always increase `level` by 1, no matter whether current level is 0
	- when meets `\'}\'`: decrease `level` by 1; then check whether current `level` is 0, if so, recursively call `braceExpansionII` to get the set of words from `expresion[start: end]`, where `end` is the current index (exclusive).
		- add the expanded word set to the current group
	- when meets a letter: check whether the current `level` is 0, if so, add `[letter]` to the current group
	- **base case: when there is no brace in the expression.**
- finally, after processing all sub expressions and collect all groups (seperated by `\',\'`), we initialize an empty word_set, and then iterate through all groups
	- for each group, find the product of the elements inside this group
	- compute the union of all groups
- sort and return
- note that `itertools.product(*group)` returns an iterator of **tuples** of characters, so we use` \'\'.join()` to convert them to strings

```py
class Solution:
    def braceExpansionII(self, expression: str) -> List[str]:
        groups = [[]]
        level = 0
        for i, c in enumerate(expression):
            if c == \'{\':
                if level == 0:
                    start = i+1
                level += 1
            elif c == \'}\':
                level -= 1
                if level == 0:
                    groups[-1].append(self.braceExpansionII(expression[start:i]))
            elif c == \',\' and level == 0:
                groups.append([])
            elif level == 0:
                groups[-1].append([c])
        word_set = set()
        for group in groups:
            word_set |= set(map(\'\'.join, itertools.product(*group)))
        return sorted(word_set)
```
</p>


### Python concise stack solution
- Author: yuanzhi247012
- Creation Date: Fri Jun 28 2019 16:34:41 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 28 2019 16:44:05 GMT+0800 (Singapore Standard Time)

<p>
Use stack for "{" and "}" just like in calculator.
Maintain two lists:
1. the previous list before ",", 
2. the current list that is still growing.

When seeing an "alphabet", grow the second list by corss multiplying. So that [a]*b will become "ab", [a,c]*b will become ["ab", "cb"]
When seeing ",", that means the second list can\'t grow anymore. combine it with the first list and clear the second list.
When seeing "{", push the two lists into stack, 
When seeing "}", pop the previous two lists, cross multiplying the result inside "{ }" into the second list.

If not considering the final sorting before return, the time complexity should be O(n)

```
class Solution:
    def braceExpansionII(self, expression: str) -> List[str]:
        stack,res,cur=[],[],[]
        for i in range(len(expression)):
            v=expression[i]
            if v.isalpha():
                cur=[c+v for c in cur or [\'\']]
            elif v==\'{\':
                stack.append(res)
                stack.append(cur)
                res,cur=[],[]
            elif v==\'}\':
                pre=stack.pop()
                preRes=stack.pop()
                cur=[p+c for c in res+cur for p in pre or [\'\']]
                res=preRes
            elif v==\',\':
                res+=cur
                cur=[]
        return sorted(set(res+cur))
```
</p>


### JAVA iter_dfs, 36ms
- Author: DIMITR
- Creation Date: Thu Aug 01 2019 14:51:00 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 01 2019 14:51:00 GMT+0800 (Singapore Standard Time)

<p>
```
    Set<String> set = new HashSet<String>();
    StringBuilder sb = new StringBuilder();
    List<String> res = new ArrayList<String>();
    Stack<String> stack = new Stack<String>();

    public List<String> braceExpansionII(String expression) {
        stack.push(expression);
        iter_dfs();
        Collections.sort(res);
        return res;
    }

    private void iter_dfs(){
        while(!stack.isEmpty()) {
            String str = stack.pop();
            if (str.indexOf(\'{\') == -1) {
                if (!set.contains(str)) {
                    res.add(str);
                    set.add(str);
                }
                continue;
            }

            int i = 0, l = 0, r = 0;
            while (str.charAt(i) != \'}\') {
                if (str.charAt(i++) == \'{\')
                    l = i - 1;
            }
            r = i;

            String before = str.substring(0, l);
            String after = str.substring(r + 1, str.length());
            String[] args = str.substring(l + 1, r).split(",");

            for (String s : args) {
                sb.setLength(0);
                stack.push(sb.append(before).append(s).append(after).toString());
            }
        }
    }
```
</p>


