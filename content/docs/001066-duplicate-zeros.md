---
title: "Duplicate Zeros"
weight: 1066
#id: "duplicate-zeros"
---
## Description
<div class="description">
<p>Given a fixed length&nbsp;array <code>arr</code> of integers, duplicate each occurrence of zero, shifting the remaining elements to the right.</p>

<p>Note that elements beyond the length of the original array are not written.</p>

<p>Do the above modifications to the input array <strong>in place</strong>, do not return anything from your function.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[1,0,2,3,0,4,5,0]</span>
<strong>Output: </strong>null
<strong>Explanation: </strong>After calling your function, the <strong>input</strong> array is modified to: <span id="example-output-1">[1,0,0,2,3,0,0,4]</span>
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[1,2,3]</span>
<strong>Output: </strong>null
<strong>Explanation: </strong>After calling your function, the <strong>input</strong> array is modified to: <span id="example-output-2">[1,2,3]</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= arr.length &lt;= 10000</code></li>
	<li><code>0 &lt;= arr[i] &lt;= 9</code></li>
</ol>
</div>

## Tags
- Array (array)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

The problem demands the array to be modified in-place. If `in-place` was not a constraint we might have just copied the elements from a source array to a destination array.

<center>
<img src="../Figures/1089/1089_Duplicate_Zeros_1.png" width="600"/>
</center>
<br>
Notice, how we copied zero twice.

```
  s = 0
  d = 0

  # Copy is performed until the destination array is full.
  for s in range(N):
    if source[s] == 0:
      # Copy zero twice.
      destination[d] = 0
      d += 1
      destination[d] = 0
    else:
      destination[d] = source[s]

    d += 1
```

The problem statement also mentions that we do not grow the new array, rather we just trim it to its original array length. This means we have to discard some elements from the end of the array. These are the elements whose new indices are beyond the length of the original array.

<center>
<img src="../Figures/1089/1089_Duplicate_Zeros_2.png" width="600"/>
</center>

Let's remind ourselves about the problem constraint that we are given. Since we can't use extra space, our source and destination array is essentially the same. We just can't go about copying the source into destination array the same way. If we do that we would lose some elements. Since, we would be overwriting the array.

<center>
<img src="../Figures/1089/1089_Duplicate_Zeros_3.png" width="600"/>
</center>

Keeping this in mind, in the approach below we start copying to the end of the array.

#### Approach 1: Two pass, O(1) space

**Intuition**

If we know the number of elements which would be discarded from the end of the array, we can copy the rest. How do we find out how many elements would be discarded in the end? The number would be equal to the number of extra zeros which would be added to the array. The extra zero would create space for itself by pushing out an element from the end of the array.

Once we know how many elements from the original array would be part of the final array, we can just start copying from the end. Copying from the end ensures we don't lose any element since, the last few extraneous elements can be overwritten.

**Algorithm**

1. Find the number of zeros which would be duplicated. Let's call it `possible_dups`. We do need to make sure we are not counting the zeros which would be trimmed off. Since, the discarded zeros won't be part of the final array. The count of `possible_dups` would give us the number of elements to be trimmed off the original array. Hence at any point, `length_ - possible_dups` is the number of elements which would be included in the final array.
    <center>
    <img src="../Figures/1089/1089_Duplicate_Zeros_4.png" width="600"/>
    </center>
    <br>
    Note: In the diagram above we just show source and destination array for understanding purpose. We will be doing these operations only on one array.

2. Handle the edge case for a zero present on the boundary of the leftover elements.

    Let's talk about the edge case of this problem. We need to be extra careful when we are duplicating the zeros in the leftover array. This care should be taken for the `zero` which is lying on the boundary. Since, this zero might be counted as with possible duplicates, or may be just got included in the left over when there was no space left to accommodate its duplicate. If it is part of the `possible_dups` we would want to duplicate it otherwise we don't.

    > An example of the edge case is - [8,4,5,0,0,0,0,7].
    In this array there is space to accommodate the duplicates of first and second occurrences of zero. But we don't have enough space for the duplicate of the third occurrence of zero.
    Hence when we are copying we need to make sure for the third occurrence we don't copy twice. Result = [8,4,5,0,`0`,0,`0`,0]

3. Iterate the array from the end and copy a non-zero element once and zero element twice.
When we say we discard the extraneous elements, it simply means we start from the left of the extraneous elements and start overwriting them with new values, eventually right shifting the left over elements and creating space for all the duplicated elements in the array.

<center>
<img src="../Figures/1089/1089_Duplicate_Zeros_5.png" width="500"/>
</center>
<br>

<iframe src="https://leetcode.com/playground/3wUwdzEP/shared" frameBorder="0" width="100%" height="500" name="3wUwdzEP"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of elements in the array. We do two passes through the array, one to find the number of `possible_dups` and the other to copy the elements. In the worst case we might be iterating the entire array, when there are less or no zeros in the array.

* Space Complexity: $$O(1)$$. We do not use any extra space.

<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java] Two Pointers, Space O(1)
- Author: lee215
- Creation Date: Sun Jun 16 2019 12:07:46 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 10 2019 20:02:05 GMT+0800 (Singapore Standard Time)

<p>
# **Intuition**
The problem can be easy solved:
1. with a copy (extra space)
2. by inserting zeros (extra time)
<br>

**Python, with extra space**
```
    def duplicateZeros(self, A):
        A[:] = [x for a in A for x in ([a] if a else [0, 0])][:len(A)]
```
<br>

# **Explanation**
We can improve it to `O(N)` time and `O(1)` space.
Basically, we apply two pointers.
`i` is the position in the original array,
`j` is the position in the new array.
(the original and the new are actually the same array.)

The first we pass forward and count the zeros.
The second we pass backward and assign the value from original input to the new array.
so that the original value won\'t be overridden too early.
<br>

**C++:**
```
    void duplicateZeros(vector<int>& A) {
        int n = A.size(), j = n + count(A.begin(), A.end(), 0);
        for (int i = n - 1; i >= 0; --i) {
            if (--j < n)
                A[j] = A[i];
            if (A[i] == 0 && --j < n)
                A[j] = 0;
        }
    }
```

**Java**
Version suggested by @davidluoyes
```java
    public void duplicateZeros(int[] arr) {
        int countZero = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) countZero++;
        }
        int len = arr.length + countZero;
        //We just need O(1) space if we scan from back
        //i point to the original array, j point to the new location
        for (int i = arr.length - 1, j = len - 1; i < j; i--, j--) {
            if (arr[i] != 0) {
                if (j < arr.length) arr[j] = arr[i];
            } else {
                if (j < arr.length) arr[j] = arr[i];
                j--;
                if (j < arr.length) arr[j] = arr[i]; //copy twice when hit \'0\'
            }
        }
    }
```
</p>


### Python 3 real in-place solution
- Author: rokanor
- Creation Date: Sat Jun 29 2019 12:06:15 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 04 2020 16:16:44 GMT+0800 (Singapore Standard Time)

<p>
Start from the back and adjust items to correct locations. If item is zero then duplicate it.

```
class Solution:
    def duplicateZeros(self, arr: List[int]) -> None:
        zeroes = arr.count(0)
        n = len(arr)
        for i in range(n-1, -1, -1):
            if i + zeroes < n:
                arr[i + zeroes] = arr[i]
            if arr[i] == 0: 
                zeroes -= 1
                if i + zeroes < n:
                    arr[i + zeroes] = 0
```
</p>


### Java/C++ O(n) | O(1)
- Author: votrubac
- Creation Date: Sun Jun 16 2019 12:11:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 16 2019 12:32:24 GMT+0800 (Singapore Standard Time)

<p>
First, go left to right and count how many shifts (```sh```) we can fit in our array.
Then, go right to left and move items; if it\'s zero - duplicate it and decrement the shift.
> Note: ```i + sh``` can exceed the array size. We need a check for this case.
```
void duplicateZeros(vector<int>& a, int i = 0, int sh = 0) {
  for (i = 0; sh + i < a.size(); ++i) sh += a[i] == 0;
  for (i = i - 1; sh > 0; --i) {
    if (i + sh < a.size()) a[i + sh] = a[i];
    if (a[i] == 0) a[i + --sh] = a[i];
  }
}
```
Java version:
```
public void duplicateZeros(int[] a) {
  int i = 0, sh = 0;
  for (i = 0; sh + i < a.length; ++i) sh += a[i] == 0 ? 1 : 0;
  for (i = i - 1; sh > 0; --i) {
    if (i + sh < a.length) a[i + sh] = a[i];
    if (a[i] == 0) a[i + --sh] = a[i];
  }
}
```
</p>


