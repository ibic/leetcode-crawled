---
title: "Maximum Product of Three Numbers"
weight: 561
#id: "maximum-product-of-three-numbers"
---
## Description
<div class="description">
<p>Given an integer array, find three numbers whose product is maximum and output the maximum product.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> [1,2,3]
<b>Output:</b> 6
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> [1,2,3,4]
<b>Output:</b> 24
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>The length of the given array will be in range [3,10<sup>4</sup>] and all elements are in the range [-1000, 1000].</li>
	<li>Multiplication of any three numbers in the input won&#39;t exceed the range of 32-bit signed integer.</li>
</ol>

<p>&nbsp;</p>

</div>

## Tags
- Array (array)
- Math (math)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Redfin - 3 (taggedByAdmin: false)
- Bloomberg - 4 (taggedByAdmin: false)
- Cisco - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Symantec - 2 (taggedByAdmin: false)
- Intuit - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Brute Force

The simplest solution is to consider every triplet out of the given $$nums$$ array and check their product and find out the maximum product out of them.

**Complexity Analysis**

* Time complexity : $$O(n^3)$$. We need to consider every triplet from $$nums$$ array of length $$n$$.

* Space complexity : $$O(1)$$. Constant extra space is used.
<br>
<br>

---
#### Approach 2: Using Sorting

**Algorithm**

Another solution could be to sort the given $$nums$$ array(in ascending order) and find out the product of the last three numbers. 

But, we can note that this product will be maximum only if all the numbers in $$nums$$ array are positive. But, in the given problem statement, negative elements could exist as well. 

Thus, it could also be possible that two negative numbers lying at the left extreme end could also contribute to lead to a larger product if the third number in the triplet being considered is the largest positive number in the $$nums$$ array. 

Thus, either the product $$nums[0] \times nums[1] \times nums[n-1]$$ or $$nums[n-3] \times nums[n-2] \times nums[n-1]$$ will give the required result. Thus, we need to find the larger one from out of these values.

<iframe src="https://leetcode.com/playground/C2o9mFDQ/shared" frameBorder="0" width="100%" height="157" name="C2o9mFDQ"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(n\log n\big)$$. Sorting the $$nums$$ array takes $$n\log n$$ time.

* Space complexity : $$O(\log n)$$. Sorting takes $$O(\log n)$$ space.
<br>
<br>

---
#### Approach 3: Single Scan

**Algorithm**

We need not necessarily sort the given $$nums$$ array to find the maximum product. Instead, we can only find the required 2 smallest values($$min1$$ and $$min2$$) and the three largest values($$max1, max2, max3$$) in the $$nums$$ array, by iterating over the $$nums$$ array only once. 

At the end, again we can find out the larger value out of $$min1 \times min2 \times max1$$ and $$max1 \times max2 \times max3$$ to find the required maximum product.

<iframe src="https://leetcode.com/playground/AHAzWa47/shared" frameBorder="0" width="100%" height="480" name="AHAzWa47"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Only one iteration over the $$nums$$ array of length $$n$$ is required.

* Space complexity : $$O(1)$$. Constant extra space is used.
<br>
<br>

## Accepted Submission (python3)
```python3
class Solution:
    def maximumProduct(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        a, b = heapq.nlargest(3, nums), heapq.nsmallest(2, nums)
        return max(a[0] * a[1] * a[2], b[0] * b[1] * a[0])
```

## Top Discussions
### Java O(1) space O(n) time solution beat 100%
- Author: dreamchase
- Creation Date: Mon Jun 26 2017 01:10:41 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 23:01:25 GMT+0800 (Singapore Standard Time)

<p>
Simply find out the three largest numbers and the two smallest numbers using one pass.
```
    public int maximumProduct(int[] nums) {
        int max1 = Integer.MIN_VALUE, max2 = Integer.MIN_VALUE, max3 = Integer.MIN_VALUE, min1 = Integer.MAX_VALUE, min2 = Integer.MAX_VALUE;
        for (int n : nums) {
            if (n > max1) {
                max3 = max2;
                max2 = max1;
                max1 = n;
            } else if (n > max2) {
                max3 = max2;
                max2 = n;
            } else if (n > max3) {
                max3 = n;
            }

            if (n < min1) {
                min2 = min1;
                min1 = n;
            } else if (n < min2) {
                min2 = n;
            }
        }
        return Math.max(max1*max2*max3, max1*min1*min2);
    }
```
</p>


### [Python] O(N) and 1 line
- Author: lee215
- Creation Date: Thu Jun 29 2017 03:15:59 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 03 2018 04:50:07 GMT+0800 (Singapore Standard Time)

<p>
My first solution using ```sort```
````
def maximumProduct(self, nums):
        nums.sort()
        return max(nums[-1] * nums[-2] * nums[-3], nums[0] * nums[1] * nums[-1])
`````
I found a exactly same solution in discuss. Anyway, O(NlogN) is not adorable and O(N) is possible.
````
def maximumProduct(self, nums):
        a, b = heapq.nlargest(3, nums), heapq.nsmallest(2, nums)
        return max(a[0] * a[1] * a[2], b[0] * b[1] * a[0])
`````
Make it 1 line if you like:
````
    def maximumProduct(self, nums):
        return max(nums) * max(a * b for a, b in [heapq.nsmallest(2, nums), heapq.nlargest(3, nums)[1:]])
````

**Update:**
**Q:** Time complexity?
**A:** Let me make it clear.
My solution is `O(NlogK)`, where K = 3.
So I said it\'s `O(N)`
And the best solution can be `O(N)`.


@macheret help explained the following:

For heapq.nlargest / heapq.nsmallest, looking at the (source code)[https://github.com/python/cpython/blob/e22072fb11246f125aa9ff7629c832b9e2407ef0/Lib/heapq.py#L545-L559]

First, k items are heapified - that\'s an O(k*log(k)) operation.
Then, n-k items are added into the heap with heapreplace - that\'s n-k O(log(k)) operations, or O((n-k)*log(k)).
Add those up, you get O(n*log(k)).
In this case k is constant and doesn\'t scale with n, so this usage is O(n).
</p>


### Java Easy AC...
- Author: LeiYu
- Creation Date: Sun Jun 25 2017 11:00:34 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 12:15:09 GMT+0800 (Singapore Standard Time)

<p>
```
    public int maximumProduct(int[] nums) {
        
         Arrays.sort(nums);
         //One of the Three Numbers is the maximum value in the array.

         int a = nums[nums.length - 1] * nums[nums.length - 2] * nums[nums.length - 3];
         int b = nums[0] * nums[1] * nums[nums.length - 1];
         return a > b ? a : b;
    }
```
python3
```
 def maximumProduct(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        nums.sort()
        a = nums[-1] * nums[-2] * nums[-3]
        b = nums[0] * nums[1] * nums[-1]
        return max(a,b)
```
</p>


