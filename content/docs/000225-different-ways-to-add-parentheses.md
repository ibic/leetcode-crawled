---
title: "Different Ways to Add Parentheses"
weight: 225
#id: "different-ways-to-add-parentheses"
---
## Description
<div class="description">
<p>Given a string of numbers and operators, return all possible results from computing all the different possible ways to group numbers and operators. The valid operators are <code>+</code>, <code>-</code> and <code>*</code>.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> <code>&quot;2-1-1&quot;</code>
<b>Output:</b> <code>[0, 2]</code>
<strong>Explanation: </strong>
((2-1)-1) = 0 
(2-(1-1)) = 2</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input: </b><code>&quot;2*3-4*5&quot;</code>
<b>Output:</b> <code>[-34, -14, -10, -10, 10]</code>
<strong>Explanation: 
</strong>(2*(3-(4*5))) = -34 
((2*3)-(4*5)) = -14 
((2*(3-4))*5) = -10 
(2*((3-4)*5)) = -10 
(((2*3)-4)*5) = 10<strong>
</strong></pre>
</div>

## Tags
- Divide and Conquer (divide-and-conquer)

## Companies
- Google - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A recursive Java solution (284 ms)
- Author: 2guotou
- Creation Date: Mon Jul 27 2015 20:45:32 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 12:18:05 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public List<Integer> diffWaysToCompute(String input) {
            List<Integer> ret = new LinkedList<Integer>();
            for (int i=0; i<input.length(); i++) {
                if (input.charAt(i) == '-' ||
                    input.charAt(i) == '*' ||
                    input.charAt(i) == '+' ) {
                    String part1 = input.substring(0, i);
                    String part2 = input.substring(i+1);
                    List<Integer> part1Ret = diffWaysToCompute(part1);
                    List<Integer> part2Ret = diffWaysToCompute(part2);
                    for (Integer p1 :   part1Ret) {
                        for (Integer p2 :   part2Ret) {
                            int c = 0;
                            switch (input.charAt(i)) {
                                case '+': c = p1+p2;
                                    break;
                                case '-': c = p1-p2;
                                    break;
                                case '*': c = p1*p2;
                                    break;
                            }
                            ret.add(c);
                        }
                    }
                }
            }
            if (ret.size() == 0) {
                ret.add(Integer.valueOf(input));
            }
            return ret;
        }
    }
</p>


### C++ 4ms Recursive & DP solution with brief explanation
- Author: Gcdofree
- Creation Date: Mon Jul 27 2015 21:50:55 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 23:26:16 GMT+0800 (Singapore Standard Time)

<p>
Here is the basic recursive solution

    class Solution {
    public:
        vector<int> diffWaysToCompute(string input) {
            vector<int> result;
            int size = input.size();
            for (int i = 0; i < size; i++) {
                char cur = input[i];
                if (cur == '+' || cur == '-' || cur == '*') {
                    // Split input string into two parts and solve them recursively
                    vector<int> result1 = diffWaysToCompute(input.substr(0, i));
                    vector<int> result2 = diffWaysToCompute(input.substr(i+1));
                    for (auto n1 : result1) {
                        for (auto n2 : result2) {
                            if (cur == '+')
                                result.push_back(n1 + n2);
                            else if (cur == '-')
                                result.push_back(n1 - n2);
                            else
                                result.push_back(n1 * n2);    
                        }
                    }
                }
            }
            // if the input string contains only number
            if (result.empty())
                result.push_back(atoi(input.c_str()));
            return result;
        }
    };

There are many repeating subquestions in this recursive method, therefore, we could use dynamic programming to avoid this situation by saving the results for subquestions. Here is the DP solution.

    class Solution {
    public:
    	vector<int> diffWaysToCompute(string input) {
    		unordered_map<string, vector<int>> dpMap;
    		return computeWithDP(input, dpMap);
    	}
    
    	vector<int> computeWithDP(string input, unordered_map<string, vector<int>> &dpMap) {
    		vector<int> result;
    		int size = input.size();
    		for (int i = 0; i < size; i++) {
    			char cur = input[i];
    			if (cur == '+' || cur == '-' || cur == '*') {
    				// Split input string into two parts and solve them recursively
    				vector<int> result1, result2;
    				string substr = input.substr(0, i);
    				// check if dpMap has the result for substr
    				if (dpMap.find(substr) != dpMap.end())
    					result1 = dpMap[substr];
    				else
    					result1 = computeWithDP(substr, dpMap);
    
    				substr = input.substr(i + 1);
    				if (dpMap.find(substr) != dpMap.end())
    					result2 = dpMap[substr];
    				else
    					result2 = computeWithDP(substr, dpMap);
    				
    				for (auto n1 : result1) {
    					for (auto n2 : result2) {
    						if (cur == '+')
    							result.push_back(n1 + n2);
    						else if (cur == '-')
    							result.push_back(n1 - n2);
    						else
    							result.push_back(n1 * n2);
    					}
    				}
    			}
    		}
    		// if the input string contains only number
    		if (result.empty())
    			result.push_back(atoi(input.c_str()));
    		// save to dpMap
    		dpMap[input] = result;
    		return result;
    	}
    };
</p>


### Python easy to understand concise solution with memorization
- Author: OldCodingFarmer
- Creation Date: Sun Aug 23 2015 00:58:04 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 24 2020 23:50:13 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def diffWaysToCompute(self, input):
        m = {}
        return self.dfs(input, m)
        
    def dfs(self, input, m):
        if input in m:
            return m[input]
        if input.isdigit():
            m[input] = int(input)
            return [int(input)]
        ret = []
        for i, c in enumerate(input):
            if c in "+-*":
                l = self.diffWaysToCompute(input[:i])
                r = self.diffWaysToCompute(input[i+1:])
                ret.extend(eval(str(x)+c+str(y)) for x in l for y in r)
        m[input] = ret
        return ret
```
</p>


