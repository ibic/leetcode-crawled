---
title: "Can I Win"
weight: 441
#id: "can-i-win"
---
## Description
<div class="description">
<p>In the &quot;100 game&quot; two players take turns adding, to a running total, any integer from <code>1</code> to <code>10</code>. The player who first causes the running total to <strong>reach or exceed</strong> 100 wins.</p>

<p>What if we change the game so that players <strong>cannot</strong> re-use integers?</p>

<p>For example, two players might take turns drawing from a common pool of numbers from 1 to 15 without replacement until they reach a total &gt;= 100.</p>

<p>Given two integers maxChoosableInteger and&nbsp;desiredTotal, return <code>true</code> if the first player to move can force a win, otherwise return <code>false</code>.&nbsp;Assume&nbsp;both players play <strong>optimally</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> maxChoosableInteger = 10, desiredTotal = 11
<strong>Output:</strong> false
<strong>Explanation:</strong>
No matter which integer the first player choose, the first player will lose.
The first player can choose an integer from 1 up to 10.
If the first player choose 1, the second player can only choose integers from 2 up to 10.
The second player will win by choosing 10 and get a total = 11, which is &gt;= desiredTotal.
Same with other integers chosen by the first player, the second player will always win.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> maxChoosableInteger = 10, desiredTotal = 0
<strong>Output:</strong> true
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> maxChoosableInteger = 10, desiredTotal = 1
<strong>Output:</strong> true
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;=&nbsp;maxChoosableInteger &lt;= 20</code></li>
	<li><code>0 &lt;=&nbsp;desiredTotal &lt;= 300</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Minimax (minimax)

## Companies
- LinkedIn - 4 (taggedByAdmin: true)
- Google - 3 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java solution using HashMap with detailed explanation
- Author: leogogogo
- Creation Date: Mon Nov 21 2016 11:01:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 14:54:59 GMT+0800 (Singapore Standard Time)

<p>
After solving several "Game Playing" questions in leetcode, I find them to be pretty similar. Most of them can be solved using the **top-down DP** approach, which "brute-forcely" simulates every possible state of the game. 

The key part for the top-down dp strategy is that we need to **avoid repeatedly solving sub-problems**. Instead, we should use some strategy to "remember" the outcome of  sub-problems. Then when we see them again, we instantly know their result.  By doing this, ~~we can always reduce time complexity from **exponential** to **polynomial**~~. 
(**EDIT:** Thanks for @billbirdh for pointing out the mistake here. For this problem, by applying the memo, we at most compute for every subproblem once, and there are ```O(2^n)``` subproblems, so the complexity is  ```O(2^n)``` after memorization. (Without memo, time complexity should be like ```O(n!)```)

For this question, the key part is: ```what is the state of the game```? Intuitively, to uniquely determine the result of any state, we need to know: 
1) The unchosen numbers 
2) The remaining desiredTotal to reach

A second thought reveals that **1)** and **2)** are actually related because we can always get the **2)** by deducting the sum of chosen numbers from original desiredTotal.

Then the problem becomes how to describe the state using **1)**.

In my solution, I use a **boolean array** to denote which numbers have been chosen, and  then a question comes to mind, if we want to use a Hashmap to remember the outcome of sub-problems, can we just use ```Map<boolean[], Boolean>``` ? **Obviously we cannot**, because the if we use boolean[] as a key, the reference to boolean[] won't reveal the actual content in boolean[]. 

Since in the problem statement, it says ```maxChoosableInteger``` will not be larger than ```20```, which means the length of our **boolean[] array** will be less than ```20```. Then we can use an ```Integer``` to represent this boolean[] array. How?

Say the boolean[] is ```{false, false, true, true, false}```, then we can transfer it to an Integer with binary representation as ```00110```. Since Integer is a perfect choice to be the key of HashMap, then we now can "memorize" the sub-problems using ```Map<Integer, Boolean>```. 

The rest part of the solution is just simulating the game process using the top-down dp.

```
public class Solution {
    Map<Integer, Boolean> map;
    boolean[] used;
    public boolean canIWin(int maxChoosableInteger, int desiredTotal) {
        int sum = (1+maxChoosableInteger)*maxChoosableInteger/2;
        if(sum < desiredTotal) return false;
        if(desiredTotal <= 0) return true;
        
        map = new HashMap();
        used = new boolean[maxChoosableInteger+1];
        return helper(desiredTotal);
    }
    
    public boolean helper(int desiredTotal){
        if(desiredTotal <= 0) return false;
        int key = format(used);
        if(!map.containsKey(key)){
    // try every unchosen number as next step
            for(int i=1; i<used.length; i++){
                if(!used[i]){
                    used[i] = true;
     // check whether this lead to a win (i.e. the other player lose)
                    if(!helper(desiredTotal-i)){
                        map.put(key, true);
                        used[i] = false;
                        return true;
                    }
                    used[i] = false;
                }
            }
            map.put(key, false);
        }
        return map.get(key);
    }
   
// transfer boolean[] to an Integer 
    public int format(boolean[] used){
        int num = 0;
        for(boolean b: used){
            num <<= 1;
            if(b) num |= 1;
        }
        return num;
    }
}
```

**Updated:** Thanks for @ckcz123 for sharing the great idea. In Java, to denote ```boolean[]```, an easier way is to use ```Arrays.toString(boolean[])```, which will transfer a ```boolean[]``` to sth like ```"[true, false, false, ....]"```, which is also not limited to how ```maxChoosableInteger``` is set, so it can be generalized to arbitrary large ```maxChoosableInteger```.
</p>


### Clean C++ beat 98.4%, DFS with early termination check (detailed explanation)
- Author: zzg_zzm
- Creation Date: Thu Jan 26 2017 13:02:51 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 24 2019 18:23:31 GMT+0800 (Singapore Standard Time)

<p>
For short notation, let `M = maxChoosableInteger` and `T = desiredTotal`.

**Key Observation:** the state of the game is completely determined by currently available numbers to pick in the common pool.

**State of Game:** initially, we have all `M` numbers `[1, M]` available in the pool. Each number may or may not be picked at a state of the game later on, so we have maximum `2^M` different states. Note that `M <= 20`, so `int` range is enough to cover it. For memorization, we define `int k` as the key for a game state, where 
* the `i`-th bit of `k`, i.e., `k&(1<<i)` represents the availability of number `i+1` (`1`: picked; `0`: not picked).

At state `k`, the current player could pick any unpicked number from the pool, so state `k` can only go to one of the valid next states `k\'`:
* if `i`-th bit of `k` is `0`, set it to be `1`, i.e., next state `k\' = k|(1<<i)`.

**Recursion:** apparently
* the current player can win at state `k` iff opponent can\'t win at some valid next state `k\'`. 

**Memorization:** to speed up the recursion, we can use a `vector<int> m` of size `2^M` to memorize calculated results `m[k]` for state key `k`:
* ` 0` : not calculated yet;
* ` 1` : current player can win;
* `-1`: current player can\'t win.

**Initial State Check:**
There are several checks to be done at initial state `k = 0` for early termination so we won\'t waste our time for DFS process:
1. if `T < 2`, obviously, the first player wins by simply picking `1`.
2. if the sum of entire pool `S = M*(M+1)/2` is less than `T`, of course, nobody can reach `T`.
3. if the sum `S == T`, the order to pick numbers from the pool is irrelevant. Whoever picks the last will reach `T`. So the first player can win iff `M` is odd. 
```
  bool canIWin(int M, int T) 
  {
    int sum = M*(M+1)/2; // sum of entire choosable pool

    // I just pick 1 to win
    if (T < 2) return true;
    
    // Total is too large, nobody can win
    else if (sum < T) return false;
    
    // Total happens to match sum, whoever picks at odd times wins
    else if (sum == T) return M%2;
    
    // Non-trivial case: do DFS
    // Initial total: T
    // Initial game state: k = 0 (all numbers are not picked)
    else return dfs(M, T, 0);
  }

  // DFS to check if I can win
  // k: current game state
  // T: remaining total to reach
  bool dfs(int M, int T, int k) 
  {
    // memorized
    if (mem[k] != 0) return mem[k] > 0;
    
    // total is already reached by opponent, so I lose
    if (T <= 0) return false;

    // try all currently available numbers
    for (int i = 0; i < M; ++i)
      // if (i+1) is available to pick and my opponent can\'t win after I picked, I win!
      if (!(k&(1<<i)) && !dfs(M, T-i-1, k|(1<<i))) {
        mem[k] = 1;
        return true;
      } 
    
    // Otherwise, I will lose
    mem[k] = -1;
    return false;      
  }

  // m[key]: memorized game result when pool state = key
  // 0: un-computed; 1: I win; -1: I lose
  int mem[1<<20] = {};
```
</p>


### From Brute Force to Top-down DP
- Author: GraceMeng
- Creation Date: Tue Jul 31 2018 14:47:31 GMT+0800 (Singapore Standard Time)
- Update Date: Mon May 04 2020 21:08:08 GMT+0800 (Singapore Standard Time)

<p>
### Brute Force
```
class Solution {
    public boolean canIWin(int maxChoosableInteger, int desiredTotal) {
        if (desiredTotal <= maxChoosableInteger) 
            return true;
        if (((1 + maxChoosableInteger) / 2 * maxChoosableInteger) < desiredTotal) {
            return false;
        }
        return canIWinFrom(maxChoosableInteger, desiredTotal, new boolean[maxChoosableInteger + 1]);
    }
    
    private boolean canIWinFrom(int maxChoosableInteger, int desiredTotal, boolean[] chosen) {
        if (desiredTotal <= 0) {
            return false;
        }
        
        for (int i = 1; i <= maxChoosableInteger; i++) {
            if (chosen[i]) {
                continue;
            }
            chosen[i] = true;
            if (!canIWinFrom(maxChoosableInteger, desiredTotal - i, chosen)) {
                chosen[i] = false;
                return true;
            }
            chosen[i] = false;
        }
        return false;
    }
}
```
### Top-down DP
Use memoization to overcome overlapping subproblems.

There are two changing variables `desiredTotal` and `chosen` in recursive function `canIWinFrom`, though only `chosen` has to be tracked (as key).
```
class Solution {
    private Map<String, Boolean> memo; // key: chosen[] to string, value: canIWinWithSituation return value when chosen to string is key
    
    public boolean canIWin(int maxChoosableInteger, int desiredTotal) {
        
        if (desiredTotal <= maxChoosableInteger) 
            return true;
        if (((1 + maxChoosableInteger) / 2 * maxChoosableInteger) < desiredTotal) {
            return false;
        }
        memo = new HashMap<>();
        
        return canIWinWithSituation(maxChoosableInteger, desiredTotal, new boolean[maxChoosableInteger + 1]);
    }
    
    private boolean canIWinWithSituation(int maxChoosableInteger, int curDesiredTotal, boolean[] chosen) {
        
        if (curDesiredTotal <= 0) { 
            return false;
        }
        
        String chosenSerialization = Arrays.toString(chosen);
        if (memo.containsKey(chosenSerialization)) {
            return memo.get(chosenSerialization);
        }
        
        for (int i = 1; i <= maxChoosableInteger; i++) {
            if (chosen[i]) {
                continue;
            }
            chosen[i] = true;
            if (!canIWinWithSituation(maxChoosableInteger, curDesiredTotal - i, chosen)) {
                memo.put(chosenSerialization, true);
                chosen[i] = false;
                return true;
            }
            chosen[i] = false;
        }
        memo.put(chosenSerialization, false);
        return false;
    }
}
```

</p>


