---
title: "LRU Cache"
weight: 146
#id: "lru-cache"
---
## Description
<div class="description">
<p>Design a data structure that follows the constraints of a <strong><a href="https://en.wikipedia.org/wiki/Cache_replacement_policies#LRU" target="_blank">Least Recently Used (LRU) cache</a></strong>.</p>

<p>Implement the <code>LRUCache</code> class:</p>

<ul>
	<li><code>LRUCache(int capacity)</code> Initialize the LRU cache with <strong>positive</strong> size <code>capacity</code>.</li>
	<li><code>int get(int key)</code> Return the value of the <code>key</code> if the key exists, otherwise return <code>-1</code>.</li>
	<li><code>void put(int key, int value)</code>&nbsp;Update the value of the <code>key</code> if the <code>key</code> exists. Otherwise, add the <code>key-value</code> pair to the cache. If the number of keys exceeds the <code>capacity</code> from this operation, <strong>evict</strong> the least recently used key.</li>
</ul>

<p><b>Follow up:</b><br />
Could you do <code>get</code> and <code>put</code> in <code>O(1)</code> time complexity?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;LRUCache&quot;, &quot;put&quot;, &quot;put&quot;, &quot;get&quot;, &quot;put&quot;, &quot;get&quot;, &quot;put&quot;, &quot;get&quot;, &quot;get&quot;, &quot;get&quot;]
[[2], [1, 1], [2, 2], [1], [3, 3], [2], [4, 4], [1], [3], [4]]
<strong>Output</strong>
[null, null, null, 1, null, -1, null, -1, 3, 4]

<strong>Explanation</strong>
LRUCache lRUCache = new LRUCache(2);
lRUCache.put(1, 1); // cache is {1=1}
lRUCache.put(2, 2); // cache is {1=1, 2=2}
lRUCache.get(1);    // return 1
lRUCache.put(3, 3); // LRU key was 2, evicts key 2, cache is {1=1, 3=3}
lRUCache.get(2);    // returns -1 (not found)
lRUCache.put(4, 4); // LRU key was 1, evicts key 1, cache is {4=4, 3=3}
lRUCache.get(1);    // return -1 (not found)
lRUCache.get(3);    // return 3
lRUCache.get(4);    // return 4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= capacity &lt;= 3000</code></li>
	<li><code>0 &lt;= key &lt;= 3000</code></li>
	<li><code>0 &lt;= value &lt;= 10<sup>4</sup></code></li>
	<li>At most <code>3 * 10<sup>4</sup></code> calls will be made to <code>get</code> and <code>put</code>.</li>
</ul>

</div>

## Tags
- Design (design)

## Companies
- Amazon - 90 (taggedByAdmin: true)
- Bloomberg - 29 (taggedByAdmin: true)
- Microsoft - 20 (taggedByAdmin: true)
- Facebook - 18 (taggedByAdmin: true)
- Apple - 16 (taggedByAdmin: false)
- Twilio - 10 (taggedByAdmin: false)
- ByteDance - 9 (taggedByAdmin: false)
- eBay - 7 (taggedByAdmin: false)
- Oracle - 7 (taggedByAdmin: false)
- Google - 6 (taggedByAdmin: true)
- Goldman Sachs - 6 (taggedByAdmin: false)
- Snapchat - 5 (taggedByAdmin: true)
- Tesla - 5 (taggedByAdmin: false)
- VMware - 4 (taggedByAdmin: false)
- Capital One - 4 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: true)
- Cisco - 3 (taggedByAdmin: false)
- Salesforce - 3 (taggedByAdmin: false)
- Cruise Automation - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)
- Nvidia - 2 (taggedByAdmin: false)
- Nutanix - 2 (taggedByAdmin: false)
- Cohesity - 2 (taggedByAdmin: false)
- Roblox - 2 (taggedByAdmin: false)
- Dropbox - 11 (taggedByAdmin: false)
- Two Sigma - 5 (taggedByAdmin: false)
- Zillow - 5 (taggedByAdmin: false)
- Intuit - 5 (taggedByAdmin: false)
- Asana - 5 (taggedByAdmin: false)
- Yahoo - 4 (taggedByAdmin: true)
- Citadel - 4 (taggedByAdmin: false)
- Docusign - 3 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Groupon - 2 (taggedByAdmin: false)
- Lyft - 4 (taggedByAdmin: false)
- GoDaddy - 4 (taggedByAdmin: false)
- SAP - 4 (taggedByAdmin: false)
- Spotify - 4 (taggedByAdmin: false)
- Baidu - 3 (taggedByAdmin: false)
- Twitter - 3 (taggedByAdmin: true)
- Cloudera - 3 (taggedByAdmin: false)
- Walmart Labs - 3 (taggedByAdmin: false)
- JPMorgan - 3 (taggedByAdmin: false)
- TripAdvisor - 3 (taggedByAdmin: false)
- Morgan Stanley - 3 (taggedByAdmin: false)
- DoorDash - 3 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- Zynga - 2 (taggedByAdmin: false)
- HBO - 2 (taggedByAdmin: false)
- Twitch - 2 (taggedByAdmin: false)
- Splunk - 2 (taggedByAdmin: false)
- Palantir Technologies - 0 (taggedByAdmin: true)
- Zenefits - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Ordered dictionary

**Intuition**

We're asked to implement [the structure](https://en.wikipedia.org/wiki/Cache_replacement_policies#LRU) 
which provides the following operations in $$\mathcal{O}(1)$$ time :

- Get the key / Check if the key exists 

- Put the key

- Delete the first added key

The first two operations in $$\mathcal{O}(1)$$ time are provided 
by the standard hashmap, and the last one - by linked list.

> There is a structure called _ordered dictionary_, it combines 
behind both hashmap and linked list. In Python this structure is called
[_OrderedDict_](https://docs.python.org/3/library/collections.html#collections.OrderedDict)
and in Java [_LinkedHashMap_](https://docs.oracle.com/javase/8/docs/api/java/util/LinkedHashMap.html).

Let's use this structure here.

**Implementation**

<iframe src="https://leetcode.com/playground/hfQBGVXK/shared" frameBorder="0" width="100%" height="500" name="hfQBGVXK"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$ both for `put`
and `get` since all operations with
ordered dictionary : `get/in/set/move_to_end/popitem` 
(`get/containsKey/put/remove`) are done in a constant time.
 
* Space complexity : $$\mathcal{O}(capacity)$$ since the 
space is used only for an ordered dictionary with at most `capacity + 1` elements.
<br />
<br />


---
#### Approach 2: Hashmap + DoubleLinkedList

**Intuition**

This Java solution is an extended version of the 
[the article published on the Discuss forum](https://leetcode.com/problems/lru-cache/discuss/45911/Java-Hashtable-%2B-Double-linked-list-(with-a-touch-of-pseudo-nodes)).

The problem can be solved with a hashmap
that keeps track of the keys and its values in the double linked list. 
That results in $$\mathcal{O}(1)$$ time for `put` and `get` operations and
allows to remove the first added node in $$\mathcal{O}(1)$$ time as well.

![compute](../Figures/146/structure.png)

One advantage of _double_ linked list is that the node can remove itself without other reference. 
In addition, it takes constant time to add and remove nodes from the head or tail.

One particularity about the double linked list implemented here
is that there are _pseudo head_ and _pseudo tail_ to mark the boundary, 
so that we don't need to check the `null` node during the update. 

![compute](../Figures/146/new_node.png)

**Implementation**

<iframe src="https://leetcode.com/playground/dcSrw233/shared" frameBorder="0" width="100%" height="500" name="dcSrw233"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$ both for `put`
and `get`.
 
* Space complexity : $$\mathcal{O}(capacity)$$ since the 
space is used only for a hashmap and double linked list 
with at most `capacity + 1` elements.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Hashtable + Double linked list (with a touch of pseudo nodes)
- Author: liaison
- Creation Date: Fri Jan 02 2015 03:26:39 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 04 2019 20:22:23 GMT+0800 (Singapore Standard Time)

<p>
The problem can be solved with a hashtable that keeps track of the keys and its values in the double linked list. One interesting property about double linked list is that the node can remove itself without other reference. In addition, it takes constant time to add and remove nodes from the head or tail.

One particularity about the double linked list that I implemented is that I create a pseudo head and tail to mark the boundary, so that we don\'t need to check the NULL node during the update. This makes the code more concise and clean, and also it is good for the performance. 

Voila, here is the code. 

```java
import java.util.Hashtable;


public class LRUCache {

class DLinkedNode {
  int key;
  int value;
  DLinkedNode pre;
  DLinkedNode post;
}

/**
 * Always add the new node right after head;
 */
private void addNode(DLinkedNode node) {
    
  node.pre = head;
  node.post = head.post;

  head.post.pre = node;
  head.post = node;
}

/**
 * Remove an existing node from the linked list.
 */
private void removeNode(DLinkedNode node){
  DLinkedNode pre = node.pre;
  DLinkedNode post = node.post;

  pre.post = post;
  post.pre = pre;
}

/**
 * Move certain node in between to the head.
 */
private void moveToHead(DLinkedNode node){
  this.removeNode(node);
  this.addNode(node);
}

// pop the current tail. 
private DLinkedNode popTail(){
  DLinkedNode res = tail.pre;
  this.removeNode(res);
  return res;
}

private Hashtable<Integer, DLinkedNode> 
  cache = new Hashtable<Integer, DLinkedNode>();
private int count;
private int capacity;
private DLinkedNode head, tail;

public LRUCache(int capacity) {
  this.count = 0;
  this.capacity = capacity;

  head = new DLinkedNode();
  head.pre = null;

  tail = new DLinkedNode();
  tail.post = null;

  head.post = tail;
  tail.pre = head;
}

public int get(int key) {

  DLinkedNode node = cache.get(key);
  if(node == null){
    return -1; // should raise exception here.
  }

  // move the accessed node to the head;
  this.moveToHead(node);

  return node.value;
}


public void put(int key, int value) {
  DLinkedNode node = cache.get(key);

  if(node == null){

    DLinkedNode newNode = new DLinkedNode();
    newNode.key = key;
    newNode.value = value;

    this.cache.put(key, newNode);
    this.addNode(newNode);

    ++count;

    if(count > capacity){
      // pop the tail
      DLinkedNode tail = this.popTail();
      this.cache.remove(tail.key);
      --count;
    }
  }else{
    // update the value.
    node.value = value;
    this.moveToHead(node);
  }
}

}
```
</p>


### Python Dict + Double LinkedList
- Author: tusizi
- Creation Date: Sat May 23 2015 16:52:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 10:18:34 GMT+0800 (Singapore Standard Time)

<p>
    class Node:
    def __init__(self, k, v):
        self.key = k
        self.val = v
        self.prev = None
        self.next = None

    class LRUCache:
    def __init__(self, capacity):
        self.capacity = capacity
        self.dic = dict()
        self.head = Node(0, 0)
        self.tail = Node(0, 0)
        self.head.next = self.tail
        self.tail.prev = self.head

    def get(self, key):
        if key in self.dic:
            n = self.dic[key]
            self._remove(n)
            self._add(n)
            return n.val
        return -1

    def set(self, key, value):
        if key in self.dic:
            self._remove(self.dic[key])
        n = Node(key, value)
        self._add(n)
        self.dic[key] = n
        if len(self.dic) > self.capacity:
            n = self.head.next
            self._remove(n)
            del self.dic[n.key]

    def _remove(self, node):
        p = node.prev
        n = node.next
        p.next = n
        n.prev = p

    def _add(self, node):
        p = self.tail.prev
        p.next = node
        self.tail.prev = node
        node.prev = p
        node.next = self.tail
</p>


### Laziest implementation: Java's LinkedHashMap takes care of everything
- Author: sky-xu
- Creation Date: Fri Apr 29 2016 04:59:52 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 12:51:27 GMT+0800 (Singapore Standard Time)

<p>
This is the laziest implementation using Java's LinkedHashMap. In the real interview, however, it is definitely not what interviewer expected.
```
    import java.util.LinkedHashMap;
    public class LRUCache {
        private LinkedHashMap<Integer, Integer> map;
        private final int CAPACITY;
        public LRUCache(int capacity) {
            CAPACITY = capacity;
            map = new LinkedHashMap<Integer, Integer>(capacity, 0.75f, true){
                protected boolean removeEldestEntry(Map.Entry eldest) {
                    return size() > CAPACITY;
                }
            };
        }
        public int get(int key) {
            return map.getOrDefault(key, -1);
        }
        public void set(int key, int value) {
            map.put(key, value);
        }
    }
```
Several points to mention:<br>
1. In the constructor, the third boolean parameter specifies the ordering mode. If we set it to true, it will be in access order. (https://docs.oracle.com/javase/8/docs/api/java/util/LinkedHashMap.html#LinkedHashMap-int-float-boolean-)<br>
2. By overriding removeEldestEntry in this way, we do not need to take care of it ourselves. It will automatically remove the least recent one when the size of map exceeds the specified capacity.(https://docs.oracle.com/javase/8/docs/api/java/util/LinkedHashMap.html#removeEldestEntry-java.util.Map.Entry-)

Below is a "normal" HashMap + doubly-linked list implementation:
```
public class LRUCache {
    private class Node{
        int key, value;
        Node prev, next;
        Node(int k, int v){
            this.key = k;
            this.value = v;
        }
        Node(){
            this(0, 0);
        }
    }
    private int capacity, count;
    private Map<Integer, Node> map;
    private Node head, tail;
    
    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.count = 0;
        map = new HashMap<>();
        head = new Node();
        tail = new Node();
        head.next = tail;
        tail.prev = head;
    }
    
    public int get(int key) {
        Node n = map.get(key);
        if(null==n){
            return -1;
        }
        update(n);
        return n.value;
    }
    
    public void set(int key, int value) {
        Node n = map.get(key);
        if(null==n){
            n = new Node(key, value);
            map.put(key, n);
            add(n);
            ++count;
        }
        else{
            n.value = value;
            update(n);
        }
        if(count>capacity){
            Node toDel = tail.prev;
            remove(toDel);
            map.remove(toDel.key);
            --count;
        }
    }
    
    private void update(Node node){
        remove(node);
        add(node);
    }
    private void add(Node node){
        Node after = head.next;
        head.next = node;
        node.prev = head;
        node.next = after;
        after.prev = node;
    }
    
    private void remove(Node node){
        Node before = node.prev, after = node.next;
        before.next = after;
        after.prev = before;
    }
}
</p>


