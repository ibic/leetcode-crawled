---
title: "Minimum Value to Get Positive Step by Step Sum"
weight: 1297
#id: "minimum-value-to-get-positive-step-by-step-sum"
---
## Description
<div class="description">
<p>Given an array of integers&nbsp;<code>nums</code>, you start with an initial <strong>positive</strong> value <em>startValue</em><em>.</em></p>

<p>In each iteration, you calculate the step by step sum of <em>startValue</em>&nbsp;plus&nbsp;elements in <code>nums</code>&nbsp;(from left to right).</p>

<p>Return the minimum <strong>positive</strong> value of&nbsp;<em>startValue</em> such that the step by step sum is never less than 1.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [-3,2,-3,4,2]
<strong>Output:</strong> 5
<strong>Explanation: </strong>If you choose startValue = 4, in the third iteration your step by step sum is less than 1.
<strong>                step by step sum
&nbsp;               startValue = 4 | startValue = 5 | nums
</strong>&nbsp;                 (4 <strong>-3</strong> ) = 1  | (5 <strong>-3</strong> ) = 2    |  -3
&nbsp;                 (1 <strong>+2</strong> ) = 3  | (2 <strong>+2</strong> ) = 4    |   2
&nbsp;                 (3 <strong>-3</strong> ) = 0  | (4 <strong>-3</strong> ) = 1    |  -3
&nbsp;                 (0 <strong>+4</strong> ) = 4  | (1 <strong>+4</strong> ) = 5    |   4
&nbsp;                 (4 <strong>+2</strong> ) = 6  | (5 <strong>+2</strong> ) = 7    |   2
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2]
<strong>Output:</strong> 1
<strong>Explanation:</strong> Minimum start value should be positive. 
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,-2,-3]
<strong>Output:</strong> 5
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 100</code></li>
	<li><code>-100 &lt;= nums[i] &lt;= 100</code></li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- Swiggy - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java O(n)
- Author: votrubac
- Creation Date: Sun Apr 19 2020 00:01:13 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 20 2020 15:15:41 GMT+0800 (Singapore Standard Time)

<p>
Find the smallest number we can get `min_sum`; our start value should be just enough ti cover that (`start == 1 - min_sum`).

**C++**
```cpp
int minStartValue(vector<int>& nums) {
    auto sum = 0, min_sum = 0;
    for (auto n : nums) {
        sum += n;
        min_sum = min(min_sum, sum);
    }
    return 1 - min_sum;
}
```
**Java**
```java
public int minStartValue(int[] nums) {
    int sum = 0, min_sum = 0;
    for (var n : nums) {
        sum += n;
        min_sum = Math.min(min_sum, sum);
    }
    return 1 - min_sum;
}
```
</p>


### [Python] One-Liner using Prefix-Sum with explanation
- Author: C0R3
- Creation Date: Sun Apr 19 2020 00:02:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 00:02:17 GMT+0800 (Singapore Standard Time)

<p>
We need to find a start value that needs to be big enough so that for any number in the input array the sum of the start value and all further numbers up to that number is at least one. To find such a number we need to sum up all the numbers and at each step check if the current prefix sum is a new minimum. Our start value needs to make up for that minimum prefix sum and we also need to add one so we are at least at 1 (```start_value = -min_prefix_sum + 1```).

Here is the calculation using the sample input:
```
num                 -3   2  -3   4   2
prefix sum          -3  -1  -4   0   2
min prefix sum      -3  -3  -4  -4  -4
min start value      4   4   5   5   5
```

**Implementation**
The implementation is straight forward. We just have to iterate over the input and add the current number to the prefix sum. Then we can calculate the minimum start value with that.

Time complexity: ```O(n)```
Space complexity: ```O(1)```
```python
class Solution:
    def minStartValue(self, nums: List[int]) -> int:
        prefix_sum = 0
        min_start_value = 1
        
        for num in nums:
            prefix_sum += num
            min_start_value = max(min_start_value, 1 - prefix_sum)
        
        return min_start_value
```

**One-Liner**
The [accumulate](https://docs.python.org/3/library/itertools.html#itertools.accumulate) function from python\'s itertools returns an iterator over the prefix sums and helps us to create a nice one-liner.
```python
class Solution:
    def minStartValue(self, nums: List[int]) -> int:
        return max(1, max(1 - prefix_sum for prefix_sum in itertools.accumulate(nums)))
```

</p>


### [JavaPython 3] 6 and 1 liners O(n) - Find the minimum of prefix sum.
- Author: rock
- Creation Date: Sun Apr 19 2020 00:01:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 21:54:54 GMT+0800 (Singapore Standard Time)

<p>
```java
    public int minStartValue(int[] nums) {
        int sum = 0, minPrefixSum = 0;
        for (int n : nums) {
            sum += n;
            minPrefixSum = Math.min(sum, minPrefixSum);
        }
        return 1 - minPrefixSum;
    }
```
```python
    def minStartValue(self, nums: List[int]) -> int:
        return 1 - min(0, min(itertools.accumulate(nums)))
```
</p>


