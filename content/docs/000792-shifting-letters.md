---
title: "Shifting Letters"
weight: 792
#id: "shifting-letters"
---
## Description
<div class="description">
<p>We have a string <code>S</code> of lowercase letters, and an integer array <code>shifts</code>.</p>

<p>Call the <em>shift</em> of a letter, the next letter in the alphabet, (wrapping around so that <code>&#39;z&#39;</code> becomes <code>&#39;a&#39;</code>).&nbsp;</p>

<p>For example, <code>shift(&#39;a&#39;) = &#39;b&#39;</code>, <code>shift(&#39;t&#39;) = &#39;u&#39;</code>, and <code>shift(&#39;z&#39;) = &#39;a&#39;</code>.</p>

<p>Now for each <code>shifts[i] = x</code>, we want to shift the first <code>i+1</code>&nbsp;letters of <code>S</code>, <code>x</code> times.</p>

<p>Return the final string&nbsp;after all such shifts to <code>S</code> are applied.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>S = &quot;abc&quot;, shifts = [3,5,9]
<strong>Output: </strong>&quot;rpl&quot;
<strong>Explanation: </strong>
We start with &quot;abc&quot;.
After shifting the first 1 letters of S by 3, we have &quot;dbc&quot;.
After shifting the first 2 letters of S by 5, we have &quot;igc&quot;.
After shifting the first 3 letters of S by 9, we have &quot;rpl&quot;, the answer.
</pre>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= S.length = shifts.length &lt;= 20000</code></li>
	<li><code>0 &lt;= shifts[i] &lt;= 10 ^ 9</code></li>
</ol>

</div>

## Tags
- String (string)

## Companies
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Prefix Sum [Accepted]

**Intuition**

Let's ask how many times the `i`th character is shifted.

**Algorithm**

The `i`th character is shifted `shifts[i] + shifts[i+1] + ... + shifts[shifts.length - 1]` times.  That's because only operations at the `i`th operation and after, affect the `i`th character.

Let `X` be the number of times the current `i`th character is shifted.  Then the next character `i+1` is shifted `X - shifts[i]` times.

For example, if `S.length = 4` and `S[0]` is shifted `X = shifts[0] + shifts[1] + shifts[2] + shifts[3]` times, then `S[1]` is shifted `shifts[1] + shifts[2] + shifts[3]` times, `S[2]` is shifted `shifts[2] + shifts[3]` times, and so on.

In general, we need to do `X -= shifts[i]` to maintain the correct value of `X` as we increment `i`.

<iframe src="https://leetcode.com/playground/U3DHELQs/shared" frameBorder="0" width="100%" height="327" name="U3DHELQs"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `S` (and `shifts`).

* Space Complexity:  $$O(N)$$, the space needed to output the answer.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Easy Understood
- Author: lee215
- Creation Date: Sun Jun 10 2018 11:03:51 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 10:01:24 GMT+0800 (Singapore Standard Time)

<p>
**Explanation**:
One pass to count suffix sum of `shifts`.
One pass to shift letters in string `S`
You can combine 2 passes as I did in 2-lines C++ version.

**Time Complexity**:
O(N)

**C++:**
```
    string shiftingLetters2(string S, vector<int> shifts) {
        for (int i = shifts.size() - 2; i >= 0; i--)
            shifts[i] = (shifts[i] + shifts[i + 1]) % 26;
        for (int i = 0; i < shifts.size(); i++)
            S[i] = (S[i] - \'a\' + shifts[i]) % 26 + \'a\';
        return S;
    }
```

**2-lines C++:**
```
    string shiftingLetters(string S, vector<int> sh) {
        for (int i = sh.size() - 1, m = 0; i >= 0; --i, m %= 26)
            S[i] = ((S[i] - \'a\') + (m += sh[i])) % 26 + \'a\';
        return S;
    }
```

**Java:**
```
    public String shiftingLetters(String S, int[] shifts) {
        StringBuilder res = new StringBuilder(S);
        for (int i = shifts.length - 2; i >= 0; i--)
            shifts[i] = (shifts[i] + shifts[i + 1]) % 26;
        for (int i = 0; i < S.length(); i++)
            res.setCharAt(i, (char)((S.charAt(i) - \'a\' + shifts[i]) % 26 + \'a\'));
        return res.toString();
    }
```
**2-lines Python:**
```
    def shiftingLetters(self, S, shifts):
        for i in range(len(shifts) - 1)[::-1]: shifts[i] += shifts[i + 1]
        return "".join(chr((ord(c) - 97 + s) % 26 + 97) for c, s in zip(S, shifts))
```
</p>


### Java clean solution
- Author: tankztc
- Creation Date: Mon Jun 11 2018 00:33:26 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 27 2018 09:18:58 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public String shiftingLetters(String S, int[] shifts) {
        char[] arr = S.toCharArray();
        int shift = 0;
        for (int i = arr.length - 1; i >= 0; i--) {
            shift = (shift + shifts[i]) % 26;
            arr[i] = (char)((arr[i] - \'a\' + shift) % 26 + \'a\');
        }
        return new String(arr);
    }
}
```
</p>


### Not medium!
- Author: gkr007
- Creation Date: Sun Mar 08 2020 13:19:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 13:19:48 GMT+0800 (Singapore Standard Time)

<p>
This should be marked as easy!
</p>


