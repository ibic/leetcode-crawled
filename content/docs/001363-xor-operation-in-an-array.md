---
title: "XOR Operation in an Array"
weight: 1363
#id: "xor-operation-in-an-array"
---
## Description
<div class="description">
<p>Given an integer <code>n</code> and an integer <code>start</code>.</p>

<p>Define an array <code>nums</code> where <code>nums[i] = start + 2*i</code> (0-indexed) and <code>n == nums.length</code>.</p>

<p>Return the bitwise&nbsp;XOR&nbsp;of all elements of <code>nums</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 5, start = 0
<strong>Output:</strong> 8
<strong>Explanation: </strong>Array nums is equal to [0, 2, 4, 6, 8] where (0 ^ 2 ^ 4 ^ 6 ^ 8) = 8.
Where &quot;^&quot; corresponds to bitwise XOR operator.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 4, start = 3
<strong>Output:</strong> 8
<strong>Explanation: </strong>Array nums is equal to [3, 5, 7, 9] where (3 ^ 5 ^ 7 ^ 9) = 8.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 1, start = 7
<strong>Output:</strong> 7
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 10, start = 5
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 1000</code></li>
	<li><code>0 &lt;= start &lt;= 1000</code></li>
	<li><code>n == nums.length</code></li>
</ul>
</div>

## Tags
- Array (array)
- Bit Manipulation (bit-manipulation)

## Companies
- Walmart Labs - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### O(1) time
- Author: coder206
- Creation Date: Sun Jun 21 2020 12:22:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 21 2020 12:22:33 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution
{
    int xorOperationB(int n, int start)
    {
        if (n % 2 == 0)
            return (n / 2) & 1;
        else
            return ((n / 2) & 1) ^ (start + n - 1);
    }
    int xorOperationA(int n, int start)
    {
        if (start & 1)
            return (start - 1) ^ xorOperationB(n + 1, start - 1);
        else
            return xorOperationB(n, start);
    }
public:
    int xorOperation(int n, int start)
    {
        int ret = 2 * xorOperationA(n, start / 2);
        if (n & start & 1) ret++;
        return ret;
    }
};
```
</p>


### [Visual Solution] Python | O(1) Time | O(1) Space
- Author: Argent
- Creation Date: Mon Jun 22 2020 04:27:44 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 04 2020 07:38:14 GMT+0800 (Singapore Standard Time)

<p>
The trivial solution to this problem is the brute force approach. 
This solution uses O(n) Time and O(n) Space

```
class Solution:
    def xorOperation(self, n: int, start: int) -> int:
        
        ans = 0
        nums = [start + n * 2 for n in range(n)]
        
        for n in nums:
            ans = ans ^ n
        return ans 
        
```

While this solution is easy let\'s try to find something better. Let\'s print some examples using the previous algorithm by adding in the following line in the for loop.

```
print(f"Nums: {n} Ans: {ans}")
```

.
**Example:**
Start  = 0 ,1 
n = 1...8 

![image](https://assets.leetcode.com/users/images/30d64e34-4ca0-4451-a5ab-d751e192c272_1592771245.7420313.png)

Looking at these charts it is easy to spot a pattern for some of the rows.
The pattern repeats after every 4th number

[N % 4 == 1] Green Row: Ans = Number[N]
[N % 4 == 2] Yellow Row: Ans = 2
[N % 4 == 3] Red Row: Ans = Number[N] ^ 2
[N % 4 == 0] Blue Row: Ans = 0

.
**Next Example:**
Start  = 2 , 3
n = 1...8 

![image](https://assets.leetcode.com/users/images/5aad9815-00c3-44de-85c3-56fe028921a0_1592770409.9596157.png)

Looking at these charts it is easy to spot a pattern for some of the rows.
There, the pattern also repeats after every 4th number.

[N % 4 == 1] Green Row: Ans = Number[1]
[N % 4 == 2] Yellow Row: Ans = Number[N] ^ Number[1]
[N % 4 == 3] Red Row: Ans = Number[1] ^ 2
[N % 4 == 0] Blue Row: Ans = Number[N] ^ Number[1] ^ 2

These patterns are a bit trickier to find. Start with the obvious one (Green). We know that each value is based on being xOr with the previous number. For yellow, just the number xOr with Green.

Red value is constant so that one is also easy to find. For blue just follow the same logic used to find yellow. 

If you were to continue this you would see that the examples would repeat themselves

.
**Code Solution:**

```
class Solution:
    def xorOperation(self, n: int, start: int) -> int:
        
        last = start + 2 * (n-1)

        if start % 4 <= 1:
            if n % 4 == 1: 
                return last
            elif n % 4 == 2: 
                return 2
            elif n % 4 == 3: 
                return 2 ^ last
            else: 
                return 0

        else:
            if n % 4 == 1: 
                return start
            elif n % 4 == 2: 
                return start ^ last
            elif n % 4 == 3: 
                return start ^ 2
            else: 
                return start ^ 2 ^ last
```
        
Argent 
Like if this helped 

</p>


### [Java/C++/Python] Array, Time O(N) Space O(1)
- Author: yedige
- Creation Date: Sun Jun 21 2020 12:01:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 21 2020 12:01:53 GMT+0800 (Singapore Standard Time)

<p>
**Explanation**
Loop once, we start from the initial value (start + 2 * 0 = start) and calculate XOR Operations step by step until we reach n-1.

**Complexity**

Time ```O(N)```
Space ```O(1)```

**Java:**
```
public int xorOperation(int n, int start) {
        int res = start;
        for (int i=1; i<n; i++){
            res = res ^ (start + 2 * i);
            }
        return res;
    }
```
**C++**
```
	int xorOperation(int n, int start) {   
		int res = start;
		for (int i=1; i<n; i++){
			res = res ^ (start + 2 * i);
			}
		return res;
	}
```
**Python**
```
    def xorOperation(self, n, start):
        res = start
        for i in range(1, n):
            res = res ^ (start + 2 * i)
        return res
```

</p>


