---
title: "Lonely Pixel II"
weight: 502
#id: "lonely-pixel-ii"
---
## Description
<div class="description">
<p>Given a picture consisting of black and white pixels, and a positive integer N, find the number of black pixels located at some specific row <b>R</b> and column <b>C</b> that align with all the following rules:</p>

<ol>
<li> Row R and column C both contain exactly N black pixels.</li>
<li> For all rows that have a black pixel at column C, they should be exactly the same as row R</li>
</ol>

<p>The picture is represented by a 2D char array consisting of 'B' and 'W', which means black and white pixels respectively. </p>

<p><b>Example:</b><br />
<pre>
<b>Input:</b>                                            
[['W', 'B', 'W', 'B', 'B', 'W'],    
 ['W', 'B', 'W', 'B', 'B', 'W'],    
 ['W', 'B', 'W', 'B', 'B', 'W'],    
 ['W', 'W', 'B', 'W', 'B', 'W']] 

N = 3
<b>Output:</b> 6
<b>Explanation:</b> All the bold 'B' are the black pixels we need (all 'B's at column 1 and 3).
        0    1    2    3    4    5         column index                                            
0    [['W', <b>'B'</b>, 'W', <b>'B'</b>, 'B', 'W'],    
1     ['W', <b>'B'</b>, 'W', <b>'B'</b>, 'B', 'W'],    
2     ['W', <b>'B'</b>, 'W', <b>'B'</b>, 'B', 'W'],    
3     ['W', 'W', 'B', 'W', 'B', 'W']]    
row index

Take 'B' at row R = 0 and column C = 1 as an example:
Rule 1, row R = 0 and column C = 1 both have exactly N = 3 black pixels. 
Rule 2, the rows have black pixel at column C = 1 are row 0, row 1 and row 2. They are exactly the same as row R = 0.

</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The range of width and height of the input 2D array is [1,200].</li>
</ol>
</p>
</div>

## Tags
- Array (array)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Verbose Java O(m*n) Solution, HashMap
- Author: shawngao
- Creation Date: Sun Mar 05 2017 12:10:47 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 08:53:43 GMT+0800 (Singapore Standard Time)

<p>
The difficult parts are validating the two rules:
1. Row ```R``` and column ```C``` both contain exactly ``N`` black pixels.
2. For all rows that have a black pixel at column ```C```, they should be exactly the same as row ```R```

My solution:
1. Scan each row. If that row has ```N``` black pixels, put a string ```signature```, which is concatenation of each characters in that row, as key and how many times we see that ```signature``` into a HashMap. Also during scan each row, we record how many black pixels in each column in an array ```colCount``` and will use it in step 2.
For input:
[['W', 'B', 'W', 'B', 'B', 'W'],    
  ['W', 'B', 'W', 'B', 'B', 'W'],    
  ['W', 'B', 'W', 'B', 'B', 'W'],    
  ['W', 'W', 'B', 'W', 'B', 'B']] 
We will get a HashMap:
{"WBWBBW": 3, "WWBWBB": 1}
and colCount array:
[0, 3, 1, 3, 4, 1]
2. Go through the HashMap and if the count of one ```signature``` is ```N```, those rows potentially contain black pixels we are looking for. Then we validate each of those columns. For each column of them has ```N``` black pixels (lookup in ```colCount``` array), we get ```N``` valid black pixels.
For above example, only the first ```signature``` "WBWBBW" has count == 3. We validate 3 column 1, 3, 4 where char == 'B', and column 1 and 3  have 3 'B', then answer is 2 * 3 = 6.

Time complexity analysis:
Because we only scan the matrix for one time, time complexity is O(m*n). m = number of rows, n = number of columns.

```
public class Solution {
    public int findBlackPixel(char[][] picture, int N) {
        int m = picture.length;
        if (m == 0) return 0;
        int n = picture[0].length;
        if (n == 0) return 0;
        
        Map<String, Integer> map = new HashMap<>();
        int[] colCount = new int[n];
        
        for (int i = 0; i < m; i++) {
            String key = scanRow(picture, i, N, colCount);
            if (key.length() != 0) {
                map.put(key, map.getOrDefault(key, 0) + 1);
            }
        }
        
        int result = 0;
        for (String key : map.keySet()) {
            if (map.get(key) == N) {
                for (int j = 0; j < n; j++) {
                    if (key.charAt(j) == 'B' && colCount[j] == N) {
                        result += N;
                    }
                }
            }
        }
        
        return result;
    }
    
    private String scanRow(char[][] picture, int row, int target, int[] colCount) {
        int n = picture[0].length;
        int rowCount = 0;
        StringBuilder sb = new StringBuilder();
        
        for (int j = 0; j < n; j++) {
            if (picture[row][j] == 'B') {
                rowCount++;
                colCount[j]++;
            }
            sb.append(picture[row][j]);
        }
        
        if (rowCount == target) return sb.toString();
        return "";
    }
}
```
</p>


### Explanation of rule 2, which confused me for a long time.
- Author: odingg
- Creation Date: Wed Mar 08 2017 08:59:37 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Mar 08 2017 08:59:37 GMT+0800 (Singapore Standard Time)

<p>
I was very confused by the rule 2 before I read the solutions. Then I realized that the rule 2 means: 
"For all rows that have a black pixel at column C, they should be exactly the same as the row R ***in terms of looking***"

So in the example, row 1 looks exactly the same as the row 0, but row 3 looks not the same as row 0.

I got confused by thinking row 1 and row 0 being the same means that the number of black pixels they have are the same or all other possibilities. Maybe that's because English is not my native language.
</p>


### Short Python
- Author: StefanPochmann
- Creation Date: Mon Mar 06 2017 04:36:31 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 06 2017 04:36:31 GMT+0800 (Singapore Standard Time)

<p>
Mainly I group and count equal rows. Look for rows that appear N times and that have N black pixels. If you find one, add N for each of its black columns that doesn't have extra black pixels (in other rows).

    def findBlackPixel(self, picture, N):
        ctr = collections.Counter(map(tuple, picture))
        cols = [col.count('B') for col in zip(*picture)]
        return sum(N * zip(row, cols).count(('B', N))
                   for row, count in ctr.items()
                   if count == N == row.count('B'))
</p>


