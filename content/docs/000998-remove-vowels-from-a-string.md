---
title: "Remove Vowels from a String"
weight: 998
#id: "remove-vowels-from-a-string"
---
## Description
<div class="description">
<p>Given a string <code>S</code>, remove the vowels <code>&#39;a&#39;</code>, <code>&#39;e&#39;</code>, <code>&#39;i&#39;</code>, <code>&#39;o&#39;</code>, and <code>&#39;u&#39;</code> from it, and return the new string.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;leetcodeisacommunityforcoders&quot;</span>
<strong>Output: </strong><span id="example-output-1">&quot;ltcdscmmntyfrcdrs&quot;</span>
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">&quot;aeiou&quot;</span>
<strong>Output: </strong><span id="example-output-2">&quot;&quot;</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>S</code> consists of lowercase English letters only.</li>
	<li><code>1 &lt;= S.length &lt;= 1000</code></li>
</ol>

</div>

## Tags
- String (string)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python] 1-Lines
- Author: lee215
- Creation Date: Sun Jul 14 2019 00:06:12 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 12 2019 16:25:27 GMT+0800 (Singapore Standard Time)

<p>
**Java, replace all vowels:**
```java
    public String removeVowels(String S) {
        return S.replaceAll("[aeiou]", "");
    }
```

**Python:**
```python
    def removeVowels(self, S):
        return "".join(c for c in S if c not in "aeiou")
```

**Python**
Suggested by @721
```python
    def removeVowels(self, S):str
        return re.sub(\'a|e|i|o|u\', \'\', S)
```
</p>


### Simple Python Solution
- Author: 5tigerjelly
- Creation Date: Wed Jul 17 2019 07:36:55 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 17 2019 07:37:03 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def removeVowels(self, S: str) -> str:
        s = set(\'aeiou\')
        ret = ""
        for ch in S:
            if ch not in s:
                ret += ch
        return ret
```		
</p>


### Java StringBuffer 0ms - with written approach to answering in interview.
- Author: user5207B
- Creation Date: Mon Dec 16 2019 01:42:52 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 16 2019 01:44:16 GMT+0800 (Singapore Standard Time)

<p>
Java solution that is faster than 100% of other submissions according to site. It is also super readable which is something interviewers will care about. No one wants to code with a coworker writing single line confusing regex solutions when there is an easier to read short solution.

Many solutions use standard functions like String.replaceAll(). Its good to call this out in an interview that the functionality exists, but be ready to answer the question in full. The interviewer will appreciate you knowing of the function, but what they really want to see is you coding.

At the end of this post is a function that uses the StringBuffer class, this is a great class to pull out in this instance because of its ability to be manipulated easily and quickly output as a string. You want to avoid appending characters with a string by doing the below:

```
// Pseudo code
string = string + character;
```

In Java this could potentially bring your solution to an O(n^2) solution. In Java each addition of the character to the string is effectively copying the string over with the added character. Effectively making each added character mean checking every other character again. This will be one of the things the interviewers will look for, to make sure that you are aware of what classes you are using and if their methods increase the time complexity of your solution.

Another thing is I added a function called isVowel(), in the interview you can talk about how sometimes people count \'y\' as a vowel, or perhaps as the functionality grows you might want to check for other characters. Having the checking logic in its own function makes it easy to adapt the program for other checks without affecting its algorithmic code. It will show the interviewer that you\'re thinking about how to structure your code so its easier to adapt for futuer needs. 

This solution as it is O(n) as it only touches each character in String S once. As the size of S grows to infinity, the time complexity grows at a linear rate.

```
class Solution {
    public String removeVowels(String S) {
        StringBuffer sb = new StringBuffer(S.length());
        
		// For each character in String S, see if its a vowel, if it isn\'t then add to StringBuffer
        for(int i = 0; i < S.length(); i++){
            if(!isVowel(S.charAt(i))){
                sb.append(S.charAt(i));
            }
        }
        
        return sb.toString();     
    }
    
	// Using a function to check values, this makes it easy to control what to check for if things change
    public boolean isVowel(char c){
		// if list of things to check for grows, could switch to a hashset to minimize comparisons.
        if(c == \'a\' || c == \'e\' || c == \'i\' || c == \'o\' || c == \'u\'){
            return true;
        }
        
        return false;
    }
}
```
</p>


