---
title: "Matrix Block Sum"
weight: 1117
#id: "matrix-block-sum"
---
## Description
<div class="description">
Given a <code>m * n</code> matrix&nbsp;<code>mat</code>&nbsp;and an integer <code>K</code>, return a matrix <code>answer</code> where each <code>answer[i][j]</code>&nbsp;is the sum of all elements <code>mat[r][c]</code> for <code>i - K &lt;= r &lt;= i + K, j - K &lt;= c &lt;= j + K</code>, and <code>(r, c)</code> is a valid position in the matrix.
<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> mat = [[1,2,3],[4,5,6],[7,8,9]], K = 1
<strong>Output:</strong> [[12,21,16],[27,45,33],[24,39,28]]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> mat = [[1,2,3],[4,5,6],[7,8,9]], K = 2
<strong>Output:</strong> [[45,45,45],[45,45,45],[45,45,45]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m ==&nbsp;mat.length</code></li>
	<li><code>n ==&nbsp;mat[i].length</code></li>
	<li><code>1 &lt;= m, n, K &lt;= 100</code></li>
	<li><code>1 &lt;= mat[i][j] &lt;= 100</code></li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Apple - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] Prefix/Range sum w/ analysis, similar to LC 304/7/8
- Author: rock
- Creation Date: Sun Jan 12 2020 00:01:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jul 18 2020 02:22:21 GMT+0800 (Singapore Standard Time)

<p>
**Note:**
1. `rangeSum[i + 1][j + 1]` corresponds to cell `(i, j)`; 
2. `rangeSum[0][j]` and `rangeSum[i][0]` are all dummy values, which are used for the convenience of computation of DP state transmission formula.

To calculate `rangeSum`, the ideas are as below - credit to @haoel
```
+-----+-+-------+     +--------+-----+     +-----+---------+     +-----+--------+
|     | |       |     |        |     |     |     |         |     |     |        |
|     | |       |     |        |     |     |     |         |     |     |        |
+-----+-+       |     +--------+     |     |     |         |     +-----+        |
|     | |       |  =  |              |  +  |     |         |  -  |              | + mat[i][j]
+-----+-+       |     |              |     +-----+         |     |              |
|               |     |              |     |               |     |              |
|               |     |              |     |               |     |              |
+---------------+     +--------------+     +---------------+     +--------------+

rangeSum[i+1][j+1] =  rangeSum[i][j+1] + rangeSum[i+1][j]    -   rangeSum[i][j]   + mat[i][j]
```
So, we use the same idea to find the specific block\'s sum. - credit to @haoel
```
+---------------+   +--------------+   +---------------+   +--------------+   +--------------+
|               |   |         |    |   |   |           |   |         |    |   |   |          |
|   (r1,c1)     |   |         |    |   |   |           |   |         |    |   |   |          |
|   +------+    |   |         |    |   |   |           |   +---------+    |   +---+          |
|   |      |    | = |         |    | - |   |           | - |      (r1,c2) | + |   (r1,c1)    |
|   |      |    |   |         |    |   |   |           |   |              |   |              |
|   +------+    |   +---------+    |   +---+           |   |              |   |              |
|        (r2,c2)|   |       (r2,c2)|   |   (r2,c1)     |   |              |   |              |
+---------------+   +--------------+   +---------------+   +--------------+   +--------------+
```
If the above is still not clear enough for you, this [website](https://computersciencesource.wordpress.com/2010/09/03/computer-vision-the-integral-image/) explains the concept really well. - credit to **@saidthesky**.

----

```java
    public int[][] matrixBlockSum(int[][] mat, int K) {
        int m = mat.length, n = mat[0].length;
        int[][] rangeSum = new int[m + 1][n + 1];
        for (int i = 0; i < m; ++i)
            for (int j = 0; j < n; ++j)
                rangeSum[i + 1][j + 1] = rangeSum[i + 1][j] + rangeSum[i][j + 1] - rangeSum[i][j] + mat[i][j];
        int[][] ans = new int[m][n];
        for (int i = 0; i < m; ++i)
            for (int j = 0; j < n; ++j) {
                int r1 = Math.max(0, i - K), c1 = Math.max(0, j - K), r2 = Math.min(m, i + K + 1), c2 = Math.min(n, j + K + 1);
                ans[i][j] = rangeSum[r2][c2] - rangeSum[r2][c1] - rangeSum[r1][c2] + rangeSum[r1][c1];
            }
        return ans;
    }
```
```python
    def matrixBlockSum(self, mat: List[List[int]], K: int) -> List[List[int]]:
        m, n = len(mat), len(mat[0])
        rangeSum = [[0] * (n + 1) for _ in range(m + 1)]
        for i in range(m):
            for j in range(n):
                rangeSum[i + 1][j + 1] = rangeSum[i + 1][j] + rangeSum[i][j + 1] - rangeSum[i][j] + mat[i][j]
        ans = [[0] * n for _ in range(m)]        
        for i in range(m):
            for j in range(n):
                r1, c1, r2, c2 = max(0, i - K), max(0, j - K), min(m, i + K + 1), min(n, j + K + 1)
                ans[i][j] = rangeSum[r2][c2] - rangeSum[r1][c2] - rangeSum[r2][c1] + rangeSum[r1][c1]
        return ans
```


**Analysis:**

Time & space: O(m * n).

----

If you need more practice with `range/prefix sum`, please refer to:
[304. Range Sum Query 2D - Immutable](https://leetcode.com/problems/range-sum-query-2d-immutable)
[307. Range Sum Query - Mutable](https://leetcode.com/problems/range-sum-query-mutable)
[308. Range Sum Query 2D - Mutable: Premium](https://leetcode.com/problems/range-sum-query-2d-mutable)

</p>


### [Java] Prefix sum with Picture explain - Clean code - O(m*n)
- Author: hiepit
- Creation Date: Sun Jan 12 2020 00:02:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Apr 11 2020 14:05:08 GMT+0800 (Singapore Standard Time)

<p>
**Similar problems:**
- [304. Range Sum Query 2D - Immutable](https://leetcode.com/problems/range-sum-query-2d-immutable/discuss/572648/Java-Prefix-sum-with-Picture-explain-Clean-code)
- [1292. Maximum Side Length of a Square with Sum Less than or Equal to Threshold](https://leetcode.com/problems/maximum-side-length-of-a-square-with-sum-less-than-or-equal-to-threshold)

**Image Explain**
![image](https://assets.leetcode.com/users/hiepit/image_1578762431.png)

```java
class Solution {
    public int[][] matrixBlockSum(int[][] mat, int K) {
        int m = mat.length, n = mat[0].length;
        int[][] sum = new int[m + 1][n + 1]; // sum[i][j] is sum of all elements from rectangle (0,0,i,j) as left, top, right, bottom corresponding
        for (int r = 1; r <= m; r++) {
            for (int c = 1; c <= n; c++) {
                sum[r][c] = mat[r - 1][c - 1] + sum[r - 1][c] + sum[r][c - 1] - sum[r - 1][c - 1];
            }
        }
        int[][] ans = new int[m][n];
        for (int r = 0; r < m; r++) {
            for (int c = 0; c < n; c++) {
                int r1 = Math.max(0, r - K), c1 = Math.max(0, c - K);
                int r2 = Math.min(m - 1, r + K), c2 = Math.min(n - 1, c + K);
                r1++; c1++; r2++; c2++; // Since `sum` start with 1 so we need to increase r1, c1, r2, c2 by 1
                ans[r][c] = sum[r2][c2] - sum[r2][c1-1] - sum[r1-1][c2] + sum[r1-1][c1-1];
            }
        }
        return ans;
    }
}
```

**Complexity:**
- Time: O(m\*n), `m` is the number of rows, `n` is the number of columns of `mat`
- Space: O(m\*n)
</p>


### C++ prefix sum with explanation
- Author: ziyanma
- Creation Date: Thu Feb 06 2020 18:13:58 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Feb 06 2020 18:14:41 GMT+0800 (Singapore Standard Time)

<p>
This question is similar to a vector segment sum. 

To extract sum of an arbitrary sub-array, we first form a prefix sum array.
[1, 2, 3, 4, 5] // original vector
[1, 3, 6, 10, 15] // prefix sum

To get sum of a sub array from index a to index b, sum(a, b), we can instead calculate prefix(b) - prefix(a-1)

Similar in 2D, we form prefix matrix, where sum[i][j] = sum of elements on top left of original matrix
[1, 2, 3]
[4, 5, 6]
[7, 8, 9]

[1, 3, 6]
[5, 12, 21]
[12, 27, 45]

Similarly, Sum of an arbitrary block from (i-K, j-K) to (i+K, j+K)
ans(i, j) = prefix(i+K, j+K) - prefix(i+K, j-K-1) - prefix (i-K-1, j+K) + prefix(i-K-1, j-K-1);

```
class Solution {
public: 
    int sizeX, sizeY;
    int extractSum(int i, int j, const vector<vector<int> >& sum) {
        if (i < 0 || j < 0) return 0;
        if (i >= sizeX) i = sizeX - 1;
        if (j >= sizeY) j = sizeY - 1;
        return sum[i][j];
    }
        
    vector<vector<int>> matrixBlockSum(vector<vector<int>>& mat, int K) {
        sizeX = mat.size();
        sizeY = mat[0].size();
        
        vector<vector<int>> sum(sizeX, vector<int>(sizeY, 0));
        // Calculate prefix matrix 
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                sum[i][j] = mat[i][j] + extractSum(i-1, j, sum) + extractSum(i, j-1, sum) - extractSum(i-1, j-1, sum);
            }
        }
        
        // Use prefix matrix to calculate our sum
        vector<vector<int>> ans(sizeX, vector<int>(sizeY, 0));
        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                ans[i][j] = extractSum(i+K, j+K, sum) - extractSum(i+K, j-K-1,sum) - extractSum (i-K-1, j+K, sum) + extractSum(i-K-1, j-K-1, sum);
            }
        }
        
        return ans;
    }
};
```
</p>


