---
title: "Rank Teams by Votes"
weight: 1271
#id: "rank-teams-by-votes"
---
## Description
<div class="description">
<p>In a special ranking system,&nbsp;each voter gives a rank from highest to lowest to all teams participated in the competition.</p>

<p>The ordering of teams is decided by who received the most position-one votes. If two or more teams tie in the first position, we consider the second position to resolve the conflict, if they tie again, we continue this process until the ties are resolved. If two or more teams are still tied after considering all positions, we rank them alphabetically based on their team letter.</p>

<p>Given an array of strings <code>votes</code> which is the votes of all voters in the ranking systems. Sort all teams according to the ranking system described above.</p>

<p>Return <em>a string of all teams</em> <strong>sorted</strong> by the ranking system.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> votes = [&quot;ABC&quot;,&quot;ACB&quot;,&quot;ABC&quot;,&quot;ACB&quot;,&quot;ACB&quot;]
<strong>Output:</strong> &quot;ACB&quot;
<strong>Explanation:</strong> Team A was ranked first place by 5 voters. No other team was voted as first place so team A is the first team.
Team B was ranked second by 2 voters and was ranked third by 3 voters.
Team C was ranked second by 3 voters and was ranked third by 2 voters.
As most of the voters ranked C second, team C is the second team and team B is the third.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> votes = [&quot;WXYZ&quot;,&quot;XYZW&quot;]
<strong>Output:</strong> &quot;XWYZ&quot;
<strong>Explanation:</strong> X is the winner due to tie-breaking rule. X has same votes as W for the first position but X has one vote as second position while W doesn&#39;t have any votes as second position. 
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> votes = [&quot;ZMNAGUEDSJYLBOPHRQICWFXTVK&quot;]
<strong>Output:</strong> &quot;ZMNAGUEDSJYLBOPHRQICWFXTVK&quot;
<strong>Explanation:</strong> Only one voter so his votes are used for the ranking.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> votes = [&quot;BCA&quot;,&quot;CAB&quot;,&quot;CBA&quot;,&quot;ABC&quot;,&quot;ACB&quot;,&quot;BAC&quot;]
<strong>Output:</strong> &quot;ABC&quot;
<strong>Explanation:</strong> 
Team A was ranked first by 2 voters, second by 2 voters and third by 2 voters.
Team B was ranked first by 2 voters, second by 2 voters and third by 2 voters.
Team C was ranked first by 2 voters, second by 2 voters and third by 2 voters.
There is a tie and we rank teams ascending by their IDs.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> votes = [&quot;M&quot;,&quot;M&quot;,&quot;M&quot;,&quot;M&quot;]
<strong>Output:</strong> &quot;M&quot;
<strong>Explanation:</strong> Only team M in the competition so it has the first rank.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= votes.length &lt;= 1000</code></li>
	<li><code>1 &lt;= votes[i].length &lt;= 26</code></li>
	<li><code>votes[i].length ==&nbsp;votes[j].length</code> for&nbsp;<code>0 &lt;= i, j &lt; votes.length</code>.</li>
	<li><code>votes[i][j]</code> is an English <strong>upper-case</strong> letter.</li>
	<li>All characters of <code>votes[i]</code> are unique.</li>
	<li>All the characters&nbsp;that occur&nbsp;in <code>votes[0]</code> <strong>also&nbsp;occur</strong>&nbsp;in <code>votes[j]</code> where <code>1 &lt;= j &lt; votes.length</code>.</li>
</ul>

</div>

## Tags
- Array (array)
- Sort (sort)

## Companies
- Google - 2 (taggedByAdmin: true)
- Atlassian - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java, O(26n+(26^2 * log26)), Sort by high rank vote to low rank vote
- Author: ToffeeLu
- Creation Date: Sun Mar 01 2020 12:03:01 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 02 2020 19:53:29 GMT+0800 (Singapore Standard Time)

<p>
```java
class Solution {
    public String rankTeams(String[] votes) {
      Map<Character, int[]> map = new HashMap<>();
      int l = votes[0].length();
      for(String vote : votes){
        for(int i = 0; i < l; i++){
          char c = vote.charAt(i);
          map.putIfAbsent(c, new int[l]);
          map.get(c)[i]++;
        }
      }
      
      List<Character> list = new ArrayList<>(map.keySet());
      Collections.sort(list, (a,b) -> {
        for(int i = 0; i < l; i++){
          if(map.get(a)[i] != map.get(b)[i]){
            return map.get(b)[i] - map.get(a)[i];
          }
        }
        return a - b;
      });
      
      StringBuilder sb = new StringBuilder();
      for(char c : list){
        sb.append(c);
      }
      return sb.toString();
    }
}

```
</p>


### [C++/Python] Count Votes
- Author: lee215
- Creation Date: Sun Mar 01 2020 12:04:23 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 06 2020 15:21:54 GMT+0800 (Singapore Standard Time)

<p>
# **Explanation**
Count the rank of vote for each candidate.
Sort all teams according to the ranking system.
<br>

# **Complexity**
Time `O(NM)` for iterating
Time `O(MMlogM)` for sorting
Space `O(M^2)`
where `N = votes.length` and `M = votes[0].length <= 26`
<br>


**C++**
```cpp
    string rankTeams(vector<string>& votes) {
        vector<vector<int>> count(26, vector<int>(27));
        for (char& c: votes[0])
            count[c - \'A\'][26] = c;

        for (string& vote: votes)
            for (int i = 0; i < vote.length(); ++i)
                --count[vote[i] - \'A\'][i];
        sort(count.begin(), count.end());
        string res;
        for (int i = 0; i < votes[0].length(); ++i)
            res += count[i][26];
        return res;
    }
```

**Python:**
@lenchen1112 helped improve it.
```py
    def rankTeams(self, votes):
        count = {v: [0] * len(votes[0]) + [v] for v in votes[0]}
        for vote in votes:
            for i, v in enumerate(vote):
                count[v][i] -= 1
        return \'\'.join(sorted(votes[0], key=count.get))
```

</p>


### [Java] Arrays.sort, n * l + 26log26 ugly solution
- Author: ycj28c
- Creation Date: Sun Mar 01 2020 12:21:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 01 2020 15:07:25 GMT+0800 (Singapore Standard Time)

<p>
```
class Vote {
    public Character c;
    public int[] cnt;
    public Vote(char c){
        cnt = new int[26];
        this.c = c;
    }
}
class Solution {
    public String rankTeams(String[] votes) {
        Vote[] vv = new Vote[26];
        for(int i=0;i<26;i++){
            vv[i] = new Vote((char) (i + \'A\'));
        }
        Set<Character> set = new HashSet<>();
        for(String item : votes){
            for(int i=0;i<item.length();i++){
                //System.out.println(item.charAt(i));
                vv[item.charAt(i) - \'A\'].cnt[i]++;
                set.add(item.charAt(i));
            }
        }
        Arrays.sort(vv, new Comparator<Vote>() {
            @Override
            public int compare(Vote o1, Vote o2) {
                if(o1.cnt[0] == o2.cnt[0]){
                    if(o1.cnt[1] == o2.cnt[1]){
                        if(o1.cnt[2] == o2.cnt[2]){
                            if(o1.cnt[3] == o2.cnt[3]){
                                if(o1.cnt[4] == o2.cnt[4]){
                                    if(o1.cnt[5] == o2.cnt[5]){
                                        if(o1.cnt[6] == o2.cnt[6]){
                                            if(o1.cnt[7] == o2.cnt[7]){
                                                if(o1.cnt[8] == o2.cnt[8]){
                                                    if(o1.cnt[9] == o2.cnt[9]){
                                                        if(o1.cnt[10] == o2.cnt[10]){
                                                            if(o1.cnt[11] == o2.cnt[11]){
                                                                if(o1.cnt[12] == o2.cnt[12]){
                                                                    if(o1.cnt[13] == o2.cnt[13]){
                                                                        if(o1.cnt[14] == o2.cnt[14]){
                                                                            if(o1.cnt[15] == o2.cnt[15]){
                                                                                if(o1.cnt[16] == o2.cnt[16]){
                                                                                    if(o1.cnt[17] == o2.cnt[17]){
                                                                                        if(o1.cnt[18] == o2.cnt[18]){
                                                                                            if(o1.cnt[19] == o2.cnt[19]){
                                                                                                if(o1.cnt[20] == o2.cnt[20]){
                                                                                                    if(o1.cnt[21] == o2.cnt[21]){
                                                                                                        if(o1.cnt[22] == o2.cnt[22]){
                                                                                                            if(o1.cnt[23] == o2.cnt[23]){
                                                                                                                if(o1.cnt[24] == o2.cnt[24]){
                                                                                                                    if(o1.cnt[25] == o2.cnt[25]){
                                                                                                                        return o1.c.compareTo(o2.c);
                                                                                                                    } else {
                                                                                                                        return o2.cnt[25] - o1.cnt[25];
                                                                                                                    }
                                                                                                                } else {
                                                                                                                    return o2.cnt[24] - o1.cnt[24];
                                                                                                                }
                                                                                                            } else {
                                                                                                                return o2.cnt[23] - o1.cnt[23];
                                                                                                            }
                                                                                                        } else {
                                                                                                            return o2.cnt[22] - o1.cnt[22];
                                                                                                        }
                                                                                                    } else {
                                                                                                        return o2.cnt[21] - o1.cnt[21];
                                                                                                    }
                                                                                                } else {
                                                                                                    return o2.cnt[20] - o1.cnt[20];
                                                                                                }
                                                                                            } else {
                                                                                                return o2.cnt[19] - o1.cnt[19];
                                                                                            }
                                                                                        } else {
                                                                                            return o2.cnt[18] - o1.cnt[18];
                                                                                        }
                                                                                    } else {
                                                                                        return o2.cnt[17] - o1.cnt[17];
                                                                                    }
                                                                                } else {
                                                                                    return o2.cnt[16] - o1.cnt[16];
                                                                                }
                                                                            } else {
                                                                                return o2.cnt[15] - o1.cnt[15];
                                                                            }
                                                                        } else {
                                                                            return o2.cnt[14] - o1.cnt[14];
                                                                        }
                                                                    } else {
                                                                        return o2.cnt[13] - o1.cnt[13];
                                                                    }
                                                                } else {
                                                                    return o2.cnt[12] - o1.cnt[12];
                                                                }
                                                            } else {
                                                                return o2.cnt[11] - o1.cnt[11];
                                                            }
                                                        } else {
                                                            return o2.cnt[10] - o1.cnt[10];
                                                        }
                                                    } else {
                                                        return o2.cnt[9] - o1.cnt[9];
                                                    }
                                                } else {
                                                    return o2.cnt[8] - o1.cnt[8];
                                                }
                                            } else {
                                                return o2.cnt[7] - o1.cnt[7];
                                            }
                                        } else {
                                            return o2.cnt[6] - o1.cnt[6];
                                        }
                                    } else {
                                        return o2.cnt[5] - o1.cnt[5];
                                    }
                                } else {
                                    return o2.cnt[4] - o1.cnt[4];
                                }
                            } else {
                                return o2.cnt[3] - o1.cnt[3];
                            }
                        } else {
                            return o2.cnt[2] - o1.cnt[2];
                        }
                    } else {
                        return o2.cnt[1] - o1.cnt[1];   
                    }
                } else {
                    return o2.cnt[0] - o1.cnt[0];
                }
            }
        });

        StringBuilder sb = new StringBuilder();
        for(int i=0;i<26;i++){
           if(set.contains(vv[i].c)) sb.append(vv[i].c);
        }
        return sb.toString();
    }
}
```
</p>


