---
title: "Minimum Number of Days to Eat N Oranges"
weight: 1419
#id: "minimum-number-of-days-to-eat-n-oranges"
---
## Description
<div class="description">
<p>There are <code>n</code> oranges in the kitchen and you decided to eat some of these oranges every day as follows:</p>

<ul>
	<li>Eat one orange.</li>
	<li>If the number of remaining oranges (<code>n</code>) is divisible by 2 then you can eat&nbsp; n/2 oranges.</li>
	<li>If the number of remaining oranges (<code>n</code>) is divisible by 3&nbsp;then you can eat&nbsp; 2*(n/3)&nbsp;oranges.</li>
</ul>

<p>You can only choose one of the actions per day.</p>

<p>Return the minimum number of days to eat <code>n</code> oranges.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 10
<strong>Output:</strong> 4
<strong>Explanation:</strong> You have 10 oranges.
Day 1: Eat 1 orange,  10 - 1 = 9.  
Day 2: Eat 6 oranges, 9 - 2*(9/3) = 9 - 6 = 3. (Since 9 is divisible by 3)
Day 3: Eat 2 oranges, 3 - 2*(3/3) = 3 - 2 = 1. 
Day 4: Eat the last orange  1 - 1  = 0.
You need at least 4 days to eat the 10 oranges.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 6
<strong>Output:</strong> 3
<strong>Explanation:</strong> You have 6 oranges.
Day 1: Eat 3 oranges, 6 - 6/2 = 6 - 3 = 3. (Since 6 is divisible by 2).
Day 2: Eat 2 oranges, 3 - 2*(3/3) = 3 - 2 = 1. (Since 3 is divisible by 3)
Day 3: Eat the last orange  1 - 1  = 0.
You need at least 3 days to eat the 6 oranges.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 1
<strong>Output:</strong> 1
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 56
<strong>Output:</strong> 6
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 2*10^9</code></li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java/Python 5 lines
- Author: votrubac
- Creation Date: Sun Aug 16 2020 12:05:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 22 2020 01:34:40 GMT+0800 (Singapore Standard Time)

<p>
#### Intuition
As pleasant as it seems, it does not make sense to eat oranges one by one.

So, the choice we have is to eat `n % 2` oranges one-by-one and then swallow `n / 2`, or eat `n % 3` oranges so that we can gobble `2 * n / 3`.

As usual, we use DP to memoise results and avoid re-computation. Since our numbers can be large, it\'s better to use a hash map instead of an array.

> Why doesn\'t make sense to eat 2 oranges first, and then do `n / 2`? Let\'s say `k` is one of the divisors we can use. `(n - k) / k` equals `n / k - 1`. We can reach `n / k - 1` in two actions: `n / k` and `n / k - 1`, while `(n - k) / k` requires `k + 1` actions. If `k == 2`, then we have 2 vs. 3 actions, and the difference grows with `k`.

**C++**
If you want the fastest runtime (~4 ms), make `dp` global.

```cpp
unordered_map<int, int> dp;
int minDays(int n) {
    if (n <= 1)
        return n;
    if (dp.count(n) == 0)
        dp[n] = 1 + min(n % 2 + minDays(n / 2), n % 3 + minDays(n / 3));
    return dp[n];
}
```
**Java**
```java
Map<Integer, Integer> dp = new HashMap<>();
public int minDays(int n) {
    if (n <= 1)
        return n;
    if (!dp.containsKey(n))
        dp.put(n, 1 + Math.min(n % 2 + minDays(n / 2), n % 3 + minDays(n / 3)));
    return dp.get(n);
}
```
**Python**
```python
class Solution:
    @lru_cache()
    def minDays(self, n: int) -> int:
        if n <= 1:
            return n;
        return 1 + min(n % 2 + self.minDays(n // 2), n % 3 + self.minDays(n // 3));   
```
</p>


### [Python/Golang] Intuitive solution with Proof
- Author: yanrucheng
- Creation Date: Sun Aug 16 2020 12:01:19 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 18 2020 08:38:14 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
The key idea is that we should never take more than 2 consecutive `-1` operations.

**Proof**
Let\'s prove by contradiction.

Suppose there exists some n, where we need to take 3 consecutive `-1` operations for the optimal schedule.
Formally, all of the following holds.
- `minDays(n - 3) + 3 < minDays(n - 2) + 2`, meaning taking 2 `-1` is worse than taking 3 `-1`
- `minDays(n - 3) + 3 < minDays(n - 1) + 1`, meaning taking 1 `-1` is worse than taking 3 `-1`
- `minDays(n - 3) + 3 < minDays(n )`, meaning taking 0 `-1` is worse than taking 3 `-1`

- if the first operation we take for `n - 3` is `/2`, then we ends in `(n - 3) / 2 = (n - 1) / 2 - 1`
	- we can easily tell minDays((n - 3) / 2) can be achieved by `-1, /2 - 1` instead of `-1, -1, -1, /2`. Thus taking 3 `-1` is not optimal
- if the first operation we take for `n - 3` is `/3`, then we ends in `(n - 3) / 3 = n / 3 - 1`
	- we can easily tell minDays((n - 3) / 3) can be achieved by `-1, /3` instead of `-1, -1, -1, /3`. Thus taking 3 `-1` is not optimal

This process can be extended to taking n consecutive `-1` cases (n >= 3)

Thus, the conclusion is that we should always only take 2 consecutive `-1` at most.

Thanks for reading my post. If you find it helpful, please upvote. Your upvote means a lot to me.

**Complexity**
- Time & Space: `O(log^2(n))`, explained by [@coder206](https://leetcode.com/problems/minimum-number-of-days-to-eat-n-oranges/discuss/794847/Polylogarithmic-solution)

**Golang**
```
/* dp */
var memo = make(map[int]int)

func minDays(n int) int {
    if n < 3 { return n }
    if _, ok := memo[n]; !ok {
        memo[n] = 1 + min(n % 2 + minDays(n / 2), n % 3 + minDays(n / 3))
    }
    return memo[n]
    // AC: 0 ms, beats 100.00%, 2.6 MB, beats 86.49%
}

func min(a, b int) int {
    if a < b { return a }
    return b
}
```

**Python**
```
from functools import lru_cache
class Solution:
    def minDays(self, n: int) -> int:
        
        @lru_cache(None)
        def helper(n):
            if n < 3:
                return n
            return 1 + min(n % 2 + helper(n // 2), n % 3 + helper(n // 3))
        
        return helper(n)
        # AC: 44 ms, beats 100.00%, 14.3 MB, beats 75.00%
```
</p>


### [Java] Easy Understanding BFS
- Author: Kieran-
- Creation Date: Sun Aug 16 2020 12:00:39 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 16 2020 12:30:29 GMT+0800 (Singapore Standard Time)

<p>
Starting from n
1. if it is divisable by 3, add n / 3
1. if it is divisable by 2, add n / 2
1. always add n - 1

Since as n goes up, it might be MLE, I added a set to remember whether this number was going through, if it has been visited, just continue to work on the remaining value in queue.

Ps: I think it is a little bit easy for this BFS for the fourth question, usually I expect dp for the fourth question.

```
class Solution {
    public int minDays(int n) {
        Queue<Integer> q = new LinkedList<>();
        q.offer(n);
        int res = 0;
        Set<Integer> set = new HashSet<>();
        while(!q.isEmpty()){
            res++;
            int size = q.size();
            for(int i = 0; i < size; i++){
                int cur = q.poll();
                if(cur == 0){
                    return res - 1;
                }
                if(set.contains(cur)){
                    continue;
                }
                else{
                    set.add(cur);
                }
                if(cur % 3 == 0){
                    q.offer(cur / 3);
                }
                if(cur % 2 == 0){
                    q.offer(cur / 2);
                }
                q.offer(cur - 1);
            }
        }
        
        return res;
        
    }
}
```
</p>


