---
title: "Score After Flipping Matrix"
weight: 805
#id: "score-after-flipping-matrix"
---
## Description
<div class="description">
<p>We have a two dimensional matrix&nbsp;<code>A</code> where each value is <code>0</code> or <code>1</code>.</p>

<p>A move consists of choosing any row or column, and toggling each value in that row or column: changing all <code>0</code>s to <code>1</code>s, and all <code>1</code>s to <code>0</code>s.</p>

<p>After making any number of moves, every row of this matrix is interpreted as a binary number, and the score of the matrix is the sum of these numbers.</p>

<p>Return the highest possible&nbsp;score.</p>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[[0,0,1,1],[1,0,1,0],[1,1,0,0]]</span>
<strong>Output: </strong><span id="example-output-1">39</span>
<strong>Explanation:
</strong>Toggled to <span id="example-input-1-1">[[1,1,1,1],[1,0,0,1],[1,1,1,1]].
0b1111 + 0b1001 + 0b1111 = 15 + 9 + 15 = 39</span></pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 20</code></li>
	<li><code>1 &lt;= A[0].length &lt;= 20</code></li>
	<li><code>A[i][j]</code>&nbsp;is <code>0</code> or <code>1</code>.</li>
</ol>
</div>

</div>

## Tags
- Greedy (greedy)

## Companies
- IIT Bombay - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Brute Force

**Intuition**

Notice that a `1` in the $$i$$th column from the right, contributes $$2^i$$ to the score.

Say we are finished toggling the rows in some configuration.  Then for each column, (to maximize the score), we'll toggle the column if it would increase the number of `1`s.

We can brute force over every possible way to toggle rows.

**Algorithm**

Say the matrix has `R` rows and `C` columns.

For each `state`, the transition `trans = state ^ (state-1)` represents the rows that must be toggled to get into the state of toggled rows represented by (the bits of) `state`.

We'll toggle them, and also maintain the correct column sums of the matrix on the side.

Afterwards, we'll calculate the score.  If for example the last column has a column sum of `3`, then the score is `max(3, R-3)`, where `R-3` represents the score we get from toggling the last column.

In general, the score is increased by `max(col_sum, R - col_sum) * (1 << (C-1-c))`, where the factor `(1 << (C-1-c))` is the power of `2` that each `1` contributes.

Note that this approach may not run in the time allotted.


<iframe src="https://leetcode.com/playground/uCctaYX5/shared" frameBorder="0" width="100%" height="500" name="uCctaYX5"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(2^R * R * C)$$, where $$R, C$$ is the number of rows and columns in the matrix.

* Space Complexity:  $$O(C)$$ in additional space complexity.
<br />
<br />


---
#### Approach 2: Greedy

**Intuition**

Notice that a `1` in the $$i$$th column from the right, contributes $$2^i$$ to the score.

Since $$2^n > 2^{n-1} + 2^{n-2} + \cdots + 2^0$$, maximizing the left-most digit is more important than any other digit.  Thus, the rows should be toggled such that the left-most column is either all `0` or all `1` (so that after toggling the left-most column [if necessary], the left column is all `1`.)

**Algorithm**

If we toggle rows by the first column (`A[r][c] ^= A[r][0]`), then the first column will be all `0`.

Afterwards, the base score is `max(col, R - col)` where `col` is the column sum; and `(1 << (C-1-c))` is the power of 2 that each `1` in that column contributes to the score.

<iframe src="https://leetcode.com/playground/vqAjBe8L/shared" frameBorder="0" width="100%" height="276" name="vqAjBe8L"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(R * C)$$, $$R, C$$ is the number of rows and columns in the matrix.

* Space Complexity:  $$O(1)$$ in additional space complexity.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Easy and Concise
- Author: lee215
- Creation Date: Sun Jul 01 2018 11:05:30 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 15 2020 15:29:56 GMT+0800 (Singapore Standard Time)

<p>
# Explantion
Assume `A` is `M * N`.
1. `A[i][0]` is worth `1 << (N - 1)` points, more than the sum of (`A[i][1] + .. + A[i][N-1]`).
We need to toggle all `A[i][0]` to `1`, here I toggle all lines for `A[i][0] = 0`.
2. `A[i][j]` is worth `1 << (N - 1 - j)`
For every col, I count the current number of `1`s.
After step 1, `A[i][j]` becomes `1` if `A[i][j] == A[i][0]`.
if `M - cur > cur`, we can toggle this column to get more `1`s.
`max(cur, M - cur)` will be the maximum number of `1`s that we can get.
<br>

# **Complexity**:
Time `O(MN)`
Space `O(1)`
<br>

**C++:**
```cpp
    int matrixScore(vector<vector<int>> A) {
        int M = A.size(), N = A[0].size(), res = (1 << (N - 1)) * M;
        for (int j = 1; j < N; j++) {
            int cur = 0;
            for (int i = 0; i < M; i++) cur += A[i][j] == A[i][0];
            res += max(cur, M - cur) * (1 << (N - j - 1));
        }
        return res;
    }
```

**Java:**
```java
    public int matrixScore(int[][] A) {
        int M = A.length, N = A[0].length, res = (1 << (N - 1)) * M;
        for (int j = 1; j < N; j++) {
            int cur = 0;
            for (int i = 0; i < M; i++) cur += A[i][j] == A[i][0] ? 1 : 0;
            res += Math.max(cur, M - cur) * (1 << (N - j - 1));
        }
        return res;
    }
```
**Python:**
```py
    def matrixScore(self, A):
        M, N = len(A), len(A[0])
        res = (1 << N - 1) * M
        for j in range(1, N):
            cur = sum(A[i][j] == A[i][0] for i in range(M))
            res += max(cur, M - cur) * (1 << N - 1 - j)
        return res
```

</p>


### [C++,Java] From Intuition, Un-optimized code to Optimized Code with Detailed Explanation 
- Author: codeforbest
- Creation Date: Sun Jul 01 2018 14:58:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 06 2018 11:29:43 GMT+0800 (Singapore Standard Time)

<p>
First of all, thanks to @lee215 for providing an optimized code. My optimized code is an exact replica of his solution. But, I would like to offer more explanation to people who might find it difficult to understand. Hence, in this post, I start from the thinking process, implementing the thought-process to an un-optimized code and then improving it.

First of all, after looking at this question, the first thing which comes in mind is simulation. But wait, the question doesn\'t state any restrictions, because you can toggle as many rows and columns as you want and however times as you want. Then it means, its practically impossible to simulate all situations. So, what do we do next. Lets understand the structure of the binary number.
A binary number, for example is something like
```
01010
```
Now, think for a moment, how can we maximize it? You guessed it right probably, by maximizing the number of 1\'s, which are as left as possible. Now, see below example,
```
0111 and 1000
```
The first number is 7 and the second is 8. So, overall we decreased the number of 1\'s, but the value has increased as we tried to get the 1 as left as possible. Now you got it? Exactly, if the first bit is zero, flip the binary. So, for our problem, we use this property (call it property 1).First thing which we will do is try to flip all rows, whose first column is zero.
Great, now lets look at another optimization. For a particular column, it would be great if we could maximize the number of 1\'s. Why? Because, if you think carefully, all are at same bit position. Hence, if the number of1\'s increase, the total sum would increase.
Hence, we use this second property(call it property 2). We scan through each column, and check if the number of 1\'s in that column are less than or equal to half the number of rows. If yes, then we flip the column to maximize the number of 1\'s in that column. However, be careful to do this for all columns except first, else you would break the first property.
That\'s it. Now, I hope you would easily understand the below unoptimized code which is written in ```C++.```
```
class Solution {
public:
    void flipRow(vector<vector<int>>& A,int row,int n){
        for(int j = 0;j < n;j++){
            if(A[row][j] == 0)
                A[row][j] = 1;
            else
                A[row][j] = 0;
        }
    }
    
    void flipCol(vector<vector<int>>& A,int col,int m){
        for(int i = 0;i < m;i++){
            if(A[i][col] == 0)
                A[i][col] = 1;
            else
                A[i][col] = 0;
        }
    }
    
    
    int matrixScore(vector<vector<int>>& A) {
        int m = A.size();
        int n = A[0].size();
        vector<int> col(n,0);
        
        for(int i = 0;i < m;i++){
            if(A[i][0] == 0)
                flipRow(A,i,n);
            for(int j = 0;j < n;j++){
                if(A[i][j] == 1)
                    col[j]++;
            }
        }
        
        for(int j = 0;j < n;j++){
            if(col[j] <= m/2)
                flipCol(A,j,m);
        }
        
        int result = 0,sum;
        for(vector<int> v : A){
            sum = 0;
            for(int j = v.size()-1;j >= 0 ;j--){
                if(v[j] == 1)
                    sum += pow(2,v.size()-1-j);
            }
            result += sum;
        }
        
        return result;
    }
};
```

But, if you carefully observe, we are not required to manipulate the matrix. By this, we can avoid unnecessary calculations.

Now, see the below ```Java``` code. I will explain it later.

```
class Solution {
    public int matrixScore(int[][] A) {
        int m = A.length;
        int n = A[0].length;
        int result = 0;
        
        result += (1 << (n-1))*m;
        
        for(int j = 1;j < n;j++){
            int same = 0;
            for(int i = 0;i < m;i++){
                if(A[i][0] == A[i][j])
                    same++;
            }
            result += (1 << (n-1-j))*Math.max(same,m-same);
        }
        
        return result;
    }
}
```

Firstly, we are sure to set all the first bits of all binary numbers as 1. So, first lets calculate the value for them. This is what is done in 
```
result += (1 << (n-1))*m;
```

Next, we start from first column and check the number of values which are same(0 or 1) for each row, with the first column element. Why? Because if they are both 0, then would be naturally flipped to 1(from property 1). If they are 1, then the row would naturally wouldn\'t be flipped(again from property 1). Hence, intuitively, you can consider the variable ```same``` to store the maximum number of 1\'s we have currently.
Now, as said before, we need to maximize ```same```.  intuitively, if ```same``` is the number of 1\'s, ```m-same``` is the number of 0\'s. We take the maximum of two to maximize the number of 1\'s, which will done by either keeping the column unflipped(if currently, number of 1\'s are more than number of 0\'s) or by flipping it as required.
Finally, we add the value for those bit positions for the 1\'s in this line
```result += (1 << (n-1-j))*Math.max(same,m-same);```
 and finally return the result.
 
 I hope you understood the solution now.
 Please let me know if I am wrong somewhere or if its unclear.
 Cheers!!
</p>


### Java, two steps, O(MN)
- Author: Centurion7
- Creation Date: Sun Jul 01 2018 13:18:34 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 06 2018 15:20:02 GMT+0800 (Singapore Standard Time)

<p>
Optimize the total sum in two steps:
1) Flip all rows that start with zero; 
2) Flip all columns where the number of zeros is larger than the number of ones;

```
class Solution {
    public int matrixScore(int[][] A) {
        int N = A.length,
            M = A[0].length;
        
        // Optimize, step1: flip all rows starting with a zero
        for (int i = 0; i < N; ++i) {
            if (A[i][0] == 0) 
                flipRow(A, i);
        }
        
        // Optimize, step 2: flip all columns where the number of zeros is larger than the number of ones
        for (int col = 1; col < M; ++col) {
            int sumCol = 0;
            for (int i = 0; i < N; ++i) 
                sumCol += A[i][col];
            
            if (sumCol * 2 < N) 
                flipCol(A, col);
        }
        
        // Count final sum
        int total = 0;
        for (int i = 0; i < N; ++i) 
            for (int j = 0; j < M; ++j)
                total += A[i][j] * (1 << (M-j-1));
            
        return total;
    }
    
    void flipRow(int[][] a, int r) {
        for (int i = 0; i < a[r].length; ++i)
            a[r][i] = (a[r][i] ^ 1); 
    }
    
    void flipCol(int[][] a, int c) {
        for (int i = 0; i < a.length; ++i)
            a[i][c] = (a[i][c] ^ 1); 
    }
}
```

### Intuition
In binary representation, the following holds true:
* 100 > 011
* 1000 > 0111
* 10000 > 01111
* etc.

That is, the first bit gives us more upside than all the rest bits combined. Thus, first and foremost, we flip all rows that have the largest bit equal to zero (step 1).
Afterwards, we try to maximize what\'s left. We no longer want to flip rows, as this would turn our largest and most valuable bits to zero. Instead, we go over each column and see if it makes sense to flip it. Whenever the number of zeros in a column is more than the number of ones, it makes sense to flip it. That\'s what we do in step 2.

Basically, it\'s just a greedy approach. 
</p>


