---
title: "Add Digits"
weight: 242
#id: "add-digits"
---
## Description
<div class="description">
<p>Given a non-negative integer <code>num</code>, repeatedly add all its digits until the result has only one digit.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> <code>38</code>
<strong>Output:</strong> 2 
<strong>Explanation: </strong>The process is like: <code>3 + 8 = 11</code>, <code>1 + 1 = 2</code>. 
&nbsp;            Since <code>2</code> has only one digit, return it.
</pre>

<p><b>Follow up:</b><br />
Could you do it without any loop/recursion in O(1) runtime?</p>
</div>

## Tags
- Math (math)

## Companies
- Bloomberg - 2 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)
- Adobe - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---

#### Overview

The value we're asked to compute is the so-called 
[Digital Root](https://en.wikipedia.org/wiki/Digital_root).
Logarithmic time solution is easy to write, although the main question here 
is how to fit into a constant time.

<iframe src="https://leetcode.com/playground/BosT2ysJ/shared" frameBorder="0" width="100%" height="310" name="BosT2ysJ"></iframe>

<br />
<br />


---
#### Approach 1: Mathematical: Digital Root

**Formula for the Digital Root**

There is a known formula to compute a digital root in a decimal 
numeral system

$$
dr_{10}(n) = 0, \qquad \text{if } n = 0 
$$

$$
dr_{10}(n) = 9, \qquad \text{if } n = 9 k 
$$

$$
dr_{10}(n) = n \mod 9, \qquad \text{if } n \ne 9 k
$$

How to derive it? Probably, you already know the following proof from school, where it was used for a divisibility by 9: "The original number is divisible by 9 if and only if the sum of its digits is divisible by 9". Let's revise it briefly. 

The input number could be presented in a standard way, where $$d_0, d_1, .. d_k$$ are digits of n:

$$
n = d_0 + d_1 \cdot 10^1 + d_2 \cdot 10^2 + ... + d_k \cdot 10^k
$$

One could expand each power of ten, using the following:

$$
10 = 9 \cdot 1 + 1 \\ 
100 = 99 + 1 = 9 \cdot 11 + 1 \\ 
1000 = 999 + 1 = 9 \cdot 111 + 1 \\
... \\
10^k = 1\underbrace{00..0}_\text{k times} = \underbrace{99..9}_\text{k times} + 1 = 9 \cdot \underbrace{11..1}_\text{k times} + 1
$$

That results in 

$$
n = d_0 + d_1 \cdot (9 \cdot 1 + 1) + d_2 \cdot(9 \cdot 11 + 1) + ... + d_k \cdot (9 \cdot \underbrace{11..1}_\text{k times} + 1)
$$

and could be simplified as

$$
n = (d_0 + d_1 + d_2 + ... + d_k) + 9 \cdot (d_1 \cdot 1 + d_2 \cdot 11 + ... + d_k \cdot \underbrace{11..1}_\text{k times})
$$

The last step is to take the modulo from both sides:

$$
n \mod 9 = (d_0 + d_1 + d_2 + ... + d_k) \mod 9
$$

and to consider separately three cases: the sum of digits is 0, the sum of digits is divisible by 9, and the sum of digits is _not_ divisible by nine:

$$
dr_{10}(n) = 0, \qquad \text{if } n = 0
$$

$$
dr_{10}(n) = 9, \qquad \text{if } n = 9 k 
$$

$$
dr_{10}(n) = n \mod 9, \qquad \text{if } n \ne 9 k
$$

**Implementation**

The straightforward implementation is 

<iframe src="https://leetcode.com/playground/fV6Sbr9f/shared" frameBorder="0" width="100%" height="174" name="fV6Sbr9f"></iframe>

though two last cases could be merged into one

$$
dr_{10}(n) = 0, \qquad \text{if } n = 0 
$$

$$
dr_{10}(n) = 1 + (n - 1) \mod 9, \qquad \text{if } n \ne 0
$$

<iframe src="https://leetcode.com/playground/2Vzw4gAg/shared" frameBorder="0" width="100%" height="140" name="2Vzw4gAg"></iframe>

**Complexity Analysis**

* Time Complexity: $$\mathcal{O}(1)$$.

* Space Complexity: $$O(1)$$.
<br />
<br />


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accepted C++ O(1)-time O(1)-space 1-Line Solution with Detail Explanations
- Author: zhiqing_xiao
- Creation Date: Sun Aug 16 2015 14:25:07 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 16:30:48 GMT+0800 (Singapore Standard Time)

<p>
The problem, widely known as *digit root* problem, has a congruence formula:

    https://en.wikipedia.org/wiki/Digital_root#Congruence_formula

For base *b* (decimal case *b* = 10), the digit root of an integer is:

- dr(*n*) = 0  if *n* == 0
- dr(*n*) = (*b*-1)  if *n* != 0 and *n* % (*b*-1) == 0
- dr(*n*) = *n* mod (*b*-1) if *n* % (*b*-1) != 0

or

- dr(*n*) = 1 + (*n* - 1) % 9

Note here, when *n* = 0, since (*n* - 1) % 9 = -1, the return value is zero (correct).

From the formula, we can find that the result of this problem is immanently periodic, with period (*b*-1).

Output sequence for decimals (*b* = 10):

~input: 0 1 2 3 4 ...  
output: 0 1 2 3 4 5 6 7 8 9 1 2 3 4 5 6 7 8 9 1 2 3 ....


Henceforth, we can write the following code, whose time and space complexities are both *O*(1).

    class Solution {
    public:
        int addDigits(int num) {
            return 1 + (num - 1) % 9;
        }
    };

Thanks for reading. :)
</p>


### Simple Java Solution No recursion/ loop
- Author: ash1625
- Creation Date: Sun Mar 27 2016 07:01:18 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 12:40:28 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public int addDigits(int num) {
            if (num == 0){
                return 0;
            }
            if (num % 9 == 0){
                return 9;
            }
            else {
                return num % 9;
            }
        }
    }
</p>


### 3 methods for python with explains
- Author: xili_keke
- Creation Date: Wed Nov 04 2015 16:31:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 12:58:13 GMT+0800 (Singapore Standard Time)

<p>
   
 2. Iteration method

  

          class Solution(object):
          def addDigits(self, num):
            """
            :type num: int
            :rtype: int
            """
            while(num >= 10):
                temp = 0
                while(num > 0):
                    temp += num % 10
                    num /= 10
                num = temp
            return num




 1. Digital Root


this method depends on the truth:

N=(a[0] * 1 + a[1] * 10 + ...a[n] * 10 ^n),and a[0]...a[n] are all between [0,9]

we set M = a[0] + a[1] + ..a[n]

and another truth is that:

1 % 9 = 1

10 % 9 = 1

100 % 9 = 1

so N % 9 = a[0] + a[1] + ..a[n]

means N % 9 = M

so N = M (% 9)

as 9 % 9 = 0,so we can make (n - 1) % 9 + 1 to help us solve the problem when n is 9.as N is 9, ( 9 - 1) % 9 + 1 = 9


   

    class Solution(object):
    def addDigits(self, num):
        """
        :type num: int
        :rtype: int
        """
        if num == 0 : return 0
        else:return (num - 1) % 9 + 1
</p>


