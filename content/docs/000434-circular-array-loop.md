---
title: "Circular Array Loop"
weight: 434
#id: "circular-array-loop"
---
## Description
<div class="description">
<p>You are given a <b>circular</b> array <code>nums</code> of positive and negative integers. If a number <i>k</i> at an index is positive, then move forward <i>k</i> steps. Conversely, if it&#39;s negative (-<i>k</i>), move backward <i>k</i>&nbsp;steps. Since the array is circular, you may assume that the last element&#39;s next element is the first element, and the first element&#39;s previous element is the last element.</p>

<p>Determine if there is a loop (or a cycle) in <code>nums</code>. A cycle must start and end at the same index and the cycle&#39;s length &gt; 1. Furthermore, movements in a cycle must all follow a single direction. In other words, a cycle must not consist of both forward and backward movements.</p>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> [2,-1,1,2,2]
<b>Output:</b> true
<b>Explanation:</b> There is a cycle, from index 0 -&gt; 2 -&gt; 3 -&gt; 0. The cycle&#39;s length is 3.
</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> [-1,2]
<b>Output:</b> false
<b>Explanation:</b> The movement from index 1 -&gt; 1 -&gt; 1 ... is not a cycle, because the cycle&#39;s length is 1. By definition the cycle&#39;s length must be greater than 1.
</pre>

<p><b>Example 3:</b></p>

<pre>
<b>Input:</b> [-2,1,-1,-2,-2]
<b>Output:</b> false
<b>Explanation:</b> The movement from index 1 -&gt; 2 -&gt; 1 -&gt; ... is not a cycle, because movement from index 1 -&gt; 2 is a forward movement, but movement from index 2 -&gt; 1 is a backward movement. All movements in a cycle must follow a single direction.</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>-1000 &le;&nbsp;nums[i] &le;&nbsp;1000</li>
	<li>nums[i] &ne;&nbsp;0</li>
	<li>1 &le;&nbsp;nums.length &le; 5000</li>
</ol>

<p>&nbsp;</p>

<p><b>Follow up:</b></p>

<p>Could you solve it in <b>O(n)</b> time complexity and&nbsp;<strong>O(1)</strong> extra space complexity?</p>
</div>

## Tags
- Array (array)
- Two Pointers (two-pointers)

## Companies
- Microsoft - 4 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Google - 8 (taggedByAdmin: false)
- Nutanix - 4 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Slow/Fast Pointer Solution
- Author: JeremyLi28
- Creation Date: Tue Nov 08 2016 13:43:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 04:54:49 GMT+0800 (Singapore Standard Time)

<p>
Just think it as finding a loop in Linkedlist, except that loops with only 1 element do not count. Use a slow and fast pointer, slow pointer moves 1 step a time while fast pointer moves 2 steps a time. If there is a loop (fast == slow), we return true, else if we meet element with different directions, then the search fail, we set all elements along the way to 0. Because 0 is fail for sure so when later search meet 0 we know the search will fail.

```java
public class Solution {
    public boolean circularArrayLoop(int[] nums) {
        int n = nums.length;
        for (int i = 0; i < n; i++) {
            if (nums[i] == 0) {
                continue;
            }
            // slow/fast pointer
            int j = i, k = getIndex(i, nums);
            while (nums[k] * nums[i] > 0 && nums[getIndex(k, nums)] * nums[i] > 0) {
                if (j == k) {
                    // check for loop with only one element
                    if (j == getIndex(j, nums)) {
                        break;
                    }
                    return true;
                }
                j = getIndex(j, nums);
                k = getIndex(getIndex(k, nums), nums);
            }
            // loop not found, set all element along the way to 0
            j = i;
            int val = nums[i];
            while (nums[j] * val > 0) {
                int next = getIndex(j, nums);
                nums[j] = 0;
                j = next;
            }
        }
        return false;
    }
    
    public int getIndex(int i, int[] nums) {
        int n = nums.length;
        return i + nums[i] >= 0? (i + nums[i]) % n: n + ((i + nums[i]) % n);
    }
}
```
</p>


### I cannot understand why test case [-2, 1, -1, -2, -2] gives false?
- Author: czhangaegean
- Creation Date: Thu Nov 24 2016 16:15:08 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 09:58:07 GMT+0800 (Singapore Standard Time)

<p>
For example, starting at index 1, nums[1] is 1, move 1 step forward to index 2. Then nums[2] is -1, move back 1 step to index 1. The loop contains indices 1 and 2. Is this a valid loop?
</p>


### Bad question. Consider rewriting or removing
- Author: mmontag
- Creation Date: Thu Nov 22 2018 09:36:13 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Nov 22 2018 09:36:13 GMT+0800 (Singapore Standard Time)

<p>
Hi, there might be an interesting problem underlying this question, but as worded, it is terribly confusing. The vote ratio is one of the worst (something like 90 up, 600 down). I feel like my time has just been wasted. Can we send this question to the archive or rewrite it?
</p>


