---
title: "Stone Game"
weight: 823
#id: "stone-game"
---
## Description
<div class="description">
<p>Alex and Lee play a game with piles of stones.&nbsp; There are an even number of&nbsp;piles <strong>arranged in a row</strong>, and each pile has a positive integer number of stones <code>piles[i]</code>.</p>

<p>The objective of the game is to end with the most&nbsp;stones.&nbsp; The total number of stones is odd, so there are no ties.</p>

<p>Alex and Lee take turns, with Alex starting first.&nbsp; Each turn, a player&nbsp;takes the entire pile of stones from either the beginning or the end of the row.&nbsp; This continues until there are no more piles left, at which point the person with the most stones wins.</p>

<p>Assuming Alex and Lee play optimally, return <code>True</code>&nbsp;if and only if Alex wins the game.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> piles = [5,3,4,5]
<strong>Output:</strong> true
<strong>Explanation: </strong>
Alex starts first, and can only take the first 5 or the last 5.
Say he takes the first 5, so that the row becomes [3, 4, 5].
If Lee takes 3, then the board is [4, 5], and Alex takes 5 to win with 10 points.
If Lee takes the last 5, then the board is [3, 4], and Alex takes 4 to win with 9 points.
This demonstrated that taking the first 5 was a winning move for Alex, so we return true.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= piles.length &lt;= 500</code></li>
	<li><code>piles.length</code> is even.</li>
	<li><code>1 &lt;= piles[i] &lt;= 500</code></li>
	<li><code>sum(piles)</code> is odd.</li>
</ul>

</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)
- Minimax (minimax)

## Companies
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Dynamic Programming

**Intuition**

Let's change the game so that whenever Lee scores points, it deducts from Alex's score instead.

Let `dp(i, j)` be the largest score Alex can achieve where the piles remaining are `piles[i], piles[i+1], ..., piles[j]`.  This is natural in games with scoring: we want to know what the value of each position of the game is.

We can formulate a recursion for `dp(i, j)` in terms of `dp(i+1, j)` and `dp(i, j-1)`, and we can use dynamic programming to not repeat work in this recursion.  (This approach can output the correct answer, because the states form a DAG (directed acyclic graph).)

**Algorithm**

When the piles remaining are `piles[i], piles[i+1], ..., piles[j]`, the player who's turn it is has at most 2 moves.

The person who's turn it is can be found by comparing `j-i` to `N` modulo 2.

If the player is Alex, then she either takes `piles[i]` or `piles[j]`, increasing her score by that amount.  Afterwards, the total score is either `piles[i] + dp(i+1, j)`, or `piles[j] + dp(i, j-1)`; and we want the maximum possible score.

If the player is Lee, then he either takes `piles[i]` or `piles[j]`, decreasing Alex's score by that amount.  Afterwards, the total score is either `-piles[i] + dp(i+1, j)`, or `-piles[j] + dp(i, j-1)`; and we want the *minimum* possible score.


<iframe src="https://leetcode.com/playground/NrhEfmjs/shared" frameBorder="0" width="100%" height="412" name="NrhEfmjs"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the number of piles.

* Space Complexity:  $$O(N^2)$$, the space used storing the intermediate results of each subgame.
<br />
<br />


---
#### Approach 2: Mathematical

**Intuition and Algorithm**

Alex clearly always wins the 2 pile game.  With some effort, we can see that she always wins the 4 pile game.

If Alex takes the first pile initially, she can always take the third pile.  If she takes the fourth pile initially, she can always take the second pile.  At least one of `first + third, second + fourth` is larger, so she can always win.

We can extend this idea to `N` piles.  Say the first, third, fifth, seventh, etc. piles are white, and the second, fourth, sixth, eighth, etc. piles are black.  Alex can always take either all white piles or all black piles, and one of the colors must have a sum number of stones larger than the other color.

Hence, Alex always wins the game.

<iframe src="https://leetcode.com/playground/kQCFqgHM/shared" frameBorder="0" width="100%" height="157" name="kQCFqgHM"></iframe>

**Complexity Analysis**

* Time and Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### DP or Just return true
- Author: lee215
- Creation Date: Sun Jul 29 2018 11:02:56 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 23 2019 01:23:19 GMT+0800 (Singapore Standard Time)

<p>
## Approach 1: Just return true
Alex is first to pick pile.
`piles.length` is even, and this lead to an interesting fact:
Alex can always pick odd piles or always pick even piles!

For example,
If Alex wants to pick even indexed piles `piles[0], piles[2], ....., piles[n-2]`,
he picks first `piles[0]`, then Lee can pick either `piles[1]` or `piles[n - 1]`.
Every turn, Alex can always pick even indexed piles and Lee can only pick odd indexed piles.

In the description, we know that `sum(piles)` is odd.
If `sum(piles[even]) > sum(piles[odd])`, Alex just picks all evens and wins.
If `sum(piles[even]) < sum(piles[odd])`, Alex just picks all odds and wins.

So, Alex always defeats Lee in this game.

**C++/Java**
```
    return true;
```

**Python:**
```
    return True
```
<br/>

## Approach 2: 2D DP

It\'s tricky when we have even number of piles of stones. You may not have this condition in an interview.

**Follow-up:**

What if piles.length can be odd?
What if we want to know exactly the diffenerce of score?
Then we need to solve it with DP.

`dp[i][j]` means the biggest number of stones you can get more than opponent picking piles in `piles[i] ~ piles[j]`.
You can first pick `piles[i]` or  `piles[j]`.
1. If you pick `piles[i]`, your result will be `piles[i] - dp[i + 1][j]`
1. If you pick `piles[j]`, your result will be `piles[j] - dp[i][j - 1]`

So we get:
`dp[i][j] = max(piles[i] - dp[i + 1][j], piles[j] - dp[i][j - 1])`
We start from smaller subarray and then we use that to calculate bigger subarray.

Note that take evens or take odds, it\'s just an easy strategy to win when the number of stones is even. 
**It\'s not the best solution!**
I didn\'t find a tricky solution when the number of stones is odd (maybe there is).

**C++:**
```
    bool stoneGame(vector<int>& p) {
        int n = p.size();
        vector<vector<int>> dp(n, vector<int>(n, 0));
        for (int i = 0; i < n; i++) dp[i][i] = p[i];
        for (int d = 1; d < n; d++)
            for (int i = 0; i < n - d; i++)
                dp[i][i + d] = max(p[i] - dp[i + 1][i + d], p[i + d] - dp[i][i + d - 1]);
        return dp[0][n - 1] > 0;
    }
```

**Java:**
```
    public boolean stoneGame(int[] p) {
        int n = p.length;
        int[][] dp  = new int[n][n];
        for (int i = 0; i < n; i++) dp[i][i] = p[i];
        for (int d = 1; d < n; d++)
            for (int i = 0; i < n - d; i++)
                dp[i][i + d] = Math.max(p[i] - dp[i + 1][i + d], p[i + d] - dp[i][i + d - 1]);
        return dp[0][n - 1] > 0;
    }
```
**Python:**
```
    def stoneGame(self, p):
        n = len(p)
        dp = [[0] * n for i in range(n)]
        for i in range(n): dp[i][i] = p[i]
        for d in range(1, n):
            for i in range(n - d):
                dp[i][i + d] = max(p[i] - dp[i + 1][i + d], p[i + d] - dp[i][i + d - 1])
        return dp[0][-1] > 0
```
<br/>

## Approach 3: 1D DP
Follow up: Use only O(N) space?

Simplify to 1D DP.

**C++:**
```
    bool stoneGame(vector<int>& p) {
        vector<int> dp = p;
        for (int d = 1; d < p.size(); d++)
            for (int i = 0; i < p.size() - d; i++)
                dp[i] = max(p[i] - dp[i + 1], p[i + d] - dp[i]);
        return dp[0] > 0;
    }
```

**Java:**
```
    public boolean stoneGame(int[] p) {
        int[] dp = Arrays.copyOf(p, p.length);;
        for (int d = 1; d < p.length; d++)
            for (int i = 0; i < p.length - d; i++)
                dp[i] = Math.max(p[i] - dp[i + 1], p[i + d] - dp[i]);
        return dp[0] > 0;
    }
```
**Python:**
```
    def stoneGame(self, p):
        n = len(p)
        dp = p[:]
        for d in range(1, n):
            for i in range(n - d):
                dp[i] = max(p[i] - dp[i + 1], p[i + d] - dp[i])
        return dp[0] > 0
```






</p>


### [Java] This is minimax + dp (fully detailed explanation + generalization + easy understand code)
- Author: aakarshmadhavan
- Creation Date: Sun Jul 29 2018 12:24:29 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 04:45:19 GMT+0800 (Singapore Standard Time)

<p>
The problem was made a "trick problem," because Alex always wins, but if you want to learn the **general idea behind these problems** here is the explanation for other cases.

```
class Solution {
    int [][][] memo;
    public boolean stoneGame(int[] piles) {
        memo = new int[piles.length + 1][piles.length + 1][2];
        for(int [][] arr : memo)
            for(int [] subArr : arr)
                Arrays.fill(subArr, -1);
        
        return (helper(0, piles.length - 1, piles, 1) >= 0);
    }
    
    public int helper(int l, int r, int [] piles, int ID){
        if(r < l)
            return 0;
        if(memo[l][r][ID] != -1)
            return memo[l][r][ID];
        
        int next = Math.abs(ID - 1);
        if(ID == 1)
            memo[l][r][ID] = Math.max(piles[l] + helper(l + 1, r, piles, next), piles[r] + helper(l, r - 1, piles, next));
        else
            memo[l][r][ID] = Math.min(-piles[l] + helper(l + 1, r, piles, next), -piles[r] + helper(l, r - 1, piles, next));
        
        return memo[l][r][ID];
    }
}
```

This is a **Minimax** problem. Each player plays optimally to win, but you can\'t greedily choose the optimal strategy so you have to try *all* strategies, this is DP now.

What does it mean for Alex to win? Alex will win if `score(Alex) >= score(Lee)`, but this also means `score(Alex) - score(Lee) >= 0`, so here you have a **common parameter** which is a `score` variable. The `score` parameter really means `score = score(Alex) - score(Lee)`.

Now, if each player is suppoed to play optimally, how do you decide this with one variable?

Well since Alex is playing optimally, he wants to **maximize** the `score` variable because remember, Alex only wins if `score = score(Alex) - score(Lee) >= 0` Alex should *add* to the score because he wants to maximize it.
Since Lee is also playing optimally, he wants to **minimize** the `score` variable, since if the `score` variable becomes negative, Lee has more individual score than Alex. But since we have only one variable, Lee should *damage* the score (or in other words, *subtract* from the score).

Now, let\'s think of the brute force solution. You are at the current state, say this is Alex\'s turn. Alex can choose either `left` or `right`, but he can\'t greedily pick so you try *both* of them, this is `O(2^n)` for runtime.

But realize the `state` you are in. You can easily memoize the this, the varying parameters are `l, r, ID`, where `ID` is the player ID (`1 for Alex, 0 for Lee`), hence you can make a DP/Cache table and return answer if you have discovered the state.

Finally, in your main function you call this `helper` function and check if you were able to get a `score >= 0`.
</p>


### Alex won't lose
- Author: stkoso
- Creation Date: Sun Jul 29 2018 11:02:17 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 00:18:54 GMT+0800 (Singapore Standard Time)

<p>
Alex can always take all odd piles or always take all even piles
Since sum of all piles is odd then sum of all odd piles won\'t equals sum of all even piles, Alex could just take the bigger ones. 
```
    bool stoneGame(vector<int>& piles) {
        return true;
    }
```
</p>


