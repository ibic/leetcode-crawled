---
title: "The Most Frequently Ordered Products for Each Customer"
weight: 1598
#id: "the-most-frequently-ordered-products-for-each-customer"
---
## Description
<div class="description">
<p>Table: <code>Customers</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| customer_id   | int     |
| name          | varchar |
+---------------+---------+
customer_id is the primary key for this table.
This table contains information about the customers.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Orders</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| order_id      | int     |
| order_date    | date    |
| customer_id   | int     |
| product_id    | int     |
+---------------+---------+
order_id is the primary key for this table.
This table contains information about the orders made by customer_id.
No customer will order the same product <strong>more than once</strong> in a single day.</pre>

<p>&nbsp;</p>

<p>Table: <code>Products</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| product_id    | int     |
| product_name  | varchar |
| price         | int     |
+---------------+---------+
product_id is the primary key for this table.
This table contains information about the products.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the most frequently ordered product(s) for each customer.</p>

<p>The result table should have the <code>product_id</code> and <code>product_name</code> for each <code>customer_id</code> who ordered at least one order. Return the result table in <strong>any order</strong>.</p>

<p>The query result format is in the following example:</p>

<pre>
<code>Customers</code>
+-------------+-------+
| customer_id | name  |
+-------------+-------+
| 1           | Alice |
| 2           | Bob   |
| 3           | Tom   |
| 4           | Jerry |
| 5           | John  |
+-------------+-------+

<code>Orders</code>
+----------+------------+-------------+------------+
| order_id | order_date | customer_id | product_id |
+----------+------------+-------------+------------+
| 1        | 2020-07-31 | 1           | 1          |
| 2        | 2020-07-30 | 2           | 2          |
| 3        | 2020-08-29 | 3           | 3          |
| 4        | 2020-07-29 | 4           | 1          |
| 5        | 2020-06-10 | 1           | 2          |
| 6        | 2020-08-01 | 2           | 1          |
| 7        | 2020-08-01 | 3           | 3          |
| 8        | 2020-08-03 | 1           | 2          |
| 9        | 2020-08-07 | 2           | 3          |
| 10       | 2020-07-15 | 1           | 2          |
+----------+------------+-------------+------------+

<code>Products</code>
+------------+--------------+-------+
| product_id | product_name | price |
+------------+--------------+-------+
| 1          | keyboard     | 120   |
| 2          | mouse        | 80    |
| 3          | screen       | 600   |
| 4          | hard disk    | 450   |
+------------+--------------+-------+
Result table:
+-------------+------------+--------------+
| customer_id | product_id | product_name |
+-------------+------------+--------------+
| 1           | 2          | mouse        |
| 2           | 1          | keyboard     |
| 2           | 2          | mouse        |
| 2           | 3          | screen       |
| 3           | 3          | screen       |
| 4           | 1          | keyboard     |
+-------------+------------+--------------+

Alice (customer 1) ordered the mouse three times and the keyboard one time, so the mouse is the most frquently ordered product for them.
Bob (customer 2) ordered the keyboard, the mouse, and the screen one time, so those are the most frquently ordered products for them.
Tom (customer 3) only ordered the screen (two times), so that is the most frquently ordered product for them.
Jerry (customer 4) only ordered the keyboard (one time), so that is the most frquently ordered product for them.
John (customer 5) did not order anything, so we do not include them in the result table.
</pre>
</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### simple and easy solution using window function
- Author: vwang0
- Creation Date: Thu Sep 24 2020 05:04:28 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 24 2020 05:09:11 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT customer_id, product_id, product_name
FROM (
    SELECT O.customer_id, O.product_id, P.product_name, 
    RANK() OVER (PARTITION BY customer_id ORDER BY COUNT(O.product_id) DESC) AS rnk
    FROM Orders O
    JOIN Products P
    ON O.product_id = P.product_id  
    GROUP BY customer_id, product_id
) temp
WHERE rnk = 1 
ORDER BY customer_id, product_id
```
</p>


### MySQL-2 Solutions
- Author: swethasekhar
- Creation Date: Mon Sep 28 2020 03:30:01 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 28 2020 03:30:01 GMT+0800 (Singapore Standard Time)

<p>
1. Without Window Function 
```
SELECT customer_id,products.product_id,product_name
from Orders
JOIN Products on Products.product_id=Orders.product_id
group by customer_id,product_id
HAVING (customer_id,COUNT(order_date)) IN(
#Get Maxiumum Count for each customer 
SELECT customer_id,MAX(cnt)FROM
(
SELECT customer_id,product_id,COUNT(order_date) as cnt
from Orders
group by customer_id,product_id
) as a
group by customer_id)

```
2. With RANK()
```

SELECT customer_id,product_id,product_name
FROM (
SELECT customer_id,products.product_id,product_name
    ,RANK() OVER(PARTITION BY customer_id ORDER BY COUNT(*) desc) as rank_count 
	# For each customer we need the maximum count of orders 
from Orders
JOIN Products on Products.product_id=Orders.product_id
group by customer_id,product_id
) as a
where a.rank_count=1

</p>


### mysql step-by-step explanation
- Author: IrisSYS
- Creation Date: Thu Sep 24 2020 08:51:54 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 24 2020 08:51:54 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT b.customer_id
    ,  b.product_id
    ,  c.product_name
FROM
# STEP2: figure out the rank of each product_id based on order_cnt. In case there are ties, use DENSE_RANK
    (
    SELECT a.customer_id
        ,  a.product_id
        ,  DENSE_RANK() OVER (PARTITION BY a.customer_id ORDER BY order_cnt DESC) AS prod_rank #In case there is a tie
    FROM
	# STEP1: find out the order count for each product for a given customer 
        (
        SELECT a.customer_id
            ,  b.product_id
            ,  COUNT(*) AS order_cnt
        FROM Customers AS a
        LEFT JOIN Orders AS b
        ON a.customer_id = b.customer_id
        GROUP BY 1,2
        ) AS a
# STEP3: Remove any customer who never ordered
    WHERE order_cnt >= 1
    ) AS b
JOIN Products AS c
ON b.product_id = c.product_id
# STEP4: pick up the most ordered product 
WHERE b.prod_rank = 1
```
</p>


