---
title: "Valid Palindrome III"
weight: 1073
#id: "valid-palindrome-iii"
---
## Description
<div class="description">
<p>Given a string <code>s</code>&nbsp;and an integer&nbsp;<code>k</code>, find out if the given string is&nbsp;a&nbsp;<em>K-Palindrome</em> or not.</p>

<p>A string is K-Palindrome if it can be&nbsp;transformed&nbsp;into a palindrome by removing at most <code>k</code> characters from it.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;abcdeca&quot;, k = 2
<strong>Output:</strong> true
<strong>Explanation: </strong>Remove &#39;b&#39; and &#39;e&#39; characters.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 1000</code></li>
	<li><code>s</code>&nbsp;has only lowercase English letters.</li>
	<li><code>1 &lt;= k&nbsp;&lt;= s.length</code></li>
</ul>

</div>

## Tags
- String (string)
- Dynamic Programming (dynamic-programming)

## Companies
- Facebook - 4 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Find Longest Palindromic Subsequence.
- Author: Poorvank
- Creation Date: Sun Oct 06 2019 00:02:31 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 06 2019 00:54:25 GMT+0800 (Singapore Standard Time)

<p>
The idea is to find the [longest palindromic subsequence](https://leetcode.com/problems/longest-palindromic-subsequence/description/)(lps) of the given string.
|lps - original string| <= k,
then the string is k-palindrome.

Eg:

One of the lps of string pqrstrp is prsrp.
*Characters not contributing to lps of the 
string should be removed in order to make the string palindrome*. So on removing q and s (or t) from pqrstrp,
string will transform into a palindrome.


```
public boolean isValidPalindrome(String str, int k) {
        int n = str.length();

        StringBuilder stringBuilder = new StringBuilder(str).reverse();
        int lps = lcs(str, stringBuilder.toString(), n, n);

        return (n - lps <= k);
    }

    /*
    longest palindromic subsequence:
    LCS of the given string & its reverse will be the longest palindromic sequence.
     */
    private int lcs(String X, String Y, int m, int n) {
        int[][] dp = new int[m + 1][n + 1];
        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 0;
                } else if (X.charAt(i - 1) == Y.charAt(j - 1)) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }
        return dp[m][n];
    }
```
</p>


### Java DP
- Author: Sun_WuKong
- Creation Date: Sun Oct 06 2019 00:08:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 06 2019 00:08:48 GMT+0800 (Singapore Standard Time)

<p>
Use a 2D array for DP, the value at [i][j] shows the minumum number of removal needed to make the substring from i to j (inclusive) to become a palinedrome.

At each [i][j] there are 2 scenarios:
- `s.charAt(i) == s.charAt(j)` then `cache[i][j] = cache[i+1][j-1]`
- `s.charAt(i) != s.charAt(j)` then `cache[i][j] = 1 + Math.min(cache[i+1][j], cache[i][j-1)`

At the end, compare `cache[0][s.length()-1]` to `k`

```
class Solution {
    public boolean isValidPalindrome(String s, int k) {
        Integer[][] cache = new Integer[s.length()][s.length()];
        return aux(s, 0, s.length()-1, cache) <= k;
    }
    
    private int aux(String s, int left, int right, Integer[][] cache) {
        if (right - left < 1) return 0;
        if (cache[left][right] != null) return cache[left][right];
        
        int step = 0;
        if (s.charAt(left) == s.charAt(right)) {
            step = aux(s, left+1, right-1, cache);
        } else {
            step = 1 + Math.min(aux(s, left+1, right, cache), aux(s, left, right-1, cache));
        }
        cache[left][right] = step;
        return step;
    }
}
```
</p>


### [Java] DP code, similar to LC 516 w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Oct 06 2019 00:05:47 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 06 2019 00:40:00 GMT+0800 (Singapore Standard Time)

<p>
Find the length of the longest palindromic subsequence, then plus k to check if the sum reaches the length of the original input string.
Refer to [516. Longest Palindromic Subsequence](https://leetcode.com/problems/longest-palindromic-subsequence/description/), and [new2500](https://leetcode.com/problems/longest-palindromic-subsequence/discuss/99101/new2500)\'s graph is very helpful.

`dp[i][j]`: the longest palindromic subsequence\'s length of substring(i, j), where i, j are left, right indices of the string.
State transition:
 if s.charAt(i) == s.charAt(j):
`dp[i][j] = dp[i+1][j-1] + 2`
otherwise, 
`dp[i][j] = Math.max(dp[i+1][j], dp[i][j-1])`.
`dp[i][i]` Initialized to `1`.

```
    public boolean isValidPalindrome(String s, int k) {
        int[][] dp = new int[s.length()][s.length()];
        for (int i = s.length() - 1; i >= 0; i--) {
            dp[i][i] = 1;
            for (int j = i + 1; j < s.length(); ++j) {
                if (s.charAt(i) == s.charAt(j)) { dp[i][j] = dp[i + 1][j - 1] + 2; } 
                else { dp[i][j] = Math.max(dp[i + 1][j], dp[i][j - 1]); }
            }
        }
        return s.length() <= k + dp[0][s.length() - 1];
    }
```
**Analysis:**
Time & space: O(n ^ 2), where n = s.length().
</p>


