---
title: "Network Delay Time"
weight: 665
#id: "network-delay-time"
---
## Description
<div class="description">
<p>There are <code>N</code> network nodes, labelled <code>1</code> to <code>N</code>.</p>

<p>Given <code>times</code>, a list of travel times as <b>directed</b> edges <code>times[i] = (u, v, w)</code>, where <code>u</code> is the source node, <code>v</code> is the target node, and <code>w</code> is the time it takes for a signal to travel from source to target.</p>

<p>Now, we send a signal from a certain node <code>K</code>. How long will it take for all nodes to receive the signal? If it is impossible, return <code>-1</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/05/23/931_example_1.png" style="width: 200px; height: 220px;" /></p>

<pre>
<strong>Input: </strong>times = <span id="example-input-1-1">[[2,1,1],[2,3,1],[3,4,1]]</span>, N = <span id="example-input-1-2">4</span>, K = <span id="example-input-1-3">2</span>
<strong>Output: </strong><span id="example-output-1">2</span>
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li><code>N</code> will be in the range <code>[1, 100]</code>.</li>
	<li><code>K</code> will be in the range <code>[1, N]</code>.</li>
	<li>The length of <code>times</code> will be in the range <code>[1, 6000]</code>.</li>
	<li>All edges <code>times[i] = (u, v, w)</code> will have <code>1 &lt;= u, v &lt;= N</code> and <code>0 &lt;= w &lt;= 100</code>.</li>
</ol>

</div>

## Tags
- Heap (heap)
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)
- Graph (graph)

## Companies
- Google - 4 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Akuna Capital - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Depth-First Search [Accepted]

**Intuition**

Let's record the time `dist[node]` when the signal reaches the node.  If some signal arrived earlier, we don't need to broadcast it anymore.  Otherwise, we should broadcast the signal.

**Algorithm**

We'll maintain `dist[node]`, the earliest that we arrived at each `node`.  When visiting a `node` while `elapsed` time has elapsed, if this is the currently-fastest signal at this node, let's broadcast signals from this node.

To speed things up, at each visited node we'll consider signals exiting the node that are faster first, by sorting the edges.


<iframe src="https://leetcode.com/playground/G4rhDsxS/shared" frameBorder="0" width="100%" height="500" name="G4rhDsxS"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^N + E \log E)$$ where $$E$$ is the length of `times`.  We can only fully visit each node up to $$N-1$$ times, one per each other node.  Plus, we have to explore every edge and sort them.  Sorting each small bucket of outgoing edges is bounded by sorting all of them, because of repeated use of the inequality $$x \log x + y \log y \leq (x+y) \log (x+y)$$.

* Space Complexity: $$O(N + E)$$, the size of the graph ($$O(E)$$), plus the size of the implicit call stack in our DFS ($$O(N)$$).

---
#### Approach #2: Dijkstra's Algorithm [Accepted]

**Intuition and Algorithm**

We use *Dijkstra's algorithm* to find the shortest path from our source to all targets.  This is a textbook algorithm, refer to [this link](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm) for more details.

Dijkstra's algorithm is based on repeatedly making the candidate move that has the least distance travelled.

In our implementations below, we showcase both $$O(N^2)$$ (basic) and $$O(N \log N)$$ (heap) approaches.

*Basic Implementation*
<iframe src="https://leetcode.com/playground/yJvLKDzT/shared" frameBorder="0" width="100%" height="500" name="yJvLKDzT"></iframe>

*Heap Implementation*

<iframe src="https://leetcode.com/playground/b3GshKFG/shared" frameBorder="0" width="100%" height="500" name="b3GshKFG"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2 + E)$$m where $$E$$ is the length of `times` in the basic implementation, and $$O(E \log E)$$ in the heap implementation, as potentially every edge gets added to the heap.

* Space Complexity: $$O(N + E)$$, the size of the graph ($$O(E)$$), plus the size of the other objects used ($$O(N)$$).

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] Bellman Ford
- Author: alexander
- Creation Date: Sun Dec 10 2017 12:01:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 11:47:07 GMT+0800 (Singapore Standard Time)

<p>
**C++**
```
class Solution {
public:
    int networkDelayTime(vector<vector<int>>& times, int N, int K) {
        vector<int> dist(N + 1, INT_MAX);
        dist[K] = 0;
        for (int i = 0; i < N; i++) {
            for (vector<int> e : times) {
                int u = e[0], v = e[1], w = e[2];
                if (dist[u] != INT_MAX && dist[v] > dist[u] + w) {
                    dist[v] = dist[u] + w;
                }
            }
        }

        int maxwait = 0;
        for (int i = 1; i <= N; i++)
            maxwait = max(maxwait, dist[i]);
        return maxwait == INT_MAX ? -1 : maxwait;
    }
};
```
</p>


### Java, Djikstra/bfs, Concise and very easy to understand
- Author: alizhiyu46
- Creation Date: Sat Dec 29 2018 05:39:46 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 17 2019 06:41:05 GMT+0800 (Singapore Standard Time)

<p>
I think bfs and djikstra are very similar problems. It\'s just that djikstra cost is different compared with bfs, so use priorityQueue instead a Queue for a standard bfs search.
```
class Solution {
    public int networkDelayTime(int[][] times, int N, int K) {
        Map<Integer, Map<Integer,Integer>> map = new HashMap<>();
        for(int[] time : times){
            map.putIfAbsent(time[0], new HashMap<>());
            map.get(time[0]).put(time[1], time[2]);
        }
        
        //distance, node into pq
        Queue<int[]> pq = new PriorityQueue<>((a,b) -> (a[0] - b[0]));
        
        pq.add(new int[]{0, K});
        
        boolean[] visited = new boolean[N+1];
        int res = 0;
        
        while(!pq.isEmpty()){
            int[] cur = pq.remove();
            int curNode = cur[1];
            int curDist = cur[0];
            if(visited[curNode]) continue;
            visited[curNode] = true;
            res = curDist;
            N--;
            if(map.containsKey(curNode)){
                for(int next : map.get(curNode).keySet()){
                    pq.add(new int[]{curDist + map.get(curNode).get(next), next});
                }
            }
        }
        return N == 0 ? res : -1;
            
    }
}
```
</p>


### Python concise queue & heap solutions
- Author: cenkay
- Creation Date: Tue Oct 30 2018 22:07:26 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 30 2018 22:07:26 GMT+0800 (Singapore Standard Time)

<p>
* Heap
```
class Solution:
    def networkDelayTime(self, times, N, K):
        q, t, adj = [(0, K)], {}, collections.defaultdict(list)
        for u, v, w in times:
            adj[u].append((v, w))
        while q:
            time, node = heapq.heappop(q)
            if node not in t:
                t[node] = time
                for v, w in adj[node]:
                    heapq.heappush(q, (time + w, v))
        return max(t.values()) if len(t) == N else -1
```
* Queue
```
class Solution:
    def networkDelayTime(self, times, N, K):
        t, graph, q = [0] + [float("inf")] * N, collections.defaultdict(list), collections.deque([(0, K)])
        for u, v, w in times:
            graph[u].append((v, w))
        while q:
            time, node = q.popleft()
            if time < t[node]:
                t[node] = time
                for v, w in graph[node]:
                    q.append((time + w, v))
        mx = max(t)
        return mx if mx < float("inf") else -1
```
</p>


