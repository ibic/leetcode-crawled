---
title: "Regular Expression Matching"
weight: 10
#id: "regular-expression-matching"
---
## Description
<div class="description">
<p>Given an input string (<code>s</code>) and a pattern (<code>p</code>), implement regular expression matching with support for <code>&#39;.&#39;</code> and <code>&#39;*&#39;</code> where:<code>&nbsp;</code></p>

<ul>
	<li><code>&#39;.&#39;</code> Matches any single character.​​​​</li>
	<li><code>&#39;*&#39;</code> Matches zero or more of the preceding element.</li>
</ul>

<p>The matching should cover the <strong>entire</strong> input string (not partial).</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aa&quot;, p = &quot;a&quot;
<strong>Output:</strong> false
<strong>Explanation:</strong> &quot;a&quot; does not match the entire string &quot;aa&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aa&quot;, p = &quot;a*&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong>&nbsp;&#39;*&#39; means zero or more of the preceding&nbsp;element, &#39;a&#39;. Therefore, by repeating &#39;a&#39; once, it becomes &quot;aa&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;ab&quot;, p = &quot;.*&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong>&nbsp;&quot;.*&quot; means &quot;zero or more (*) of any character (.)&quot;.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;aab&quot;, p = &quot;c*a*b&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong>&nbsp;c can be repeated 0 times, a can be repeated 1 time. Therefore, it matches &quot;aab&quot;.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;mississippi&quot;, p = &quot;mis*is*p*.&quot;
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= s.length&nbsp;&lt;= 20</code></li>
	<li><code>0 &lt;= p.length&nbsp;&lt;= 30</code></li>
	<li><code>s</code>&nbsp;contains only lowercase English letters.</li>
	<li><code>p</code>&nbsp;contains only lowercase English letters, <code>&#39;.&#39;</code>, and&nbsp;<code>&#39;*&#39;</code>.</li>
</ul>

</div>

## Tags
- String (string)
- Dynamic Programming (dynamic-programming)
- Backtracking (backtracking)

## Companies
- Facebook - 11 (taggedByAdmin: true)
- Coursera - 7 (taggedByAdmin: false)
- Amazon - 7 (taggedByAdmin: false)
- Goldman Sachs - 6 (taggedByAdmin: false)
- Bloomberg - 5 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: true)
- Apple - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Databricks - 2 (taggedByAdmin: false)
- Lyft - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)
- Twitter - 2 (taggedByAdmin: true)
- Pocket Gems - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Houzz - 2 (taggedByAdmin: false)
- Flipkart - 2 (taggedByAdmin: false)
- Zulily - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Recursion

**Intuition**

If there were no Kleene stars (the `*` wildcard character for regular expressions), the problem would be easier - we simply check from left to right if each character of the text matches the pattern.

When a star is present, we may need to check many different suffixes of the text and see if they match the rest of the pattern.  A recursive solution is a straightforward way to represent this relationship.

**Algorithm**

Without a Kleene star, our solution would look like this:


<iframe src="https://leetcode.com/playground/Z2XSmAHG/shared" frameBorder="0" width="100%" height="123" name="Z2XSmAHG"></iframe>

If a star is present in the pattern, it will be in the second position $$\text{pattern[1]}$$.  Then, we may ignore this part of the pattern, or delete a matching character in the text.  If we have a match on the remaining strings after any of these operations, then the initial inputs matched.

<iframe src="https://leetcode.com/playground/EX8cYcs3/shared" frameBorder="0" width="100%" height="293" name="EX8cYcs3"></iframe>

**Complexity Analysis**

* Time Complexity: Let $$T, P$$ be the lengths of the text and the pattern respectively.  In the worst case, a call to `match(text[i:], pattern[2j:])` will be made $$\binom{i+j}{i}$$ times, and strings of the order $$O(T - i)$$ and $$O(P - 2*j)$$ will be made.  Thus, the complexity has the order $$\sum_{i = 0}^T \sum_{j = 0}^{P/2} \binom{i+j}{i} O(T+P-i-2j)$$.  With some effort outside the scope of this article, we can show this is bounded by $$O\big((T+P)2^{T + \frac{P}{2}}\big)$$.

* Space Complexity:  For every call to `match`, we will create those strings as described above, possibly creating duplicates.  If memory is not freed, this will also take a total of $$O\big((T+P)2^{T + \frac{P}{2}}\big)$$ space, even though there are only order $$O(T^2 + P^2)$$ unique suffixes of $$P$$ and  $$T$$ that are actually required.
<br />
<br />
---
#### Approach 2: Dynamic Programming

**Intuition**

As the problem has an **optimal substructure**, it is natural to cache intermediate results.  We ask the question $$\text{dp(i, j)}$$: does $$\text{text[i:]}$$ and $$\text{pattern[j:]}$$ match?  We can describe our answer in terms of answers to questions involving smaller strings.

**Algorithm**

We proceed with the same recursion as in [Approach 1](#approach-1-recursion), except because calls will only ever be made to `match(text[i:], pattern[j:])`, we use $$\text{dp(i, j)}$$ to handle those calls instead, saving us expensive string-building operations and allowing us to cache the intermediate results.


*Top-Down Variation*
<iframe src="https://leetcode.com/playground/Fpg6LXEX/shared" frameBorder="0" width="100%" height="500" name="Fpg6LXEX"></iframe>

*Bottom-Up Variation*

<iframe src="https://leetcode.com/playground/dmAyPDG3/shared" frameBorder="0" width="100%" height="395" name="dmAyPDG3"></iframe>

**Complexity Analysis**

* Time Complexity: Let $$T, P$$ be the lengths of the text and the pattern respectively.  The work for every call to `dp(i, j)` for $$i=0, ... ,T$$; $$j=0, ... ,P$$ is done once, and it is $$O(1)$$ work.  Hence, the time complexity is $$O(TP)$$.

* Space Complexity:  The only memory we use is the $$O(TP)$$ boolean entries in our cache.  Hence, the space complexity is $$O(TP)$$.

## Accepted Submission (java)
```java
class Solution {
public boolean isMatch(String s, String p) {

    if (s == null || p == null) {
        return false;
    }
    boolean[][] dp = new boolean[s.length()+1][p.length()+1];
    dp[0][0] = true;
    for (int i = 0; i < p.length(); i++) {
        if (p.charAt(i) == '*' && dp[0][i-1]) {
            dp[0][i+1] = true;
        }
    }
    for (int i = 0 ; i < s.length(); i++) {
        for (int j = 0; j < p.length(); j++) {
            if (p.charAt(j) == '.') {
                dp[i+1][j+1] = dp[i][j];
            }
            if (p.charAt(j) == s.charAt(i)) {
                dp[i+1][j+1] = dp[i][j];
            }
            if (p.charAt(j) == '*') {
                if (p.charAt(j-1) != s.charAt(i) && p.charAt(j-1) != '.') {
                    dp[i+1][j+1] = dp[i+1][j-1];
                } else {
                    dp[i+1][j+1] = (dp[i+1][j] || dp[i][j+1] || dp[i+1][j-1]);
                }
            }
        }
    }
    return dp[s.length()][p.length()];
}
}
```

## Top Discussions
### Easy DP Java Solution with detailed Explanation
- Author: monkeyGoCrazy
- Creation Date: Sun Mar 20 2016 02:53:30 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 23:37:48 GMT+0800 (Singapore Standard Time)

<p>
This Solution use 2D DP. beat 90% solutions, very simple.

Here are some conditions to figure out, then the logic can be very straightforward.

    1, If p.charAt(j) == s.charAt(i) :  dp[i][j] = dp[i-1][j-1];
    2, If p.charAt(j) == '.' : dp[i][j] = dp[i-1][j-1];
    3, If p.charAt(j) == '*': 
       here are two sub conditions:
                   1   if p.charAt(j-1) != s.charAt(i) : dp[i][j] = dp[i][j-2]  //in this case, a* only counts as empty
                   2   if p.charAt(i-1) == s.charAt(i) or p.charAt(i-1) == '.':
                                  dp[i][j] = dp[i-1][j]    //in this case, a* counts as multiple a 
                               or dp[i][j] = dp[i][j-1]   // in this case, a* counts as single a
                               or dp[i][j] = dp[i][j-2]   // in this case, a* counts as empty

Here is the solution

    public boolean isMatch(String s, String p) {

        if (s == null || p == null) {
            return false;
        }
        boolean[][] dp = new boolean[s.length()+1][p.length()+1];
        dp[0][0] = true;
        for (int i = 0; i < p.length(); i++) {
            if (p.charAt(i) == '*' && dp[0][i-1]) {
                dp[0][i+1] = true;
            }
        }
        for (int i = 0 ; i < s.length(); i++) {
            for (int j = 0; j < p.length(); j++) {
                if (p.charAt(j) == '.') {
                    dp[i+1][j+1] = dp[i][j];
                }
                if (p.charAt(j) == s.charAt(i)) {
                    dp[i+1][j+1] = dp[i][j];
                }
                if (p.charAt(j) == '*') {
                    if (p.charAt(j-1) != s.charAt(i) && p.charAt(j-1) != '.') {
                        dp[i+1][j+1] = dp[i+1][j-1];
                    } else {
                        dp[i+1][j+1] = (dp[i+1][j] || dp[i][j+1] || dp[i+1][j-1]);
                    }
                }
            }
        }
        return dp[s.length()][p.length()];
    }
</p>


### My concise recursive and DP solutions with full explanation in C++
- Author: xiaohui7
- Creation Date: Fri Dec 19 2014 23:17:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 08:44:41 GMT+0800 (Singapore Standard Time)

<p>
Please refer to [my blog post][1] if you have any comment. Wildcard matching problem can be solved similarly.

    class Solution {
    public:
        bool isMatch(string s, string p) {
            if (p.empty())    return s.empty();
            
            if ('*' == p[1])
                // x* matches empty string or at least one character: x* -> xx*
                // *s is to ensure s is non-empty
                return (isMatch(s, p.substr(2)) || !s.empty() && (s[0] == p[0] || '.' == p[0]) && isMatch(s.substr(1), p));
            else
                return !s.empty() && (s[0] == p[0] || '.' == p[0]) && isMatch(s.substr(1), p.substr(1));
        }
    };
    
    class Solution {
    public:
        bool isMatch(string s, string p) {
            /**
             * f[i][j]: if s[0..i-1] matches p[0..j-1]
             * if p[j - 1] != '*'
             *      f[i][j] = f[i - 1][j - 1] && s[i - 1] == p[j - 1]
             * if p[j - 1] == '*', denote p[j - 2] with x
             *      f[i][j] is true iff any of the following is true
             *      1) "x*" repeats 0 time and matches empty: f[i][j - 2]
             *      2) "x*" repeats >= 1 times and matches "x*x": s[i - 1] == x && f[i - 1][j]
             * '.' matches any single character
             */
            int m = s.size(), n = p.size();
            vector<vector<bool>> f(m + 1, vector<bool>(n + 1, false));
            
            f[0][0] = true;
            for (int i = 1; i <= m; i++)
                f[i][0] = false;
            // p[0.., j - 3, j - 2, j - 1] matches empty iff p[j - 1] is '*' and p[0..j - 3] matches empty
            for (int j = 1; j <= n; j++)
                f[0][j] = j > 1 && '*' == p[j - 1] && f[0][j - 2];
            
            for (int i = 1; i <= m; i++)
                for (int j = 1; j <= n; j++)
                    if (p[j - 1] != '*')
                        f[i][j] = f[i - 1][j - 1] && (s[i - 1] == p[j - 1] || '.' == p[j - 1]);
                    else
                        // p[0] cannot be '*' so no need to check "j > 1" here
                        f[i][j] = f[i][j - 2] || (s[i - 1] == p[j - 2] || '.' == p[j - 2]) && f[i - 1][j];
            
            return f[m][n];
        }
    };

  [1]: http://xiaohuiliucuriosity.blogspot.com/2014/12/regular-expression-matching.html
</p>


### C++ O(n)-space DP
- Author: jianchao-li
- Creation Date: Mon Jul 06 2015 00:22:49 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 11:14:00 GMT+0800 (Singapore Standard Time)

<p>
We define `dp[i][j]` to be `true` if `s[0..i)` matches `p[0..j)` and `false` otherwise. The state equations will be:

 1. `dp[i][j] = dp[i - 1][j - 1]`, if `p[j - 1] != \'*\' && (s[i - 1] == p[j - 1] || p[j - 1] == \'.\')`;
 2. `dp[i][j] = dp[i][j - 2]`, if `p[j - 1] == \'*\'` and the pattern repeats for 0 time;
 3. `dp[i][j] = dp[i - 1][j] && (s[i - 1] == p[j - 2] || p[j - 2] == \'.\')`, if `p[j - 1] == \'*\'` and the pattern repeats for at least 1 time.

```cpp
class Solution {
public:
    bool isMatch(string s, string p) {
        int m = s.size(), n = p.size();
        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1, false));
        dp[0][0] = true;
        for (int i = 0; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (p[j - 1] == \'*\') {
                    dp[i][j] = dp[i][j - 2] || (i && dp[i - 1][j] && (s[i - 1] == p[j - 2] || p[j - 2] == \'.\'));
                } else {
                    dp[i][j] = i && dp[i - 1][j - 1] && (s[i - 1] == p[j - 1] || p[j - 1] == \'.\');
                }
            }
        }
        return dp[m][n];
    }
};
```

And you may further reduce the memory usage down to two vectors (`O(n)`).

```cpp
class Solution {
public:
    bool isMatch(string s, string p) {
        int m = s.size(), n = p.size();
        vector<bool> pre(n + 1, false), cur(n + 1, false);
        cur[0] = true;
        for (int i = 0; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (p[j - 1] == \'*\') {
                    cur[j] = cur[j - 2] || (i && pre[j] && (s[i - 1] == p[j - 2] || p[j - 2] == \'.\'));
                } else {
                    cur[j] = i && pre[j - 1] && (s[i - 1] == p[j - 1] || p[j - 1] == \'.\');
                }
            }
            fill(pre.begin(), pre.end(), false);
			swap(pre, cur);
        }
        return pre[n];
    }
};
```

Or even just one vector.

```cpp
class Solution {
public:
    bool isMatch(string s, string p) {
        int m = s.size(), n = p.size();
        vector<bool> cur(n + 1, false);
        for (int i = 0; i <= m; i++) {
            bool pre = cur[0];
            cur[0] = !i;
            for (int j = 1; j <= n; j++) {
                bool temp = cur[j];
                if (p[j - 1] == \'*\') {
                    cur[j] = cur[j - 2] || (i && cur[j] && (s[i - 1] == p[j - 2] || p[j - 2] == \'.\'));
                } else {
                    cur[j] = i && pre && (s[i - 1] == p[j - 1] || p[j - 1] == \'.\');
                }
                pre = temp;
            }
        }
        return cur[n];
    }
};
```
</p>


