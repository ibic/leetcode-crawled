---
title: "Spiral Matrix"
weight: 54
#id: "spiral-matrix"
---
## Description
<div class="description">
<p>Given a matrix of <em>m</em> x <em>n</em> elements (<em>m</em> rows, <em>n</em> columns), return all elements of the matrix in spiral order.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong>
[
 [ 1, 2, 3 ],
 [ 4, 5, 6 ],
 [ 7, 8, 9 ]
]
<strong>Output:</strong> [1,2,3,6,9,8,7,4,5]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong>
[
  [1, 2, 3, 4],
  [5, 6, 7, 8],
  [9,10,11,12]
]
<strong>Output:</strong> [1,2,3,4,8,12,11,10,9,5,6,7]
</pre>

</div>

## Tags
- Array (array)

## Companies
- Microsoft - 21 (taggedByAdmin: true)
- Amazon - 14 (taggedByAdmin: false)
- Apple - 6 (taggedByAdmin: false)
- Facebook - 6 (taggedByAdmin: false)
- Bloomberg - 4 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Robinhood - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: true)
- Adobe - 4 (taggedByAdmin: false)
- Visa - 4 (taggedByAdmin: false)
- Epic Systems - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: true)
- ByteDance - 2 (taggedByAdmin: false)
- Huawei - 2 (taggedByAdmin: false)
- Walmart Labs - 5 (taggedByAdmin: false)
- Hulu - 3 (taggedByAdmin: false)
- LinkedIn - 3 (taggedByAdmin: false)
- Snapchat - 3 (taggedByAdmin: false)
- Drawbridge - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach 1: Simulation

**Intuition**

Draw the path that the spiral makes.  We know that the path should turn clockwise whenever it would go out of bounds or into a cell that was previously visited.

**Algorithm**

Let the array have $$\text{R}$$ rows and $$\text{C}$$ columns.  $$\text{seen[r][c]}$$ denotes that the cell on the$$\text{r}$$-th row and $$\text{c}$$-th column was previously visited.  Our current position is $$\text{(r, c)}$$, facing direction $$\text{di}$$, and we want to visit $$\text{R}$$ x $$\text{C}$$ total cells.

As we move through the matrix, our candidate next position is $$\text{(cr, cc)}$$.  If the candidate is in the bounds of the matrix and unseen, then it becomes our next position; otherwise, our next position is the one after performing a clockwise turn.


<iframe src="https://leetcode.com/playground/KsMbLEib/shared" frameBorder="0" width="100%" height="497" name="KsMbLEib"></iframe>


**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the total number of elements in the input matrix.  We add every element in the matrix to our final answer.

* Space Complexity: $$O(N)$$, the information stored in `seen` and in `ans`.
<br />
<br />
---
#### Approach 2: Layer-by-Layer

**Intuition**

The answer will be all the elements in clockwise order from the first-outer layer, followed by the elements from the second-outer layer, and so on.

**Algorithm**

We define the $$\text{k}$$-th outer layer of a matrix as all elements that have minimum distance to some border equal to $$\text{k}$$.  For example, the following matrix has all elements in the first-outer layer equal to 1, all elements in the second-outer layer equal to 2, and all elements in the third-outer layer equal to 3.

```plain-text
[[1, 1, 1, 1, 1, 1, 1],
 [1, 2, 2, 2, 2, 2, 1],
 [1, 2, 3, 3, 3, 2, 1],
 [1, 2, 2, 2, 2, 2, 1],
 [1, 1, 1, 1, 1, 1, 1]]
```

For each outer layer, we want to iterate through its elements in clockwise order starting from the top left corner.  Suppose the current outer layer has top-left coordinates $$\text{(r1, c1)}$$ and bottom-right coordinates $$\text{(r2, c2)}$$.

Then, the top row is the set of elements $$\text{(r1, c)}$$ for $$\text{c = c1,...,c2}$$, in that order.  The rest of the right side is the set of elements $$\text{(r, c2)}$$ for $$\text{r = r1+1,...,r2}$$, in that order.  Then, if there are four sides to this layer (ie., $$\text{r1 < r2}$$ and $$\text{c1 < c2}$$), we iterate through the bottom side and left side as shown in the solutions below.

![SpiralMatrix](../Figures/54_spiralmatrix.png)

<iframe src="https://leetcode.com/playground/sydUfzEL/shared" frameBorder="0" width="100%" height="446" name="sydUfzEL"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the total number of elements in the input matrix.  We add every element in the matrix to our final answer.

* Space Complexity: 

    - $$O(1)$$ without considering the output array, 
    since we don't use any additional data structures for our computations.

    - $$O(N)$$ if the output array is taken into account.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Super Simple and Easy to Understand Solution
- Author: qwl5004
- Creation Date: Wed Oct 01 2014 04:48:07 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 01:25:05 GMT+0800 (Singapore Standard Time)

<p>
This is a very simple and easy to understand solution. I traverse right and increment rowBegin, then traverse down and decrement colEnd, then I traverse left and decrement rowEnd, and finally I traverse up and increment colBegin.

The only tricky part is that when I traverse left or up I have to check whether the row or col still exists to prevent duplicates. If anyone can do the same thing without that check, please let me know!

Any comments greatly appreciated.

    public class Solution {
        public List<Integer> spiralOrder(int[][] matrix) {
            
            List<Integer> res = new ArrayList<Integer>();
            
            if (matrix.length == 0) {
                return res;
            }
            
            int rowBegin = 0;
            int rowEnd = matrix.length-1;
            int colBegin = 0;
            int colEnd = matrix[0].length - 1;
            
            while (rowBegin <= rowEnd && colBegin <= colEnd) {
                // Traverse Right
                for (int j = colBegin; j <= colEnd; j ++) {
                    res.add(matrix[rowBegin][j]);
                }
                rowBegin++;
                
                // Traverse Down
                for (int j = rowBegin; j <= rowEnd; j ++) {
                    res.add(matrix[j][colEnd]);
                }
                colEnd--;
                
                if (rowBegin <= rowEnd) {
                    // Traverse Left
                    for (int j = colEnd; j >= colBegin; j --) {
                        res.add(matrix[rowEnd][j]);
                    }
                }
                rowEnd--;
                
                if (colBegin <= colEnd) {
                    // Traver Up
                    for (int j = rowEnd; j >= rowBegin; j --) {
                        res.add(matrix[j][colBegin]);
                    }
                }
                colBegin ++;
            }
            
            return res;
        }
    }
</p>


### 1-liner in Python + Ruby
- Author: StefanPochmann
- Creation Date: Sat Jul 18 2015 00:27:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 07:18:53 GMT+0800 (Singapore Standard Time)

<p>
Take the first row plus the spiral order of the rotated remaining matrix. Inefficient for large matrices, but here I got it accepted in 40 ms, one of the fastest Python submissions.

Python:

    def spiralOrder(self, matrix):
        return matrix and list(matrix.pop(0)) + self.spiralOrder(zip(*matrix)[::-1])

Python 3:

    def spiralOrder(self, matrix):
        return matrix and [*matrix.pop(0)] + self.spiralOrder([*zip(*matrix)][::-1])

Ruby:
```
def spiral_order(matrix)
  (row = matrix.shift) ? row + spiral_order(matrix.transpose.reverse) : []
end
```
or
```
def spiral_order(matrix)
  matrix[0] ? matrix.shift + spiral_order(matrix.transpose.reverse) : []
end
```

### Visualization

Here's how the matrix changes by always extracting the first row and rotating the remaining matrix counter-clockwise:

        |1 2 3|      |6 9|      |8 7|      |4|  =>  |5|  =>  ||
        |4 5 6|  =>  |5 8|  =>  |5 4|  =>  |5|
        |7 8 9|      |4 7|

Now look at the first rows we extracted:

        |1 2 3|      |6 9|      |8 7|      |4|      |5|

Those concatenated are the desired result.

### Another visualization
```
  spiral_order([[1, 2, 3],
                [4, 5, 6],
                [7, 8, 9]])

= [1, 2, 3] + spiral_order([[6, 9],
                            [5, 8],
                            [4, 7]])

= [1, 2, 3] + [6, 9] + spiral_order([[8, 7],
                                     [5, 4]])

= [1, 2, 3] + [6, 9] + [8, 7] + spiral_order([[4],
                                              [5]])

= [1, 2, 3] + [6, 9] + [8, 7] + [4] + spiral_order([[5]])

= [1, 2, 3] + [6, 9] + [8, 7] + [4] + [5] + spiral_order([])

= [1, 2, 3] + [6, 9] + [8, 7] + [4] + [5] + []

= [1, 2, 3, 6, 9, 8, 7, 4, 5]
```
</p>


### A concise C++ implementation based on Directions
- Author: stellari
- Creation Date: Sat Jun 06 2015 17:07:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 07:01:31 GMT+0800 (Singapore Standard Time)

<p>
When traversing the matrix in the spiral order, at any time we follow one out of the following four directions: RIGHT DOWN LEFT UP. Suppose we are working on a 5 x 3 matrix as such:

0  1   2   3   4   5
    6   7   8   9   10
   11 12 13 14 15

Imagine a cursor starts off at (0, -1), i.e. the position at '0', then we can achieve the spiral order by doing the following:

1. Go right 5 times 
2. Go down 2 times
3. Go left 4 times
4. Go up 1 times.
5. Go right 3 times
6. Go down 0 times -> quit
  
Notice that the directions we choose always follow the order 'right->down->left->up', and for horizontal movements, the number of shifts follows:{5, 4, 3}, and vertical movements follows {2, 1, 0}. 

Thus, we can make use of a direction matrix that records the offset for all directions, then an array of two elements that stores the number of shifts for horizontal and vertical movements, respectively. This way, we really just need one for loop instead of four.

Another good thing about this implementation is that: If later we decided to do spiral traversal on a different direction (e.g. Counterclockwise), then we only need to change the Direction matrix; the main loop does not need to be touched.

    vector<int> spiralOrder(vector<vector<int>>& matrix) {
        vector<vector<int> > dirs{{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
        vector<int> res;
        int nr = matrix.size();     if (nr == 0) return res;
        int nc = matrix[0].size();  if (nc == 0) return res;
        
        vector<int> nSteps{nc, nr-1};
        
        int iDir = 0;   // index of direction.
        int ir = 0, ic = -1;    // initial position
        while (nSteps[iDir%2]) {
            for (int i = 0; i < nSteps[iDir%2]; ++i) {
                ir += dirs[iDir][0]; ic += dirs[iDir][1];
                res.push_back(matrix[ir][ic]);
            }
            nSteps[iDir%2]--;
            iDir = (iDir + 1) % 4;
        }
        return res;
    }
</p>


