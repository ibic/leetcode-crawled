---
title: "Verifying an Alien Dictionary"
weight: 903
#id: "verifying-an-alien-dictionary"
---
## Description
<div class="description">
<p>In an alien language, surprisingly they also use english lowercase letters, but possibly&nbsp;in a different <code>order</code>. The&nbsp;<code>order</code> of the alphabet&nbsp;is some permutation&nbsp;of lowercase letters.</p>

<p>Given a sequence of <code>words</code>&nbsp;written in the alien language,&nbsp;and the <code>order</code> of the alphabet,&nbsp;return <code>true</code> if and only if the given <code>words</code>&nbsp;are sorted lexicographicaly in this alien language.</p>
<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> words = [&quot;hello&quot;,&quot;leetcode&quot;], order = &quot;hlabcdefgijkmnopqrstuvwxyz&quot;
<strong>Output:</strong> true
<strong>Explanation: </strong>As &#39;h&#39; comes before &#39;l&#39; in this language, then the sequence is sorted.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> words = [&quot;word&quot;,&quot;world&quot;,&quot;row&quot;], order = &quot;worldabcefghijkmnpqstuvxyz&quot;
<strong>Output:</strong> false
<strong>Explanation: </strong>As &#39;d&#39; comes after &#39;l&#39; in this language, then words[0] &gt; words[1], hence the sequence is unsorted.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> words = [&quot;apple&quot;,&quot;app&quot;], order = &quot;abcdefghijklmnopqrstuvwxyz&quot;
<strong>Output:</strong> false
<strong>Explanation: </strong>The first three characters &quot;app&quot; match, and the second string is shorter (in size.) According to lexicographical rules &quot;apple&quot; &gt; &quot;app&quot;, because &#39;l&#39; &gt; &#39;&empty;&#39;, where &#39;&empty;&#39; is defined as the blank character which is less than any other character (<a href="https://en.wikipedia.org/wiki/Lexicographical_order" target="_blank">More info</a>).
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= words.length &lt;= 100</code></li>
	<li><code>1 &lt;= words[i].length &lt;= 20</code></li>
	<li><code>order.length == 26</code></li>
	<li>All characters in <code>words[i]</code> and <code>order</code> are English lowercase letters.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Facebook - 169 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Airbnb - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Check Adjacent Words

**Intuition**

The words are sorted lexicographically if and only if adjacent words are.  This is because order is transitive: `a <= b` and `b <= c` implies `a <= c`.

**Algorithm**

Let's check whether all adjacent words `a` and `b` have `a <= b`.

To check whether `a <= b` for two adjacent words `a` and `b`, we can find their first difference.  For example, `"applying"` and `"apples"` have a first difference of `y` vs `e`.  After, we compare these characters to the index in `order`.

Care must be taken to deal with the blank character effectively.  If for example, we are comparing `"app"` to `"apply"`, this is a first difference of `(null)` vs `"l"`.

<iframe src="https://leetcode.com/playground/cN3oA8so/shared" frameBorder="0" width="100%" height="500" name="cN3oA8so"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\mathcal{C})$$, where $$\mathcal{C}$$ is the total *content* of `words`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (python3)
```python3
from typing import Dict

class Solution:
    def isAlienSorted(self, words: List[str], order: str) -> bool:
        def compare(wa: str, wb: str, orderDict: Dict[str, int]) -> int:
            lena = len(wa)
            lenb = len(wb)
            ls = min(lena, lenb)
            for i in range(ls):
                ca = wa[i]
                cb = wb[i]
                oa = orderDict[ca]
                ob = orderDict[cb]
                if oa < ob:
                    return -1
                if oa > ob:
                    return 1
            return lena - lenb
        
        orderDict = {}
        for i in range(len(order)):
            orderDict[order[i]] = i
        
        for i in range(len(words) - 1):
            if compare(words[i], words[i+1], orderDict) > 0:
                return False
        return True
```

## Top Discussions
### [Java/C++/Python] Mapping to Normal Order
- Author: lee215
- Creation Date: Sun Dec 09 2018 12:08:35 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 18 2019 23:45:11 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Build a transform mapping from `order`,
Find all alien words with letters in normal order.

For example, if we have `order = "xyz..."`
We can map the word `"xyz"` to `"abc"` or `"123"`

Then we check if all words are in sorted order.

## **Complexity**
Time `O(NS)`
Space `O(1)`

**Java**
```java
    int[] mapping = new int[26];
    public boolean isAlienSorted(String[] words, String order) {
        for (int i = 0; i < order.length(); i++)
            mapping[order.charAt(i) - \'a\'] = i;
        for (int i = 1; i < words.length; i++)
            if (bigger(words[i - 1], words[i]))
                return false;
        return true;
    }

    boolean bigger(String s1, String s2) {
        int n = s1.length(), m = s2.length();
        for (int i = 0; i < n && i < m; ++i)
            if (s1.charAt(i) != s2.charAt(i))
                return mapping[s1.charAt(i) - \'a\'] > mapping[s2.charAt(i) - \'a\'];
        return n > m;
    }
```

**C++**:
```cpp
    bool isAlienSorted(vector<string> words, string order) {
        int mapping[26];
        for (int i = 0; i < 26; i++)
            mapping[order[i] - \'a\'] = i;
        for (string &w : words)
            for (char &c : w)
                c = mapping[c - \'a\'];
        return is_sorted(words.begin(), words.end());
    }
```

**Python**
```py
    def isAlienSorted(self, words, order):
        m = {c: i for i, c in enumerate(order)}
        words = [[m[c] for c in w] for w in words]
        return all(w1 <= w2 for w1, w2 in zip(words, words[1:]))
```

**Python 1-line**
Slow, just for fun
```py
    def isAlienSorted(self, words, order):
        return words == sorted(words, key=lambda w: map(order.index, w))
```
</p>


### Python straightforward solution
- Author: cenkay
- Creation Date: Sun Dec 09 2018 12:05:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 09 2018 12:05:52 GMT+0800 (Singapore Standard Time)

<p>
* Hash indexes of each character for better runtime
* Compare every adjacent word
* If any letter of former word is in higher order, return False
* If current letter of former word is in lower order, forget the rest of word 
* If lenght of former word is longer and latter word is substring of former, return False (apple & app etc.)
* Return True
```
class Solution:
    def isAlienSorted(self, words, order):
        ind = {c: i for i, c in enumerate(order)}
        for a, b in zip(words, words[1:]):
            if len(a) > len(b) and a[:len(b)] == b:
                return False
            for s1, s2 in zip(a, b):
                if ind[s1] < ind[s2]:
                    break
                elif ind[s1] > ind[s2]:
                    return False
        return True
```
</p>


### why this test case is true?
- Author: bluecode2017
- Creation Date: Sun Jun 23 2019 11:14:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 23 2019 11:14:26 GMT+0800 (Singapore Standard Time)

<p>
![image](https://assets.leetcode.com/users/bluecode2017/image_1561259655.png)

</p>


