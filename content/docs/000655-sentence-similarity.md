---
title: "Sentence Similarity"
weight: 655
#id: "sentence-similarity"
---
## Description
<div class="description">
<p>We can represent a&nbsp;sentence as an array of words, for example, the&nbsp;sentence <code>&quot;I am happy with leetcode&quot;</code> can be represented as <code>arr = [&quot;I&quot;,&quot;am&quot;,happy&quot;,&quot;with&quot;,&quot;leetcode&quot;]</code>.</p>

<p>Given two&nbsp;sentences&nbsp;<code>sentence1</code> and&nbsp;<code>sentence2</code> each represented as a string array and given an array of string pairs <code>similarPairs</code> where&nbsp;<code>similarPairs[i] = [x<sub>i</sub>, y<sub>i</sub>]</code>&nbsp;indicates that the two words&nbsp;<code>x<sub>i</sub></code> and&nbsp;<code>y<sub>i</sub></code> are similar.</p>

<p>Return <em><code>true</code> if&nbsp;<code>sentence1</code>&nbsp;and&nbsp;<code>sentence2</code>&nbsp;are similar, or <code>false</code> if they are not similar</em>.</p>

<p>Two sentences are similar if:</p>

<ul>
	<li>They have <strong>the same length</strong> (i.e. the same number of words)</li>
	<li><code>sentence1[i]</code> and&nbsp;<code>sentence2[i]</code>&nbsp;are similar.</li>
</ul>

<p>Notice that a word is always similar to itself, also notice that the similarity relation is not transitive. For example, if the words&nbsp;<code><font face="monospace">a</font></code>&nbsp;and <code>b</code> are similar and the words <code>b</code>&nbsp;and <code>c</code> are similar, <code>a</code> and <code>c</code> are&nbsp;<strong>not&nbsp;necessarily similar</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> sentence1 = [&quot;great&quot;,&quot;acting&quot;,&quot;skills&quot;], sentence2 = [&quot;fine&quot;,&quot;drama&quot;,&quot;talent&quot;], similarPairs = [[&quot;great&quot;,&quot;fine&quot;],[&quot;drama&quot;,&quot;acting&quot;],[&quot;skills&quot;,&quot;talent&quot;]]
<strong>Output:</strong> true
<strong>Explanation:</strong> The two sentences have the same length and each word i of sentence1 is also similar to the corresponding word in sentence2.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> sentence1 = [&quot;great&quot;], sentence2 = [&quot;great&quot;], similarPairs = []
<strong>Output:</strong> true
<strong>Explanation:</strong> A word is similar to itself.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> sentence1 = [&quot;great&quot;], sentence2 = [&quot;doubleplus&quot;,&quot;good&quot;], similarPairs = [[&quot;great&quot;,&quot;doubleplus&quot;]]
<strong>Output:</strong> false
<strong>Explanation:</strong> As they don&#39;t have the same length, we return false.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;=&nbsp;sentence1.length,&nbsp;sentence2.length &lt;= 1000</code></li>
	<li><code>1 &lt;=&nbsp;sentence1[i].length,&nbsp;sentence2[i].length &lt;= 20</code></li>
	<li><code>sentence1[i]</code> and&nbsp;<code>sentence2[i]</code>&nbsp;consist of lower-case and upper-case English letters.</li>
	<li><code>0 &lt;=&nbsp;similarPairs.length &lt;= 1000</code></li>
	<li><code>similarPairs[i].length == 2</code></li>
	<li><code>1 &lt;=&nbsp;x<sub>i</sub>.length,&nbsp;y<sub>i</sub>.length&nbsp;&lt;= 20</code></li>
	<li><code>x<sub>i</sub></code> and <code>y<sub>i</sub></code>&nbsp;consist of lower-case and upper-case English letters.</li>
	<li>All the pairs <code>(x<sub>i</sub>,<sub>&nbsp;</sub>y<sub>i</sub>)</code> are <strong>distinct</strong>.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Google - 10 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Set [Accepted]

**Intuition and Algorithm**

To check whether `words1[i]` and `words2[i]` are similar, either they are the same word, or `(words1[i], words2[i])` or `(words2[i], words1[i])` appear in `pairs`.

To check whether `(words1[i], words2[i])` appears in `pairs` quickly, we could put all such pairs into a Set structure.

<iframe src="https://leetcode.com/playground/QrhWYt5i/shared" frameBorder="0" width="100%" height="361" name="QrhWYt5i"></iframe>


**Complexity Analysis**

* Time Complexity: $$O(N+P)$$, where $$N$$ is the maximum length of `words1` and `words2`, and $$P$$ is the length of `pairs`.

* Space Complexity: $$O(P)$$, the size of `pairs`.  Intermediate objects created in evaluating whether a pair of words are similar are created one at a time, so they don't take additional space.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Super Clean Code (Similarity I and II)
- Author: FLAGbigoffer
- Creation Date: Mon Nov 27 2017 01:25:01 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 01:14:19 GMT+0800 (Singapore Standard Time)

<p>
Sentence Similarity I (Transitive is not allowed.)
```
class Solution {
    public boolean areSentencesSimilar(String[] words1, String[] words2, String[][] pairs) {
        if (words1.length != words2.length) return false;
        
        Map<String, Set<String>> map = new HashMap<>();
        for (String[] p : pairs) {
            map.putIfAbsent(p[0], new HashSet<>());
            map.putIfAbsent(p[1], new HashSet<>());
            map.get(p[0]).add(p[1]);
            map.get(p[1]).add(p[0]);
        }
        
        for (int i = 0; i < words1.length; i++) {
            if (words1[i].equals(words2[i])) continue;
            if (!map.containsKey(words1[i])) return false;
            if (!map.get(words1[i]).contains(words2[i])) return false;
        }
        
        return true;
    }
}
```


Sentence Similarity II (Transitive is allowed.)
The idea is simple:
1. Build the graph according to the similar word pairs. Each word is a graph node.
2. For each word in words1, we do DFS search to see if the corresponding word is existing in words2.

See the clean code below. Happy coding!
```
class Solution {
    public boolean areSentencesSimilarTwo(String[] words1, String[] words2, String[][] pairs) {
        if (words1.length != words2.length) {
            return false;
        }
        
        Map<String, Set<String>> graph = new HashMap<>();
        for (String[] p : pairs) {
            graph.putIfAbsent(p[0], new HashSet<>());
            graph.putIfAbsent(p[1], new HashSet<>());
            graph.get(p[0]).add(p[1]);
            graph.get(p[1]).add(p[0]);
        }
        
        for (int i = 0; i < words1.length; i++) {
            if (words1[i].equals(words2[i])) continue;           
            if (!graph.containsKey(words1[i])) return false;            
            if (!dfs(graph, words1[i], words2[i], new HashSet<>())) return false;
        }
        
        return true;
    }
    
    private boolean dfs(Map<String, Set<String>> graph, String source, String target, Set<String> visited) {
        if (graph.get(source).contains(target)) return true;
        
        if (visited.add(source)) {
            for (String next : graph.get(source)) {
                if (!visited.contains(next) && dfs(graph, next, target, visited)) 
                    return true;
            }
        }
        return false;
    }
}
````
</p>


### Simple Python Solution - 32ms!
- Author: yangshun
- Creation Date: Sun Nov 26 2017 14:20:30 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 12:37:01 GMT+0800 (Singapore Standard Time)

<p>
We use a dictionary to map words with their similar words and simply look up the dictionary when we compare pairs of words in `words1` and `words2` by checking if the second word is in the first word's similar word set.

For this pairs of words, the following dict is generated:

`[["a", "b"], ["a", "c"], ["b", "d"]]`

```
{
  "a": set(["b", "c"]),
  "b": set(["a", "d"]),
  "c": set(["a"]),
  "d": set(["b"]),
}
```

*- Yangshun*

```
class Solution(object):
    def areSentencesSimilar(self, words1, words2, pairs):
        from collections import defaultdict
        if len(words1) != len(words2):
            return False
        words = defaultdict(set)
        for word1, word2 in pairs:
            words[word1].add(word2)
            words[word2].add(word1)
        for word1, word2 in zip(words1, words2):
            if word1 != word2 and word2 not in words[word1]:
                return False
        return True
```
</p>


### Ambiguity of the question
- Author: imsure
- Creation Date: Tue Jan 02 2018 07:37:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 29 2018 08:54:42 GMT+0800 (Singapore Standard Time)

<p>
There is one `ambiguity` of the question: by looking at the official solution, there is an `unspecified assumption` for this problem: the words are matched only at the same position, e.g., `words1[0]` is matched against `words2[0]`, `words1[1]` is matched against `words2[1]`, so on and so forth. However, considering this two sentences: `Bob is a talented actor` and `a talented actor is Bob`, shouldn't they also be similar? But the official solution gave `false` in this case.
</p>


