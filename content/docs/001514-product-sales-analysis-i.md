---
title: "Product Sales Analysis I"
weight: 1514
#id: "product-sales-analysis-i"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Sales</code></p>

<pre>
+-------------+-------+
| Column Name | Type  |
+-------------+-------+
| sale_id     | int   |
| product_id  | int   |
| year        | int   |
| quantity    | int   |
| price       | int   |
+-------------+-------+
(sale_id, year) is the primary key of this table.
product_id is a foreign key to <code>Product</code> table.
Note that the price is per unit.
</pre>

<p>Table:&nbsp;<code>Product</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| product_id   | int     |
| product_name | varchar |
+--------------+---------+
product_id is the primary key of this table.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query that reports all <strong>product names</strong> of the products in the <code>Sales</code> table along with their selling <strong>year</strong> and&nbsp;<strong>price</strong>.</p>

<p>For example:</p>

<pre>
<code>Sales</code> table:
+---------+------------+------+----------+-------+
| sale_id | product_id | year | quantity | price |
+---------+------------+------+----------+-------+ 
| 1       | 100        | 2008 | 10       | 5000  |
| 2       | 100        | 2009 | 12       | 5000  |
| 7       | 200        | 2011 | 15       | 9000  |
+---------+------------+------+----------+-------+

Product table:
+------------+--------------+
| product_id | product_name |
+------------+--------------+
| 100        | Nokia        |
| 200        | Apple        |
| 300        | Samsung      |
+------------+--------------+

Result table:
+--------------+-------+-------+
| product_name | year  | price |
+--------------+-------+-------+
| Nokia        | 2008  | 5000  |
| Nokia        | 2009  | 5000  |
| Apple        | 2011  | 9000  |
+--------------+-------+-------+
</pre>

</div>

## Tags


## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL 99.42%
- Author: ye15
- Creation Date: Sat Oct 26 2019 10:36:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 26 2019 10:36:06 GMT+0800 (Singapore Standard Time)

<p>
Likely what is happening is that in the `Sales` table there are multiple transactions of the same `product_id`, `year`, `price` at different `quantity`. As a result, if `DISTINCT` entries were retrieved before joining with `Product` table, it runs a lot faster. 
```
SELECT DISTINCT 
    P.product_name, S.year, S.price 
FROM 
    (SELECT DISTINCT product_id, year, price FROM Sales) S
INNER JOIN
    Product AS P
USING (product_id);
```
</p>


### Why is this incorrect / timeout?
- Author: buschl
- Creation Date: Sun Feb 02 2020 10:02:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 02 2020 10:02:16 GMT+0800 (Singapore Standard Time)

<p>
Can someone help me understand what\'s wrong with this answer? All I get is a timeout error


\'\'\'select p.product_name, 
	s.year, 
    s.price
FROM product p
JOIN sales s
	ON p.product_id = s.product_id\'\'\'
</p>


### easy solution
- Author: alishaanjum4992
- Creation Date: Wed Nov 20 2019 11:00:08 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Nov 20 2019 11:00:08 GMT+0800 (Singapore Standard Time)

<p>
# Write your MySQL query statement below
select p.product_name, s.year, s.price
from product p
join sales s
on p.product_id = s.product_id
</p>


