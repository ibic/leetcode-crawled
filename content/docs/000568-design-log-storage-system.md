---
title: "Design Log Storage System"
weight: 568
#id: "design-log-storage-system"
---
## Description
<div class="description">
<p>You are given several logs, where each log contains a unique ID and timestamp. Timestamp is a string that has the following format: <code>Year:Month:Day:Hour:Minute:Second</code>, for example, <code>2017:01:01:23:59:59</code>. All domains are zero-padded decimal numbers.</p>

<p>Implement the <code>LogSystem</code> class:</p>

<ul>
	<li><code>LogSystem()</code> Initializes the <code>LogSystem</code><b> </b>object.</li>
	<li><code>void put(int id, string timestamp)</code> Stores the given log <code>(id, timestamp)</code> in your storage system.</li>
	<li><code>int[] retrieve(string start, string end, string granularity)</code> Returns the IDs of the logs whose timestamps are within the range from <code>start</code> to <code>end</code> inclusive. <code>start</code> and <code>end</code> all have the same format as <code>timestamp</code>, and <code>granularity</code> means how precise the range should be (i.e. to the exact <code>Day</code>, <code>Minute</code>, etc.). For example, <code>start = &quot;2017:01:01:23:59:59&quot;</code>, <code>end = &quot;2017:01:02:23:59:59&quot;</code>, and <code>granularity = &quot;Day&quot;</code> means that we need to find the logs within the inclusive range from <strong>Jan. 1st 2017</strong> to <strong>Jan. 2nd 2017</strong>, and the <code>Hour</code>, <code>Minute</code>, and <code>Second</code> for each log entry can be ignored.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;LogSystem&quot;, &quot;put&quot;, &quot;put&quot;, &quot;put&quot;, &quot;retrieve&quot;, &quot;retrieve&quot;]
[[], [1, &quot;2017:01:01:23:59:59&quot;], [2, &quot;2017:01:01:22:59:59&quot;], [3, &quot;2016:01:01:00:00:00&quot;], [&quot;2016:01:01:01:01:01&quot;, &quot;2017:01:01:23:00:00&quot;, &quot;Year&quot;], [&quot;2016:01:01:01:01:01&quot;, &quot;2017:01:01:23:00:00&quot;, &quot;Hour&quot;]]
<strong>Output</strong>
[null, null, null, null, [3, 2, 1], [2, 1]]

<strong>Explanation</strong>
LogSystem logSystem = new LogSystem();
logSystem.put(1, &quot;2017:01:01:23:59:59&quot;);
logSystem.put(2, &quot;2017:01:01:22:59:59&quot;);
logSystem.put(3, &quot;2016:01:01:00:00:00&quot;);

// return [3,2,1], because you need to return all logs between 2016 and 2017.
logSystem.retrieve(&quot;2016:01:01:01:01:01&quot;, &quot;2017:01:01:23:00:00&quot;, &quot;Year&quot;);

// return [2,1], because you need to return all logs between Jan. 1, 2016 01:XX:XX and Jan. 1, 2017 23:XX:XX.
// Log 3 is not returned because Jan. 1, 2016 00:00:00 comes before the start of the range.
logSystem.retrieve(&quot;2016:01:01:01:01:01&quot;, &quot;2017:01:01:23:00:00&quot;, &quot;Hour&quot;);
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= id &lt;= 500</code></li>
	<li><code>2000 &lt;= Year &lt;= 2017</code></li>
	<li><code>1 &lt;= Month &lt;= 12</code></li>
	<li><code>1 &lt;= Day &lt;= 31</code></li>
	<li><code>0 &lt;= Hour &lt;= 23</code></li>
	<li><code>0 &lt;= Minute, Second &lt;= 59</code></li>
	<li><code>granularity</code> is one of the values <code>[&quot;Year&quot;, &quot;Month&quot;, &quot;Day&quot;, &quot;Hour&quot;, &quot;Minute&quot;, &quot;Second&quot;]</code>.</li>
	<li>At most <code>500</code> calls will be made to <code>put</code> and <code>retrieve</code>.</li>
</ul>

</div>

## Tags
- String (string)
- Design (design)

## Companies
- Twitter - 4 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Snapchat - 0 (taggedByAdmin: true)

## Official Solution
[TOC]



## Solution

---
#### Approach #1 Converting timestamp into a number [Accepted]

This solution is based on converting the given timestap into a number. This can help because retrieval of Logs lying
within a current range can be very easily done if the range to be used can be represented in the form of a single number.
Let's look at the individual implementations directly.

1. `put`: Firstly, we split the given timestamp based on `:` and store the individual components obtained into an $$st$$ array.
Now, in order to put this log's entry into the storage, firstly, we convert this timestamp, now available as individual components 
in the $$st$$ array into a single number. To obtain a number which is unique for each timestamp, the number chosen is such that 
it represents the timestamp in terms of seconds. But, doing so for the Year values can lead to very large numbers, which could 
lead to a potential overflow. Since, we know that the Year's value can start from 2000 only, we subtract 1999 from the Year's value 
before doing the conversion of the given timestamp into seconds. We store this timestamp(in the form of a number now), along with the 
Log's id, in s $$list$$, which stores data in the form $$[converted\_timestamp, id]$$. 

2. `retrieve`: In order to retrieve the logs' ids lying within the timestamp $$s$$ and $$e$$, with a granularity $$gra$$, firstly, we 
need to convert the given timestamps into seconds. But, since, we need to take care of granularity, before doing the conversion, we 
need to consider the effect of granularity. Granularity, in a way, depicts the precision of the results. Thus, for a granularity corresponding to 
a Day, we need to consider the portion of the timestamp considering the precision upto Day only. The rest of the fields 
can be assumed to be all 0's. After doing this for both the start and end timestamp, we do the conversion of the timestamp with the 
required precision into seconds. Once this has been done, we iterate over all the logs in the $$list$$ to obtain the ids of those 
logs which lie within the required range. We keep on adding these ids to a $$res$$ list which is returned at the end of this function call.

<iframe src="https://leetcode.com/playground/zHbRZ2y6/shared" frameBorder="0" name="zHbRZ2y6" width="100%" height="515"></iframe>
**Performance Analysis**

* The `put`method takes $$O(1)$$ time to insert a new entry into the given set of logs.

* The `retrieve` method takes $$O(n)$$ time to retrieve the logs in the required range. Determining the granularity takes $$O(1)$$ time. But, to find the logs lying in the required range, we need to iterate over the set of logs atleast once. Here, $$n$$ refers to the number of entries in the current set of logs.


---
#### Approach #2 Better Retrieval [Accepted]

This method remains almost the same as the last approach, except that, in order to store the timestamp data, we make use 
of a TreeMap instead of a list, with the key values being the timestamps converted in seconds form and the values being the 
ids of the corresponding logs.

Thus, the `put` method remains the same as the last approach. However, we can save some time in `retrieve` approach by making use 
of `tailMap` function of TreeMap, which stores the entries in the form of a sorted navigable binary tree. By making use of this function, instead of iterating over all the timestamps in 
the storage to find the timestamps lying within the required range(after considering the granularity as in the last approach),
we can reduce the search space to only those elements whose timestamp is larger than(or equal to) the starting timestamp value.


<iframe src="https://leetcode.com/playground/5bhZzaNE/shared" frameBorder="0" name="5bhZzaNE" width="100%" height="515"></iframe>
**Performance Analysis**

* The TreeMap is maintained internally as a Red-Black(balanced) tree. Thus, the `put`method takes $$O\big(log(n)\big)$$ time to insert a new entry into the given set of logs. Here, $$n$$ refers to the number of entries currently present in the given set of logs.

* The `retrieve` method takes $$O(m_{start})$$ time to retrieve the logs in the required range. Determining the granularity takes $$O(1)$$ time. To find the logs in the required range, we only need to iterate over those elements which already lie in the required range. Here, $$m_{start}$$ refers to the number of entries in the current set of logs which have a timestamp greater than the current $$start$$ value.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java range query using TreeMap.subMap()
- Author: dreamchase
- Creation Date: Fri Jul 07 2017 03:01:06 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 05:43:50 GMT+0800 (Singapore Standard Time)

<p>
Given the granularity we can set a lower bound and upper bound of timestamp so we could do a range query using TreeMap. To get the lower bound we append a suffix of "2000:01:01:00:00:00" to the prefix of s and to get the upper bound we append a suffix of "2017:12:31:23:59:59" to the prefix of e.
```
public class LogSystem {
    private String min, max;
    private HashMap<String, Integer> map;
    private TreeMap<String, LinkedList<Integer>> logs;
    public LogSystem() {
        min = "2000:01:01:00:00:00";
        max = "2017:12:31:23:59:59";
        map = new HashMap<>();
        map.put("Year", 4);
        map.put("Month", 7);
        map.put("Day", 10);
        map.put("Hour", 13);
        map.put("Minute", 16);
        map.put("Second", 19);
        logs = new TreeMap<>();
    }

    public void put(int id, String timestamp) {
        if(!logs.containsKey(timestamp)) logs.put(timestamp, new LinkedList<>());
        logs.get(timestamp).add(id);
    }

    public List<Integer> retrieve(String s, String e, String gra) {
        int index = map.get(gra);
        String start = s.substring(0, index)+min.substring(index), end = e.substring(0, index)+max.substring(index);
        NavigableMap<String, LinkedList<Integer>> sub = logs.subMap(start, true, end, true);
        LinkedList<Integer> ans = new LinkedList<>();
        for (Map.Entry<String, LinkedList<Integer>> entry : sub.entrySet()) {
            ans.addAll(entry.getValue());
        }
        return ans;
    }
}
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun Jul 02 2017 11:04:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 15 2018 05:12:07 GMT+0800 (Singapore Standard Time)

<p>
```
class LogSystem(object):
    def __init__(self):
        self.logs = []

    def put(self, tid, timestamp):
        self.logs.append((tid, timestamp))
        
    def retrieve(self, s, e, gra):
        index = {'Year': 5, 'Month': 8, 'Day': 11, 
                 'Hour': 14, 'Minute': 17, 'Second': 20}[gra]
        start = s[:index]
        end = e[:index]
        
        return sorted(tid for tid, timestamp in self.logs
                      if start <= timestamp[:index] <= end)
```
Because the number of operations is very small, we do not need a complicated structure to store the logs: a simple list will do.

Let's focus on the ```retrieve``` function.  For each granularity, we should consider all timestamps to be truncated to that granularity.  For example, if the granularity is ```'Day'```, we should truncate the timestamp '2017:07:02:08:30:12' to be '2017:07:02'.  Now for each log, if the truncated timetuple ```cur``` is between ```start``` and ```end```, then we should add the id of that log into our answer.
</p>


### Concise Java Solution
- Author: compton_scatter
- Creation Date: Sun Jul 02 2017 11:24:55 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 06:13:02 GMT+0800 (Singapore Standard Time)

<p>
```
public class LogSystem {
    
    List<String[]> timestamps = new LinkedList<>();
    List<String> units = Arrays.asList("Year", "Month", "Day", "Hour", "Minute", "Second");
    int[] indices = new int[]{4,7,10,13,16,19};
    
    public void put(int id, String timestamp) { timestamps.add(new String[]{Integer.toString(id), timestamp}); }

    public List<Integer> retrieve(String s, String e, String gra) {
        List<Integer> res = new LinkedList<>();
        int idx = indices[units.indexOf(gra)];
        for (String[] timestamp : timestamps) {
            if (timestamp[1].substring(0, idx).compareTo(s.substring(0, idx)) >= 0 &&
               	timestamp[1].substring(0, idx).compareTo(e.substring(0, idx)) <= 0) res.add(Integer.parseInt(timestamp[0]));
        }
        return res;
    }
}
```
</p>


