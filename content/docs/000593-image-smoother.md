---
title: "Image Smoother"
weight: 593
#id: "image-smoother"
---
## Description
<div class="description">
<p>Given a 2D integer matrix M representing the gray scale of an image, you need to design a smoother to make the gray scale of each cell becomes the average gray scale (rounding down) of all the 8 surrounding cells and itself.  If a cell has less than 8 surrounding cells, then use as many as you can.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b>
[[1,1,1],
 [1,0,1],
 [1,1,1]]
<b>Output:</b>
[[0, 0, 0],
 [0, 0, 0],
 [0, 0, 0]]
<b>Explanation:</b>
For the point (0,0), (0,2), (2,0), (2,2): floor(3/4) = floor(0.75) = 0
For the point (0,1), (1,0), (1,2), (2,1): floor(5/6) = floor(0.83333333) = 0
For the point (1,1): floor(8/9) = floor(0.88888889) = 0
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The value in the given matrix is in the range of [0, 255].</li>
<li>The length and width of the given matrix are in the range of [1, 150].</li>
</ol>
</p>
</div>

## Tags
- Array (array)

## Companies
- Apple - 3 (taggedByAdmin: false)
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Iterate Through Grid

**Intuition and Algorithm**

For each cell in the grid, look at the immediate neighbors - up to 9 of them, including the original cell.

Then, we will add the sum of the neighbors into `ans[r][c]` while recording `count`, the number of such neighbors.  The final answer is the sum divided by the count.

<iframe src="https://leetcode.com/playground/YkxmvELL/shared" frameBorder="0" width="100%" height="395" name="YkxmvELL"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of pixels in our image.  We iterate over every pixel.

* Space Complexity: $$O(N)$$, the size of our answer.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ O(1) space using "game of life" idea
- Author: jeanmuu
- Creation Date: Fri Sep 01 2017 04:26:04 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 24 2018 20:46:27 GMT+0800 (Singapore Standard Time)

<p>
Derived from StefanPochmann's idea in "game of life": the board has ints  in [0, 255], hence only 8-bit is used, we can use the middle 8-bit to store the new state (average value),  replace the old state with the new state by shifting all values 8 bits to the right.
```
    vector<vector<int>> imageSmoother(vector<vector<int>>& M) {
        int m = M.size(), n = M[0].size();
        if (m == 0 || n == 0) return {{}};
        vector<vector<int>> dirs = {{0,1},{0,-1},{1,0},{-1,0},{-1,-1},{1,1},{-1,1},{1,-1}};
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int sum = M[i][j], cnt = 1;
                for (int k = 0; k < dirs.size(); k++) {
                    int x = i + dirs[k][0], y = j + dirs[k][1];
                    if (x < 0 || x > m - 1 || y < 0 || y > n - 1) continue;
                    sum += (M[x][y] & 0xFF);
                    cnt++;
                }
                M[i][j] |= ((sum / cnt) << 8);
            }
        }
         for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                M[i][j] >>= 8;
            }
         }
        return M;
    }

```
</p>


### Don't understand the question and sample output
- Author: mbowen13
- Creation Date: Sat Sep 09 2017 06:28:45 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 10:40:55 GMT+0800 (Singapore Standard Time)

<p>
Is there a way to reword the question? I do not understand what is being asked.
</p>


### Very Clean Solution in Java
- Author: seanzhou1023
- Creation Date: Sat Aug 26 2017 13:06:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 00:16:32 GMT+0800 (Singapore Standard Time)

<p>
Here we have a check function to check the boundary and a inner double loop to traverse the 9 potential candidates:

```
public class ImageSmoother {

    public int[][] imageSmoother(int[][] M) {
        if (M == null) return null;
        int rows = M.length;
        if (rows == 0) return new int[0][];
        int cols = M[0].length;

        int result[][] = new int[rows][cols];

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                int count = 0;
                int sum = 0;
                for (int incR : new int[]{-1, 0, 1}) {
                    for (int incC : new int[]{-1, 0, 1}) {
                        if (isValid(row + incR, col + incC, rows, cols)) {
                            count++;
                            sum += M[row + incR][col + incC];
                        }
                    }
                }
                result[row][col] = sum / count;
            }
        }

        return result;

    }

    private boolean isValid(int x, int y, int rows, int cols) {
        return x >= 0 && x < rows && y >= 0 && y < cols;
    }
}
```
</p>


