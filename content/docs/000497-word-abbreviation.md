---
title: "Word Abbreviation"
weight: 497
#id: "word-abbreviation"
---
## Description
<div class="description">
<p>Given an array of n distinct non-empty strings, you need to generate <b>minimal</b> possible abbreviations for every word following rules below.</p>

<ol>
<li>Begin with the first character and then the number of characters abbreviated, which followed by the last character.</li>
<li>If there are any conflict, that is more than one words share the same abbreviation, a longer prefix is used instead of only the first character until making the map from word to abbreviation become unique. In other words, a final abbreviation cannot map to more than one original words.</li>
<li> If the abbreviation doesn't make the word shorter, then keep it as original.</li>
</ol>

<p><b>Example:</b><br />
<pre>
<b>Input:</b> ["like", "god", "internal", "me", "internet", "interval", "intension", "face", "intrusion"]
<b>Output:</b> ["l2e","god","internal","me","i6t","interval","inte4n","f2e","intr4n"]
</pre>
</p>


<b>Note:</b> 
<ol>
<li> Both n and the length of each word will not exceed 400.</li>
<li> The length of each word is greater than 1.</li>
<li> The words consist of lowercase English letters only.</li>
<li> The return answers should be <b>in the same order</b> as the original array.</li>
</ol>
</div>

## Tags
- String (string)
- Sort (sort)

## Companies
- Google - 3 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)
- Snapchat - 3 (taggedByAdmin: true)
- Grab - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Greedy [Accepted]

**Intuition**

Let's choose the shortest abbreviation for each word.  Then, while we have duplicates, we'll increase the length of all duplicates.

**Algorithm**

For example, let's say we have `"aabaaa", "aacaaa", "aacdaa"`, then we start with `"a4a", "a4a", "a4a"`.  Since these are duplicated, we lengthen them to `"aa3a", "aa3a", "aa3a"`.  They are still duplicated, so we lengthen them to `"aab2a", "aac2a", "aac2a"`.  The last two are still duplicated, so we lengthen them to `"aacaaa", "aacdaa"`.

Throughout this process, we were tracking an index `prefix[i]` which told us up to what index to take the prefix to.  For example, `prefix[i] = 2` means to take a prefix of `word[0], word[1], word[2]`.

<iframe src="https://leetcode.com/playground/4A56cMbF/shared" frameBorder="0" width="100%" height="500" name="4A56cMbF"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(C^2)$$ where $$C$$ is the number of characters across all words in the given array.

* Space Complexity: $$O(C)$$.

---
#### Approach #2: Group + Least Common Prefix [Accepted]

**Intuition and Algorithm**

Two words are only eligible to have the same abbreviation if they have the same first letter, last letter, and length.  Let's group each word based on these properties, and then sort out the conflicts.

In each group `G`, if a word `W` has a longest common prefix `P` with any other word `X` in `G`, then our abbreviation must contain a prefix of more than `|P|` characters.  The longest common prefixes must occur with words adjacent to `W` (in lexicographical order), so we can just sort `G` and look at the adjacent words.

<iframe src="https://leetcode.com/playground/SUrAuMyC/shared" frameBorder="0" width="100%" height="500" name="SUrAuMyC"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(C \log C)$$ where $$C$$ is the number of characters across all words in the given array.  The complexity is dominated by the sorting step.

* Space Complexity: $$O(C)$$.

---
#### Approach #3: Group + Trie [Accepted]

**Intuition and Algorithm**

As in *Approach #1*, let's group words based on length, first letter, and last letter, and discuss when words in a group do not share a longest common prefix.

Put the words of a group into a trie (prefix tree), and count at each node (representing some prefix `P`) the number of words with prefix `P`.  If the count is 1, we know the prefix is unique.

<iframe src="https://leetcode.com/playground/DvpehjMe/shared" frameBorder="0" width="100%" height="500" name="DvpehjMe"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(C)$$ where $$C$$ is the number of characters across all words in the given array.

* Space Complexity: $$O(C)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Really simple and straightforward Java solution
- Author: ckcz123
- Creation Date: Sun Mar 12 2017 13:58:52 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 07:32:58 GMT+0800 (Singapore Standard Time)

<p>
Make abbreviation for each word.
Then, check each word, if there are some strings which have same abbreviation with it, increase the prefix.

``` java
    public List<String> wordsAbbreviation(List<String> dict) {
        int len=dict.size();
        String[] ans=new String[len];
        int[] prefix=new int[len];
        for (int i=0;i<len;i++) {
            prefix[i]=1;
            ans[i]=makeAbbr(dict.get(i), 1); // make abbreviation for each string
        }
        for (int i=0;i<len;i++) {
            while (true) {
                HashSet<Integer> set=new HashSet<>();
                for (int j=i+1;j<len;j++) {
                    if (ans[j].equals(ans[i])) set.add(j); // check all strings with the same abbreviation
                }
                if (set.isEmpty()) break;
                set.add(i);
                for (int k: set) 
                    ans[k]=makeAbbr(dict.get(k), ++prefix[k]); // increase the prefix
            }
        }
        return Arrays.asList(ans);
    }

    private String makeAbbr(String s, int k) {
        if (k>=s.length()-2) return s;
        StringBuilder builder=new StringBuilder();
        builder.append(s.substring(0, k));
        builder.append(s.length()-1-k);
        builder.append(s.charAt(s.length()-1));
        return builder.toString();
    }
```
</p>


### HashMap + Trie => O(nL) solution
- Author: mgispk
- Creation Date: Sun Mar 12 2017 13:55:33 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 22:36:01 GMT+0800 (Singapore Standard Time)

<p>
The basic idea is to group all conflicted words, and then resolve the conflicts using Trie. The time complexity will be O(nL) for building trie, O(nL) to resolve conflicts, O(n) to group words. So the time complexity will be O(n(2L + 1). n is the number of words, and L is the average length of each words. 

I added the comments in code, so you can directly see the code. Please correct me if I make some mistakes and welcome to make my code concise.
```java
public class Solution {
    
    public List<String> wordsAbbreviation(List<String> dict) {
        Map<String, List<Integer>> abbrMap = new HashMap<>();
        // 1) create result set
        List<String> res = new ArrayList<>(Collections.nCopies(dict.size(), null));
        // 2) Group all words with the same shortest abbreviation. For example:
        // "internal", "interval" => grouped by "i6l"
        // "intension", "intrusion" => grouped by "i7n"
        // "god" => grouped by "god"
        // we can notice that only words with the same length and the same start
        // and end letter could be grouped together
        for (int i = 0; i < dict.size(); i ++) {
            String word = dict.get(i);
            String st = getShortestAbbr(word);
            List<Integer> pos = abbrMap.get(st);
            if (pos == null) {
                pos = new ArrayList<>();
                abbrMap.put(st, pos);
            }
            pos.add(i);
        }
        // 3) Resolve conflicts in each group
        for (Map.Entry<String, List<Integer>> entry : abbrMap.entrySet()) {
            String abbr = entry.getKey();
            List<Integer> pos = entry.getValue();
            resolve(dict, res, abbr, pos);
        }
        return res;
    }
    
    /**
     * To resolve conflicts in a group, we could build a trie for all the words
     * in the group. The trie node will contain the count of words that has the
     * same prefix. Then we could use this trie to determine when we could resolve
     * a conflict by identifying that the count of words in that trie node will only
     * have one word has the prefix.
     */
    private void resolve(List<String> dict, List<String> res, String abbr, List<Integer> pos) {
        if (pos.size() == 1) {
            res.set(pos.get(0), abbr);
        } else {
            Trie trie = buildTrie(dict, pos);
            for (int p : pos) {
                String w = dict.get(p);
                Trie cur = trie;
                int i = 0;
                int n = w.length();
                // while loop to find the trie node which only has the 1 word which has
                // the prefix. That means in that position, only current word has that
                // specific character.
                while (i < n && cur.next.get(w.charAt(i)).cnt > 1) {
                    cur = cur.next.get(w.charAt(i));
                    i ++;
                }
                if (i >= n - 3) {
                    res.set(p, w);
                } else {
                    String pre = w.substring(0, i+1);
                    String st = pre + (n - i - 2) + "" + w.charAt(n - 1);
                    res.set(p, st);
                }
            }
        }
    }
    
    /**
     * Get the shortest abbreviation for a word
     */ 
    private String getShortestAbbr(String s) {
        if (s.length() <= 3) {
            return s;
        } else {
            return s.charAt(0) + "" + (s.length() - 2) + "" + s.charAt(s.length() - 1);
        }
    }
    
    /**
     * Standard way to build the trie, but we record each trie node with the information
     * of the count of words with the same prefix.
     */
    private Trie buildTrie(List<String> dict, List<Integer> pos) {
        Trie root = new Trie();
        for (int p : pos) {
            String w = dict.get(p);
            Trie cur = root;
            for (int i = 0; i < w.length(); i ++) {
                char c = w.charAt(i);
                if (cur.next.containsKey(c)) {
                    cur = cur.next.get(c);
                } else {
                    Trie next = new Trie();
                    cur.next.put(c, next);
                    cur = next;
                }
                cur.cnt ++;
            }
        }
        return root;
    }
    
    private class Trie {
        int cnt = 0;
        Map<Character, Trie> next = new HashMap<>();
    }
}
```
</p>


### Visualization
- Author: sys
- Creation Date: Sun Mar 19 2017 04:46:12 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 12:10:31 GMT+0800 (Singapore Standard Time)

<p>
https://www.youtube.com/watch?v=yAQMcGY4c90

Inspired by @ckcz123's [solution](https://discuss.leetcode.com/topic/82613/really-simple-and-straightforward-java-solution)
</p>


