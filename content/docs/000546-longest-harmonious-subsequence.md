---
title: "Longest Harmonious Subsequence"
weight: 546
#id: "longest-harmonious-subsequence"
---
## Description
<div class="description">
<p>We define a harmonious array as an array where the difference between its maximum value and its minimum value is <b>exactly</b> <code>1</code>.</p>

<p>Given an integer array <code>nums</code>, return <em>the length of its longest harmonious subsequence among all its possible subsequences</em>.</p>

<p>A <strong>subsequence</strong> of array is a sequence that can be derived from the array by deleting some or no elements without changing the order of the remaining elements.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,3,2,2,5,2,3,7]
<strong>Output:</strong> 5
<strong>Explanation:</strong> The longest harmonious subsequence is [3,2,2,2,3].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3,4]
<strong>Output:</strong> 2
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,1,1,1]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 2 * 10<sup>4</sup></code></li>
	<li><code>-10<sup>9</sup> &lt;= nums[i] &lt;= 10<sup>9</sup></code></li>
</ul>

</div>

## Tags
- Hash Table (hash-table)

## Companies
- Apple - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- LiveRamp - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Brute Force

In the brute force solution, we consider every possible subsequence that can be formed using the elements of the given array. For every subsequence, we find the maximum and minimum values in the subsequence. If the difference between the maximum and the minimum values obtained is 1, it means the current subsequence forms a harmonious subsequence. Thus, we can consider the number of elements in this subsequence to be compared with the length of the last longest harmonious subsequence. 

In order to obtain all the subseqeuences possible, we make use of binary number representation of decimal numbers. For a binary number of size $$n$$, a total of $$2^n$$ different binary numbers can be generated. We generate all these binary numbers from $$0$$ to $$2^n$$. For every binary number generated, we consider the subsequence to be comprised of only those elements of $$nums$$ which have a 1 at the corresponding position in the current binary number. The following figure shows an example of the way the elements of $$nums$$ are considered in the current subsequence.

![Harmonic_Subsequence](../Figures/594_Harmonic_Subsequence_Binary.PNG)

<iframe src="https://leetcode.com/playground/H2Qr2BKn/shared" frameBorder="0" width="100%" height="361" name="H2Qr2BKn"></iframe>

**Complexity Analysis**

* Time complexity : $$O(2^n)$$. Number of subsequences generated will be $$2^n$$.

* Space complexity : $$O(1)$$. Constant space required.
<br>
<br>

---
#### Approach 2: Better Brute Force

**Algorithm**

In the last approach, we created every possible subsequence, and for every such subsequence, we found out if it satisfies the harmonicity condition. Instead of doing this, we can do as follows. We can consider every element of the given $$nums$$ array one by one. For $$nums[i]$$ chosen to be the current element, we determine the $$count$$ of all the elements in the $$nums$$ array, which satisfy the harmonicity condition with $$nums[i]$$, i.e. the $$count$$ of all such $$nums[j]$$ satisfying $$nums[i] == nums[j]$$ or $$nums[i] == nums[j] + 1$$. When we reach the end of the array for $$nums[i]$$ being the current element, we compare this $$count$$ obtained with the result obtained from the previous traversals and update the result appropriately. When all the elements of the array have been chosen as the element to be chosen as the base for harmonicity check, we get the required length of the longest harmonic subsequence.

The following animation illustrates the process:

!?!../Documents/594_Harmonic_Subsequence_1.json:1000,563!?!

<iframe src="https://leetcode.com/playground/esihum3Z/shared" frameBorder="0" width="100%" height="395" name="esihum3Z"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. Two nested loops are there.

* Space complexity : $$O(1)$$. Constant space required.
<br>
<br>

---
#### Approach 3: Using Sorting

**Algorithm**

Since we are concerned only with the count of elements which are at a difference of 1, we can use sorting to our advantage. If we sort the given $$nums$$ array, the related elements will get arranged close to each other. Thus, we can traverse over the sorted array, and find the count of similar elements and elements one larger than the current ones, which occur consecutively(all the similar elements will be lying consecutively now). Initially, this value is stored in $$prev\_count$$ variable. Then, if we encounter an element which is just 1 larger than the last elements, we count the occurences of such elements as well. This value is stored in $$count$$ variable. 

Thus, now for the harmonic subsequence comprised of only these two elements is a subsequence of length $$count + prev\_count$$. This result is stored in $$res$$ for each subsequence found. When we move forward to considering the next set of similar consecutive elements, we need to update the $$prev\_count$$ with the $$count$$'s value, since now $$count$$ will act as the count of the elements 1 lesser than the next elements encountered. The value of $$res$$ is always updated to be the larger of previous $$res$$ and the current $$count + prev\_count$$ value.

When we are done traversing over the whole array, the value of $$res$$ gives us the required result.

<iframe src="https://leetcode.com/playground/qjbYZPWC/shared" frameBorder="0" width="100%" height="463" name="qjbYZPWC"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n\log n)$$. Sorting takes $$O(n\log n)$$ time.

* Space complexity : $$O(\log n)$$. $$\log n$$ space is required by sorting in average case.
<br>
<br>

---
#### Approach 4: Using HashMap

**Algorithm**

In this approach, we make use of a hashmap $$map$$ which stores the number of times an element occurs in the array along with the element's value in the form $$(num: count\_num)$$, where $$num$$ refers to an element in the array and $$count\_num$$ refers to the number of times this $$num$$ occurs in the $$nums$$ array. We traverse over the $$nums$$ array and fill this $$map$$ once.

After this, we traverse over the keys of the $$map$$ created. For every key of the $$map$$ considered, say $$key$$, we find out if the map contains the $$key + 1$$. Such an element is found, since only such elements can be counted for the harmonic subsequence if $$key$$ is considered as one of the element of the harmonic subsequence. We need not care about $$key - 1$$, because if $$key$$ is present in the harmonic subsequence, at one time either $$key + 1$$ or $$key - 1$$ only could be included in the harmonic subsequence. The case of $$key - 1$$ being in the harmonic subsequence will automatically be considered, when $$key - 1$$ is encountered as the current key. 

Now, whenver we find that $$key + 1$$ exists in the keys of $$map$$, we determine the count of the current harmonic subsequence as $$count_{key} + count_{key+1}$$, where $$count_i$$ refers to the value corresponding to the key $$i$$ in $$map$$, which reprents the number of times $$i$$ occurs in the array $$nums$$.

Look at the animation below for a pictorial view of the process:

!?!../Documents/594_Harmonic_Subsequence_2.json:1000,563!?!

<iframe src="https://leetcode.com/playground/eq45aYH5/shared" frameBorder="0" width="100%" height="293" name="eq45aYH5"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. One loop is required to fill $$map$$ and one for traversing the $$map$$.

* Space complexity : $$O(n)$$. In worst case map size grows upto size $$n$$.
<br>
<br>

---
#### Approach 5: In Single Loop

**Algorithm**

Instead of filling the $$map$$ first and then traversing over the $$map$$ to determine the lengths of the harmonic subsequences encountered, we can traverse over the $$nums$$ array, and while doing the traversals, we can determine the lengths of the harmonic subsequences possible till the current index of the $$nums$$ array. 

The method of finding the length of harmonic subsequence remains the same as the last approach. But, this time, we need to consider the existence of both $$key + 1$$ and $$key - 1$$ exclusively and determine the counts corresponding to both the cases. This is needed now because it could be possible that $$key$$ has already been added to the $$map$$ and later on $$key - 1$$ is encountered. In this case, if we consider the presence of $$key + 1$$ only, we'll go in the wrong direction.

Thus, we consider the $$count$$s corresponding to both the cases separately for every $$key$$ and determine the maximum out of them. 
Thus, now the same task can be done only in a single traveral of the $$nums$$ array.

See the animation below for understanding the process:

!?!../Documents/594_Harmonic_Subsequence_3.json:1000,563!?!

<iframe src="https://leetcode.com/playground/mEou6uoB/shared" frameBorder="0" width="100%" height="293" name="mEou6uoB"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Only one loop is there.

* Space complexity : $$O(n)$$. $$map$$ size grows upto size $$n$$.
<br>
<br>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java HashMap Solution
- Author: earlme
- Creation Date: Sun May 21 2017 11:02:24 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 04:04:24 GMT+0800 (Singapore Standard Time)

<p>
- The idea is to keep a count of all the numbers, and eventually for each of the numbers, check if there's any adjacent number. If it's present, then add the count of both - since these two numbers form subsequence in the array. 

**Update : from @harkness comment, we don't need to check both +1 and -1;**

    public int findLHS(int[] nums) {
        Map<Long, Integer> map = new HashMap<>();
        for (long num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        int result = 0;
        for (long key : map.keySet()) {
            if (map.containsKey(key + 1)) {
                result = Math.max(result, map.get(key + 1) + map.get(key));
            }
        }
        return result;
    }
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun May 21 2017 11:03:20 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 06:50:01 GMT+0800 (Singapore Standard Time)

<p>
Let ```count[x]``` be the number of ```x```'s in our array.
Suppose our longest subsequence ```B``` has ```min(B) = x``` and ```max(B) = x+1```.
Evidently, it should use all occurrences of ```x``` and ```x+1``` to maximize it's length, so ```len(B) = count[x] + count[x+1]```.
Additionally, it must use ```x``` and ```x+1``` atleast once, so ```count[x]``` and ```count[x+1]``` should both be positive.

```
def findLHS(self, A):
    count = collections.Counter(A)
    ans = 0
    for x in count:
        if x+1 in count:
            ans = max(ans, count[x] + count[x+1])
    return ans
```

Alternatively, we can count values in a straightforward way using a dictionary: replacing our first line of ```count = collections.Counter(A)``` with:

```
count = {}
for x in A:
    count[x] = count.get(x, 0) + 1
```
</p>


### Three C++ Solution run time with explanation
- Author: beckswu
- Creation Date: Wed Aug 23 2017 23:36:48 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 22:26:54 GMT+0800 (Singapore Standard Time)

<p>
1. run time O(n)  space O(n) use unordered_map Two pass solution
     First loop through all elements and count each number appearance. Then loop through unordered_map, to find if the key - 1 is in the unordered map(To avoid counting twice, we only find if key - 1 in the map instead of finding key + 1 and key -1). If key - 1 and key both in the map, update the result
````
    int findLHS(vector<int>& nums) {
        unordered_map<int,int>m;
        for(auto i: nums)
            m[i]++;
        int res = 0;
        for(auto it:m)
            if(m.count(it.first-1)>0)
                res = max(res, it.second+m[it.first-1]);
        return res;
    }
````

2. run time O(n)  space O(n) use unordered_map One pass solution
     Loop through all elements and count each number appearance. And find if key - 1 or key + 1 in the unordered_map(because key+1 and key-1 may appear before key in nums). If either in the unordered_map, update the result.
````
    int findLHS(vector<int>& nums) {
        unordered_map<int,int>m;
        int res = 0;
        for(auto i: nums){
            m[i]++;
            if(m.count(i+1))
                res = max(res, m[i] + m[i+1]);
            if(m.count(i-1))
                res = max(res, m[i] + m[i-1]);
        }
        return res;        
    }

````

3. O(nlogn) running time ,space O(1)  using sort
The idea is to loop through each elements and update the result. The start position is used for counting purpose and new start is used for whenever come across different number

When the number is different from previous number, update the new start position. When difference between current position and start position is bigger than 1 then update start position. 

````
       int findLHS(vector<int>& nums) {
        sort(nums.begin(),nums.end());
        int len = 0;
        for(int i = 1, start = 0, new_start = 0; i<nums.size(); i++)
        {

            if (nums[i] - nums[start] > 1)    
                start = new_start;
            if (nums[i] != nums[i-1]) 
                new_start = i;
            if(nums[i] - nums[start] == 1)
                len = max(len, i-start+1);
        }
        return len;
````
</p>


