---
title: "Flip Game"
weight: 276
#id: "flip-game"
---
## Description
<div class="description">
<p>You are playing the following Flip Game with your friend: Given a string that contains only these two characters: <code>+</code> and <code>-</code>, you and your friend take turns to flip two <b>consecutive</b> <code>&quot;++&quot;</code> into <code>&quot;--&quot;</code>. The game ends when a person can no longer make a move and therefore the other person will be the winner.</p>

<p>Write a function to compute all possible states of the string after one valid move.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> <code>s = &quot;++++&quot;</code>
<strong>Output:</strong> 
[
  &quot;--++&quot;,
  &quot;+--+&quot;,
  &quot;++--&quot;
]
</pre>

<p><strong>Note: </strong>If there is no valid move, return an empty list <code>[]</code>.</p>

</div>

## Tags
- String (string)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 4 lines in Java
- Author: StefanPochmann
- Creation Date: Fri Oct 16 2015 05:01:34 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 16 2015 05:01:34 GMT+0800 (Singapore Standard Time)

<p>
    public List<String> generatePossibleNextMoves(String s) {
        List list = new ArrayList();
        for (int i=-1; (i = s.indexOf("++", i+1)) >= 0; )
            list.add(s.substring(0, i) + "--" + s.substring(i+2));
        return list;
    }
</p>


### Simple solution in Java
- Author: OPyrohov
- Creation Date: Fri Oct 16 2015 15:29:40 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 16 2015 15:29:40 GMT+0800 (Singapore Standard Time)

<p>
We start from `i = 1` and check whether current and previous characters of the input string equals to `+`. If true, then add substring to a list: characters before previous one (concatenating with `--`) and characters after the current character.

    public List<String> generatePossibleNextMoves(String s) {
        List<String> list = new ArrayList<String>();
        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) == '+' && s.charAt(i - 1) == '+') {
                list.add(s.substring(0, i - 1) + "--" + s.substring(i + 1, s.length()));
            }
        }
        return list;
    }
</p>


### 1 line in Python
- Author: agave
- Creation Date: Sat Jun 25 2016 21:53:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 25 2016 21:53:43 GMT+0800 (Singapore Standard Time)

<p>
    class Solution(object):
        def generatePossibleNextMoves(self, s):
            return [s[:i] + "--" + s[i+2:] for i in range(len(s) - 1) if s[i] == s[i + 1] == "+"]
</p>


