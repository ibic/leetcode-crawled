---
title: "Video Stitching"
weight: 990
#id: "video-stitching"
---
## Description
<div class="description">
<p>You are given a series of video clips from a sporting event that lasted <code>T</code> seconds.&nbsp;&nbsp;These video clips can be overlapping with each other and have varied lengths.</p>

<p>Each video clip <code>clips[i]</code>&nbsp;is an interval: it starts at time <code>clips[i][0]</code> and ends at time <code>clips[i][1]</code>.&nbsp; We can cut these clips into segments freely: for example, a clip <code>[0, 7]</code> can be cut into segments&nbsp;<code>[0, 1] +&nbsp;[1, 3] + [3, 7]</code>.</p>

<p>Return the minimum number of clips needed so that we can cut the clips into segments that cover the entire sporting event (<code>[0, T]</code>).&nbsp; If the task is impossible, return <code>-1</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>clips = <span id="example-input-1-1">[[0,2],[4,6],[8,10],[1,9],[1,5],[5,9]]</span>, T = <span id="example-input-1-2">10</span>
<strong>Output: </strong><span id="example-output-1">3</span>
<strong>Explanation: </strong>
We take the clips [0,2], [8,10], [1,9]; a total of 3 clips.
Then, we can reconstruct the sporting event as follows:
We cut [1,9] into segments [1,2] + [2,8] + [8,9].
Now we have segments [0,2] + [2,8] + [8,10] which cover the sporting event [0, 10].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>clips = <span id="example-input-2-1">[[0,1],[1,2]]</span>, T = <span id="example-input-2-2">5</span>
<strong>Output: </strong><span id="example-output-2">-1</span>
<strong>Explanation: </strong>
We can&#39;t cover [0,5] with only [0,1] and [1,2].
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>clips = <span id="example-input-3-1">[[0,1],[6,8],[0,2],[5,6],[0,4],[0,3],[6,7],[1,3],[4,7],[1,4],[2,5],[2,6],[3,4],[4,5],[5,7],[6,9]]</span>, T = <span id="example-input-3-2">9</span>
<strong>Output: </strong><span id="example-output-3">3</span>
<strong>Explanation: </strong>
We can take clips [0,4], [4,7], and [6,9].
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong>clips = <span id="example-input-4-1">[[0,4],[2,8]]</span>, T = <span id="example-input-4-2">5</span>
<strong>Output: </strong><span id="example-output-4">2</span>
<strong>Explanation: </strong>
Notice you can have extra video after the event ends.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= clips.length &lt;= 100</code></li>
	<li><code>0 &lt;= clips[i][0] &lt;=&nbsp;clips[i][1] &lt;= 100</code></li>
	<li><code>0 &lt;= T &lt;= 100</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Greedy Solution, O(1) Space
- Author: lee215
- Creation Date: Sun Apr 07 2019 12:13:50 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 22 2019 20:16:55 GMT+0800 (Singapore Standard Time)

<p>
## Solution 1: Sort

Time `O(NlogN)`, Space `O(1)`
**Java**
```
    public int videoStitching(int[][] clips, int T) {
        int res = 0;
        Arrays.sort(clips, (a,b) ->  a[0] - b[0]);
        for (int i = 0, st = 0, end = 0; st < T; st = end, ++res) {
            for (; i < clips.length && clips[i][0] <= st; ++i)
                end = Math.max(end, clips[i][1]);
            if (st == end) return -1;
        }
        return res;
    }
```

**C++**
```
    int videoStitching(vector<vector<int>>& clips, int T) {
        sort(begin(clips), end(clips));
        int res = 0;
        for (auto i = 0, st = 0, end = 0; st < T; st = end, ++res) {
            for (; i < clips.size() && clips[i][0] <= st; ++i)
                end = max(end, clips[i][1]);
            if (st == end) return -1;
        }
        return res;
    }
```

**Python:**
```
    def videoStitching(self, clips, T):
        end, end2, res = -1, 0, 0
        for i, j in sorted(clips):
            if end2 >= T or i > end2:
                break
            elif end < i <= end2:
                res, end = res + 1, end2
            end2 = max(end2, j)
        return res if end2 >= T else -1
```

<br>

## Solution 2: Sort + DP
Sort clips first.
Then for each clip, update `dp[clip[0]] ~ dp[clip[1]]`.

Time `O(NlogN + NT)`, Space `O(T)`

**C++**
```
    int videoStitching(vector<vector<int>>& clips, int T) {
        sort(clips.begin(), clips.end());
        vector<int> dp(101, 101);
        dp[0] = 0;
        for (auto& c : clips)
            for (int i = c[0] + 1; i <= c[1]; i++)
                dp[i] = min(dp[i], dp[c[0]] + 1);
        return dp[T] >= 100 ? -1 : dp[T];
    }
```

<br>

## Solution 3: DP

Loop on i form `0` to `T`,
loop on all `clips`,
If `clip[0] <= i  <= clip[1]`, we update `dp[i]`

Time `O(NT)`, Space `O(T)`

**Java**
```
    public int videoStitching(int[][] clips, int T) {
        int[] dp = new int[T + 1];
        Arrays.fill(dp, T + 1);
        dp[0] = 0;
        for (int i = 1; i <= T && dp[i - 1] < T; i++) {
            for (int[] c : clips) {
                if (c[0] <= i && i <= c[1])
                    dp[i] = Math.min(dp[i], dp[c[0]] + 1);
            }
        }
        return dp[T] == T + 1 ? -1 : dp[T];
    }
```
</p>


### C++/Java 6 lines, O(n log n)
- Author: votrubac
- Creation Date: Sun Apr 07 2019 12:03:50 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 23 2019 22:42:05 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
We track our current stitching position (```st```). For each iteration, we check all overlapping clips, and pick the one that advances our stitching position the furthest.
# Solution
We sort our clips by the starting point. Since clips are sorted, we need to only analyze each clip once. For each round, we check all overlapping clips (```clips[i][0] <= st```) and advance our stitching position as far as we can (```end = max(end, clips[i][1])```).

Return ```-1``` if we cannot advance our stitching position (```st == end```).
```
int videoStitching(vector<vector<int>>& clips, int T, int res = 0) {
  sort(begin(clips), end(clips));
  for (auto i = 0, st = 0, end = 0; st < T; st = end, ++res) {
    while (i < clips.size() && clips[i][0] <= st) end = max(end, clips[i++][1]);
    if (st == end) return -1;
  }
  return res;
}
```
Java version:
```
public int videoStitching(int[][] clips, int T) {
  int res = 0;
  Arrays.sort(clips, new Comparator<int[]>() {
    public int compare(int[] a, int[] b) { return a[0] - b[0]; }
  });
  for (int i = 0, st = 0, end = 0; st < T; st = end, ++res) {
    for (; i < clips.length && clips[i][0] <= st; ++i)
      end = Math.max(end, clips[i][1]);
    if (st == end) return -1;
  }
  return res;
}
```
# Complexity Analysis
Runtime: *O(n log n)*, where n is the number of clips. We sort all clips, and then processing each clip only once.
Memory: *O(1)*.
</p>


### [C++] O(N) No Sorting (Greedy) Explained
- Author: PhoenixDD
- Creation Date: Mon Apr 08 2019 07:44:32 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 24 2020 12:24:25 GMT+0800 (Singapore Standard Time)

<p>
**Observation 1:** We will always pick a clip with the maximum coverage if they have same starting points.

eg: [0,3],[0,7],[0,5] -> We pick [0,7] , there is no point of picking others as the problem states that we need to minimize the number of clips picked, and this can only be done if we maximize the gap between start and end point of each clip.

**Observation 2:** Once we start picking the clips from the minimum starting point, we only increase the count if we find a starting point greater than previously selected clip\'s end, from then on we keep maximizing the reachable end without increasing count.

eg: [[0,4],[1,4],[2,6],[3,4],[4,7],[5,7],[6,9]], T=9

* We select [0,4], count=1 ,selectedEnd=0,reachableEnd=4
* We select [1,4], start > selectedEnd we increase count=2 and reachableEnd=4, selectedEnd=4 (Previously selected end)
* We encounter [2,6], we increase rechableEnd=6, we don\'t increase count asthe clip\'s starting point is still between [0,4] (previously selected clip)
* Similarly We encounter [4,7], we increase reachableEnd=7.
* When we reach [5,7] increase count=3 as 5 > [0,4] interval\'s end (It is here that we decide that the previously selected interval is actually [4,7] and not [1,4] where we actually increased count(step 2))
* We encounter [6,7] increase reachableEnd=7 and return count=3.

```c++
// Similar to find #55 Jump game with count of selected indices
class Solution {
public:
    int videoStitching(vector<vector<int>>& clips, int T) 
    {
        unordered_map<int,int> max_ends(clips.size());
        for(vector<int> &clip:clips)                                            //Get max end for each starting point.
            max_ends[clip[0]]=max(max_ends[clip[0]],clip[1]);
        int selectedEnd=-1,reachableEnd=0,count=0;
        for(int i=0;i<=T;i++)
        {
            if(reachableEnd>=T||i>reachableEnd)        //If there is a break between clips or we already reached the end, return
                break;
            if(max_ends.count(i))                                 //If clip with i as starting point exists.
            {
                if(i>selectedEnd)                   //Increase the count if starting point is greater than previously selected clip end
                    count++,selectedEnd=reachableEnd;              //New selected clip ends at max end
                reachableEnd=max(reachableEnd,max_ends[i]);//Maximize reachable end till starting point is less than or equal to previously selected clip
            }
        }
        return reachableEnd>=T?count:-1;
    }
};
```

Since, in the worst case clips.length<=100 and T<=100 O(T)=O(n)
</p>


