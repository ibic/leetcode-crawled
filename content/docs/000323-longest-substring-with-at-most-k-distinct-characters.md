---
title: "Longest Substring with At Most K Distinct Characters"
weight: 323
#id: "longest-substring-with-at-most-k-distinct-characters"
---
## Description
<div class="description">
<p>Given a string, find the length of the longest substring T that contains at most <i>k</i> distinct characters.</p>

<p><strong>Example 1:</strong></p>

<div>
<pre>
<strong>Input: </strong>s = <span id="example-input-1-1">&quot;eceba&quot;</span>, k = <span id="example-input-1-2">2</span>
<strong>Output: </strong><span id="example-output-1">3</span>
<strong>Explanation: </strong>T is &quot;ece&quot; which its length is 3.</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>s = <span id="example-input-2-1">&quot;aa&quot;</span>, k = <span id="example-input-2-2">1</span>
<strong>Output: </strong>2
<strong>Explanation: </strong>T is &quot;aa&quot; which its length is 2.
</pre>
</div>
</div>
</div>

## Tags
- Hash Table (hash-table)
- String (string)
- Sliding Window (sliding-window)

## Companies
- Facebook - 18 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: true)
- Snapchat - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- AppDynamics - 0 (taggedByAdmin: true)
- Coupang - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Sliding Window + Hashmap.
**Intuition**

Let's use here the logic from the more simple 
[problem with at most two distinct
characters](https://leetcode.com/articles/longest-substring-with-at-most-two-distinct-charac/).


To solve the problem in one pass
let's use here _sliding window_ approach with two set pointers
`left` and `right` serving as the window boundaries.

The idea is to set both pointers in the position `0` and
then move `right` pointer to the right while the
window contains not more than `k` distinct characters. 
If at some point we've got `k + 1` distinct characters,
let's move `left` pointer to keep not more than `k + 1`
distinct characters in the window.

![compute](../Figures/340/substring.png)

Basically that's the algorithm : to move sliding window along the string,
to keep not more than `k` distinct characters in the window, and
to update max substring length at each step.

> There is just one more question to reply - 
how to move the left pointer
to keep only `k` distinct characters in the string?

Let's use for this purpose hashmap containing all characters 
in the sliding window as keys and their rightmost positions 
as values. At each moment, this hashmap could contain 
not more than `k + 1` elements.

![compute](../Figures/340/hash.png)

For example, using this hashmap one knows that the rightmost position
of character `O` in `"LOVELEE"` window is `1` and so one has
to move `left` pointer in the position `1 + 1 = 2` to
exclude the character `O` from the sliding window.  

**Algorithm**

Now one could write down the algortihm.

- Return `0` if the string is empty or `k` is equal to zero. 
- Set both set pointers in the beginning 
 of the string `left = 0` and `right = 0` and init max substring
 length `max_len = 1`.
- While `right` pointer is less than `N`:
    * Add the current character `s[right]` in the hashmap and
    move `right` pointer to the right.
    * If hashmap contains `k + 1` distinct characters,
    remove the leftmost character from the hashmap
    and move the `left` pointer so that sliding window contains
    again `k` distinct characters only.
    * Update `max_len`.

**Implementation**

!?!../Documents/340_LIS.json:1000,440!?!

<iframe src="https://leetcode.com/playground/vUFLpCJ2/shared" frameBorder="0" width="100%" height="500" name="vUFLpCJ2"></iframe>

**Complexity Analysis**

> Do we have here the best possible time complexity
$$\mathcal{O}(N)$$
as it was for more simple [problem with at most two distinct
characters](https://leetcode.com/articles/longest-substring-with-at-most-two-distinct-charac/)?

For the best case when input string contains not more than 
`k` distinct characters the answer is _yes_. 
It's the only one pass along the string with 
`N` characters and the time complexity is $$\mathcal{O}(N)$$.

For the worst case when the input string contains 
`n` distinct characters, the answer is _no_. In that case at each 
step one uses $$\mathcal{O}(k)$$ time to find a minimum value
in the hashmap with `k` elements and so the overall time 
complexity is $$\mathcal{O}(N k)$$.

* Time complexity : $$\mathcal{O}(N)$$ in the best case 
of `k` distinct characters in the string and
$$\mathcal{O}(N k)$$ in the worst case 
of `N` distinct characters in the string.
 
* Space complexity : $$\mathcal{O}(k)$$ since additional 
space is used only for a hashmap with at most `k + 1` elements.
<br />
<br />


---
#### Approach 2: Sliding Window + Ordered Dictionary.

**How to achieve $$\mathcal{O}(N)$$ time complexity**

Approach 1 with a standard hashmap couldn't 
ensure $$\mathcal{O}(N)$$ time complexity. 

To have $$\mathcal{O}(N)$$ algorithm performance, 
one would need a structure, which 
provides four operations in $$\mathcal{O}(1)$$ time :

- Insert the key 

- Get the key / Check if the key exists 

- Delete the key

- Return the first / or the last added key/value

The first three operations in $$\mathcal{O}(1)$$ time are provided 
by the standard hashmap, and the forth one - by linked list.

> There is a structure called _ordered dictionary_, it combines 
behind both hashmap and linked list. In Python this structure is called
[_OrderedDict_](https://docs.python.org/3/library/collections.html#collections.OrderedDict)
and in Java [_LinkedHashMap_](https://docs.oracle.com/javase/8/docs/api/java/util/LinkedHashMap.html).

Ordered dictionary is quite popular for the interviews, for
example, check [to implement LRU cache](https://leetcode.com/problems/lru-cache/)
question by Google. 

**Algorithm**

Let's use ordered dictionary instead of standard hashmap to trim the 
algorithm from the approach 1 :

- Return `0` if the string is empty or `k` is equal to zero. 
- Set both set pointers in the beginning 
 of the string `left = 0` and `right = 0` and init max substring
 length `max_len = 1`.
- While `right` pointer is less than `N`:
    * If the current character `s[right]` is already in the ordered dictionary
    `hashmap` -- delete it, to ensure that the first key in `hashmap` is 
    the leftmost character.
    * Add the current character `s[right]` in the ordered dictionary and
    move `right` pointer to the right.
    * If ordered dictionary `hashmap` contains `k + 1` distinct characters,
    remove the leftmost one
    and move the `left` pointer so that sliding window contains
    again `k` distinct characters only.
    * Update `max_len`.

**Implementation**

<iframe src="https://leetcode.com/playground/28MJYk7b/shared" frameBorder="0" width="100%" height="500" name="28MJYk7b"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$ since all operations with
ordered dictionary : `insert/get/delete/popitem` 
(`put/containsKey/remove`) are done in a constant time.
 
* Space complexity : $$\mathcal{O}(k)$$ since additional 
space is used only for an ordered dictionary with at most `k + 1` elements.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 15 lines java solution using slide window
- Author: jiangbowei2010
- Creation Date: Sun Apr 03 2016 12:02:56 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 16:30:46 GMT+0800 (Singapore Standard Time)

<p>
feel it is not a new question, just use num to track the number of distinct characters within the slide window

    public class Solution {
        public int lengthOfLongestSubstringKDistinct(String s, int k) {
            int[] count = new int[256];
            int num = 0, i = 0, res = 0;
            for (int j = 0; j < s.length(); j++) {
                if (count[s.charAt(j)]++ == 0) num++;
                if (num > k) {
                    while (--count[s.charAt(i++)] > 0);
                    num--;
                }
                res = Math.max(res, j - i + 1);
            }
            return res;
        }
    }
</p>


### Java O(nlogk) using TreeMap to keep last occurrence Interview "follow-up" question!
- Author: xuyirui
- Creation Date: Wed Jun 22 2016 04:46:40 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 17 2018 11:46:40 GMT+0800 (Singapore Standard Time)

<p>
Solving the problem with O(n) time is not enough, some interviewer may require this solution as a followup. Instead of recording each char's count, we keep track of char's last occurrence.  If you consider k as constant, it is also a O(n) algorithm.

inWindow keeps track of each char in window and its last occurrence position
 
lastOccurrence is used to find the char in window with left most last occurrence. A better idea is to use a PriorityQueue, as it takes O(1) to getMin,  However Java's PQ does not support O(logn) update a internal node, it takes O(n).  TreeMap takes O(logn) to do both getMin and update.
Every time when the window is full of k distinct chars, we lookup TreeMap to find the one with leftmost last occurrence and set left bound j to be 1 + first to exclude the char to allow new char coming into window.

       public class Solution {
            public int lengthOfLongestSubstringKDistinct(String str, int k) {
                if (str == null || str.isEmpty() || k == 0) {
                    return 0;
                }
                TreeMap<Integer, Character> lastOccurrence = new TreeMap<>();
                Map<Character, Integer> inWindow = new HashMap<>();
                int j = 0;
                int max = 1;
                for (int i = 0; i < str.length(); i++) {
                    char in = str.charAt(i);
                    while (inWindow.size() == k && !inWindow.containsKey(in)) {
                        int first = lastOccurrence.firstKey();
                        char out = lastOccurrence.get(first);
                        inWindow.remove(out);
                        lastOccurrence.remove(first);
                        j = first + 1;
                    }
                    //update or add in's position in both maps
                    if (inWindow.containsKey(in)) {
                        lastOccurrence.remove(inWindow.get(in));
                    }
                    inWindow.put(in, i);
                    lastOccurrence.put(i, in);
                    max = Math.max(max, i - j + 1);
                }
                return max;
            }
        }
</p>


### 10-line Python Solution using dictionary with easy to understand explanation
- Author: myliu
- Creation Date: Tue May 03 2016 23:45:02 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 14 2018 04:22:06 GMT+0800 (Singapore Standard Time)

<p>
    class Solution(object):
        
        """
        The general idea is to iterate over string s.
        Always put the character c and its location i in the dictionary d.
        1) If the sliding window contains less than or equal to k distinct characters, simply record the return value, and move on.
        2) Otherwise, we need to remove a character from the sliding window.
           Here's how to find the character to be removed:
           Because the values in d represents the rightmost location of each character in the sliding window, in order to find the longest substring T, we need to locate the smallest location, and remove it from the dictionary, and then record the return value.
        """
        def lengthOfLongestSubstringKDistinct(self, s, k):
            """
            :type s: str
            :type k: int
            :rtype: int
            """
            # Use dictionary d to keep track of (character, location) pair,
            # where location is the rightmost location that the character appears at
            d = {}
            low, ret = 0, 0
            for i, c in enumerate(s):
                d[c] = i
                if len(d) > k:
                    low = min(d.values())
                    del d[s[low]]
                    low += 1
                ret = max(i - low + 1, ret)
            return ret
</p>


