---
title: "Count Negative Numbers in a Sorted Matrix"
weight: 1266
#id: "count-negative-numbers-in-a-sorted-matrix"
---
## Description
<div class="description">
<p>Given a <code>m&nbsp;* n</code>&nbsp;matrix <code>grid</code>&nbsp;which is sorted in non-increasing order both row-wise and column-wise.&nbsp;</p>

<p>Return the number of <strong>negative</strong> numbers in&nbsp;<code>grid</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> grid = [[4,3,2,-1],[3,2,1,-1],[1,1,-1,-2],[-1,-1,-2,-3]]
<strong>Output:</strong> 8
<strong>Explanation:</strong> There are 8 negatives number in the matrix.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> grid = [[3,2],[1,0]]
<strong>Output:</strong> 0
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> grid = [[1,-1],[-1,-1]]
<strong>Output:</strong> 3
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> grid = [[-1]]
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m == grid.length</code></li>
	<li><code>n == grid[i].length</code></li>
	<li><code>1 &lt;= m, n &lt;= 100</code></li>
	<li><code>-100 &lt;= grid[i][j] &lt;= 100</code></li>
</ul>
</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Amazon - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] 2 similar O(m + n) codes w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Feb 16 2020 12:03:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 19 2020 10:13:04 GMT+0800 (Singapore Standard Time)

<p>
Please refer to the perspicacious elaboration from **@ikeabord** as follows:
This solution uses the fact that the negative regions of the matrix will form a "staircase" shape, e.g.:
```
++++++
++++--
++++--
+++---
+-----
+-----
```
What this solution then does is to "trace" the outline of the staircase.

---
Start from bottom-left corner of the matrix, count in the negative numbers in each row. 


```java
    public int countNegatives(int[][] grid) {
        int m = grid.length, n = grid[0].length, r = m - 1, c = 0, cnt = 0;
        while (r >= 0 && c < n) {
            if (grid[r][c] < 0) {
                --r;
                cnt += n - c; // there are n - c negative numbers in current row.
            }else {
                ++c;
            }
        }
        return cnt;
    }
```
```python
    def countNegatives(self, grid: List[List[int]]) -> int:
        m, n = len(grid), len(grid[0])
        r, c, cnt = m - 1, 0, 0
        while r >= 0 and c < n:
            if grid[r][c] < 0:
                cnt += n - c
                r -= 1
            else:
                c += 1
        return cnt
```
Simlarly, you can also start from top-right corner, whichever you feel comfortable with, count in the negative numers in each column.
```java
    public int countNegatives(int[][] grid) {
        int m = grid.length, n = grid[0].length, r = 0, c = n - 1, cnt = 0;
        while (r < m && c >= 0) {
            if (grid[r][c] < 0) {
                --c;
                cnt += m - r; // there are m - r negative numbers in current column.
            }else {
                ++r;
            }
        }
        return cnt;
    }
```
```python
    def countNegatives(self, grid: List[List[int]]) -> int:
        m, n = len(grid), len(grid[0])
        r, c, cnt = 0, n - 1, 0
        while r < m and c >= 0:
            if grid[r][c] < 0:
                cnt += m - r
                c -= 1
            else:
                r += 1
        return cnt
```

---

**Analysis:**

At most move `m + n` steps.

Time: `O(m + n)`, space: O(1).

----

 For more practice of similar problem, please refer to:
 [ 240. Search a 2D Matrix II ](https://leetcode.com/problems/search-a-2d-matrix-ii/), -- credit to **@cerb668**.
 [1428. Leftmost Column with at Least a One](https://leetcode.com/problems/leftmost-column-with-at-least-a-one/discuss/590240/Java-O(m-+-n)-Follow-the-top-left-part-of-the-contour-of-the-1\'s.)
</p>


### Java, binary search, beats 100%, explained
- Author: gthor10
- Creation Date: Tue Feb 18 2020 03:45:24 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Feb 18 2020 03:51:26 GMT+0800 (Singapore Standard Time)

<p>
This problem can be solved with a binary search. 

For every row do the binary search to find exact position of the fist negative element, after that all elements are negative.  Optimization here - for every next row the right limit for the binary search can be the index of the first negative from the previous row. This is due to fact that cols are also sorted so for the same column for every negative element the element below can be only negative. Thus we can explude it from the binary search on the next step.

Also taking care of the edge cases helps - if first element is < 0 then all elements in a row are negative, if last one is non-negative then all elements are non-negative.

O(rows x lg(cols)) time - need to do lg(cols) binary search for each of rows row, O(1) space - no extrace space other than few variables.

```
    public int countNegatives(int[][] grid) {
        int rows = grid.length, cols = grid[0].length; 
        int res = 0, lastNeg = cols - 1;
        for (int row = 0; row < rows; row++) {
            //check edge cases - if first element is < 0 - all elements in row are negative
            if (grid[row][0] < 0) {
                res+=cols;
                continue;
            }
            //if last element is positive - it means there are no negative numbers in a row
            if (grid[row][cols - 1] > 0)
                continue;
            //there is a mix of negative and positive ones, need to find the border. starting
            //binary search
            int l = 0, r = lastNeg;
            while (l <= r) {
                int m = l + (r - l)/2;
                if (grid[row][m] < 0) {
                    r = m - 1;
                } else
                    l = m + 1;
            }
            //l points to the first negative element, which means cols - l is a number of
            //such elements
            res += (cols - l); lastNeg = l;
        }
        return res;
    }
```
</p>


### [C++] Three Methods
- Author: orangezeit
- Creation Date: Sun Feb 16 2020 12:01:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 17 2020 00:36:33 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    int countNegatives(vector<vector<int>>& grid) {
        int ans(0);
        
        // Brute Force: O(mn) - check if negative
        for (const vector<int>& row: grid)
            for (const int& i: row)
                if (i < 0) ans++;
        return ans;
        
        // Binary Search: O(m lg(n)) or equivalently O(n lg(m)) - look for break point of each row / column
        for (const vector<int>& row: grid)
            ans += upper_bound(row.rbegin(), row.rend(), -1) - row.rbegin();
        return ans;
        
        // Search Break Points: O(m + n) - traverse from upper right to lower left
        int m(grid.size()), n(grid[0].size()), r(0), c(n - 1);
        while (r < m) {
            while (c >= 0 && grid[r][c] < 0) c--;
            ans += n - c - 1;
            r++;
        }
        return ans;
    }
};
```
</p>


