---
title: "Average Salary: Departments VS Company"
weight: 1506
#id: "average-salary-departments-vs-company"
---
## Description
<div class="description">
Given two tables as below, write a query to display the comparison result (higher/lower/same) of the average salary of employees in a department to the company&#39;s average salary.
<p>&nbsp;</p>
Table: <code>salary</code>

<pre>
| id | employee_id | amount | pay_date   |
|----|-------------|--------|------------|
| 1  | 1           | 9000   | 2017-03-31 |
| 2  | 2           | 6000   | 2017-03-31 |
| 3  | 3           | 10000  | 2017-03-31 |
| 4  | 1           | 7000   | 2017-02-28 |
| 5  | 2           | 6000   | 2017-02-28 |
| 6  | 3           | 8000   | 2017-02-28 |
</pre>

<p>&nbsp;</p>
The <b>employee_id</b> column refers to the <b>employee_id</b> in the following table <code>employee</code>.

<p>&nbsp;</p>

<pre>
| employee_id | department_id |
|-------------|---------------|
| 1           | 1             |
| 2           | 2             |
| 3           | 2             |
</pre>

<p>&nbsp;</p>
So for the sample data above, the result is:

<p>&nbsp;</p>

<pre>
| pay_month | department_id | comparison  |
|-----------|---------------|-------------|
| 2017-03   | 1             | higher      |
| 2017-03   | 2             | lower       |
| 2017-02   | 1             | same        |
| 2017-02   | 2             | same        |
</pre>

<p>&nbsp;</p>
<b>Explanation</b>

<p>&nbsp;</p>
In March, the company&#39;s average salary is (9000+6000+10000)/3 = 8333.33...

<p>&nbsp;</p>
The average salary for department &#39;1&#39; is 9000, which is the salary of <b>employee_id</b> &#39;1&#39; since there is only one employee in this department. So the comparison result is &#39;higher&#39; since 9000 &gt; 8333.33 obviously.

<p>&nbsp;</p>
The average salary of department &#39;2&#39; is (6000 + 10000)/2 = 8000, which is the average of <b>employee_id</b> &#39;2&#39; and &#39;3&#39;. So the comparison result is &#39;lower&#39; since 8000 &lt; 8333.33.

<p>&nbsp;</p>
With he same formula for the average salary comparison in February, the result is &#39;same&#39; since both the department &#39;1&#39; and &#39;2&#39; have the same average salary with the company, which is 7000.

<p>&nbsp;</p>

</div>

## Tags


## Companies
- Amazon - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using `avg()` and `case...when...` [Accepted]

**Intuition**

Solve this problem by 3 steps as below.

**Algorithm**

1.Calculate the company's average salary in every month
MySQL has the built-in function avg() to get the average of a list of numbers. Also, we need to format the *pay_date* column for future use.

```sql
select avg(amount) as company_avg,  date_format(pay_date, '%Y-%m') as pay_month
from salary
group by date_format(pay_date, '%Y-%m')
;
```

| company_avg | pay_month |
|-------------|-----------|
| 7000.0000   | 2017-02   |
| 8333.3333   | 2017-03   |

2.Calculate the each department's average salary in every month
To do this, we need to join the **salary** table with the **employee** table using condition `salary.employee_id = employee.id`.

```sql
select department_id, avg(amount) as department_avg, date_format(pay_date, '%Y-%m') as pay_month
from salary
join employee on salary.employee_id = employee.employee_id
group by department_id, pay_month
;
```

| department_id | department_avg | pay_month |
|---------------|----------------|-----------|
| 1             | 7000.0000      | 2017-02   |
| 1             | 9000.0000      | 2017-03   |
| 2             | 7000.0000      | 2017-02   |
| 2             | 8000.0000      | 2017-03   |

3.Compare the previous numbers and display the result
This step might be the hardest if you have no idea on how to use MySQL flow control statement [`case...when...`](https://dev.mysql.com/doc/refman/5.7/en/case.html).

MySQL, like other programming languages, also has its flow control. Click [this link](https://dev.mysql.com/doc/refman/5.7/en/flow-control-statements.html) to learn it.

At last, combine the above two query and join them `on department_salary.pay_month = company_salary.pay_month`.

**MySQL**

```sql
select department_salary.pay_month, department_id,
case
  when department_avg>company_avg then 'higher'
  when department_avg<company_avg then 'lower'
  else 'same'
end as comparison
from
(
  select department_id, avg(amount) as department_avg, date_format(pay_date, '%Y-%m') as pay_month
  from salary join employee on salary.employee_id = employee.employee_id
  group by department_id, pay_month
) as department_salary
join
(
  select avg(amount) as company_avg,  date_format(pay_date, '%Y-%m') as pay_month from salary group by date_format(pay_date, '%Y-%m')
) as company_salary
on department_salary.pay_month = company_salary.pay_month
;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MY SQL SERVER SOLUTION, USING WINDOW FUNCTIONS, ACCEPTED
- Author: starry0731
- Creation Date: Wed May 22 2019 21:50:29 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 22 2019 21:50:29 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT DISTINCT pay_month, department_id, 
(CASE WHEN department_avg_salary > company_avg_salary THEN \'higher\'
     WHEN department_avg_salary < company_avg_salary THEN \'lower\'
     WHEN department_avg_salary = company_avg_salary THEN \'same\' END) AS comparison
FROM (
SELECT A.employee_id, amount, pay_date,department_id, LEFT(pay_date,7) as pay_month, AVG(amount) OVER(PARTITION BY A.pay_date) AS company_avg_salary,
AVG(amount) OVER(PARTITION BY A.pay_date, B.department_id) AS department_avg_salary
FROM salary AS A
JOIN employee AS B
ON A.employee_id = B.employee_id) AS temp;
```
</p>


### My solution using LEFT JOIN
- Author: tucaoneo
- Creation Date: Tue Jun 06 2017 11:28:51 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 08:07:26 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT d1.pay_month, d1.department_id, 
CASE WHEN d1.department_avg > c1.company_avg THEN 'higher'
     WHEN d1.department_avg < c1.company_avg THEN 'lower'
     ELSE 'same'
END AS 'comparison'
FROM ((SELECT LEFT(s1.pay_date, 7) pay_month, e1.department_id, AVG(s1.amount) department_avg
FROM salary s1
JOIN employee e1 ON s1.employee_id = e1.employee_id
GROUP BY pay_month, e1.department_id) d1
LEFT JOIN (SELECT LEFT(pay_date, 7) pay_month, AVG(amount) company_avg
FROM salary
GROUP BY pay_month) c1 ON d1.pay_month = c1.pay_month)
ORDER BY pay_month DESC, department_id;
```
</p>


### easy sql server
- Author: yjddxtl
- Creation Date: Mon Oct 14 2019 08:48:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 14 2019 08:48:03 GMT+0800 (Singapore Standard Time)

<p>
```
select t.pay_month, t.department_id, case when avg1=avg2 then \'same\' when avg1>avg2 then \'higher\' else \'lower\' end comparison
from
(select distinct e.department_id, left(pay_date,7) pay_month,
AVG(amount) over(partition by left(pay_date,7),e.department_id) avg1,
AVG(amount) over(partition by left(pay_date,7)) avg2
from salary s
inner join employee e 
on s.employee_id=e.employee_id) t
```
</p>


