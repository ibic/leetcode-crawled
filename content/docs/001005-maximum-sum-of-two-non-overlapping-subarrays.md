---
title: "Maximum Sum of Two Non-Overlapping Subarrays"
weight: 1005
#id: "maximum-sum-of-two-non-overlapping-subarrays"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of non-negative integers, return the maximum sum of elements in two non-overlapping (contiguous) subarrays, which have lengths&nbsp;<code>L</code> and <code>M</code>.&nbsp; (For clarification, the <code>L</code>-length subarray could occur before or after the <code>M</code>-length subarray.)</p>

<p>Formally,&nbsp;return the largest <code>V</code> for which&nbsp;<code>V = (A[i] + A[i+1] + ... + A[i+L-1]) + (A[j] + A[j+1] + ... + A[j+M-1])</code> and either:</p>

<ul>
	<li><code>0 &lt;= i &lt; i + L - 1 &lt; j &lt; j + M - 1 &lt; A.length</code>, <strong>or</strong></li>
	<li><code>0 &lt;= j &lt; j + M - 1 &lt; i &lt; i + L - 1 &lt; A.length</code>.</li>
</ul>

<p>&nbsp;</p>

<ol>
</ol>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[0,6,5,2,2,5,1,9,4]</span>, L = <span id="example-input-1-2">1</span>, M = <span id="example-input-1-3">2</span>
<strong>Output: </strong><span id="example-output-1">20
<strong>Explanation:</strong> One choice of subarrays is [9] with length 1, and [6,5] with length 2.</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">[3,8,1,3,2,1,8,9,0]</span>, L = <span id="example-input-2-2">3</span>, M = <span id="example-input-2-3">2</span>
<strong>Output: </strong><span id="example-output-2">29
</span><span id="example-output-1"><strong>Explanation:</strong> One choice of subarrays is</span><span> [3,8,1] with length 3, and [8,9] with length 2.</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-3-1">[2,1,5,6,0,9,5,0,3,8]</span>, L = <span id="example-input-3-2">4</span>, M = <span id="example-input-3-3">3</span>
<strong>Output: </strong><span id="example-output-3">31
</span><span id="example-output-1"><strong>Explanation:</strong> One choice of subarrays is</span><span> [5,6,0,9] with length 4, and [3,8] with length 3.</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>L &gt;= 1</code></li>
	<li><code>M &gt;= 1</code></li>
	<li><code>L + M &lt;= A.length &lt;= 1000</code></li>
	<li><code>0 &lt;= A[i] &lt;= 1000</code></li>
</ol>
</div>
</div>
</div>

</div>

## Tags
- Array (array)

## Companies
- Google - 5 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Grab - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] O(N)Time O(1) Space
- Author: lee215
- Creation Date: Sat Apr 20 2019 17:06:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Apr 20 2019 17:06:20 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**

`Lsum`, sum of the last `L` elements
`Msum`, sum of the last `M` elements

`Lmax`, max sum of contiguous `L` elements before the last `M` elements.
`Mmax`, max sum of contiguous `M` elements before the last `L` elements/


## **Complexity**
Two pass, `O(N)` time,
`O(1)` extra space.

It can be done in one pass. I just don\'t feel like merging them.
If you don\'t like change the original input, don\'t have to.

<br>

**Java:**
```
    public int maxSumTwoNoOverlap(int[] A, int L, int M) {
        for (int i = 1; i < A.length; ++i)
            A[i] += A[i - 1];
        int res = A[L + M - 1], Lmax = A[L - 1], Mmax = A[M - 1];
        for (int i = L + M; i < A.length; ++i) {
            Lmax = Math.max(Lmax, A[i - M] - A[i - L - M]);
            Mmax = Math.max(Mmax, A[i - L] - A[i - L - M]);
            res = Math.max(res, Math.max(Lmax + A[i] - A[i - M], Mmax + A[i] - A[i - L]));
        }
        return res;
    }
```

**Another Java**
```
    public int maxSumTwoNoOverlap(int[] A, int L, int M) {
        int res = 0, Lsum = 0, Lmax = 0, Msum = 0, Mmax = 0;
        for (int i = 0; i < A.length; ++i) {
            Msum += A[i];
            if (i - M >= 0) Msum -= A[i - M];
            if (i - M >= 0) Lsum += A[i - M];
            if (i - M - L >= 0) Lsum -= A[i - L - M];
            Lmax = Math.max(Lmax, Lsum);
            res = Math.max(res, Lmax + Msum);
        }
        Lsum = Lmax = Msum = Mmax = 0;
        for (int i = 0; i < A.length; ++i) {
            Lsum += A[i];
            if (i - L >= 0) Lsum -= A[i - L];
            if (i - L >= 0) Msum += A[i - L];
            if (i - M - L >= 0) Msum -= A[i - L - M];
            Mmax = Math.max(Mmax, Msum);
            res = Math.max(res, Mmax + Lsum);
        }
        return res;
    }
}
```

**C++:**
```
    int maxSumTwoNoOverlap(vector<int>& A, int L, int M) {
        for (int i = 1; i < A.size(); ++i)
            A[i] += A[i - 1];
        int res = A[L + M - 1], Lmax = A[L - 1], Mmax = A[M - 1];
        for (int i = L + M; i < A.size(); ++i) {
            Lmax = max(Lmax, A[i - M] - A[i - L - M]);
            Mmax = max(Mmax, A[i - L] - A[i - L - M]);
            res = max(res, max(Lmax + A[i] - A[i - M], Mmax + A[i] - A[i - L]));
        }
        return res;
    }
```

**Python:**
```
    def maxSumTwoNoOverlap(self, A, L, M):
        for i in xrange(1, len(A)):
            A[i] += A[i - 1]
        res, Lmax, Mmax = A[L + M - 1], A[L - 1], A[M - 1]
        for i in xrange(L + M, len(A)):
            Lmax = max(Lmax, A[i - M] - A[i - L - M])
            Mmax = max(Mmax, A[i - L] - A[i - L - M])
            res = max(res, Lmax + A[i] - A[i - M], Mmax + A[i] - A[i - L])
        return res
```

</p>


### C++ O(N) buy/sell stock 2 times
- Author: votrubac
- Creation Date: Sun Apr 21 2019 12:03:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 21 2019 12:03:01 GMT+0800 (Singapore Standard Time)

<p>
Similar to [Best Time to Buy and Sell Stock III](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/), but instead of maximum profit, we track maximum sum of ```N``` elements.

Left-to-right, track the maximum sum of ```L``` elements in ```left```. Right-to-left, track the maximum sum of ```M``` elements in ```right```.

Then, find the split point where ```left[i] + right[i]``` gives us the maximum sum.

**Note:** we need to do it twice for (L, M) and (M, L).
```
int maxTwoNoOverlap(vector<int>& A, int L, int M, int sz, int res = 0) {
  vector<int> left(sz + 1), right(sz + 1);
  for (int i = 0, j = sz - 1, s_r = 0, s_l = 0; i < sz; ++i, --j) {
    s_l += A[i], s_r += A[j];
    left[i + 1] = max(left[i], s_l);
    right[j] = max(right[j + 1], s_r);
    if (i + 1 >= L) s_l -= A[i + 1 - L];
    if (i + 1 >= M) s_r -= A[j + M - 1];
  }
  for (auto i = 0; i < A.size(); ++i) {
    res = max(res, left[i] + right[i]);
  }
  return res;
}
int maxSumTwoNoOverlap(vector<int>& A, int L, int M) {
  return max(maxTwoNoOverlap(A, L, M, A.size()), maxTwoNoOverlap(A, M, L, A.size()));
}
```
</p>


### [Java/Python 3] two  easy DP codes w/ comment - time O(n), NO change of input
- Author: rock
- Creation Date: Mon Apr 22 2019 01:25:54 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 29 2020 20:11:14 GMT+0800 (Singapore Standard Time)

<p>
**Q & A**

Q1: How do you guarantee the two subarrays are not overlapping in your code?
A1: During traversal, the upper bound of the `L-length` left subarray is `i - M` inclusively, and the lower bound of the `M-length` right subarray is also `i - M` exclusively. Therefore, the two subarrays will never overlap.

**End of Q & A**

----

1. Scan the prefix sum array from index `L + M`, which is the first possible position;
2. update the max value of the `L-length` subarray; then update max value of the sum of the both;
3. we need to swap `L` and `M` to scan twice, since either subarray can occur before the other.
4. In private method, prefix sum difference `p[i - M] - p[i - M - L]` is `L-length` subarray from index `i - M - L` to `i - M - 1`, and `p[i] - p[i - M]` is `M-length` subarray from index `i - M` to `i - 1`. 

**Solution 1: Prefix Sum**

```java
    public int maxSumTwoNoOverlap(int[] A, int L, int M) {
        int[] prefixSum = new int[A.length + 1];
        for (int i = 0; i < A.length; ++i) {
            prefixSum[i + 1] = prefixSum[i] + A[i];
        }
        return Math.max(maxSum(prefixSum, L, M), maxSum(prefixSum, M, L));
    }
    private int maxSum(int[] p, int L, int M) {
        int ans = 0;
        for (int i = L + M, maxL = 0; i < p.length; ++i) {
            maxL = Math.max(maxL, p[i - M] - p[i - M - L]); // update max of L-length subarray.
            ans = Math.max(ans, maxL + p[i] - p[i - M]); // update max of the sum of L-length & M-length subarrays.
        }
        return ans;
    }
```
```python
    def maxSumTwoNoOverlap(self, A: List[int], L: int, M: int) -> int:
        
        def maxSum(L:int, M:int) -> int:
            maxL = ans = 0
            for i in range(L + M, len(prefixSum)):
                maxL = max(maxL, prefixSum[i - M] - prefixSum[i - L - M])
                ans = max(ans, maxL + prefixSum[i] - prefixSum[i - M])
            return ans
        
        prefixSum = [0] * (len(A) + 1)
        for i, a in enumerate(A):
            prefixSum[i + 1] = prefixSum[i] + a
        return max(maxSum(L, M), maxSum(M, L))
```

**Analysis:**

Time & space: `O(n)`, where `n = A.length`.

----

**Solution 2: Sliding Window**

Based on **Solution 1**, we can further get rid of prefix sum array to implement the following space `O(1)` code. 

**Unfortunately, the boundary conditions are headache and fallible, please let me know if you can improve the readability, or at least make it concise.**

```java
    public int maxSumTwoNoOverlap(int[] A, int L, int M) {
        return Math.max(maxSum(A, L, M), maxSum(A, M, L));
    }
    private int maxSum(int[] A, int L, int M) {
        int ans = 0; 
        for (int i = 0, maxL = 0, sumL = 0, sumM = 0; i < A.length; ++i) {
            if (i < L || i >= L + M) { sumL += i < L ? A[i] : A[i - M]; } // first L-length subarray at index [0...L - 1], no update between index [L...L + M - 1].
            if (i >= L) { sumM += A[i]; } // first M-length subarray starts from index L to L + M - 1.
            if (i >= L + M) { sumL -= A[i - L - M]; } // deduct first item from current L-length subarray.
            if (i >= L + M) { sumM -= A[i - M]; } // deduct first item from current  M-length subarray.
            if (i >= L + M - 1) { maxL = Math.max(maxL, sumL); } // update max of L-length subarray.
            if (i >= L + M - 1) { ans = Math.max(ans, maxL + sumM); } // update max of L-length & M-length subarrays.
        }
        return ans;
    }
```
**Update with a more readable version:**
```java
    public int maxSumTwoNoOverlap(int[] A, int L, int M) {
        return Math.max(maxSum(A, L, M), maxSum(A, M, L));
    }
    private int maxSum(int[] A, int L, int M) {
        int sumL = 0, sumM = 0;
        for (int i = 0; i < L + M; ++i) { // compute the initial values of L & M length subarrays.
            if (i < L) sumL += A[i];
            else sumM += A[i];
        }
        int ans = sumM + sumL; // sum of sumL and sumM.
        for (int i = L + M, maxL = sumL; i < A.length; ++i) {
            sumM += A[i] - A[i - M]; // update sumM.
            sumL += A[i - M] - A[i - L - M]; // update sumL.
            maxL = Math.max(maxL, sumL); // update max value of L-length subarray.
            ans = Math.max(ans, maxL + sumM); // update max value of sum of L & M-length subarrays.
        }
        return ans;
    }
```
```python
    def maxSumTwoNoOverlap(self, A: List[int], L: int, M: int) -> int:
        
        def maxSum(L:int, M:int) -> int:
            sumL = sumM = 0
            for i in range(0, L + M):
                if i < L:
                    sumL += A[i]
                else:
                    sumM += A[i]    
            maxL, ans = sumL, sumL + sumM
            for i in range(L + M, len(A)):
                sumL += A[i - M] - A[i - L - M]
                maxL = max(maxL, sumL)
                sumM += A[i] - A[i - M]
                ans = max(ans, maxL + sumM)
            return ans
        
        return max(maxSum(L, M), maxSum(M, L)) 
```
**Analysis:**

Time: `O(n)`, space: `O(1)`, where `n = A.length`.


</p>


