---
title: "Validate IP Address"
weight: 445
#id: "validate-ip-address"
---
## Description
<div class="description">
<p>Given a string <code>IP</code>, return <code>&quot;IPv4&quot;</code> if IP is a valid IPv4 address,&nbsp;<code>&quot;IPv6&quot;</code> if&nbsp;IP is a valid IPv6 address or <code>&quot;Neither&quot;</code> if IP is not a correct IP of any type.</p>

<p><strong>A valid IPv4</strong> address&nbsp;is an IP in the form <code>&quot;x<sub>1</sub>.x<sub>2</sub>.x<sub>3</sub>.x<sub>4</sub>&quot;</code> where <code>0 &lt;=&nbsp;x<sub><span style="font-size: 10.8333px;">i</span></sub>&nbsp;&lt;= 255</code> and <code>x<sub>i</sub></code> <strong>cannot contain</strong> leading zeros. For example, <code>&quot;192.168.1.1&quot;</code> and&nbsp;<code>&quot;192.168.1.0&quot;</code> are valid IPv4 addresses but <code>&quot;192.168.01.1&quot;</code>, while <code>&quot;192.168.1.00&quot;</code>&nbsp;and <code>&quot;192.168@1.1&quot;</code> are invalid IPv4 addresses.</p>

<p><strong>A valid IPv6</strong> address&nbsp;is an IP in the form <code>&quot;x<sub>1</sub>:x<sub>2</sub>:x<sub>3</sub>:x<sub>4:</sub>x<sub>5</sub>:x<sub>6</sub>:x<sub>7</sub>:x<sub>8</sub>&quot;</code> where:</p>

<ul>
	<li><code>1 &lt;= x<sub>i</sub>.length &lt;= 4</code></li>
	<li><code>x<sub>i</sub></code> is a&nbsp;<strong>hexadecimal string</strong> which may contain digits, lower-case English letter (<code>&#39;a&#39;</code> to <code>&#39;f&#39;</code>) and upper-case English letters (<code>&#39;A&#39;</code> to <code>&#39;F&#39;</code>).</li>
	<li>Leading zeros are allowed in <code>x<sub>i</sub></code>.</li>
</ul>

<p>For example,&nbsp;&quot;<code>2001:0db8:85a3:0000:0000:8a2e:0370:7334&quot;</code>&nbsp;and &quot;<code>2001:db8:85a3:0:0:8A2E:0370:7334&quot;</code> are valid IPv6 addresses, while &quot;<code>2001:0db8:85a3::8A2E:037j:7334&quot;</code>&nbsp;and &quot;<code>02001:0db8:85a3:0000:0000:8a2e:0370:7334&quot;</code> are invalid IPv6 addresses.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> IP = &quot;172.16.254.1&quot;
<strong>Output:</strong> &quot;IPv4&quot;
<strong>Explanation:</strong> This is a valid IPv4 address, return &quot;IPv4&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> IP = &quot;2001:0db8:85a3:0:0:8A2E:0370:7334&quot;
<strong>Output:</strong> &quot;IPv6&quot;
<strong>Explanation:</strong> This is a valid IPv6 address, return &quot;IPv6&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> IP = &quot;256.256.256.256&quot;
<strong>Output:</strong> &quot;Neither&quot;
<strong>Explanation:</strong> This is neither a IPv4 address nor a IPv6 address.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> IP = &quot;2001:0db8:85a3:0:0:8A2E:0370:7334:&quot;
<strong>Output:</strong> &quot;Neither&quot;
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> IP = &quot;1e1.4.5.6&quot;
<strong>Output:</strong> &quot;Neither&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>IP</code> consists only of English letters, digits and the characters <code>&#39;.&#39;</code> and <code>&#39;:&#39;</code>.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Cisco - 10 (taggedByAdmin: false)
- Microsoft - 6 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Overview

The first idea is to use try/catch construct with built-in 
facilities: [ipaddress](https://docs.python.org/3/library/ipaddress.html) 
lib in Python and [InetAddress](https://docs.oracle.com/javase/7/docs/api/java/net/InetAddress.html) 
class in Java.

**Note that the code below validates the _real-life_ IPv4, 
and _real-life_ IPv6. 
It will not work for this problem because the problem validates 
not _real-life_ but _"simplified"_ versions of IPv4 and IPv6.**

Some big companies, for example, Microsoft and Amazon, 
redefine IPv4 and IPv6 on the interviews for the sake of simplicity.
Below one could find an extended discussion about the differences.

<iframe src="https://leetcode.com/playground/MerLbXFX/shared" frameBorder="0" width="100%" height="208" name="MerLbXFX"></iframe>

Note that these facilities both refer to 
[POSIX-compatible](https://linux.die.net/man/3/inet_addr) 
`inet-addr()` routine for parsing addresses. 
That's why they consider chunks with leading zeros
not as an error, but as an _octal_ representation.

> Components of the dotted address can be specified in decimal, 
_octal (with a leading 0)_, or hexadecimal, with a leading 0X). 

As a result, `01.01.01.012` will be a valid IP address in 
octal representation, as it should be. 
To check this behaviour, one can run the command `ping 01.01.01.012` 
in the console. The address `01.01.01.012` will be considered 
as the one in octal representation, 
converted into its decimal representation `1.1.1.10`,
therefore the ping command would be executed without errors.

By contrary, problem description directly states that 
_leading zeros in the IPv4 is invalid_.
That's not a real-life case, but probably done for the sake 
of simplicity.
Imho, that makes the problem to be a bit schoolish and less fun.
Though let's deal with it anyway, since the problem is very popular recently 
in Microsoft and Amazon. 

There are three main ways to solve it:
 
- Regex (_i.e._ regular expression). Less performing one, though it's a good way to demonstrate 
your knowledge of regex.

- Divide and Conquer, the simplest one.

- Mix of "Divide and Conquer" and "Try/Catch with built-in facilities", 
this time with ones to convert string to integer. 
Try/catch in this situation is a sort of "dirty"
solution because [usually the code inside try blocks is not optimized as 
it'd otherwise be by the compiler](https://blogs.msmvps.com/peterritchie/2007/06/22/performance-implications-of-try-catch-finally/),
and it's better not to use it during the interview.
<br />
<br />


---
#### Approach 1: Regex

Let's construct step by step regex for "IPv4" 
as it's described in the problem description. Note, that it's not
a real-life IPv4 because of leading zeros problem as we've discussed above. 

Anyway, we start to construct regex pattern by using raw string in Python 
`r''` and standard string `""` in Java. Here is how its skeleton looks like for Python

![diff](../Figures/468/regex_ipv4.png)

and here is for Java

![diff](../Figures/468/java_ipv4.png)

Now the problem is reduced to the construction of pattern to match each chunk.
It's an integer in range (0, 255), and the leading zeros are not allowed.
That results in five possible situations:

1. Chunk contains only one digit, from 0 to 9.

2. Chunk contains two digits. The first one could be from 1 to 9, and the second 
one from 0 to 9.

3. Chunk contains three digits, and the first one is `1`. The second and the third ones 
could be from 0 to 9.

4. Chunk contains three digits, the first one is `2` and the second one is from 0 to 4.
Then the third one could be from 0 to 9.

5. Chunk contains three digits, the first one is `2`,  and the second one is `5`.
Then the third one could be from 0 to 5.

Let's use pipe to create a regular expression that will match either case 1, or 
case 2, ..., or case 5. 

![diff](../Figures/468/chunk_regex.png) 

The job is done. The same logic could be used to construct "IPv6" regex pattern.

**Implementation**

<iframe src="https://leetcode.com/playground/GvZ6YH32/shared" frameBorder="0" width="100%" height="310" name="GvZ6YH32"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(1)$$ because the patterns to match have 
constant length.
    
* Space complexity: $$\mathcal{O}(1)$$. 
<br />
<br />


---
#### Approach 2: Divide and Conquer

**Intuition**

Both IPv4 and IPv6 addresses are composed of several substrings separated by certain delimiter,
and each of the substrings is of the same format.

![diff](../Figures/468/divide_conquer.png)

Therefore, intuitively, we could break down the address into chunks, 
and then verify them one by one.

The address is valid _if and only if_ each of the chunks is valid.
We can call this methodology _divide and conquer_.

**Algorithm**

- For the IPv4 address, we split IP into four chunks by the delimiter `.`,
while for IPv6 address, we split IP into eight chunks by the delimiter `:`.

- For each substring of "IPv4" address, 
we check if it is an integer between `0 - 255`, and there is no leading zeros.

- For each substring of "IPv6" address, 
we check if it's a hexadecimal number of length `1 - 4`.
 
**Implementation**

<iframe src="https://leetcode.com/playground/5cVP4s6p/shared" frameBorder="0" width="100%" height="500" name="5cVP4s6p"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N)$$ because to count number of dots requires to
parse the entire input string.
    
* Space complexity: $$\mathcal{O}(1)$$. 
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python/Java] Easy Understand Solution
- Author: lee215
- Creation Date: Thu Sep 14 2017 16:20:02 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 25 2020 16:43:33 GMT+0800 (Singapore Standard Time)

<p>
**Python**
```py
def validIPAddress(self, IP):

        def isIPv4(s):
            try: return str(int(s)) == s and 0 <= int(s) <= 255
            except: return False

        def isIPv6(s):
            try: return len(s) <= 4 and int(s, 16) >= 0
            except: return False

        if IP.count(".") == 3 and all(isIPv4(i) for i in IP.split(".")):
            return "IPv4"
        if IP.count(":") == 7 and all(isIPv6(i) for i in IP.split(":")):
            return "IPv6"
        return "Neither"
```

**Java**
from @kusiroll
```java
  public static String validIPAddress(String IP) {
        String[] ipv4 = IP.split("\\.",-1);
        String[] ipv6 = IP.split("\\:",-1);
        if(IP.chars().filter(ch -> ch == \'.\').count() == 3){
            for(String s : ipv4) if(isIPv4(s)) continue;else return "Neither"; return "IPv4";
        }
        if(IP.chars().filter(ch -> ch == \':\').count() == 7){
            for(String s : ipv6) if(isIPv6(s)) continue;else return "Neither";return "IPv6";
        }
        return "Neither";
    }
    public static boolean isIPv4 (String s){
         try{ return String.valueOf(Integer.valueOf(s)).equals(s) && Integer.parseInt(s) >= 0 && Integer.parseInt(s) <= 255;}
         catch (NumberFormatException e){return false;}
    }
    public static boolean isIPv6 (String s){
        if (s.length() > 4) return false;
        try {return Integer.parseInt(s, 16) >= 0  && s.charAt(0) != \'-\';}
        catch (NumberFormatException e){return false;}
    }
```
</p>


### [C++] {Regex Solution, 4 lines of code, Time & Space= O(1) beats 100%}
- Author: hritikkumar
- Creation Date: Tue Jun 16 2020 17:03:05 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 16 2020 20:16:23 GMT+0800 (Singapore Standard Time)

<p>
# **Solution**
#
**If, You like the solution. Please upvote!**
```
class Solution {
public:
    string validIPAddress(string IP) {
        regex ipv4("(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])"), ipv6("((([0-9a-fA-F]){1,4})\\:){7}([0-9a-fA-F]){1,4}");   // create regex object for regulare expression
        if(regex_match(IP, ipv4)) // match regex expression with given IP string for IPv4
            return "IPv4";
        else if(regex_match(IP, ipv6)) // match regex expression with given IP string for IPv6
            return "IPv6";
        return "Neither"; // Otherwise return "Neither"
    }
};
```
# **Learn Regex**
**Regular Expression (Regex) In C++**
A regular expression or regex is an expression containing a sequence of characters that define a particular search pattern that can be used in string searching algorithms, find or find/replace algorithms, etc. Regexes are also used for input validation.

Most of the programming languages provide either built-in capability for regex or through libraries. From C++11 onwards, C++ provides regex support by means of the standard library via the header.

A regex processor that is used to parse a regex translates it into an internal representation that is executed and matched against a string that represents the text being searched. C++11 uses ECMAScript grammar as the default grammar for regex. ECMAScript is simple, yet it provides powerful regex capabilities.

Let us see some of the patterns that we specify in regex like Range Specification, Repeated Patterns, etc.

**Range Specifications**
Specifying a range of characters or literals is one of the simplest criteria used in a regex.

For example, we can specify a range of lowercase letters from a to z as follows:

```
[a-z]
```

This will match exactly one lowercase character.

The following criteria,

```
[A-Za-z0-9]
```

The above expression specifies the range containing one single uppercase character, one lowercase character and a digit from 0 to 9.

The brackets ([]) in the above expressions have a special meaning i.e. they are used to specify the range. If you want to include a bracket as part of an expression, then you will need to escape it.

So the following expression,

```
[\[0-9]
```

The above expression indicates an opening bracket and a digit in the range 0 to 9 as a regex.

But note that as we are programming in C++, we need to use the C++ specific escape sequence as follows:

```
[\\[0-9]
```

**Repeated Pattern**
The range examples that we have specified above match only one character or literal. If we want to match more than one character, we usually specify the \u201Cexpression modifier\u201D along with the pattern thereby making it a repeated pattern.

An expression modifier can be \u201C+\u201D that suggests matching the occurrence of a pattern one or more times or it can be \u201C*\u201D that suggests matching the occurrence of a pattern zero or more times.

For Example, the following expression,

[a-z]+ matches the strings like a, aaa, abcd, softwaretestinghelp, etc. Note that it will never match a blank string.

The expression,

```
[a-z]* 
```
will match a blank string or any of the above strings.

If you want to specify a group of characters to match one or more times, then you can use the parentheses as follows:

```
(Xyz)+
```

The above expression will match Xyz, XyzXyz, and XyzXyzXyz, etc.

**C++ regex Example**
Consider a regular expression that matches an MS-DOS filename as shown below.

```char regex_filename[] = \u201C[a-zA-Z_] [a-zA-Z_0-9]*\\.[a-zA-Z0-9]+\u201D;```
The above regex can be interpreted as follows:

Match a letter (lowercase and then uppercase) or an underscore. Then match zero or more characters, in which each may be a letter, or an underscore or a digit. Then match a literal dot (.). After the dot, match one or more characters, in which each may be a letter or digit indicating file extension.

Function Templates Used In C++ Regex
Let\'s now discuss some of the important function templates while programming regex in C++.

regex_match()
This function template is used to match the given pattern. This function returns true if the given expression matches the string. Otherwise, the function returns false.

Following is a C++ programming example that demonstrates the regex_match function.
```
#include <iostream>
#include <string>
#include <regex>
using namespace std;
int main () {
   if (regex_match ("softwareTesting", regex("(soft)(.*)") ))
      cout << "string:literal => matched\
";
   const char mystr[] = "SoftwareTestingHelp";
   string str ("software");
   regex str_expr ("(soft)(.*)");
   if (regex_match (str,str_expr))
      cout << "string:object => matched\
";
   if ( regex_match ( str.begin(), str.end(), str_expr ) )
      cout << "string:range(begin-end)=> matched\
";
   cmatch cm;
   regex_match (mystr,cm,str_expr);
    
   smatch sm;
   regex_match (str,sm,str_expr);
    
   regex_match ( str.cbegin(), str.cend(), sm, str_expr);
   cout << "String:range, size:" << sm.size() << " matches\
";
   
   regex_match ( mystr, cm, str_expr, regex_constants::match_default );
   cout << "the matches are: ";
   for (unsigned i=0; i<sm.size(); ++i) {
      cout << "[" << sm[i] << "] ";
   }
   cout << endl;
   return 0;
}
```

```
Output:

C++ input validation
```

In the above program, first, we match the string \u201CsoftwareTesting\u201D against the regular expression \u201C(\u201C(soft)(.*)\u201D using the regex_match function. Subsequently, we also demonstrate different variations of regex_match by passing it a string object, range, etc.

```
regex_search()
```
The function regex_search() is used to search for a pattern in the string that matches the regular expression.

Consider the following C++ program that shows the usage of regex_search().
```
#include
#include
#include<string.h>
using namespace std;

int main()
{
//string to be searched
string mystr = "She sells_sea shells in the sea shore";

// regex expression for pattern to be searched 
regex regexp("s[a-z_]+"); 

// flag type for determining the matching behavior (in this case on string objects)
 smatch m; 

// regex_search that searches pattern regexp in the string mystr  
regex_search(mystr, m, regexp); 

cout<<"String that matches the pattern:"<<endl;
for (auto x : m) 
    cout << x << " "; 
return 0; 
}
```
  
```
Output:

regex_search
```
We specify a string and then a regular expression using the regex object. This string and regex are passed to the regex_search function along with the smatch flag type. The function searches for the first occurrence of pattern in the input string and returns the matched string.
```
regex_replace()
```
The function regex_replace() is used to replace the pattern matching to a regular expression with a string.

Let\'s use a C++ program to demonstrate the regex_replace() function.
```
#include
#include
#include
#include
using namespace std;

int main()
{
string mystr = "This is software testing Help portal \
";

cout<<"Input string: "<<mystr<<endl;
   
// regex to match string beginning with \'p\' 
regex regexp("p[a-zA-z]+"); 
cout<<"Replace the word \'portal\' with word \'website\' : "; 
// regex_replace() for replacing the match with the word \'website\'  
cout << regex_replace(mystr, regexp, "website"); 
 
string result; 
   
cout<<"Replace the word \'website\' back to \'portal\': ";
// regex_replace( ) for replacing the match back with \'portal\' 
regex_replace(back_inserter(result), mystr.begin(), mystr.end(), 
              regexp,  "portal"); 

cout << result; 

return 0; 
}
```
Output:

regex_replace
Here, we have an input string. We provide a regular expression to match a string starting with \u2018p\u2019. Then we replace the matched word with the word \u2018website\u2019. Next, we replace the \u2018website\u2019 word back to the portal.

C++ Input Validation
We have discussed the major function templates that are used for pattern matching using regex. It is notable that the main purpose that regex serves is input validation. You can validate the input entered from a standard input device using the regex expression.

Check the below program to demonstrate how you can use the regex to validate incoming data.
```
#include <iostream>
#include <regex>
#include <string>
using namespace std;
  
int main()
{
    string input;
    regex integer_expr("(\\+|-)?[[:digit:]]+");
    //As long as the input is correct ask for another number
    while(true)
    {
        cout<<"Enter the input: ";
        cin>>input;
        if(!cin) break;
        //Exit when the user inputs q
        if(input=="q")
            break;
        if(regex_match(input,integer_expr))
            cout<<"Input is an integer"<<endl;
        else
        {cout<<"Invalid input : Not an integer"<<endl;}
    }
}
```
```
Output:

C++ input validation
```
This program matches the input entered by the user to validate if it\u2019s an integer. The above output shows that when an integer is entered, it gives an appropriate message and when any other data is entered it gives the message as invalid input.

**Conclusion**
Regex is used in search engines to search patterns, search & replace dialogs of applications like word processors and text editors. Regex is also used in UNIX utilities like sed, awk as well as lexical analysis of the program.

We have seen the functions that are used to match, search and replace patterns in this tutorial. Using these functions, we can basically develop an efficient application that implements the desired functionality using regex.

Regex allows efficiently validating the input or search and replacing a string by patching pattern and this can be done using a very few lines of C++ code.
</p>


### Java Simple Solution
- Author: fabrizio3
- Creation Date: Sun Dec 11 2016 19:50:33 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 12:58:00 GMT+0800 (Singapore Standard Time)

<p>
```
public String validIPAddress(String IP) {
	if(isValidIPv4(IP)) return "IPv4";
	else if(isValidIPv6(IP)) return "IPv6";
	else return "Neither";
}

public boolean isValidIPv4(String ip) {
	if(ip.length()<7) return false;
	if(ip.charAt(0)=='.') return false;
	if(ip.charAt(ip.length()-1)=='.') return false;
	String[] tokens = ip.split("\\.");
	if(tokens.length!=4) return false;
	for(String token:tokens) {
		if(!isValidIPv4Token(token)) return false;
	}
	return true;
}
public boolean isValidIPv4Token(String token) {
	if(token.startsWith("0") && token.length()>1) return false;
	try {
		int parsedInt = Integer.parseInt(token);
		if(parsedInt<0 || parsedInt>255) return false;
		if(parsedInt==0 && token.charAt(0)!='0') return false;
	} catch(NumberFormatException nfe) {
		return false;
	}
	return true;
}
	
public boolean isValidIPv6(String ip) {
	if(ip.length()<15) return false;
	if(ip.charAt(0)==':') return false;
	if(ip.charAt(ip.length()-1)==':') return false;
	String[] tokens = ip.split(":");
	if(tokens.length!=8) return false;
	for(String token: tokens) {
		if(!isValidIPv6Token(token)) return false;
	}
	return true;
}
public boolean isValidIPv6Token(String token) {
	if(token.length()>4 || token.length()==0) return false;
	char[] chars = token.toCharArray();
	for(char c:chars) {
		boolean isDigit = c>=48 && c<=57;
		boolean isUppercaseAF = c>=65 && c<=70;
		boolean isLowerCaseAF = c>=97 && c<=102;
		if(!(isDigit || isUppercaseAF || isLowerCaseAF)) 
			return false;
	}
	return true;
}
```
</p>


