---
title: "Design a Stack With Increment Operation"
weight: 1283
#id: "design-a-stack-with-increment-operation"
---
## Description
<div class="description">
<p>Design a stack which supports the following operations.</p>

<p>Implement the <code>CustomStack</code> class:</p>

<ul>
	<li><code>CustomStack(int maxSize)</code> Initializes the object with <code>maxSize</code> which is the maximum number of elements in the stack or do nothing if the stack reached the <code>maxSize</code>.</li>
	<li><code>void push(int x)</code>&nbsp;Adds <code>x</code> to the top of the stack if the stack hasn&#39;t reached the <code>maxSize</code>.</li>
	<li><code>int pop()</code>&nbsp;Pops and returns the top of stack or <strong>-1</strong> if the stack is empty.</li>
	<li><code>void inc(int k, int val)</code> Increments the bottom <code>k</code> elements of the stack by <code>val</code>. If there are less than <code>k</code> elements in the stack, just increment all the elements in the stack.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;CustomStack&quot;,&quot;push&quot;,&quot;push&quot;,&quot;pop&quot;,&quot;push&quot;,&quot;push&quot;,&quot;push&quot;,&quot;increment&quot;,&quot;increment&quot;,&quot;pop&quot;,&quot;pop&quot;,&quot;pop&quot;,&quot;pop&quot;]
[[3],[1],[2],[],[2],[3],[4],[5,100],[2,100],[],[],[],[]]
<strong>Output</strong>
[null,null,null,2,null,null,null,null,null,103,202,201,-1]
<strong>Explanation</strong>
CustomStack customStack = new CustomStack(3); // Stack is Empty []
customStack.push(1);                          // stack becomes [1]
customStack.push(2);                          // stack becomes [1, 2]
customStack.pop();                            // return 2 --&gt; Return top of the stack 2, stack becomes [1]
customStack.push(2);                          // stack becomes [1, 2]
customStack.push(3);                          // stack becomes [1, 2, 3]
customStack.push(4);                          // stack still [1, 2, 3], Don&#39;t add another elements as size is 4
customStack.increment(5, 100);                // stack becomes [101, 102, 103]
customStack.increment(2, 100);                // stack becomes [201, 202, 103]
customStack.pop();                            // return 103 --&gt; Return top of the stack 103, stack becomes [201, 202]
customStack.pop();                            // return 202 --&gt; Return top of the stack 102, stack becomes [201]
customStack.pop();                            // return 201 --&gt; Return top of the stack 101, stack becomes []
customStack.pop();                            // return -1 --&gt; Stack is empty return -1.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= maxSize &lt;= 1000</code></li>
	<li><code>1 &lt;= x &lt;= 1000</code></li>
	<li><code>1 &lt;= k &lt;= 1000</code></li>
	<li><code>0 &lt;= val &lt;= 100</code></li>
	<li>At most&nbsp;<code>1000</code>&nbsp;calls will be made to each method of <code>increment</code>, <code>push</code> and <code>pop</code> each separately.</li>
</ul>
</div>

## Tags
- Stack (stack)
- Design (design)

## Companies
- Microsoft - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- endurance - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Lazy increment, O(1)
- Author: lee215
- Creation Date: Sun Mar 15 2020 12:02:06 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 15 2020 22:57:09 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Use an additional array to record the increment value.
`inc[i]` means for all elements `stack[0] ~ stack[i]`,
we should plus `inc[i]` when popped from the stack.
Then `inc[i-1]+=inc[i]`,
so that we can accumulate the increment `inc[i]`
for the bottom elements and the following `pop`s.
<br>

## **Complexity**
C++/Python, initialization is `O(1)` time & space.
Java, initialization is `O(N)` time & space.
(We cam use ArrayList, but shrug)
`push`, `pop`, `increment`, all O(1) time and space.
<br>

**Java**
```java
    int n;
    int[] inc;
    Stack<Integer> stack;
    public CustomStack(int maxSize) {
        n = maxSize;
        inc = new int[n];
        stack = new Stack<>();
    }

    public void push(int x) {
        if (stack.size() < n)
            stack.push(x);
    }

    public int pop() {
        int i = stack.size() - 1;
        if (i < 0)
            return -1;
        if (i > 0)
            inc[i - 1] += inc[i];
        int res = stack.pop() + inc[i];
        inc[i] = 0;
        return res;
    }

    public void increment(int k, int val) {
        int i = Math.min(k, stack.size()) - 1;
        if (i >= 0)
            inc[i] += val;
    }
```
**C++:**
```cpp
    vector<int> stack, inc;
    int n;
    CustomStack(int maxSize) {
        n = maxSize;
    }

    void push(int x) {
        if (stack.size() == n) return;
        stack.push_back(x);
        inc.push_back(0);
    }

    int pop() {
        int i = stack.size() - 1;
        if (i < 0) return -1;
        if (i > 0) inc[i - 1] += inc[i];
        int res = stack[i] + inc[i];
        stack.pop_back();
        inc.pop_back();
        return res;
    }

    void increment(int k, int val) {
        int i = min(k, (int)stack.size()) - 1;
        if (i >= 0) inc[i] += val;
    }
```

**Python:**
```py
    def __init__(self, maxSize):
        self.n = maxSize
        self.stack = []
        self.inc = []

    def push(self, x):
        if len(self.inc) < self.n:
            self.stack.append(x)
            self.inc.append(0)

    def pop(self):
        if not self.inc: return -1
        if len(self.inc) > 1:
            self.inc[-2] += self.inc[-1]
        return self.stack.pop() + self.inc.pop()

    def increment(self, k, val):
        if self.inc:
            self.inc[min(k, len(self.inc)) - 1] += val
```

</p>


### [Java/Python 3] simple codes using  ArrayList/list.
- Author: rock
- Creation Date: Sun Mar 15 2020 12:01:37 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 15 2020 19:25:40 GMT+0800 (Singapore Standard Time)

<p>
```java
    private List<Integer> stk = new ArrayList<>();
    private int sz;
    public CustomStack(int maxSize) {
        sz = maxSize;
    }
    
    public void push(int x) {
        if (stk.size() < sz) {
            stk.add(x);
        }
    }
    
    public int pop() {
        return stk.isEmpty() ? -1 : stk.remove(stk.size() - 1);
    }
    
    public void increment(int k, int val) {
        for (int i = 0; i < k && i < stk.size(); ++i) {
            stk.set(i, stk.get(i) + val);            
        }
    }
```
```python

    def __init__(self, maxSize: int):
        self.stk = []
        self.sz = maxSize

    def push(self, x: int) -> None:
        if len(self.stk) < self.sz:
            self.stk.append(x)

    def pop(self) -> int:
        return self.stk.pop() if self.stk else -1

    def increment(self, k: int, val: int) -> None:
        for i in range(min(k, len(self.stk))):
            self.stk[i] += val
```
</p>


### Python Prefix sum with 1 array
- Author: hxuanhung
- Creation Date: Tue Mar 17 2020 16:21:35 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 17 2020 16:21:35 GMT+0800 (Singapore Standard Time)

<p>
Prefix sum reference can be found [here](https://en.wikipedia.org/wiki/Prefix_sum/).

```
class CustomStack:
    def __init__(self, maxSize: int):
        self.size = maxSize
        self.stack = []
        
    def push(self, x: int) -> None:
        if len(self.stack) < self.size:
            self.stack.append([x, 0])
        
    def pop(self) -> int:
        if not self.stack:
            return -1
        
        res, inc = self.stack.pop()
        res += inc
        
        if self.stack:
            self.stack[-1][1] += inc
            
        return res
        
    def increment(self, k: int, val: int) -> None:
        if self.stack:
            self.stack[min(k, len(self.stack)) - 1][1] += val
```
</p>


