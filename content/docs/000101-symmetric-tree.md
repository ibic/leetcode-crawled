---
title: "Symmetric Tree"
weight: 101
#id: "symmetric-tree"
---
## Description
<div class="description">
<p>Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).</p>

<p>For example, this binary tree <code>[1,2,2,3,4,4,3]</code> is symmetric:</p>

<pre>
    1
   / \
  2   2
 / \ / \
3  4 4  3
</pre>

<p>&nbsp;</p>

<p>But the following <code>[1,2,2,null,3,null,3]</code> is not:</p>

<pre>
    1
   / \
  2   2
   \   \
   3    3
</pre>

<p>&nbsp;</p>

<p><b>Follow up:</b> Solve it both recursively and iteratively.</p>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 13 (taggedByAdmin: false)
- Capital One - 7 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: true)
- Oracle - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- LinkedIn - 7 (taggedByAdmin: true)
- Microsoft - 4 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- Apple - 5 (taggedByAdmin: false)
- Atlassian - 4 (taggedByAdmin: false)
- Yandex - 3 (taggedByAdmin: false)
- Twitter - 3 (taggedByAdmin: false)
- TripAdvisor - 2 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Tesla - 2 (taggedByAdmin: false)
- Audible - 2 (taggedByAdmin: false)

## Official Solution
## Solution
---

#### Approach 1: Recursive

A tree is symmetric if the left subtree is a mirror reflection of the right subtree.

![Push an element in stack](https://leetcode.com/media/original_images/101_Symmetric.png){:width="200px"}
{:align="center"}

Therefore, the question is: when are two trees a mirror reflection of each other?

Two trees are a mirror reflection of each other if:

1. Their two roots have the same value.
2. The right subtree of each tree is a mirror reflection of the left subtree of the other tree.

![Push an element in stack](https://leetcode.com/media/original_images/101_Symmetric_Mirror.png){:width="400px"}
{:align="center"}

This is like a person looking at a mirror. The reflection in the mirror has the same head, but the reflection's right arm corresponds to the actual person's left arm, and vice versa.

The explanation above translates naturally to a recursive function as follows.

<iframe src="https://leetcode.com/playground/bQ9ZjXvv/shared" frameBorder="0" width="100%" height="242" name="bQ9ZjXvv"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Because we traverse the entire input tree once, the total run time is $$O(n)$$, where $$n$$ is the total number of nodes in the tree.


* Space complexity : The number of recursive calls is bound by the height of the tree. In the worst case, the tree is linear and the height is in $$O(n)$$. Therefore, space complexity due to recursive calls on the stack is $$O(n)$$ in the worst case.
<br />
<br />
---
#### Approach 2: Iterative

Instead of recursion, we can also use iteration with the aid of a queue. Each two consecutive nodes in the queue should be equal, and their subtrees a mirror of each other. Initially, the queue contains `````root````` and `````root`````. Then the algorithm works similarly to BFS, with some key differences. Each time, two nodes are extracted and their values compared. Then, the right and left children of the two nodes are inserted in the queue in opposite order. The algorithm is done when either the queue is empty, or we detect that the tree is not symmetric (i.e. we pull out two consecutive nodes from the queue that are unequal).

<iframe src="https://leetcode.com/playground/n5mXkUjQ/shared" frameBorder="0" width="100%" height="344" name="n5mXkUjQ"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Because we traverse the entire input tree once, the total run time is $$O(n)$$, where $$n$$ is the total number of nodes in the tree.


* Space complexity : There is additional space required for the search queue. In the worst case, we have to insert $$O(n)$$ nodes in the queue. Therefore, space complexity is $$O(n)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Recursive and non-recursive solutions in Java
- Author: lvlolitte
- Creation Date: Fri Dec 12 2014 16:03:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 23:12:41 GMT+0800 (Singapore Standard Time)

<p>
Recursive--400ms:

    public boolean isSymmetric(TreeNode root) {
        return root==null || isSymmetricHelp(root.left, root.right);
    }
    
    private boolean isSymmetricHelp(TreeNode left, TreeNode right){
        if(left==null || right==null)
            return left==right;
        if(left.val!=right.val)
            return false;
        return isSymmetricHelp(left.left, right.right) && isSymmetricHelp(left.right, right.left);
    }

Non-recursive(use Stack)--460ms:

    public boolean isSymmetric(TreeNode root) {
        if(root==null)  return true;
        
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode left, right;
        if(root.left!=null){
            if(root.right==null) return false;
            stack.push(root.left);
            stack.push(root.right);
        }
        else if(root.right!=null){
            return false;
        }
            
        while(!stack.empty()){
            if(stack.size()%2!=0)   return false;
            right = stack.pop();
            left = stack.pop();
            if(right.val!=left.val) return false;
            
            if(left.left!=null){
                if(right.right==null)   return false;
                stack.push(left.left);
                stack.push(right.right);
            }
            else if(right.right!=null){
                return false;
            }
                
            if(left.right!=null){
                if(right.left==null)   return false;
                stack.push(left.right);
                stack.push(right.left);
            }
            else if(right.left!=null){
                return false;
            }
        }
        
        return true;
    }
</p>


### 1ms recursive Java Solution, easy to understand
- Author: yangneu2015
- Creation Date: Mon Nov 02 2015 02:20:28 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 05:13:16 GMT+0800 (Singapore Standard Time)

<p>
     public boolean isSymmetric(TreeNode root) {
        if(root==null) return true;
        return isMirror(root.left,root.right);
    }
    public boolean isMirror(TreeNode p, TreeNode q) {
        if(p==null && q==null) return true;
        if(p==null || q==null) return false;
        return (p.val==q.val) && isMirror(p.left,q.right) && isMirror(p.right,q.left);
    }
</p>


### Recursively and iteratively solution in Python
- Author: wizcabbit
- Creation Date: Sun Nov 02 2014 16:35:29 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 12:12:23 GMT+0800 (Singapore Standard Time)

<p>
Basically, this question is recursively. Or we can say, the tree structure is recursively, so the recursively solution maybe easy to write:

TC: O(b) SC: O(log n)

    class Solution:
      def isSymmetric(self, root):
        if root is None:
          return True
        else:
          return self.isMirror(root.left, root.right)

      def isMirror(self, left, right):
        if left is None and right is None:
          return True
        if left is None or right is None:
          return False

        if left.val == right.val:
          outPair = self.isMirror(left.left, right.right)
          inPiar = self.isMirror(left.right, right.left)
          return outPair and inPiar
        else:
          return False

The essence of recursively is Stack, so we can use our own stack to rewrite it into iteratively:

     class Solution2:
      def isSymmetric(self, root):
        if root is None:
          return True

        stack = [[root.left, root.right]]

        while len(stack) > 0:
          pair = stack.pop(0)
          left = pair[0]
          right = pair[1]

          if left is None and right is None:
            continue
          if left is None or right is None:
            return False
          if left.val == right.val:
            stack.insert(0, [left.left, right.right])

            stack.insert(0, [left.right, right.left])
          else:
            return False
        return True
</p>


