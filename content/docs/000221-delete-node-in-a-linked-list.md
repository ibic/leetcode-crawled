---
title: "Delete Node in a Linked List"
weight: 221
#id: "delete-node-in-a-linked-list"
---
## Description
<div class="description">
<p>Write a function to <strong>delete a node</strong> in a singly-linked list. You will <strong>not</strong> be given access to the <code>head</code> of the list, instead you will be given access to <strong>the node to be deleted</strong> directly.</p>

<p>It is <strong>guaranteed</strong> that the node to be deleted is <strong>not a tail node</strong> in the list.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/01/node1.jpg" style="width: 450px; height: 322px;" />
<pre>
<strong>Input:</strong> head = [4,5,1,9], node = 5
<strong>Output:</strong> [4,1,9]
<strong>Explanation: </strong>You are given the second node with value 5, the linked list should become 4 -&gt; 1 -&gt; 9 after calling your function.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/01/node2.jpg" style="width: 450px; height: 354px;" />
<pre>
<strong>Input:</strong> head = [4,5,1,9], node = 1
<strong>Output:</strong> [4,5,9]
<strong>Explanation: </strong>You are given the third node with value 1, the linked list should become 4 -&gt; 5 -&gt; 9 after calling your function.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> head = [1,2,3,4], node = 3
<strong>Output:</strong> [1,2,4]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> head = [0,1], node = 0
<strong>Output:</strong> [1]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> head = [-3,5,-99], node = -3
<strong>Output:</strong> [5,-99]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of the nodes in the given list is in the range <code>[2, 1000]</code>.</li>
	<li><code>-1000 &lt;= Node.val &lt;= 1000</code></li>
	<li>The value of each node in the list is <strong>unique</strong>.</li>
	<li>The <code>node</code> to be deleted is <strong>in the list</strong> and is <strong>not a tail</strong> node</li>
</ul>

</div>

## Tags
- Linked List (linked-list)

## Companies
- Adobe - 2 (taggedByAdmin: true)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Qualcomm - 2 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: true)
- Apple - 2 (taggedByAdmin: true)
- Yahoo - 2 (taggedByAdmin: false)

## Official Solution
## Solution

#### Approach: Swap with Next Node [Accepted]

The usual way of deleting a node `node` from a linked list is to modify the `next` pointer of the node *before* it, to point to the node *after* it.

<img src= "https://leetcode.com/media/original_images/237_LinkedList.png" width="400" />

Since we do not have access to the node *before* the one we want to delete, we cannot modify the `next` pointer of that node in any way. Instead, we have to replace the value of the node we want to delete with the value in the node after it, and then delete the node after it.

<img src="https://leetcode.com/media/original_images/237_LinkedList2.png" width="400" />

<img src="https://leetcode.com/media/original_images/237_LinkedList3.png" width="400" />

<img src="https://leetcode.com/media/original_images/237_LinkedList4.png" width="330" />

Because we know that the node we want to delete is not the tail of the list, we can guarantee that this approach is possible.

**Java**

```java
public void deleteNode(ListNode node) {
    node.val = node.next.val;
    node.next = node.next.next;
}
```

**Complexity Analysis**

Time and space complexity are both $$O(1)$$.

Analysis written by: @noran

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Why LeetCode accepted such stupid question?
- Author: smfwuxiao
- Creation Date: Wed Jul 15 2015 13:59:39 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 03:03:15 GMT+0800 (Singapore Standard Time)

<p>
This question is stupid and should be deleted intermediately.
</p>


### 1-3 lines, C++/Java/Python/C/C#/JavaScript/Ruby
- Author: StefanPochmann
- Creation Date: Wed Jul 15 2015 07:31:04 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 09:08:27 GMT+0800 (Singapore Standard Time)

<p>
We can't really delete the node, but we can kinda achieve the same effect by instead removing the **next** node after copying its data into the node that we were asked to delete.

**C++**

    void deleteNode(ListNode* node) {
        *node = *node->next;
    }

But better properly delete the next node:

    void deleteNode(ListNode* node) {
        auto next = node->next;
        *node = *next;
        delete next;
    }

**Java and C#**

    public void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }

**Python**

    def deleteNode(self, node):
        node.val = node.next.val
        node.next = node.next.next

**C**

    void deleteNode(struct ListNode* node) {
        *node = *node->next;
    }

But better properly free the next node's memory:

    void deleteNode(struct ListNode* node) {
        struct ListNode* next = node->next;
        *node = *next;
        free(next);
    }

**JavaScript**

    var deleteNode = function(node) {
        node.val = node.next.val;
        node.next = node.next.next;
    };

**Ruby**

    def delete_node(node)
        node.val = node.next.val
        node.next = node.next.next
        nil
    end
</p>


### Easy solution in java
- Author: yangneu2015
- Creation Date: Thu Oct 29 2015 07:28:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 23 2018 09:10:16 GMT+0800 (Singapore Standard Time)

<p>
    public void deleteNode(ListNode node) {
        node.val=node.next.val;
        node.next=node.next.next;
    }

Since we couldn't enter the preceding node, we can not delete the given node. We can just copy the next node to the given node and delete the next one.
</p>


