---
title: "Shortest Path to Get All Keys"
weight: 809
#id: "shortest-path-to-get-all-keys"
---
## Description
<div class="description">
<p>We are given a 2-dimensional&nbsp;<code>grid</code>.&nbsp;<code>&quot;.&quot;</code> is an empty cell, <code>&quot;#&quot;</code> is&nbsp;a wall, <code>&quot;@&quot;</code> is the starting point, (<code>&quot;a&quot;</code>, <code>&quot;b&quot;</code>, ...) are keys, and (<code>&quot;A&quot;</code>,&nbsp;<code>&quot;B&quot;</code>, ...) are locks.</p>

<p>We start at the starting point, and one move consists of walking one space in one of the 4 cardinal directions.&nbsp; We cannot walk outside the grid, or walk into a wall.&nbsp; If we walk over a key, we pick it up.&nbsp; We can&#39;t walk over a lock unless we have the corresponding key.</p>

<p>For some <font face="monospace">1 &lt;= K &lt;= 6</font>, there is exactly one lowercase and one uppercase letter of the first <code>K</code> letters of the English alphabet in the grid.&nbsp; This means that there is exactly one key for each lock, and one lock for each key; and also that the letters used to represent the keys and locks were&nbsp;chosen in the same order as the English alphabet.</p>

<p>Return the lowest number of moves to acquire all keys.&nbsp; If&nbsp;it&#39;s impossible, return <code>-1</code>.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[&quot;@.a.#&quot;,&quot;###.#&quot;,&quot;b.A.B&quot;]</span>
<strong>Output: </strong><span id="example-output-1">8</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[&quot;@..aA&quot;,&quot;..B#.&quot;,&quot;....b&quot;]</span>
<strong>Output: </strong><span id="example-output-2">6</span>
</pre>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= grid.length&nbsp;&lt;= 30</code></li>
	<li><code>1 &lt;= grid[0].length&nbsp;&lt;= 30</code></li>
	<li><code>grid[i][j]</code> contains only<code> &#39;.&#39;</code>, <code>&#39;#&#39;</code>, <code>&#39;@&#39;</code>,&nbsp;<code>&#39;a&#39;-</code><code>&#39;f</code><code>&#39;</code> and <code>&#39;A&#39;-&#39;F&#39;</code></li>
	<li>The number of keys is in <code>[1, 6]</code>.&nbsp; Each key has a different letter and opens exactly one lock.</li>
</ol>
</div>

</div>

## Tags
- Heap (heap)
- Breadth-first Search (breadth-first-search)

## Companies
- Airbnb - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Brute Force + Permutations

**Intuition and Algorithm**

We have to pick up the keys $$K$$ in some order, say $$K_{\sigma_i}$$.

For each ordering, let's do a breadth first search to find the distance to the next key.

For example, if the keys are `'abcdef'`, then for each ordering such as `'bafedc'`, we will try to calculate the candidate distance from `'@' -> 'b' -> 'a' -> 'f' -> 'e' -> 'd' -> 'c'`.

Between each segment of our path (and corresponding breadth-first search), we should remember what keys we've picked up.  Keys that are picked up become part of a mask that helps us identify what locks we are allowed to walk through during the next breadth-first search.

Each part of the algorithm is relatively straightforward, but the implementation in total can be quite challenging.  See the comments for more details.

<iframe src="https://leetcode.com/playground/hAZj7aeU/shared" frameBorder="0" width="100%" height="500" name="hAZj7aeU"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(R * C * \mathcal{A} * \mathcal{A}!)$$, where $$R, C$$ are the dimensions of the grid, and $$\mathcal{A}$$ is the maximum number of keys ($$\mathcal{A}$$ because it is the "size of the alphabet".)  Each `bfs` is performed up to $$\mathcal{A} * \mathcal{A}!$$ times.

* Space Complexity:  $$O(R * C + \mathcal{A}!)$$, the space for the `bfs` and to store the candidate key permutations.
<br />
<br />


---
#### Approach 2: Points of Interest + Dijkstra

**Intuition and Algorithm**

Clearly, we only really care about walking between points of interest: the keys, locks, and starting position.  We can use this insight to speed up our calculation.

Let's make this intuition more formal: any walk can be decomposed into *primitive* segments, where each segment (between two points of interest) is primitive if and only if it doesn't touch any other point of interest in between.

Then, we can calculate the distance (of a primitive segment) between any two points of interest, using a breadth first search.

Afterwards, we have some graph (where each node refers to at most $$13$$ places, and at most $$2^6$$ states of keys).  We have a starting node (at `'@'` with no keys) and ending nodes (at anywhere with all keys.)  We also know all the costs to go from one node to another - each node has outdegree at most 13.  This shortest path problem is now ideal for using Dijkstra's algorithm.

Dijkstra's algorithm uses a priority queue to continually searches the path with the lowest cost to destination, so that when we reach the target, we know it must have been through the lowest cost path.  Refer to [this link](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm) for more detail.

Again, each part of the algorithm is relatively straightforward (for those familiar with BFS and Dijkstra's algorithm), but the implementation in total can be quite challenging.

<iframe src="https://leetcode.com/playground/B7FbiSus/shared" frameBorder="0" width="100%" height="500" name="B7FbiSus"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(RC(2\mathcal{A} + 1) + \mathcal{E} \log \mathcal{N})$$, where $$R, C$$ are the dimensions of the grid, and $$\mathcal{A}$$ is the maximum number of keys, $$\mathcal{N} = (2\mathcal{A} + 1) * 2^\mathcal{A}$$ is the number of nodes when we perform Dijkstra's, and $$\mathcal{E} = \mathcal{N} * (2 \mathcal{A} + 1)$$ is the maximum number of edges.

* Space Complexity:  $$O(\mathcal{N})$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java BFS Solution
- Author: wangzi6147
- Creation Date: Sun Jul 08 2018 13:27:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 00:57:59 GMT+0800 (Singapore Standard Time)

<p>
1. Use Bit to represent the keys.
2. Use `State` to represent visited states.

```
class Solution {
    class State {
        int keys, i, j;
        State(int keys, int i, int j) {
            this.keys = keys;
            this.i = i;
            this.j = j;
        }
    }
    public int shortestPathAllKeys(String[] grid) {
        int x = -1, y = -1, m = grid.length, n = grid[0].length(), max = -1;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                char c = grid[i].charAt(j);
                if (c == \'@\') {
                    x = i;
                    y = j;
                }
                if (c >= \'a\' && c <= \'f\') {
                    max = Math.max(c - \'a\' + 1, max);
                }
            }
        }
        State start = new State(0, x, y);
        Queue<State> q = new LinkedList<>();
        Set<String> visited = new HashSet<>();
        visited.add(0 + " " + x + " " + y);
        q.offer(start);
        int[][] dirs = new int[][]{{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
        int step = 0;
        while (!q.isEmpty()) {
            int size = q.size();
            while (size-- > 0) {
                State cur = q.poll();
                if (cur.keys == (1 << max) - 1) {
                    return step;
                }
                for (int[] dir : dirs) {
                    int i = cur.i + dir[0];
                    int j = cur.j + dir[1];
                    int keys = cur.keys;
                    if (i >= 0 && i < m && j >= 0 && j < n) {
                        char c = grid[i].charAt(j);
                        if (c == \'#\') {
                            continue;
                        }
                        if (c >= \'a\' && c <= \'f\') {
                            keys |= 1 << (c - \'a\');
                        }
                        if (c >= \'A\' && c <= \'F\' && ((keys >> (c - \'A\')) & 1) == 0) {
                            continue;
                        }
                        if (!visited.contains(keys + " " + i + " " + j)) {
                            visited.add(keys + " " + i + " " + j);
                            q.offer(new State(keys, i, j));
                        }
                    }
                }
            }
            step++;
        }
        return -1;
    }
}
```
</p>


### [C++] BFS with current key recorded visited map (12ms)
- Author: rx3360441619
- Creation Date: Sun Jul 08 2018 15:10:11 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 16 2018 09:47:28 GMT+0800 (Singapore Standard Time)

<p>
The idea is based on the thoughts that, usually, you won\'t want to visit a position where you have traversed.
However, in this problem, you may have to go back and forth to get keys for the lock. 
So the keypoint is "how to relax the visited condition"?
The keypoint is that "you are only allowed to revisit a position if you get new keys".
To solve it, I use a visited map considering current carried keys, and prevent to revisit the same position if hold key is not updated. 
Original visited map is two-dimension, and with keys dimension added, it extends to a three-dimension vector. 
Since the problem tells us that 1<=K<=6, we know at most 64 conditions here. (only have to consider key, and no need to consider lock status)
```
int shortestPathAllKeys(vector<string>& grid) {
    int m=grid.size(), n=m?grid[0].size():0;
    if(!m || !n) return 0;
    int path=0, K=0;
    vector<int> dirs={0,-1,0,1,0};
    vector<vector<vector<bool>>> visited(m,vector<vector<bool>>(n,vector<bool>(64,0))); //at most 6 keys, using bitmap 111111
    queue<pair<int,int>> q; //<postion, hold keys mapping>
    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            if(grid[i][j]==\'@\'){
                q.push({i*n+j,0});
                visited[i][j][0]=1;                    
            }
            if(grid[i][j]>=\'A\' && grid[i][j]<=\'F\') K++; //total alpha number
        }
    }
    while(!q.empty()){
        int size=q.size();
        for(int i=0;i<size;i++){
            int a=q.front().first/n, b=q.front().first%n;
            int carry=q.front().second;
            q.pop();        
            if(carry==((1<<K)-1)) return path; //if all keys hold, just return 
            for(int j=0;j<4;j++){
                int x=a+dirs[j], y=b+dirs[j+1], k=carry;
                if(x<0 || x>=m || y<0 || y>=n || grid[x][y]==\'#\') continue;
                if(grid[x][y]>=\'a\' && grid[x][y]<=\'f\'){
                    k=carry|(1<<(grid[x][y]-\'a\')); //update hold keys
                }
                else if(grid[x][y]>=\'A\' && grid[x][y]<=\'F\'){
                    if(!(carry & (1<<(grid[x][y]-\'A\')))) continue;
                }
                if(!visited[x][y][k]){
                    visited[x][y][k]=1;
                    q.push({x*n+y,k});
               }                
            }
        }
        path++;
    }
    return -1;
}
```
</p>


### O(mn2^k) 500ms Python BFS Easy to Understand Solution
- Author: yidong_w
- Creation Date: Sun Jul 08 2018 12:45:55 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 02:22:58 GMT+0800 (Singapore Standard Time)

<p>
1. Get the starting point, and how many keys are in the grid.
2. BFS. Find all the keys.

Trick1: I have a string called keys which not only have keys, but also all the chars that you  can step on. I appended Keys to that string when I got one.  
Trick2: In order not to repeat past moves, I have a set called moves. I check if I have already went to that location with current key string.

If I run out of options,  I will then return -1

Time complexity: O(mn2^k)
k: number of keys
n: column length
m: row length

```
class Solution(object):
    def shortestPathAllKeys(self, grid):
        """
        :type grid: List[str]
        :rtype: int
        """
        n, m = len(grid), len(grid[0])
        numOfKeys = 0
        direc = [[0,1],[0,-1],[1,0],[-1,0]]
        moves = set()
        
        for i in range(n):
            for j in range(m):
                if grid[i][j] == \'@\':
                    starti = i
                    startj = j
                elif grid[i][j] in "abcdef":
                    numOfKeys += 1
        
        deque = collections.deque()
        deque.append([starti, startj, 0, ".@abcdef", 0])
        
        while deque:
            i, j, steps, keys, collectedKeys = deque.popleft()

            if grid[i][j] in "abcdef" and grid[i][j].upper() not in keys:
                keys += grid[i][j].upper()
                collectedKeys += 1
            
            if collectedKeys == numOfKeys:
                return steps

            for x, y in direc:
                ni = i+x
                nj = j+y
                if 0<=ni<n and 0<=nj<m and grid[ni][nj] in keys:
                    if (ni, nj, keys) not in moves:
                        moves.add((ni,nj,keys))
                        deque.append([ni, nj, steps + 1, keys, collectedKeys])
                
        return -1
```

edit: thanks to @yecye, I change the time complexity from O(kmn) to O(mn2^k)
</p>


