---
title: "Shortest Path Visiting All Nodes"
weight: 791
#id: "shortest-path-visiting-all-nodes"
---
## Description
<div class="description">
<p>An undirected, connected graph of N nodes (labeled&nbsp;<code>0, 1, 2, ..., N-1</code>) is given as <code>graph</code>.</p>

<p><code>graph.length = N</code>, and <code>j != i</code>&nbsp;is in the list&nbsp;<code>graph[i]</code>&nbsp;exactly once, if and only if nodes <code>i</code> and <code>j</code> are connected.</p>

<p>Return the length of the shortest path that visits every node. You may start and stop at any node, you may revisit nodes multiple times, and you may reuse edges.</p>

<p>&nbsp;</p>

<ol>
</ol>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>[[1,2,3],[0],[0],[0]]
<strong>Output: </strong>4
<strong>Explanation</strong>: One possible path is [1,0,2,0,3]</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>[[1],[0,2,4],[1,3,4],[2],[1,2]]
<strong>Output: </strong>4
<strong>Explanation</strong>: One possible path is [0,1,4,2,3]
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= graph.length &lt;= 12</code></li>
	<li><code>0 &lt;= graph[i].length &lt;&nbsp;graph.length</code></li>
</ol>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Breadth First Search [Accepted]

**Intuition**

A path 'state' can be represented as the subset of nodes visited, plus the current 'head' node.  Then, the problem reduces to a shortest path problem among these states, which can be solved with a breadth-first search.

**Algorithm**

Let's call the set of nodes visited by a path so far the *cover*, and the current node as the *head*.  We'll store the `cover` states using set bits: `k` is in the cover if the `k`th bit of `cover` is 1.

For states `state = (cover, head)`, we can perform a breadth-first search on these states.  The neighbors are `(cover | (1 << child), child)` for each `child in graph[head]`.

If at any point we find a state with all set bits in it's cover, because it is a breadth-first search, we know this must represent the shortest path length.

<iframe src="https://leetcode.com/playground/ZQqZacPp/shared" frameBorder="0" width="100%" height="500" name="ZQqZacPp"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(2^N * N)$$.

* Space Complexity:  $$O(2^N * N)$$.

---
#### Approach #2: Dynamic Programming [Accepted]

**Intuition**

A path 'state' can be represented as the subset of nodes visited, plus the current 'head' node.  As in Approach #1, we have a recurrence in states: `answer(cover, head)` is `min(1 + answer(cover | (1<<child), child) for child in graph[head])`.  Because these states form a DAG (a directed graph with no cycles), we can do dynamic programming.

**Algorithm**

Let's call the set of nodes visited by a path so far the *cover*, and the current node as the *head*.  We'll store `dist[cover][head]` as the length of the shortest path with that cover and head.  We'll store the `cover` states using set bits, and maintain the loop invariant (on `cover`), that `dist[k][...]` is correct for `k < cover`.

For every state `(cover, head)`, the possible `next` (neighbor) nodes in the path are found in `graph[head]`.  The new `cover2` is the old cover plus `next`.

For each of these, we perform a "relaxation step" (for those familiar with the Bellman-Ford algorithm), where if the new candidate distance for `dist[cover2][next]` is larger than `dist[cover][head] + 1`, we'll update it to `dist[cover][head] + 1`.

Care must be taken to perform the relaxation step multiple times on the same cover if `cover = cover2`.  This is because a minimum state for `dist[cover][head]` might only be achieved through multiple steps through some path.

Finally, it should be noted that we are using implicitly the fact that when iterating `cover = 0 .. (1<<N) - 1`, that each new cover `cover2 = cover | 1 << x` is such that `cover2 >= cover`.  This implies a topological ordering, which means that the recurrence on these states form a DAG.

<iframe src="https://leetcode.com/playground/RUqmSSSw/shared" frameBorder="0" width="100%" height="500" name="RUqmSSSw"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(2^N * N)$$.

* Space Complexity:  $$O(2^N * N)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Fast BFS Solution (46ms) -- Clear, Detailed Explanation Included
- Author: simonzhu91
- Creation Date: Sun Jun 03 2018 23:28:11 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 13:54:00 GMT+0800 (Singapore Standard Time)

<p>
Idea is to use `BFS` to traverse the graph. Since all edges are weighted 1, we can use a Queue (instead of a PriorityQueue sorted by cost). Since all edges are weighted 1, then closer nodes will always be evaluated before farther ones. 

In order to represent a path, I used a combination of 3 variables: 
1. `int bitMask`: mask of all the nodes we visited so far: `0 -> not visited`, `1 -> visited` (Originally this was `Set<Integer> `of all nodes we visited so far, but since the Solution `TLE` and  `N <= 12`, it turns out we can use a bitMask and `32 bits` total in an Integer can cover all the possibilities. This is a small speed up optimization.)
2. `int curr`: current node we are on
3. `int cost`: the total cost in the path. 

Each path taken will have a unique combination of these 3 variables. 

We initialize our queue to contain N possible paths each starting from [0,N-1]. This is because we can start at any of N possible Nodes.

At each step, we remove element from the `queue` and see if we have covered all 12 nodes in our `bitMask`. If we cover all nodes, we return the cost of the path immediately. Since we are using `BFS`, this is *guranteed to be path with the lowest cost.* 

Otherwise, we get all the neighbors of current node, and for each neighbor, set the Node to visited in `bitMask`, and then add it back into the queue. 

In order to prevent duplicate paths from being visited, we use a `Set<Tuple>` to store the `Set<Path>` that we have visited before. Since we don\'t really need the `cost` here, I set `cost` to 0 for elements stored in `Set`. *You could also set the actual cost value here, it wouldn\'t make a difference :)*

```
class Solution {
    
    public int shortestPathLength(int[][] graph) {
        
        int N = graph.length;
        
        Queue<Tuple> queue = new LinkedList<>();
        Set<Tuple> set = new HashSet<>();

        for(int i = 0; i < N; i++){
            int tmp = (1 << i);
            set.add(new Tuple(tmp, i, 0));
            queue.add(new Tuple(tmp, i, 1));
        }
        
        while(!queue.isEmpty()){
            Tuple curr = queue.remove();
    
            if(curr.bitMask == (1 << N) - 1){
                return curr.cost - 1;
            } else {
                int[] neighbors = graph[curr.curr];
                
                for(int v : neighbors){
                    int bitMask = curr.bitMask;
                    bitMask = bitMask | (1 << v);
                    
                    Tuple t = new Tuple(bitMask, v, 0);
                    if(!set.contains(t)){
                        queue.add(new Tuple(bitMask, v, curr.cost + 1));
                        set.add(t);
                    }         
                }
            }
        }
        return -1;
    }
}

class Tuple{
    int bitMask;
    int curr;
    int cost;
    
    public Tuple(int bit, int n, int c){
        bitMask = bit;
        curr = n;
        cost = c;
    }
    
    public boolean equals(Object o){
        Tuple p = (Tuple) o;
        return bitMask == p.bitMask && curr == p.curr && cost == p.cost;
    }
    
    public int hashCode(){
        return 1331 * bitMask + 7193 * curr + 727 * cost;
    }
}
```
</p>


### Screencast of LeetCode Weekly Contest 87
- Author: cuiaoxiang
- Creation Date: Sun Jun 03 2018 11:04:01 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 14 2019 17:03:00 GMT+0800 (Singapore Standard Time)

<p>
https://www.youtube.com/watch?v=Zx8rhXFichQ

Not doing a good job this time. Wasted a lot of time debugging on some stupid mistakes in problem 4, and debugging on LeetCode web UI is definitely not a great experience :p

Hope it\'s helpful.
</p>


### Java DP Solution
- Author: jasonzzzzz
- Creation Date: Sun Jun 03 2018 12:52:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 11 2018 01:55:50 GMT+0800 (Singapore Standard Time)

<p>
Using a two-dimensional matrix to record the distance from a point through those points.
Example: 
dp[0][3(00000...00011)] = 2 means the distance starting point 0 through 0, 1 is 2 
dp[1][7(00000...00111)] = 3 means the distance starting point 1 through 0, 1, 2 is 3.

After BFS, we will know the distance through all nodes, compare dp[0][11111...11111](it means that start point 0 through all nodes),  dp[1][11111...11111]
..., dp[n-1][11111...11111], we will get the result.
```
class Solution {
    public int shortestPathLength(int[][] graph) {
	int[][] dp = new int[graph.length][1<<graph.length];
        Queue<State> queue =  new LinkedList<>();
        for (int i = 0; i < graph.length; i++) {
            Arrays.fill(dp[i], Integer.MAX_VALUE);
            dp[i][1<<i]=0;
            queue.offer(new State(1<<i, i));
        }
        
        while (!queue.isEmpty()) {
            State state = queue.poll();
            
            for (int next : graph[state.source]) {
                int nextMask = state.mask | 1 << next;
                if (dp[next][nextMask] > dp[state.source][state.mask]+1) {
                    dp[next][nextMask] = dp[state.source][state.mask]+1;
                    queue.offer(new State(nextMask, next));
                }
            }
        }
        
        int res = Integer.MAX_VALUE;
        for (int i = 0; i < graph.length; i++) {
            res = Math.min(res, dp[i][(1<<graph.length)-1]);
        }
        return res;
	}

	class State {
		public int mask, source;
		public State(int m, int s) {
			mask=m;
			source=s;
		}
	}
}
```
</p>


