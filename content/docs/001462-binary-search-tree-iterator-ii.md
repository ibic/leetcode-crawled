---
title: "Binary Search Tree Iterator II"
weight: 1462
#id: "binary-search-tree-iterator-ii"
---
## Description
<div class="description">
<p>Implement the <code>BSTIterator</code> class that represents an iterator over&nbsp;the <strong><a href="https://en.wikipedia.org/wiki/Tree_traversal#In-order_(LNR)">in-order traversal</a></strong>&nbsp;of&nbsp;a binary search tree (BST):</p>

<ul>
	<li><code>BSTIterator(TreeNode root)</code> Initializes an object of the <code>BSTIterator</code> class. The <code>root</code> of the BST is given as part of the constructor. The pointer should be initialized to a non-existent number smaller than any element in the BST.</li>
	<li><code>boolean hasNext()</code> Returns <code>true</code> if there exists a number in the traversal to the right of the pointer, otherwise returns <code>false</code>.</li>
	<li><code>int next()</code> Moves the pointer to the right, then returns the number at the pointer.</li>
	<li><code>boolean hasPrev()</code> Returns <code>true</code> if there exists a number in the traversal to the left of the pointer, otherwise returns <code>false</code>.</li>
	<li><code>int prev()</code> Moves the pointer to the left, then returns the number at the pointer.</li>
</ul>

<p>Notice that by initializing the pointer to a non-existent smallest number, the first call to <code>next()</code> will return the smallest element in the BST.</p>

<p>You may assume that <code>next()</code> and <code>prev()</code> calls will always be valid. That is, there will be at least a next/previous number in the in-order traversal&nbsp;when <code>next()</code>/<code>prev()</code> is called.</p>

<p><strong>Follow up:</strong> Could you solve the problem without precalculating the values of the tree?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/09/14/untitled-diagram-1.png" style="width: 201px; height: 201px;" /></strong></p>

<pre>
<strong>Input</strong>
[&quot;BSTIterator&quot;, &quot;next&quot;, &quot;next&quot;, &quot;prev&quot;, &quot;next&quot;, &quot;hasNext&quot;, &quot;next&quot;, &quot;next&quot;, &quot;next&quot;, &quot;hasNext&quot;, &quot;hasPrev&quot;, &quot;prev&quot;, &quot;prev&quot;]
[[[7, 3, 15, null, null, 9, 20]], [null], [null], [null], [null], [null], [null], [null], [null], [null], [null], [null], [null]]
<strong>Output</strong>
[null, 3, 7, 3, 7, true, 9, 15, 20, false, true, 15, 9]

<strong>Explanation</strong>
// The underlined element is where the pointer currently is.
BSTIterator bSTIterator = new BSTIterator([7, 3, 15, null, null, 9, 20]); // state is <u> </u> [3, 7, 9, 15, 20]
bSTIterator.next(); // state becomes [<u>3</u>, 7, 9, 15, 20], return 3
bSTIterator.next(); // state becomes [3, <u>7</u>, 9, 15, 20], return 7
bSTIterator.prev(); // state becomes [<u>3</u>, 7, 9, 15, 20], return 3
bSTIterator.next(); // state becomes [3, <u>7</u>, 9, 15, 20], return 7
bSTIterator.hasNext(); // return true
bSTIterator.next(); // state becomes [3, 7, <u>9</u>, 15, 20], return 9
bSTIterator.next(); // state becomes [3, 7, 9, <u>15</u>, 20], return 15
bSTIterator.next(); // state becomes [3, 7, 9, 15, <u>20</u>], return 20
bSTIterator.hasNext(); // return false
bSTIterator.hasPrev(); // return true
bSTIterator.prev(); // state becomes [3, 7, 9, <u>15</u>, 20], return 15
bSTIterator.prev(); // state becomes [3, 7, <u>9</u>, 15, 20], return 9
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree is in the range <code>[1, 10<sup>5</sup>]</code>.</li>
	<li><code>0 &lt;= Node.val &lt;= 10<sup>6</sup></code></li>
	<li>At most 10<sup>5</sup> calls will be made to <code>hasNext</code>, <code>next</code>, <code>hasPrev</code>, and <code>prev</code>.</li>
</ul>
</div>

## Tags
- Tree (tree)
- Design (design)

## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ Stack and List
- Author: votrubac
- Creation Date: Fri Sep 18 2020 02:04:19 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 18 2020 02:04:19 GMT+0800 (Singapore Standard Time)

<p>
For going forward - using a stack, like for the previous problem. For going back, storing traversed values in the list and remember the position.

```cpp
stack<TreeNode*> path;
vector<int> vals;
int pos = -1;
void traverseLeft(TreeNode *root) {
    for (; root != nullptr; root = root->left)
	    path.push(root);
}
BSTIterator(TreeNode* root) { traverseLeft(root); }
bool hasNext() { return pos + 1 < vals.size() || !path.empty(); }
int next() {
    if (++pos == vals.size()) {
        auto cur = path.top();
        path.pop();
        traverseLeft(cur->right);
        vals.push_back(cur->val);
    }
    return vals[pos];
}
bool hasPrev() { return pos > 0; }
int prev() { return vals[--pos]; }
```
</p>


### Java - In-Place, O(n)/O(h) (Detailed)
- Author: crayola
- Creation Date: Wed Sep 23 2020 04:20:46 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 23 2020 05:09:46 GMT+0800 (Singapore Standard Time)

<p>
Same as BST Iterator I. We need a stack to collect all in-order nodes. 
The only difference is that the tree is converted into a list.
Here, all ```left``` nodes will be our ```prev``` that we see in ListNodes. Similarly, all ```right``` nodes will be our ```next```.
```
class BSTIterator {
    private TreeNode dummy = new TreeNode(-1);
    private TreeNode curr = dummy;
    private Stack<TreeNode> stack = new Stack<>();
    
	// Init
    public BSTIterator(TreeNode root) {
        this.collectMins(root);
    }
    
	// If there\'s no right and there\'s no node to append to the \'list\', there\'s no next
    public boolean hasNext() {
        return !(curr.right == null && stack.isEmpty());
    }

	// Go forward. If no right, then we\'re at the end, append next node. Otherwise, just move forward
    public int next() {
		// If there\'s no next then we must have a node ready in the Stack
        if (curr.right == null) {
            TreeNode next = stack.pop();
            
			// Discover next mins and unlink
            collectMins(next.right);
            next.right = null;
            
			// Append to \'list\'
            next.left = curr;
            curr.right = next;
        }

        curr = curr.right;
        
        return curr.val;
    }
    
	// If the \'list\' is empty or we\'re at the first nodes, there is no prev
    public boolean hasPrev() {
        return (curr != dummy && curr.left != dummy);
    }
    
	// Go back
    public int prev() {
        curr = curr.left;
        return curr.val;
    }
    
    // Should only be called once per node
    private void collectMins(TreeNode root) {
        while (root != null) {
            stack.push(root);
            root = root.left;
        }
    }
}
```
</p>


### java stack + linkedList 100%
- Author: trustno1
- Creation Date: Mon Sep 21 2020 12:59:51 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 21 2020 12:59:51 GMT+0800 (Singapore Standard Time)

<p>
```
class BSTIterator {
    Stack<TreeNode> stack;
    class ListNode {
        ListNode prev, next;
        int val;
        public ListNode(int v) {
            val = v;
        }
    }
    ListNode head, tail, cur = null;
    public BSTIterator(TreeNode root) {
        stack = new Stack<>();
        head = new ListNode(-1);
        tail = new ListNode(-1);
        head.next = tail;
        tail.prev = head;
        cur = head;
        while(root != null) {
            stack.push(root);
            root = root.left;
        }
    }
    
    public boolean hasNext() {
        return !stack.isEmpty();
    }
    
    public int next() {
        if(cur.next != tail) {
            cur = cur.next;
            return cur.val;
        }
        if(!stack.isEmpty()) {
            TreeNode pop = stack.pop();
            int val = pop.val;
            pop = pop.right;
            while(pop != null) {
                stack.push(pop);
                pop = pop.left;
            }
            ListNode n = new ListNode(val);
            addToTail(n);
            cur = n;
            return val;
            
        } else return -1;
        
    }
    
    public boolean hasPrev() {
        return cur != head && cur.prev != head;
    }
    
    public int prev() {
      //  System.out.println("cur = " + cur.val);
        if(hasPrev()) {
            cur = cur.prev;
            return cur.val;
        } else return -1;
    }
    private void addToTail(ListNode n) {
        ListNode prev = tail.prev;
        prev.next = n;
        n.prev = prev;
        n.next = tail;
        tail.prev = n;
    }
}

</p>


