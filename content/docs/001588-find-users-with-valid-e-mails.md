---
title: "Find Users With Valid E-Mails"
weight: 1588
#id: "find-users-with-valid-e-mails"
---
## Description
<div class="description">
<p>Table: <code>Users</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| user_id       | int     |
| name          | varchar |
| mail          | varchar |
+---------------+---------+
user_id is the primary key for this table.
This table contains information of the users signed up in a website. Some e-mails are invalid.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to&nbsp;find the&nbsp;users who have <strong>valid emails</strong>.</p>

<p>A valid e-mail has a prefix name and a domain where:&nbsp;</p>

<ul>
	<li><strong>The prefix name</strong> is a string that may contain letters (upper or lower case), digits, underscore <code>&#39;_&#39;</code>, period <code>&#39;.&#39;</code> and/or dash <code>&#39;-&#39;</code>. The prefix name <strong>must</strong> start with a letter.</li>
	<li><strong>The domain</strong> is <code>&#39;@leetcode.com&#39;</code>.</li>
</ul>

<p>Return the result table in any order.</p>

<p>The query result format is in the following example.</p>

<p>&nbsp;</p>

<pre>
<code>Users</code>
+---------+-----------+-------------------------+
| user_id | name      | mail                    |
+---------+-----------+-------------------------+
| 1       | Winston   | winston@leetcode.com    |
| 2       | Jonathan  | jonathanisgreat         |
| 3       | Annabelle | bella-@leetcode.com     |
| 4       | Sally     | sally.come@leetcode.com |
| 5       | Marwan    | quarz#2020@leetcode.com |
| 6       | David     | david69@gmail.com       |
| 7       | Shapiro   | .shapo@leetcode.com     |
+---------+-----------+-------------------------+

Result table:
+---------+-----------+-------------------------+
| user_id | name      | mail                    |
+---------+-----------+-------------------------+
| 1       | Winston   | winston@leetcode.com    |
| 3       | Annabelle | bella-@leetcode.com     |
| 4       | Sally     | sally.come@leetcode.com |
+---------+-----------+-------------------------+
The mail of user 2 doesn&#39;t have a domain.
The mail of user 5 has # sign which is not allowed.
The mail of user 6 doesn&#39;t have leetcode domain.
The mail of user 7 starts with a period.
</pre>

</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A character by character explanation of regular expression
- Author: liuhaigang1989
- Creation Date: Tue Jul 21 2020 06:16:53 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 21 2020 06:20:28 GMT+0800 (Singapore Standard Time)

<p>
```
"""
A detailed explanation of the following regular expression solution:

\'^[A-Za-z]+[A-Za-z0-9\_\.\-]*@leetcode.com\'

1. ^ means the beginning of the string
    - This is important because without it, we can have something like
    \'.shapiro@leetcode.com\'
    This is because *part* of the regex matches the pattern perfectly. 
    The part that is \'shapiro@leetcode.com\'.
    This is how I understand it: regex will return the whole 
    thing as long as part of it matches. By adding ^ we are saying: you have to
    match FROM THE START.
	
2. [] means character set. [A-Z] means any upper case chars. In other words, 
    the short dash in the character set means range.
	
3. After the first and the second character set, there is a notation: + or *.
    + means at least one of the character from the preceding charset, and * means 
    0 or more. 
	
4. \ inside the charset mean skipping. In other words, \. means we want the dot as 
    it is. Remember, for example, - means range in the character set. So what if
     we would like to find - itself as a character? use \-. 
	 
5. Everything else, like @leetcode.com refers to exact match.
"""

select * from Users 
where regexp_like(mail, \'^[A-Za-z]+[A-Za-z0-9\_\.\-]*@leetcode.com\')

</p>


### MySQL regexp with explanation
- Author: BjornCommers
- Creation Date: Wed Jul 22 2020 06:15:28 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 23 2020 02:09:51 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT *
FROM Users
WHERE mail REGEXP \'^[A-Za-z][A-Za-z0-9\_\.\-]*@leetcode\.com$\'
```

^[A-Za-z] the first character must be a letter. after that...
[A-Za-z0-9\_\.\-]* match any number of letters, numbers, underscore, periods, dashes (the * sign indicating "any number of, including zero"). after that...
@leetcode\.com$  the email must end with exactly "@leetcode.com" (the $ sign indicating "ending with")

Note that the escape characters are not strictly necessary. This means that the backslash ( \ ) symbol that precedes the period, dash, etc is not needed here. I\'ve left them in because it\'s what I\'m accustomed to. I don\'t know why exactly MySQL does not require them, but so it is.

Also note that, since SQL is not case sensitive generally, it\'s also not necessary to include both uppercase and lowercase letters (ie one could just as easily use [A-Z] instead of [A-Za-z]. Again, I\'ve included them because outside of SQL that would be necessary.

</p>


### MS SQL LIKE
- Author: serpol
- Creation Date: Mon Jul 20 2020 16:24:17 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 21 2020 15:59:45 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT
    user_id,
    name,
    mail
FROM Users
WHERE mail LIKE \'[a-zA-Z]%@leetcode.com\' AND LEFT(mail, LEN(mail) - 13) NOT LIKE \'%[^0-9a-zA-Z_.-]%\'
```
Here first LIKE checks that the mail starts with letter and ends with @leetcode.com, the second LIKE checks that the first part of the mail does not contain any symbol except allowed.
</p>


