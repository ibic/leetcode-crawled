---
title: "Best Position for a Service Centre"
weight: 1388
#id: "best-position-for-a-service-centre"
---
## Description
<div class="description">
<p>A delivery company wants to build a new service centre in a new city. The company knows the positions of all the customers in this city on a 2D-Map and wants to build the new centre in a position such that <strong>the sum of the euclidean distances to all customers is minimum</strong>.</p>

<p>Given an array <code>positions</code> where <code>positions[i] = [x<sub>i</sub>, y<sub>i</sub>]</code> is the position of the <code>ith</code> customer on the map, return <em>the minimum sum of the euclidean distances</em> to all customers.</p>

<p>In other words, you need to choose the position of the service centre <code>[x<sub>centre</sub>, y<sub>centre</sub>]</code> such that the following formula is minimized:</p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/06/25/q4_edited.jpg" />
<p>Answers within&nbsp;<code>10^-5</code>&nbsp;of the actual value will be accepted.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/06/25/q4_e1.jpg" style="width: 377px; height: 362px;" />
<pre>
<strong>Input:</strong> positions = [[0,1],[1,0],[1,2],[2,1]]
<strong>Output:</strong> 4.00000
<strong>Explanation:</strong> As shown, you can see that choosing [x<sub>centre</sub>, y<sub>centre</sub>] = [1, 1] will make the distance to each customer = 1, the sum of all distances is 4 which is the minimum possible we can achieve.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/06/25/q4_e3.jpg" style="width: 419px; height: 419px;" />
<pre>
<strong>Input:</strong> positions = [[1,1],[3,3]]
<strong>Output:</strong> 2.82843
<strong>Explanation:</strong> The minimum possible sum of distances = sqrt(2) + sqrt(2) = 2.82843
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> positions = [[1,1]]
<strong>Output:</strong> 0.00000
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> positions = [[1,1],[0,0],[2,0]]
<strong>Output:</strong> 2.73205
<strong>Explanation:</strong> At the first glance, you may think that locating the centre at [1, 0] will achieve the minimum sum, but locating it at [1, 0] will make the sum of distances = 3.
Try to locate the centre at [1.0, 0.5773502711] you will see that the sum of distances is 2.73205.
Be careful with the precision!
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> positions = [[0,1],[3,2],[4,5],[7,6],[8,9],[11,1],[2,12]]
<strong>Output:</strong> 32.94036
<strong>Explanation:</strong> You can use [4.3460852395, 4.9813795505] as the position of the centre.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;=&nbsp;positions.length &lt;= 50</code></li>
	<li><code>positions[i].length == 2</code></li>
	<li><code>0 &lt;=&nbsp;positions[i][0],&nbsp;positions[i][1] &lt;= 100</code></li>
</ul>

</div>

## Tags
- Geometry (geometry)

## Companies
- Citadel - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ with picture, zoom-in
- Author: votrubac
- Creation Date: Sun Jul 12 2020 12:02:10 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 13 2020 03:55:07 GMT+0800 (Singapore Standard Time)

<p>
#### Disclaimer
The solution below is an approximation; it\'s accepted but may fail specific test cases. I believe that LeetCode did this on purpose so non-math solutions are accepted during the contest.

The math solution for this problem is to find the [geometric median](https://en.wikipedia.org/wiki/Geometric_median), which can be done using the Weiszfeld\'s algorithm. You can find many great math solutions in Discuss.

The approximation solution, however, can be adjusted to achieve the required precision. For example, you can run it several times with different factor (e.g. 1/10, 1/2, etc.), and then pick the best answer. Also, the approximation solution works for other formulas, not just distances :)

#### Solution

We first find the best 10x10 square to put the service center in the 100x100 grid. Then, find the best 1x1 square in the 30x30 grid (with 10x10 square from the previous step in the middle).

Continue this process, dividing the grid size (`delta`) by 10 till it\'s smaller than `10^-5`.

![image](https://assets.leetcode.com/users/images/bba9a8c2-2ae8-483a-8805-c6de13fcf964_1594580905.356849.png)

```cpp
double getMinDistSum(vector<vector<int>>& pos) {
    double left = 100, bottom = 100, right = 0, top = 0;
    for (auto &p : pos) {
        left = min(left, (double)p[0]);
        bottom = min(bottom, (double)p[1]);
        right = max(right, (double)p[0]);
        top = max(top, (double)p[1]);
    }
    double res = DBL_MAX, res_x = 0, res_y = 0;
    for (double delta = 10; delta >= 0.00001; delta /= 10) {
        for (double x = left; x <= right; x += delta)
            for (double y = bottom; y <= top; y += delta) {
                double d = 0;
                for (auto &p : pos)
                    d += sqrt((p[0] - x) * (p[0] - x) + (p[1] - y) * (p[1] - y));
                if (res > d) {
                    res = d;
                    res_x = x;
                    res_y = y;
                }
            }
        left = res_x - delta;
        bottom = res_y - delta;
        right = res_x + delta * 2;
        top = res_y + delta * 2;
    }
    return res == DBL_MAX ? 0 : res;
}
```
</p>


### java solution, search large area and then narrowdown
- Author: pcheng13
- Creation Date: Sun Jul 12 2020 12:01:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 12 2020 13:10:20 GMT+0800 (Singapore Standard Time)

<p>
The idea is simple:
1. Search a big area with lower precision
2. Once get the closest point, we search around this point with higher precision

However, this solution runs for 1s. Need to optimize. (By changing the descent step from 100 to 10, this solution runs for 20ms. )
```
class Solution {
    public double getMinDistSum(int[][] positions) {
        double min = Double.MAX_VALUE;
        
        //(x,y) is the center of the current search area, delta is the distance we search from the center.
        double x = 50, y=50, delta = 50; 
        // to track the best point we found
        double min_x=x, min_y=y; 
        
        while(delta >= 10e-4){
            //"delta/100" is the incremental step, so the time cost will be 200*200 for each loop. 
            for(double i= x-delta; i<=x+delta; i+=delta/100){
                for(double j= y-delta; j<=y+delta; j+=delta/100){
                    double d = dist(positions,i,j);
                    if(d<=min){
                        min = d;
                        min_x = i;
                        min_y = j;
                    }
                }
            }
            // reset the center for the searching area to (min_x,min_y), set delta to the incremental step delta/100
            x=min_x;
            y=min_y;
            delta /=100;
        }
        return min;
    }
    
    private double dist(int[][] positions, double x, double y){
        double ans = 0;
        
        for(int[] p: positions){
            double d = Math.pow(p[0]-x,2) + Math.pow(p[1]-y,2);
            ans += Math.sqrt(d);
        }
        
        return ans;
    }
}
```
</p>


### C++ Simulated Annealing
- Author: lzl124631x
- Creation Date: Sun Jul 12 2020 12:03:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 12 2020 12:08:12 GMT+0800 (Singapore Standard Time)

<p>
## Solution 1. Simulated Annealing

Starting from a random point (here I use `(0, 0)`). We move around in 4 directions with some initial `step` (I used `100`).

If we can find smaller total distance, we move to that point.

Otherwise, we set `step /= 2`.

We keep this iteration until the `step` is smaller than `1e-6`.

```cpp
// OJ: https://leetcode.com/contest/weekly-contest-197/problems/best-position-for-a-service-centre/
// Author: github.com/lzl124631x
class Solution {
    double dist(vector<int> &a, vector<double> &b) {
        return sqrt(pow(a[0] - b[0], 2) + pow(a[1] - b[1], 2));
    }
    double all(vector<vector<int>> &A, vector<double> &p) {
        double ans = 0;
        for (auto &a : A) ans += dist(a, p);
        return ans;
    }
    const int dirs[4][2] = {{0,1},{0,-1},{-1,0},{1,0}};
public:
    double getMinDistSum(vector<vector<int>>& A) {
        double ans = DBL_MAX;
        vector<double> p(2, 0);
        double step = 100, eps = 1e-6;
        while (step > eps) {
            bool found = false;
            for (auto &dir : dirs) {
                vector<double> next = {p[0] + step * dir[0], p[1] + step * dir[1]};
                double d = all(A, next);
                if (d < ans) {
                    ans = d;
                    p = next;
                    found = true;
                    break;
                }
            }
            if (!found) step /= 2;
        }
        return ans;
    }
};
```
</p>


