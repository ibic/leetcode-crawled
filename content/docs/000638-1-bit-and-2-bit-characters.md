---
title: "1-bit and 2-bit Characters"
weight: 638
#id: "1-bit-and-2-bit-characters"
---
## Description
<div class="description">
<p>We have two special characters. The first character can be represented by one bit <code>0</code>. The second character can be represented by two bits (<code>10</code> or <code>11</code>).  </p>

<p>Now given a string represented by several bits. Return whether the last character must be a one-bit character or not. The given string will always end with a zero.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> 
bits = [1, 0, 0]
<b>Output:</b> True
<b>Explanation:</b> 
The only way to decode it is two-bit character and one-bit character. So the last character is one-bit character.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> 
bits = [1, 1, 1, 0]
<b>Output:</b> False
<b>Explanation:</b> 
The only way to decode it is two-bit character and two-bit character. So the last character is NOT one-bit character.
</pre>
</p>

<p><b>Note:</b>
<li><code>1 <= len(bits) <= 1000</code>.</li>
<li><code>bits[i]</code> is always <code>0</code> or <code>1</code>.</li>
</p>
</div>

## Tags
- Array (array)

## Companies
- Google - 2 (taggedByAdmin: false)
- IXL - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Quora - 0 (taggedByAdmin: true)

## Official Solution
[TOC]


#### Approach #1: Increment Pointer [Accepted]

**Intuition and Algorithm**

When reading from the `i`-th position, if `bits[i] == 0`, the next character must have 1 bit; else if `bits[i] == 1`, the next character must have 2 bits.  We increment our read-pointer `i` to the start of the next character appropriately.  At the end, if our pointer is at `bits.length - 1`, then the last character must have a size of 1 bit.

**Python**
```python
class Solution(object):
    def isOneBitCharacter(self, bits):
        i = 0
        while i < len(bits) - 1:
            i += bits[i] + 1
        return i == len(bits) - 1
```

**Java**
```java
class Solution {
    public boolean isOneBitCharacter(int[] bits) {
        int i = 0;
        while (i < bits.length - 1) {
            i += bits[i] + 1;
        }
        return i == bits.length - 1;
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of `bits`.

* Space Complexity: $$O(1)$$, the space used by `i`.

---
#### Approach #2: Greedy [Accepted]

**Intuition and Algorithm**

The second-last `0` must be the end of a character (or, the beginning of the array if it doesn't exist).  Looking from that position forward, the array `bits` takes the form `[1, 1, ..., 1, 0]` where there are zero or more `1`'s present in total.  It is easy to show that the answer is `true` if and only if there are an even number of ones present.

In our algorithm, we will find the second last zero by performing a linear scan from the right.  We present two slightly different approaches below.

**Python**
```python
class Solution(object):
    def isOneBitCharacter(self, bits):
        parity = bits.pop()
        while bits and bits.pop(): parity ^= 1
        return parity == 0
```

**Java**
```java
class Solution {
    public boolean isOneBitCharacter(int[] bits) {
        int i = bits.length - 2;
        while (i >= 0 && bits[i] > 0) i--;
        return (bits.length - i) % 2 == 0;
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of `bits`.

* Space Complexity: $$O(1)$$, the space used by `parity` (or `i`).

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### JAVA, check only the end of array
- Author: anna_boltenko
- Creation Date: Mon Oct 30 2017 07:28:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 08:16:42 GMT+0800 (Singapore Standard Time)

<p>
We don't need to traverse the whole array,  just check the last part of it.
1) if there is only one symbol in array the answer is always true (as last element is 0)
2) if there are two 0s at the end again the answer is true no matter what  the rest symbols are( ...1100, ...1000,)
3) if there is 1 right before the last element(...10), the outcome depends on the count of sequential 1, i.e.
    a) if there is odd amount of 1(10, ...01110, etc) the answer is false as there is  a single 1 without pair
  b) if it's even (110, ...011110, etc) the answer is true, as 0 at the end doesn't have anything to pair with

```
class Solution {
    public boolean isOneBitCharacter(int[] bits) {
        int ones = 0;
        //Starting from one but last, as last one is always 0.
        for (int i = bits.length - 2; i >= 0 && bits[i] != 0 ; i--) { 
            ones++;
        }
        if (ones % 2 > 0) return false; 
        return true;
    }
}
```
</p>


### Java solution, 1 or 2
- Author: shawngao
- Creation Date: Sun Oct 29 2017 11:26:39 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 21:30:05 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public boolean isOneBitCharacter(int[] bits) {
        int n = bits.length, i = 0;
        while (i < n - 1) {
            if (bits[i] == 0) i++;
            else i += 2;
        }
        return i == n - 1;
    }
}
```
</p>


### Confusing Description
- Author: lMiaoj
- Creation Date: Mon Nov 18 2019 10:29:10 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 18 2019 10:31:17 GMT+0800 (Singapore Standard Time)

<p>
It took me a long time to look at the description and try to understand it. But I failed anyway. I only figured things out after I checked others\' solution in the Discuss area. Funny thing is even when I know what to do, I still find the description confusing.
Here I provide a new version of description.

**Personal Translation of the description :
We use three bit-representations to represent three characters : `0`, `10`, `11` (the corresponding characters don\'t matter)
So bits representation `1`, `01` are not valid.
Given an array of bits ending with `0`, find out if it\'s only valid to translate the last `0` into the `0` bit representation (means it\'s not valid when the last `0` is translated into the `10` bits representation)**
</p>


