---
title: "Lucky Numbers in a Matrix"
weight: 1282
#id: "lucky-numbers-in-a-matrix"
---
## Description
<div class="description">
<p>Given a <code>m * n</code> matrix of <strong>distinct </strong>numbers, return all lucky numbers in the&nbsp;matrix in <strong>any </strong>order.</p>

<p>A lucky number is an element of the matrix such that it is the minimum element in its row and maximum in its column.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> matrix = [[3,7,8],[9,11,13],[15,16,17]]
<strong>Output:</strong> [15]
<strong>Explanation:</strong> 15 is the only lucky number since it is the minimum in its row and the maximum in its column
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> matrix = [[1,10,4,2],[9,3,8,7],[15,16,17,12]]
<strong>Output:</strong> [12]
<strong>Explanation:</strong> 12 is the only lucky number since it is the minimum in its row and the maximum in its column.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> matrix = [[7,8],[1,2]]
<strong>Output:</strong> [7]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m == mat.length</code></li>
	<li><code>n == mat[i].length</code></li>
	<li><code>1 &lt;= n, m &lt;= 50</code></li>
	<li><code>1 &lt;=&nbsp;matrix[i][j]&nbsp;&lt;= 10^5</code>.</li>
	<li>All elements in the matrix are distinct.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Oracle - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] Two 2/3 pass codes w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Mar 15 2020 12:03:06 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 10 2020 23:52:06 GMT+0800 (Singapore Standard Time)

<p>
**Q & A**:
**Q1:** Why we needed to fill array mi with max value?
```
         Arrays.fill(mi, Integer.MAX_VALUE);
```
**A1:** We need it because if we don\'t change every elements in array `mi` with max value, then we will be getting the min of the elements of the matrix against a mi[i] that is set to zero. So min will always be ZERO if we don\'t fill array `mi` with max value. For `mx` value we don\'t need Integer.MIN_VALUE because elements in `mx` are already 0, and elements within the matrix is guaranteed to be positive.  -- credit to **@LC000**.

**End of Q & A**

----

**Method 1:**
1. Find minimum and maxmium values for each row and each column, respectively;
2. Since all numbers in the `matrix` are **distinct**, only when the row minimum equals to the column maximum, it is a lucky number by the problem definition.

```java
    public List<Integer> luckyNumbers (int[][] matrix) {
        int m = matrix.length, n = matrix[0].length;
        int[] mi = new int[m], mx = new int[n];
        Arrays.fill(mi, Integer.MAX_VALUE);
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                mi[i] = Math.min(matrix[i][j], mi[i]);
                mx[j] = Math.max(matrix[i][j], mx[j]);
            }
        }
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < m; ++i)
            for (int j = 0; j < n; ++j)
                if (mi[i] == mx[j]) 
                    res.add(mi[i]);
        return res;        
    }
```
3-pass Python 3 code:
```python
    def luckyNumbers (self, matrix: List[List[int]]) -> List[int]:
        mi = [min(row) for row in matrix]
        mx = [max(col) for col in zip(*matrix)]
        return [cell for i, row in enumerate(matrix) for j, cell in enumerate(row) if mi[i] == mx[j]]
```

----
**Method 2: Set Intersection** credit to **@C0R3**
```java
    public List<Integer> luckyNumbers (int[][] matrix) {
        Set<Integer> minSet = new HashSet<>(), maxSet = new HashSet<>();
        for (int[] row : matrix) {
            int mi = row[0];
            for (int cell : row)
                mi = Math.min(mi, cell);
            minSet.add(mi);
        }
        for (int j = 0; j < matrix[0].length; ++j) {
            int mx = matrix[0][j];
            for (int i = 0; i < matrix.length; ++i)
                mx = Math.max(matrix[i][j], mx);
            if (minSet.contains(mx))
                maxSet.add(mx);
        }
        return new ArrayList<>(maxSet);        
    }
```
```python
    def luckyNumbers (self, matrix: List[List[int]]) -> List[int]:
        return list({min(row) for row in matrix} & {max(col) for col in zip(*matrix)})
```
**Analysis:**

Time: O(m * n), space: O(max(m, n)), where m = matrix.length, n = matrix[0].length.

----


For more practice on similar problem, please refer to: [807. Max Increase to Keep City Skyline](https://leetcode.com/problems/max-increase-to-keep-city-skyline/) - credit to **@cerb668**.
</p>


### [Python] One-Liner using Set Intersection
- Author: C0R3
- Creation Date: Sun Mar 15 2020 12:04:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 15 2020 12:11:13 GMT+0800 (Singapore Standard Time)

<p>
We can create a set of the minimum row values using set comprehension.
To create a set of the maximum column values we can use the unpacking operator (*) on the matrix and use zip to iterate over the unpacked rows in parallel.
We can then get a set of our lucky numbers by intersecting those two sets using the intersection operator (&). Then all that is left to do is to convert the set back into a list.

Time complexity: ```O(m * n)```
Space complexity: ```O(m + n)```
```
class Solution:
    def luckyNumbers(self, matrix: List[List[int]]) -> List[int]:
        return list({min(row) for row in matrix} & {max(col) for col in zip(*matrix)})
```
</p>


### Python 3 -- Easy solution -- Beats 97.29%
- Author: nbismoi
- Creation Date: Sun Mar 29 2020 09:16:05 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 29 2020 09:16:05 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def luckyNumbers (self, matrix: List[List[int]]) -> List[int]:
        minrow = {min(r) for r in matrix}
        maxcol = {max(c) for c in zip(*matrix)} # zip(*) \u5BF9\u77E9\u9635\u8FDB\u884C\u8F6C\u7F6E\uFF0C\u5373\u627E\u51FA\u6BCF\u4E00\u5217\u4E2D\u7684\u6700\u5927\u503C
        return list(minrow & maxcol)

```
</p>


