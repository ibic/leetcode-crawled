---
title: "Customer Order Frequency"
weight: 1587
#id: "customer-order-frequency"
---
## Description
<div class="description">
<p>Table: <code>Customers</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| customer_id   | int     |
| name          | varchar |
| country       | varchar |
+---------------+---------+
customer_id is the primary key for this table.
This table contains information of the customers in the company.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Product</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| product_id    | int     |
| description   | varchar |
| price         | int     |
+---------------+---------+
product_id is the primary key for this table.
This table contains information of the products in the company.
price is the product cost.</pre>

<p>&nbsp;</p>

<p>Table: <code>Orders</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| order_id      | int     |
| customer_id   | int     |
| product_id    | int     |
| order_date    | date    |
| quantity      | int     |
+---------------+---------+
order_id is the primary key for this table.
This table contains information on customer orders.
customer_id is the id of the customer who bought &quot;quantity&quot; products with id &quot;product_id&quot;.
Order_date is the date in format (&#39;YYYY-MM-DD&#39;) when the order was shipped.</pre>

<p>&nbsp;</p>

<p>Write an SQL query to&nbsp;report the&nbsp;customer_id and customer_name of customers who have spent at least $100 in each month of June and July 2020.</p>

<p>Return the result table in any order.</p>

<p>The query result format is in the following example.</p>

<p>&nbsp;</p>

<pre>
<code>Customers</code>
+--------------+-----------+-------------+
| customer_id  | name &nbsp;    | country &nbsp; &nbsp; |
+--------------+-----------+-------------+
| 1    &nbsp;       | Winston  &nbsp;| USA        &nbsp;|
| 2          &nbsp; | Jonathan  | Peru       &nbsp;|
| 3          &nbsp; | Moustafa &nbsp;| Egypt      &nbsp;|
+--------------+-----------+-------------+

<code>Product</code>
+--------------+-------------+-------------+
| product_id   | description | price   &nbsp; &nbsp; |
+--------------+-------------+-------------+
| 10   &nbsp;       | LC Phone &nbsp;  | 300        &nbsp;|
| 20         &nbsp; | LC T-Shirt  | 10         &nbsp;|
| 30         &nbsp; | LC Book    &nbsp;| 45         &nbsp;|
| 40           | LC Keychain&nbsp;| 2         &nbsp; |
+--------------+-------------+-------------+

<code>Orders</code>
+--------------+-------------+-------------+-------------+-----------+
| order_id     | customer_id | product_id  | order_date  | quantity  |
+--------------+-------------+-------------+-------------+-----------+
| 1    &nbsp;       | 1        &nbsp;  | 10         &nbsp;| 2020-06-10  | 1         |
| 2          &nbsp; | 1           | 20         &nbsp;| 2020-07-01  | 1         |
| 3          &nbsp; | 1           | 30         &nbsp;| 2020-07-08  | 2         |
| 4    &nbsp;       | 2        &nbsp;  | 10         &nbsp;| 2020-06-15  | 2         |
| 5          &nbsp; | 2           | 40         &nbsp;| 2020-07-01  | 10        |
| 6          &nbsp; | 3           | 20         &nbsp;| 2020-06-24  | 2         |
| 7    &nbsp;       | 3        &nbsp;  | 30         &nbsp;| 2020-06-25  | 2         |
| 9          &nbsp; | 3           | 30         &nbsp;| 2020-05-08  | 3         |
+--------------+-------------+-------------+-------------+-----------+

Result table:
+--------------+------------+
| customer_id  | name       |  
+--------------+------------+
| 1            | Winston    |
+--------------+------------+ 
Winston spent $300 (300 * 1) in June and $100 ( 10 * 1 + 45 * 2) in July 2020.
Jonathan spent $600 (300 * 2) in June and $20 ( 2 * 10) in July 2020.
Moustafa spent $110 (10 * 2 + 45 * 2) in June and $0 in July 2020.
</pre>
</div>

## Tags


## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple fast MySQL, just GROUP BY and HAVING
- Author: OutOfMemory123
- Creation Date: Tue Jul 21 2020 06:02:43 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 21 2020 06:02:43 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT customer_id, name
FROM Customers JOIN Orders USING(customer_id) 
    JOIN Product USING(product_id)
GROUP BY customer_id
HAVING SUM(IF(LEFT(order_date, 7) = \'2020-06\', quantity, 0) * price) >= 100
    AND SUM(IF(LEFT(order_date, 7) = \'2020-07\', quantity, 0) * price) >= 100
```
</p>


### MySQL - JOIN
- Author: XTY123
- Creation Date: Sun Jul 12 2020 06:57:19 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 15 2020 02:54:05 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT customer_id, name
FROM (
    SELECT c.customer_id, c.name, 
    SUM(CASE WHEN LEFT(o.order_date, 7) = \'2020-06\' THEN p.price*o.quantity ELSE 0 END) AS t1, 
    SUM(CASE WHEN LEFT(o.order_date, 7) = \'2020-07\' THEN p.price*o.quantity ELSE 0 END) AS t2
    FROM customers c
    JOIN orders o
    ON c.customer_id = o.customer_id
    JOIN product p
    ON p.product_id = o.product_id
    GROUP BY 1
    ) tmp
WHERE t1 >= 100 AND t2 >= 100
```
</p>


### Easy to Understand MySQL solution
- Author: vertikau
- Creation Date: Tue Jul 14 2020 10:41:19 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 14 2020 10:41:19 GMT+0800 (Singapore Standard Time)

<p>
```
with temp as(
    select 
    c.customer_id, c.name, sum(p.price * o.quantity) as price
    from Customers c
    inner join 
    Orders o
    on c.customer_id = o.customer_id
    inner join
    Product p
    on o.product_id = p.product_id
    where o.order_date like \'2020-06%\' or o.order_date like \'2020-07%\'
    group by c.customer_id, c.name, month(o.order_date))

select customer_id, name from temp
group by customer_id, name
having sum(case when price >= 100 then 1 else 0 end) = 2
    
    
```
</p>


