---
title: "Design Linked List"
weight: 759
#id: "design-linked-list"
---
## Description
<div class="description">
<p>Design your&nbsp;implementation of the linked list. You can choose to use a singly or doubly linked list.<br />
A node in a singly&nbsp;linked list should have two attributes: <code>val</code>&nbsp;and <code>next</code>. <code>val</code> is the value of the current node, and <code>next</code>&nbsp;is&nbsp;a&nbsp;pointer/reference to the next node.<br />
If you want to use the doubly linked list,&nbsp;you will need&nbsp;one more attribute <code>prev</code> to indicate the previous node in the linked list. Assume all nodes in the linked list are <strong>0-indexed</strong>.</p>

<p>Implement the <code>MyLinkedList</code>&nbsp;class:</p>

<ul>
	<li><code>MyLinkedList()</code>&nbsp;Initializes&nbsp;the&nbsp;<code>MyLinkedList</code> object.</li>
	<li><code>int get(int index)</code>&nbsp;Get the value of&nbsp;the <code>index<sup>th</sup></code>&nbsp;node in the linked list. If the index is invalid, return <code>-1</code>.</li>
	<li><code>void addAtHead(int val)</code>&nbsp;Add a node of value <code>val</code>&nbsp;before the first element of the linked list. After the insertion, the new node will be the first node of the linked list.</li>
	<li><code>void addAtTail(int val)</code>&nbsp;Append a node of value <code>val</code>&nbsp;as the last element of the linked list.</li>
	<li><code>void addAtIndex(int index, int val)</code>&nbsp;Add a node of value <code>val</code>&nbsp;before the <code>index<sup>th</sup></code>&nbsp;node in the linked list.&nbsp;If <code>index</code>&nbsp;equals the length of the linked list, the node will be appended to the end of the linked list. If <code>index</code> is greater than the length, the node <strong>will not be inserted</strong>.</li>
	<li><code>void deleteAtIndex(int index)</code>&nbsp;Delete&nbsp;the <code>index<sup>th</sup></code>&nbsp;node in the linked list, if the index is valid.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input</strong>
[&quot;MyLinkedList&quot;, &quot;addAtHead&quot;, &quot;addAtTail&quot;, &quot;addAtIndex&quot;, &quot;get&quot;, &quot;deleteAtIndex&quot;, &quot;get&quot;]
[[], [1], [3], [1, 2], [1], [1], [1]]
<strong>Output</strong>
[null, null, null, null, 2, null, 3]

<strong>Explanation</strong>
MyLinkedList myLinkedList = new MyLinkedList();
myLinkedList.addAtHead(1);
myLinkedList.addAtTail(3);
myLinkedList.addAtIndex(1, 2);    // linked list becomes 1-&gt;2-&gt;3
myLinkedList.get(1);              // return 2
myLinkedList.deleteAtIndex(1);    // now the linked list is 1-&gt;3
myLinkedList.get(1);              // return 3
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= index, val &lt;= 1000</code></li>
	<li>Please do not use the built-in LinkedList library.</li>
	<li>At most <code>2000</code>&nbsp;calls will be made to&nbsp;<code>get</code>,&nbsp;<code>addAtHead</code>,&nbsp;<code>addAtTail</code>,&nbsp; <code>addAtIndex</code> and&nbsp;<code>deleteAtIndex</code>.</li>
</ul>

</div>

## Tags
- Linked List (linked-list)
- Design (design)

## Companies
- Microsoft - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Interview Strategy

[Linked List](https://en.wikipedia.org/wiki/Linked_list#Basic_concepts_and_nomenclature) 
is a data structure with zero or several elements. 
Each element contains a value and link(s) to the other element(s).
Depending on the number of links, that could be 
singly linked list, doubly linked list and multiply linked list.  

Singly linked list is the simplest one, it provides `addAtHead` in a constant
time, and `addAtTail` in a linear time.
Though doubly linked list is the most used one, because it provides both
`addAtHead` and `addAtTail` in a constant time, and optimises the insert and delete operations.

Doubly linked list is implemented in Java as [LinkedList](https://docs.oracle.com/javase/8/docs/api/java/util/LinkedList.html).
Since these structures are quite well-known, a good interview strategy would be to mention them during the discussion but not to base the code on them. 
Better to use the limited interview time to work with two ideas:
 
- [Sentinel nodes](https://leetcode.com/articles/remove-linked-list-elements/)

>Sentinel nodes are widely used in the trees and linked lists as _pseudo-heads_, _pseudo-tails_, _etc_. They serve as the guardians, as the name suggests, and usually they do not hold any data.

Sentinels nodes will be used here to simplify insert and delete. We would apply this in both of the following approaches.

- Bidirectional search for doubly-linked list

Rather than starting from the head, we could search the node in a doubly-linked list from both head and tail.

If you are familiar with the concepts, you can start directly from the Approach #2. By the way, the Approach #2 is 90% of what you need to solve the problem of [LRU Cache](https://leetcode.com/articles/lru-cache/).

#### Approach 1: Singly Linked List

Let's start from the simplest possible MyLinkedList,
which contains just a structure size and a sentinel head.

![bla](../Figures/707/singly4.png)

<iframe src="https://leetcode.com/playground/FgapYiju/shared" frameBorder="0" width="100%" height="191" name="FgapYiju"></iframe>

Note, that sentinel node is used as a pseudo-head and is always present. 
This way the structure could never be empty, it will 
contain at least a sentinel head.
All nodes in MyLinkedList have a type ListNode: value + 
link to the next element.

<iframe src="https://leetcode.com/playground/SYX4EYUc/shared" frameBorder="0" width="100%" height="140" name="SYX4EYUc"></iframe>

**Add at Index, Add at Head and Add at Tail**

Let's first discuss insert at index operation, 
because thanks to the sentinel node 
addAtTail and addAtHead operations could be reduced to this operation 
as well. 

The idea is straightforward:

- Find the predecessor of the node to insert. If the node is to be inserted
at head, its predecessor is a sentinel head. If the node is to be inserted
at tail, its predecessor is the last node.

- Insert the node by changing the link to the next node.

<iframe src="https://leetcode.com/playground/fwoAySwZ/shared" frameBorder="0" width="100%" height="89" name="fwoAySwZ"></iframe>

![bla](../Figures/707/singly_insert.png)

---

![bla](../Figures/707/singly_insert_head.png)

**Delete at Index**

Basically, the same as insert:

- Find the predecessor.

- Delete node by changing the links to the next node.

<iframe src="https://leetcode.com/playground/JCn4VUye/shared" frameBorder="0" width="100%" height="89" name="JCn4VUye"></iframe>

![bla](../Figures/707/singly_delete.png)

**Get**            

Get is a very straightforward: start from the sentinel node 
and do `index + 1` steps

<iframe src="https://leetcode.com/playground/eaZUVpYD/shared" frameBorder="0" width="100%" height="140" name="eaZUVpYD"></iframe>

![bla](../Figures/707/singly_get.png)

**Implementation**

<iframe src="https://leetcode.com/playground/VzATUXAt/shared" frameBorder="0" width="100%" height="500" name="VzATUXAt"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(1)$$ for addAtHead.
$$\mathcal{O}(k)$$ for get, addAtIndex, and deleteAtIndex, 
where $$k$$ is an index of the element to get, add or delete. 
$$\mathcal{O}(N)$$ for addAtTail.
 
* Space complexity: $$\mathcal{O}(1)$$ for all operations.
<br />
<br />


---
#### Approach 2: Doubly Linked List

Time to implement DLL MyLinkedList, 
which is a much faster (twice faster on the testcase set here) 
though a bit more complex.
It contains size, sentinel head and sentinel tail.

![bla](../Figures/707/dll.png)

<iframe src="https://leetcode.com/playground/fdnutEP7/shared" frameBorder="0" width="100%" height="259" name="fdnutEP7"></iframe>

Note, that sentinel head and tail are always present. 
All nodes in MyLinkedList have a type ListNode: value + 
two links: to the next and to the previous elements.

<iframe src="https://leetcode.com/playground/FLqUZNBB/shared" frameBorder="0" width="100%" height="157" name="FLqUZNBB"></iframe>

**Add at Index, Add at Head and Add at Tail**

The idea is simple:

- Find the predecessor and the successor of the node to insert. 
If the node is to be inserted
at head, its predecessor is a sentinel head. If the node is to be inserted
at tail, its successor is a sentinel tail.

> Use bidirectional search to perform faster.

- Insert the node by changing the links to the next and previous nodes.

<iframe src="https://leetcode.com/playground/AvVfcNym/shared" frameBorder="0" width="100%" height="123" name="AvVfcNym"></iframe>

![bla](../Figures/707/dll_insert2.png)

**Delete at Index**

Basically, the same as insert:

- Find the predecessor and successor.

- Delete node by changing the links to the next and previous nodes.

<iframe src="https://leetcode.com/playground/2cTdWwXC/shared" frameBorder="0" width="100%" height="89" name="2cTdWwXC"></iframe>

![bla](../Figures/707/dll_delete2.png)

**Get**            

Get is very straightforward: 

- Compare `index` and `size - index` to define the fastest way:
starting from the head, or starting from the tail.

- Go to the wanted node.

<iframe src="https://leetcode.com/playground/6sJoFdk8/shared" frameBorder="0" width="100%" height="225" name="6sJoFdk8"></iframe>

![bla](../Figures/707/dll_get2.png)

**Implementation**

<iframe src="https://leetcode.com/playground/7AyotnLt/shared" frameBorder="0" width="100%" height="500" name="7AyotnLt"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(1)$$ for addAtHead and addAtTail.
$$\mathcal{O}(\min(k, N - k))$$ for get, addAtIndex, and deleteAtIndex, 
where $$k$$ is an index of the element to get, add or delete. 
 
* Space complexity: $$\mathcal{O}(1)$$ for all operations.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python solution
- Author: sarishinohara
- Creation Date: Sat Jun 16 2018 21:22:47 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 22:49:46 GMT+0800 (Singapore Standard Time)

<p>
```
class Node(object):

    def __init__(self, val):
        self.val = val
        self.next = None


class MyLinkedList(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.head = None
        self.size = 0

    def get(self, index):
        """
        Get the value of the index-th node in the linked list. If the index is invalid, return -1.
        :type index: int
        :rtype: int
        """
        if index < 0 or index >= self.size:
            return -1

        if self.head is None:
            return -1

        curr = self.head
        for i in range(index):
            curr = curr.next
        return curr.val

    def addAtHead(self, val):
        """
        Add a node of value val before the first element of the linked list.
        After the insertion, the new node will be the first node of the linked list.
        :type val: int
        :rtype: void
        """
        node = Node(val)
        node.next = self.head
        self.head = node

        self.size += 1

    def addAtTail(self, val):
        """
        Append a node of value val to the last element of the linked list.
        :type val: int
        :rtype: void
        """
        curr = self.head
        if curr is None:
            self.head = Node(val)
        else:
            while curr.next is not None:
                curr = curr.next
            curr.next = Node(val)

        self.size += 1

    def addAtIndex(self, index, val):
        """
        Add a node of value val before the index-th node in the linked list.
        If index equals to the length of linked list, the node will be appended to the end of linked list.
        If index is greater than the length, the node will not be inserted.
        :type index: int
        :type val: int
        :rtype: void
        """
        if index < 0 or index > self.size:
            return

        if index == 0:
            self.addAtHead(val)
        else:
            curr = self.head
            for i in range(index - 1):
                curr = curr.next
            node = Node(val)
            node.next = curr.next
            curr.next = node

            self.size += 1

    def deleteAtIndex(self, index):
        """
        Delete the index-th node in the linked list, if the index is valid.
        :type index: int
        :rtype: void
        """
        if index < 0 or index >= self.size:
            return

        curr = self.head
        if index == 0:
            self.head = curr.next
        else:
            for i in range(index - 1):
                curr = curr.next
            curr.next = curr.next.next

        self.size -= 1
```
</p>


### Python AC short & simple linked list solution
- Author: cenkay
- Creation Date: Thu Jul 12 2018 17:48:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 03:21:33 GMT+0800 (Singapore Standard Time)

<p>
```
class Node:
    def __init__(self, value):
        self.val = value
        self.next = self.pre = None
class MyLinkedList:

    def __init__(self):
        self.head = self.tail = Node(-1)
        self.head.next = self.tail
        self.tail.pre = self.head
        self.size = 0
        
    def add(self, preNode, val):
        node = Node(val)
        node.pre = preNode
        node.next = preNode.next
        node.pre.next = node.next.pre = node
        self.size += 1
        
    def remove(self, node):
        node.pre.next = node.next
        node.next.pre = node.pre
        self.size -= 1
        
    def forward(self, start, end, cur):
        while start != end:
            start += 1
            cur = cur.next
        return cur
    
    def backward(self, start, end, cur):
        while start != end:
            start -= 1
            cur = cur.pre
        return cur
    
    def get(self, index):
        if 0 <= index <= self.size // 2:
            return self.forward(0, index, self.head.next).val
        elif self.size // 2 < index < self.size:
            return self.backward(self.size - 1, index, self.tail.pre).val
        return -1

    def addAtHead(self, val):
        self.add(self.head, val)

    def addAtTail(self, val):
        self.add(self.tail.pre, val)

    def addAtIndex(self, index, val):
        if 0 <= index <= self.size // 2:
            self.add(self.forward(0, index, self.head.next).pre, val)
        elif self.size // 2 < index <= self.size:
            self.add(self.backward(self.size, index, self.tail).pre, val)

    def deleteAtIndex(self, index):
        if 0 <= index <= self.size // 2:
            self.remove(self.forward(0, index, self.head.next))
        elif self.size // 2 < index < self.size:
            self.remove(self.backward(self.size - 1, index, self.tail.pre))
```
</p>


