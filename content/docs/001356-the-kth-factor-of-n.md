---
title: "The kth Factor of n"
weight: 1356
#id: "the-kth-factor-of-n"
---
## Description
<div class="description">
<p>Given two positive integers <code>n</code> and <code>k</code>.</p>

<p>A factor of an integer <code>n</code> is defined as an integer <code>i</code> where <code>n % i == 0</code>.</p>

<p>Consider a list of all factors of <code>n</code>&nbsp;sorted in <strong>ascending order</strong>, return <em>the </em><code>kth</code><em> factor</em> in this list or return <strong>-1</strong> if <code>n</code> has less than&nbsp;<code>k</code> factors.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 12, k = 3
<strong>Output:</strong> 3
<strong>Explanation:</strong> Factors list is [1, 2, 3, 4, 6, 12], the 3rd factor is 3.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 7, k = 2
<strong>Output:</strong> 7
<strong>Explanation:</strong> Factors list is [1, 7], the 2nd factor is 7.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 4, k = 4
<strong>Output:</strong> -1
<strong>Explanation:</strong> Factors list is [1, 2, 4], there is only 3 factors. We should return -1.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 1, k = 1
<strong>Output:</strong> 1
<strong>Explanation:</strong> Factors list is [1], the 1st factor is 1.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 1000, k = 3
<strong>Output:</strong> 4
<strong>Explanation:</strong> Factors list is [1, 2, 4, 5, 8, 10, 20, 25, 40, 50, 100, 125, 200, 250, 500, 1000].
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= k &lt;= n &lt;= 1000</code></li>
</ul>
</div>

## Tags
- Math (math)

## Companies
- Expedia - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java Straightforward + Sqrt(n)
- Author: votrubac
- Creation Date: Sun Jun 28 2020 00:02:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 28 2020 04:55:29 GMT+0800 (Singapore Standard Time)

<p>
Just check the divisibility and count; note that we only go till `n / 2`. In the end, if `k == 1`, then the last divisor is our number `n` itself.

The code bellow is good for both C++ and Java.

```cpp
int kthFactor(int n, int k) {
    for (int d = 1; d <= n / 2; ++d)
        if (n % d == 0 && --k == 0)
            return d;
    return k == 1 ? n : -1;
}
```

Observing that `d` produces two divisors - `d` and `n / d`, we can improve the solution above to run in `sqrt(n)` time. To make it simple, I am using two loops. Note that we need to skip a divisor in the second loop if `n * n == d`.
```cpp
int kthFactor(int n, int k) {
    int d = 1;
    for (; d * d <= n; ++d)
        if (n % d == 0 && --k == 0)
            return d;
    for (d = d - 1; d >= 1; --d) {
        if (d * d == n)
            continue;
        if (n % d == 0 && --k == 0)
            return n / d;
    }
    return -1;
}
```
**Complexity Analysis**
- Time: O(n) or O(sqrt n) for the second approach.
- Memory: O(1).
</p>


### Java simple Loop O(sqrt(n))
- Author: hobiter
- Creation Date: Sun Jun 28 2020 02:25:51 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 15 2020 13:00:22 GMT+0800 (Singapore Standard Time)

<p>
Just loop until square root of n, by caching the other facters to a list.
i will track factors from 1, List will cache factors from n, then converge to sqrt(n);
just try count the kth, based on above logic.
```
    public int kthFactor(int n, int k) {
        int cnt = 0;
        List<Integer> l = new ArrayList<>();
        for(int i = 1; i * i <= n; i++) {
            if (n % i == 0) {
                if (i * i != n) l.add(n / i);
                if (++cnt == k) return i;
            }
        }
        if (l.size() + cnt < k) return -1;
        return l.get(l.size() - (k - cnt));
    }
```
</p>


### [Java/Python 3] 2 modular operation codes, O(n) and O(n ^ 0.5) w/ brief explanation and analysis.
- Author: rock
- Creation Date: Sun Jun 28 2020 00:13:30 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 29 2020 00:30:25 GMT+0800 (Singapore Standard Time)

<p>
**O(n) codes:**
From 1 to n, check if it is a factor of n: 
1. if yes, decrease `k` by `1` and when `k` reaches 0, return the current factor; 
2. if `k` never reaches `0`, return -1.
```java
    public int kthFactor(int n, int k) {
        for (int factor = 1; factor <= n; ++factor) {
            if (n % factor == 0 && --k == 0) {
                return factor;
            }
        }
        return -1;
    }
```
```python
    def kthFactor(self, n: int, k: int) -> int:
        for factor in range(1, n + 1):
            if n % factor == 0:
                k -= 1
                if k == 0:
                    return factor
        return -1
```
**Analysis:**

Time: O(n), space: O(1).

----

**O(n ^ 0.5) codes:**
1. Use a List to store all factors less than `n ^ 0.5`; decrease `k` by `1` whenever finding a factor; return the factor if `k` reaches `0`;
2. If not found in 1, check if the factor list is shorter than `k`: if yes, return `-1`; otherwise, use the `kth` factor from the last to divide `n` to get the correct factor.
3. **Note:** if the square root of `n` is among the factors, do NOT put it into factor list in order to avoid duplicate factors.

```java
    public int kthFactor(int n, int k) {
        List<Integer> factorList = new ArrayList<>();
        for (int factor = 1; factor * factor <= n; ++factor) {
            if (n % factor == 0) {
                if (factor * factor != n) {
                    factorList.add(factor);
                }
                if (--k == 0) {
                    return factor;
                }
            }
        }
        int size = factorList.size();
        return k > size ? -1 : n / factorList.get(size - k);
    }
```
```python
    def kthFactor(self, n: int, k: int) -> int:
        factor_list = []
        for factor in range(1, int(math.sqrt(n)) + 1):
            if n % factor == 0:
                if factor ** 2 != n:
                    factor_list.append(factor)
                k -= 1
                if k == 0:
                    return factor
        return -1 if k > len(factor_list) else n // factor_list[-k]        
```
**Analysis:**

Time & space: O(n ^ 0.5).

----

**Further discussion:** - credit to **@Ntt150292**

We can reduce the space to O(1) with 2 passes, still time O(log n).

The first pass, factor goes from `1 -> int(n**0.5)`. If `n % factor == 0` and `k == 0` then return factor
The second pass, factor goes from `int(n**0.5) -> 1`. If `n % factor == 0` and `k == 0` then return `n / factor`.
</p>


