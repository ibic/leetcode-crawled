---
title: "Number of Distinct Islands II"
weight: 632
#id: "number-of-distinct-islands-ii"
---
## Description
<div class="description">
<p>Given a non-empty 2D array <code>grid</code> of 0's and 1's, an <b>island</b> is a group of <code>1</code>'s (representing land) connected 4-directionally (horizontal or vertical.)  You may assume all four edges of the grid are surrounded by water.</p>

<p>Count the number of <b>distinct</b> islands.  An island is considered to be the same as another if they have the same shape, or have the same shape after <b>rotation</b> (90, 180, or 270 degrees only) or <b>reflection</b> (left/right direction or up/down direction).</p>

<p><b>Example 1:</b><br />
<pre>
11000
10000
00001
00011
</pre>
Given the above grid map, return <code>1</code>.
<br><br>
Notice that:
<pre>
11
1
</pre>
and
<pre>
 1
11
</pre>
are considered <b>same</b> island shapes. Because if we make a 180 degrees clockwise rotation on the first island, then two islands will have the same shapes.
</p>

<p><b>Example 2:</b><br />
<pre>
11100
10001
01001
01110</pre>
Given the above grid map, return <code>2</code>.<br />
<br>
Here are the two distinct islands:
<pre>
111
1
</pre>
and
<pre>
1
1
</pre>
<br />
Notice that:
<pre>
111
1
</pre>
and
<pre>
1
111
</pre>
are considered <b>same</b> island shapes. Because if we flip the first array in the up/down direction, then they have the same shapes.
</p>

<p><b>Note:</b>
The length of each dimension in the given <code>grid</code> does not exceed 50.
</p>
</div>

## Tags
- Hash Table (hash-table)
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Canonical Hash [Accepted]

**Intuition**

As in *Approach #1* to the sister problem [Number of Distinct Islands](https://leetcode.com/articles/number-of-distinct-islands/), we determine local coordinates for each island.

Afterwards, we will rotate and reflect the coordinates about the origin and translate the shape so that the bottom-left-most coordinate is (0, 0).  At the end, the smallest of these lists coordinates will be the *canonical representation* of the shape.

**Algorithm**

We feature two different implementations, but the core idea is the same.  We start with the code from the previous problem, *Number of Distinct Islands*.

For each of 8 possible rotations and reflections of the shape, we will perform the transformation and then translate the shape so that the bottom-left-most coordinate is (0, 0).  Afterwards, we will consider the canonical hash of the shape to be the maximum of these 8 intermediate hashes.

In Python, the motivation to use complex numbers is that rotation by 90 degrees is the same as multiplying by the imaginary unit, `1j`.  In Java, we manipulate the coordinates directly.  The 8 rotations and reflections of each point are `(x, y), (-x, y), (x, -y), (-x, -y), (y, x), (-y, x), (y, -x), (-y, -x)`.

**Python**
```python
class Solution(object):
    def numDistinctIslands2(self, grid):
        seen = set()
        def explore(r, c):
            if (0 <= r < len(grid) and 0 <= c < len(grid[0]) and
                    grid[r][c] and (r, c) not in seen):
                seen.add((r, c))
                shape.add(complex(r, c))
                explore(r+1, c)
                explore(r-1, c)
                explore(r, c+1)
                explore(r, c-1)

        def canonical(shape):
            def translate(shape):
                w = complex(min(z.real for z in shape),
                            min(z.imag for z in shape))
                return sorted(str(z-w) for z in shape)

            ans = None
            for k in xrange(4):
                ans = max(ans, translate([z * (1j)**k for z in shape]))
                ans = max(ans,  translate([complex(z.imag, z.real) * (1j)**k
                                           for z in shape]))
            return tuple(ans)

        shapes = set()
        for r in range(len(grid)):
            for c in range(len(grid[0])):
                shape = set()
                explore(r, c)
                if shape:
                    shapes.add(canonical(shape))

        return len(shapes)
```

**Java**

```java
class Solution {
    int[][] grid;
    boolean[][] seen;
    ArrayList<Integer> shape;

    public void explore(int r, int c) {
        if (0 <= r && r < grid.length && 0 <= c && c < grid[0].length &&
                grid[r][c] == 1 && !seen[r][c]) {
            seen[r][c] = true;
            shape.add(r * grid[0].length + c);
            explore(r+1, c);
            explore(r-1, c);
            explore(r, c+1);
            explore(r, c-1);
        }
    }

    public String canonical(ArrayList<Integer> shape) {
        String ans = "";
        int lift = grid.length + grid[0].length;
        int[] out = new int[shape.size()];
        int[] xs = new int[shape.size()];
        int[] ys = new int[shape.size()];

        for (int c = 0; c < 8; ++c) {
            int t = 0;
            for (int z: shape) {
                int x = z / grid[0].length;
                int y = z % grid[0].length;
                //x y, x -y, -x y, -x -y
                //y x, y -x, -y x, -y -x
                xs[t] = c<=1 ? x : c<=3 ? -x : c<=5 ? y : -y;
                ys[t++] = c<=3 ? (c%2==0 ? y : -y) : (c%2==0 ? x : -x);
            }

            int mx = xs[0], my = ys[0];
            for (int x: xs) mx = Math.min(mx, x);
            for (int y: ys) my = Math.min(my, y);

            for (int j = 0; j < shape.size(); ++j) {
                out[j] = (xs[j] - mx) * lift + (ys[j] - my);
            }
            Arrays.sort(out);
            String candidate = Arrays.toString(out);
            if (ans.compareTo(candidate) < 0) ans = candidate;
        }
        return ans;
    }

    public int numDistinctIslands2(int[][] grid) {
        this.grid = grid;
        seen = new boolean[grid.length][grid[0].length];
        Set shapes = new HashSet<String>();

        for (int r = 0; r < grid.length; ++r) {
            for (int c = 0; c < grid[0].length; ++c) {
                shape = new ArrayList();
                explore(r, c);
                if (!shape.isEmpty()) {
                    shapes.add(canonical(shape));
                }
            }
        }

        return shapes.size();
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(R*C \log{(R*C)})$$, where $$R$$ is the number of rows in the given `grid`, and $$C$$ is the number of columns.  We visit every square once, and each square belongs to at most one shape.  The log factor comes from sorting the shapes.

* Space complexity: $$O(R*C)$$, the space used to keep track of the shapes.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Consise C++ solution, using DFS +sorting to find canonical representation for each island
- Author: imrusty
- Creation Date: Wed Oct 18 2017 01:19:55 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 04:04:16 GMT+0800 (Singapore Standard Time)

<p>
After we get coordinates for an island, compute all possible rotations/reflections (https://en.wikipedia.org/wiki/Dihedral_group)  of it and then sort them using the default comparison. The first representation in this order is then the canonical one.

```
class Solution {
public:
    map<int, vector<pair<int,int>>> mp;
    
    void dfs(int r, int c, vector<vector<int>> &g, int cnt) {
        if ( r < 0 || c < 0 || r >= g.size() || c >= g[0].size()) return;
        if (g[r][c] != 1) return;
        g[r][c] = cnt;
        mp[cnt].push_back({r,c});
        dfs(r+1,c,g,cnt);
        dfs(r-1,c,g,cnt);
        dfs(r,c+1,g,cnt);
        dfs(r,c-1,g,cnt);
    }
    
    vector<pair<int,int>> norm(vector<pair<int,int>> v) {
        vector<vector<pair<int,int>>> s(8);
        // compute rotations/reflections.
        for (auto p:v) {
            int x = p.first, y = p.second;
            s[0].push_back({x,y});
            s[1].push_back({x,-y});
            s[2].push_back({-x,y});
            s[3].push_back({-x,-y});
            s[4].push_back({y,-x});
            s[5].push_back({-y,x});
            s[6].push_back({-y,-x});
            s[7].push_back({y,x});
        }
        for (auto &l:s) sort(l.begin(),l.end());
        for (auto &l:s) {
            for (int i = 1; i < v.size(); ++i) 
                l[i] = {l[i].first-l[0].first, l[i].second - l[0].second};
            l[0] = {0,0};
        }
        sort(s.begin(),s.end());
        return s[0];
    }
    
    int numDistinctIslands2(vector<vector<int>>& g) {
        int cnt = 1;
        set<vector<pair<int,int>>> s;
        for (int i = 0; i < g.size(); ++i) for (int j = 0; j < g[i].size(); ++j) if (g[i][j] == 1) {
            dfs(i,j,g, ++cnt);
            s.insert(norm(mp[cnt]));
        }
        
        return s.size();
    }
};
```
</p>


### The problem is from IOI 98 Starry Night. 
- Author: SolaAoi
- Creation Date: Fri Apr 27 2018 16:36:24 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 12:08:33 GMT+0800 (Singapore Standard Time)

<p>

http://olympiads.win.tue.nl/ioi/ioi98/contest/day1/starry/starry.html

```
import java.util.*;

class Solution {
    private static final int dr[] = {1, -1, 0, 0};
    private static final int dc[] = {0, 0, 1, -1};
    private static final int INF = 10000;

    private int left = INF;
    private int right = 0;
    private int top = INF;
    private int bottom = 0;

    private void markIsland(int[][] grid, int row, int col, int id) {
        left = Math.min(left, col);
        right = Math.max(right, col);
        top = Math.min(top, row);
        bottom = Math.max(bottom, row);

        grid[row][col] = id;
        for (int i = 0; i < 4; ++i) {
            int nr = row + dr[i];
            int nc = col + dc[i];
            if (nr >= 0 && nr < grid.length && nc >= 0 && nc < grid[0].length && grid[nr][nc] == 1) {
                markIsland(grid, nr, nc, id);
            }
        }
    }

    private int[][] cutIsland(int[][] grid, int r1, int c1, int r2, int c2, int id) {
        int[][] res = new int[r2 - r1 + 1][c2 - c1 + 1];
        for (int i = r1; i <= r2; ++i) {
            for (int j = c1; j <= c2; ++j) {
                if (grid[i][j] == id) {
                    res[i - r1][j - c1] = 1;
                }
            }
        }
        return res;
    }

    private int[][] rotate90(int[][] grid) {
        int row = grid.length;
        int col = grid[0].length;

        int[][] res = new int[col][row];
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; ++j) {
                res[j][row - i - 1] = grid[i][j];
            }
        }
        return res;
    }

    private int[][] upDown(int[][] grid) {
        int row = grid.length;
        int col = grid[0].length;

        int[][] res = new int[row][col];
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; ++j) {
                res[row - i - 1][j] = grid[i][j];
            }
        }
        return res;
    }

    private int[][] leftRight(int[][] grid) {
        int row = grid.length;
        int col = grid[0].length;

        int[][] res = new int[row][col];
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; ++j) {
                res[i][col - j - 1] = grid[i][j];
            }
        }
        return res;
    }

    boolean same(int[][] g1, int[][] g2) {
        if (g1.length != g2.length || g1[0].length != g2[0].length) {
            return false;
        }

        for (int i = 0; i < g1.length; ++i) {
            for (int j = 0; j < g1[0].length; ++j) {
                if (g1[i][j] != g2[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    List<int[][]> transform(int[][] orig) {
        List<int[][]> trans = new ArrayList<>();
        trans.add(orig);
        trans.add(rotate90(trans.get(trans.size() - 1)));
        trans.add(rotate90(trans.get(trans.size() - 1)));
        trans.add(rotate90(trans.get(trans.size() - 1)));
        trans.add(upDown(orig));
        trans.add(rotate90(trans.get(trans.size() - 1)));
        trans.add(rotate90(trans.get(trans.size() - 1)));
        trans.add(rotate90(trans.get(trans.size() - 1)));
        trans.add(leftRight(orig));
        trans.add(rotate90(trans.get(trans.size() - 1)));
        trans.add(rotate90(trans.get(trans.size() - 1)));
        trans.add(rotate90(trans.get(trans.size() - 1)));
        return trans;
    }

    public int numDistinctIslands2(int[][] grid) {
        int id = 2;

        int row = grid.length;
        int col = grid[0].length;

        List<int[][]> islands = new ArrayList<>();
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; ++j) {
                if (grid[i][j] == 1) {
                    left = INF; right = 0; top = INF; bottom = 0;
                    markIsland(grid, i, j, id);
                    islands.add(cutIsland(grid, top, left, bottom, right, id));
                    ++id;
                }
            }
        }

        int sz = islands.size();
        boolean[] vis = new boolean[sz];

        int res = 0;
        for (int i = 0; i < sz; ++i) {
            if (vis[i] == false) {
                ++res;

                List<int[][]> trans = transform(islands.get(i));
                int num = trans.size();
                for (int j = i + 1; j < sz; ++j) {
                    for (int k = 0; k < num; ++k) {
                        if (same(islands.get(j), trans.get(k))) {
                            vis[j] = true;
                            break;
                        }
                    }
                }
            }
        }

        return res;
    }
}

```
</p>


### after rotation or/A~~~N~~~D~~~ reflection!
- Author: iaming
- Creation Date: Wed Nov 01 2017 08:18:20 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 11:24:54 GMT+0800 (Singapore Standard Time)

<p>
after rotation or reflection
is different from 
after rotation or/and reflection
!!!
</p>


