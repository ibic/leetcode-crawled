---
title: "Number of Sub-arrays of Size K and Average Greater than or Equal to Threshold"
weight: 1239
#id: "number-of-sub-arrays-of-size-k-and-average-greater-than-or-equal-to-threshold"
---
## Description
<div class="description">
<p>Given an array of integers <code>arr</code> and two integers <code>k</code> and <code>threshold</code>.</p>

<p>Return <em>the number of sub-arrays</em> of size <code>k</code> and average greater than or equal to <code>threshold</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [2,2,2,2,5,5,5,8], k = 3, threshold = 4
<strong>Output:</strong> 3
<strong>Explanation:</strong> Sub-arrays [2,5,5],[5,5,5] and [5,5,8] have averages 4, 5 and 6 respectively. All other sub-arrays of size 3 have averages less than 4 (the threshold).
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,1,1,1,1], k = 1, threshold = 0
<strong>Output:</strong> 5
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [11,13,17,23,29,31,7,5,2,3], k = 3, threshold = 5
<strong>Output:</strong> 6
<strong>Explanation:</strong> The first 6 sub-arrays of size 3 have averages greater than 5. Note that averages are not integers.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> arr = [7,7,7,7,7,7,7], k = 7, threshold = 7
<strong>Output:</strong> 1
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> arr = [4,4,4,4], k = 4, threshold = 1
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 10^4</code></li>
	<li><code>1 &lt;= k &lt;= arr.length</code></li>
	<li><code>0 &lt;= threshold &lt;= 10^4</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- LinkedIn - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] 2 codes: Prefix sum and sliding window w/ analysis.
- Author: rock
- Creation Date: Sun Feb 09 2020 00:04:16 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 21 2020 01:32:20 GMT+0800 (Singapore Standard Time)

<p>
**Prefix Sum:**

```java
    public int numOfSubarrays(int[] a, int k, int threshold) {
        int n = a.length, count = 0;
        int[] prefixSum = new int[n + 1];
        for (int i = 0; i < n; ++i)
            prefixSum[i + 1] = prefixSum[i] + a[i];
        for (int i = 0; i + k <= n; ++i)
            if (prefixSum[i + k] - prefixSum[i] >= k * threshold) 
                ++count;
        return count;        
    }
```
```python
    def numOfSubarrays(self, a: List[int], k: int, threshold: int) -> int:
        prefixSum = [0]
        for i in a:
            prefixSum.append(i + prefixSum[-1])
        return sum(prefixSum[i + k] - prefixSum[i] >= k * threshold for i in range(len(a) - k + 1))
```

**Analysis:**

Time & space: O(n), where n = a.length.

----

**Sliding Window:**

```java
    public int numOfSubarrays(int[] a, int k, int threshold) {
        int count = 0;
        for (int lo = -1, hi = 0, sumOfWin = 0, target = k * threshold; hi < a.length; ++hi) {
            sumOfWin += a[hi];
            if (hi - lo == k) { // Has the width of the window  reached k?
                if (sumOfWin >= target) {
                    ++count;
                }
                sumOfWin -= a[++lo];
            }
        }
        return count;        
    }
```
```python
    def numOfSubarrays(self, a: List[int], k: int, threshold: int) -> int:
        lo, sum_of_win, cnt, target = -1, 0, 0, k * threshold
        for hi, v in enumerate(a):
            sum_of_win += v
            if hi - lo == k:
                if sum_of_win >= target:
                    cnt += 1
                lo += 1   
                sum_of_win -= a[lo]
        return cnt
```
**Analysis:**

Time: O(n),  space: O(1), where n = a.length.


</p>


### Java O(n) sliding window, clean and straigthtforward
- Author: hobiter
- Creation Date: Tue Feb 18 2020 15:48:03 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 03 2020 03:30:22 GMT+0800 (Singapore Standard Time)

<p>
use sum other than average for comparison
```
    public int numOfSubarrays(int[] arr, int k, int threshold) {
        int t = k * threshold, s = 0, cnt = 0;
        for (int i = 0; i < arr.length; i++) {
            s += arr[i];
            if (i < k - 1) continue;
            if (i > k - 1) s -= arr[i - k];
            if (s >= t) cnt++;
        }
        return cnt;
    }
```
</p>


### C++ Short Solution !!!
- Author: kylewzk
- Creation Date: Sun Feb 09 2020 14:39:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 09 2020 14:39:27 GMT+0800 (Singapore Standard Time)

<p>
```
    int numOfSubarrays(vector<int>& a, int k, int threshold) {
        int count = 0, sum = 0, t = k*threshold;
        for(int i = 0; i < a.size(); i++) {
            sum += a[i];
            if(i >= k) sum -= a[i-k];
            if(i >= k-1 && sum >= t) count++;
        }
        return count;
    }
```
</p>


