---
title: "Range Addition"
weight: 353
#id: "range-addition"
---
## Description
<div class="description">
<p>Assume you have an array of length <b><i>n</i></b> initialized with all <b>0</b>&#39;s and are given <b><i>k</i></b> update operations.</p>

<p>Each operation is represented as a triplet: <b>[startIndex, endIndex, inc]</b> which increments each element of subarray <b>A[startIndex ... endIndex]</b> (startIndex and endIndex inclusive) with <b>inc</b>.</p>

<p>Return the modified array after all <b><i>k</i></b> operations were executed.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input: </strong>length = <span id="example-input-1-1">5</span>, updates = <span id="example-input-1-2">[[1,3,2],[2,4,3],[0,2,-2]]</span>
<strong>Output: </strong><span id="example-output-1">[-2,0,3,5,3]</span>
</pre>

<p><b>Explanation:</b></p>

<pre>
Initial state:
[0,0,0,0,0]

After applying operation [1,3,2]:
[0,2,2,2,0]

After applying operation [2,4,3]:
[0,2,5,5,3]

After applying operation [0,2,-2]:
[-2,0,3,5,3]
</pre>
</div>

## Tags
- Array (array)

## Companies
- Google - 3 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Naïve Approach

**Algorithm**

The algorithm is trivial. For each update query, we iterate over the required update range and update each element individually.


Each query of `updates` is a tuple of 3 integers: $$start, end$$ (the start and end indexes for the update range) and $$val$$ (the amount by which each array element in this range must be incremented).

<iframe src="https://leetcode.com/playground/5yopnQJa/shared" frameBorder="0" width="100%" height="293" name="5yopnQJa"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n \cdot k)$$ (worst case) where $$k$$ is the number of update queries and $$n$$ is the length of the array. Each of the $$k$$ update operations take up $$O(n)$$ time (worst case, when all updates are over the entire range).

* Space complexity : $$O(1)$$. No extra space required.
<br>
<br>

---
#### Approach 2: Range Caching

**Intuition**

* There is only one read query on the entire range, and it occurs at the end of all update queries. Additionally, the order of processing update queries is irrelevant.

* Cumulative sums or `partial_sum` operations apply the effects of past elements to the future elements in the sequence.

**Algorithm**

The algorithm makes use of the above intuition to simply store changes at the *borders* of the update ranges (instead of processing the entire range). Finally a single post processing operation is carried out over the entire output array.

The two steps that are required are as follows:

1. For each update query $$(start, end, val)$$ on the array $$arr$$, we need to do *only* two operations:

    * Update $$start$$ boundary of the range:

    $$
    arr_{start} = arr_{start} + val
    $$

    * Update just beyond the $$end$$ boundary of the range:

    $$
    arr_{end+1} = arr_{end+1} - val
    $$

2. Final Transformation. The cumulative sum of the entire array is taken (`0` - based indexing)

    $$
    arr_i = arr_i + arr_{i-1} \quad \forall \quad i \in [1, n)
    $$


<iframe src="https://leetcode.com/playground/qZVkkbBN/shared" frameBorder="0" width="100%" height="446" name="qZVkkbBN"></iframe>

**Formal Explanation**

For each update query $$(start, end, val)$$ on the array $$arr$$, the goal is to achieve the result:

$$
arr_i = arr_i + val \quad \forall \quad i \in [start, end]
$$

Applying the final transformation, ensures two things:

1. It carries over the $$+val$$ increment over to every element $$ arr_i \; \forall \; i \ge start $$.
2. It carries over the $$-val$$ increment (equivalently, a $$+val$$ decrement) over to every element $$ arr_j \; \forall \; j \gt end $$.

The net result is that:

$$
\begin{aligned}
arr_i &= arr_i + val  \quad && \forall \quad i \in [start, end] \\
arr_j &= arr_j + val - val = arr_j  \quad && \forall \quad i \in (end, length)
\end{aligned}
$$

which meets our end goal. It is easy to see that the updates over a range did not carry over beyond it due to the compensating effect of the $$-val$$ increment over the $$+val$$ increment.

It is good to note that this works for multiple update queries because the particular binary operations here (namely addition and subtraction):

* are *closed* over the entire domain of `Integer`s. (A counter example is division which is not closed over all `Integer`s).

* are *complementary* operations. (As a counter example multiplication and division are not always complimentary due to possible loss of precision when dividing `Integer`s).

**Complexity Analysis**

* Time complexity : $$O(n + k)$$. Each of the $$k$$ update operations is done in constant $$O(1)$$ time. The final cumulative sum transformation takes $$O(n)$$ time always.

* Space complexity : $$O(1)$$. No extra space required.
<br>
<br>

---
## Further Thoughts

An extension of this problem is to apply such updates on an array where all elements are **not** the same.

In this case, the second approach requires that the original configuration must be stored separately before applying the final transformation. This incurs an additional space complexity of $$O(n)$$.

[@StefanPochmann](https://leetcode.com/StefanPochmann/) suggested another method (see comment section) which does not require extra space, but requires an extra linear pass over the entire array. The idea is to apply *reverse* `partial_sum` operation on the array (for example, array $$[2, 3, 10, 5]$$ transforms to $$[2, 1, 7, -5]$$) as an initialization step and then proceed with the second method as usual.

---

Another general, more complex version of this problem comprises of multiple read and update queries over *ranges*. Such problems can be solved quite efficiently with [Segment Trees by applying a popular trick called **Lazy Propogation**.](https://leetcode.com/articles/a-recursive-approach-to-segment-trees-range-sum-queries-lazy-propagation/)

Analysis written by [@babhishek21](https://leetcode.com/babhishek21).

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(K + N)time complexity Solution
- Author: lid004
- Creation Date: Wed Jun 29 2016 15:00:48 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 00:34:23 GMT+0800 (Singapore Standard Time)

<p>
Just store every start index for each value and at end index plus one minus it 

for example it will look like:

[1 , 3 , 2]   ,  [2, 3,  3]  (length = 5)

res[ 0, 2, ,0, 0 -2 ]

res[ 0 ,2, 3, 0, -5]

sum 0, 2, 5, 5, 0

res[0, 2, 5, 5, 0] 



     public int[] getModifiedArray(int length, int[][] updates) {

        int[] res = new int[length];
         for(int[] update : updates) {
            int value = update[2];
            int start = update[0];
            int end = update[1];
            
            res[start] += value;
            
            if(end < length - 1)
                res[end + 1] -= value;
            
        }
        
        int sum = 0;
        for(int i = 0; i < length; i++) {
            sum += res[i];
            res[i] = sum;
        }
        
        return res;
    }
</p>


### Detailed explanation if you don't understand, especially "put negative inc at [endIndex+1]"
- Author: ivan2
- Creation Date: Thu Aug 04 2016 00:09:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 20:38:45 GMT+0800 (Singapore Standard Time)

<p>
You may see many of the elegant solutions (such as [this solution](https://discuss.leetcode.com/topic/49666/my-simple-c-solution)) that **puts inc at startIndex and -inc at endIndex + 1**, but it might take you a while to understand why it works, if you are still stuck, read on.

The idea seems tricky at first look but is actually simple after you understand it, basically we want to achieve the final result array in two passes:
1. Iterate through the k update operations and "somehow" mark them in the [0, 0, 0, 0, 0] array (using length 5 for example), for each operation, only update startIndex and endIndex + 1. this is O(k) in total.
2. iterate through the marked array and "somehow" transforms it to the final result array. this is O(n) in total (n = length).
All in all it is O(n + k).

Now think in a simpler way first, if you have **only one update operation**, suppose input is (n = 5, updates = { {1, 3, 2} }), what does the O(n + k) solution do?

* Initialize the result array as length of n + 1, because we will operate on endIndex + 1: 
result = [0, 0, 0, 0, 0, 0]
* Then marks index 1 as 2 and marks index 3+1 as -2: 
result = [0, 2, 0, 0, -2, 0]
* Next, iterate through result, and accumulates previous sum to current position, just like [303. Range Sum Query - Immutable](https://leetcode.com/problems/range-sum-query-immutable/): 
result = [0, 0 + 2, 0 + 2, 0 + 2, 2 + (-2), 0] = [0, 2, 2, 2, 0, 0]
* Finally, trivial work to discard the last element because we don't need it: 
result = [0, 2, 2, 2, 0], which is the final result.

Now you might see why we do "puts inc at startIndex and -inc at endIndex + 1":
* Put inc at startIndex allows the **inc to be carried** to the next index starting from startIndex when we do the sum accumulation.
* Put -inc at endIndex + 1 simply means **cancel out the previous carry** from the next index of the endIndex, because the previous carry should not be counted beyond endIndex.

And finally, because each of the update operation is independent and the list operation is just an accumulation of the "marks" we do, so it can be "makred" all at once first and do the range sum at one time at last step.

Code in c++ is simply:
```
vector<int> getModifiedArray(int length, vector<vector<int>>& updates) {
    vector<int> result(length + 1, 0);
    for (auto &e : updates) {
        result[e[0]] += e[2];
        result[e[1] + 1] -= e[2];
    }
    
    // do range sum
    for (int i = 1; i < length; ++ i) {
        result[i] += result[i - 1];
    }
    
    result.pop_back();
    return result;
}
```
</p>


### My Simple C++ Solution
- Author: grandyang
- Creation Date: Wed Jun 29 2016 12:32:10 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 29 2016 12:32:10 GMT+0800 (Singapore Standard Time)

<p>
From the hint, we only need to update first and end element, so we update the startIndex with inc, then update endIndex + 1 with -inc. 
Using the example in the problem, We get vector nums = {-2, 2, 3, 2, -2, -3}, then we compute range sum ( [Range Sum Query - Immutable][1]), that is the final result = {-2, 0, 3, 5, 3}. 

    class Solution {
    public:
        vector<int> getModifiedArray(int length, vector<vector<int>>& updates) {
            vector<int> res, nums(length + 1, 0);
            for (int i = 0; i < updates.size(); ++i) {
                nums[updates[i][0]] += updates[i][2];
                nums[updates[i][1] + 1] -= updates[i][2];
            }
            int sum = 0;
            for (int i = 0; i < length; ++i) {
                sum += nums[i];
                res.push_back(sum);
            }
            return res;
        }
    };


  [1]: https://leetcode.com/problems/range-sum-query-immutable/
</p>


