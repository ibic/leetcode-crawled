---
title: "Pseudo-Palindromic Paths in a Binary Tree"
weight: 1341
#id: "pseudo-palindromic-paths-in-a-binary-tree"
---
## Description
<div class="description">
<p>Given a binary tree where node values are digits from 1 to 9. A path in the binary tree is said to be <strong>pseudo-palindromic</strong> if at least one permutation of the node values in the path is a palindrome.</p>

<p><em>Return the number of <strong>pseudo-palindromic</strong> paths going from the root node to leaf nodes.</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/05/06/palindromic_paths_1.png" style="width: 300px; height: 201px;" /></p>

<pre>
<strong>Input:</strong> root = [2,3,1,3,1,null,1]
<strong>Output:</strong> 2 
<strong>Explanation:</strong> The figure above represents the given binary tree. There are three paths going from the root node to leaf nodes: the red path [2,3,3], the green path [2,1,1], and the path [2,3,1]. Among these paths only red path and green path are pseudo-palindromic paths since the red path [2,3,3] can be rearranged in [3,2,3] (palindrome) and the green path [2,1,1] can be rearranged in [1,2,1] (palindrome).
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/05/07/palindromic_paths_2.png" style="width: 300px; height: 314px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [2,1,1,1,3,null,null,null,null,null,1]
<strong>Output:</strong> 1 
<strong>Explanation:</strong> The figure above represents the given binary tree. There are three paths going from the root node to leaf nodes: the green path [2,1,1], the path [2,1,3,1], and the path [2,1]. Among these paths only the green path is pseudo-palindromic since [2,1,1] can be rearranged in [1,2,1] (palindrome).
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [9]
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The&nbsp;given binary tree will have between <code>1</code> and <code>10^5</code> nodes.</li>
	<li>Node values are digits from <code>1</code> to <code>9</code>.</li>
</ul>

</div>

## Tags
- Bit Manipulation (bit-manipulation)
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] At most one odd occurrence
- Author: lee215
- Creation Date: Sun May 24 2020 12:19:26 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 27 2020 14:14:40 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
The necessar and suffisant condition of pseudo-palindromic,
is that at most one digit has odd occurrence.
<br>

# **Solution 1: Use Array**
Recursively iterate all paths from root to leaves,
count the occurrence of each digits in an **array**.
At the leaf node, check if at most one digit has odd occurrence.

Time `O(NK)` or `O(N)`
Space `O(K + H)`
where K is the number of different elements,
`H` is the height.
In this problem, `K = 9`
<br>

# **Solution 2: Use HashSet**
Recursively iterate all paths from root to leaves,
count the occurrence of each digits in an **hashset**.

Whenever meet an element, toggle it in the set:
If set contains it, remove it.
If set donesn\'t contain it, add it.

At the leaf node, check if the size of set <= 1.

Time `O(N)`
Space `O(K + H)`
<br>


# **Solution 3: Use an integer**
Recursively iterate all paths from root to leaves,
count the occurrence of each digits in an **integer**.

Use this integer as a bit mask.
Also c++, we can use bitmask directly.

Whenever meet an element,
toggle the corresponding bit using `^` operation.


At the leaf node,
check if the count has only one bit that is 1.

We use lowbit to help count this.
Google it if you don\'t know.

Time `O(N)`
Space `O(K + H)`
<br>

**Java:**
```java
    public int pseudoPalindromicPaths (TreeNode root) {
        return dfs(root, 0);
    }

    private int dfs(TreeNode root, int count) {
        if (root == null) return 0;
        count ^= 1 << (root.val - 1);
        int res = dfs(root.left, count) + dfs(root.right, count);
        if (root.left == root.right && (count & (count - 1)) == 0) res++;
        return res;
    }
```

**C++:**
```cpp
    int pseudoPalindromicPaths (TreeNode* root) {
        return dfs(root, 0);
    }

    int dfs(TreeNode* root, int count) {
        if (!root) return 0;
        count ^= 1 << (root->val - 1);
        int res = dfs(root->left, count) + dfs(root->right, count);
        if (root->left == root->right && (count & (count - 1)) == 0) res++;
        return res;
    }
```

**Python:**
```py
    def pseudoPalindromicPaths(self, root):

        def dfs(root, count=0):
            if not root: return 0
            count ^= 1 << (root.val - 1)
            res = dfs(root.left, count) + dfs(root.right, count)
            if root.left == root.right:
                if count & (count - 1) == 0:
                    res += 1
            return res
        return dfs(root)
```

</p>


### [C++] DFS + Bitvector
- Author: PhoenixDD
- Creation Date: Sun May 24 2020 12:01:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 24 2020 13:00:31 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
To check if any permutaion of numbers can make a palindrome all we need to do is check if we have even count of all numbers and at max one odd count (for odd palindromes with a center with single number). eg: 121.
We can easily do this by using a bit vector to store the parity of the counts of numbers (even or odd).
All we need to do now is maintain this bit vector while traversing down the tree in a dfs fashion and check at leaf nodes if we can create a palindrome.
Note that XOR operation comes in handy in such cases, to flip the bits.

**Solution**
```c++
class Solution {
public:
    int count=0;
    void dfs(TreeNode* root,int bitVec)
    {
        if(!root)
            return;
        if(!root->left&&!root->right)			//Leaf node.
        {
            count+=__builtin_popcount(bitVec^(1<<root->val))<=1;//Check if number of 1\'s in the bit vector is <=1 (Only 1 odd number).
            return;
        }
        dfs(root->left,bitVec^(1<<root->val)),dfs(root->right,bitVec^(1<<root->val));	//DFS to the left and right node and updating the bit vector
    }
    int pseudoPalindromicPaths (TreeNode* root)
    {
        dfs(root,0);
        return count;
    }
};
```
**Complexity**
Space: `O(1)` for bit vector, `O(h)` for recursion stack where `h` is the height of the tree.
Time `O(n)`.
</p>


### Palindrome Property Trick [Java] Solution Explained
- Author: Shradha1994
- Creation Date: Sun May 24 2020 12:16:38 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 26 2020 12:08:50 GMT+0800 (Singapore Standard Time)

<p>
Note that we maintain a count array of size 9 (numbers 1 to 9).
The array stores the count of each number in the tree path from root to leaf.

**Steps** - 
	- Traverse the tree from root to all the path. Normal tree traversal.
	Keep track of count of all the numbers in every path from root to leaf. 
	- When we reach the leaf, we have to check if current path is pseudo random palindrome or not.
**Trick**
- We know that in palindrome,every number occurs **even** number of times .
- But  in odd length palindrome, **only** one number can occur odd number of times.
We have used that property in **isPalindrome** method to check if numbers are pseudo palindrome or not.
```
class Solution {
    int result = 0;
    public int pseudoPalindromicPaths (TreeNode root) {
        int[] map = new int[10];
        findPesudoPalindromUtil(root,map);
        return result;
    }
    
    void findPesudoPalindromUtil(TreeNode root,int[] map){
       
        if(root == null){
            return;
        }
         map[root.val] = map[root.val]+1;
         if(root.left == null && root.right == null){
             if(isPalindrome(map))
                result++;
        }
       
        findPesudoPalindromUtil(root.left,map);
        findPesudoPalindromUtil(root.right,map);
        //backtrack
        map[root.val] = map[root.val]-1;
       
        
    }
    boolean isPalindrome(int[] map){
        int miss = 0;
        for(int i=0;i<=9;i++){
            if(map[i] % 2 != 0)
                miss++;
            if(miss > 1)
                return false;
        }
        return true;
    }
}
```
</p>


