---
title: "Binary Tree Longest Consecutive Sequence"
weight: 281
#id: "binary-tree-longest-consecutive-sequence"
---
## Description
<div class="description">
<p>Given a binary tree, find the length of the longest consecutive sequence path.</p>

<p>The path refers to any sequence of nodes from some starting node to any node in the tree along the parent-child connections. The longest consecutive path need to be from parent to child (cannot be the reverse).</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong>

   1
    \
     3
    / \
   2   4
        \
         5

<strong>Output:</strong> <code>3</code>

<strong>Explanation: </strong>Longest consecutive sequence path is <code>3-4-5</code><span style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;">, so return </span><code>3</code><span style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;">.</span></pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:

</strong>   2
    \
     3
    / 
   2    
  / 
 1

<strong>Output: 2 

Explanation: </strong>Longest consecutive sequence path is <code>2-3</code><span style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;">, not </span><code>3-2-1</code><span style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;">, so return </span><code>2</code><span style="font-family: sans-serif, Arial, Verdana, &quot;Trebuchet MS&quot;;">.</span></pre>
</div>

## Tags
- Tree (tree)

## Companies
- Google - 5 (taggedByAdmin: true)
- Facebook - 2 (taggedByAdmin: false)
- Amazon - 6 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach #1 (Top Down Depth-first Search) [Accepted]

**Algorithm**

A top down approach is similar to an in-order traversal. We use a variable `length` to store the current consecutive path length and pass it down the tree. As we traverse, we compare the current node with its parent node to determine if it is consecutive. If not, we reset the length.

```java
private int maxLength = 0;
public int longestConsecutive(TreeNode root) {
    dfs(root, null, 0);
    return maxLength;
}

private void dfs(TreeNode p, TreeNode parent, int length) {
    if (p == null) return;
    length = (parent != null && p.val == parent.val + 1) ? length + 1 : 1;
    maxLength = Math.max(maxLength, length);
    dfs(p.left, p, length);
    dfs(p.right, p, length);
}
```

@lightmark presents [a neat approach](https://leetcode.com/discuss/66486/short-and-simple-c-solution) without storing the maxLength as a global variable.

```java
public int longestConsecutive(TreeNode root) {
    return dfs(root, null, 0);
}

private int dfs(TreeNode p, TreeNode parent, int length) {
    if (p == null) return length;
    length = (parent != null && p.val == parent.val + 1) ? length + 1 : 1;
    return Math.max(length, Math.max(dfs(p.left, p, length),
                                     dfs(p.right, p, length)));
}
```

**Complexity analysis**

* Time complexity : $$O(n)$$.
The time complexity is the same as an in-order traversal of a binary tree with $$n$$ nodes.

* Space complexity : $$O(n)$$.
The extra space comes from implicit stack space due to recursion. For a skewed binary tree, the recursion could go up to $$n$$ levels deep.

---
#### Approach #2 (Bottom Up Depth-first Search) [Accepted]

**Algorithm**

The bottom-up approach is similar to a post-order traversal. We return the consecutive path length starting at current node to its parent. Then its parent can examine if its node value can be included in this consecutive path.

```java
private int maxLength = 0;
public int longestConsecutive(TreeNode root) {
    dfs(root);
    return maxLength;
}

private int dfs(TreeNode p) {
    if (p == null) return 0;
    int L = dfs(p.left) + 1;
    int R = dfs(p.right) + 1;
    if (p.left != null && p.val + 1 != p.left.val) {
        L = 1;
    }
    if (p.right != null && p.val + 1 != p.right.val) {
        R = 1;
    }
    int length = Math.max(L, R);
    maxLength = Math.max(maxLength, length);
    return length;
}
```

**Complexity analysis**

* Time complexity : $$O(n)$$.
The time complexity is the same as a post-order traversal in a binary tree, which is $$O(n)$$.

* Space complexity : $$O(n)$$.
The extra space comes from implicit stack space due to recursion. For a skewed binary tree, the recursion could go up to $$n$$ levels deep.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy Java DFS, is there better time complexity solution?
- Author: czonzhu
- Creation Date: Wed Oct 28 2015 23:48:28 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 11:46:03 GMT+0800 (Singapore Standard Time)

<p>
Just very intuitive depth-first search, send cur node value to the next level and compare it with the next level node.

    public class Solution {
        private int max = 0;
        public int longestConsecutive(TreeNode root) {
            if(root == null) return 0;
            helper(root, 0, root.val);
            return max;
        }
        
        public void helper(TreeNode root, int cur, int target){
            if(root == null) return;
            if(root.val == target) cur++;
            else cur = 1;
            max = Math.max(cur, max);
            helper(root.left, cur, root.val + 1);
            helper(root.right, cur, root.val + 1);
        }
    }
</p>


### Simple Recursive DFS without global variable
- Author: nightowl
- Creation Date: Tue Nov 10 2015 12:36:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 11:46:40 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public int longestConsecutive(TreeNode root) {
            return (root==null)?0:Math.max(dfs(root.left, 1, root.val), dfs(root.right, 1, root.val));
        }
        
        public int dfs(TreeNode root, int count, int val){
            if(root==null) return count;
            count = (root.val - val == 1)?count+1:1;
            int left = dfs(root.left, count, root.val);
            int right = dfs(root.right, count, root.val);
            return Math.max(Math.max(left, right), count);
        }
    }
</p>


### Don't understand what is consecutive sequence
- Author: hhhhdddd
- Creation Date: Fri Nov 06 2015 11:14:04 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 27 2018 05:16:31 GMT+0800 (Singapore Standard Time)

<p>
Hi,

Can anyone help me out?

I don't understand what does the path mean.
 

      2
        \
         3
        / 
       2    
      / 
     1

why 3-2-1 not a path?


THX!
</p>


