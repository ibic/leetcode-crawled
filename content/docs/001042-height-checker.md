---
title: "Height Checker"
weight: 1042
#id: "height-checker"
---
## Description
<div class="description">
<p>Students are asked to stand in non-decreasing order of heights for an annual photo.</p>

<p>Return the minimum number of students that must move in order for all students to be standing in non-decreasing order of height.</p>

<p>Notice that when a group of students is selected they can reorder in any possible way between themselves and the non selected students&nbsp;remain on their seats.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> heights = [1,1,4,2,1,3]
<strong>Output:</strong> 3
<strong>Explanation:</strong> 
Current array : [1,1,4,2,1,3]
Target array  : [1,1,1,2,3,4]
On index 2 (0-based) we have 4 vs 1 so we have to move this student.
On index 4 (0-based) we have 1 vs 3 so we have to move this student.
On index 5 (0-based) we have 3 vs 4 so we have to move this student.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> heights = [5,1,2,3,4]
<strong>Output:</strong> 5
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> heights = [1,2,3,4,5]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= heights.length &lt;= 100</code></li>
	<li><code>1 &lt;= heights[i] &lt;= 100</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Goldman Sachs - 12 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Salesforce - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Not a well defined problem
- Author: kris-randen
- Creation Date: Sun May 26 2019 12:10:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 26 2019 12:10:14 GMT+0800 (Singapore Standard Time)

<p>
If you consider the input 
[1,2,1,2,1,1,1,2,1]

LeetCode\'s solution (at least in today\'s contest) was 4 moves for the above problem. And it\'s coming from the sort and compare solution strategy or something similar. But the minimum number of student moves for them to be in order of non-decreasing height is 3. You move each 2 to the end of the array one by one and you can sort the array in 3 student moves. I think the definition should just be how many students are out of order.
</p>


### Java 0ms O(n) solution - no need to sort
- Author: fan_zh
- Creation Date: Tue May 28 2019 05:39:34 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 28 2019 05:40:23 GMT+0800 (Singapore Standard Time)

<p>
Just count the frequency of each height (using HashMap or int[] as the height is promised to be within range[1, 100]) and use 2 pointers to make comparison:

```
class Solution {
    public int heightChecker(int[] heights) {
        int[] heightToFreq = new int[101];
        
        for (int height : heights) {
            heightToFreq[height]++;
        }
        
        int result = 0;
        int curHeight = 0;
        
        for (int i = 0; i < heights.length; i++) {
            while (heightToFreq[curHeight] == 0) {
                curHeight++;
            }
            
            if (curHeight != heights[i]) {
                result++;
            }
            heightToFreq[curHeight]--;
        }
        
        return result;
    }
}
```
</p>


### Python 1-liner
- Author: cenkay
- Creation Date: Sun May 26 2019 12:01:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 26 2019 16:26:35 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def heightChecker(self, heights: List[int]) -> int:
        return sum(h1 != h2 for h1, h2 in zip(heights, sorted(heights)))
```
</p>


