---
title: "Smallest Range Covering Elements from K Lists"
weight: 565
#id: "smallest-range-covering-elements-from-k-lists"
---
## Description
<div class="description">
<p>You have <code>k</code> lists of sorted integers in <strong>non-decreasing&nbsp;order</strong>. Find the <b>smallest</b> range that includes at least one number from each of the <code>k</code> lists.</p>

<p>We define the range <code>[a, b]</code> is smaller than range <code>[c, d]</code> if <code>b - a &lt; d - c</code> <strong>or</strong> <code>a &lt; c</code> if <code>b - a == d - c</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [[4,10,15,24,26],[0,9,12,20],[5,18,22,30]]
<strong>Output:</strong> [20,24]
<strong>Explanation: </strong>
List 1: [4, 10, 15, 24,26], 24 is in range [20,24].
List 2: [0, 9, 12, 20], 20 is in range [20,24].
List 3: [5, 18, 22, 30], 22 is in range [20,24].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [[1,2,3],[1,2,3],[1,2,3]]
<strong>Output:</strong> [1,1]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [[10,10],[11,11]]
<strong>Output:</strong> [10,11]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [[10],[11]]
<strong>Output:</strong> [10,11]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> nums = [[1],[2],[3],[4],[5],[6],[7]]
<strong>Output:</strong> [1,7]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>nums.length == k</code></li>
	<li><code>1 &lt;= k &lt;= 3500</code></li>
	<li><code>1 &lt;= nums[i].length &lt;= 50</code></li>
	<li><code>-10<sup>5</sup> &lt;= nums[i][j] &lt;= 10<sup>5</sup></code></li>
	<li><code>nums[i]</code>&nbsp;is sorted in <strong>non-decreasing</strong> order.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Two Pointers (two-pointers)
- String (string)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Facebook - 5 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)
- Lyft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force [Time Limit Exceeded]

The naive approach is to consider every pair of elements, $$nums[i][j]$$ and $$nums[k][l]$$ from amongst the given 
lists and consider the range formed by these elements. For every range currently considered, we can traverse over all the 
lists to find if atleast one element from these lists can be included in the current range. If so, we store the end-points of the current range 
and compare it with the previous minimum range found, if any, satisfying the required criteria, to find the smaller range from among them.

Once all the element pairs have been considered as the ranges, we can obtain the required minimum range.

<iframe src="https://leetcode.com/playground/SMprvqVp/shared" frameBorder="0" name="SMprvqVp" width="100%" height="515"></iframe>


**Complexity Analysis**

* Time complexity : $$O(n^3)$$. Considering every possible range(element pair) requires $$O(n^2)$$ time. For each range considered, 
we need to traverse over all the elements of the given lists in the worst case requiring another $$O(n)$$ time. Here, $$n$$ refers to the 
total number of elements in the given lists.

* Space complexity : $$O(1)$$. Constant extra space is used.

---
#### Approach #2 Better Brute Force [Time Limit Exceeded]

**Algorithm**

In the last approach, we consider every possible range and then traverse over every list to check if atleast one of the 
elements from these lists lies in the required range. Instead of doing this traversal for every range, we can make use 
of Binary Search to find the index of the element just larger than(or equal to) the lower limit of the range currently 
considered. 

If all the elements in the current list are lesser than this lower limit, we'll get the index as $$nums[k].length$$
 for the $$k^{th}$$ list being currently checked. In this case, none of the elements of the current list lies in the
current range.

 On the other hand, if all the elements in this list are larger than this lower limit, we'll get the index of the first element(minimum) in the current list. If this element happens to be larger than the upper limit  of the range currently considered, then also, none of the elements of the current list lies within the current range.
 
 Whenever a range is found which satisfies the required criteria, we can compare it with the minimum range found so far 
 to determine the required minimum range.

<iframe src="https://leetcode.com/playground/uXxKqhkz/shared" frameBorder="0" name="uXxKqhkz" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(n^2log(k)\big)$$. The time required to consider every possible range is $$O(n^2)$$. For every range currently considered, 
a Binary Search requiring $$O\big(log(k)\big)$$ time is required. Here, $$n$$ refers to the total number of elements in the given 
lists and $$k$$ refers to the average length of each list.

* Space complexity : $$O(1)$$. Constant extra space is used.

---
#### Approach #3  Using Pointers [Time Limit Exceeded]

**Algorithm**

We'll discuss about the implementation used in the current approach along with the idea behind it. 

This approach makes use of an array of pointers, $$next$$, whose length is equal to the number of given lists. In this 
array, $$next[i]$$ refers to the element which needs to be considered next in the $$(i-1)^{th}$$ list. The meaning of this will become 
more clearer when we look at the process.

We start by initializing all the elements of $$next$$ to 0. Thus, currently, we are considering the first(minimum) element 
among all the lists. Now, we find out the index of the list containing the maximum($$max_i$$) and minimum($$min_i$$) 
elements from amongst the elements currently pointed by $$next$$. The range formed by these maximum and minimum elements surely  
contains atleast one element from each list. 

But, now our objective is to minimize this range. To do so, there are two options: Either decrease the maximum value or increase the 
minimum value. 

Now, the maximum value can't be reduced any further, since it already corresponds to the minimum value in one of the lists. 
Reducing it any further will lead to the exclusion of all the elements of this list(containing the last maximum value) 
from the new range. 

Thus, the only option left in our hand is to try to increase the minimum value. To do so, we now need to consider the
 next element in the list containing the last minimum value. Thus, we increment the entry at the corresponding index
  in $$next$$, to indicate that the next element in this list now needs to be considered. 
  
  Thus, at every step, we find the maximum and minimum values being pointed currently, update the $$next$$ values 
  appropriately, and also find out the range formed by these maximum and minimum values to find out the smallest range 
 satisfying the given criteria. 
 
 While doing this process, if any of the lists gets completely exhausted, it means that the minimum value being increased for 
 minimizing the range being considered can't be increased any further, without causing the exclusion of all the elements in atleast 
 one of the lists. Thus, we can stop the search process whenever any list gets completely exhausted.
 
 We can also stop the process, when all the elements of the given lists have been exhausted.
 
 Summarizing the statements above, the process becomes:
 
 1. Initialize $$next$$ array(pointers) with all 0's.
 
 2. Find the indices of the lists containing the minimum($$min_i$$) and the maximum($$max_i$$) elements amongst the elements pointed by the $$next$$ array.
 
 3. If the range formed by the maximum and minimum elements found above is larger than the previous maximum range, update the boundary values used for the maximum range.
 
 4. Increment the pointer $$nums[min_i]$$.
 
 5. Repeat steps 2 to 4 till any of the lists gets exhausted.
 
 The animation below illustrates the process for a visual understanding of the process.
 
 !?!../Documents/632_Smallest_Range.json:1000,563!?!


<iframe src="https://leetcode.com/playground/rnPo3vGZ/shared" frameBorder="0" name="rnPo3vGZ" width="100%" height="513"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n*m)$$. In the worst case, we need to traverse over $$next$$ array(of length $$m$$) for all the elements of the given lists.
Here, $$n$$ refers to the total number of elements in all the lists. $$m$$ refers to the total number of lists.

* Space complexity : $$O(m)$$. $$next$$ array of size $$m$$ is used.

---
#### Approach #4 Using Priority Queue [Accepted]:

**Algorithm**

In the last approach, at each step, we update the pointer corresponding to the current minimum element and traverse over the whole
$$next$$ array to determine the new maximum and minimum values. We can do some optimization here, by making use of a simple observation.

Whenever we update a single entry of $$next$$ to consider the new maximum and minimum values(if we already know the last maximum 
and minimum values), all the elements to be considered for finding the maximum and minimum values remain the same except the new element 
being pointed by a single updated entry in $$next$$.  This new entry is certainly larger than the last minimum value(since that was the 
reasoning behind the updation). 

Thus, we can't be sure whether this is the new minimum element or not. But, since it is larger than the last 
value being considered, it could be a potential competitor for the new maximum value. Thus, we can directly compare it with the last 
maximum value to determine the current maximum value.

Now, we're left with finding the minimum value iteratively at every step. To avoid this iterative process, a better idea 
is to make use of a Min-Heap, which stores the values being pointed currently by the $$next$$ array. Thus, the minimum value always 
lies at the top of this heap, and we need not do the iterative search process. 

At every step, we remove the minimum element from this heap and find out the range formed by the current maximum and minimum values, and 
compare it with the minimum range found so far to determine the required minimum range. We also update the increment the index in $$next$$ 
corresponding to the list containing this minimum entry and add this element to the heap as well.

The rest of the process remains the same as the last approach.

<iframe src="https://leetcode.com/playground/kBqfu7ju/shared" frameBorder="0" name="kBqfu7ju" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O\big(n*log(m)\big)$$. Heapification of $$m$$ elements requires $$O\big(log(m)\big)$$ time. This step could be done 
for all the elements of the given lists in the worst case. Here, $$n$$ refers to the total number of elements in 
all the lists. $$m$$ refers to the total number of lists.


* Space complexity : $$O(m)$$. $$next$$ array of size $$m$$ is used. A Min-Heap with $$m$$ elements is also used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python, Heap-based solution
- Author: awice
- Creation Date: Sun Jul 02 2017 13:11:59 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 03:58:15 GMT+0800 (Singapore Standard Time)

<p>
```
def smallestRange(self, A):
    pq = [(row[0], i, 0) for i, row in enumerate(A)]
    heapq.heapify(pq)
    
    ans = -1e9, 1e9
    right = max(row[0] for row in A)
    while pq:
        left, i, j = heapq.heappop(pq)
        if right - left < ans[1] - ans[0]:
            ans = left, right
        if j + 1 == len(A[i]):
            return ans
        v = A[i][j+1]
        right = max(right, v)
        heapq.heappush(pq, (v, i, j+1))
```

Keep a heap of the smallest elements.  As we pop element A[i][j], we'll replace it with A[i][j+1].  For each such element ```left```, we want ```right```, the maximum of the closest value in each row of the array that is ```>= left```, which is also equal to the current maximum of our heap.  We'll keep track of ```right``` as we proceed.

*Edited with thanks to @StefanPochmann*
</p>


### Java Code using PriorityQueue. similar to merge k array
- Author: helloworldzt
- Creation Date: Sun Jul 02 2017 11:22:21 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 22:48:51 GMT+0800 (Singapore Standard Time)

<p>
Image you are merging k sorted array using a heap. Then everytime you pop the smallest element out and add the next element of that array to the heap. By keep doing this, you will have the smallest range. 

```
public int[] smallestRange(int[][] nums) {
		PriorityQueue<Element> pq = new PriorityQueue<Element>(new Comparator<Element>() {
			public int compare(Element a, Element b) {
				return a.val - b.val;
			}
		});
		int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
		for (int i = 0; i < nums.length; i++) {
			Element e = new Element(i, 0, nums[i][0]);
			pq.offer(e);
			max = Math.max(max, nums[i][0]);
		}
		int range = Integer.MAX_VALUE;
		int start = -1, end = -1;
		while (pq.size() == nums.length) {

			Element curr = pq.poll();
			if (max - curr.val < range) {
				range = max - curr.val;
				start = curr.val;
				end = max;
			}
			if (curr.idx + 1 < nums[curr.row].length) {
				curr.idx = curr.idx + 1;
				curr.val = nums[curr.row][curr.idx];
				pq.offer(curr);
				if (curr.val > max) {
					max = curr.val;
				}
			}
		}

		return new int[] { start, end };
	}

	class Element {
		int val;
		int idx;
		int row;

		public Element(int r, int i, int v) {
			val = v;
			idx = i;
			row = r;
		}
	}
```
</p>


### Clean C++ priority_queue solution using iterators
- Author: Aeonaxx
- Creation Date: Sun Jul 02 2017 15:08:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 23:11:12 GMT+0800 (Singapore Standard Time)

<p>
Yes. The idea is just similar to Merge K Sorted List. Keep a priority queue of iterators/pointers which points to the current head of a row.

```
class Solution {
public:
    vector<int> smallestRange(vector<vector<int>>& nums) {
        typedef vector<int>::iterator vi;
        
        struct comp {
            bool operator()(pair<vi, vi> p1, pair<vi, vi> p2) {
                return *p1.first > *p2.first;
            }
        };
        
        int lo = INT_MAX, hi = INT_MIN;
        priority_queue<pair<vi, vi>, vector<pair<vi, vi>>, comp> pq;
        for (auto &row : nums) {
            lo = min(lo, row[0]);
            hi = max(hi, row[0]);
            pq.push({row.begin(), row.end()});
        }
        
        vector<int> ans = {lo, hi};
        while (true) {
            auto p = pq.top();
            pq.pop();
            ++p.first;
            if (p.first == p.second)
                break;
            pq.push(p);
            
            lo = *pq.top().first;
            hi = max(hi, *p.first);
            if (hi - lo < ans[1] - ans[0])
                ans = {lo, hi};
        }
        return ans;
    }
};
```
</p>


