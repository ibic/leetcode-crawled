---
title: "Delete Nodes And Return Forest"
weight: 1092
#id: "delete-nodes-and-return-forest"
---
## Description
<div class="description">
<p>Given the <code>root</code>&nbsp;of a binary tree, each node in the tree has a distinct value.</p>

<p>After deleting&nbsp;all nodes with a value in <code>to_delete</code>, we are left with a forest (a&nbsp;disjoint union of trees).</p>

<p>Return the roots of the trees in the remaining forest.&nbsp; You may return the result in any order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/07/01/screen-shot-2019-07-01-at-53836-pm.png" style="width: 237px; height: 150px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [1,2,3,4,5,6,7], to_delete = [3,5]
<strong>Output:</strong> [[1,2,null,4],[6],[7]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the given tree is at most <code>1000</code>.</li>
	<li>Each node has a distinct value between <code>1</code> and <code>1000</code>.</li>
	<li><code>to_delete.length &lt;= 1000</code></li>
	<li><code>to_delete</code> contains distinct values between <code>1</code> and <code>1000</code>.</li>
</ul>
</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Google - 15 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Recursion Solution
- Author: lee215
- Creation Date: Sun Jul 07 2019 12:02:02 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Nov 13 2019 22:50:36 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
As I keep saying in my video "courses",
solve tree problem with recursion first.
<br>

## **Explanation**
If a `node` is root (has no parent) and isn\'t deleted,
when will we add it to the `result`.
<br>

## **Complexity**
Time `O(N)`
Space `O(H + N)`, where `H` is the height of tree.
<br>

**Java:**
```java
    Set<Integer> to_delete_set;
    List<TreeNode> res;
    public List<TreeNode> delNodes(TreeNode root, int[] to_delete) {
        to_delete_set = new HashSet<>();
        res = new ArrayList<>();
        for (int i : to_delete)
            to_delete_set.add(i);
        helper(root, true);
        return res;
    }

    private TreeNode helper(TreeNode node, boolean is_root) {
        if (node == null) return null;
        boolean deleted = to_delete_set.contains(node.val);
        if (is_root && !deleted) res.add(node);
        node.left = helper(node.left, deleted);
        node.right =  helper(node.right, deleted);
        return deleted ? null : node;
    }
```

**C++:**
```cpp
    vector<TreeNode*> result;
    set<int> to_delete_set;
    vector<TreeNode*> delNodes(TreeNode* root, vector<int>& to_delete) {
        for (int i : to_delete)
            to_delete_set.insert(i);
        helper(root, result, to_delete_set, true);
        return result;
    }

    TreeNode* helper(TreeNode* node, vector<TreeNode*>& result, set<int>& to_delete_set, bool is_root) {
        if (node == NULL) return NULL;
        bool deleted = to_delete_set.find(node->val) != to_delete_set.end();
        if (is_root && !deleted) result.push_back(node);
        node->left = helper(node->left, result, to_delete_set, deleted);
        node->right =  helper(node->right, result, to_delete_set, deleted);
        return deleted ? NULL : node;
    }
```

**Python:**
```python
    def delNodes(self, root, to_delete):
        to_delete_set = set(to_delete)
        res = []

        def helper(root, is_root):
            if not root: return None
            root_deleted = root.val in to_delete_set
            if is_root and not root_deleted:
                res.append(root)
            root.left = helper(root.left, root_deleted)
            root.right = helper(root.right, root_deleted)
            return None if root_deleted else root
        helper(root, True)
        return res
```

</p>


### [Python] Recursion, with explanation 【Question seen in a 2016 interview】
- Author: kaiwensun
- Creation Date: Sun Jul 07 2019 12:02:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 07 2019 13:11:59 GMT+0800 (Singapore Standard Time)

<p>
### Question analysis
The question is composed of two requirements:
- To remove a node, the child need to **notify** its parent about the child\'s existance.
- To determine whether a node is a root node in the final forest, we need to know [1] whether the node is removed (which is trivial), and [2] whether its parent is removed (which requires the parent to **notify** the child)

It is very obvious that a tree problem is likely to be solved by recursion. The two components above are actually examining interviewees\' understanding to the two key points of **recursion**:
- passing info downwards -- by arguments
- passing info upwards -- by return value

---

### Fun fact
I\'ve seen this question in 2016 at a real newgrad interview. It was asked by a very well-known company and was a publicly known question at that time. I highly doubt that the company will ask this question again nowadays.

---

### Code [Python]

```
class Solution(object):
    def delNodes(self, root, to_delete):
        to_delete = set(to_delete)
        res = []
        def walk(root, parent_exist):
            if root is None:
                return None
            if root.val in to_delete:
                root.left = walk(root.left, parent_exist=False)
                root.right = walk(root.right, parent_exist=False)
                return None
            else:
                if not parent_exist:
                    res.append(root)
                root.left = walk(root.left, parent_exist=True)
                root.right = walk(root.right, parent_exist=True)
                return root
        walk(root, parent_exist=False)
        return res
```

</p>


### Simple Java Sol
- Author: Poorvank
- Creation Date: Sun Jul 07 2019 12:02:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 07 2019 12:02:29 GMT+0800 (Singapore Standard Time)

<p>
```
public List<TreeNode> delNodes(TreeNode root, int[] to_delete) {
        List<TreeNode> forest = new ArrayList<>();
        if (root == null) return forest;
        Set<Integer> set = new HashSet<>();
        for(int i : to_delete) {
            set.add(i);
        }
        deleteNodes(root, set, forest);
        if (!set.contains(root.val)) {
            forest.add(root);
        }
        return forest;
    }

    private TreeNode deleteNodes(TreeNode node, Set<Integer> set, List<TreeNode> forest) {
        if (node == null) return null;

        node.left = deleteNodes(node.left, set, forest);
        node.right = deleteNodes(node.right, set, forest);

        if (set.contains(node.val)) {
            if (node.left != null) forest.add(node.left);
            if (node.right != null) forest.add(node.right);
            return null;
        }

        return node;
    }
```
</p>


