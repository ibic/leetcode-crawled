---
title: "Find the Index of the Large Integer"
weight: 1416
#id: "find-the-index-of-the-large-integer"
---
## Description
<div class="description">
<p>We have an integer array <code>arr</code>, where all the integers in <code>arr</code> are equal except for one integer which is <strong>larger</strong> than the rest of the integers. You will not be given direct access to the array, instead, you will have an <strong>API</strong> <code>ArrayReader</code> which have the following functions:</p>

<ul>
	<li><code>int compareSub(int l, int r, int x, int y)</code>: where <code>0 &lt;= l, r, x, y &lt;&nbsp;ArrayReader.length()</code>, <code>l &lt;= r and</code>&nbsp;<code>x &lt;= y</code>. The function compares the sum of sub-array <code>arr[l..r]</code> with the sum of the sub-array <code>arr[x..y]</code> and returns:

	<ul>
		<li><strong>1</strong> if <code>arr[l]+arr[l+1]+...+arr[r] &gt; arr[x]+arr[x+1]+...+arr[y]</code>.</li>
		<li><strong>0</strong> if <code>arr[l]+arr[l+1]+...+arr[r] == arr[x]+arr[x+1]+...+arr[y]</code>.</li>
		<li><strong>-1</strong> if <code>arr[l]+arr[l+1]+...+arr[r] &lt; arr[x]+arr[x+1]+...+arr[y]</code>.</li>
	</ul>
	</li>
	<li><code>int length()</code>: Returns the size of the array.</li>
</ul>

<p>You are allowed to call&nbsp;<code>compareSub()</code>&nbsp;<b>20 times</b> at most. You can assume both functions work in <code>O(1)</code> time.</p>

<p>Return <em>the index of the array <code>arr</code> which has the largest integer</em>.</p>

<p><strong>Follow-up:</strong></p>

<ul>
	<li>What if there are two numbers in <code>arr</code> that are bigger than all other numbers?</li>
	<li>What if there is one number that is bigger than other numbers and one number that is smaller than other numbers?</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [7,7,7,7,10,7,7,7]
<strong>Output:</strong> 4
<strong>Explanation:</strong> The following calls to the API
reader.compareSub(0, 0, 1, 1) // returns 0 this is a query comparing the sub-array (0, 0) with the sub array (1, 1), (i.e. compares arr[0] with arr[1]).
Thus we know that arr[0] and arr[1] doesn&#39;t contain the largest element.
reader.compareSub(2, 2, 3, 3) // returns 0, we can exclude arr[2] and arr[3].
reader.compareSub(4, 4, 5, 5) // returns 1, thus for sure arr[4] is the largest element in the array.
Notice that we made only 3 calls, so the answer is valid.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [6,6,12]
<strong>Output:</strong> 2
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= arr.length&nbsp;&lt;= 5 * 10^5</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 100</code></li>
	<li>All elements of <code>arr</code> are equal except for one element which is larger than all other elements.</li>
</ul>

</div>

## Tags
- Binary Search (binary-search)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java/C++/C#/Python Binary Search
- Author: votrubac
- Creation Date: Thu Jul 30 2020 14:48:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 01 2020 11:21:59 GMT+0800 (Singapore Standard Time)

<p>
Just a regular binary search, with two caveats:
- The lengh of two halfs should be the same.
- Go "down" only if the biggest number is in the left half.
	- Otherwise, it can be in the right part (`-1`) or it\'s the last element in the case of odd intervals.
	- We can add a special check for `0` and exit earlier, but sorry - #minimalizm.

**Java (and also C++ and C#)**
Adding C# because there is a bug in C# code definition.

```cpp
// Pascal-case all functions for C#: GetIndex, Length, CompareSub
// int getIndex(ArrayReader &rd) { // use for C++
public int getIndex(ArrayReader rd) { // use for Java and C#
    int l = 0, r = rd.length() - 1;
    while (l < r) {
        int h = (r - l + 1) / 2; // half, h * 2 <= r - l + 1
        if (rd.compareSub(l, l + h - 1, l + h, l + h * 2 - 1) != 1)
            l = l + h;
        else
            r = l + h - 1;
    }
    return l;
}
```
**Python**
```python
class Solution:
    def getIndex(self, rd: \'ArrayReader\') -> int:
        l, r = 0, rd.length() - 1
        while l < r:
            h = (r - l + 1) // 2  # half, h * 2 <= r - l + 1
            if rd.compareSub(l, l + h - 1, l + h, l + h * 2 - 1) != 1:
                l = l + h
            else:
                r = l + h - 1
        return l
```
</p>


### Python3 Binary Search
- Author: htkzmo
- Creation Date: Thu Jul 30 2020 20:30:30 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 30 2020 20:30:30 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def getIndex(self, reader: \'ArrayReader\') -> int:
        n = reader.length()
        lo, hi = 0, n - 1
        while lo < hi:
            mid = lo + (hi - lo) // 2
            if (hi - lo + 1) % 2 == 0:
                compare = reader.compareSub(lo, mid, mid + 1, hi)
            else:
                compare = reader.compareSub(lo, mid, mid, hi)
            
            if compare < 0:
                lo = mid + 1
            else:
                hi = mid
                
        return lo
```
</p>


### Understand the tricky part
- Author: tupac_shakur
- Creation Date: Mon Aug 03 2020 10:41:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 03 2020 10:41:27 GMT+0800 (Singapore Standard Time)

<p>
Yes, this needs `logn` time, so that calls to `reader.compareSub()` is reduced.

Tricky part is to understand that `l, r` and `x,y` should have equal number of elements **to get valid results** from `reader.compareSub()`.

```
    fun getIndex(reader: ArrayReader): Int {
        var (low, high) = 0 to (reader.length() - 1)
        var mid = low + (high - low) / 2
        while (low < high) {
            mid = low + (high - low) / 2
            val compare = if ((high - low) % 2 == 0) { // even elements in total, so to divide this range into two parts, include mid.
                reader.compareSub(low, mid, mid, high)
            } else reader.compareSub(low, mid, mid + 1, high) // odd elements in total, we can divide this range easily into two parts
            when(compare) 
                1  -> high = mid
                -1 -> low = mid + 1
                else -> return mid
            }
        }
        return low
    }
```


</p>


