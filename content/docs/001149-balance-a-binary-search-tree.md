---
title: "Balance a Binary Search Tree"
weight: 1149
#id: "balance-a-binary-search-tree"
---
## Description
<div class="description">
<p>Given a binary search tree, return a <strong>balanced</strong> binary search tree with the same node values.</p>

<p>A binary search tree is <em>balanced</em> if and only if&nbsp;the depth of the two subtrees of&nbsp;every&nbsp;node never differ by more than 1.</p>

<p>If there is more than one answer, return any of them.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/08/22/1515_ex1.png" style="width: 250px; height: 248px;" /><img alt="" src="https://assets.leetcode.com/uploads/2019/08/22/1515_ex1_out.png" style="width: 200px; height: 200px;" /></strong></p>

<pre>
<strong>Input:</strong> root = [1,null,2,null,3,null,4,null,null]
<strong>Output:</strong> [2,1,3,null,null,null,4]
<b>Explanation:</b> This is not the only correct answer, [3,1,4,null,2,null,null] is also correct.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree is between&nbsp;<code>1</code>&nbsp;and&nbsp;<code>10^4</code>.</li>
	<li>The tree nodes will have distinct values between&nbsp;<code>1</code>&nbsp;and&nbsp;<code>10^5</code>.</li>
</ul>
</div>

## Tags
- Binary Search Tree (binary-search-tree)

## Companies
- Amazon - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java with picture, DSW O(n)|O(1)
- Author: votrubac
- Creation Date: Tue Mar 17 2020 05:42:48 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Mar 18 2020 17:44:09 GMT+0800 (Singapore Standard Time)

<p>
The DSW algorithm is more complicated than re-creating a balanced tree from a sorted array. However, it does not require any extra memory as we manipulate existing nodes. It can be a big plus if your node stores more than just an int.

The idea is to convert the tree into a vine (like linked list) using left rotations, and then balance it using right rotations. You can look online for the full description of the DSW algorithm.

**Algorithm**
1. Convert the initial tree into a vine. By doing right rotations, we flatten a tree into a \'linked list\', where the head is the former leftmost node, and tail - former rightmost node.
2. As you convert the tree into a vine, count the total number of nodes in `cnt`.
3. Calculate the height of the closest perfectly balanced tree: `h = log2(cnt + 1)`. 
4. Calculate the number of nodes in the closest perfectly balanced tree: `m = pow(2, h) - 1`.
5. Left-rotate `cnt - m` nodes to cover up the excess of nodes.
> Note: you rotate the root node, then you rotate the right child of the new root node, and so on. In other words, left rotations are performed on every second node of the vine. See pictures below for the illustration.
6. Left-rotate `m / 2` nodes.
7. Divide `m` by two and repeat the step above while `m / 2` is greater than zero.  

**Example**
This example is borrowed from [this article](https://csactor.blogspot.com/2018/08/dsw-day-stout-warren-algorithm-dsw.html).

1. Our initial tree has 9 nodes, and we convert it to a vine.

![image](https://assets.leetcode.com/users/votrubac/image_1584399705.png)![image](https://assets.leetcode.com/users/votrubac/image_1584399732.png)
2. The height of the closest perfectly balanced tree is 3, and it contains 7 nodes.
3. So, we initially perform 2 left rotations (9 - 7) to cover up the excess.

![image](https://assets.leetcode.com/users/votrubac/image_1584400245.png)
4. Then, we perform 3 left rotations (7 / 2).

![image](https://assets.leetcode.com/users/votrubac/image_1584400266.png)
5. Finally, we perform 1 left rotation (3 / 2).

![image](https://assets.leetcode.com/users/votrubac/image_1584400292.png)

> Note two excess nodes fill the next (4th) level of the resulting tree, so it\'s not just balanced but also complete.

**Implementation**
In order to achieve O(1) memory, we must to avoid recursion. To do that, we track the grandparent `grand` when doing rotations. Also, we create a temporary node as a tree root to be the initial grandparent.

**C++**
```cpp
int makeVine(TreeNode *grand, int cnt = 0) {
  auto n = grand->right;
  while (n != nullptr) {
    if (n->left != nullptr) {
      auto old_n = n;
      n = n->left;
      old_n->left = n->right;
      n->right = old_n;
      grand->right = n;
    }
    else {      
        ++cnt;
        grand = n;
        n = n->right;
    }
  }
  return cnt;
}
void compress(TreeNode *grand, int m) {
  auto n = grand->right;
  while (m-- > 0) {
    auto old_n = n;
    n = n->right;
    grand->right = n;
    old_n->right = n->left;
    n->left = old_n;
    grand = n;
    n = n->right;
  }
}
TreeNode* balanceBST(TreeNode *root) {
  TreeNode grand;
  grand.right = root;
  auto cnt = makeVine(&grand);
  int m = pow(2, int(log2(cnt + 1))) - 1;
  compress(&grand, cnt - m);
  for (m = m / 2; m > 0; m /= 2)
    compress(&grand, m);
  return grand.right;
}
```

**Java**
```java
int makeVine(TreeNode grand) {
  int cnt = 0;
  var n = grand.right;
  while (n != null) {
    if (n.left != null) {
      var old_n = n;
      n = n.left;
      old_n.left = n.right;
      n.right = old_n;
      grand.right = n;
    }
    else {      
        ++cnt;
        grand = n;
        n = n.right;
    }
  }
  return cnt;
}
void compress(TreeNode grand, int m) {
  var n = grand.right;
  while (m-- > 0) {
    var old_n = n;
    n = n.right;
    grand.right = n;
    old_n.right = n.left;
    n.left = old_n;
    grand = n;
    n = n.right;
  }
}    
public TreeNode balanceBST(TreeNode root) {
  TreeNode grand = new TreeNode(0);
  grand.right = root;
  int cnt = makeVine(grand);
  int m = (int)Math.pow(2, (int)(Math.log(cnt + 1) / Math.log(2))) - 1;
  compress(grand, cnt - m);
  for (m = m / 2; m > 0; m /= 2)
    compress(grand, m);
  return grand.right;
}
```

**Complexity Analysis**
- Time: *O(n)*. We perform up to *n* rotations and traverse *n* nodes.
- Memory: *O(1)*. We re-use existing nodes and process our tree iteratively.
</p>


### [Java/C++] Sorted Array to BST - O(N) - Clean code
- Author: hiepit
- Creation Date: Sun Mar 15 2020 12:01:06 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 17 2020 20:24:08 GMT+0800 (Singapore Standard Time)

<p>
**Intuitive**
- Traverse binary tree in-order to get sorted array
- The problem become [108. Convert Sorted Array to Binary Search Tree
](https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/)

**Java**
```java
class Solution {
    List<TreeNode> sortedArr = new ArrayList<>();
    public TreeNode balanceBST(TreeNode root) {
        inorderTraverse(root);
        return sortedArrayToBST(0, sortedArr.size() - 1);
    }
    void inorderTraverse(TreeNode root) {
        if (root == null) return;
        inorderTraverse(root.left);
        sortedArr.add(root);
        inorderTraverse(root.right);
    }
    TreeNode sortedArrayToBST(int start, int end) {
        if (start > end) return null;
        int mid = (start + end) / 2;
        TreeNode root = sortedArr.get(mid);
        root.left = sortedArrayToBST(start, mid - 1);
        root.right = sortedArrayToBST(mid + 1, end);
        return root;
    }
}
```
**C++**
```c++
class Solution {
public:
    vector<TreeNode*> sortedArr;
    TreeNode* balanceBST(TreeNode* root) {
        inorderTraverse(root);
        return sortedArrayToBST(0, sortedArr.size() - 1);
    }
    void inorderTraverse(TreeNode* root) {
        if (root == NULL) return;
        inorderTraverse(root->left);
        sortedArr.push_back(root);
        inorderTraverse(root->right);
    }
    TreeNode* sortedArrayToBST(int start, int end) {
        if (start > end) return NULL;
        int mid = (start + end) / 2;
        TreeNode* root = sortedArr[mid];
        root->left = sortedArrayToBST(start, mid - 1);
        root->right = sortedArrayToBST(mid + 1, end);
        return root;
    }
};
```
Complexity
- Time & Space: `O(n)`
</p>


### python 3 easy to understand
- Author: akaghosting
- Creation Date: Sun Mar 15 2020 15:45:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 15 2020 15:45:40 GMT+0800 (Singapore Standard Time)

<p>
	# Definition for a binary tree node.
	# class TreeNode:
	#     def __init__(self, x):
	#         self.val = x
	#         self.left = None
	#         self.right = None

	class Solution:
		def balanceBST(self, root: TreeNode) -> TreeNode:
			v = []
			def dfs(node):
				if node:
					dfs(node.left)
					v.append(node.val)
					dfs(node.right)
			dfs(root)

			def bst(v):
				if not v:
					return None
				mid = len(v) // 2
				root = TreeNode(v[mid])
				root.left = bst(v[:mid])
				root.right = bst(v[mid + 1:])
				return root

			return bst(v)
</p>


