---
title: "Longest String Chain"
weight: 1036
#id: "longest-string-chain"
---
## Description
<div class="description">
<p>Given a list of words, each word consists of English lowercase letters.</p>

<p>Let&#39;s say <code>word1</code> is a predecessor of <code>word2</code>&nbsp;if and only if we can add exactly one letter anywhere in <code>word1</code> to make it equal to <code>word2</code>.&nbsp; For example,&nbsp;<code>&quot;abc&quot;</code>&nbsp;is a predecessor of <code>&quot;abac&quot;</code>.</p>

<p>A <em>word chain&nbsp;</em>is a sequence of words <code>[word_1, word_2, ..., word_k]</code>&nbsp;with <code>k &gt;= 1</code>,&nbsp;where <code>word_1</code> is a predecessor of <code>word_2</code>, <code>word_2</code> is a predecessor of <code>word_3</code>, and so on.</p>

<p>Return the longest possible length of a word chain with words chosen from the given list of <code>words</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[&quot;a&quot;,&quot;b&quot;,&quot;ba&quot;,&quot;bca&quot;,&quot;bda&quot;,&quot;bdca&quot;]</span>
<strong>Output: </strong><span id="example-output-1">4
<strong>Explanation</strong>: one of </span>the longest word chain is &quot;a&quot;,&quot;ba&quot;,&quot;bda&quot;,&quot;bdca&quot;.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= words.length &lt;= 1000</code></li>
	<li><code>1 &lt;= words[i].length &lt;= 16</code></li>
	<li><code>words[i]</code> only consists of English lowercase letters.</li>
</ol>

<div>
<p>&nbsp;</p>
</div>
</div>

## Tags
- Hash Table (hash-table)
- Dynamic Programming (dynamic-programming)

## Companies
- Two Sigma - 8 (taggedByAdmin: true)
- Google - 6 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- VMware - 3 (taggedByAdmin: false)
- SAP - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Akuna Capital - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] DP Solution
- Author: lee215
- Creation Date: Sun May 19 2019 12:02:29 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Feb 20 2020 16:29:23 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Sort the `words` by word\'s length. (also can apply bucket sort)
For each word, loop on all possible previous word with 1 letter missing.
If we have seen this previous word, update the longest chain for the current word.
Finally return the longest word chain.
<br>

## **Complexity**
Time `O(NlogN)` for sorting,
Time  `O(NSS)` for the `for` loop, where the second `S` refers to the string generation and `S <= 16`.
Space `O(NS)`
<br>

**Python:**
```python
    def longestStrChain(self, words):
        dp = {}
        for w in sorted(words, key=len):
            dp[w] = max(dp.get(w[:i] + w[i + 1:], 0) + 1 for i in xrange(len(w)))
        return max(dp.values())
```

**C++**
```cpp
    static bool compare(const string &s1, const string &s2) {
        return s1.length() < s2.length();
    }

    int longestStrChain(vector<string>& words) {
        sort(words.begin(), words.end(), compare);
        unordered_map<string, int> dp;
        int res = 0;
        for (string w : words) {
            for (int i = 0; i < w.length(); i++)
                dp[w] = max(dp[w], dp[w.substr(0, i) + w.substr(i + 1)] + 1);
            res = max(res, dp[w]);
        }
        return res;
    }
```

**Java**
Suggested by @noname_minion and @jenniferwang
```java
    public int longestStrChain(String[] words) {
        Map<String, Integer> dp = new HashMap<>();
        Arrays.sort(words, (a, b)->a.length() - b.length());
        int res = 0;
        for (String word : words) {
            int best = 0;
            for (int i = 0; i < word.length(); ++i) {
                String prev = word.substring(0, i) + word.substring(i + 1);
                best = Math.max(best, dp.getOrDefault(prev, 0) + 1);
            }
            dp.put(word, best);
            res = Math.max(res, best);
        }
        return res;
    }
```


</p>


### Incomplete/wrong problem description
- Author: SuccessInVain
- Creation Date: Tue May 21 2019 02:25:54 GMT+0800 (Singapore Standard Time)
- Update Date: Tue May 21 2019 02:26:13 GMT+0800 (Singapore Standard Time)

<p>
The problem description is incomplete or wrong on two counts:

1. It wasn\'t said in the problem that order of words should not be altered.
It should be clearly mentioned that order doesn\'t matter and you could rearrange words to get the longest sequence.

2. Also if no such sequence exists you need to return 1 otherwise the judge fails your solution. For example there was an input in which there is no such sequence. Yet there are no words that have a length of 1. Consider this input ["ab"], only one word. What should be the output for this input? 0 or 1? I would say it is 0, because "ab" cannot be obtained by addition of "exactly" 1 letter, rather two letters : \'a\' and \'b\'. But the judge expects you to return 1.
</p>


### JAVA - Easy Solution with Explanation (MUST READ)!
- Author: anubhavjindal
- Creation Date: Sun Dec 22 2019 23:15:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 07 2020 12:40:41 GMT+0800 (Singapore Standard Time)

<p>
```
public int longestStrChain(String[] words) {
        
	if(words == null || words.length == 0) return 0;
	int res = 0;
	Arrays.sort(words, (a,b)-> a.length()-b.length());  // Sort the words based on their lengths
	HashMap<String, Integer> map = new HashMap();       //Stores each word and length of its max chain.

	for(String w : words) {                             //From shortest word to longest word
		map.put(w, 1);                                  //Each word is atleast 1 chain long
		for(int i=0; i<w.length(); i++) {               //Form next word removing 1 char each time for each w
			StringBuilder sb = new StringBuilder(w);
			String next = sb.deleteCharAt(i).toString();
			if(map.containsKey(next) && map.get(next)+1 > map.get(w))
				map.put(w, map.get(next)+1);            //If the new chain is longer, update the map
		}
		res = Math.max(res, map.get(w));                //Store max length of all chains
	}
	return res;
}
```

**Edit History:**
1. In agreement to @Krizzt comment below, I removed `if(map.containsKey(w)) continue;` since it was not necessary
</p>


