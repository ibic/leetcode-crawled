---
title: "Count Subtrees With Max Distance Between Cities"
weight: 1617
#id: "count-subtrees-with-max-distance-between-cities"
---
## Description
<div class="description">
<p>There are <code>n</code> cities numbered from <code>1</code> to <code>n</code>. You are given an array <code>edges</code> of size <code>n-1</code>, where <code>edges[i] = [u<sub>i</sub>, v<sub>i</sub>]</code> represents a bidirectional edge between cities <code>u<sub>i</sub></code> and <code>v<sub>i</sub></code>. There exists a unique path between each pair of cities. In other words, the cities form a <strong>tree</strong>.</p>

<p>A <strong>subtree</strong> is a subset of cities where every city is reachable from every other city in the subset, where the path between each pair passes through only the cities from the subset. Two subtrees are different if there is a city in one subtree that is not present in the other.</p>

<p>For each <code>d</code> from <code>1</code> to <code>n-1</code>, find the number of subtrees in which the <strong>maximum distance</strong> between any two cities in the subtree is equal to <code>d</code>.</p>

<p>Return <em>an array of size</em> <code>n-1</code> <em>where the </em><code>d<sup>th</sup></code><em> </em><em>element <strong>(1-indexed)</strong> is the number of subtrees in which the <strong>maximum distance</strong> between any two cities is equal to </em><code>d</code>.</p>

<p><strong>Notice</strong>&nbsp;that&nbsp;the <strong>distance</strong> between the two cities is the number of edges in the path between them.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/09/21/p1.png" style="width: 161px; height: 181px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 4, edges = [[1,2],[2,3],[2,4]]
<strong>Output:</strong> [3,4,0]
<strong>Explanation:
</strong>The subtrees with subsets {1,2}, {2,3} and {2,4} have a max distance of 1.
The subtrees with subsets {1,2,3}, {1,2,4}, {2,3,4} and {1,2,3,4} have a max distance of 2.
No subtree has two nodes where the max distance between them is 3.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 2, edges = [[1,2]]
<strong>Output:</strong> [1]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 3, edges = [[1,2],[2,3]]
<strong>Output:</strong> [2,1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 15</code></li>
	<li><code>edges.length == n-1</code></li>
	<li><code>edges[i].length == 2</code></li>
	<li><code>1 &lt;= u<sub>i</sub>, v<sub>i</sub> &lt;= n</code></li>
	<li>All pairs <code>(u<sub>i</sub>, v<sub>i</sub>)</code> are distinct.</li>
</ul>
</div>

## Tags
- Backtracking (backtracking)

## Companies
- Codenation - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Bitmask try all subset of cities - Clean & Concise - O(2^n * n)
- Author: hiepit
- Creation Date: Sun Oct 11 2020 12:20:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2020 15:47:20 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1: Bitmask + StraightForward**
- Since `n <= 15`, there is a maximum `2^15` subset of cities numbered from `1` to `n`.
- For each of subset of cities, we calculate the **maximum distance** between any two cities in our subset.
	- For each `city` in our subset:
		- Using `bfs()` or `dsf()` to calculate distance between `city` to other cities in subset.
		- If `citiy` can\'t reach to any other cities then subset form an **invalid subtree.**
	- Return distance of 2 cities with maxium distance.

Complexity
- Time: `O(2^n * n^2)`
- Space: `O(n^2)`

```python
class Solution(object):
    def countSubgraphsForEachDiameter(self, n, edges):
        def bfs(src, graph):
            visited = {src}
            q = deque([(src, 0)])  # Pair of (vertex, distance)
            farthestDist = 0 # Farthest distance from src to other nodes
            while len(q) > 0:
                u, d = q.popleft()
                farthestDist = d
                for v in graph[u]:
                    if v not in visited:
                        visited.add(v)
                        q.append((v, d+1))
            return farthestDist, visited

        def maxDistance(state):  # return: maximum distance between any two cities in our subset. O(n^2)
            cities = set()
            for i in range(n):
                if (state >> i) & 1 == 1:
                    cities.add(i)
            graph = defaultdict(list)
            for u, v in edges:
                u, v = u - 1, v - 1
                if u in cities and v in cities:
                    graph[u].append(v)
                    graph[v].append(u)
            ans = 0
            for i in cities:
                farthestDist, visited = bfs(i, graph)
                if len(visited) < len(cities): return 0  # Can\'t visit all nodes of the tree -> Invalid tree
                ans = max(ans, farthestDist)
            return ans

        ans = [0] * (n - 1)
        for state in range(1, 2 ** n):
            d = maxDistance(state)
            if d > 0: ans[d - 1] += 1
        return ans
```

**Solution 2: Bitmask + Diamter of the tree**
- Since `n <= 15`, there is a maximum `2^15` subset of cities numbered from `1` to `n`.
- For each of subset, we calculate the **maximum distance** between any two cities in our subset. 
- **Maximum distance** between any two cities in our subset (subset must be a subtree) is the **diameter** of the tree. Can reference: https://leetcode.com/problems/tree-diameter/

Complexity
- Time: `O(2^n * n)`
- Space: `O(n^2)`

```python
class Solution(object):
    def countSubgraphsForEachDiameter(self, n, edges):
        def bfs(src, graph):
            visited = {src}
            q = deque([(src, 0)])  # Pair of (vertex, distance)
            farthestNode, farthestDist = -1, 0
            while len(q) > 0:
                farthestNode, farthestDist = u, d = q.popleft()
                for v in graph[u]:
                    if v not in visited:
                        visited.add(v)
                        q.append((v, d + 1))
            return farthestNode, farthestDist, visited

        def diameterOfTree(cities, graph):
            anyNode = cities.pop()
            cities.add(anyNode)
            farthestNode, _, visited = bfs(anyNode, graph)
            if len(visited) < len(cities): return 0  # Can\'t visit all nodes of the tree -> Invalid tree
            _, dist, _ = bfs(farthestNode, graph)
            return dist

        def maxDistance(state):  # return: maximum distance between any two cities in our subset. O(n)
            cities = set()
            for i in range(n):
                if (state >> i) & 1 == 1:
                    cities.add(i)
            graph = defaultdict(list)
            for u, v in edges:
                u, v = u - 1, v - 1
                if u in cities and v in cities:
                    graph[u].append(v)
                    graph[v].append(u)
            return diameterOfTree(cities, graph)

        ans = [0] * (n - 1)
        for state in range(1, 2 ** n):
            d = maxDistance(state)
            if d > 0: ans[d - 1] += 1
        return ans
```
Credit [@siranjoy](https://leetcode.com/siranjoy/) for reminding me this approach.

</p>


### C++ O(2 ^ n) solution. bitmask + DFS, with explanation.
- Author: chejianchao
- Creation Date: Sun Oct 11 2020 12:00:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2020 12:00:19 GMT+0800 (Singapore Standard Time)

<p>


here is the idea:
1. iterate all the possible subtree from 1 to (1 << n - 1), the positions of `1` bits are the tree node ID.
2. use dfs to calcuate the maximum distance for the subtree.
3. verfiy if the subtree is the valid subtree. We simply visit one node and clear the specific bit, if subtree == 0, that means it is a valid subtree.
4. update ans.

```
class Solution {
public:
    map<int, set<int> > g;
    int dfs(int u, int p, int &subtree, int &mx) {
        vector<int> arr = {0, 0};
        subtree = subtree ^ (1 << (u - 1));  //clear visited Node bit.
        for(auto v : g[u]) {
            if(v == p) continue;
            if((subtree & (1 << (v - 1))) == 0) continue; //the next node is not included in the subtree, ignore this node.
            int res = dfs(v, u, subtree, mx) + 1;
            arr.push_back(res);
        }
        sort(arr.begin(), arr.end());
        mx = max(mx, arr.back() + arr[arr.size() - 2]);
        return arr.back();
    }
    vector<int> countSubgraphsForEachDiameter(int n, vector<vector<int>>& edges) {
        vector<int> ans(n - 1);
        for(auto &e : edges) {     //build graph.
            g[e[0]].insert(e[1]);
            g[e[1]].insert(e[0]);
        }
        int size = 1 << n;
        for(int i = 1; i < size; ++i) {
            if(((i - 1) & i) == 0) continue;  //we don\'t need to calculate the subtree which have one node only.
            int subtree = i;
            int u = 0;
            int mx = 0;
            for(int j = 0; j < n; ++j) {  // to get the start node.
                if((1 <<j) & i) {
                    u = j + 1;
                    break;
                }
            }
            int res = dfs(u, -1, subtree, mx);
            if(subtree == 0) {
                ++ans[mx - 1];
            }
        }
        return ans;
    }
};
```
</p>


### Python, try all subsets, test if subset is subtree, find diameter of subtree
- Author: raymondhfeng
- Creation Date: Sun Oct 11 2020 12:03:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2020 14:43:18 GMT+0800 (Singapore Standard Time)

<p>
Use dfs to determine whether or not we are a valid subtree. Test connectivity(can reach all), and exclusivity(no extra nodes). To find diameter of tree, run dfs twice. Since n is small enough can just use a bitmask to enumerate all possible subsets. Tougher to solve if n can grow large. 
```
class Solution:
    def countSubgraphsForEachDiameter(self, n: int, edges: List[List[int]]) -> List[int]:
        def dfs(edges,start,i): # returns whether or not we are a tree
            seen = set()
            stack = [start]
            while len(stack) > 0:
                curr = stack.pop()
                if curr not in seen:
                    seen.add(curr)
                    for neigh in edges[curr]:
                        stack.append(neigh)
            nodeSet = set()
            for j in range(n):
                if i & (1 << j):
                    nodeSet.add(j)
            return nodeSet == seen
        
        def diameter(edges,start): # returns the diameter of tree
            farthest = start
            farthestDist = 0
            seen = set()
            stack = [(start,0)]
            while len(stack) > 0:
                curr,dist = stack.pop()
                if curr not in seen:
                    seen.add(curr)
                    if dist > farthestDist:
                        farthest = curr
                        farthestDist = dist
                    for neigh in edges[curr]:
                        stack.append((neigh,dist+1))
            start = farthest
            farthestDist = 0
            seen = set()
            stack = [(start,0)]
            while len(stack) > 0:
                curr,dist = stack.pop()
                if curr not in seen:
                    seen.add(curr)
                    if dist > farthestDist:
                        farthest = curr
                        farthestDist = dist
                    for neigh in edges[curr]:
                        stack.append((neigh,dist+1))
            return farthestDist
            
        diameterCounts = defaultdict(int)
        for i in range(1,2**n):
            if sum([int(elem) for elem in bin(i)[2:]]) == 1:
                continue
            subTreeEdges = defaultdict(list)
            for edge in edges:
                l,m = edge
                l -= 1
                m -= 1
                if i & (1 << l) and i & (1 << m):
                    subTreeEdges[l].append(m)
                    subTreeEdges[m].append(l)
            start = None
            for j in range(n):
                if i & (1 << j):
                    start = j
                    break
            isSubtree = dfs(subTreeEdges,start,i)
            if isSubtree:
                diameterCounts[diameter(subTreeEdges,start)] += 1
            else:
                pass
        
        return [diameterCounts[i] for i in range(1,n)]            
```
</p>


