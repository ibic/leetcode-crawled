---
title: "Find a Value of a Mysterious Function Closest to Target"
weight: 1393
#id: "find-a-value-of-a-mysterious-function-closest-to-target"
---
## Description
<div class="description">
<p><img alt="" src="https://assets.leetcode.com/uploads/2020/07/09/change.png" style="width: 635px; height: 312px;" /></p>

<p>Winston was given the above mysterious function <code>func</code>. He has an integer array <code>arr</code> and an integer <code>target</code> and he wants to find the values&nbsp;<code>l</code> and <code>r</code>&nbsp;that make&nbsp;the value <code>|func(arr, l, r) - target|</code> minimum possible.</p>

<p>Return <em>the minimum possible value</em> of <code>|func(arr, l, r) - target|</code>.</p>

<p>Notice that <code>func</code> should be called with the values&nbsp;<code>l</code> and <code>r</code> where <code>0 &lt;= l, r &lt; arr.length</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [9,12,3,7,15], target = 5
<strong>Output:</strong> 2
<strong>Explanation:</strong> Calling func with all the pairs of [l,r] = [[0,0],[1,1],[2,2],[3,3],[4,4],[0,1],[1,2],[2,3],[3,4],[0,2],[1,3],[2,4],[0,3],[1,4],[0,4]], Winston got the following results [9,12,3,7,15,8,0,3,7,0,0,3,0,0,0]. The value closest to 5 is 7 and 3, thus the minimum difference is 2.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1000000,1000000,1000000], target = 1
<strong>Output:</strong> 999999
<strong>Explanation:</strong> Winston called the func with all possible values of [l,r] and he always got 1000000, thus the min difference is 999999.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,2,4,8,16], target = 0
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 10^5</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 10^6</code></li>
	<li><code>0 &lt;= target &lt;= 10^7</code></li>
</ul>

</div>

## Tags
- Binary Search (binary-search)
- Bit Manipulation (bit-manipulation)
- Segment Tree (segment-tree)

## Companies
- American Express - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Detailed General Idea/solution for such problems ( AND, OR, GCD ) in O(N * log( max(arr[i]) ) )
- Author: mtewathia99
- Creation Date: Sun Jul 19 2020 16:43:51 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 08 2020 13:35:22 GMT+0800 (Singapore Standard Time)

<p>
## Observations:

1. If we start taking AND of elements of any sequence, the AND value will either **remain same or decrease**
	let  ```arr[1], arr[2], arr[3]```  be the  sequence
		 
		 -> a1 = arr[1], 
		 -> a2 = arr[1]&arr[2], 
		 -> a3 = arr[1]&arr[2]&arr[3]
		We can say : a1 >= a2 >= a3
		because while taking AND we cannot set a bit so either the AND value will remain same or decrease

2.  AND value can decrease by atmost **number of set bits in A**  times i.e at max **max(log (A))** times. 
3.  The number of unique AND of a subarrays starting at any position will change by atmost LOG( A[i] ) times ( A[i] is first element of  subarray)
4. The AND\'s of subarrays starting at ith index can be calculated from AND\'s of subarrays starting at (i+1)th index

	 **st[i]** :  contains **AND values** of subarrays starting from ith index 
	
		for eg : arr[] = [2,6,7,2,4]; 	(0 based indexing)
	
		Subarrays starting at 4th index
			: [4]			            --> st[5] = { 4 }
		Subarrays starting at 3rd index
			: [2], [2,4] 			    --> st[4] = { 2, 0 }
		Subarrays starting at 2nd index
			: [7], [7,2], [7,2,4], 		--> st[3] = { 7, 2, 0 }
	
	 **AND_VAL[i, len]** :  AND of elements of subarray starting from \'ith\' index of length \'len\'
	**Observe that** 
	```
	AND_VAL[i,1] = arr[i];
	AND_VAL[i,2] = arr[i] & AND_VAL[i+1,1];
	AND_VAL[i,3] = arr[i] & AND_VAL[i+1,2];
	AND_VAL[i,4] = arr[i] & AND_VAL[i+1,3];
	.
	.
	AND_VAL[i,j] = arr[i] & AND_VAL[i+1,j-1];
	```
	also **AND_VAL[i+1, j]** are present in **st[i+1]**. ( check definition of st[i] )
	Therefore **st[i] can be calculated from st[i+1]** as AND values of st[i] are nothing but **AND of arr[i] with values in st[i+1]** 
	
	From Observation (3) we can conclude  :
		```
		st[i].size() <= log( max( A ) ) <= 20 ``` as ( A <= 10^6 )
	
	***Time Complexity : O( n  *  log( max( arr[i] ) ) )***
	**Code** 	:
	
	
	```c++
	int closestToTarget(vector<int>& arr, int target) {
        int ans = INT_MAX, n = arr.size();
        
        set<int> st[n];
		// set[i] -> contains **unique** AND values of subarrays starting from ith index 

        st[n-1] = { arr[n-1] };
        for(int i = n-2; i >= 0; i--)
        {   
            // calcuting st[i] with help of st[i+1]
            st[i].insert(arr[i]);
            for(auto &val: st[i+1])
            {
                int new_val = (arr[i]&val);
                st[i].insert( new_val );
            }
        }
        
        // Iterate over all possible AND values and find the ans
        for(int i = 0; i < n; i++)
        {
            for( auto &val: st[i] ) ans = min(ans, abs(val - target));
        } 
        
        return ans;
    }
	```
	
	**Similar concept can be applied to  bitwise OR / GCD because they too are monotonic when applied on a sequence and
	the values of cumulative  bitwise OR  / GCD changes atmost log( max( element) ) times**
	



</p>


### Java Straightforward O(N^2) with optimization Vs Set O(N)
- Author: hobiter
- Creation Date: Sun Jul 19 2020 12:02:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 04:19:10 GMT+0800 (Singapore Standard Time)

<p>
You may be feeling cheated, or may there be a better solution. 

Straightforward O(N^2) with optimization
see comment for optimization. Passed without TLE; (TLE now, updated on 7/28/2020)
Space: O(1)
```
    public int closestToTarget(int[] arr, int t) {
        int m = arr.length, res = Integer.MAX_VALUE;
        for (int i = 0; i < m; i++) {
            int sum = arr[i];
            for (int r = i; r < m; r++) {
                sum = sum & arr[r];
                res = Math.min(res, Math.abs(sum - t));
                if (sum <= t) break;   // sum is decreasing within inner loop. and now sum < t, res won\'t be lower.
            }
            if (sum >= t) break; // the future sum won\'t be smaller than this sum, so res won\'t be smaller
			//current sum = arr[i] & ... & arr[n -1], which is the smallest, any further sum after this loop, 
			// which will be arr[k] & ... & arr[n -1], where k > i, will be larger than current sum;
        }
        return res;
    }
```

Update on 7/28/2020:
Now above version is TLE, so we have to use the following version:

There is another HashSet O(32 * N) ==> O(N) solution, though space is not strictly O(1) :
https://leetcode.com/problems/find-a-value-of-a-mysterious-function-closest-to-target/discuss/743442

But not sure why it cost more time and space in OJ, should have been better than this:
```
    public int closestToTarget(int[] arr, int t) {
        int m = arr.length, res = Integer.MAX_VALUE;
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < m; i++) {
            Set<Integer> tmp = new HashSet<>();
            tmp.add(arr[i]);
            for (int n : set)  tmp.add(n & arr[i]);
            for (int n : tmp) res = Math.min(res, Math.abs(n - t));
            set = tmp;
        }
        return res;
    }
```
</p>


### [Python] 6 lines O(nlogm) solution
- Author: qqwqert007
- Creation Date: Sun Jul 19 2020 13:06:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 21 2020 17:14:33 GMT+0800 (Singapore Standard Time)

<p>
Let `AND(i, j)` denote `arr[i] & arr[i+1] & ... & arr[j]`.
For a fixed `j`, let `s(j)` record all the results of `AND(i, j)` where `0 <= i <= j`.
Then `s(j + 1) = {arr[j + 1]} | {arr[j + 1] & a} for all a in s(j)`.
Therefore we can get all `s(0), s(1), ..., s(n-1)` and find the answer.
The size of each `s(j)` is `O(logm)` where `m = max(arr)`.
Why is that? Because of monotonicity.
Let\'s compare `AND(i-1, j)` and `AND(i, j)`, there are two situations:
1.  `AND(i-1, j) == AND(i, j)`.
2.  `AND(i-1, j) < AND(i, j)`.

In situation 2, `AND(i-1, j)` has less bits of 1 than `AND(i, j)`.
As the total number of bits is `O(logm)`, the size of `s(j)` is `O(logm)`.

```python
class Solution:
    def closestToTarget(self, arr: List[int], target: int) -> int:
        s, ans = set(), float(\'inf\')
        for a in arr:
            s = {a & b for b in s} | {a}
            for c in s:
                ans = min(ans, abs(c - target))
        return ans
```

To @ArthurLin:
Actually I used another complicated method to solve this problem in the contest.
After the contest, I was inspired by the solution of other participants.
I suddenly realized that I have seen the same trick before, and here are some similar questions: 
1. [898. Bitwise ORs of Subarrays](https://leetcode.com/problems/bitwise-ors-of-subarrays/): I used the same trick 3 months ago but I can\'t remember during the contest.
2. [1016. Binary String With Substrings Representing 1 To N](https://leetcode.com/problems/binary-string-with-substrings-representing-1-to-n/): I was stuck by the O(n^2) substring, but found only O(nlogm) useful in the end.

The lesson I learnt is that: Don\'t be afraid of the large state space of the problem and what is useful to you may be much less.
</p>


