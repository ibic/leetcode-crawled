---
title: "Random Flip Matrix"
weight: 827
#id: "random-flip-matrix"
---
## Description
<div class="description">
<p>You are given the number of rows <code>n_rows</code>&nbsp;and number of columns <code>n_cols</code>&nbsp;of a&nbsp;2D&nbsp;binary matrix&nbsp;where all values are initially 0.&nbsp;Write a function <code>flip</code>&nbsp;which chooses&nbsp;a 0 value&nbsp;<a href="https://en.wikipedia.org/wiki/Discrete_uniform_distribution" target="_blank">uniformly at random</a>,&nbsp;changes it to 1,&nbsp;and then returns the position <code>[row.id, col.id]</code> of that value. Also, write a function <code>reset</code> which sets all values back to 0.&nbsp;<strong>Try to minimize the number of calls to system&#39;s Math.random()</strong> and optimize the time and&nbsp;space complexity.</p>

<p>Note:</p>

<ol>
	<li><code>1 &lt;= n_rows, n_cols&nbsp;&lt;= 10000</code></li>
	<li><code>0 &lt;= row.id &lt; n_rows</code> and <code>0 &lt;= col.id &lt; n_cols</code></li>
	<li><code>flip</code>&nbsp;will not be called when the matrix has no&nbsp;0 values left.</li>
	<li>the total number of calls to&nbsp;<code>flip</code>&nbsp;and <code>reset</code>&nbsp;will not exceed&nbsp;1000.</li>
</ol>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: 
</strong><span id="example-input-1-1">[&quot;Solution&quot;,&quot;flip&quot;,&quot;flip&quot;,&quot;flip&quot;,&quot;flip&quot;]
</span><span id="example-input-1-2">[[2,3],[],[],[],[]]</span>
<strong>Output: </strong><span id="example-output-1">[null,[0,1],[1,2],[1,0],[1,1]]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: 
</strong><span id="example-input-2-1">[&quot;Solution&quot;,&quot;flip&quot;,&quot;flip&quot;,&quot;reset&quot;,&quot;flip&quot;]
</span><span id="example-input-2-2">[[1,2],[],[],[],[]]</span>
<strong>Output: </strong><span id="example-output-2">[null,[0,0],[0,1],null,[0,0]]</span></pre>
</div>

<p><strong>Explanation of Input Syntax:</strong></p>

<p>The input is two lists:&nbsp;the subroutines called&nbsp;and their&nbsp;arguments. <code>Solution</code>&#39;s constructor&nbsp;has two arguments, <code>n_rows</code> and <code>n_cols</code>.&nbsp;<code>flip</code>&nbsp;and <code>reset</code> have&nbsp;no&nbsp;arguments.&nbsp;Arguments&nbsp;are&nbsp;always wrapped with a list, even if there aren&#39;t any.</p>

</div>

## Tags
- Random (random)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---

#### Preface:

Because the matrix is given to us in an abstract way, we must design our own representation of the matrix. In addition to being accurate, we would want our representation to have the following properties:

1. It utilizes less space than $$O(\text{n_rows} \cdot \text{n_cols})$$, since $$\text{n_rows}$$ and $$\text{n_cols}$$ can be very large.
2. It allows us to generate a random $$0$$ position efficiently, using significantly less than $$O(\text{n_rows} \cdot \text{n_cols})$$ time and only one call to the random number generator.

Described below are two different representations which have these properties.

#### Approach 1: Virtual Array

**Intuition**

Let assume that we are given an array-like data structure $$V$$ of size $$\text{n_rows} \cdot \text{n_cols}$$, where all cells $$V[i]$$ have been initialized to $$i$$th entry in the matrix, and each assignment and access takes $$O(1)$$ time.

This data structure can encode the state of the matrix. How can it encode the state of the matrix in a way that enables efficient generation of random $$0$$ positions?

Also, since this data structure isn't actually given to us, how can we construct this data structure in less than $$O(\text{n_rows} \cdot \text{n_cols})$$ time?

**Algorithm**

Data structure $$V$$, described above, obviously cannot be constructed in less than $$O(\text{n_rows} \cdot \text{n_cols})$$ time if the initializations are explicit. We must approach it in another way.

We create a new data type which is a slight modification of HashMap, where access to uninitialized key $$k$$ will initialize $$V[k]$$ to the $$k$$th entry in the matrix and then return it, rather than throwing an error. In this way, it is implied that all $$V[k]$$ are initialized to $$k$$th entry in the matrix.

Let the number of $$0$$ entries remaining in the matrix be denoted as $$\text{rem}$$. As we perform $$\text{flip}$$ and $$\text{reset}$$ operations, we update $$V$$ to maintain the invariant that $$V[0] \dots V[\text{rem}-1]$$ map to all $$0$$ entries and $$V[\text{rem}] \dots V[\text{n_rows} \cdot \text{n_cols}-1]$$ map to all $$1$$ entries.

　$$\text{flip}$$ will change the $$0$$ entry stored at $$V[k]$$ to $$1$$, where $$k$$ is a random integer in the range $$[0, \text{rem})$$. Then, it will decrement rem and swap $$V[k]$$ with $$V[\text{rem}]$$.

　$$\text{reset}$$ will clear $$V$$ of all assigned values and set $$\text{rem}$$ to $$\text{n_rows} \cdot \text{n_cols}$$.

<iframe src="https://leetcode.com/playground/jm6vxgX6/shared" frameBorder="0" width="100%" height="500" name="jm6vxgX6"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(1)$$ preprocessing. $$O(1)$$ $$\text{flip}$$. $$O(\text{min}(\text{F}, \text{n_rows} \cdot \text{n_cols}))$$ $$\text{reset}$$, where $$\text{F}$$ is the total number of flips.
* Space Complexity: $$O(\text{min}(\text{F}, \text{n_rows} \cdot \text{n_cols}))$$.

<br/>

---

#### Approach 2: Square-Root Decomposition

**Intuition**

Say that we have $$\text{rem}$$ $$0$$ entries left in the matrix and have randomly chosen the $$k$$th $$0$$ entry to be flipped, where $$0 \leq k < \text{rem}$$. Traversing through all $$\text{n_rows} \cdot \text{n_cols}$$ cells to find its position is too costly. If we split the matrix into roughly $$\sqrt{\text{n_rows} \cdot \text{n_cols}}$$ contiguous groups of roughly size $$\sqrt{\text{n_rows} \cdot \text{n_cols}}$$ each, we can find the containing group in $$O\!\left( \sqrt{\text{n_rows} \cdot \text{n_cols}}\, \right)$$ time and then search through that group in $$O\!\left( \sqrt{\text{n_rows} \cdot \text{n_cols}}\, \right)$$ time to find the $$k$$th $$0$$ entry.

**Algorithm**

Create roughly $$\sqrt{\text{n_rows} \cdot \text{n_cols}}$$ buckets, and have the $$i$$th entry in the matrix belong to bucket number $$\dfrac{i}{\sqrt{\text{n_rows} \cdot \text{n_cols}}}$$.

Each bucket has a $$\text{size}$$ attribute which represents the number of entries in the matrix that map to it. Also, each bucket tracks which of its entries are $$1$$-valued vs $$0$$-valued by storing its $$1$$-valued entries in a HashSet.

To find the $$k$$th remaining $$0$$ entry in the matrix, loop through the list of buckets and use the $$\text{size}$$ and the count of $$1$$s in each bucket to calculate which bucket contains the $$k$$th remaining $$0$$ entry. Then, loop through all entries which belong to this bucket, checking which are $$0$$-valued and which are not, to find out what the $$k$$th remaining $$0$$ entry is.

<iframe src="https://leetcode.com/playground/SGFC3j6D/shared" frameBorder="0" width="100%" height="500" name="SGFC3j6D"></iframe>

**Complexity Analysis**

* Time Complexity: $$O\!\left( \sqrt{\text{n_rows} \cdot \text{n_cols}}\, \right)$$ preprocessing. $$O\!\left( \sqrt{\text{n_rows} \cdot \text{n_cols}}\, \right)$$ $$\text{flip}$$. $$O(\sqrt{\text{n_rows} \cdot \text{n_cols}} + \min (\text{F}, (\text{n_rows} \cdot \text{n_cols})))$$ $$\text{reset}$$, where $$\text{F}$$ is the total number of flips.
* Space Complexity: $$O(\sqrt{\text{n_rows} \cdot \text{n_cols}} + \min (\text{F}, (\text{n_rows} \cdot \text{n_cols})))$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java AC Solution, call Least times of Random.nextInt() function
- Author: BingleLove
- Creation Date: Fri Jul 27 2018 12:58:26 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 27 2018 12:58:26 GMT+0800 (Singapore Standard Time)

<p>
Only call once every time
```Java
class Solution {

    Map<Integer, Integer> map;
    int rows, cols, total;
    Random rand;
    
    public Solution(int n_rows, int n_cols) {
        map = new HashMap<>();
        rand = new Random();
        rows = n_rows; 
        cols = n_cols; 
        total = rows * cols;
    }
    
    public int[] flip() {
        int r = rand.nextInt(total--);
        int x = map.getOrDefault(r, r);
        map.put(r, map.getOrDefault(total, total));
        return new int[]{x / cols, x % cols};
    }
    
    public void reset() {
        map.clear();
        total = rows * cols;
    }
}
```
</p>


### [Easy] Algorithm Explanation Step By Step.
- Author: oa5cuxe0To3
- Creation Date: Tue Jul 31 2018 16:49:36 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 15 2020 04:36:16 GMT+0800 (Singapore Standard Time)

<p>
**Description**
We will be using modern method of the Fisher\u2013Yates shuffle.

Please read how the original algorithm works: [Examples](https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#Examples).

**Overview**
* generate random from 0 to n : m
* swap m and n
* decrease n

**Implementation**
```java
class Solution 
{
    private Map<Integer, Integer> map;
    private int rows, cols, total;
    private Random rand;
    
    public Solution(int n_rows, int n_cols) 
	{
        map = new HashMap<>();
        rand = new Random();
        rows = n_rows; 
        cols = n_cols; 
        total = rows * cols;
    }
    
    public int[] flip() 
	{
        // generate index, decrease total number of values
        int r = rand.nextInt(total--); 
		
        // check if we have already put something at this index
        int x = map.getOrDefault(r, r); 
		
        // swap - put total at index that we generated
        map.put(r, map.getOrDefault(total, total)); 
		
        return new int[]{x / cols, x % cols}; 
    }
    
    public void reset() 
	{
        map.clear();
        total = rows * cols; 
    }
}
```
</p>


### Python solution based on random shuffle with explanation
- Author: veihbisd
- Creation Date: Sat Jul 28 2018 22:47:52 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 06:16:58 GMT+0800 (Singapore Standard Time)

<p>
This is a sampling n elements without replacement problem. It is the same as the operation that random shuffe an array and then return the first n elements. 

Here come the trick. When we random pick an element in the array we can store its new position in a hash table instead of the array because n is extremely less than the total num. So we can accomplish this within O(1) time and O(k) space where k is the maxium call of flip.

```
class Solution:

    def __init__(self, n_rows, n_cols):
        """
        :type n_rows: int
        :type n_cols: int
        """
        self.c = n_cols
        self.end = n_rows * n_cols - 1
        self.d = {}
        self.start = 0
        
    def flip(self):
        """
        :rtype: List[int]
        """
        rand = random.randint(self.start, self.end)
        res = self.d.get(rand, rand)
        self.d[rand] = self.d.get(self.start, self.start)
        self.start += 1
        return divmod(res, self.c)
    
    def reset(self):
        """
        :rtype: void
        """
        self.d = {}
        self.start = 0
```
</p>


