---
title: "Game Play Analysis I"
weight: 1523
#id: "game-play-analysis-i"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Activity</code></p>

<pre>
+--------------+---------+
| Column Name  | Type    |
+--------------+---------+
| player_id    | int     |
| device_id    | int     |
| event_date   | date    |
| games_played | int     |
+--------------+---------+
(player_id, event_date) is the primary key of this table.
This table shows the activity of players of some game.
Each row is a record of a player who logged in and played a number of games (possibly 0) before logging out on some day using some device.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query that reports the&nbsp;<strong>first login date</strong> for each player.</p>

<p>The query result format is in the following example:</p>

<pre>
Activity table:
+-----------+-----------+------------+--------------+
| player_id | device_id | event_date | games_played |
+-----------+-----------+------------+--------------+
| 1         | 2         | 2016-03-01 | 5            |
| 1         | 2         | 2016-05-02 | 6            |
| 2         | 3         | 2017-06-25 | 1            |
| 3         | 1         | 2016-03-02 | 0            |
| 3         | 4         | 2018-07-03 | 5            |
+-----------+-----------+------------+--------------+

Result table:
+-----------+-------------+
| player_id | first_login |
+-----------+-------------+
| 1         | 2016-03-01  |
| 2         | 2017-06-25  |
| 3         | 2016-03-02  |
+-----------+-------------+
</pre>

</div>

## Tags


## Companies
- GSN Games - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL Solution
- Author: mbodke41
- Creation Date: Sat Jun 15 2019 23:18:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 15 2019 23:18:24 GMT+0800 (Singapore Standard Time)

<p>
```
select player_id, min(event_date) as first_login
from activity 
group by player_id
```
</p>


### Solution
- Author: XYGong12
- Creation Date: Tue Jul 30 2019 14:40:47 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 30 2019 14:40:47 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT player_id, MIN(event_date) AS first_login
FROM activity
GROUP BY player_id
</p>


### MySQL using group by and min()
- Author: Roger_Q
- Creation Date: Fri Jul 05 2019 10:35:13 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 05 2019 10:35:13 GMT+0800 (Singapore Standard Time)

<p>
```
# Write your MySQL query statement below
select player_id
    , min(event_date) as first_login
from Activity 
group by 1 

```
</p>


