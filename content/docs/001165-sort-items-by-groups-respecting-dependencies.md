---
title: "Sort Items by Groups Respecting Dependencies"
weight: 1165
#id: "sort-items-by-groups-respecting-dependencies"
---
## Description
<div class="description">
<p>There are&nbsp;<code>n</code>&nbsp;items each&nbsp;belonging to zero or one of&nbsp;<code>m</code>&nbsp;groups where <code>group[i]</code>&nbsp;is the group that the <code>i</code>-th item belongs to and it&#39;s equal to <code>-1</code>&nbsp;if the <code>i</code>-th item belongs to no group. The items and the groups are zero indexed. A group can have no item belonging to it.</p>

<p>Return a sorted list of the items such that:</p>

<ul>
	<li>The items that belong to the same group are next to each other in the sorted list.</li>
	<li>There are some&nbsp;relations&nbsp;between these items where&nbsp;<code>beforeItems[i]</code>&nbsp;is a list containing all the items that should come before the&nbsp;<code>i</code>-th item in the sorted array (to the left of the&nbsp;<code>i</code>-th item).</li>
</ul>

<p>Return any solution if there is more than one solution and return an <strong>empty list</strong>&nbsp;if there is no solution.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/09/11/1359_ex1.png" style="width: 191px; height: 181px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 8, m = 2, group = [-1,-1,1,0,0,1,0,-1], beforeItems = [[],[6],[5],[6],[3,6],[],[],[]]
<strong>Output:</strong> [6,3,4,1,5,2,0,7]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 8, m = 2, group = [-1,-1,1,0,0,1,0,-1], beforeItems = [[],[6],[5],[6],[3],[],[4],[]]
<strong>Output:</strong> []
<strong>Explanation:</strong>&nbsp;This is the same as example 1 except that 4 needs to be before 6 in the sorted list.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= m &lt;= n &lt;= 3*10^4</code></li>
	<li><code>group.length == beforeItems.length == n</code></li>
	<li><code>-1 &lt;= group[i] &lt;= m-1</code></li>
	<li><code>0 &lt;= beforeItems[i].length &lt;= n-1</code></li>
	<li><code>0 &lt;= beforeItems[i][j] &lt;= n-1</code></li>
	<li><code>i != beforeItems[i][j]</code></li>
	<li><code>beforeItems[i]&nbsp;</code>does not contain&nbsp;duplicates elements.</li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Graph (graph)
- Topological Sort (topological-sort)

## Companies
- Google - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ with picture, generic topological sort
- Author: votrubac
- Creation Date: Sat Oct 12 2019 12:26:13 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 27 2020 03:14:14 GMT+0800 (Singapore Standard Time)

<p>
I solved this problem first with two-level topological sort, but the code was long and complicated. So I wanted to find a way to transform it to use a generic topological sort.
1. Initialize adjacency list of `n + 2m` size. We will be using two extra nodes per group.
2.	Wrap all nodes in the group with inbound and outbound dependency nodes.
	- Nodes not belonging to any group do not need to be wrapped.
3.	Connect nodes directly for inter-group dependencies.
4.	Connect to/from dependency nodes for intra-group dependencies.
5.	Perform topological sort, starting from dependency nodes.
	- This way, all nodes within a single group will be together.
6.	Remove dependency nodes from the result.

In this example, the group dependency nodes are 8/10 (group 0) and 9/11 (group 1):

![image](https://assets.leetcode.com/users/votrubac/image_1570863057.png)

The result of the topological sort could be, for example,  `[0, 7, 8, 6, 3, 4, 10, 9, 2, 5, 11, 1]`, which after removing dependency nodes becomes `[0, 7, 6, 3, 4, 2, 5, 1]`.
```
bool topSort(vector<unordered_set<int>>& al, int i, vector<int>& res, vector<int>& stat) {
    if (stat[i] != 0) return stat[i] == 2;
    stat[i] = 1;
    for (auto n : al[i])
        if (!topSort(al, n, res, stat)) return false;
    stat[i] = 2;
    res.push_back(i);
    return true;
}
vector<int> sortItems(int n, int m, vector<int>& group, vector<vector<int>>& beforeItems) {
    vector<int> res_tmp, res(n), stat(n + 2 * m);
    vector<unordered_set<int>> al(n + 2 * m);
    for (auto i = 0; i < n; ++i) {
        if (group[i] != -1) {
            al[n + group[i]].insert(i);
            al[i].insert(n + m + group[i]);
        }
        for (auto j : beforeItems[i]) {
            if (group[i] != -1 && group[i] == group[j]) al[j].insert(i);
            else {
                auto ig = group[i] == -1 ? i : n + group[i];
                auto jg = group[j] == -1 ? j : n + m + group[j];
                al[jg].insert(ig);
            }
        }
    }
    for (int n = al.size() - 1; n >= 0; --n)
        if (!topSort(al, n, res_tmp, stat)) return {};
    reverse(begin(res_tmp), end(res_tmp));
    copy_if(begin(res_tmp), end(res_tmp), res.begin(), [&](int i) {return i < n; });
    return res;
}
```
</p>


### [JAVA] Topological sort. Detailed Explanation
- Author: heijihattori
- Creation Date: Wed Sep 25 2019 09:12:33 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 25 2019 09:12:33 GMT+0800 (Singapore Standard Time)

<p>
To briefly explain the solution approach:
* Convert the problem into graph. Directed Acyclic Graph to be specific.
* Perform topological sort onto graph.
* The solution to the question is related to topological ordering.

## Detailed Explanation:

### Brief explanation to Topological Sort:
In simple terms directed acyclic graph is a directed graph with no cycles. To help in understanding conceptually or visually think of the graph as a dependency graph. These types of graphs have specific order known as topological ordering which lists the nodes of the graph in the order of dependencies. To know and practice more about these types of graph checkout [Course Schedule](https://leetcode.com/problems/course-schedule/). 

Take for example the graph show in the image:![image](https://assets.leetcode.com/users/heijihattori/image_1569372526.png)
##### Note: Image taken from [GeeksforGeeks](https://www.geeksforgeeks.org)

As show in the image the graph is a directed acyclic graph. The given graph has mulitple topological orders: [5,4,2,3,1,0], [4,5,2,3,1,0], etc. As you might have noticed in the list the nodes are listed in order of their dependencies. To elaborate if there exists an edge from a -> b. The topological order will always have a before b. 

### Application to our problem:

In this problem we have two requirements. 
* All items belonging to the same group must be together. 
* Items in beforeItems for a number should always be followed by those numbers. For example if we have [1,3,4] in beforeItems for 5. This means that the order should always contain 1,3 & 4 before 5. 

This gives us a slight idea that we can form a dependency or Directed acyclic graph using the beforeItems list. But it is not that simple we can have dependency between items across different groups or dependency between items in the same group. 

**Observe here that the concrete graph structure can be determined by the dependencies across different groups.** 

### Solution approach: 
To address the dependencies across different groups let\'s from a graph of groups. Performing topological sort of this graph gives us the order of the groups. Now we don\'t need to worry about the dependencies across different groups because if let\'s say we have group1 : [1,2,3] and group2: [4,5,6] and we have dependencies say:
1->5 & 3->4 because the topological order of the groups will always have group1 before group2 these dependencies are always satisfied. 
Coming on to the dependencies that are in the same group we can use topological ordering of the items to form a relative order of items in the same group. Thus this will get us our final answer. 

### Code:
```
class Solution {
  Map <Integer, List<Integer>> groupGraph;
  Map <Integer, List<Integer>> itemGraph;
  
  int[] groupsIndegree;
  int[] itemsIndegree;
  
  private void buildGraphOfGroups(int[] group, List<List<Integer>> beforeItems, int n) {
    for (int i=0;i<group.length;i++) {
      int toGroup = group[i];
      List <Integer> fromItems = beforeItems.get(i);
      for (int fromItem : fromItems) {
        int fromGroup = group[fromItem];
        if(fromGroup != toGroup) {
          groupGraph.computeIfAbsent(fromGroup, x->new ArrayList()).add(toGroup); 
          groupsIndegree[toGroup]++;
        }        
      }
    }
  }
  
  private void buildGraphOfItems(List<List<Integer>> beforeItems, int n) {
    for (int i=0;i<n;i++) {
      List<Integer> items = beforeItems.get(i);
      for (Integer item : items) {
        itemGraph.computeIfAbsent(item, x->new ArrayList()).add(i);
        itemsIndegree[i]++;
      }
    }
  }
   
  private List<Integer> topologicalSortUtil(Map <Integer, List<Integer>> graph, int[] indegree, int n) {
    List <Integer> list = new ArrayList<Integer>();
    Queue <Integer> queue = new LinkedList();
    for (int key : graph.keySet()) {
      if(indegree[key] == 0) {
        queue.add(key);
      }
    }
    
    while(!queue.isEmpty()) {
      int node = queue.poll();
      n--;
      list.add(node);
      for (int neighbor : graph.get(node)) {
        indegree[neighbor] --;        
        if(indegree[neighbor] == 0) {
          queue.offer(neighbor);          
        }
      }
    }    
    return n == 0 ? list : new ArrayList();
  }  
  
  public int[] sortItems(int n, int m, int[] group, List<List<Integer>> beforeItems) {
    groupGraph = new HashMap();
    itemGraph = new HashMap();
    
    // Each item belongs to a group. If an item doesn\'t have a group it forms it\'s own isolated group.
    for (int i=0;i<group.length;i++) {
      if(group[i] == -1) group[i] = m++;
    }  
    
    for (int i=0;i<m;i++) {
      groupGraph.put(i, new ArrayList());      
    }
    
    for (int i=0;i<n;i++) {
      itemGraph.put(i, new ArrayList());      
    }
    
    groupsIndegree = new int[m];
    itemsIndegree = new int[n];    
    
    // form graph structure across different groups and also calculate indegree.
    buildGraphOfGroups(group, beforeItems, n);
    
    // form graph structure across different items and also calculate indegree.
    buildGraphOfItems(beforeItems, n);
    
    // Topological ordering of the groups.
    List<Integer> groupsList = topologicalSortUtil(groupGraph, groupsIndegree, m);
    // Topological ordering of the items.
    List<Integer> itemsList = topologicalSortUtil(itemGraph, itemsIndegree, n);
        
    // Detect if there are any cycles.
    if(groupsList.size() == 0 || itemsList.size() == 0) return new int[0];    
    
    // This Map holds relative order of items in the same group computed in the loop below. 
    Map <Integer, List<Integer>> groupsToItems = new HashMap();
        
    for (Integer item : itemsList) {
      groupsToItems.computeIfAbsent(group[item], x->new ArrayList()).add(item);
    }
    
    int[] ans = new int[n];
    int idx = 0;
    for (Integer grp : groupsList) {
      List <Integer> items = groupsToItems.getOrDefault(grp, new ArrayList());
      for (Integer item : items) {
        ans[idx++] = item;  
      }      
    }
    
    return ans;
  }
}
```


</p>


### Concise Python two-level topological sorting
- Author: otoc
- Creation Date: Mon Sep 23 2019 08:41:52 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 23 2019 08:53:00 GMT+0800 (Singapore Standard Time)

<p>
This is a topological sorting problem.
Firstly sort the groups, then sort the items in each group.

```
    def sortItems(self, n: int, m: int, group: List[int], beforeItems: List[List[int]]) -> List[int]:
        def topo_sort(points, pre, suc):
            order = []
            sources = [p for p in points if not pre[p]]
            while sources:
                s = sources.pop()
                order.append(s)
                for u in suc[s]:
                    pre[u].remove(s)
                    if not pre[u]:
                        sources.append(u)
            return order if len(order) == len(points) else []
        
        # find the group of each item
        group2item = collections.defaultdict(set)
        for i in range(n):
            if group[i] == -1:
                group[i] = m
                m += 1
            group2item[group[i]].add(i)
        # find the relationships between the groups and each items in the same group
        t_pre, t_suc = collections.defaultdict(set), collections.defaultdict(set)
        g_pre, g_suc = collections.defaultdict(set), collections.defaultdict(set)
        for i in range(n):
            for j in beforeItems[i]:
                if group[i] == group[j]:
                    t_pre[i].add(j)
                    t_suc[j].add(i)
                else:
                    g_pre[group[i]].add(group[j])
                    g_suc[group[j]].add(group[i])
        # topological sort the groups
        groups_order = topo_sort([i for i in group2item], g_pre, g_suc)
        # topological sort the items in each group
        t_order = []
        for i in groups_order:
            items = group2item[i]
            i_order = topo_sort(items, t_pre, t_suc)
            if len(i_order) != len(items):
                return []
            t_order += i_order
        return t_order if len(t_order) == n else []
```
</p>


