---
title: "Group Anagrams"
weight: 49
#id: "group-anagrams"
---
## Description
<div class="description">
<p>Given an array of strings <code>strs</code>, group <strong>the anagrams</strong> together. You can return the answer in <strong>any order</strong>.</p>

<p>An <strong>Anagram</strong> is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> strs = ["eat","tea","tan","ate","nat","bat"]
<strong>Output:</strong> [["bat"],["nat","tan"],["ate","eat","tea"]]
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> strs = [""]
<strong>Output:</strong> [[""]]
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> strs = ["a"]
<strong>Output:</strong> [["a"]]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= strs.length &lt;= 10<sup>4</sup></code></li>
	<li><code>0 &lt;= strs[i].length &lt;= 100</code></li>
	<li><code>strs[i]</code> consists of lower-case English letters.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- String (string)

## Companies
- Amazon - 28 (taggedByAdmin: true)
- Apple - 13 (taggedByAdmin: false)
- Goldman Sachs - 12 (taggedByAdmin: false)
- Facebook - 7 (taggedByAdmin: true)
- Microsoft - 6 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: true)
- Adobe - 2 (taggedByAdmin: false)
- Twilio - 2 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)
- Docusign - 2 (taggedByAdmin: false)
- Cisco - 2 (taggedByAdmin: false)
- Uber - 8 (taggedByAdmin: true)
- Mathworks - 5 (taggedByAdmin: false)
- Yandex - 4 (taggedByAdmin: false)
- Oracle - 4 (taggedByAdmin: false)
- Salesforce - 2 (taggedByAdmin: false)
- Tesla - 2 (taggedByAdmin: false)
- Affirm - 2 (taggedByAdmin: false)
- Yahoo - 9 (taggedByAdmin: false)
- Walmart Labs - 7 (taggedByAdmin: false)
- Wish - 4 (taggedByAdmin: false)
- Hulu - 3 (taggedByAdmin: false)
- Snapchat - 3 (taggedByAdmin: false)
- Booking.com - 3 (taggedByAdmin: false)
- Visa - 3 (taggedByAdmin: false)
- Electronic Arts - 2 (taggedByAdmin: false)
- Qualtrics - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Nutanix - 2 (taggedByAdmin: false)
- Intuit - 2 (taggedByAdmin: false)
- Yelp - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach 1: Categorize by Sorted String

**Intuition**

Two strings are anagrams if and only if their sorted strings are equal.

**Algorithm**

Maintain a map `ans : {String -> List}` where each key $$\text{K}$$ is a sorted string, and each value is the list of strings from the initial input that when sorted, are equal to $$\text{K}$$.

In Java, we will store the key as a string, eg. `code`.  In Python, we will store the key as a hashable tuple, eg. `('c', 'o', 'd', 'e')`.

![Anagrams](../Figures/49_groupanagrams1.png)

<iframe src="https://leetcode.com/playground/HwiBG7Pz/shared" frameBorder="0" width="100%" height="293" name="HwiBG7Pz"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(NK \log K)$$, where $$N$$ is the length of `strs`, and $$K$$ is the maximum length of a string in `strs`.  The outer loop has complexity $$O(N)$$ as we iterate through each string.  Then, we sort each string in $$O(K \log K)$$ time.

* Space Complexity: $$O(NK)$$, the total information content stored in `ans`.
<br />
<br />
---
#### Approach 2: Categorize by Count

**Intuition**

Two strings are anagrams if and only if their character counts (respective number of occurrences of each character) are the same.

**Algorithm**

We can transform each string $$\text{s}$$ into a character count, $$\text{count}$$, consisting of 26 non-negative integers representing the number of $$\text{a}$$'s, $$\text{b}$$'s, $$\text{c}$$'s, etc.  We use these counts as the basis for our hash map.

In Java, the hashable representation of our count will be a string delimited with '**#**' characters.  For example, `abbccc` will be `#1#2#3#0#0#0...#0` where there are 26 entries total.  In python, the representation will be a tuple of the counts.  For example, `abbccc` will be `(1, 2, 3, 0, 0, ..., 0)`, where again there are 26 entries total.

![Anagrams](../Figures/49_groupanagrams2.png)

<iframe src="https://leetcode.com/playground/DvDMzZTX/shared" frameBorder="0" width="100%" height="412" name="DvDMzZTX"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(NK)$$, where $$N$$ is the length of `strs`, and $$K$$ is the maximum length of a string in `strs`.  Counting each string is linear in the size of the string, and we count every string.

* Space Complexity: $$O(NK)$$, the total information content stored in `ans`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Share my short JAVA solution
- Author: legendaryengineer
- Creation Date: Wed Sep 16 2015 22:19:06 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 30 2020 14:28:10 GMT+0800 (Singapore Standard Time)

<p>
```
    public List<List<String>> groupAnagrams(String[] strs) {
        if (strs == null || strs.length == 0) return new ArrayList<>();
        Map<String, List<String>> map = new HashMap<>();
        for (String s : strs) {
            char[] ca = s.toCharArray();
            Arrays.sort(ca);
            String keyStr = String.valueOf(ca);
            if (!map.containsKey(keyStr)) map.put(keyStr, new ArrayList<>());
            map.get(keyStr).add(s);
        }
        return new ArrayList<>(map.values());
    }
```

Instead of sorting, we can also build the key string in this way. Thanks @davidluoyes for pointing this out.

```
    public List<List<String>> groupAnagrams(String[] strs) {
        if (strs == null || strs.length == 0) return new ArrayList<>();
        Map<String, List<String>> map = new HashMap<>();
        for (String s : strs) {
            char[] ca = new char[26];
            for (char c : s.toCharArray()) ca[c - \'a\']++;
            String keyStr = String.valueOf(ca);
            if (!map.containsKey(keyStr)) map.put(keyStr, new ArrayList<>());
            map.get(keyStr).add(s);
        }
        return new ArrayList<>(map.values());
    }
```
</p>


### C++ unordered_map and counting sort
- Author: jianchao-li
- Creation Date: Mon Aug 10 2015 10:49:33 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 11:27:03 GMT+0800 (Singapore Standard Time)

<p>
Use an `unordered_map` to group the strings by their sorted counterparts. Use the sorted string as the key and all anagram strings as the value.

```cpp
class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        unordered_map<string, vector<string>> mp;
        for (string s : strs) {
            string t = s; 
            sort(t.begin(), t.end());
            mp[t].push_back(s);
        }
        vector<vector<string>> anagrams;
        for (auto p : mp) { 
            anagrams.push_back(p.second);
        }
        return anagrams;
    }
};
```

Moreover, since the string only contains lower-case alphabets, we can sort them using counting sort to improve the time complexity.

```cpp
class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        unordered_map<string, vector<string>> mp;
        for (string s : strs) {
            mp[strSort(s)].push_back(s);
        }
        vector<vector<string>> anagrams;
        for (auto p : mp) { 
            anagrams.push_back(p.second);
        }
        return anagrams;
    }
private:
    string strSort(string s) {
        int counter[26] = {0};
        for (char c : s) {
            counter[c - \'a\']++;
        }
        string t;
        for (int c = 0; c < 26; c++) {
            t += string(counter[c], c + \'a\');
        }
        return t;
    }
};
```
</p>


### 5-line Python solution, easy to understand
- Author: kitt
- Creation Date: Tue Mar 15 2016 15:48:30 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 13:33:36 GMT+0800 (Singapore Standard Time)

<p>
    def groupAnagrams(self, strs):
        d = {}
        for w in sorted(strs):
            key = tuple(sorted(w))
            d[key] = d.get(key, []) + [w]
        return d.values()
</p>


