---
title: "Lonely Pixel I"
weight: 500
#id: "lonely-pixel-i"
---
## Description
<div class="description">
<p>Given a picture consisting of black and white pixels, find the number of <b>black</b> lonely pixels.</p>

<p>The picture is represented by a 2D char array consisting of 'B' and 'W', which means black and white pixels respectively. </p>

<p>A black lonely pixel is character 'B' that located at a specific position where the same row and same column don't have any other black pixels.</p>

<p><b>Example:</b><br />
<pre>
<b>Input:</b> 
[['W', 'W', 'B'],
 ['W', 'B', 'W'],
 ['B', 'W', 'W']]

<b>Output:</b> 3
<b>Explanation:</b> All the three 'B's are black lonely pixels.
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The range of width and height of the input 2D array is [1,500].</li>
</ol>
</p>
</div>

## Tags
- Array (array)
- Depth-first Search (depth-first-search)

## Companies
- Google - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(nm) time with O(n+m) Space and O(1) Space Solutions
- Author: compton_scatter
- Creation Date: Sun Mar 05 2017 12:05:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 08 2018 15:53:24 GMT+0800 (Singapore Standard Time)

<p>
O(nm) Time, O(n+m) Space Solution:

```
public int findLonelyPixel(char[][] picture) {
    int n = picture.length, m = picture[0].length;
    
    int[] rowCount = new int[n], colCount = new int[m];
    for (int i=0;i<n;i++) 
        for (int j=0;j<m;j++) 
            if (picture[i][j] == 'B') { rowCount[i]++; colCount[j]++; }

    int count = 0;
    for (int i=0;i<n;i++) 
        for (int j=0;j<m;j++) 
            if (picture[i][j] == 'B' && rowCount[i] == 1 && colCount[j] == 1) count++;
                
    return count;
}
```

O(nm) Time, O(1) Space Solution:

```
public int findLonelyPixel(char[][] picture) {
    int n = picture.length, m = picture[0].length;
    
    int firstRowCount = 0;
    for (int i=0;i<n;i++) 
        for (int j=0;j<m;j++) 
            if (picture[i][j] == 'B') {   
                if (picture[0][j] < 'Y' && picture[0][j] != 'V') picture[0][j]++;
                if (i == 0) firstRowCount++;
                else if (picture[i][0] < 'Y' && picture[i][0] != 'V') picture[i][0]++;
            }

    int count = 0;
    for (int i=0;i<n;i++) 
        for (int j=0;j<m;j++) 
            if (picture[i][j] < 'W' && (picture[0][j] == 'C' || picture[0][j] == 'X')) { 
                if (i == 0) count += firstRowCount == 1 ? 1 : 0;
                else if (picture[i][0] == 'C' || picture[i][0] == 'X') count++;
            }
                
    return count;
}
```
</p>


### 1-liner Python
- Author: StefanPochmann
- Creation Date: Sun Mar 05 2017 18:41:32 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 05 2017 18:41:32 GMT+0800 (Singapore Standard Time)

<p>
Go through the columns, count how many have exactly one black pixel and it's in a row that also has exactly one black pixel.

    def findLonelyPixel(self, picture):
        return sum(col.count('B') == 1 == picture[col.index('B')].count('B') for col in zip(*picture))
</p>


### Java O(mn) time, O(m) space. 28ms
- Author: lusoul
- Creation Date: Sun Mar 05 2017 12:40:40 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 08 2018 15:53:18 GMT+0800 (Singapore Standard Time)

<p>
thought is very simple, we can easily count how many times B occurs in each row. But how can we know if this col has existing B?
for example, input is 
W B B B
B W W W
W W W B
W W W B
we can maintain an array calls colArray[], which is used to record how many times the B occurs in each column. Then solution is simple
```
public class Solution {
    public int findLonelyPixel(char[][] picture) {
        if (picture == null || picture.length == 0 || picture[0].length == 0) return 0;

        int[] colArray = new int[picture[0].length];
        for (int i = 0; i < picture.length; i++) {
            for (int j = 0; j < picture[0].length; j++) {
                if (picture[i][j] == 'B') colArray[j]++;
            }
        }

        int ret = 0;
        for (int i = 0; i < picture.length; i++) {
            int count = 0, pos = 0;
            for (int j = 0; j < picture[0].length; j++) {
                if (picture[i][j] == 'B') {
                    count++;
                    pos = j;
                }
            }
            if (count == 1 && colArray[pos] == 1) ret++;
        }
        return ret;
    }
}
```
</p>


