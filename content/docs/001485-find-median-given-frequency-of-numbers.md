---
title: "Find Median Given Frequency of Numbers"
weight: 1485
#id: "find-median-given-frequency-of-numbers"
---
## Description
<div class="description">
<p>The <code>Numbers</code> table keeps the value of number and its frequency.</p>

<pre>
+----------+-------------+
|  Number  |  Frequency  |
+----------+-------------|
|  0       |  7          |
|  1       |  1          |
|  2       |  3          |
|  3       |  1          |
+----------+-------------+
</pre>

<p>In this table, the numbers are <code>0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 2, 3</code>, so the median is <code>(0 + 0) / 2 = 0</code>.</p>

<pre>
+--------+
| median |
+--------|
| 0.0000 |
+--------+
</pre>

<p>Write a query to find the median of all numbers and name the result as <code>median</code>.</p>

</div>

## Tags


## Companies
- Pinterest - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy peasy
- Author: yauheni
- Creation Date: Mon Sep 25 2017 11:39:07 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 11:21:10 GMT+0800 (Singapore Standard Time)

<p>
My solution, I think, is super simple.
```
select  avg(n.Number) median
from Numbers n
where n.Frequency >= abs((select sum(Frequency) from Numbers where Number<=n.Number) -
                         (select sum(Frequency) from Numbers where Number>=n.Number))
```
Explanation: 
Let's take all numbers from left including current number and then do same for right.
(select sum(Frequency) from Numbers where Number<=n.Number) as left
(select sum(Frequency) from Numbers where Number<=n.Number) as right
Now if difference between Left and Right less or equal to Frequency of the current number that means this number is median. 
Ok, what if we get two numbers satisfied this condition? Easy peasy - take AVG(). Ta-da!
</p>


### Simple solution, NO join, ONE subquery
- Author: lolozo
- Creation Date: Mon Sep 11 2017 05:15:41 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 19:35:20 GMT+0800 (Singapore Standard Time)

<p>
```
select avg(number) as median 
from (
    select l.number
    from numbers l join numbers r
    group by 1
    having abs(sum(sign(l.number - r.number) * r.frequency)) <= max(l.frequency)
) t
;
```

Key is to understand the having clause:
```
having abs(sum(sign(l.number - r.number) * r.frequency)) <= max(l.frequency)
```

Explain:
If a number is a median, it's frequency must be greater or equal than the diff of total frequency of numbers greater or less than itself.

Examples for the sub/inner query:
**Example 1:**
```
+----------+-------------+
|  Number  |  Frequency  | 
+----------+-------------|
|  0       |  5          |
|  1       |  1          |
|  2       |  5          |
+----------+-------------+
for 0, greater numbers are 1,2, total frequency is 6, 
       smaller numbers are    , total frequency is 0,
       diff is 6
       it's own frequency is 5
       5 >= 6 == false
for 1, greater numbers are 2  , total frequency is 5, 
       smaller numbers are 0  , total frequency is 5,
       diff is 0
       it's own frequency is 1
       1 >= 0 == true
for 2, same as for 0
So [1] is selected
```
**Example 2:**
```
+----------+-------------+
|  Number  |  Frequency  | 
+----------+-------------|
|  0       |  4          |
|  1       |  1          |
|  2       |  5          |
+----------+-------------+
for 0, greater numbers are 1,2, total frequency is 6, 
       smaller numbers are    , total frequency is 0,
       diff is 6
       it's own frequency is 4
       4 >= 6 == false
for 1, greater numbers are 2  , total frequency is 5, 
       smaller numbers are 0  , total frequency is 4,
       diff is 1
       it's own frequency is 1
       1 >= 1 == true
for 2, greater numbers are    , total frequency is 0, 
       smaller numbers are 0,1, total frequency is 5,
       diff is 5
       it's own frequency is 5
       5 >= 5 == true
So [1,2] is selected
```
</p>


### So far the easiest way in mssql
- Author: miazhang
- Creation Date: Thu Apr 09 2020 11:12:26 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 09 2020 11:12:26 GMT+0800 (Singapore Standard Time)

<p>
```
WITH tmp AS (SELECT Number, Frequency,
             SUM(Frequency) OVER (ORDER BY Number ASC) rk1,
             SUM(Frequency) OVER (ORDER BY Number DESC) rk2
             FROM Numbers)
  
SELECT AVG(Number*1.0) AS median
FROM tmp
WHERE ABS(rk1-rk2)<=Frequency
```
</p>


