---
title: "Maximum Binary Tree"
weight: 586
#id: "maximum-binary-tree"
---
## Description
<div class="description">
<p>
Given an integer array with no duplicates. A maximum tree building on this array is defined as follow:
<ol>
<li>The root is the maximum number in the array. </li>
<li>The left subtree is the maximum tree constructed from left part subarray divided by the maximum number.</li>
<li>The right subtree is the maximum tree constructed from right part subarray divided by the maximum number.</li> 
</ol>
</p>

<p>
Construct the maximum tree by the given array and output the root node of this tree.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> [3,2,1,6,0,5]
<b>Output:</b> return the tree root node representing the following tree:

      6
    /   \
   3     5
    \    / 
     2  0   
       \
        1
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The size of the given array will be in the range [1,1000].</li>
</ol>
</p>
</div>

## Tags
- Tree (tree)

## Companies
- Google - 2 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]


## Solution

---
#### Approach 1: Recursive Solution

The current solution is very simple. We make use of a function `construct(nums, l, r)`, which returns the maximum binary tree consisting of numbers within the indices $$l$$ and $$r$$ in the given $$nums$$ array(excluding the $$r^{th}$$ element).

The algorithm consists of the following steps:

1. Start with the function call `construct(nums, 0, n)`. Here, $$n$$ refers to the number of elements in the given $$nums$$ array.

2. Find the index, $$max_i$$, of the largest element in the current range of indices $$(l:r-1)$$. Make this largest element, $$nums[max\_i]$$ as the local root node.

3. Determine the left child using `construct(nums, l, max_i)`. Doing this recursively finds the largest element in the subarray left to the current largest element.

4. Similarly, determine the right child using `construct(nums, max_i + 1, r)`.

5. Return the root node to the calling function.

<iframe src="https://leetcode.com/playground/pi6yQuYg/shared" frameBorder="0" width="100%" height="429" name="pi6yQuYg"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. The function `construct` is called $$n$$ times. At each level of the recursive tree, we traverse over all the $$n$$ elements to find the maximum element.  In the average case, there will be a $$\log n$$ levels leading to a complexity of $$O\big(n\log n\big)$$. In the worst case, the depth of the recursive tree can grow upto $$n$$, which happens in the case of a sorted $$nums$$ array, giving a complexity of $$O(n^2)$$.

* Space complexity : $$O(n)$$. The size of the $$set$$ can grow upto $$n$$ in the worst case. In the average case, the size will be $$\log n$$ for $$n$$ elements in $$nums$$, giving an average case complexity of $$O(\log n)$$

## Accepted Submission (java)
```java
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    private int findMax(int[] arr, int start, int stop) {
        int v = Integer.MIN_VALUE;
        int p = start;
        for (int i = start; i < stop; i++) {
            if (arr[i] > v) {
                p = i;
                v = arr[i];
            }
        }
        return p;
    }
    public TreeNode construct(int[] nums, int start, int stop) {
        int rp = findMax(nums, start, stop);
        TreeNode r = new TreeNode(nums[rp]);
        if (rp == start) {
            r.left = null;
        } else if (rp == start + 1) {
            r.left = new TreeNode(nums[start]);
        } else {
            r.left = construct(nums, start, rp);
        }
        if (rp == stop - 1) {
            r.right = null;
        } else if (rp == stop - 2) {
            r.right = new TreeNode(nums[stop - 1]);
        } else {
            r.right = construct(nums, rp + 1, stop);
        }
        return r;
    }

    public TreeNode constructMaximumBinaryTree(int[] nums) {
        return construct(nums, 0, nums.length);
    }
}
```

## Top Discussions
### C++ O(N) solution
- Author: Mrsuyi
- Creation Date: Sun Aug 06 2017 23:13:37 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 06:54:22 GMT+0800 (Singapore Standard Time)

<p>
This solution is inspired by @votrubac
Here is his brilliant solution
https://discuss.leetcode.com/topic/98454/c-9-lines-o-n-log-n-map

The key idea is:
1. We scan numbers from left to right, build the tree one node by one step;
2. We use a stack to keep some (not all) tree nodes and ensure a decreasing order;
3. For each number, we keep pop the stack until empty or a bigger number; The bigger number (if exist, it will be still in stack) is current number's root, and the last popped number (if exist) is current number's left child (temporarily, this relationship may change in the future); Then we push current number into the stack.

```
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* constructMaximumBinaryTree(vector<int>& nums) {
        vector<TreeNode*> stk;
        for (int i = 0; i < nums.size(); ++i)
        {
            TreeNode* cur = new TreeNode(nums[i]);
            while (!stk.empty() && stk.back()->val < nums[i])
            {
                cur->left = stk.back();
                stk.pop_back();
            }
            if (!stk.empty())
                stk.back()->right = cur;
            stk.push_back(cur);
        }
        return stk.front();
    }
};
```

This solution will be slightly faster than normal recursive solution.

Again, great thanks to @votrubac  !!!
</p>


### Java worst case O(N) solution
- Author: Claraaa
- Creation Date: Wed Oct 04 2017 11:08:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 20:13:47 GMT+0800 (Singapore Standard Time)

<p>
Each node went into stack once, and went out stack once. Worst case time complexity O(N).

```
class Solution {
    public TreeNode constructMaximumBinaryTree(int[] nums) {
        Deque<TreeNode> stack = new LinkedList<>();
        for(int i = 0; i < nums.length; i++) {
            TreeNode curr = new TreeNode(nums[i]);
            while(!stack.isEmpty() && stack.peek().val < nums[i]) {
                curr.left = stack.pop();
            }
            if(!stack.isEmpty()) {
                stack.peek().right = curr;
            }
            stack.push(curr);
        }
        
        return stack.isEmpty() ? null : stack.removeLast();
    }
}
</p>


### poor quality question
- Author: waitingtodie
- Creation Date: Fri Mar 09 2018 08:18:00 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 03:34:39 GMT+0800 (Singapore Standard Time)

<p>
The question does not clearly specify the requirements.

At no point does it clearly explain how to construct the tree from the array. Normally when a description is poor, you may be be able to work it out from the examples - but the single example provided is lacking too.

I suggest whoever wrote the question study plain english.
</p>


