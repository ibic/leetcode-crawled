---
title: "Reverse String II"
weight: 509
#id: "reverse-string-ii"
---
## Description
<div class="description">
</p>
Given a string and an integer k, you need to reverse the first k characters for every 2k characters counting from the start of the string. If there are less than k characters left, reverse all of them. If there are less than 2k but greater than or equal to k characters, then reverse the first k characters and left the other as original.
</p>

<p><b>Example:</b><br />
<pre>
<b>Input:</b> s = "abcdefg", k = 2
<b>Output:</b> "bacdfeg"
</pre>
</p>

<b>Restrictions:</b> </b>
<ol>
<li> The string consists of lower English letters only.</li>
<li> Length of the given string and k will in the range [1, 10000]</li>
</ol>
</div>

## Tags
- String (string)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Direct [Accepted]

**Intuition and Algorithm**

We will reverse each block of `2k` characters directly.

Each block starts at a multiple of `2k`: for example, `0, 2k, 4k, 6k, ...`.  One thing to be careful about is we may not reverse each block if there aren't enough characters.

To reverse a block of characters from `i` to `j`, we can swap characters in positions `i++` and `j--`.

<iframe src="https://leetcode.com/playground/GRuDtVaZ/shared" frameBorder="0" width="100%" height="293" name="GRuDtVaZ"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the size of `s`.  We build a helper array, plus reverse about half the characters in `s`.

* Space Complexity: $$O(N)$$, the size of `a`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java Concise Solution
- Author: Shevchenko_7
- Creation Date: Sun Mar 12 2017 15:17:57 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 10:41:22 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    public String reverseStr(String s, int k) {
        char[] arr = s.toCharArray();
        int n = arr.length;
        int i = 0;
        while(i < n) {
            int j = Math.min(i + k - 1, n - 1);
            swap(arr, i, j);
            i += 2 * k;
        }
        return String.valueOf(arr);
    }
    private void swap(char[] arr, int l, int r) {
        while (l < r) {
            char temp = arr[l];
            arr[l++] = arr[r];
            arr[r--] = temp;
        }
    }
}
```
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun Mar 12 2017 12:46:56 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 09:53:57 GMT+0800 (Singapore Standard Time)

<p>
For every block of 2k characters starting with position i, we want to replace S[i:i+k] with it's reverse.
```
def reverseStr(self, s, k):
    s = list(s)
    for i in xrange(0, len(s), 2*k):
        s[i:i+k] = reversed(s[i:i+k])
    return "".join(s)
```
</p>


### [C++][Java] Clean Code
- Author: alexander
- Creation Date: Sun Mar 12 2017 15:24:24 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 00:22:37 GMT+0800 (Singapore Standard Time)

<p>
**C++**
```
class Solution {
public:
    /**
     * 0            k           2k          3k
     * |-----------|-----------|-----------|---
     * +--reverse--+           +--reverse--+
     */
    string reverseStr(string s, int k) {
        for (int left = 0; left < s.size(); left += 2 * k) {
            for (int i = left, j = min(left + k - 1, (int)s.size() - 1); i < j; i++, j--) {
                swap(s[i], s[j]);
            }
        }
        return s;
    }
};
```

**Java**
```
public class Solution {
    public String reverseStr(String s, int k) {
        char[] ca = s.toCharArray();
        for (int left = 0; left < ca.length; left += 2 * k) {
            for (int i = left, j = Math.min(left + k - 1, ca.length - 1); i < j; i++, j--) {
                char tmp = ca[i];
                ca[i] = ca[j];
                ca[j] = tmp;
            }
        }
        return new String(ca);
    }
}
```
</p>


