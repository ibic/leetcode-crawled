---
title: "Implement Rand10() Using Rand7()"
weight: 817
#id: "implement-rand10-using-rand7"
---
## Description
<div class="description">
<p>Given the <strong>API</strong> <code>rand7()</code> that generates a uniform random integer in the range <code>[1, 7]</code>, write a function <code>rand10()</code> that generates a uniform random integer in the range <code>[1, 10]</code>. You can only call the API <code>rand7()</code>, and you shouldn&#39;t call any other API. Please <strong>do not</strong> use a language&#39;s built-in random API.</p>

<p>Each test case will have one <strong>internal</strong> argument <code>n</code>, the number of times that your implemented function <code>rand10()</code> will be called while testing. Note that this is <strong>not an argument</strong> passed to <code>rand10()</code>.</p>

<p><strong>Follow up:</strong></p>

<ul>
	<li>What is the <a href="https://en.wikipedia.org/wiki/Expected_value" target="_blank">expected value</a>&nbsp;for the number of calls to&nbsp;<code>rand7()</code>&nbsp;function?</li>
	<li>Could you minimize the number of calls to <code>rand7()</code>?</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> n = 1
<strong>Output:</strong> [2]
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> n = 2
<strong>Output:</strong> [2,8]
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> n = 3
<strong>Output:</strong> [3,8,10]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10<sup>5</sup></code></li>
</ul>

</div>

## Tags
- Random (random)
- Rejection Sampling (rejection-sampling)

## Companies
- LinkedIn - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Rejection Sampling

**Intuition**

What if you could generate a random integer in the range 1 to 49? How would you generate a random integer in the range of 1 to 10? What would you do if the generated number is in the desired range? What if it is not?

**Algorithm**

This solution is based upon [Rejection Sampling](https://en.wikipedia.org/wiki/Rejection_sampling). The main idea is when you generate a number in the desired range, output that number immediately. If the number is out of the desired range, reject it and re-sample again. As each number in the desired range has the same probability of being chosen, a uniform distribution is produced.

Obviously, we have to run rand7() function at least twice, as there are not enough numbers in the range of 1 to 10. By running rand7() twice, we can get integers from 1 to 49 uniformly. Why?

<br/>

<p align="center">
<img src="../Figures/470/rejectionSamplingTable.png" alt="rejectionSamplingTable" style="height: 300px;"/>

<br/>

A table is used to illustrate the concept of rejection sampling. Calling rand7() twice will get us row and column index that corresponds to a unique position in the table above. Imagine that you are choosing a number randomly from the table above. If you hit a number, you return that number immediately. If you hit a * , you repeat the process again until you hit a number.
</p>

Since 49 is not a multiple of 10, we have to use rejection sampling. Our desired range is integers from 1 to 40, which we can return the answer immediately. If not (the integer falls between 41 to 49), we reject it and repeat the whole process again.

<iframe src="https://leetcode.com/playground/K5FVm2EL/shared" frameBorder="0" width="100%" height="259" name="K5FVm2EL"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(1)$$ average, but  $$O(\infty)$$ worst case.

The [expected value](https://en.wikipedia.org/wiki/Expected_value) for the number of calls to rand7() can be computed as follows:

![formula 1](../Figures/470/470_formula_1.png)

* Space Complexity: $$O(1)$$.

<br/>

---

#### Approach 2: Utilizing out-of-range samples

**Intuition**

There are a total of 2.45 calls to rand7() on average when using approach 1. Can we do better? Glad that you asked. In fact, we are able to improve average number of calls to rand7() by about 10%.

The idea is that we should not throw away the out-of-range samples, but instead use them to increase our chances of finding an in-range sample on the successive call to rand7.

**Algorithm**

Start by generating a random integer in the range 1 to 49 using the aforementioned method. In the event that we could not generate a number in the desired range (1 to 40), it is equally likely that each number of 41 to 49 would be chosen. In other words, we are able to obtain integers in the range of 1 to 9 uniformly. Now, run rand7() again to obtain integers in the range of 1 to 63 uniformly. Apply rejection sampling where the desired range is 1 to 60. If the generated number is in the desired range (1 to 60), we return the number. If it is not (61 to 63), we at least obtain integers of 1 to 3 uniformly. Run rand7() again to obtain integers in the range of 1 to 21 uniformly. The desired range is 1 to 20, and in the unlikely event we get a 21, we reject it and repeat the entire process again.

<iframe src="https://leetcode.com/playground/Ly87c5HR/shared" frameBorder="0" width="100%" height="480" name="Ly87c5HR"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(1)$$ average, but  $$O(\infty)$$ worst case.

The [expected value](https://en.wikipedia.org/wiki/Expected_value) for the number of calls to rand7() can be computed as follows (with some steps omitted due to tediousness):

![formula 2](../Figures/470/470_formula_2.png)

* Space Complexity: $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Three-line Java solution, the idea can be generalized to "Implement RandM() Using RandN()"
- Author: hwy_2015
- Creation Date: Mon Jul 16 2018 11:46:10 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 14:14:09 GMT+0800 (Singapore Standard Time)

<p>
Idea: ```rand7()``` -> ```rand49()``` -> ```rand40()``` -> ```rand10()```
```
public int rand10() {
    int result = 40;
    while (result >= 40) {result = 7 * (rand7() - 1) + (rand7() - 1);}
    return result % 10 + 1;
}
```
#
### **Time Complexity**
#
The total number of iterations follows **geometric distribution**. For each iteration in the ```while``` loop, the probabilty of exiting the loop is *p* = 40/49. So the average time complexity T(n) = O(1/*p*) = O(49/40) = O(1).
#
### **Correctness**
#
Note that rand49() generates a uniform random integer in [1, 49], so any number in this range has the same probability to be generated. Suppose *k* is an integer in range [1, 40], *P*(```rand49()``` = *k*) = 1/49.

&nbsp;&nbsp;&nbsp;*P*(result = *k*)
= *P*(```rand49()``` = *k* in the 1st iteration) +
&nbsp;&nbsp;&nbsp;*P*(```rand49()``` > 40 in the 1st iteration) * *P*(```rand49()``` = *k* in the 2nd iteration) +
&nbsp;&nbsp;&nbsp;*P*(```rand49()``` > 40 in the 1st iteration) * *P*(```rand49()``` > 40 in the 2nd iteration) * *P*(```rand49()``` = *k* in the 3rd iteration) +
&nbsp;&nbsp;&nbsp;*P*(```rand49()``` > 40 in the 1st iteration) * *P*(```rand49()``` > 40 in the 2nd iteration) * *P*(```rand49()``` > 40 in the 3rd iteration) * *P*(```rand49()``` = *k* in the 4th iteration) +
&nbsp;&nbsp;&nbsp;...
= (1/49) + (9/49) * (1/49) + (9/49)^2 * (1/49) + (9/49)^3 * (1/49) + ...
= (1/49) * [1 + (9/49) + (9/49)^2 + (9/49)^3 + ... ]
= (1/49) * [1/(1-9/49)]
= (1/49) * (49/40)
= 1/40
#
### **Generalization**
#
Implement ```randM()``` using ```randN()``` when M > N:
**Step 1:** Use ```randN()``` to generate ```randX()```, where X >= M. In this problem,  I use ```7 * (rand7() - 1) + (rand7() - 1)``` to generate ```rand49() - 1```.
**Step 2:** Use ```randX()``` to generate ```randM()```. In this problem,  I use ```rand49()``` to generate ```rand40()``` then generate ```rand10```.

**Note:** ```N^b * (randN() - 1) + N^(b - 1) * (randN() - 1) + N^(b - 2) * (randN() - 1) + ... + N^0 * (randN() - 1)``` generates ```randX() - 1```, where ```X = N^(b + 1)```.
#
### **More Examples**
#
(1) Implement ```rand11()``` using ```rand3()```: 
```
public int rand11() {
    int result = 22;
    while (result >= 22) {result = 3 * 3 * (rand3() - 1) + 3 * (rand3() - 1) + (rand3() - 1);}
    return result % 11 + 1;
}
```
Idea: ```rand3()``` -> ```rand27()``` -> ```rand22``` -> ```rand11```
Time Comlexity: O(27/22)
# 
(2) Implement ```rand9()``` using ```rand7()```: 
```
public int rand9() {
    int result = 45;
    while (result >= 45) {result = 7 * (rand7() - 1) + (rand7() - 1);}
    return result % 9 + 1;
}
```
Idea: ```rand7()``` -> ```rand49()``` -> ```rand45()``` -> ```rand9()```
Time Comlexity: O(49/45)
# 
(3) Implement ```rand13()``` using ```rand6()```: 
```
public int rand13() {
    int result = 26;
    while (result >= 26) {result = 6 * (rand6() - 1) + (rand6() - 1);}
    return result % 13 + 1;
}
```
Idea: ```rand6()``` -> ```rand36()``` -> ```rand26``` -> ```rand13()```
Time Comlexity: O(36/26)
</p>


### [C++/Java/Python] 1.183 Call of rand7 Per rand10
- Author: lee215
- Creation Date: Fri Jul 20 2018 01:23:00 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 02 2020 22:04:32 GMT+0800 (Singapore Standard Time)

<p>
# Solution 0: Easy Solution with random 49

`rand7()` will get random 1 ~ 7
`(rand7() - 1) * 7 + rand7() - 1` will get random 0 ~ 48
We discard `40 ~ 48`, now we have `rand40` equals to random 0 ~ 39.
We just need to return `rand40 % 10 + 1` and we get random 1 ~ 10.

In `9/49` cases, we need to start over again.
In `40/49` cases, we call `rand7()` two times.

Overall, we need `49/40*2 = 2.45` calls of `rand7()` per `rand10()`.


**Java:**
```java
    public int rand10() {
        int rand40 = 40;
        while (rand40 >= 40) {
            rand40 = (rand7() - 1) * 7 + rand7() - 1;
        }
        return rand40 % 10 + 1;
    }
```

**C++:**
```cpp
    int rand10() {
        int rand40 = 40;
        while (rand40 >= 40) {
            rand40 = (rand7() - 1) * 7 + rand7() - 1;
        }
        return rand40 % 10 + 1;
    }
```

**Python:**
```python
    def rand10(self):
        rand40 = 40
        while rand40 >= 40:
            rand40 = (rand7() - 1) * 7 + rand7() - 1
        return rand40 % 10 + 1
```
<br>

# **Intuition of Improvement**:
Solution 0 is a good answer and you may pass a interview with this solution.
The average call of `rand7` here is 2.45 calls.

However, we may think about, what is the limit?
Is that possible to get an average of 2 calls.
<br>

# **What is the Limit**
It may seem impossible, but unfortunately, even average 2 is still far from the best answer.

The problem is that you generate 49 random states, waste 9 of them.
And we arrange the rest 40 states into 10 states.
You can see that in that solution,
80% of random states waste and we satisfy with only 20% efficiency.

Did a quick math for the limit `log10 / log7 = 1.1833`,
which lead me to find the following solution.
<br>

# **Solution 1: instead of 49, we use bigger pow of 7**:

`rand10()` will consume the generated random integer from stack `cache`.
If `cache` is empty, it will call function `generate()`.

In `generate()`, it will generate an integer in range `[0, 7^19]`.
7^19 = 11398895185373143, and close to an pow of 10.
So in 11398895185373140 / 11398895185373143 = 99.99999999999997% cases, it will generate at least 1 integer.
And in 10000000000000000 / 11398895185373143 = 87.73% cases, it will generate 16 integers.

`N = 19` is the best we can choose in long integer range,
and `N = 7` is another good choice for 32 bits integer range.

This solution got average 1.199 calls, it\'s really close to theoretic limit.

**C++:**
```cpp
    stack<int> cache;
    int rand10() {
        while (cache.size() == 0) generate();
        int res = cache.top(); cache.pop();
        return res;
    }

    void generate() {
        int n = 19;
        long cur = 0, range = long(pow(7, n));
        for (int i = 0; i < n; ++i) cur += long(pow(7, i)) * (rand7() - 1);
        while (cur < range / 10 * 10) {
            cache.push(cur % 10 + 1);
            cur /= 10;
            range /= 10;
        }
    }
```

**Java:**
```java
    Stack<Integer> cache = new Stack<Integer>();
    public int rand10() {
        while (cache.empty()) generate();
        return cache.pop();
    }

    void generate() {
        int n = 19;
        long cur = 0, range = (long)Math.pow(7, n);
        for (int i = 0; i < n; ++i) cur += (long)Math.pow(7, i) * (rand7() - 1);
        while (cur < range / 10 * 10) {
            cache.push((int)(cur % 10 + 1));
            cur /= 10;
            range /= 10;
        }
    }
```
**Python:**
```python
    cache = []
    def rand10(self):
        while not self.cache: self.generate()
        return self.cache.pop()

    def generate(self):
        n = 19  # 1.199
        cur = sum((self.rand7() - 1) * (7**i) for i in range(n))
        rang = 7 ** n
        while cur < rang / 10 * 10:
            self.cache.append(cur % 10 + 1)
            cur /= 10
            rang /= 10
```


# Solution 2: No need to decide the pow in advance
Someone may have a concern,
how can we find a magic pow like 19 or 7?

Actually, we don\'t need to.
Continue to improve the solution 1,
we cache a big random number.
And when we need a rand10, we take a part from it.

**Python**
```py
    cache, upper = 0, 1
    def rand10(self):
        cache, upper = self.cache, self.upper
        while upper < 10**9:
            cache, upper = cache * 7 + self.rand7() - 1, upper * 7
        res = cache % 10 + 1
        self.cache, self.upper = cache / 10, upper / 10
        self.count10 += 1  # test
        self.count[res] += 1  # test
        return res
```
This solution will achieve 1.183 calls of `rand7()` per `rand10()`.

</p>


### In-depth straightforward detailed explanation. (Short Java solution)
- Author: jolyon129
- Creation Date: Fri Jul 19 2019 05:21:36 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 31 2020 10:01:22 GMT+0800 (Singapore Standard Time)

<p>
Inspired by Vivekanand Kiruban from [link](https://www.quora.com/How-do-you-design-a-rand7-function-using-a-rand5-function).

There are three concepts in geometric distribution you need to know before you solve these kind of questions. They are very easy to understand.

# Expanding a zero-based `random`

Let\'s say you have a `random10` which return a number from 0 to 9. How would you build `random100` which return from 0 to 99? 
You can call `random10` to generate each digit.  Specifically, `10*random10()` will generate the tens\' place for a two digit number, and `1*random10()` will generate the ones\' place. Then the biggest number `10*random10() + 1*random10()`  could produce is 10*9+1*9 = 99. The smallest number is `0`
```
random100 = 10*random10() + 1*random10()
random1000 = 100*random10() + 10*random10() + 1*random10()
```

If we change our definition of `random`, now `random10` will return a number from 1 to 10. Can we use the above formula to generate `random100`(1 to 100)? 

The answer is **NO**.

If you think about it, the `10*random10() + 1*random10()` can only return a number from ~~2 to 101~~ 11 to 110., instead of 1 to 100.

You need to tweak the formula a little bit. Specifically, you need to zero-based your `random`.

```
random100 = 10*(random10()-1) + 1*(random10()-1) + 1
```

Notice that we need to `add 1` in the end because `10*(random10()-1) + 1*(random10()-1)` will result in a number from 0 to 99, and what we want is actually 1 to 100.

# Discarding and Rolling again
If you have a 6 sided dice and want to use it determine "Head or Tail" using this side, and if you get 1, say heads, if you get 2, say tails, and if you get anything else, you discard them by rolling again. This approach will guarantee an equivalent to H/T, but notice you may be rolling the die many times.

# Folding to Map
In the above example of dice, an another solution you have is mapping the other values into `H` or `T`. For example, 1,2,3 (= heads), 4,5,6 (= tails). When it comes to implementation, we can use module operator to do that.
E.g,
```
random3 = random6 % 3 // Assuming the random here is zero-based
```

# Use the three concepts
What we have right now is 1-based random7, and we need to generate random10. Here are the steps we gonna go through:
1. zero-based `random7`: `random7-1`(0..6)
2. expanding zero-based `random7` to zero-based `random49`: `(random7-1)*7 + (random7-1)`
3. discarding the number over 40 by rolling again, then we get zero-based `random40`: 
	```
	while(random40>=40){
		random40 = (random7-1)*7 + (random7-1)
	}
	```
4. folding zero-based `random40` to one-based `random10`
	```
	random10 = random40%10+1
	```


The complete code:
```
class Solution extends SolBase {
    public int rand10() {
        int rand40 = Integer.MAX_VALUE;
        while (rand40 >= 40) {
            rand40 = (rand7() - 1) * 7 + rand7() - 1;
        }
        return rand40 % 10 + 1;
    }
}
```
</p>


