---
title: "Minimum Absolute Difference"
weight: 1162
#id: "minimum-absolute-difference"
---
## Description
<div class="description">
<p>Given an&nbsp;array&nbsp;of <strong>distinct</strong>&nbsp;integers <code>arr</code>, find all pairs of elements with the minimum absolute difference of any two elements.&nbsp;</p>

<p>Return a list of pairs in ascending order(with respect to pairs), each pair <code>[a, b]</code> follows</p>

<ul>
	<li><code>a, b</code> are from <code>arr</code></li>
	<li><code>a &lt; b</code></li>
	<li><code>b - a</code>&nbsp;equals to the minimum absolute difference of any two elements in <code>arr</code></li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [4,2,1,3]
<strong>Output:</strong> [[1,2],[2,3],[3,4]]
<strong>Explanation: </strong>The minimum absolute difference is 1. List all pairs with difference equal to 1 in ascending order.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,3,6,10,15]
<strong>Output:</strong> [[1,3]]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [3,8,-10,23,19,-4,-14,27]
<strong>Output:</strong> [[-14,-10],[19,23],[23,27]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= arr.length &lt;= 10^5</code></li>
	<li><code>-10^6 &lt;= arr[i] &lt;= 10^6</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Audible - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python3] 3-liner
- Author: cenkay
- Creation Date: Sun Sep 22 2019 12:16:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 22 2019 12:16:30 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def minimumAbsDifference(self, arr: List[int]) -> List[List[int]]:
        arr.sort()
        mn = min(b - a for a, b in zip(arr, arr[1:]))
        return [[a, b] for a, b in zip(arr, arr[1:]) if b - a == mn]        
```
</p>


### Java, sorting, beats 100%, explained
- Author: gthor10
- Creation Date: Mon Sep 23 2019 08:14:41 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Nov 28 2019 00:02:59 GMT+0800 (Singapore Standard Time)

<p>
The simplest way to achieve requested result is to sort elements and then check difference of each pair of neighbouring elements. Because of the sort if you take any one element the minimum distance will be between it and the next element only, because every next element will be greater than a previous one - thus difference increases. 

The simplest approach to find the min difference is to scan array saving current min difference and update if we found lower one. Then the second pass required to find every pair with such difference.

However I\'d like to combine those two passes into one. We check for min difference as we go, adding to result list everytime our difference == min difference. When we met new min difference we clear the result list and start a new one.

O(nlgn) time - Onlg for sorting and On to form the result list. Space is O(1) - just several state variables. 

```
    public List<List<Integer>> minimumAbsDifference(int[] arr) {
        List<List<Integer>> res = new ArrayList();
        //sort elements
        Arrays.sort(arr);
        //init our min difference value
        int min = Integer.MAX_VALUE;
        //start looping over array to find real min element. Each time we found smaller difference
        //we reset resulting list and start building it from scratch. If we found pair with the same
        //difference as min - add it to the resulting list
        for (int i = 0; i < arr.length - 1; i++) {
            int diff = arr[i + 1] - arr[i];
            if (diff < min) {
                min = diff;
                res.clear();
                res.add(Arrays.asList(arr[i], arr[i + 1]));
            } else if (diff == min) {
                res.add(Arrays.asList(arr[i], arr[i + 1]));
            }
        }
        return res;
    }
```
</p>


### Easy C++ Solution
- Author: dnuang
- Creation Date: Mon Sep 23 2019 07:30:33 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 23 2019 07:40:59 GMT+0800 (Singapore Standard Time)

<p>
```
vector<vector<int>> minimumAbsDifference(vector<int>& arr) {
    sort(arr.begin(), arr.end());
    
    vector<vector<int>> result;
    int min_diff = INT_MAX;
    
    // find min difference
    for (int i = 0; i < arr.size() - 1; ++i) {
        min_diff = min(arr[i + 1] - arr[i], min_diff);
    }
    
    // form a list of pairs with min difference, ascending  
    for (int i = 0; i < arr.size() - 1; ++i) {
        if (arr[i + 1] - arr[i] == min_diff)
            result.push_back({arr[i], arr[i + 1]});
    }
    return result;        
}
```
</p>


