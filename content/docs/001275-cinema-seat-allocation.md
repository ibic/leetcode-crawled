---
title: "Cinema Seat Allocation"
weight: 1275
#id: "cinema-seat-allocation"
---
## Description
<div class="description">
<p><img alt="" src="https://assets.leetcode.com/uploads/2020/02/14/cinema_seats_1.png" style="width: 400px; height: 149px;" /></p>

<p>A cinema&nbsp;has <code>n</code>&nbsp;rows of seats, numbered from 1 to <code>n</code>&nbsp;and there are ten&nbsp;seats in each row, labelled from 1&nbsp;to 10&nbsp;as shown in the figure above.</p>

<p>Given the array <code>reservedSeats</code> containing the numbers of seats already reserved, for example, <code>reservedSeats[i] = [3,8]</code>&nbsp;means the seat located in row <strong>3</strong> and labelled with <b>8</b>&nbsp;is already reserved.</p>

<p><em>Return the maximum number of four-person groups&nbsp;you can assign on the cinema&nbsp;seats.</em> A four-person group&nbsp;occupies four&nbsp;adjacent seats <strong>in one single row</strong>. Seats across an aisle (such as [3,3]&nbsp;and [3,4]) are not considered to be adjacent, but there is an exceptional case&nbsp;on which an aisle split&nbsp;a four-person group, in that case, the aisle split&nbsp;a four-person group in the middle,&nbsp;which means to have two people on each side.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/02/14/cinema_seats_3.png" style="width: 400px; height: 96px;" /></p>

<pre>
<strong>Input:</strong> n = 3, reservedSeats = [[1,2],[1,3],[1,8],[2,6],[3,1],[3,10]]
<strong>Output:</strong> 4
<strong>Explanation:</strong> The figure above shows the optimal allocation for four groups, where seats mark with blue are already reserved and contiguous seats mark with orange are for one group.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 2, reservedSeats = [[2,1],[1,8],[2,6]]
<strong>Output:</strong> 2
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 4, reservedSeats = [[4,3],[1,4],[4,6],[1,7]]
<strong>Output:</strong> 4
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^9</code></li>
	<li><code>1 &lt;=&nbsp;reservedSeats.length &lt;= min(10*n, 10^4)</code></li>
	<li><code>reservedSeats[i].length == 2</code></li>
	<li><code>1&nbsp;&lt;=&nbsp;reservedSeats[i][0] &lt;= n</code></li>
	<li><code>1 &lt;=&nbsp;reservedSeats[i][1] &lt;= 10</code></li>
	<li>All <code>reservedSeats[i]</code> are distinct.</li>
</ul>

</div>

## Tags
- Array (array)
- Greedy (greedy)

## Companies
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java] Straightforward solution (bitwise)
- Author: OPyrohov
- Creation Date: Sun Mar 22 2020 00:01:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 22 2020 01:06:16 GMT+0800 (Singapore Standard Time)

<p>
From the beginning I used  a `Map<Integer, Set<Integer>>` to represent reserved seats as a graph.
But then I realized that we can use a bit vector instead of `Set<Integer>`.

Also, seats `2,3,4,5` can be represented as `(1 << 2) | (1 << 3) | (1 << 4) | (1 << 5) = 60`, for example.
So, I use this value to check whether the seats `2,3,4,5` are available when traversing the graph (togather with `6,7,8,9` and `4,5,6,7`).
```
class Solution {
    public int maxNumberOfFamilies(int n, int[][] reservedSeats) {
        Map<Integer, Integer> graph = new HashMap<>();
        for (int[] reserved : reservedSeats) {
            int row = reserved[0];
            int col = reserved[1];
            graph.put(row, graph.getOrDefault(row, 0) | (1 << col)); // create a bit vector of reserved seats
        }
        int max = 0;
        for (int row : graph.keySet()) {
            int reserved = graph.get(row);
            int cnt = 0;
            if ((reserved &  60) == 0) cnt += 1; // check if seats 2,3,4,5 are available
            if ((reserved & 960) == 0) cnt += 1; // check if seats 6,7,8,9 are available
            if ((reserved & 240) == 0 && cnt == 0) cnt = 1; // check if seats 4,5,6,7 are available
            max += cnt;
        }
        return max + 2 * (n - graph.size());
    }
}
</p>


### [Python] Group reserved seats by row using hash map
- Author: insane_reckless
- Creation Date: Sun Mar 22 2020 00:09:25 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 22 2020 08:49:07 GMT+0800 (Singapore Standard Time)

<p>
**Intuition**
If there are not any reserved seats, the answer should be 2*n*. Then we investigate the reserved seats and group them on a row basis. In this way, we subtract the seats that are not suitable for a four-person family.

**Solution1**
Time Complexity: O(M)
Space Complexity: O(M)
(M is the size of the reserved seats)
```
class Solution(object):
    def maxNumberOfFamilies(self, n, reservedSeats):
        """
        :type n: int
        :type reservedSeats: List[List[int]]
        :rtype: int
        """
        res = 2*n
        ht = {}
        for r in reservedSeats:
            if r[0] not in ht:
                ht[r[0]] = {r[1]}
            else:
                ht[r[0]].add(r[1])
        for x in ht:
            cnt = 0
            if 2 not in ht[x] and 3 not in ht[x] and 4 not in ht[x] and 5 not in ht[x]:
                cnt +=1
            if 6 not in ht[x] and 7 not in ht[x] and 8 not in ht[x] and 9 not in ht[x]:
                cnt += 1
            if 4 not in ht[x] and 5 not in ht[x] and 6 not in ht[x] and 7 not in ht[x] and cnt == 0:
                cnt += 1
            res += (cnt-2)
        return res
```

**Solution2**
We sort the reserved seats first and then do the same as the Solution 1. We sacrifice little time complexity and save a lot space. 

Time Complexity: O(MlogM)
Space Complexity: O(1)
(M is the size of the reserved seats)
```
class Solution(object):
    def maxNumberOfFamilies(self, n, reservedSeats):
        """
        :type n: int
        :type reservedSeats: List[List[int]]
        :rtype: int
        """
        res = 2*n
        reservedSeats.sort(key = lambda x:x[0])
        i = 0
        ht = set()
        while i < len(reservedSeats):
            curRow = reservedSeats[i][0]
            ht.clear()
            j = i
            while j < len(reservedSeats) and reservedSeats[j][0] == curRow:
                ht.add(reservedSeats[j][1])
                j+=1
            i = j
            cnt = 0
            if 2 not in ht and 3 not in ht and 4 not in ht and 5 not in ht:
                cnt +=1
            if 6 not in ht and 7 not in ht and 8 not in ht and 9 not in ht:
                cnt += 1
            if 4 not in ht and 5 not in ht and 6 not in ht and 7 not in ht and cnt == 0:
                cnt += 1
            res += (cnt-2)
        return res
```
</p>


### [Python] Very concise solution using hash map
- Author: lichuan199010
- Creation Date: Fri Mar 27 2020 02:09:11 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 27 2020 02:09:11 GMT+0800 (Singapore Standard Time)

<p>
We use three numbers to record whether the left, the middle or the right is occupied or not.
First, we record whether the left, middle or right is occupied or not using a set as the value in the dictionary.
For n rows, the maximum number of families that can sit together are 2*n.
Then we iterate through the dictionary, if all three positions in the row was blocked, the total cnt should -2.
If less than 3 positions was blocked, the total cnt should -1.

    class Solution:
		def maxNumberOfFamilies(self, n: int, reservedSeats: List[List[int]]) -> int:
			seats = collections.defaultdict(set)

			for i,j in reservedSeats:
				if j in [2,3,4,5]:
					seats[i].add(0)
				if j in [4,5,6,7]:
					seats[i].add(1)
				if j in [6,7,8,9]:
					seats[i].add(2)
			res = 2*n
			for i in seats:
				if len(seats[i]) == 3:
					res -= 2
				else:
					res -= 1

			return res
</p>


