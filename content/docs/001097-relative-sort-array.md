---
title: "Relative Sort Array"
weight: 1097
#id: "relative-sort-array"
---
## Description
<div class="description">
<p>Given two arrays <code>arr1</code> and <code>arr2</code>, the elements of <code>arr2</code> are distinct, and all elements in <code>arr2</code> are also in <code>arr1</code>.</p>

<p>Sort the elements of <code>arr1</code> such that the relative ordering of items in <code>arr1</code> are the same as in <code>arr2</code>.&nbsp; Elements that don&#39;t appear in <code>arr2</code> should be placed at the end of <code>arr1</code> in <strong>ascending</strong> order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> arr1 = [2,3,1,3,2,4,6,7,9,2,19], arr2 = [2,1,4,3,9,6]
<strong>Output:</strong> [2,2,2,1,4,3,3,9,6,7,19]
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>arr1.length, arr2.length &lt;= 1000</code></li>
	<li><code>0 &lt;= arr1[i], arr2[i] &lt;= 1000</code></li>
	<li>Each&nbsp;<code>arr2[i]</code>&nbsp;is&nbsp;distinct.</li>
	<li>Each&nbsp;<code>arr2[i]</code> is in <code>arr1</code>.</li>
</ul>

</div>

## Tags
- Array (array)
- Sort (sort)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- eBay - 2 (taggedByAdmin: false)
- DE Shaw - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java in-place solution using counting sort
- Author: motorix
- Creation Date: Sun Jul 14 2019 23:08:12 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 07 2020 05:16:16 GMT+0800 (Singapore Standard Time)

<p>
1. Because ```0 <= arr1[i], arr2[i] <= 1000```, we use an array to count every element.
2. Go through every element in the second array, and update values of the first array based on the order in the second array.
```
class Solution {
    public int[] relativeSortArray(int[] arr1, int[] arr2) {
        int[] cnt = new int[1001];
        for(int n : arr1) cnt[n]++;
        int i = 0;
        for(int n : arr2) {
            while(cnt[n]-- > 0) {
                arr1[i++] = n;
            }
        }
        for(int n = 0; n < cnt.length; n++) {
            while(cnt[n]-- > 0) {
                arr1[i++] = n;
            }
        }
        return arr1;
    }
}
```
Time: O(1)
Space: O(1)

--
**Follow-up: What if this constraint ```0 <= arr1[i], arr2[i] <= 1000``` doesn\'t exist?**
Use TreeMap.
```
class Solution {
    public int[] relativeSortArray(int[] arr1, int[] arr2) {
        TreeMap<Integer, Integer> map = new TreeMap<>();
        for(int n : arr1) map.put(n, map.getOrDefault(n, 0) + 1);
        int i = 0;
        for(int n : arr2) {
            for(int j = 0; j < map.get(n); j++) {
                arr1[i++] = n;
            }
            map.remove(n);
        }
        for(int n : map.keySet()){
            for(int j = 0; j < map.get(n); j++) {
                arr1[i++] = n;
            }
        }
        return arr1;
    }
}
```
Time: O(NlogN)
Space: O(N)
</p>


### [Python] Straight Forward, 1 line and 2 lines
- Author: lee215
- Creation Date: Sun Jul 14 2019 12:03:27 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 23 2020 23:05:06 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
Save the order of `B` to a hashmap `k`,
because we\'ll use it again and again.

Sort `A` and take `k[a]` as the key.
If `a` not in `k`, take `1000 + a` as key.
<br>

## **Complexity**
Time `O(AlogA + B)`
`O(B)` extra space,
`O(A)` spacefor output
<br>

```py
def relativeSortArray(self, A, B):
        k = {b: i for i, b in enumerate(B)}
        return sorted(A, key=lambda a: k.get(a, 1000 + a))
```
<br>

## 1-line for fun
Time `O(A(A+B)logA)`
Space `O(A+B)`
```py
    def relativeSortArray(self, A, B):
        return sorted(A, key=(B + sorted(A)).index)
```

</p>


### [Java/Python 3] O(max(n2, N)) code, similar to 791 Custom Sort String.
- Author: rock
- Creation Date: Sun Jul 14 2019 12:02:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 06 2020 00:06:38 GMT+0800 (Singapore Standard Time)

<p>
Similar to [my solution](https://leetcode.com/problems/custom-sort-string/discuss/116615/JavaPython-3-5-ms-10-line-and-6-line-counting-solutions-w-comment) of [791. Custom Sort String](https://leetcode.com/problems/custom-sort-string/description/)


```
    public int[] relativeSortArray(int[] arr1, int[] arr2) {
        int k = 0;
        int[] cnt = new int[1001], ans = new int[arr1.length];
        for (int i : arr1)                      // Count each number in arr1.
            ++cnt[i];
        for (int i : arr2)                      // Sort the common numbers in both arrays by the order of arr2.
            while (cnt[i]-- > 0)
                ans[k++] = i;
        for (int i = 0; i < 1001; ++i)          // Sort the numbers only in arr1.
            while (cnt[i]-- > 0)
                ans[k++] = i;
        return ans;
    }
```
**Analysis:**
Time: O(max(n2, N)), space: O(N), where n2 = arr2.length, N = max(arr2).

----

**Python 3:**
```
    def relativeSortArray(self, arr1: List[int], arr2: List[int]) -> List[int]:
        ans, cnt = [], collections.Counter(arr1)         # Count each number in arr1
        for i in arr2:
            if cnt[i]: ans.extend([i] * cnt.pop(i))      # Sort the common numbers in both arrays by the order of arr2. 
        for i in range(1001):               
            if cnt[i]: ans.extend([i] * cnt.pop(i))      # Sort the numbers only in arr1.
        return ans
```

**Analysis:**

Time: O(max(n2, N)), space: O(n1).

The for loop in above code:
```
        for i in range(1001):               
            if cnt[i]: ans.extend([i] * cnt.pop(i))      # Sort the numbers only in arr1.
```
can also be rewritten as:
```
        ans.extend(sorted(cnt.elements()))               # Sort the numbers only in arr1.
```
However, the time will change from O(max(n2, N)) to O(n1logn1).
</p>


