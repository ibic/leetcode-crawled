---
title: "Dota2 Senate"
weight: 581
#id: "dota2-senate"
---
## Description
<div class="description">
<p>In the world of Dota2, there are two parties: the <code>Radiant</code> and the <code>Dire</code>.</p>

<p>The Dota2 senate consists of senators coming from two parties. Now the senate wants to make a decision about a change in the Dota2 game. The voting for this change is a round-based procedure. In each round, each senator can exercise <code>one</code> of the two rights:</p>

<ol>
	<li><code>Ban one senator&#39;s right</code>:<br />
	A senator can make another senator lose <b>all his rights</b> in this and all the following rounds.</li>
	<li><code>Announce the victory</code>:<br />
	If this senator found the senators who still have rights to vote are all from <b>the same party</b>, he can announce the victory and make the decision about the change in the game.</li>
</ol>

<p>&nbsp;</p>

<p>Given a string representing each senator&#39;s party belonging. The character &#39;R&#39; and &#39;D&#39; represent the <code>Radiant</code> party and the <code>Dire</code> party respectively. Then if there are <code>n</code> senators, the size of the given string will be <code>n</code>.</p>

<p>The round-based procedure starts from the first senator to the last senator in the given order. This procedure will last until the end of voting. All the senators who have lost their rights will be skipped during the procedure.</p>

<p>Suppose every senator is smart enough and will play the best strategy for his own party, you need to predict which party will finally announce the victory and make the change in the Dota2 game. The output should be <code>Radiant</code> or <code>Dire</code>.</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> &quot;RD&quot;
<b>Output:</b> &quot;Radiant&quot;
<b>Explanation:</b> The first senator comes from Radiant and he can just ban the next senator&#39;s right in the round 1. 
And the second senator can&#39;t exercise any rights any more since his right has been banned. 
And in the round 2, the first senator can just announce the victory since he is the only guy in the senate who can vote.
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> &quot;RDD&quot;
<b>Output:</b> &quot;Dire&quot;
<b>Explanation:</b> 
The first senator comes from Radiant and he can just ban the next senator&#39;s right in the round 1. 
And the second senator can&#39;t exercise any rights anymore since his right has been banned. 
And the third senator comes from Dire and he can ban the first senator&#39;s right in the round 1. 
And in the round 2, the third senator can just announce the victory since he is the only guy in the senate who can vote.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li>The length of the given string will in the range [1, 10,000].</li>
</ol>

<p>&nbsp;</p>

</div>

## Tags
- Greedy (greedy)

## Companies
- Valve - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Simulation [Accepted]

**Intuition**

A senator performing a ban doesn't need to use it on another senator immediately.  We can wait to see when another team's senator will vote, then use that ban retroactively.

**Algorithm**

Put the senators in an integer queue: `1` for `'Radiant'` and `0` for `'Dire'`.

Now process the queue: if there is a floating ban for that senator, exercise it and continue.  Otherwise, add a floating ban against the other team, and enqueue this senator again.

<iframe src="https://leetcode.com/playground/ZzmPDP3h/shared" frameBorder="0" width="100%" height="497" name="ZzmPDP3h"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ where $$N$$ is the size of the senate.  Every vote removes one senator from the other team.

* Space Complexity: $$O(N)$$, the space used by our queue.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] Very simple greedy solution with explanation
- Author: caihao0727mail
- Creation Date: Sun Jul 30 2017 17:01:07 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 13:20:57 GMT+0800 (Singapore Standard Time)

<p>
This is obliviously a greedy algorithm problem. Each senate ```R``` must ban its next **closest** senate ```D``` who is from another party, or else ```D``` will ban its next  senate from  ```R's``` party. 

The idea is to use two queues to save the index of each senate from ```R's``` and ```D's``` parties, respectively. During each round, we delete the banned senate's index; and plus the remainning senate's index with ```n```(the length of the input string ```senate```), then move it to the back of its respective queue.

Java version:
```
    public String predictPartyVictory(String senate) {
        Queue<Integer> q1 = new LinkedList<Integer>(), q2 = new LinkedList<Integer>();
        int n = senate.length();
        for(int i = 0; i<n; i++){
            if(senate.charAt(i) == 'R')q1.add(i);
            else q2.add(i);
        }
        while(!q1.isEmpty() && !q2.isEmpty()){
            int r_index = q1.poll(), d_index = q2.poll();
            if(r_index < d_index)q1.add(r_index + n);
            else q2.add(d_index + n);
        }
        return (q1.size() > q2.size())? "Radiant" : "Dire";
    }
```

C++ version:
```
string predictPartyVictory(string senate) {
        queue<int> q1, q2;
        int n = senate.length();
        for(int i = 0; i<n; i++)
            (senate[i] == 'R')?q1.push(i):q2.push(i);
        while(!q1.empty() && !q2.empty()){
            int r_index = q1.front(), d_index = q2.front();
            q1.pop(), q2.pop();
            (r_index < d_index)?q1.push(r_index + n):q2.push(d_index + n);
        }
        return (q1.size() > q2.size())? "Radiant" : "Dire";
    }
```
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun Jul 30 2017 12:09:01 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 30 2017 12:09:01 GMT+0800 (Singapore Standard Time)

<p>
Simulate the process.  We don't have to resolve bans until later - we can just let them "float".

We have ```people = [int, int]``` representing how many people are in the queue, and ```bans = [int, int]``` representing how many "floating" bans there are.  When we meet a person, if there is a floating ban waiting for them, then they are banned and we skip them.  Eventually, the queue will just have one type of person, which is when we break.

```
def predictPartyVictory(self, senate):
    A = collections.deque()
    people = [0, 0]
    bans = [0, 0]

    for person in senate:
        x = person == 'R'
        people[x] += 1
        A.append(x)

    while all(people):
        x = A.popleft()
        people[x] -= 1
        if bans[x]:
            bans[x] -= 1
        else:
            bans[x^1] += 1
            A.append(x)
            people[x] += 1

    return "Radiant" if people[1] else "Dire"
```
</p>


### C++, O(n) solution
- Author: zestypanda
- Creation Date: Mon Jul 31 2017 01:00:01 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 31 2017 01:00:01 GMT+0800 (Singapore Standard Time)

<p>
It is not hard to see the greedy way to win is to ban next earliest opponent in a circle.
I use a variable "count" for how many R or D, and a string s for the state after current iteration. count > 0 means more active R, while count < 0 means more active D. When encountering 'D', if count > 0, this 'D' is banned, and count--. This 'D' is not appended to string s. If count <= 0, it will be appended to s.
Because every active senate will ban another senate in a circle, the length of senate will shrink in half. Otherwise the game is over in current or next iteration. So the run time is n+n/2+n/4+..., which is O(n).
```
class Solution {
public:
    string predictPartyVictory(string senate) {
        int count = 0, len = 0;
        // When the length of senate doesn't decrease, the game is over.
        while (senate.size() != len) {
            string s;
            len = senate.size();
            for (int i = 0; i < len; i++) {
                if (senate[i] == 'R') {
                    if (count++ >= 0) s += 'R';
                }
                else if (senate[i] == 'D') {
                    if (count-- <= 0) s += 'D';
                }
            }  
            swap(s, senate);
        }
        if (senate[0] == 'R') 
            return "Radiant";
        else 
            return "Dire";
    }
};
```
</p>


