---
title: "Smallest Integer Divisible by K"
weight: 973
#id: "smallest-integer-divisible-by-k"
---
## Description
<div class="description">
<p>Given a positive integer <code>K</code>, you need find the <strong>smallest</strong>&nbsp;positive integer <code>N</code> such that <code>N</code> is divisible by <code>K</code>, and <code>N</code> only contains the digit <strong>1</strong>.</p>

<p>Return the&nbsp;length of <code>N</code>.&nbsp; If there is no such <code>N</code>,&nbsp;return -1.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> 1
<strong>Output:</strong> 1
<strong>Explanation:</strong> The smallest answer is N = 1, which has length 1.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> 2
<strong>Output:</strong> -1
<strong>Explanation:</strong> There is no such positive integer N divisible by 2.</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> 3
<strong>Output:</strong> 3
<strong>Explanation:</strong> The smallest answer is N = 111, which has length 3.</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>1 &lt;= K &lt;= 10^5</code></li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

#### Overview

It's an interesting problem that requires a little observation and insight. It's recommended to try a few numbers to find out some regular patterns. Below, we will discuss a simple approach to solve this problem.

---

#### Approach 1: Checking Loop

**Intuition**

We need to do two things:

1. check if the required number `N` exists.
2. find out `length(N)`.

The second one is easy: we only need to keep multiplying `N` by 10 and adding 1 until `N%K==0`. However, since `N` might overflow, we need to use the remainder. The pseudo-code is as below:

<pre>
remainder = 1
length_N = 1

while remainder%K != 0
    N = remainder*10 + 1
    remainder = N%K
    length_N += 1

return length_N
</pre>

Since the `remainder` and `N` have the same remainder of `K`, it OK to use `remainder` instead of `N`.

Now, the only problem is how to check whether the required number `N` exists.

Notice that if `N` does not exist, this while-loop will continue endlessly. However, the possible values of `remainder` are limited -- ranging from `0` to `K-1`. Therefore, if the while-loop continues forever, the `remainder` repeats. Also, if `remainder` repeats, then it gets into a loop. Hence, the while-loop is endless if and only if the `remainder` repeats.

In this case, we can check if the `remainder` repeats to check if the while-loop is endless:

<pre>
remainder = 1
length_N = 1

seen_remainders = set()

while remainder%K != 0
    N = remainder*10 + 1
    remainder = N%K
    length_N += 1

    if remainder in seen_remainders
        return -1
    else
        seen_remainders.add(remainder)

return length_N
</pre>

Now we have an algorithm that can solve the problem. 

Furthermore, we can improve this algorithm with [Pigeonhole Principle](https://en.wikipedia.org/wiki/Pigeonhole_principle). Recall that the number of possible values of `remainder` (ranging from `0` to `K-1`) is limited, and in fact, the number is `K`. As a result, if the while-loop continues more than `K` times, and haven't stopped, then we can conclude that `remainder` repeats -- you can not have more than `K` different `remainder`.

Hence, if `N` exists, the while-loop must return `length_N` in the first `K` loops. Otherwise, it goes into an infinite loop.

Therefore, we can just run the while-loop `K` times, and return -1 if not stopped.


**Algorithm**

We just run the while-loop `K` times, check if the remainder is 0, and return -1 if not stopped.

> Note: After reading the Algorithm part, it is recommended to try writing the code on your own before reading the solution code.

<iframe src="https://leetcode.com/playground/XdQW4H3h/shared" frameBorder="0" width="100%" height="259" name="XdQW4H3h"></iframe>

There are a few interesting points worth pointing out in the code above:

1. We initialize `remainder` to 0, not 1, to keep code consistency because in the first loop the `remainder` changes to 1. You can also initialize it as 1, but it requires a little change in code.
2. We only run the loop `K` times at most, not `K+1`. This is because if it does not stop in the previous `K` loop, it will continue the `K+1`-th iteration, which must have repeated `remainder`. Therefore, it is not necessary to check the `K+1`-th iteration.

Also, note that `111...111` can never be divided by 2 or 5 because its last digit is never an even number or 5. You can just return -1 if you find that 2 or 5 is a factor of `K`.

**Complexity Analysis**

- Time Complexity : $$\mathcal{O}(K)$$ since we at most run the loop $$\mathcal{O}(K)$$ times.

- Space Complexity : $$\mathcal{O}(1)$$ since we only use three ints: `K`, `remainder`, and `length_N`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python O(K) with Detailed Explanations
- Author: infinute
- Creation Date: Sun Mar 24 2019 12:03:09 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 24 2019 12:03:09 GMT+0800 (Singapore Standard Time)

<p>
For a given K, we evaluate `1 % K, 11 % K, 111 % K, ..., 11...1 (K \'1\'s) % K`. 
* If any remainder is 0, then the current number is the smallest integer divisible by `K`. 
* If none of the remainders is 0, then at some point, there must be some duplicate remainders (due to [Pigeonhole principle](https://en.wikipedia.org/wiki/Pigeonhole_principle)), as the `K` remainders can only take at most `K-1` different values excluding 0. In this case, no number with the pattern 1...1 is divisible by `K`. This is because once a remainder has a duplicate, the next remainder will be in a loop, as the previous remainder determines the next_mod, i.e., `next_mod = (10 * prev_mod + 1) % K`. Therefore, we will never see remainder==0.

A simple example is when K is 6. Once we see 1111 % 6 = 1, we immediately know 11111 % 6 will be 5, since 1 % 6 = 1 and 11 % 6 = 5. Therefore, there will be no such number that is divisible by 6.
* 1 % 6 = 1
* 11 % 6 = 5
* 111 % 6 = 3
* 1111 % 6 = 1
* 11111 % 6 = 5
* 111111 % 6 = 3

Also, it is easy to see that for any number whose last digit is not in `{1, 3, 7, 9}`, we should return `-1`.
```
class Solution:
    def smallestRepunitDivByK(self, K: int) -> int:
        if K % 10 not in {1, 3, 7, 9}: return -1
        mod, mod_set = 0, set()
        for length in range(1, K + 1):
            mod = (10 * mod + 1) % K
            if mod == 0: return length
            if mod in mod_set: return -1
            mod_set.add(mod)
        return -1
```
</p>


### [Java/C++/Python] O(1) Space with Proves of Pigeon Holes
- Author: lee215
- Creation Date: Sun Mar 24 2019 11:06:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 24 2019 11:06:42 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**:
Let\'s say the final result has `N` digits of 1. 
If `N` exist, `N <= K`, just do a brute force check.
Also if `K % 2 == 0`, return -1, because 111....11 can\'t be even.
Also if `K % 5 == 0`, return -1, because 111....11 can\'t end with 0 or 5.
<br>


## **Explanation**
For different `N`, we calculate the remainder of mod `K`.
It has to use the remainder for these two reason:

1. Integer overflow
2. The division operation for big integers, is **NOT** `O(1)`, it\'s actually depends on the number of digits..


## **Prove**

Why 5 is a corner case? It has a reason and we can find it.
Assume that `N = 1` to `N = K`, if there isn\'t `111...11 % K == 0`
There are at most `K - 1` different remainders: `1, 2, .... K - 1`.

So this is a pigeon holes problem:
There must be at least 2 same remainders.


Assume that,
`f(N) \u2261 f(M)`, `N > M`
`f(N - M) * 10 ^ M \u2261 0`
`10 ^ M \u2261 0, mod K`
so that K has factor 2 or factor 5.


Proof by contradiction\uFF0C
`If (K % 2 == 0 || K % 5 == 0) return -1;`
otherwise, there must be a solution `N <= K`.

## **Time Complexity**:
Time `O(K)`, Space `O(1)`

<br>

**Java:**
```
    public int smallestRepunitDivByK(int K) {
        if (K % 2 == 0 || K % 5 == 0) return -1;
        int r = 0;
        for (int N = 1; N <= K; ++N) {
            r = (r * 10 + 1) % K;
            if (r == 0) return N;
        }
        return -1;
    }
```

**C++:**
```
    int smallestRepunitDivByK(int K) {
        for (int r = 0, N = 1; N <= K; ++N)
            if ((r = (r * 10 + 1) % K) == 0)
                return N;
        return -1;
    }
```

**Python:**
```
    def smallestRepunitDivByK(self, K):
        if K % 2 == 0 or K % 5 == 0: return -1
        r = 0
        for N in xrange(1, K + 1):
            r = (r * 10 + 1) % K
            if not r: return N
```

</p>


### Proof: we only need to consider the first K candidates in [1, 11, 111, 1111, ...]
- Author: StarsThu2016
- Creation Date: Sun Mar 24 2019 12:29:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 24 2019 12:29:07 GMT+0800 (Singapore Standard Time)

<p>
This problem is the second problem from Leetcode Contest 129. Among all the top-25 ranked participants, only 10 of them bounded their loop in K iterations. Thus, we have the strong motivation to mathematically prove the title of this post and bound our execution in K iterations. Otherwise, most people basically set an upper bound as 10^5 according to the note or just write a "while True" loop.

If this is true, the code in Python can be very elegant like this.
```
def smallestRepunitDivByK(self, K: int) -> int:    
    if K % 2 ==0 or K % 5==0:
        return -1       
    module = 1
    for Ndigits in range(1, K+1):
        if module % K == 0:
            return Ndigits
        module = (module * 10 + 1) % K
    return 1234567890 # This line never executes
```
Proof:
It is obvious that if K is the multiple of 2 or multiple of 5, N is definitely not multiple of K because the ones digit of N is 1. Thus, return -1 for this case.

If K is neither multiple of 2 nor multiple of 5, we have this theorem. 

Theorem: there must be one number from the K-long candidates list [1, 11, 111, ..., 111..111(with K ones)], which is the multiple of K. 

Why? Let\'s think in the opposite way. Is it possible that none of the K candidates is the multiple of K?

If true, then the module of these K candidate numbers (mod K) must be in [1, .., K-1] (K-1 possible module values). Thus, there must be 2 candidate numbers with the same module. Let\'s denote these two numbers as N_i with i ones, and N_j with j ones and i<j.

Thus N_j-N_i = 1111...1000...0 with (j-i) ones and i zeros. N_j-N_i = 111..11 (j-i ones) * 100000..000 (i zeros). We know that N_i and N_j have the same module of K. Then N_j-N_i is the multiple of K. However, 100000..000 (i zeros) does not contain any factors of K (K is neither multiple of 2 nor multiple of 5). Thus, 111..11 (j-i ones) is the multiple of K. This is contradictory to our assumption that all K candidates including 111..11 (j-i ones) are not multiple of K.

Finally, we know that there is at least one number, from the first K candidates, which is the multiple of K.
</p>


