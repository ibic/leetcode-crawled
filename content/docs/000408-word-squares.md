---
title: "Word Squares"
weight: 408
#id: "word-squares"
---
## Description
<div class="description">
<p>Given a set of words <b>(without duplicates)</b>, find all <a href="https://en.wikipedia.org/wiki/Word_square" target="_blank">word squares</a> you can build from them.</p>

<p>A sequence of words forms a valid word square if the <i>k</i><sup>th</sup> row and column read the exact same string, where 0 &le; <i>k</i> &lt; max(numRows, numColumns).</p>

<p>For example, the word sequence <code>["ball","area","lead","lady"]</code> forms a word square because each word reads the same both horizontally and vertically.</p>

<pre>
b a l l
a r e a
l e a d
l a d y
</pre>

<p><b>Note:</b><br />
<ol>
<li>There are at least 1 and at most 1000 words.</li>
<li>All words will have the exact same length.</li>
<li>Word length is at least 1 and at most 5.</li>
<li>Each word contains only lowercase English alphabet <code>a-z</code>.</li>
</ol>
</p>

<p><b>Example 1:</b>
<pre>
<b>Input:</b>
["area","lead","wall","lady","ball"]

<b>Output:</b>
[
  [ "wall",
    "area",
    "lead",
    "lady"
  ],
  [ "ball",
    "area",
    "lead",
    "lady"
  ]
]

<b>Explanation:</b>
The output consists of two word squares. The order of output does not matter (just the order of words in each word square matters).
</pre>
</p>

<p><b>Example 2:</b>
<pre>
<b>Input:</b>
["abat","baba","atan","atal"]

<b>Output:</b>
[
  [ "baba",
    "abat",
    "baba",
    "atan"
  ],
  [ "baba",
    "abat",
    "baba",
    "atal"
  ]
]

<b>Explanation:</b>
The output consists of two word squares. The order of output does not matter (just the order of words in each word square matters).
</pre>
</p>
</div>

## Tags
- Backtracking (backtracking)
- Trie (trie)

## Companies
- Bloomberg - 2 (taggedByAdmin: false)
- Oracle - 7 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

Before diving into the solutions, it could be helpful to take a step back and clarify the requirements of the problem first.

Given a list of non-duplicate words, we are asked to construct all possible combinations of word squares. And here is the definition of _**word square**_.

>A sequence of words forms a valid _word square_, if and only if each string ($$H_k$$) that is formed horizontally from the kth row equals to the string ($$V_k$$) that is formed vertically from the kth column, _i.e._
$$
H_k == V_k \qquad \forall{k} \quad 0 ≤ k < \max(\text{numRows}, \text{numColumns}).
$$

![pic](../Figures/425/425_valid_word_square.png)

As we can see from the definition, for a word square with equal-sized row and column, the resulting letter matrix should be **symmetrical** across its diagonal. 

In other words, if we know the upper-right part of the word square, we could infer its lower-left part, and vice versa. This symmetric property of the word square could also be interpreted as the **constraint** of the problem (as in the _constraint programming_), which could help us to narrow down the valid combinations. 
<br/>
<br/>

---

#### Algorithm: Backtracking 

Given a list of words, we are asked to find a combination of words upon with we could construct a _word square_. The backbone of the algorithm to solve the above problem could be surprisingly simple.

>The idea is that we construct the word square **_row by row_** from top to down. At each row, we simply do _trial and error_, _i.e._ we try with one word, if it does not meet the constraint then we try another one.

As one might notice, the above idea of the algorithm is actually known as [backtracking](https://leetcode.com/explore/learn/card/recursion-ii/472/backtracking/2654/), which is often associated with _recursion_ and _DFS_ (Depth-First Search) as well.

Let us illustrate the idea with an example. Given a list of words `[ball, able, area, lead, lady]`, we should pile up 4 words together in order to build a word square.

![pic](../Figures/425/425_backtrack.png)

- Let us start with the word `ball` as the first word in the word square, _i.e._ the word that we would put in the first row.

- We then move on to the second row. Given the _symmetric_ property of the word square, we now know the letters that we should fill on the first _column_ of the second row. In other words, we know that the word in the second row should start with the _prefix_ `a`.

- Among the list of words, there are two words with prefix `a` (_i.e._ `able`, `area`). Both of them could be the candidates to fill the second row of the square. We then should try both of them in the next step. 

- In the next step (1), let us fill the second row with the word `able`. Then we could move on to the third row. Again, due to the symmetric property, we know that the word in the third row should start with the prefix `ll`. Unfortunately, we do not find any word start with `ll`. As a result, we could no longer move forwards. We then abandon this path, and **_backtrack_** to the previous state (with the first row filled).

- As an alternative next step (1), we could try with the word `area` in the second row. Once we fill the second row, we would know that in the next row, the word to be filled should start with the prefix `le`. And this time, we find the candidate (_i.e._ `lead`).

- As a result, in the next step (2), we fill the third row with the word `lead`. So on and so forth.

- _At the end, if one repeats the above steps with each word as the starting word, one would exhaust all the possibilities to construct a valid word square_.

One could follow the [code template of backtracking](https://leetcode.com/explore/learn/card/recursion-ii/472/backtracking/2793/) from our Explore card to implement the above algorithm. Here is one example.

<iframe src="https://leetcode.com/playground/qFhYnZMc/shared" frameBorder="0" width="100%" height="500" name="qFhYnZMc"></iframe>

At the first glance of the code, the problem does not seem to be as daunting as it is labeled. Actually if one could come up the skeleton of code in the interview, it would be fair to say that one has _scored_ the interview.

The above implementation is correct and would pass most of the test cases in the online judge. However, it would run into `Time Limit Exceeded` exception for certain test cases with large input.
But, there is no need for dismay, since we've already figured out the _hard_ part of the algorithm. We just need to iron out the last bit of _optimization_ which actually could be a followup question during the interview.
<br/>
<br/>

---

#### Approach 1: Backtracking with HashTable

**Intuition**

As one might notice in the above backtracking algorithm, the bottleneck lies in the function `getWordsWithPrefix()` which is to find all words with the given prefix. At each invocation of the function, we were iterating through the entire input list of words, which is of linear time complexity $$\mathcal{O}(N)$$.

>One of the ideas to optimize the `getWordsWithPrefix()` function would be to process the words beforehand and to build a data structure that could speed up the lookup procedure later.

As one might recall, one of the data structures that provide a fast lookup operation is called _**hashtable**_ or dictionary. We could simply build a hashtable with all possible prefixes as _keys_ and the words that are associated with the prefix as the _values_ in the table. Later, given the prefix, we should be able to list all the words with the given prefix in constant time $$\mathcal{O}(1)$$.

**Algorithm**

- We build upon the backtracking algorithm that we listed above, and tweak two parts.

- In the first part, we add a new function `buildPrefixHashTable(words)` to build a hashtable out of the input words.

- Then in the second part, in the function `getWordsWithPrefix()` we simply query the hashtable to retrieve all the words that possess the given prefix.

Here are some sample implementations. The idea is inspired by the post of [gabbu](https://leetcode.com/problems/word-squares/discuss/91360/3-Python-Solutions-with-very-detailed-explanations) in the discussion forum. 

<iframe src="https://leetcode.com/playground/Pm8PWU4k/shared" frameBorder="0" width="100%" height="500" name="Pm8PWU4k"></iframe>


**Complexity Analysis**

- Time complexity: $$\mathcal{O}(N\cdot26^{L})$$, where $$N$$ is the number of input words and $$L$$ is the length of a single word.

    - It is tricky to calculate the exact number of operations in the backtracking algorithm. We know that the trace of the backtrack would form a n-ary tree. Therefore, the upper bound of the operations would be the total number of nodes in a full-blossom n-ary tree.
    
    - In our case, at any node of the trace, at maximum it could have 26 branches (_i.e._ 26 alphabet letters). Therefore, the maximum number of nodes in a 26-ary tree would be approximately $$26^L$$. 

    - In the loop around the backtracking function, we enumerate the possibility of having each word as the starting word in the word square. As a result, in total the overall time complexity of the algorithm should be $$\mathcal{O}(N\cdot26^{L})$$.

    - As large as the time complexity might appear, in reality, the actual trace of the backtracking is much smaller than its upper bound, thanks to the _constraint_ checking (symmetric of word square) which greatly prunes the trace of the backtracking.

- Space Complexity: $$\mathcal{O}(N\cdot{L} + N\cdot\frac{L}{2}) = \mathcal{O}(N\cdot{L})$$ where $$N$$ is the number of words and $$L$$ is the length of a single word.

    - The first half of the space complexity (_i.e._ $$N\cdot{L}$$) is the values in the hashtable, where we store $$L$$ times all words in the hashtable.

    - The second half of the space complexity (_i.e._ $$N\cdot\frac{L}{2}$$) is the space took by the keys of the hashtable, which include all prefixes of all words.

    - In total, we could say that the overall space of the algorithm is proportional to the total words times the length of a single word.

<br/>
<br/>

---

#### Approach 2: Backtracking with Trie

**Intuition**

Speaking about prefix, there is another data structure called **_Trie_** (also known as **_prefix tree_**), which could find its use in this problem.

In the above approach, we have reduce the time complexity of retrieving a list of words with a given prefix from the linear $$\mathcal{O}(N)$$ to the constant time $$\mathcal{O}(1)$$. In exchange, we have to spend some extra space to store all the prefixes of each words.

>The Trie data structure provides a _compact_ and yet still _fast_ way to retrieve words with a given prefix.

In the following graph, we show an example on how we can build a Trie from a list of words.

![pic](../Figures/425/425_trie.png)

As we can see, basically Trie is a n-aray tree, where each node represents a character in a prefix. The Trie data structure is **compact** to store the prefixes, since it deduplicate the redundant prefixes, _e.g._ the prefixes of `le` and `la` would share one node. And yet it is still fast to retrieve words from the Trie. It takes $$\mathcal{O}(L)$$ to retrieve a word, where $$L$$ is the length of the word, which is much faster than the brute-force enumeration.

**Algorithm**

- We build upon the backtracking algorithm that we listed above, and tweak two parts.

- In the first part, we add a new function `buildTrie(words)` to build a Trie out of the input words.

- Then in the second part, in the function `getWordsWithPrefix(prefix)` we simply query the Trie to retrieve all the words that possess the given prefix.

Here are some sample implementations. Note that, we tweak the Trie data structure a bit, in order to further optimize the time and space complexity.

* Instead of labeling the word at the leaf node of the Trie, we label the word at each node so that we don't need to perform a further traversal once we reach the last node in the prefix. This trick could help us with the time complexity.

* Instead of storing the actual words in the Trie, we keep only the index of the word, which could greatly save the space.

<iframe src="https://leetcode.com/playground/jw35nkTM/shared" frameBorder="0" width="100%" height="500" name="jw35nkTM"></iframe>


**Complexity Analysis**

- Time complexity: $$\mathcal{O}(N\cdot26^{L}\cdot{L})$$, where $$N$$ is the number of input words and $$L$$ is the length of a single word.

    - Basically, the time complexity is same with the Approach #1 ($$\mathcal{O}(N\cdot26^{L})$$), except that instead of the constant-time access for the function `getWordsWithPrefix(prefix)`, we now need $$\mathcal{O}(L)$$.

- Space Complexity: $$\mathcal{O}(N\cdot{L} + N\cdot\frac{L}{2}) = \mathcal{O}(N\cdot{L})$$ where $$N$$ is the number of words and $$L$$ is the length of a single word.

    - The first half of the space complexity (_i.e._ $$N\cdot{L}$$) is the word indice that we store in the Trie, where we store $$L$$ times the index for each word.

    - The second half of the space complexity (_i.e._ $$N\cdot\frac{L}{2}$$) is the space took by the  prefixes of all words. In the worst case, we have no overlapping among the prefixes.

    - Overall, this approach has the same space complexity as the previous approach. Yet, in running time, it would consume less memory thanks to all the optimization that we have done.

<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Explained. My Java solution using Trie [126ms 16/16]
- Author: lzb700m
- Creation Date: Mon Oct 17 2016 07:51:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 02:53:30 GMT+0800 (Singapore Standard Time)

<p>
My first approach is brute-force, try every possible word sequences, and use the solution of Problem 422 (https://leetcode.com/problems/valid-word-square/) to check each sequence. This solution is straightforward, but too slow (TLE).

A better approach is to check the validity of the word square while we build it.
Example: ```["area","lead","wall","lady","ball"]```
We know that the sequence contains 4 words because the length of each word is 4.
Every word can be the first word of the sequence, let's take ```"wall"``` for example.
Which word could be the second word? Must be a word start with ```"a"``` (therefore ```"area"```), because it has to match the second letter of word ```"wall"```.
Which word could be the third word? Must be a word start with ```"le"``` (therefore ```"lead"```), because it has to match the third letter of word  ```"wall"``` and the third letter of word ```"area"```.
What about the last word? Must be a word start with ```"lad"``` (therefore ```"lady"```). For the same reason above.

The picture below shows how the prefix are matched while building the sequence.

![0_1476809138708_wordsquare.png](/uploads/files/1476809120456-wordsquare.png) 

In order for this to work, we need to fast retrieve all the words with a given **prefix**. There could be 2 ways doing this:
1. Using a hashtable, key is **prefix**, value is a list of words with that prefix.
2. Trie, we store a list of words with the **prefix** on each trie node.

The implemented below uses Trie.

```
public class Solution {
    class TrieNode {
        List<String> startWith;
        TrieNode[] children;

        TrieNode() {
            startWith = new ArrayList<>();
            children = new TrieNode[26];
        }
    }

    class Trie {
        TrieNode root;

        Trie(String[] words) {
            root = new TrieNode();
            for (String w : words) {
                TrieNode cur = root;
                for (char ch : w.toCharArray()) {
                    int idx = ch - 'a';
                    if (cur.children[idx] == null)
                        cur.children[idx] = new TrieNode();
                    cur.children[idx].startWith.add(w);
                    cur = cur.children[idx];
                }
            }
        }

        List<String> findByPrefix(String prefix) {
            List<String> ans = new ArrayList<>();
            TrieNode cur = root;
            for (char ch : prefix.toCharArray()) {
                int idx = ch - 'a';
                if (cur.children[idx] == null)
                    return ans;

                cur = cur.children[idx];
            }
            ans.addAll(cur.startWith);
            return ans;
        }
    }

    public List<List<String>> wordSquares(String[] words) {
        List<List<String>> ans = new ArrayList<>();
        if (words == null || words.length == 0)
            return ans;
        int len = words[0].length();
        Trie trie = new Trie(words);
        List<String> ansBuilder = new ArrayList<>();
        for (String w : words) {
            ansBuilder.add(w);
            search(len, trie, ans, ansBuilder);
            ansBuilder.remove(ansBuilder.size() - 1);
        }

        return ans;
    }

    private void search(int len, Trie tr, List<List<String>> ans,
            List<String> ansBuilder) {
        if (ansBuilder.size() == len) {
            ans.add(new ArrayList<>(ansBuilder));
            return;
        }

        int idx = ansBuilder.size();
        StringBuilder prefixBuilder = new StringBuilder();
        for (String s : ansBuilder)
            prefixBuilder.append(s.charAt(idx));
        List<String> startWith = tr.findByPrefix(prefixBuilder.toString());
        for (String sw : startWith) {
            ansBuilder.add(sw);
            search(len, tr, ans, ansBuilder);
            ansBuilder.remove(ansBuilder.size() - 1);
        }
    }
}
```
</p>


### Short Python/C++ solution
- Author: StefanPochmann
- Creation Date: Sun Oct 16 2016 19:25:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 07 2018 06:48:47 GMT+0800 (Singapore Standard Time)

<p>
## Python Solution <sup>(accepted in ~870 ms)</sup>

    def wordSquares(self, words):
        n = len(words[0])
        fulls = collections.defaultdict(list)
        for word in words:
            for i in range(n):
                fulls[word[:i]].append(word)
        def build(square):
            if len(square) == n:
                squares.append(square)
                return
            for word in fulls[''.join(zip(*square)[len(square)])]:
                build(square + [word])
        squares = []
        for word in words:
            build([word])
        return squares

## Explanation

I try every word for the first row. For each of them, try every fitting word for the second row. And so on. The first few rows determine the first few columns and thus determine how the next row's word must start. For example:
```
wall      Try words      wall                     wall                      wall
a...   => starting  =>   area      Try words      area                      area
l...      with "a"       le..   => starting  =>   lead      Try words       lead
l...                     la..      with "le"      lad.   => starting   =>   lady
                                                            with "lad"
```
For quick lookup, my `fulls` dictionary maps prefixes to lists of words who have that prefix.

<br>

## C++ Solution <sup>(accepted in ~180 ms)</sup>
```
class Solution {
public:
    vector<vector<string>> wordSquares(vector<string>& words) {
        n = words[0].size();
        square.resize(n);
        for (string word : words)
            for (int i=0; i<n; i++)
                fulls[word.substr(0, i)].push_back(word);
        build(0);
        return squares;
    
    }
    int n;
    unordered_map<string, vector<string>> fulls;
    vector<string> square;
    vector<vector<string>> squares;
    void build(int i) {
        if (i == n) {
            squares.push_back(square);
            return;
        }
        string prefix;
        for (int k=0; k<i; k++)
            prefix += square[k][i];
        for (string word : fulls[prefix]) {
            square[i] = word;
            build(i + 1);
        }
    }
};
```
</p>


### Java DFS+Trie 54 ms, 98% so far
- Author: yubad2000
- Creation Date: Tue Oct 25 2016 14:44:36 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 25 2016 14:44:36 GMT+0800 (Singapore Standard Time)

<p>
By considering the word squares as a symmetric matrix, my idea is to go through the top right triangular matrix in left-to-right and then down order.
For example, with the case of ["area","lead","wall","lady","ball"] where length = 4,
we start with 4 empty string
""
""
""
""
Next, [0,0] , "a","b", "l", "w" can be placed, we start with "a"
"a"
""
""
""
[0,1] go right, "r" can be placed after "a", but no words start with "r" at [1,0], so this DFS ends. 
"ar" 
""
""
""
Now, start with "b" at [0,0]
"b"
""
""
""
We can have "ba" at [0,1] and there is a word start with "a"
"ba"
"a"
""
""
Next
"bal"
"a"
"l"
""
Next
"ball"
"a"
"l"
"l"
When finish the first row, go down to next row and start at [1,1]
"ball"
"ar"
"l"
"l"
..... so on and so forth until reaching [4,4] 


```
public class Solution {
    class Node{
        Node[] nodes;
        String word;
        Node(){
            this.nodes = new Node[26];
            this.word = null;
        }
    }
    void add(Node root, String word){
        Node node = root;
        for (char c : word.toCharArray() ) {
            int idx = c-'a';
            if (node.nodes[idx] == null) node.nodes[idx] = new Node();
            node = node.nodes[idx];
        }
        node.word = word;
    }
    void helper(int row, int col, int len, Node[] rows, List<List<String>> ret) {
        if ( (col == row) && (row == len) ) { // last char
            List<String> res = new ArrayList<String>();
            for (int i=0; i<len; i++) {
                res.add(new String(rows[i].word) );
            }
            ret.add( res );
        } else { // from left to right and then go down to the next row
            if ( col < len  ) { // left to right first
                Node pre_row = rows[row];
                Node pre_col = rows[col];
                for (int i=0; i<26; i++) { // find all the possible next char
                    if ( (rows[row].nodes[i] != null) && (rows[col].nodes[i] != null) ) {
                        rows[row] = rows[row].nodes[i];
                        if (col != row) rows[col] = rows[col].nodes[i];
                        helper(row, col+1, len, rows, ret);
                        rows[row] = pre_row;
                        if (col != row) rows[col] = pre_col;
                    }
                }
            } else { // reach the end of column, go to the next row
                helper(row+1, row+1, len, rows, ret);
            }
        }
    }
    public List<List<String>> wordSquares(String[] words) {
        List<List<String>> ret = new ArrayList();
        if (words==null || words.length==0) return ret;
        Node root = new Node();
        int len = words[0].length();
        for (String word: words) add(root, word);
        Node[] rows = new Node[len];
        for (int i=0; i<len; i++) rows[i]=root;
        helper(0, 0, len, rows, ret);
        return ret;
    }
}
```
</p>


