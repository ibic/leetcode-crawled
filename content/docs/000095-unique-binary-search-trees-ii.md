---
title: "Unique Binary Search Trees II"
weight: 95
#id: "unique-binary-search-trees-ii"
---
## Description
<div class="description">
<p>Given an integer <code>n</code>, generate all structurally unique <strong>BST&#39;s</strong> (binary search trees) that store values 1 ...&nbsp;<em>n</em>.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> 3
<strong>Output:</strong>
[
&nbsp; [1,null,3,2],
&nbsp; [3,2,null,1],
&nbsp; [3,1,null,null,2],
&nbsp; [2,1,3],
&nbsp; [1,null,2,null,3]
]
<strong>Explanation:</strong>
The above output corresponds to the 5 unique BST&#39;s shown below:

   1         3     3      2      1
    \       /     /      / \      \
     3     2     1      1   3      2
    /     /       \                 \
   2     1         2                 3
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= n &lt;= 8</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)
- Tree (tree)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

**Tree definition**

First of all, here is the definition of the ```TreeNode``` which we would use.

<iframe src="https://leetcode.com/playground/qARrtrf7/shared" frameBorder="0" width="100%" height="225" name="qARrtrf7"></iframe>
<br />
<br />


---
#### Approach 1: Recursion

First of all let's count how many trees do we have to construct.
As you could check in [this article](https://leetcode.com/articles/unique-binary-search-trees/),
the number of possible BST is actually a [Catalan number](https://en.wikipedia.org/wiki/Catalan_number). 

Let's follow the logic from the above article, this time not to count but
to actually construct the trees. 
 
**Algorithm**

Let's pick up number `i` out of the sequence `1 ..n` and use it as the root
of the current tree. 
Then there are ```i - 1``` elements available for the construction of the
left subtree and ```n - i``` elements available for the right subtree. 
As we [already discussed](https://leetcode.com/articles/unique-binary-search-trees/)
that results in ```G(i - 1)``` different left subtrees and ```G(n - i)```
different right subtrees, where ```G``` is a Catalan number. 

![BST](../Figures/96_BST.png)

Now let's repeat the step above for the sequence `1 ... i - 1` to construct
all left subtrees, and then for the sequence `i + 1 ... n` to construct all
right subtrees. 

This way we have a root `i` and two lists for the possible left and right subtrees.
The final step is to loop over both lists to link left and right
subtrees to the root.  

<iframe src="https://leetcode.com/playground/RshkqCas/shared" frameBorder="0" width="100%" height="500" name="RshkqCas"></iframe>

**Complexity analysis**

* Time complexity : The main computations are to construct all possible 
trees with a given root, that is actually Catalan number $$G_n$$ as was discussed
above. This is done `n` times, that results in time complexity $$n G_n$$.
Catalan numbers grow as $$\frac{4^n}{n^{3/2}}$$ that gives the 
final complexity $$\mathcal{O}(\frac{4^n}{n^{1/2}})$$.
Seems to be large but let's not forget that here we're asked to generate
$$G_n \sim \frac{4^n}{n^{3/2}}$$ tree objects as output.

* Space complexity : $$n G_n$$ as we keep $$G_n$$ trees with `n` elements each, 
that results in $$\mathcal{O}(\frac{4^n}{n^{1/2}})$$
complexity.

## Accepted Submission (python3)
```python3
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def __init__(self):
        self.memo = {}
        #self.memo[self.genKey(0, 1)] = []

    # generate key for map
    def genKey(self, m, n):
        return (m, n)

    def genTreesRange(self, start, stop):
        if stop == start:
            return [None]
        key = self.genKey(start, stop)
        if key in self.memo:
            return self.memo[key]
        r = self.memo[key] = []
        for i in range(start, stop):
            left = self.genTreesRange(start, i)
            right= self.genTreesRange(i + 1, stop)
            for leftTree in left:
                for rightTree in right:
                    node = TreeNode(i)
                    node.left = leftTree
                    node.right = rightTree
                    r.append(node)
        return r

    def generateTrees(self, n):
        """
        :type n: int
        :rtype: List[TreeNode]
        """
        if n == 0:
            return []
        return self.genTreesRange(1, n + 1)

class SolutionUnconventioanl:
    def cloneTree(self, tree):
        if tree == None:
            return None
        newTree = TreeNode(tree.val)
        newTree.left = self.cloneTree(tree.left)
        newTree.right = self.cloneTree(tree.right)
        return newTree

    def getRightLength(self, tree):
        rlen = 0
        pTree = tree
        while pTree != None:
            pTree = pTree.right
            rlen += 1
        return rlen

    def generateTrees(self, n):
        """
        :type n: int
        :rtype: List[TreeNode]
        """
        memo = [[], [TreeNode(1)]]
        for i in range(2, n + 1):
            forest = []
            for preTree in memo[i - 1]:
                rlen = self.getRightLength(preTree)

                node = TreeNode(i)
                clone = self.cloneTree(preTree)
                rm = clone
                for k in range(rlen - 1):
                    rm = rm.right
                rm.right = node
                forest.append(clone)

                for j in range(rlen):
                    clone = self.cloneTree(preTree)
                    front = TreeNode(-1)
                    front.right = clone
                    node = TreeNode(i)
                    pc = front
                    for k in range(j):
                        pc = pc.right
                    originalRight = pc.right
                    pc.right = node
                    node.left = originalRight
                    forest.append(front.right)
            memo.append(forest)

        return memo[n]
                
        
```

## Top Discussions
### A simple recursive solution
- Author: Jayanta
- Creation Date: Tue Sep 02 2014 16:17:00 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 04:36:28 GMT+0800 (Singapore Standard Time)

<p>
I start by noting that 1..n is the in-order traversal for any BST with nodes 1 to n. So if I pick i-th node as my root, the left subtree will contain elements 1 to (i-1), and the right subtree will contain elements (i+1) to n. I use recursive calls to get back all possible trees for left and right subtrees and combine them in all possible ways with the root. 



    public class Solution {
        public List<TreeNode> generateTrees(int n) {
            
            return genTrees(1,n);
        }
            
        public List<TreeNode> genTrees (int start, int end)
        {
    
            List<TreeNode> list = new ArrayList<TreeNode>();
    
            if(start>end)
            {
                list.add(null);
                return list;
            }
            
            if(start == end){
                list.add(new TreeNode(start));
                return list;
            }
            
            List<TreeNode> left,right;
            for(int i=start;i<=end;i++)
            {
                
                left = genTrees(start, i-1);
                right = genTrees(i+1,end);
                
                for(TreeNode lnode: left)
                {
                    for(TreeNode rnode: right)
                    {
                        TreeNode root = new TreeNode(i);
                        root.left = lnode;
                        root.right = rnode;
                        list.add(root);
                    }
                }
                    
            }
            
            return list;
        }
    }
</p>


### Java Solution with DP
- Author: jianwu
- Creation Date: Mon Aug 25 2014 02:26:18 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 23:59:18 GMT+0800 (Singapore Standard Time)

<p>
Here is my java solution with DP:


    public static List<TreeNode> generateTrees(int n) {
        List<TreeNode>[] result = new List[n + 1];
        result[0] = new ArrayList<TreeNode>();
        if (n == 0) {
            return result[0];
        }

        result[0].add(null);
        for (int len = 1; len <= n; len++) {
            result[len] = new ArrayList<TreeNode>();
            for (int j = 0; j < len; j++) {
                for (TreeNode nodeL : result[j]) {
                    for (TreeNode nodeR : result[len - j - 1]) {
                        TreeNode node = new TreeNode(j + 1);
                        node.left = nodeL;
                        node.right = clone(nodeR, j + 1);
                        result[len].add(node);
                    }
                }
            }
        }
        return result[n];
    }

    private static TreeNode clone(TreeNode n, int offset) {
        if (n == null) {
            return null;
        }
        TreeNode node = new TreeNode(n.val + offset);
        node.left = clone(n.left, offset);
        node.right = clone(n.right, offset);
        return node;
    }



**result[i]** stores the result until length **i**. For the result for length i+1, select the root node j from 0 to i, combine the result from left side and right side. Note for the right side we have to clone the nodes as the value will be offsetted by **j**.
</p>


### Divide-and-conquer.  F(i) = G(i-1) * G(n-i)
- Author: liaison
- Creation Date: Wed Feb 04 2015 23:31:14 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 02:23:40 GMT+0800 (Singapore Standard Time)

<p>
This problem is a variant of the problem of [Unique Binary Search Trees][1]. 

I provided a solution along with explanation for the above problem, in the question ["DP solution in 6 lines with explanation"](https://leetcode.com/problems/unique-binary-search-trees/discuss/31666/DP-Solution-in-6-lines-with-explanation.-F(i-n)-G(i-1)-*-G(n-i))

It is intuitive to solve this problem by following the same algorithm. Here is the code in a divide-and-conquer style. 

    public List<TreeNode> generateTrees(int n) {
    	return generateSubtrees(1, n);
    }
 
	private List<TreeNode> generateSubtrees(int s, int e) {
		List<TreeNode> res = new LinkedList<TreeNode>();
		if (s > e) {
			res.add(null); // empty tree
			return res;
		}

		for (int i = s; i <= e; ++i) {
			List<TreeNode> leftSubtrees = generateSubtrees(s, i - 1);
			List<TreeNode> rightSubtrees = generateSubtrees(i + 1, e);

			for (TreeNode left : leftSubtrees) {
				for (TreeNode right : rightSubtrees) {
					TreeNode root = new TreeNode(i);
					root.left = left;
					root.right = right;
					res.add(root);
				}
			}
		}
		return res;
	}

  [1]: https://oj.leetcode.com/problems/unique-binary-search-trees/
  [2]: https://oj.leetcode.com/discuss/24282/dp-solution-in-6-lines-with-explanation-f-i-g-i-1-g-n-i
</p>


