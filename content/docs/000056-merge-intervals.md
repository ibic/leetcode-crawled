---
title: "Merge Intervals"
weight: 56
#id: "merge-intervals"
---
## Description
<div class="description">
<p>Given a collection of intervals, merge all overlapping intervals.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[1,3],[2,6],[8,10],[15,18]]
<strong>Output:</strong> [[1,6],[8,10],[15,18]]
<strong>Explanation:</strong> Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[1,4],[4,5]]
<strong>Output:</strong> [[1,5]]
<strong>Explanation:</strong> Intervals [1,4] and [4,5] are considered overlapping.</pre>

<p><strong>NOTE:</strong>&nbsp;input types have been changed on April 15, 2019. Please reset to default code definition to get new method signature.</p>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>intervals[i][0] &lt;= intervals[i][1]</code></li>
</ul>

</div>

## Tags
- Array (array)
- Sort (sort)

## Companies
- Facebook - 39 (taggedByAdmin: true)
- Amazon - 29 (taggedByAdmin: false)
- Bloomberg - 17 (taggedByAdmin: true)
- Google - 9 (taggedByAdmin: true)
- Paypal - 6 (taggedByAdmin: false)
- Goldman Sachs - 6 (taggedByAdmin: false)
- VMware - 6 (taggedByAdmin: false)
- Cisco - 5 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: true)
- Apple - 4 (taggedByAdmin: false)
- eBay - 4 (taggedByAdmin: false)
- ByteDance - 4 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Palantir Technologies - 2 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Square - 2 (taggedByAdmin: false)
- Netflix - 2 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- IXL - 2 (taggedByAdmin: false)
- LinkedIn - 8 (taggedByAdmin: true)
- Salesforce - 6 (taggedByAdmin: false)
- DoorDash - 4 (taggedByAdmin: false)
- Zulily - 4 (taggedByAdmin: false)
- Wayfair - 3 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: true)
- ServiceNow - 2 (taggedByAdmin: false)
- Twitch - 2 (taggedByAdmin: false)
- Postmates - 2 (taggedByAdmin: false)
- Two Sigma - 2 (taggedByAdmin: false)
- Yelp - 5 (taggedByAdmin: true)
- Wish - 5 (taggedByAdmin: false)
- SAP - 4 (taggedByAdmin: false)
- Intuit - 3 (taggedByAdmin: false)
- Walmart Labs - 3 (taggedByAdmin: false)
- Sumologic - 3 (taggedByAdmin: false)
- Dataminr - 3 (taggedByAdmin: false)
- Pinterest - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- Tencent - 2 (taggedByAdmin: false)
- Coupang - 2 (taggedByAdmin: false)
- Visa - 2 (taggedByAdmin: false)
- Cohesity - 2 (taggedByAdmin: false)
- Cruise Automation - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Morgan Stanley - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Connected Components

**Intuition**

If we draw a graph (with intervals as nodes) that contains undirected edges
between all pairs of intervals that overlap, then all intervals in each
*connected component* of the graph can be merged into a single interval.

**Algorithm**

With the above intuition in mind, we can represent the graph as an adjacency
list, inserting directed edges in both directions to simulate undirected
edges. Then, to determine which connected component each node is it, we
perform graph traversals from arbitrary unvisited nodes until all nodes have
been visited. To do this efficiently, we store visited nodes in a `Set`,
allowing for constant time containment checks and insertion. Finally, we
consider each connected component, merging all of its intervals by
constructing a new `Interval` with `start` equal to the minimum start among
them and `end` equal to the maximum end.

This algorithm is correct simply because it is basically the brute force
solution. We compare every interval to every other interval, so we know
exactly which intervals overlap. The reason for the connected component
search is that two intervals may not directly overlap, but might overlap
indirectly via a third interval. See the example below to see this more
clearly.

![Components Example](../Figures/56/component.png)
{:align="center"}

Although (1, 5) and (6, 10) do not directly overlap, either would overlap
with the other if first merged with (4, 7). There are two connected
components, so if we merge their nodes, we expect to get the following two
merged intervals:

(1, 10), (15, 20)


<iframe src="https://leetcode.com/playground/XjJLetAG/shared" frameBorder="0" width="100%" height="500" name="XjJLetAG"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$

    Building the graph costs $$O(V + E) = O(V) + O(E) = O(n) + O(n^2) = O(n^2)$$
    time, as in the worst case all intervals are mutually overlapping.
    Traversing the graph has the same cost (although it might appear higher
    at first) because our `visited` set guarantees that each node will be
    visited exactly once. Finally, because each node is part of exactly one
    component, the merge step costs $$O(V) = O(n)$$ time. This all adds up as
    follows:

    $$
        O(n^2) + O(n^2) + O(n) = O(n^2)
    $$

* Space complexity : $$O(n^2)$$

    As previously mentioned, in the worst case, all intervals are mutually
    overlapping, so there will be an edge for every pair of intervals.
    Therefore, the memory footprint is quadratic in the input size.

<br/>

---

#### Approach 2: Sorting

**Intuition**

If we sort the intervals by their `start` value, then each set of intervals
that can be merged will appear as a contiguous "run" in the sorted list.

**Algorithm**

First, we sort the list as described. Then, we insert the first interval into
our `merged` list and continue considering each interval in turn as follows:
If the current interval begins *after* the previous interval ends, then they
do not overlap and we can append the current interval to `merged`. Otherwise,
they do overlap, and we merge them by updating the `end` of the previous
interval if it is less than the `end` of the current interval.

A simple proof by contradiction shows that this algorithm always produces the
correct answer. First, suppose that the algorithm at some point fails to
merge two intervals that should be merged. This would imply that there exists
some triple of indices $$i$$, $$j$$, and $$k$$ in a list of intervals
$$\text{ints}$$ such that $$i < j < k$$ and ($$\text{ints[i]}$$, $$\text{ints[k]}$$) can be
merged, but neither ($$\text{ints[i]}$$, $$\text{ints[j]}$$) nor ($$\text{ints[j]}$$, $$\text{ints[k]}$$)
can be merged. From this scenario follow several inequalities:

$$
\begin{aligned}
    \text{ints[i].end} < \text{ints[j].start} \\
    \text{ints[j].end} < \text{ints[k].start} \\
    \text{ints[i].end} \geq \text{ints[k].start} \\
\end{aligned}
$$

We can chain these inequalities (along with the following inequality, implied
by the well-formedness of the intervals: $$\text{ints[j].start} \leq \text{ints[j].end}$$) to
demonstrate a contradiction:

$$
\begin{aligned}
    \text{ints[i].end} < \text{ints[j].start} \leq \text{ints[j].end} < \text{ints[k].start} \\
    \text{ints[i].end} \geq \text{ints[k].start}
\end{aligned}
$$

Therefore, all mergeable intervals must occur in a contiguous run of the
sorted list.

![Sorting Example](../Figures/56/sort.png)
{:align="center"}


Consider the example above, where the intervals are sorted, and then all
mergeable intervals form contiguous blocks.

<iframe src="https://leetcode.com/playground/JbFaLecg/shared" frameBorder="0" width="100%" height="500" name="JbFaLecg"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n\log{}n)$$

    Other than the `sort` invocation, we do a simple linear scan of the list,
    so the runtime is dominated by the $$O(nlgn)$$ complexity of sorting.

* Space complexity : $$O(1)$$ (or $$O(n)$$)

    If we can sort `intervals` in place, we do not need more than constant
    additional space. Otherwise, we must allocate linear space to store a
    copy of `intervals` and sort that.

## Accepted Submission (java)
```java
class Solution {
//    public int[][] merge(int[][] intervals) {
//        if (intervals.length <= 0) {
//            return intervals;
//        }
//        List<int[]> rlist = new ArrayList<>();
//        Arrays.sort(intervals, new Comparator<int[]>() {
//            @Override
//            public int compare(int[] o1, int[] o2) {
//                return o1[0] - o2[0];
//            }
//        });
//        int start = 0;
//        while (start < intervals.length) {
//            int stop = start + 1;
//            int stopValue = intervals[start][1];
//            while (stop < intervals.length && stopValue >= intervals[stop][0]) {
//                stopValue = Math.max(stopValue, intervals[stop][1]);
//                stop++;
//            }
//            int[] merged = new int[]{intervals[start][0], stopValue};
//            rlist.add(merged);
//            start = stop;
//        }
//        int[][] r = new int[rlist.size()][];
//        for (int i = 0; i < rlist.size(); i++) {
//            r[i] = rlist.get(i);
//        }
//        return r;
//    }
    public int[][] merge(int[][] intervals) {
        if (intervals.length <= 0) {
            return intervals;
        }
        List<int[]> rlist = new ArrayList<>();
        Arrays.sort(intervals, (left, right) -> left[0] - right[0]);
        int[] curInterval = intervals[0];
        for (int[] interval: intervals) {
            if (curInterval[1] >= interval[0]) {
                curInterval[1] = Math.max(curInterval[1], interval[1]);
            } else {
                rlist.add(curInterval);
                curInterval = interval;
            }
        }
        rlist.add(curInterval);
        return rlist.toArray(new int[][]{});
    }
}

```

## Top Discussions
### A simple Java solution
- Author: brubru777
- Creation Date: Thu Oct 23 2014 10:14:26 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 30 2019 03:10:21 GMT+0800 (Singapore Standard Time)

<p>
The idea is to sort the intervals by their starting points. Then, we take the first interval and compare its end with the next intervals starts. As long as they overlap, we update the end to be the max end of the overlapping intervals. Once we find a non overlapping interval, we can add the previous "extended" interval and start over.

Sorting takes O(n log(n)) and merging the intervals takes O(n). So, the resulting algorithm takes O(n log(n)).

I used a lambda comparator (Java 8) and a for-each loop to try to keep the code clean and simple.

EDIT: The function signature changed in april 2019.
Here is a new version of the algorithm with arrays. To make more memory efficient, I reused the initial array (sort of "in-place") but it would be easy to create new subarrays if you wanted to keep the initial data.
It takes less memory than 99% of the other solutions (sometimes 90% depending on the run) and is more than 10 times faster than the previous version with lists.

	class Solution {
		public int[][] merge(int[][] intervals) {
			if (intervals.length <= 1)
				return intervals;

			// Sort by ascending starting point
			Arrays.sort(intervals, (i1, i2) -> Integer.compare(i1[0], i2[0]));

			List<int[]> result = new ArrayList<>();
			int[] newInterval = intervals[0];
			result.add(newInterval);
			for (int[] interval : intervals) {
				if (interval[0] <= newInterval[1]) // Overlapping intervals, move the end if needed
					newInterval[1] = Math.max(newInterval[1], interval[1]);
				else {                             // Disjoint intervals, add the new interval to the list
					newInterval = interval;
					result.add(newInterval);
				}
			}

			return result.toArray(new int[result.size()][]);
		}
	}

Previous version with lists.

    public List<Interval> merge(List<Interval> intervals) {
        if (intervals.size() <= 1)
            return intervals;
        
        // Sort by ascending starting point using an anonymous Comparator
        intervals.sort((i1, i2) -> Integer.compare(i1.start, i2.start));
        
        List<Interval> result = new LinkedList<Interval>();
        int start = intervals.get(0).start;
        int end = intervals.get(0).end;
        
        for (Interval interval : intervals) {
            if (interval.start <= end) // Overlapping intervals, move the end if needed
                end = Math.max(end, interval.end);
            else {                     // Disjoint intervals, add the previous one and reset bounds
                result.add(new Interval(start, end));
                start = interval.start;
                end = interval.end;
            }
        }
        
        // Add the last interval
        result.add(new Interval(start, end));
        return result;
    }

EDIT: Updated with Java 8 lambda comparator.
EDIT 25/05/2019: Updated for new method signature.
</p>


### 7 lines, easy, Python
- Author: StefanPochmann
- Creation Date: Fri Jun 26 2015 19:32:52 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 05:32:27 GMT+0800 (Singapore Standard Time)

<p>
Just go through the intervals sorted by start coordinate and either combine the current interval with the previous one if they overlap, or add it to the output by itself if they don't.

    def merge(self, intervals):
        out = []
        for i in sorted(intervals, key=lambda i: i.start):
            if out and i.start <= out[-1].end:
                out[-1].end = max(out[-1].end, i.end)
            else:
                out += i,
        return out
</p>


### C++ 10 line solution. easing understanding
- Author: lchen77
- Creation Date: Fri Jul 31 2015 23:52:35 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 14:10:45 GMT+0800 (Singapore Standard Time)

<p>
    vector<Interval> merge(vector<Interval>& ins) {
        if (ins.empty()) return vector<Interval>{};
        vector<Interval> res;
        sort(ins.begin(), ins.end(), [](Interval a, Interval b){return a.start < b.start;});
        res.push_back(ins[0]);
        for (int i = 1; i < ins.size(); i++) {
            if (res.back().end < ins[i].start) res.push_back(ins[i]);
            else
                res.back().end = max(res.back().end, ins[i].end);
        }
        return res;
    }
</p>


