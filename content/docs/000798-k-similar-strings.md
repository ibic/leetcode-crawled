---
title: "K-Similar Strings"
weight: 798
#id: "k-similar-strings"
---
## Description
<div class="description">
<p>Strings&nbsp;<code>A</code> and <code>B</code> are <code>K</code>-similar (for some non-negative integer <code>K</code>) if we can swap the positions of two letters in <code>A</code> exactly <code>K</code>&nbsp;times so that the resulting string equals <code>B</code>.</p>

<p>Given two anagrams <code>A</code> and <code>B</code>, return the smallest <code>K</code>&nbsp;for which <code>A</code> and <code>B</code> are <code>K</code>-similar.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">&quot;ab&quot;</span>, B = <span id="example-input-1-2">&quot;ba&quot;</span>
<strong>Output: </strong><span id="example-output-1">1</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">&quot;abc&quot;</span>, B = <span id="example-input-2-2">&quot;bca&quot;</span>
<strong>Output: </strong><span id="example-output-2">2</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-3-1">&quot;abac&quot;</span>, B = <span id="example-input-3-2">&quot;baca&quot;</span>
<strong>Output: </strong><span id="example-output-3">2</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-4-1">&quot;aabc&quot;</span>, B = <span id="example-input-4-2">&quot;abca&quot;</span>
<strong>Output: </strong><span id="example-output-4">2</span></pre>
</div>
</div>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length == B.length &lt;= 20</code></li>
	<li><code>A</code> and <code>B</code> contain only lowercase letters from the set <code>{&#39;a&#39;, &#39;b&#39;, &#39;c&#39;, &#39;d&#39;, &#39;e&#39;, &#39;f&#39;}</code></li>
</ol>

</div>

## Tags
- Breadth-first Search (breadth-first-search)
- Graph (graph)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach Framework

**Explanation**

We'll call the *underlying graph* of the problem, the graph with 6 nodes `'a', 'b', ..., 'f'` and the edges `A[i] -> B[i]`.  Our goal is for this graph to have only self-edges (edges of the form `a -> a`.)  Let's make some deductions about how swaps between `A[i]` and `A[j]` affect this graph, and the nature of optimal swap schedules.

If `A = 'ca...'` and `B = 'ab...'`, then the first two edges of the underlying graph are `c -> a` and `a -> b`; and a swap between `A[1]` and `A[0]` changes these two edges to the single edge `c -> b`.  Let's call this type of operation *'cutting corners'*.  Intuitively, our optimal swap schedule always increases the number of matches (`A[i] == B[i]`s) for each swap, so cutting corners is the only type of operation we need to consider.  (This is essentially the *happy swap assumption*, proved in [765 - Couples Holding Hands](https://leetcode.com/articles/couples-holding-hands/))

Now consider any cycle decomposition of the underlying graph.  [This decomposition (or the number of cycles), is not necessarily unique.]  Through operations of cutting corners, we'll delete all the (non-self) edges.  Each cycle of length `k` requires `k-1` operations to delete.  Thus, the answer is just the minimum possible value of $$\sum (C_k - 1)$$, where $$C_1, \cdots C_k$$ are the lengths of the cycles in some cycle decomposition of the underlying graph.  This can be re-written as $$\text{(Number of non-self edges)} - \text{(Number of cycles)}$$.  Hence, we want to maximize the number of cycles in a cycle decomposition of the underlying graph.
<br />
<br />

---

#### Approach 1: Brute Force with Dynamic Programming

**Intuition and Algorithm**

Let $$P_1, P_2, \cdots$$ be possible cycles of the underlying graph $$G$$.  Let's attempt to write $$G = \sum k_i P_i$$ for some constants $$k_i$$.  Then, the number of cycles is $$\sum k_i$$.

We can represent $$G$$ and $$P_i$$ as the number of directed edges from node $$X$$ to $$Y$$.  For example, if $$P_1$$ is the cycle `a -> b -> d -> e -> a`, then $$P_1$$ is `a -> b` plus `b -> d` plus `d -> e` plus `e -> a`.  This can be represented as a column vector `possibles[0]` of 1s and 0s, with four 1s, (each `possibles[0][i] == 1` represents the `i`th directed edge being there [and having quantity 1]).  Similarly, the graph $$G$$ can also be represented as a column vector.

This sets the stage for dynamic programming.  For each graph $$G$$, represented as a column vector, say we want to find `numCycles(G)`.  We can take all possible cycles $$C$$, and check if $$G$$ contains $$C$$.  If it does, then a candidate answer is `1 + numCycles(G - C)`.

It should also be noted that maximizing the number of cycles cannot be done in a greedy way, ie. by always removing the lowest size cycle.  For example, consider the graph with edges `a -> b -> c -> a`, `b -> d -> e -> b`, and `c -> e -> f -> c`.  Those form cycles, and there is a fourth 3-cycle `b -> c -> e -> b`.  If we remove that cycle first, then we would have only two cycles; but if we remove the first 3 cycles, then we would have three cycles.

<iframe src="https://leetcode.com/playground/BR9ryonr/shared" frameBorder="0" width="100%" height="500" name="BR9ryonr"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(2^{N+W})$$, where $$N$$ is the length of the string, and $$W$$ is the length of the alphabet.

* Space Complexity:  $$O(2^{N+W})$$.
<br />
<br />

---
#### Approach 2: Pruned Breadth First Search

**Intuition**

Based on the *underlying graph* interpretation of the problem, we can prove that an optimal solution swaps the left-most unmatched character `A[i]` with an appropriate match `A[j] == B[i] (j > i)`.

This reduces the number of "neighbors" of a node (string state) from $$O(N^2)$$ to $$O(N)$$, but it also focuses our search greatly.  Each node searched with `k` matches, will have at most $$2^k$$ swaps on the unmatched characters.  This leads to $$\sum_k \binom{N}{k} 2^k = 3^N$$ checked states.  Furthermore, some characters are the same, so we must divide the number of states by approximate factors of $$\prod (N_i)!$$ [where $$N_i$$ is the number of occurrences of the $$i$$th character in `A`.]  With $$N \leq 20$$, this means the number of states will be small.

**Algorithm**

We'll perform a regular breadth-first search.  The neighbors to each node string `S` are all the strings reachable with 1 swap, that match the first unmatched character in `S`.

<iframe src="https://leetcode.com/playground/xjHtkSC9/shared" frameBorder="0" width="100%" height="500" name="xjHtkSC9"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\sum_{k=0}^n \binom{N}{k} \frac{\min(2^k, (N-k)!)}{W * (\frac{N-k}{W})!})$$, where $$N$$ is the length of the string, and $$W$$ is the length of the alphabet.

* Space Complexity:  $$O(N * t)$$, where $$t$$ is the time complexity given above.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### JAVA BFS 32 ms clean/concise/explanation/whatever
- Author: caraxin
- Creation Date: Mon Jun 18 2018 04:10:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 27 2019 05:23:15 GMT+0800 (Singapore Standard Time)

<p>
The trick is that you just need to swap the first (a,b)->(b,a) pair every round, instead of other pairs like (c,d)->(d,c) (otherwise it would cause TLE), and BFS would guarantee the shortest path.
credit to @sijian2001
**update** sorry I just realized that my explaination has some flaw (the code is correct though).
I turned  
```if (s.charAt(j)==B.charAt(j) || s.charAt(i)!=B.charAt(j) ) continue;```
into
``` if (s.charAt(j)==B.charAt(j) || s.charAt(i)!=B.charAt(j) || s.charAt(j)!=B.charAt(i) ) continue;```
and it can\'t pass all test case, which means we are not actually swapping the direct reversed pair, we are just trying to fix the some wrong character in each loop.
To make it better understood, I suggest we change this line to:
```if (s.charAt(j)==B.charAt(j) || s.charAt(j)!=B.charAt(i) ) continue;```
which means we only swap the i-th and j-th character when j-th character is wrong and it happends to be the target character of i-th position. 
So that in each round we will repair the **first unordered character** and as a result move forward at least 1 step.
Sorry again for misleading so many people.
```
class Solution {
    public int kSimilarity(String A, String B) {
        if (A.equals(B)) return 0;
        Set<String> vis= new HashSet<>();
        Queue<String> q= new LinkedList<>();
        q.add(A);
        vis.add(A);
        int res=0;
        while(!q.isEmpty()){
            res++;
            for (int sz=q.size(); sz>0; sz--){
                String s= q.poll();
                int i=0;
                while (s.charAt(i)==B.charAt(i)) i++;
                for (int j=i+1; j<s.length(); j++){
                    if (s.charAt(j)==B.charAt(j) || s.charAt(j)!=B.charAt(i) ) continue;
                    String temp= swap(s, i, j);
                    if (temp.equals(B)) return res;
                    if (vis.add(temp)) q.add(temp);
                }
            }
        }
        return res;
    }
    public String swap(String s, int i, int j){
        char[] ca=s.toCharArray();
        char temp=ca[i];
        ca[i]=ca[j];
        ca[j]=temp;
        return new String(ca);
    }
}
```
![image](https://s3-lc-upload.s3.amazonaws.com/users/caraxin/image_1529267603.png)

</p>


### Logical Thinking with Clear Java Code
- Author: GraceMeng
- Creation Date: Thu Jul 19 2018 18:28:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 03 2019 06:08:11 GMT+0800 (Singapore Standard Time)

<p>
**Logical Thinking**
In fact, the essence of the problem is to get the minimum number of swaps A needs to make itself equal to B.

It is a **shortest-path** problem so we can utilize **BFS**. The `graph` we build sets all possible strings that can be swapped from A as `node`s, and an `edge` exists if one string can be transformed into the other by one swap. We start at `A`, and target at `B`.

However, that will cause TLE.

We set all possible strings that can be formed by swapping the positions of two letters in A\' one time as neighbours of A\', however, only those can make A and B differ less are **meaningful neighbours**. That is, **if A\'[i] != B[i] but A\'[j] == B[i], the string formed by swap(A, i, j)** is a meaningful neighbour of A\'. Please note that we just need to swap the first pair (A\'[i], A\'[j]) we meet because the order of swap doesn\'t matter.

**Clear Java Code**
```
    public int kSimilarity(String A, String B) {

        Queue<String> queue = new LinkedList<>();
        Set<String> visited = new HashSet<>();
        queue.offer(A);
        visited.add(A);
        int level = 0;

        while (!queue.isEmpty()) {
            int sz = queue.size();
            for (int i = 0; i < sz; i++) {
                String curNode = queue.poll();
                if (curNode.equals(B)) {
                    return level;
                }
                for (String neighbour : getNeighbours(curNode, B)) {
                    if (!visited.contains(neighbour)) {
                        queue.offer(neighbour);
                        visited.add(neighbour);
                    }
                }
            }
            level++;
        }

        return -1;
    }
    
    private List<String> getNeighbours(String S, String B) { 
        
        List<String> result = new ArrayList<>();
        char[] arr = S.toCharArray(); 
        
        int i = 0;
        for (; i < arr.length; i++) {
            if (arr[i] != B.charAt(i)) {
                break;
            }
        }
                
        for (int j = i + 1; j < arr.length; j++) { 
            if (arr[j] == B.charAt(i)) {
                swap(arr, i, j);             
                result.add(new String(arr));
                swap(arr, i, j);
            }
        }     
        
        return result;
    }
    
    private void swap(char[] arr, int i, int j) {
        
        char tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
```
**I would appreciate your VOTE UP ;)**
</p>


### C++ 6ms Solution
- Author: yijinru
- Creation Date: Mon Jun 18 2018 22:58:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 15 2018 17:39:38 GMT+0800 (Singapore Standard Time)

<p>
We employ a greedy recursive algorithm based on the following observations:
1. Let n be the size of anagrams, at most n (actually n-1) swaps would make them identical.
2. In the optimal strategy, each swap should contribute to at least 1 match of characters. We also note the best we can achieve in a single swap is 2 matches, which is guaranteed to be optimal (although it might not be the only one). Therefore, if A[i]!=B[i], we should try our luck and look for a position j, such that A[j]==B[i] && A[i]==B[j], if one is found, then we achieve a optimal double match and proceed to match the remaining. Otherwise, we look for a position j, such that A[j]==B[i], and MAKE SURE IT IS NOT A MATCH ALREADY (i.e., A[j]!=B[j], otherwise you simply push the mismatch in position i to position j and don\'t make any real progress). There might be multiple positions j satisfying such condition, and we need to go over each one of them, as certain j might give rise to a double match later on, and therefore outperform others.

```
class Solution {
public:
    int kSimilarity(string A, string B) {
      for (int i=0; i<A.size(); i++) {
        if (A[i]==B[i]) continue;
        vector<int> matches;
        for (int j=i+1;j<A.size();j++) { 
          if (A[j]==B[i] && A[j]!=B[j]) {
            matches.push_back(j);
            if (A[i]==B[j]) {
              swap(A[i],A[j]);
              return 1+kSimilarity(A.substr(i+1),B.substr(i+1));
            }
          }
        }
        int best=A.size()-1;
        for (int j: matches) {
            swap(A[i],A[j]);
            best = min(best, 1+kSimilarity(A.substr(i+1),B.substr(i+1)));
            swap(A[i],A[j]);
        }
        return best;
      }
      return 0;
    }
};
```
</p>


