---
title: "Minimum Difference Between Largest and Smallest Value in Three Moves"
weight: 1368
#id: "minimum-difference-between-largest-and-smallest-value-in-three-moves"
---
## Description
<div class="description">
<p>Given an array <code>nums</code>, you are allowed to choose one element of <code>nums</code> and change it by any&nbsp;value in one move.</p>

<p>Return the minimum difference between the largest and smallest value of <code>nums</code>&nbsp;after perfoming at most 3 moves.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [5,3,2,4]
<strong>Output:</strong> 0
<strong>Explanation:</strong> Change the array [5,3,2,4] to [<strong>2</strong>,<strong>2</strong>,2,<strong>2</strong>].
The difference between the maximum and minimum is 2-2 = 0.</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,5,0,10,14]
<strong>Output:</strong> 1
<strong>Explanation:</strong> Change the array [1,5,0,10,14] to [1,<strong>1</strong>,0,<strong>1</strong>,<strong>1</strong>]. 
The difference between the maximum and minimum is 1-0 = 1.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> nums = [6,6,0,1,1,4,6]
<strong>Output:</strong> 2
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,5,6,14,15]
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= nums.length &lt;= 10^5</code></li>
	<li><code>-10^9 &lt;= nums[i] &lt;= 10^9</code></li>
</ul>
</div>

## Tags
- Array (array)
- Sort (sort)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Jul 12 2020 00:04:35 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 17 2020 00:46:56 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
If we can do 0 move, return max(A) - min(A)
If we can do 1 move, return min(the second max(A) - min(A),  the max(A) - second min(A))
and so on.
<br>

# **Explanation**
We have 4 plans:
1. kill 3 biggest elements
2. kill 2 biggest elements + 1 smallest elements
3. kill 1 biggest elements + 2 smallest elements
3. kill 3 smallest elements
<br>

Example from @himanshusingh11:

`A = [1,5,6,13,14,15,16,17]`
`n = 8`

Case 1: kill 3 biggest elements

All three biggest elements can be replaced with 14
[1,5,6,13,14,`15,16,17`] -> [1,5,6,13,14,`14,14,14`] == can be written as `A[n-4] - A[0] == (14-1 = 13)`

Case 2: kill 2 biggest elements + 1 smallest elements

[`1`,5,6,13,14,15,`16,17`] -> [`5`,5,6,13,14,15,`15,15`] == can be written as `A[n-3] - A[1] == (15-5 = 10)`

Case 3: kill 1 biggest elements + 2 smallest elements

[`1,5`,6,13,14,15,16,`17`] -> [`6,6`,6,13,14,15,16,`16`] == can be written as `A[n-2] - A[2] == (16-6 = 10)`

Case 4: kill 3 smallest elements

[`1,5,6`,13,14,15,16,17] -> [`13,13,13`,13,14,15,16,17] == can be written as `A[n-1] - A[3] == (17-13 = 4)`

Answer is minimum of all these cases!
<br>

# Solution 1: Quick Sort
I used quick sort to find out the biggest and smallest
So time and space are `O(quick sort)`
<br>

**C++**
```cpp
    int minDifference(vector<int>& A) {
        int n = A.size();
        if (n < 5) return 0;
        sort(A.begin(), A.end());
        return min({A[n - 1] - A[3], A[n - 2] - A[2], A[n - 3] - A[1], A[n - 4] - A[0]});
    }
```
**Java**
```java
    public int minDifference(int[] A) {
        int n = A.length, res = Integer.MAX_VALUE;
        if (n < 5) return 0;
        Arrays.sort(A);
        for (int i = 0; i < 4; ++i) {
            res = Math.min(res, A[n - 4 + i] - A[i]);
        }
        return res;
    }
```
**Python:**
```py
    def minDifference(self, A):
        A.sort()
        return min(b - a for a, b in zip(A[:4], A[-4:]))
```
<br>

# Solution 2: Partial Sorting
Changed from @Sklert
Reference https://en.cppreference.com/w/cpp/algorithm/partial_sort

Time `O(NlogK)`
Space `O(logK)`

**C++**
```cpp
    int minDifference(vector<int>& A) {
        int n = A.size();
        if (n < 5)
            return 0;
        partial_sort(A.begin(), A.begin() + 4, A.end());
        nth_element(A.begin() + 4, A.end() - 4, A.end());
        sort(A.end() - 4, A.end());
        return min({A[n - 1] - A[3], A[n - 2] - A[2], A[n - 3] - A[1], A[n - 4] - A[0]});
    }
```
**Python**
```py
    def minDifference(self, A):
        return min(a - b for a,b in zip(heapq.nlargest(4, A), heapq.nsmallest(4, A)[::-1]))
```
</p>


### Detailed Explanation with Code
- Author: karprabin1
- Creation Date: Thu Jul 23 2020 22:59:10 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 23 2020 22:59:10 GMT+0800 (Singapore Standard Time)

<p>
**Detailed Video Explanation** https://www.youtube.com/watch?v=Ht6hc3UsvY0

**Idea:** 
Case 1: Update only 3 min, 0 max numbers
Case 2: Update 2 min, 1 max numbers
Case 2: Update 1 min, 2 max numbers
Case 2: Update 0 min, 3 max numbers

```
class Solution {
    public int minDifference(int[] nums) {
        int n = nums.length;
        if(n < 5) return 0; 
        Arrays.sort(nums);
        int result = Integer.MAX_VALUE;
        for(int i = 0; i < 4; i++) {
            result = Math.min(result, nums[n - 1 - 3 + i] - nums[i]);
        }
        return result;
    }
}
```
</p>


### Similar to 1423. Maximum Points You Can Obtain from Cards
- Author: claytonjwong
- Creation Date: Sun Jul 12 2020 06:18:02 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 12 2020 13:15:56 GMT+0800 (Singapore Standard Time)

<p>
**Synopsis:**

Sort `A`, then perform a brute-force search of all posibilities using a sliding window of size 3 similar to [1423. Maximum Points You Can Obtain from Cards](https://leetcode.com/problems/maximum-points-you-can-obtain-from-cards/discuss/597883/Javascript-and-C%2B%2B-solutions).  Initially set the disclude window of size 3 to the last 3 elements of `A`, then slide the disclude window by 3.  Since `A` is sorted, we know the element at the i-th index is the minimum element under consideration and the element at the j-th index is the maximum element under consideration.  Below is a picture of this sliding disclude window for example 4.  In this example, `i` and `j` are adjacent to eachother, however, for larger inputs there exists an arbitrary and irrelevant amount of monotonically non-decreasing values in between `i` and `j`.

**Example 4:**

**Input:** A = [1,5,6,14,15]
**Output:** 1

![image](https://assets.leetcode.com/users/images/6fed1231-e63e-45e6-acc7-a22dbca7664e_1594507497.278275.png)

**Note:** these are the original [Javascript and C++ solutions](https://leetcode.com/problems/minimum-difference-between-largest-and-smallest-value-in-three-moves/discuss/730531/Javascript-and-C%2B%2B-solutions) I came up with during this contest.  I simply refactored those solutions into the concise solutions below.

---

*Javascript*
```
let minDifference = (A, min = Infinity) => {
    A.sort((a, b) => a - b);
    let N = A.length,
        i = 0,
        j = N - 4; // \u2B50\uFE0F -4 because N - 1 is the last element and we want to disclude 3 elements, thus N - 1 - 3 == N - 4
    while (0 <= j && j < N)
		min = Math.min(min, A[j++] - A[i++]); // slide window by 3 \uD83D\uDC49
    return min < Infinity ? min : 0;
};
```

*C++*
```
class Solution {
public:
    using VI = vector<int>;
    static constexpr int INF = 2e9 + 7;
    int minDifference(VI& A, int min = INF) {
        sort(A.begin(), A.end());
        int N = A.size(),
            i = 0,
            j = N - 4;// \u2B50\uFE0F -4 because N - 1 is the last element and we want to disclude 3 elements, thus N - 1 - 3 == N - 4
        while (0 <= j && j < N)
			min = std::min(min, A[j++] - A[i++]); // slide window by 3 \uD83D\uDC49
        return min < INF ? min : 0;
    }
};
```
</p>


