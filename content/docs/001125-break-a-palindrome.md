---
title: "Break a Palindrome"
weight: 1125
#id: "break-a-palindrome"
---
## Description
<div class="description">
<p>Given a palindromic string <code>palindrome</code>, replace <strong>exactly one</strong> character by any lowercase English letter so that the string becomes the lexicographically smallest possible string that <strong>isn&#39;t</strong> a palindrome.</p>

<p>After doing so, return the final string.&nbsp; If there is no way to do so, return the empty string.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> palindrome = &quot;abccba&quot;
<strong>Output:</strong> &quot;aaccba&quot;
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> palindrome = &quot;a&quot;
<strong>Output:</strong> &quot;&quot;
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= palindrome.length &lt;= 1000</code></li>
	<li><code>palindrome</code>&nbsp;consists of only lowercase English letters.</li>
</ul>
</div>

## Tags
- String (string)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- VMware - 3 (taggedByAdmin: true)
- Roblox - 3 (taggedByAdmin: false)
- JPMorgan - 2 (taggedByAdmin: false)
- Expedia - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Easy and Concise
- Author: lee215
- Creation Date: Sun Jan 26 2020 00:22:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 26 2020 00:58:32 GMT+0800 (Singapore Standard Time)

<p>
Check half of the string,
replace a non `\'a\'` character to `\'a\'`.

If only one character, return empty string.
Otherwise repalce the last character to `\'b\'`
<br>

## **Complexity**
Time `O(N)`
Space `O(N)`
<br>

**Java**
From @kniffina
```java
    public String breakPalindrome(String palindrome) {
        char[] s = palindrome.toCharArray();
        int n = s.length;

        for (int i = 0; i < n / 2; i++) {
            if (s[i] != \'a\') {
                s[i] = \'a\';
                return String.valueOf(s);
            }
        }
        s[n - 1] = \'b\'; //if all \'a\'
        return n < 2 ? "" : String.valueOf(s);
    }
```
**C++**
```cpp
    string breakPalindrome(string S) {
        int n = S.size();
        for (int i = 0; i < n / 2; ++i) {
            if (S[i] != \'a\') {
                S[i] = \'a\';
                return S;
            }
        }
        S[n - 1] = \'b\';
        return n < 2 ? "" : S;
    }
```
**Python**
```py
    def breakPalindrome(self, S):
        for i in xrange(len(S) / 2):
            if S[i] != \'a\':
                return S[:i] + \'a\' + S[i + 1:]
        return S[:-1] + \'b\' if S[:-1] else \'\'
```


</p>


### Break Palindrome [C++]
- Author: AshokaKS
- Creation Date: Mon Jan 27 2020 21:43:32 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 27 2020 21:43:32 GMT+0800 (Singapore Standard Time)

<p>
Hint 1 : Length of string == 1, return ""
H2: Palindrome string is lexicographical, so return first non \'a\' by \'a\' to break sequence.

100% accpeted solution.

```
string breakPalindrome(string p) {
        
        size_t sz = p.size();
        if (sz <= 1) return "";
        
        for (size_t i=0; i < sz/2; i++) {
            if (p[i] > \'a\')  {
                p[i] = \'a\';
                return p;
            }
        }
        
        p[sz-1] = \'b\';
        return p;
    }
```
</p>


### Straightforward Python
- Author: sushanthsamala
- Creation Date: Sun Jan 26 2020 07:27:49 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 26 2020 07:28:07 GMT+0800 (Singapore Standard Time)

<p>

```
if len(palindrome) == 1: return \'\'
        for i,val in enumerate(palindrome):
            if val != \'a\' and i != len(palindrome)//2:
                return palindrome[:i] + \'a\' + palindrome[i+1:]
            elif val == \'a\' and i == len(palindrome) - 1:
                return palindrome[:-1] + \'b\'
```
</p>


