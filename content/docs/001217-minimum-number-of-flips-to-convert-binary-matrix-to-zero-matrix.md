---
title: "Minimum Number of Flips to Convert Binary Matrix to Zero Matrix"
weight: 1217
#id: "minimum-number-of-flips-to-convert-binary-matrix-to-zero-matrix"
---
## Description
<div class="description">
<p>Given a <code>m x n</code> binary matrix <code>mat</code>. In one step, you can choose one cell and flip it and all the four neighbours of it&nbsp;if they exist (Flip is changing 1 to 0 and 0 to 1). A pair of cells are called neighboors if they share one edge.</p>

<p>Return the <em>minimum number of steps</em> required to convert <code>mat</code>&nbsp;to a zero matrix or <strong>-1</strong> if you cannot.</p>

<p>Binary matrix is a matrix with all cells equal to 0 or 1 only.</p>

<p>Zero matrix is a matrix with all cells equal to 0.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2019/11/28/matrix.png" style="width: 409px; height: 86px;" />
<pre>
<strong>Input:</strong> mat = [[0,0],[0,1]]
<strong>Output:</strong> 3
<strong>Explanation:</strong> One possible solution is to flip (1, 0) then (0, 1) and finally (1, 1) as shown.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> mat = [[0]]
<strong>Output:</strong> 0
<strong>Explanation:</strong> Given matrix is a zero matrix. We don&#39;t need to change it.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> mat = [[1,1,1],[1,0,1],[0,0,0]]
<strong>Output:</strong> 6
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> mat = [[1,0,0],[1,0,0]]
<strong>Output:</strong> -1
<strong>Explanation:</strong> Given matrix can&#39;t be a zero matrix
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m ==&nbsp;mat.length</code></li>
	<li><code>n ==&nbsp;mat[0].length</code></li>
	<li><code>1 &lt;= m&nbsp;&lt;= 3</code></li>
	<li><code>1 &lt;= n&nbsp;&lt;= 3</code></li>
	<li><code>mat[i][j]</code> is 0 or 1.</li>
</ul>

</div>

## Tags
- Breadth-first Search (breadth-first-search)

## Companies
- Google - 3 (taggedByAdmin: false)
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] Bit vector + Regular BFS
- Author: PhoenixDD
- Creation Date: Sun Dec 08 2019 12:00:55 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Mar 18 2020 11:00:20 GMT+0800 (Singapore Standard Time)

<p>
**Observation**
Since the problem constraints are so small we can easily come up with a BFS/DFS solution without worrying about TLE.
We can improve upon the time by using bit vectors to store the state of the matrix. At each possible matrix state we can perform flip operation on any of the cells of the matrix and thus during BFS we need to consider this when traversing the neighbours of the nodes (This will be clearer when you go through the solution).

**Note:** The solution may seem long as I have broken the operations down to different functons for readability. The solution however is very straightforward once you go through it. 
Also as pointed in one of the comments, you can make the code shorter by not creating the bit vectors but that would increase the runtime of the solution since each check and insertion on a hash set would be O(mn) as opposed to O(1) in this case.

**Solution**
```c++
static vector<int> dirs={0,1,0,-1,0};
class Solution {
public:
    int m,n;                       //Stores #rows and #columns of the matrix.
    int createBitVec(vector<vector<int>>& mat)        //Creates bitVector using a matrix of `m` rows and `n` columns.
    {
        int bitvec=0;
        for(int i=0;i<m;i++)
            for(int j=0;j<n;j++)
                bitvec<<=1,bitvec|=mat[i][j];
        return bitvec;
    }
    int getFlip(int i,int j,int bitVec)       //Get flip of the matrix from position i,j and return it\'s bit vector representation.
    {
        int x,y,pos=m*n-1-i*n-j;             //Get position in bit vector using i,j.
        bitVec^=1<<pos;
        for(int d=0;d<4;d++)
        {
            x=i+dirs[d],y=j+dirs[d+1];
            if(x>=0&&y>=0&&x<m&&y<n)
                pos=m*n-1-x*n-y,bitVec^=1<<pos;             //Get position in bit vector using x,y and flip it.
        }
        return bitVec;
    }
    int minFlips(vector<vector<int>>& mat) 
    {
        m=mat.size(),n=mat[0].size();
        int bitvec=createBitVec(mat);                     //Bitvec stores initial state of the matrix
        if(!bitvec)
            return 0;
        queue<int> q;
        unordered_set<int> visited;                      //To keep track of visited states.
        int distance=0,size;
        q.push(bitvec);						//Push intial state to queue as starting state.
        while(!q.empty())                              //Regular BFS.
        {
            size=q.size();
            while(size--)
            {
                if(!q.front())
                    return distance;
                for(int i=0;i<m;i++)                   //Flip the matrix from every cell.
                    for(int j=0;j<n;j++)
                    {
                        bitvec=getFlip(i,j,q.front());  //Get bit vector for flip from position i,j.
                        if(!visited.count(bitvec))
                            q.push(bitvec),visited.insert(bitvec);
                    }
                q.pop();
            }
            distance++;
        }
        return -1;   
    }
};
```
**Complexity**
Time: `O(mn*2^mn)`.
Space: `O(2^mn)`.

</p>


### [Java/Python 3] Convert matrix to int:  BFS and DFS codes w/ explanation, comments and analysis.
- Author: rock
- Creation Date: Sun Dec 08 2019 12:02:27 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Dec 24 2019 21:18:06 GMT+0800 (Singapore Standard Time)

<p>
**Method 1: BFS**

**Update**
**Q & A**
*Q*: Could you please further explain about the logic of transferring the matrix to int?
`sum(cell << (i * n + j) for i, row in enumerate(mat) for j, cell in enumerate(row))`
I wonder how it works and why it works. 

*A*: For Input: mat = [[0,0],[0,1]], map it to `0b1000`, that is, mapping mat[i][j] to the `(i * n + j)`th bit of an int. specifically,
mat[0][0] = 0, corresponds to `0th` bit, which is `0`;
mat[0][1] = 0, corresponds to `1st` bit, which is `0`;
mat[1][0] = 0, corresponds to `2nd` bit, which is `0`;
mat[1][1] = 1, corresponds to `3rd` bit, which is `1`;

After mapping, **any operations on the binary cells of the `mat` are equals to the operations on the corresponding bits of the mapped int**. That\'s why it works.

*Q*: Why do you use "|" to initialize the matrix and use "^" to calculate the next?

*A*: 
1. When using `0 |= b`, where `b = 0 or 1`, the result is `b`; you can change it to
```
start  += mat[i][j] << (i * n + j); 
```
2. Use `next ^ 1 << k` (where k = i * n + j) to flip `kth` bit of `next`, which is equal to flipping the corresponding cell (i, j) in the matrix.

**end of Q & A**

----
1. Since `m < 3, n < 3` are given as constraints, there are at most `9` cells and an int has enough bits to store their values;
2. Map the `m * n` cells of the initial state of the matrix to the `0` ~ `m * n - 1`th bits of an int: `start`;
3. For each one of the `m * n` bits, flip it and its neighbors, then BFS to check if `0`, corresponding to an all `0`s matrix, is among the resulting states; if yes, return the minimum steps needed;
4. Use a Set to avoid duplicates;
5. If after the traversal of all states without finding `0`, return `-1`.

```java
    private static final int[] d = {0, 0, 1, 0, -1, 0};
    public int minFlips(int[][] mat) {
        int start = 0, m = mat.length, n = mat[0].length;
        for (int i = 0; i < m; ++i)
            for (int j = 0; j < n; ++j)
                start |= mat[i][j] << (i * n + j); // convert the matrix to an int.
        Queue<Integer> q = new LinkedList<>(Arrays.asList(start));
        Set<Integer> seen = new HashSet<>(q);
        for (int step = 0; !q.isEmpty(); ++step) {
            for (int sz = q.size(); sz > 0; --sz) {
                int cur = q.poll();
                if (cur == 0) // All 0s matrix found.
                    return step;
                for (int i = 0; i < m; ++i) { // traverse all m * n bits of cur.
                    for (int j = 0; j < n; ++j) {
                        int next = cur;
                        for (int k = 0; k < 5; ++k) { // flip the cell (i, j) and its neighbors.
                            int r = i + d[k], c = j + d[k + 1];
                            if (r >= 0 && r < m && c >= 0 && c < n)
                                next ^= 1 << (r * n + c);
                        }
                        if (seen.add(next)) // seen it before ?
                            q.offer(next); // no, put it into the Queue.
                    }
                }    
            }
        }
        return -1; // impossible to get all 0s matrix.
    }
```
```python
    def minFlips(self, mat: List[List[int]]) -> int:
        m, n = len(mat), len(mat[0])
        start = sum(cell << (i * n + j) for i, row in enumerate(mat) for j, cell in enumerate(row))
        dq = collections.deque([(start, 0)])
        seen = {start}
        while dq:
            cur, step = dq.popleft()
            if not cur:
                return step
            for i in range(m):
                for j in range(n):
                    next = cur
                    for r, c in (i, j), (i, j + 1), (i, j - 1), (i + 1, j), (i - 1, j):
                        if m > r >= 0 <= c < n:
                            next ^= 1 << (r * n + c)
                    if next not in seen:
                        seen.add(next)
                        dq.append((next, step + 1))
        return -1
```
**Analysis:**
Time: `O(m * n * 2 ^ (m * n))`, Space: `O(2 ^ (m * n))`.

----

**Method 2: DFS**
1. Use a Stack to store the status: current value and steps needed; Use a HashMap to update the status when encounering same value with less steps;
2. After exhaustion of all statuses, return the steps needed for `0`.

```java
    private static final int[] d = {0, 0, 1, 0, -1, 0};

    public int minFlips(int[][] mat) {
        int start = 0, m = mat.length, n = mat[0].length;
        for (int i = 0; i < m; ++i) 
            for (int j = 0; j < n; ++j) 
                start |= mat[i][j] << i * n + j;
        Deque<int[]> stk = new ArrayDeque<>();
        stk.push(new int[]{start, 0});
        Map<Integer, Integer> seenSteps = new HashMap<>();
        seenSteps.put(start, 0);
        while (!stk.isEmpty()) {
            int[] a = stk.pop();
            int cur = a[0], step = a[1];
            for (int i = 0; i < m; ++i) {
                for (int j = 0; j < n; ++j) {
                    int next = cur;
                    for (int k = 0; k < 5; ++k) {
                        int r = i + d[k], c = j + d[k + 1];
                        if (r >= 0 && r < m && c >= 0 && c < n) {
                            next ^= 1 << r * n + c;
                        }
                    }
                    if (seenSteps.getOrDefault(next, Integer.MAX_VALUE) > step + 1) {
                        seenSteps.put(next, step + 1);
                        stk.push(new int[]{next, step + 1});
                    }
                }
            }
        }
        return seenSteps.getOrDefault(0, -1);
    }
```
```python
    def minFlips(self, mat: List[List[int]]) -> int:
        m, n = len(mat), len(mat[0])
        start = sum(cell << i * n + j for i, row in enumerate(mat) for j, cell in enumerate(row))
        stack = [(start, 0)]
        seenSteps = {start : 0}
        while stack:
            cur, step = stack.pop()
            for i in range(m):
                for j in range(n):
                    next = cur
                    for r, c in (i, j), (i, j + 1), (i, j - 1), (i + 1, j), (i - 1, j):
                        if m > r >= 0 <= c < n:
                            next ^= 1 << r * n + c
                    if seenSteps.get(next, float(\'inf\')) > step + 1:
                        seenSteps[next] = step + 1
                        stack.append((next, step + 1))
        return seenSteps.get(0, -1)
```
**Analysis:**
Time: `O(m * n * 2 ^ (m * n))`, Space: `O(2 ^ (m * n))`.
</p>


### [Java] Recursion + Memoization Explained
- Author: manrajsingh007
- Creation Date: Sun Dec 08 2019 12:04:16 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Dec 13 2019 04:57:00 GMT+0800 (Singapore Standard Time)

<p>
##### Recursion + Memoization 
In this problem, we can think in a **brute manner first.**
We will try to flip the element at each index of the 2D matrix once and let recursion get the answer for the flipped array.
* Now while doing this we need to take care that we don\'t get trapped in a cycle (I have taken care of it by using a hashset).
* For eg consider the 2D matrix given below.
 [1 0]
 [1 0]
* Initially the call will be made by flipping the (0, 0) indexed element and its neighbors according to what is given in question.
* The 2D matrix changes to :-
 [0 1]
 [0 0]
* Now this configuration of the 2D array will make a recursive call by flipping the element at index (0, 0) and its neighbors. This will again give us an array configuration we had seen before in the same recursive branh (a cycle). 
 [1 0]
 [1 0]
* To avoid cycles i have used a set.
Since the constraints of the problem are very weak, so using a string to memoize the state would not cost much on the runtime though concatenating strings is expensive.
In this problem I have tried all possibilities of flipping all elements in a 2D array and made recursive calls to the resulting configurations. The recursive branch which brings the minimum no of steps will be a part of the solution. 
In the base case we check if all elements in the array are 0 or not.
(BFS is another way to come up with a solution in which you will be needing a visited array(to avoid cycle) and a queue).
```
class Solution {
    
    public static boolean check(int[][] mat, int n, int m){
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                if(mat[i][j] == 1) return false;
            }
        }
        return true;
    }
    
    public static void flip(int[][] mat, int n, int m, int i, int j){
        mat[i][j] = mat[i][j] ^ 1;
        if(i - 1 >= 0) mat[i - 1][j] = mat[i - 1][j] ^ 1;
        if(j - 1 >= 0) mat[i][j - 1] = mat[i][j - 1] ^ 1;
        if(i + 1 < n) mat[i + 1][j] = mat[i + 1][j] ^ 1;
        if(j + 1 < m) mat[i][j + 1] = mat[i][j + 1] ^ 1;
    }
    
    public static int func(int[][] mat, int n, int m, HashSet<String> set, HashMap<String, Integer> dp){
        if(check(mat, n, m)) return 0;
        String t = "";
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                t += Integer.toString(mat[i][j]);
            }
        }
        
        if(dp.containsKey(t)) return dp.get(t);
        if(set.contains(t)) return Integer.MAX_VALUE;
        
        set.add(t);
        int min = Integer.MAX_VALUE;
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                flip(mat, n, m, i, j);
                int small = func(mat, n, m, set, dp);
                if(small != Integer.MAX_VALUE) min = Math.min(min, 1 + small);
                flip(mat, n, m, i, j);
            }
        }
        set.remove(t);
        dp.put(t, min);
        return min;
    }
    
    public int minFlips(int[][] mat) {
        int n = mat.length, m = mat[0].length;
        HashMap<String, Integer> dp = new HashMap<>();
        int ans = func(mat, n, m, new HashSet<>(), dp);
        return ans == Integer.MAX_VALUE ? -1 : ans;
    }
}
</p>


