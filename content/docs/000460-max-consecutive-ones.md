---
title: "Max Consecutive Ones"
weight: 460
#id: "max-consecutive-ones"
---
## Description
<div class="description">
<p>Given a binary array, find the maximum number of consecutive 1s in this array.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> [1,1,0,1,1,1]
<b>Output:</b> 3
<b>Explanation:</b> The first two digits or the last three digits are consecutive 1s.
    The maximum number of consecutive 1s is 3.
</pre>
</p>

<p><b>Note:</b>
<ul>
<li>The input array will only contain <code>0</code> and <code>1</code>.</li>
<li>The length of input array is a positive integer and will not exceed 10,000</li>
</ul>
</p>
</div>

## Tags
- Array (array)

## Companies
- Google - 2 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

The constraints for this problem make it easy  
to understand that it can be done in one iteration.

> The length of input array is a positive integer and will not exceed 10,000

How else do you expect to find the number of 1's in an array without making at least one pass through the array. So let's look at the solution.

#### Approach: One pass

**Intuition**

The intuition is pretty simple. We keep a count of the number of 1's encountered. And reset the count whenever we encounter anything other than 1 (which is 0 for this problem). Thus, maintaining count of 1's between zeros or rather maintaining counts of contiguous 1's. It's the same as keeping a track of the number of hours of sleep you had, without waking up in between.

**Algorithm**

1. Maintain a counter for the number of `1`'s.

2. Increment the counter by 1, whenever you encounter a `1`.

3. Whenever you encounter a `0`

    a. Use the current count of `1`'s to find the maximum contiguous `1`'s till now.

    b. Afterwards, reset the counter for `1`'s to 0.

4. Return the maximum in the end.

  <center>
  <img src="../Figures/485/485_Max_Consecutive_Ones_1.png" width="500"/>
  </center>
  <br>

  In the above diagram we found out that the maximum number of consecutive `1`'s is 3. There were two breaks in the count we encountered while iterating the array. Every time the break i.e. `0` was encountered we had to reset the count of `1` to zero.

  >Note - The maximum count is only calculated when we encounter a break i.e. 0, since thats where a subarray of 1's ends.

<iframe src="https://leetcode.com/playground/u9ZwNcgd/shared" frameBorder="0" width="100%" height="361" name="u9ZwNcgd"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of elements in the array.

* Space Complexity: $$O(1)$$. We do not use any extra space.

#### Follow up:

You can also try something fancy one liner [solution](https://leetcode.com/articles/duplicate-zeros/) as shared by [Stefan Pochmann](https://leetcode.com/stefanpochmann/).

```python
def findMaxConsecutiveOnes(self, nums):
  return max(map(len, ''.join(map(str, nums)).split('0')))
```

Note, how he converts the list into a string, using `map` and `join` functions. Then, splits the resultant string on `0`. The maximum of the lengths of these list of strings of `1`'s is the answer we are looking for.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 4 lines concise solution with explanation
- Author: yuxiangmusic
- Creation Date: Sun Jan 15 2017 12:28:50 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 03 2018 06:09:26 GMT+0800 (Singapore Standard Time)

<p>

```
    public int findMaxConsecutiveOnes(int[] nums) {
        int maxHere = 0, max = 0;
        for (int n : nums)
            max = Math.max(max, maxHere = n == 0 ? 0 : maxHere + 1);
        return max; 
    } 
```

The idea is to reset ```maxHere``` to 0 if we see 0, otherwise increase ```maxHere``` by 1
The max of all ```maxHere``` is the solution

```
110111
^ maxHere = 1

110111
.^ maxHere = 2

110111
..^ maxHere = 0

110111
...^ maxHere = 1

110111
....^ maxHere = 2

110111
.....^ maxHere = 3
```

We can also solve this problem by setting ```k = 0``` of [Max Consecutive Ones II](https://discuss.leetcode.com/topic/75445/java-clean-solution-easily-extensible-to-flipping-k-zero-and-follow-up-handled)
</p>


### Easy Java Solution
- Author: shawngao
- Creation Date: Sun Jan 15 2017 12:10:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 15 2018 08:07:59 GMT+0800 (Singapore Standard Time)

<p>
This is a really easy problem. No explanation :)
```
public class Solution {
    public int findMaxConsecutiveOnes(int[] nums) {
        int result = 0;
        int count = 0;
        
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 1) {
        	count++;
        	result = Math.max(count, result);
            }
            else count = 0;
        }
        
        return result;
    }
}
```
</p>


### Simple Python
- Author: Ipeq1
- Creation Date: Sun Jan 15 2017 12:03:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 15 2017 12:03:59 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def findMaxConsecutiveOnes(self, nums):
        cnt = 0
        ans = 0
        for num in nums:
            if num == 1:
                cnt += 1
                ans = max(ans, cnt)
            else:
                cnt = 0
        return ans
```
</p>


