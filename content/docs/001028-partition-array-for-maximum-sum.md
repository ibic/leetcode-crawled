---
title: "Partition Array for Maximum Sum"
weight: 1028
#id: "partition-array-for-maximum-sum"
---
## Description
<div class="description">
<p>Given an integer array <code>arr</code>, you should partition the array into (contiguous) subarrays of length at most <code>k</code>. After partitioning, each subarray has their values changed to become the maximum value of that subarray.</p>

<p>Return <em>the largest sum of the given array after partitioning.</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,15,7,9,2,5,10], k = 3
<strong>Output:</strong> 84
<strong>Explanation:</strong> arr becomes [15,15,15,9,10,10,10]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,4,1,5,7,3,6,1,9,9,3], k = 4
<strong>Output:</strong> 83
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> arr = [1], k = 1
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= arr.length &lt;= 500</code></li>
	<li><code>0 &lt;= arr[i] &lt;= 10<sup>9</sup></code></li>
	<li><code>1 &lt;= k &lt;= arr.length</code></li>
</ul>

</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] DP
- Author: lee215
- Creation Date: Sun May 12 2019 12:04:18 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jun 03 2019 16:18:43 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**

`dp[i]` record the maximum sum we can get considering `A[0] ~ A[i]`
To get `dp[i]`, we will try to change `k` last numbers separately to the maximum of them,
where `k = 1` to `k = K`.

## **Complexity**
Time `O(NK)`, Space `O(N)`

<br>

**Java:**
```
    public int maxSumAfterPartitioning(int[] A, int K) {
        int N = A.length, dp[] = new int[N];
        for (int i = 0; i < N; ++i) {
            int curMax = 0;
            for (int k = 1; k <= K && i - k + 1 >= 0; ++k) {
                curMax = Math.max(curMax, A[i - k + 1]);
                dp[i] = Math.max(dp[i], (i >= k ? dp[i - k] : 0) + curMax * k);
            }
        }
        return dp[N - 1];
    }
```

**C++:**
```
    int maxSumAfterPartitioning(vector<int>& A, int K) {
        int N = A.size();
        vector<int> dp(N);
        for (int i = 0; i < N; ++i) {
            int curMax = 0;
            for (int k = 1; k <= K && i - k + 1 >= 0; ++k) {
                curMax = max(curMax, A[i - k + 1]);
                dp[i] = max(dp[i], (i >= k ? dp[i - k] : 0) + curMax * k);
            }
        }
        return dp[N - 1];
    }
```

**Python:**
```
    def maxSumAfterPartitioning(self, A, K):
        N = len(A)
        dp = [0] * (N + 1)
        for i in xrange(N):
            curMax = 0
            for k in xrange(1, min(K, i + 1) + 1):
                curMax = max(curMax, A[i - k + 1])
                dp[i] = max(dp[i], dp[i - k] + curMax * k)
        return dp[N - 1]
```

</p>


### Shouldn't this problem come under DP instead of Graph?
- Author: girishiitj
- Creation Date: Tue Jun 25 2019 21:40:14 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 25 2019 21:40:14 GMT+0800 (Singapore Standard Time)

<p>
Shouldn\'t this problem come under DP instead of Graph?
</p>


### Java - visualize the pattern
- Author: tianchi-seattle
- Creation Date: Sun May 12 2019 16:51:06 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 12 2019 17:17:55 GMT+0800 (Singapore Standard Time)

<p>
Code is same as others, but I found a way to visualize the pattern. Alfter all, the goal is to derive the rule.

```java
class Solution {
    // Let k be 2
    // Focus on "growth" of the pattern
    // Define A\' to be a partition over A that gives max sum
    
    // #0
    // A = {1}
    // A\'= {1} => 1
    
    // #1
    // A = {1, 2}
    // A\'= {1}{2} => 1 + 2 => 3 X
    // A\'= {1, 2} => {2, 2} => 4 AC
        
    // #2
    // A = {1, 2, 9}
    // A\'= {1, 2}{9} => {2, 2}{9} => 4 + 9 => 13 X
    // A\'= {1}{2, 9} => {1}{9, 9} => 1 + 18 => 19 AC
    
    // #3
    // A = {1, 2, 9, 30}
    // A\'= {1}{2, 9}{30} => {1}{9, 9}{30} => 19 + 30 => 49 X
    // A\'= {1, 2}{9, 30} => {2, 2}{30, 30} => 4 + 60 => 64 AC
    
    // Now, label each instance. Use F1() to represent how A is partitioned and use F2() to represent
    // the AC value of that partition. F2() is the dp relation we are looking for.
    
    // #4
    // A = {1, 2, 9, 30, 5}
    // A\'= F1(#3){5} => F2(#3) + 5 => 69 X
    // A\'= F1(#2){30, 5} => F2(#2) + 30 + 30 => 79 AC
    // => F2(#4) = 79
    
    public int maxSumAfterPartitioning(int[] A, int K) {
        int N = A.length, dp[] = new int[N];
        for (int i = 0; i < N; ++i) {
            int curMax = 0;
            for (int k = 1; k <= K && i - k + 1 >= 0; ++k) {
                curMax = Math.max(curMax, A[i - k + 1]);
                dp[i] = Math.max(dp[i], (i >= k ? dp[i - k] : 0) + curMax * k);
            }
        }
        return dp[N - 1];
    }
}
```
</p>


