---
title: "Not Boring Movies"
weight: 1509
#id: "not-boring-movies"
---
## Description
<div class="description">
X city opened a new cinema, many people would like to go to this cinema. The cinema also gives out a poster indicating the movies&rsquo; ratings and descriptions.
<p>Please write a SQL query to output movies with an odd numbered ID and a description that is not &#39;boring&#39;. Order the result by rating.</p>

<p>&nbsp;</p>

<p>For example, table <code>cinema</code>:</p>

<pre>
+---------+-----------+--------------+-----------+
|   id    | movie     |  description |  rating   |
+---------+-----------+--------------+-----------+
|   1     | War       |   great 3D   |   8.9     |
|   2     | Science   |   fiction    |   8.5     |
|   3     | irish     |   boring     |   6.2     |
|   4     | Ice song  |   Fantacy    |   8.6     |
|   5     | House card|   Interesting|   9.1     |
+---------+-----------+--------------+-----------+
</pre>
For the example above, the output should be:

<pre>
+---------+-----------+--------------+-----------+
|   id    | movie     |  description |  rating   |
+---------+-----------+--------------+-----------+
|   5     | House card|   Interesting|   9.1     |
|   1     | War       |   great 3D   |   8.9     |
+---------+-----------+--------------+-----------+
</pre>

<p>&nbsp;</p>

</div>

## Tags


## Companies


## Official Solution
[TOC]

## Solution
---
#### Approach: Using `MOD()` function [Accepted]

**Algorithm**

We can use the `mod(id,2)=1` to determine the odd id, and then add a `description != 'boring'` should address this problem.

**MySQL**

```sql
select *
from cinema
where mod(id, 2) = 1 and description != 'boring'
order by rating DESC
;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My solution
- Author: angelachang27
- Creation Date: Wed Jun 14 2017 09:28:02 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 21:23:57 GMT+0800 (Singapore Standard Time)

<p>
```SELECT * FROM cinema WHERE (id % 2 = 1) AND (description <> 'boring') ORDER BY rating DESC```
</p>


### Answer should be NOT LIKE '%boring%' rather than != 'boring'
- Author: Nommm
- Creation Date: Thu Apr 11 2019 12:18:07 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Apr 11 2019 12:18:07 GMT+0800 (Singapore Standard Time)

<p>
The description field seems to be free text that enables the user to type any answer they desire.

With that being said, a user could type \'Very boring\' or \'I found it boring\', etc. The accepted answer would not pick up on this because it does a direct comparison.
</p>


### Faster than 90% by Bit operation
- Author: bm521
- Creation Date: Thu Dec 05 2019 17:00:07 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Dec 05 2019 17:00:07 GMT+0800 (Singapore Standard Time)

<p>
```
select * from cinema where description <> \'boring\' and id & 1 = 1 order by rating DESC
```


</p>


