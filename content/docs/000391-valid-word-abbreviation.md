---
title: "Valid Word Abbreviation"
weight: 391
#id: "valid-word-abbreviation"
---
## Description
<div class="description">
<p>
Given a <b>non-empty</b> string <code>s</code> and an abbreviation <code>abbr</code>, return whether the string matches with the given abbreviation.
</p>

<p>A string such as <code>"word"</code> contains only the following valid abbreviations:</p>

<pre>["word", "1ord", "w1rd", "wo1d", "wor1", "2rd", "w2d", "wo2", "1o1d", "1or1", "w1r1", "1o2", "2r1", "3d", "w3", "4"]
</pre>

<p>Notice that only the above abbreviations are valid abbreviations of the string <code>"word"</code>. Any other string is not a valid abbreviation of <code>"word"</code>.</p>

<p><b>Note:</b><br />
Assume <code>s</code> contains only lowercase letters and <code>abbr</code> contains only lowercase letters and digits.
</p>

<p><b>Example 1:</b><br />
<pre>
Given <b>s</b> = "internationalization", <b>abbr</b> = "i12iz4n":

Return true.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
Given <b>s</b> = "apple", <b>abbr</b> = "a2e":

Return false.
</pre>
</p>
</div>

## Tags
- String (string)

## Companies
- Facebook - 4 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Short and easy to understand Java Solution
- Author: lzmshiwo
- Creation Date: Mon Oct 03 2016 00:07:34 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Aug 28 2018 12:27:36 GMT+0800 (Singapore Standard Time)

<p>
```
    public boolean validWordAbbreviation(String word, String abbr) {
        int i = 0, j = 0;
        while (i < word.length() && j < abbr.length()) {
            if (word.charAt(i) == abbr.charAt(j)) {
                ++i;++j;
                continue;
            }
            if (abbr.charAt(j) <= '0' || abbr.charAt(j) > '9') {
                return false;
            }
            int start = j;
            while (j < abbr.length() && abbr.charAt(j) >= '0' && abbr.charAt(j) <= '9') {
                ++j;
            }
            int num = Integer.valueOf(abbr.substring(start, j));
            i += num;
        }
        return i == word.length() && j == abbr.length();
    }

```
</p>


### Simple Regex One-liner (Java, Python)
- Author: StefanPochmann
- Creation Date: Mon Oct 03 2016 00:49:08 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 10:47:21 GMT+0800 (Singapore Standard Time)

<p>
## Update

Much nicer, I just turn an abbreviation like `"i12iz4n"` into a regular expression like `"i.{12}iz.{4}n"`. Duh.

Java:

    public boolean validWordAbbreviation(String word, String abbr) {
        return word.matches(abbr.replaceAll("[1-9]\\d*", ".{$0}"));
    }

Python:

    def validWordAbbreviation(self, word, abbr):
        return bool(re.match(re.sub('([1-9]\d*)', r'.{\1}', abbr) + '$', word))

<br>

---

## Obsolete original

(This now gets a memory error, since the exploding testcase I suggested at the end has been added to the test suite.)

"Clean":

    def validWordAbbreviation(self, word, abbr):
        regex = re.sub('\d+', lambda m: '.' * int(m.group()), abbr)
        return bool(re.match(regex + '$', word)) and not re.search('(^|\D)0', abbr)

"Dirty" (abusing how Python handles the `>` there):

    def validWordAbbreviation(self, word, abbr):
        regex = re.sub('\d+', lambda m: '.' * int(m.group()), abbr)
        return re.match(regex + '$', word) > re.search('(^|\D)0', abbr)

I turn each number into that many dots to get a regular expression. For example, when asked whether `"3t2de"` is a valid abbreviation for word `"leetcode"`, I turn `"3t2de"` into `"...t..de"` and check whether that regular expression matches `"leetcode"`, which it does. I also need to rule out the number `"0"` and leading zeros, which I do with another regular expression.

@1337c0d3r I suggest adding test case `"bignumberhahaha", "999999999"`, as that gets me a fully deserved MemoryError :-)
</p>


### Concise C++ Solution
- Author: zyoppy008
- Creation Date: Mon Oct 03 2016 07:53:14 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 03 2016 07:53:14 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:
    bool validWordAbbreviation(string word, string abbr) {
        int i = 0, j = 0;
        while (i < word.size() && j < abbr.size()) {
            if (isdigit(abbr[j])) {
                if (abbr[j] == '0') return false;
                int val = 0;
                while (j < abbr.size() && isdigit(abbr[j])) 
                    val = val * 10 + abbr[j++] - '0';
                i += val;
            }
            else if (word[i++] != abbr[j++]) return false;
        }
        return i == word.size() && j == abbr.size();
    }
};
```
</p>


