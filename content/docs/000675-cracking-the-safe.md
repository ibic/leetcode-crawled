---
title: "Cracking the Safe"
weight: 675
#id: "cracking-the-safe"
---
## Description
<div class="description">
<p>There is a box protected by a password. The password is a sequence of&nbsp;<code>n</code> digits&nbsp;where each digit can be one of the first <code>k</code> digits <code>0, 1, ..., k-1</code>.</p>

<p>While entering a password,&nbsp;the last <code>n</code> digits entered will automatically be matched against the correct password.</p>

<p>For example, assuming the correct password is <code>&quot;345&quot;</code>,&nbsp;if you type <code>&quot;012345&quot;</code>, the box will open because the correct password matches the suffix of the entered password.</p>

<p>Return any password of <strong>minimum length</strong> that is guaranteed to open the box at some point of entering it.</p>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> n = 1, k = 2
<b>Output:</b> &quot;01&quot;
<b>Note:</b> &quot;10&quot; will be accepted too.
</pre>

<p><b>Example 2:</b></p>

<pre>
<b>Input:</b> n = 2, k = 2
<b>Output:</b> &quot;00110&quot;
<b>Note:</b> &quot;01100&quot;, &quot;10011&quot;, &quot;11001&quot; will be accepted too.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ol>
	<li><code>n</code> will be in the range <code>[1, 4]</code>.</li>
	<li><code>k</code> will be in the range <code>[1, 10]</code>.</li>
	<li><code>k^n</code> will be at most <code>4096</code>.</li>
</ol>

<p>&nbsp;</p>

</div>

## Tags
- Math (math)
- Depth-first Search (depth-first-search)

## Companies
- Google - 5 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Hierholzer's Algorithm [Accepted]

**Intuition**

We can think of this problem as the problem of finding an Euler path (a path visiting every edge exactly once) on the following graph: there are $$k^{n-1}$$ nodes with each node having $$k$$ edges.

For example, when `k = 4, n = 3`, the nodes are `'00', '01', '02', ..., '32', '33'` and each node has 4 edges `'0', '1', '2', '3'`.  A node plus edge represents a *complete edge* and viewing that substring in our answer.

Any connected directed graph where all nodes have equal in-degree and out-degree has an Euler circuit (an Euler path ending where it started.)  Because our graph is highly connected and symmetric, we should expect intuitively that taking any path greedily in some order will probably result in an Euler path.  

This intuition is called Hierholzer's algorithm: whenever there is an Euler cycle, we can construct it greedily.  The algorithm goes as follows:

* Starting from a vertex `u`, we walk through (unwalked) edges until we get stuck.  Because the in-degrees and out-degrees of each node are equal, we can only get stuck at `u`, which forms a cycle.

* Now, for any node `v` we had visited that has unwalked edges, we start a new cycle from `v` with the same procedure as above, and then merge the cycles together to form a new cycle $$u \rightarrow \dots \rightarrow v \rightarrow \dots \rightarrow v \rightarrow \dots \rightarrow u$$.


**Algorithm**

We will modify our standard depth-first search: instead of keeping track of nodes, we keep track of (complete) edges: `seen` records if an edge has been visited.

Also, we'll need to visit in a sort of "post-order", recording the answer after visiting the edge.  This is to prevent getting stuck.  For example, with `k = 2, n = 2`, we have the nodes `'0', '1'`.  If we greedily visit complete edges `'00', '01', '10'`, we will be stuck at the node `'0'` prematurely.  However, if we visit in post-order, we'll end up visiting `'00', '01', '11', '10'` correctly.

In general, during our Hierholzer walk, we will record the results of other subcycles first, before recording the main cycle we started from, just as in our first description of the algorithm.  Technically, we are recording backwards, as we exit the nodes.

For example, we will walk (in the "original cycle") until we get stuck, then record the node as we exit.  (Every edge walked is always marked immediately so that it can no longer be used.)  Then in the penultimate node of our original cycle, we will do a Hierholzer walk and then record this node; then in the third-last node of our original cycle we will do a Hierholzer walk and then record this node, and so on.


<iframe src="https://leetcode.com/playground/si5Kc26P/shared" frameBorder="0" width="100%" height="500" name="si5Kc26P"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(n * k^n)$$.  We visit every edge once in our depth-first search, and nodes take $$O(n)$$ space.

* Space Complexity: $$O(n * k^n)$$, the size of `seen`.

---
#### Approach #2: Inverse Burrows-Wheeler Transform [Accepted]

**Explanation**

If we are familiar with the theory of combinatorics on words, recall that a *Lyndon Word* `L` is a word that is the unique minimum of it's rotations.

One important mathematical result (due to [Fredericksen and Maiorana](http://www-igm.univ-mlv.fr/~perrin/Recherche/Publications/Articles/debruijnRevised3.pdf)), is that the concatenation in lexicographic order of Lyndon words with length dividing `n`, forms a *de Bruijin* sequence: a sequence where every every word (from the $$k^n$$ available) appears as a substring of length `n` (where we are allowed to wrap around.)

For example, when `n = 6, k = 2`, all the Lyndon words with length dividing `n` in lexicographic order are (spaces for convenience):
`0 000001 000011 000101 000111 001 001011 001101 001111 01
010111 011 011111 1`.  It turns out this is the smallest de Bruijin sequence.

We can use the *Inverse Burrows-Wheeler Transform* (IBWT) to generate these Lyndon words.  Consider two sequences: `S` is the alphabet repeated $$k^{n-1}$$ times: `S = 0123...0123...0123....`, and `S'` is the alphabet repeated $$k^{n-1}$$ times for each letter: `S' = 00...0011...1122....`  We can think of `S'` and `S` as defining a permutation, where the `j`-th occurrence of each letter of the alphabet in `S'` maps to the corresponding `j`-th occurrence in `S`.  The cycles of this permutation turn out to be the corresponding smallest de Bruijin sequence ([link](http://www.macs.hw.ac.uk/~markl/Higgins.pdf)).

Under this view, the permutation $$S' \rightarrow S$$ [mapping permutation indices $$(i * k^{n-1} + q) \rightarrow (q * k + i)$$] form the desired Lyndon words.

<iframe src="https://leetcode.com/playground/nNRB5bTb/shared" frameBorder="0" width="100%" height="463" name="nNRB5bTb"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(k^n)$$.  We loop through every possible substring.

* Space Complexity: $$O(k^n)$$, the size of `P` and `ans`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### DFS with Explanations
- Author: GraceMeng
- Creation Date: Tue Jul 24 2018 07:16:25 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 13:31:59 GMT+0800 (Singapore Standard Time)

<p>
In order to guarantee to open the box at last, the input password ought to contain all length-n combinations on digits [0..k-1] - there should be `k^n` combinations in total. 

To make the input password as short as possible, we\'d better make each possible length-n combination on digits [0..k-1] occurs **exactly once** as a substring of the password. The existence of such a password is proved by [De Bruijn sequence](https://en.wikipedia.org/wiki/De_Bruijn_sequence#Uses):

*A de Bruijn sequence of order n on a size-k alphabet A is a cyclic sequence in which every possible length-n string on A occurs exactly once as a substring. It has length k^n, which is also the number of distinct substrings of length n on a size-k alphabet; de Bruijn sequences are therefore optimally short.*

We reuse last n-1 digits of the input-so-far password as below:
```
e.g., n = 2, k = 2
all 2-length combinations on [0, 1]: 
00 (`00`110), 
 01 (0`01`10), 
  11 (00`11`0), 
   10 (001`10`)
   
the password is 00110
```

We can utilize **DFS** to find the password:
> **goal**: to find the shortest input password such that each possible n-length combination of digits [0..k-1] occurs exactly once as a substring.

>**node**: current input password

>**edge**: if the last n - 1 digits of `node1` can be transformed to `node2` by appending a digit from `0..k-1`, there will be an edge between `node1` and `node2` 

>**start node**: n repeated 0\'s
**end node**: all n-length combinations among digits 0..k-1 are visited

> **visitedComb**: all combinations that have been visited
****
```
    public String crackSafe(int n, int k) {
        // Initialize pwd to n repeated 0\'s as the start node of DFS.
        String strPwd = String.join("", Collections.nCopies(n, "0"));
        StringBuilder sbPwd = new StringBuilder(strPwd);
        
        Set<String> visitedComb = new HashSet<>();
        visitedComb.add(strPwd);
    
        int targetNumVisited = (int) Math.pow(k, n);
        
        crackSafeAfter(sbPwd, visitedComb, targetNumVisited, n, k);
        
        return sbPwd.toString();
    }
    
    private boolean crackSafeAfter(StringBuilder pwd, Set<String> visitedComb, int targetNumVisited, int n, int k) {
        // Base case: all n-length combinations among digits 0..k-1 are visited. 
        if (visitedComb.size() == targetNumVisited) {
            return true;
        }
        
        String lastDigits = pwd.substring(pwd.length() - n + 1); // Last n-1 digits of pwd.
        for (char ch = \'0\'; ch < \'0\' + k; ch++) {
            String newComb = lastDigits + ch;
            if (!visitedComb.contains(newComb))  {
                visitedComb.add(newComb);
                pwd.append(ch);
                if (crackSafeAfter(pwd, visitedComb, targetNumVisited, n, k)) {
                    return true;
                }
                visitedComb.remove(newComb);
                pwd.deleteCharAt(pwd.length() - 1);
            }
        }
        
        return false;
    }
```

**(\u4EBA \u2022\u0348\u1D17\u2022\u0348)** Thanks for voting!
</p>


### Having trouble understanding it? Try this.
- Author: harsh700ca
- Creation Date: Mon Dec 25 2017 09:40:53 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 22:43:39 GMT+0800 (Singapore Standard Time)

<p>
https://www.youtube.com/watch?v=iPLQgXUiU14

This video helped a lot, combined with spending some time to reprove it on my own, in my own words!
</p>


### Easy DFS
- Author: 1moretry
- Creation Date: Sun Dec 24 2017 12:15:18 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 15 2018 23:41:27 GMT+0800 (Singapore Standard Time)

<p>
This is kinda greedy approach.
So straight up we can tell that we have k^n combinations of the lock.
So the best way to generate the string is reusing last n-1 digits of previous lock << I can't really prove this but I realized this after writing down some examples.

Hence, we'll use dfs to generate the string with goal is when our string contains all possible combinations.

```java
class Solution {
    public String crackSafe(int n, int k) {
        StringBuilder sb = new StringBuilder();
        int total = (int) (Math.pow(k, n));
        for (int i = 0; i < n; i++) sb.append('0');

        Set<String> visited = new HashSet<>();
        visited.add(sb.toString());

        dfs(sb, total, visited, n, k);

        return sb.toString();
    }

    private boolean dfs(StringBuilder sb, int goal, Set<String> visited, int n, int k) {
        if (visited.size() == goal) return true;
        String prev = sb.substring(sb.length() - n + 1, sb.length());
        for (int i = 0; i < k; i++) {
            String next = prev + i;
            if (!visited.contains(next)) {
                visited.add(next);
                sb.append(i);
                if (dfs(sb, goal, visited, n, k)) return true;
                else {
                    visited.remove(next);
                    sb.delete(sb.length() - 1, sb.length());
                }
            }
        }
        return false;
    }

}
```
</p>


