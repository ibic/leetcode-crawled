---
title: "Unpopular Books"
weight: 1528
#id: "unpopular-books"
---
## Description
<div class="description">
<p>Table:&nbsp;<code>Books</code></p>

<pre>
+----------------+---------+
| Column Name    | Type    |
+----------------+---------+
| book_id        | int     |
| name           | varchar |
| available_from | date    |
+----------------+---------+
book_id is the primary key of this table.
</pre>

<p>Table:&nbsp;<code>Orders</code></p>

<pre>
+----------------+---------+
| Column Name    | Type    |
+----------------+---------+
| order_id       | int     |
| book_id        | int     |
| quantity       | int     |
| dispatch_date  | date    |
+----------------+---------+
order_id is the primary key of this table.
book_id is a foreign key to the Books table.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query that reports the&nbsp;<strong>books</strong> that have sold <strong>less than 10</strong> copies in the last year, excluding books that have been available for less than 1 month from today. <strong>Assume today is 2019-06-23</strong>.</p>

<p>The query result format is in the following example:</p>

<pre>
Books table:
+---------+--------------------+----------------+
| book_id | name               | available_from |
+---------+--------------------+----------------+
| 1       | &quot;Kalila And Demna&quot; | 2010-01-01     |
| 2       | &quot;28 Letters&quot;       | 2012-05-12     |
| 3       | &quot;The Hobbit&quot;       | 2019-06-10     |
| 4       | &quot;13 Reasons Why&quot;   | 2019-06-01     |
| 5       | &quot;The Hunger Games&quot; | 2008-09-21     |
+---------+--------------------+----------------+

Orders table:
+----------+---------+----------+---------------+
| order_id | book_id | quantity | dispatch_date |
+----------+---------+----------+---------------+
| 1        | 1       | 2        | 2018-07-26    |
| 2        | 1       | 1        | 2018-11-05    |
| 3        | 3       | 8        | 2019-06-11    |
| 4        | 4       | 6        | 2019-06-05    |
| 5        | 4       | 5        | 2019-06-20    |
| 6        | 5       | 9        | 2009-02-02    |
| 7        | 5       | 8        | 2010-04-13    |
+----------+---------+----------+---------------+

Result table:
+-----------+--------------------+
| book_id   | name               |
+-----------+--------------------+
| 1         | &quot;Kalila And Demna&quot; |
| 2         | &quot;28 Letters&quot;       |
| 5         | &quot;The Hunger Games&quot; |
+-----------+--------------------+
</pre>

</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Three MySQL solutions with explanations
- Author: olivia612
- Creation Date: Mon Aug 19 2019 03:09:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 19 2019 03:09:27 GMT+0800 (Singapore Standard Time)

<p>
The idea of this first solution is to run the calculation first in the subquery to give us the books that have been sold in the last year, and then use a left join to give us the books that was abailable after 2019-05-23 and filter the books using multiple condition in the where clause to give us the books that have been sold less than 10 copies.
```
select b.book_id, b.name
from books b left join
    (select book_id, sum(quantity) as book_sold
    from Orders 
    where dispatch_date between \'2018-06-23\' and \'2019-06-23\'
    group by book_id) t
on b.book_id = t.book_id
where available_from < \'2019-05-23\'
and (book_sold is null or book_sold <10)
order by b.book_id;
```

The logic of the second solution is to first filter the two tables first and then run the calculation based on those two subqueries. The first subquery is to filter out the books that was available for more than one month, and the second subquery was to filter out the books that was sold during the last year. After joined the two \'tables\', used a HAVING clause to find out the books that have been  sold less than 10 copies during the last year. 
```
select b.book_id, b.name from
(select * from books where available_from < \'2019-05-23\') b
left join
(select * from Orders where dispatch_date > \'2018-06-23\') o
on b.book_id = o.book_id
group by b.book_id, b.name
having sum(o.quantity) is null or sum(o.quantity) <10;
```

The third solution has similiar logic with the second one. The major difference is that this one joined the tables first and then run the filter later to give us the information that we need. 
```
select b.book_id, b.name
from books b left join orders o
on b.book_id = o.book_id and dispatch_date between \'2018-06-23\' and \'2019-06-23\'
where datediff(\'2019-06-23\', available_from) > 30
group by b.book_id, b.name
having ifnull(sum(quantity),0) <10;
```
</p>


### 核心：NOT IN
- Author: kevinwangglobal
- Creation Date: Tue Feb 25 2020 05:32:39 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Mar 16 2020 04:24:08 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT book_id, 
       name
FROM Books
WHERE available_from < \'2019-05-23\'
AND book_id NOT IN
            (SELECT book_id
             FROM Orders
             WHERE dispatch_date BETWEEN \'2018-06-23\' AND \'2019-06-23\'
             GROUP BY book_id
             Having sum(quantity) >= 10) 
 ```
 
#some books available_from early than \'2019-05-23\', but no sales  during \'2018-06-23\' and \'2019-06-23\', so the Group BY cannot reflect those book with no sales, however, they are book that have less than 10 copies sold. So, we have to use NOT IN (those books sold more than 10 copies), instead of IN (those books sold less than 10 copies (0 copy books not included))

</p>


### MySQL - Filter first and Join later for better performance.
- Author: rkonda
- Creation Date: Tue Jul 02 2019 06:37:37 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 02 2019 06:37:37 GMT+0800 (Singapore Standard Time)

<p>
This solution is easy to visualize and readable. Filter first and join later will perform better if the data set is huge is required run in distributed environment.
```
select 
b.book_id,
b.name
from 
(select * from books where available_from <= "2019-05-23") b 
left join (select * from orders where dispatch_date >= "2018-06-23") o
on b.book_id=o.book_id 
group by b.book_id,b.name
having sum(o.quantity) is null or sum(quantity)<10
</p>


