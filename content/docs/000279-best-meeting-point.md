---
title: "Best Meeting Point"
weight: 279
#id: "best-meeting-point"
---
## Description
<div class="description">
<p>A group of two or more people wants to meet and minimize the total travel distance. You are given a 2D grid of values 0 or 1, where each 1 marks the home of someone in the group. The distance is calculated using <a href="http://en.wikipedia.org/wiki/Taxicab_geometry" target="_blank">Manhattan Distance</a>, where distance(p1, p2) = <code>|p2.x - p1.x| + |p2.y - p1.y|</code>.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> 

1 - 0 - 0 - 0 - 1
|   |   |   |   |
0 - 0 - 0 - 0 - 0
|   |   |   |   |
0 - 0 - 1 - 0 - 0

<strong>Output: 6 

Explanation: </strong>Given three people living at <code>(0,0)</code>, <code>(0,4)</code>, and <code>(2,2)</code>:
&nbsp;            The point <code>(0,2)</code> is an ideal meeting point, as the total travel distance 
&nbsp;            of 2+2+2=6 is minimal. So return 6.</pre>

</div>

## Tags
- Math (math)
- Sort (sort)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Snapchat - 4 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 (Breadth-first Search) [Time Limit Exceeded]

A brute force approach is to evaluate all possible meeting points in the grid. We could apply breadth-first search originating from each of the point.

While inserting a point into the queue, we need to record the distance of that point from the meeting point.  Also, we need an extra `visited` table to record which point had already been visited to avoid being inserted into the queue again.

```java
public int minTotalDistance(int[][] grid) {
    int minDistance = Integer.MAX_VALUE;
    for (int row = 0; row < grid.length; row++) {
        for (int col = 0; col < grid[0].length; col++) {
            int distance = search(grid, row, col);
            minDistance = Math.min(distance, minDistance);
        }
    }
    return minDistance;
}

private int search(int[][] grid, int row, int col) {
    Queue<Point> q = new LinkedList<>();
    int m = grid.length;
    int n = grid[0].length;
    boolean[][] visited = new boolean[m][n];
    q.add(new Point(row, col, 0));
    int totalDistance = 0;
    while (!q.isEmpty()) {
        Point point = q.poll();
        int r = point.row;
        int c = point.col;
        int d = point.distance;
        if (r < 0 || c < 0 || r >= m || c >= n || visited[r][c]) {
            continue;
        }
        if (grid[r][c] == 1) {
            totalDistance += d;
        }
        visited[r][c] = true;
        q.add(new Point(r + 1, c, d + 1));
        q.add(new Point(r - 1, c, d + 1));
        q.add(new Point(r, c + 1, d + 1));
        q.add(new Point(r, c - 1, d + 1));
    }
    return totalDistance;
}

public class Point {
    int row;
    int col;
    int distance;
    public Point(int row, int col, int distance) {
        this.row = row;
        this.col = col;
        this.distance = distance;
    }
}
```

**Complexity analysis**

* Time complexity : $$O(m^2n^2)$$.
For each point in the $$m \times n$$ size grid, the breadth-first search takes at most $$m \times n$$ steps to reach all points. Therefore the time complexity is $$O(m^2n^2)$$.

* Space complexity : $$O(mn)$$.
The `visited` table consists of $$m \times n$$ elements map to each point in the grid. We insert at most $$m \times n$$ points into the queue.

---
#### Approach #2 (Manhattan Distance Formula) [Time Limit Exceeded]

You may notice that breadth-first search is unnecessary. You can just calculate the Manhattan distance using the formula:

$$
distance(p1, p2) = \left | p2.x - p1.x \right | + \left | p2.y - p1.y \right |
$$

```java
public int minTotalDistance(int[][] grid) {
    List<Point> points = getAllPoints(grid);
    int minDistance = Integer.MAX_VALUE;
    for (int row = 0; row < grid.length; row++) {
        for (int col = 0; col < grid[0].length; col++) {
            int distance = calculateDistance(points, row, col);
            minDistance = Math.min(distance, minDistance);
        }
    }
    return minDistance;
}

private int calculateDistance(List<Point> points, int row, int col) {
    int distance = 0;
    for (Point point : points) {
        distance += Math.abs(point.row - row) + Math.abs(point.col - col);
    }
    return distance;
}

private List<Point> getAllPoints(int[][] grid) {
    List<Point> points = new ArrayList<>();
    for (int row = 0; row < grid.length; row++) {
        for (int col = 0; col < grid[0].length; col++) {
            if (grid[row][col] == 1) {
                points.add(new Point(row, col));
            }
        }
    }
    return points;
}

public class Point {
    int row;
    int col;
    public Point(int row, int col) {
        this.row = row;
        this.col = col;
    }
}
```

**Complexity analysis**

* Time complexity : $$O(m^2n^2)$$.
Assume that $$k$$ is the total number of houses. For each point in the $$m \times n$$ size grid, we calculate the manhattan distance in $$O(k)$$. Therefore the time complexity is $$O(mnk)$$. But do note that there could be up to $$m \times n$$ houses, making the worst case time complexity to be $$O(m^2n^2)$$.

* Space complexity : $$O(mn)$$.

---
#### Approach #3 (Sorting) [Accepted]

Finding the best meeting point in a 2D grid seems difficult. Let us take a step back and solve the 1D case which is much simpler. Notice that the Manhattan distance is the sum of two independent variables. Therefore, once we solve the 1D case, we can solve the 2D case as two independent 1D problems.

Let us look at some 1D examples below:

    Case #1: 1-0-0-0-1

    Case #2: 0-1-0-1-0

We know the best meeting point must locate somewhere between the left-most and right-most point. For the above two cases, we would select the center point at $$x = 2$$ as the best meeting point. How about choosing the mean of all points as the meeting point?

Consider this case:

    Case #3: 1-0-0-0-0-0-0-1-1



Using the mean gives us $$\bar{x} = \frac{0 + 7 + 8}{3} = 5$$ as the meeting point. The total distance is $$10$$.

But the best meeting point should be at $$x = 7$$ and the total distance is $$8$$.

You may argue that the mean is *close* to the optimal point. But imagine a larger case with many 1's congregating on the right side and just a single 1 on the left-most side. Using the mean as the meeting point would be far from optimal.

Besides mean, what is a better way to represent the distribution of points? Would median be a better representation? Indeed. In fact, the median *must* be the optimal meeting point.

    Case #4: 1-1-0-0-1

To see why this is so, let us look at case #4 above and choose the median $$x = 1$$ as our initial meeting point. Assume that the total distance traveled is *d*. Note that we have equal number of points distributed to its left and to its right. Now let us move one step to its right where $$x = 2$$ and notice how the distance changes accordingly.

Since there are two points to the left of $$x = 2$$, we add $$2 * (+1)$$ to *d*. And *d* is offset by –1 since there is one point to the right. This means the distance had overall increased by 1.

Therefore, it is clear that:

>As long as there is equal number of points to the left and right of the meeting point, the total distance is minimized.

    Case #5: 1-1-0-0-1-1

One may think that the optimal meeting point must fall on one of the 1's. This is true for cases with odd number of 1's, but not necessarily true when there are even number of 1's, just like case #5 does. You can choose any of the $$x = 1$$ to $$x = 4$$ points and the total distance is minimized. Why?

The implementation is direct. First we collect both the row and column coordinates, sort them and select their middle elements. Then we calculate the total distance as the sum of two independent 1D problems.

```java
public int minTotalDistance(int[][] grid) {
    List<Integer> rows = new ArrayList<>();
    List<Integer> cols = new ArrayList<>();
    for (int row = 0; row < grid.length; row++) {
        for (int col = 0; col < grid[0].length; col++) {
            if (grid[row][col] == 1) {
                rows.add(row);
                cols.add(col);
            }
        }
    }
    int row = rows.get(rows.size() / 2);
    Collections.sort(cols);
    int col = cols.get(cols.size() / 2);
    return minDistance1D(rows, row) + minDistance1D(cols, col);
}

private int minDistance1D(List<Integer> points, int origin) {
    int distance = 0;
    for (int point : points) {
        distance += Math.abs(point - origin);
    }
    return distance;
}
```

Note that in the code above we do not need to sort *rows*, why?

**Complexity analysis**

* Time complexity : $$O(mn \log mn)$$.
Since there could be at most $$m \times n$$ points, therefore the time complexity is $$O(mn \log mn)$$ due to sorting.

* Space complexity : $$O(mn)$$.

---
#### Approach #4 (Collect Coordinates in Sorted Order) [Accepted]

We could use the [Selection algorithm](https://en.wikipedia.org/wiki/Selection_algorithm) to select the median in $$O(mn)$$ time, but there is an easier way. Notice that we can collect both the row and column coordinates in sorted order.

```java
public int minTotalDistance(int[][] grid) {
    List<Integer> rows = collectRows(grid);
    List<Integer> cols = collectCols(grid);
    int row = rows.get(rows.size() / 2);
    int col = cols.get(cols.size() / 2);
    return minDistance1D(rows, row) + minDistance1D(cols, col);
}

private int minDistance1D(List<Integer> points, int origin) {
    int distance = 0;
    for (int point : points) {
        distance += Math.abs(point - origin);
    }
    return distance;
}

private List<Integer> collectRows(int[][] grid) {
    List<Integer> rows = new ArrayList<>();
    for (int row = 0; row < grid.length; row++) {
        for (int col = 0; col < grid[0].length; col++) {
            if (grid[row][col] == 1) {
                rows.add(row);
            }
        }
    }
    return rows;
}

private List<Integer> collectCols(int[][] grid) {
    List<Integer> cols = new ArrayList<>();
    for (int col = 0; col < grid[0].length; col++) {
        for (int row = 0; row < grid.length; row++) {
            if (grid[row][col] == 1) {
                cols.add(col);
            }
        }
    }
    return cols;
}
```

<br>
You can calculate the distance without knowing the median using a two pointer approach. This neat approach is inspired by [@larrywang2014's solution](https://leetcode.com/discuss/65336/14ms-java-solution).

```java
public int minTotalDistance(int[][] grid) {
    List<Integer> rows = collectRows(grid);
    List<Integer> cols = collectCols(grid);
    return minDistance1D(rows) + minDistance1D(cols);
}

private int minDistance1D(List<Integer> points) {
    int distance = 0;
    int i = 0;
    int j = points.size() - 1;
    while (i < j) {
        distance += points.get(j) - points.get(i);
        i++;
        j--;
    }
    return distance;
}
```

**Complexity analysis**

* Time complexity : $$O(mn)$$.

* Space complexity : $$O(mn)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 14ms java solution
- Author: larrywang2014
- Creation Date: Thu Oct 22 2015 11:49:06 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 30 2018 09:48:48 GMT+0800 (Singapore Standard Time)

<p>
    public int minTotalDistance(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        
        List<Integer> I = new ArrayList<>(m);
        List<Integer> J = new ArrayList<>(n);
        
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                if(grid[i][j] == 1){
                    I.add(i);
                    J.add(j);
                }
            }
        }
        
        return getMin(I) + getMin(J);
    }
    
    private int getMin(List<Integer> list){
        int ret = 0;
        
        Collections.sort(list);
        
        int i = 0;
        int j = list.size() - 1;
        while(i < j){
            ret += list.get(j--) - list.get(i++);
        }
        
        return ret;
    }
</p>


### Am I the only person who don't know why median could give shortest distance?
- Author: TonyLic
- Creation Date: Wed Oct 28 2015 01:05:51 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 17:41:28 GMT+0800 (Singapore Standard Time)

<p>
When I first saw this question, intuitively I know shortest meeting point should be found in two separate dimension, however, even if on 1-D, how could I find the shortest meeting point? Then I clicked discuss and found out everybody's solution was using median to get shortest meeting point? WHY?

Actually, there is a famous conclusion in statistics that [the median minimizes the sum of absolute deviations][1].


  [1]: http://math.stackexchange.com/questions/113270/the-median-minimizes-the-sum-of-absolute-deviations
</p>


### Java 2ms/Python 40ms two pointers solution no median no sort with explanation
- Author: dietpepsi
- Creation Date: Fri Oct 23 2015 02:54:19 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 13:22:25 GMT+0800 (Singapore Standard Time)

<p>
Before solving the 2D problem we first consider a 1D case. The solution is quite simple. Just find the median of all the `x` coordinates and calculate the distance to the median.

Alternatively, we can also use two pointers to solve the 1D problem. `left` and `right` are how many people one left/right side of coordinates `i`/`j`. If we have more people on the left we let `j` decrease otherwise increase `i`. The time complexity is `O(n)` and space is `O(1)`.

To be more clear, a better view is we can think `i` and `j` as two meet points. All the people in `[0, i]` go to meet at `i` and all the people in `[j, n - 1]` meet at `j`. We let `left = sum(vec[:i+1])`, `right = sum(vec[j:])`, which are the number of people at each meet point, and `d` is the total distance for the `left` people meet at `i` and `right` people meet at `j`.

Our job is to let `i == j` with minimum `d`.

If we increase `i` by 1, the distance will increase by `left` since there are 'left' people at `i` and they just move 1 step. The same applies to `j`, when decrease `j` by 1, the distance will increase by `right`. To make sure the total distance `d` is minimized we certainly want to move the point with less people. And to make sure we do not skip any possible meet point options we need to move one by one.

For the 2D cases we first need to sum the columns and rows into two vectors and call the 1D algorithm.
The answer is the sum of the two. The time is then `O(mn)` and extra space is `O(m+n)`

Moreover, the solution is still `O(mn)` with the follow up:

> What if there are people sharing same home? 
> In other words the number in the grid can be more than 1.

**Java**

    public class Solution {
        public int minTotalDistance(int[][] grid) {
            int m = grid.length, n = grid[0].length;
            int[] row_sum = new int[n], col_sum = new int[m];
    
            for (int i = 0; i < m; ++i)
                for (int j = 0; j < n; ++j) {
                    row_sum[j] += grid[i][j];
                    col_sum[i] += grid[i][j];
                }

            return minDistance1D(row_sum) + minDistance1D(col_sum);
        }

        public int minDistance1D(int[] vector) {
            int i = -1, j = vector.length;
            int d = 0, left = 0, right = 0;
    
            while (i != j) {
                if (left < right) {
                    d += left;
                    left += vector[++i];
                }
                else {
                    d += right;
                    right += vector[--j];
                }
            }
            return d;
        }

    }
    // Runtime: 2ms

**Python**

    def minTotalDistance(self, grid):
        row_sum = map(sum, grid)
        col_sum = map(sum, zip(*grid)) # syntax sugar learned from stefan :-)

        def minTotalDistance1D(vec):
            i, j = -1, len(vec)
            d = left = right = 0
            while i != j:
                if left < right:
                    d += left
                    i += 1
                    left += vec[i]
                else:
                    d += right
                    j -= 1
                    right += vec[j]
            return d

        return minTotalDistance1D(row_sum) + minTotalDistance1D(col_sum)


    # 57 / 57 test cases passed.
    # Status: Accepted
    # Runtime: 40 ms
</p>


