---
title: "Actors and Directors Who Cooperated At Least Three Times"
weight: 1513
#id: "actors-and-directors-who-cooperated-at-least-three-times"
---
## Description
<div class="description">
<p>Table: <code>ActorDirector</code></p>

<pre>
+-------------+---------+
| Column Name | Type    |
+-------------+---------+
| actor_id    | int     |
| director_id | int     |
| timestamp   | int     |
+-------------+---------+
timestamp is the primary key column for this table.
</pre>

<p>&nbsp;</p>

<p>Write a SQL query for a report that provides the pairs <code>(actor_id, director_id)</code> where the actor have cooperated with the director at least 3 times.</p>

<p><strong>Example:</strong></p>

<pre>
ActorDirector table:
+-------------+-------------+-------------+
| actor_id    | director_id | timestamp   |
+-------------+-------------+-------------+
| 1           | 1           | 0           |
| 1           | 1           | 1           |
| 1           | 1           | 2           |
| 1           | 2           | 3           |
| 1           | 2           | 4           |
| 2           | 1           | 5           |
| 2           | 1           | 6           |
+-------------+-------------+-------------+

Result table:
+-------------+-------------+
| actor_id    | director_id |
+-------------+-------------+
| 1           | 1           |
+-------------+-------------+
The only pair is (1, 1) where they cooperated exactly 3 times.
</pre>

</div>

## Tags


## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Concise MySQL Solution Using HAVING Clause
- Author: zac4
- Creation Date: Thu May 23 2019 12:38:31 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 24 2019 13:41:08 GMT+0800 (Singapore Standard Time)

<p>
```SQL
SELECT actor_id, director_id
FROM ActorDirector
GROUP BY actor_id, director_id
HAVING COUNT(1) >= 3
```

</p>


### Solution Using MySQL GROUP BY and HAVING
- Author: huangguoxin0512
- Creation Date: Sun May 26 2019 05:43:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 26 2019 05:43:54 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT ACTOR_ID, DIRECTOR_ID
FROM ActorDirector
GROUP BY actor_id, director_id
HAVING COUNT(*) > 2
```
</p>


### Easy solution with HAVING COUNT(a =b)
- Author: mma4
- Creation Date: Thu Jul 16 2020 02:07:18 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 16 2020 02:07:18 GMT+0800 (Singapore Standard Time)

<p>
SELECT actor_id, director_id
FROM ActorDirector
GROUP BY actor_id, director_id
HAVING COUNT(actor_id = director_id) >= 3;
</p>


