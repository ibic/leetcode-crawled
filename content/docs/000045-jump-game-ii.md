---
title: "Jump Game II"
weight: 45
#id: "jump-game-ii"
---
## Description
<div class="description">
<p>Given an array of non-negative integers, you are initially positioned at the first index of the array.</p>

<p>Each element in the array represents your maximum jump length at that position.</p>

<p>Your goal is to reach the last index in the minimum number of jumps.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong> [2,3,1,1,4]
<strong>Output:</strong> 2
<strong>Explanation:</strong> The minimum number of jumps to reach the last index is 2.
    Jump 1 step from index 0 to 1, then 3 steps to the last index.</pre>

<p><strong>Note:</strong></p>

<p>You can assume that you can always reach the last index.</p>

</div>

## Tags
- Array (array)
- Greedy (greedy)

## Companies
- Amazon - 16 (taggedByAdmin: false)
- Microsoft - 6 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Nutanix - 14 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Huawei - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Greedy, Moving Forward

**Optimal solution**

There are several ways to solve this problem:

- Backtracking, $$\mathcal{O}(2^N)$$ time.

- Dynamic programming, $$\mathcal{O}(N)$$ time, $$\mathcal{O}(N)$$ space.

- Greedy, $$\mathcal{O}(N)$$ time and $$\mathcal{O}(1)$$ space.

In this article we will consider in details the optimal greedy approach. 

**Greedy algorithms**

Greedy problems usually look like 
"Find minimum number of _something_ to do _something_" or 
"Find maximum number of _something_ to fit in _some conditions_".

> The idea of greedy algorithm is to pick _locally_
optimal move at each step, that will lead to _globally_ optimal solution.

**Jump Game**

Let us start from the predecessor problem - [Jump Game](https://leetcode.com/articles/jump-game/),
which is to determine if it's possible to reach the last index. 

> The most straightforward solution is to compute at each point the _maximum position_ 
`max_pos` that one could reach starting from the current index `i` or _before_. 

The minimum value for `max_pos` is `i + nums[i]` = current position + jump length. 
For example, for index 0, `max_pos = nums[0]`.

![traversal](../Figures/45/max_pos.png)

Maximum position could be greater than `i + nums[i]` if one
of the previous cells allows longer jump. For example, cell 2 offers jump length equal
to 1, _i.e._ starting from index 2, one could reach cell 3. 
Although, starting _before_, at cell 1, one could reach cell 4, and hence
the maximum position at index 2 is 4. 

![traversal](../Figures/45/more_pos.png)

Now the solution is simple: to compare for each index `max_pos[i - 1]`
and `i`. 

> One couldn't reach index `i` if the maximum position that
one could reach starting from the previous cells is less than `i`. 
Unreachable index `i <= n -1` means that the last index is
unreachable as well.

![traversal](../Figures/45/impossible.png)

<iframe src="https://leetcode.com/playground/GTtrATcL/shared" frameBorder="0" width="100%" height="378" name="GTtrATcL"></iframe>


**Jump Game II**

To solve the current problem, let's use the same _maximum position_ 
`max_pos` that one could reach starting from the current index `i` or _before_. 
Here it's guaranteed that one can _always_ reach the last index, but 
what is the _minimum_ number of jumps needed for that? 

Let's use `max_steps` variable to track the maximum position 
reachable _during_ the current jump. For index 0, 
`max_steps = nums[0]`.  

![traversal](../Figures/45/step.png)

One could continue to the index `i = max_steps`. At this index
one needs one more jump, and this jump has to be performed from the index
`i = max_step` or before. 

![traversal](../Figures/45/one_more.png)

> To minimize the number of jumps,
let's follow the "greedy" strategy and choose the longest possible 
jump. The longest jump is defined by the maximum reachable position that
we've computed just above: `max_steps = max_pos`. 

![traversal](../Figures/45/greedy.png)

Now we could repeat the above process again and again, till we reach 
the last index.

**Algorithm**

- Initiate the maximum position that one could reach starting from the current 
index `i` or before: `max_pos = nums[0]`.

- Initiate the maximum position reachable _during_ the current jump:
`max_steps = nums[0]`.

- Initiate number of steps: at least one, if array has more than 
1 cell.

- Iterate over number of elements in the input array:

    - If `max_step < i`, one needs one more jump: 
    `jumps += 1`. 
    To minimize the number of jumps,
    choose the longest possible one: `max_steps = max_pos`.
    
    - Update `max_pos = max(max_pos, i + nums[i])`.
    
- Return the number of jumps.

**Implementation**

!?!../Documents/45_LIS.json:1000,304!?!

<iframe src="https://leetcode.com/playground/w7mr2YWr/shared" frameBorder="0" width="100%" height="497" name="w7mr2YWr"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N)$$, it's one pass along the 
input array.
 
* Space complexity : $$\mathcal{O}(1)$$ since it's a constant 
space solution.

## Accepted Submission (python3)
```python3
class Solution:
    def jump(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        leny = len(nums)
        lastFurthest = 0
        furthest = 0
        steps = 0
        for i in range(leny):
            if lastFurthest < i:
                steps += 1
                lastFurthest = furthest
            furthest = max(furthest, i + nums[i])
        return steps
```

## Top Discussions
### Concise O(n) one loop JAVA solution based on Greedy
- Author: Cheng_Zhang
- Creation Date: Sat Oct 31 2015 08:16:55 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 14:26:03 GMT+0800 (Singapore Standard Time)

<p>
**Explanation**

The main idea is based on greedy. Let's say the range of the current jump is [curBegin, curEnd], curFarthest is the farthest point that all points in [curBegin, curEnd] can reach.  Once the current point reaches curEnd, then trigger another jump, and set the new curEnd with curFarthest, then keep the above steps, as the following:

    public int jump(int[] A) {
    	int jumps = 0, curEnd = 0, curFarthest = 0;
    	for (int i = 0; i < A.length - 1; i++) {
    		curFarthest = Math.max(curFarthest, i + A[i]);
    		if (i == curEnd) {
    			jumps++;
    			curEnd = curFarthest;
    		}
    	}
    	return jumps;
    }
</p>


### O(n), BFS solution
- Author: enriquewang
- Creation Date: Mon Sep 08 2014 12:38:48 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 11:34:12 GMT+0800 (Singapore Standard Time)

<p>
I try to change this problem to a BFS problem, where nodes in level i are all the nodes that can be reached in i-1th jump. for example.   2 3 1 1 4 , is 
                   2||
               3   1||
               1   4  || 

clearly,  the minimum jump of 4 is 2 since 4 is in level 3.  my ac code.



     int jump(int A[], int n) {
    	 if(n<2)return 0;
    	 int level=0,currentMax=0,i=0,nextMax=0;
    
    	 while(currentMax-i+1>0){		//nodes count of current level>0
    		 level++;
    		 for(;i<=currentMax;i++){	//traverse current level , and update the max reach of next level
    			nextMax=max(nextMax,A[i]+i);
    			if(nextMax>=n-1)return level;   // if last element is in level+1,  then the min jump=level 
    		 }
    		 currentMax=nextMax;
    	 }
    	 return 0;
     }
</p>


### 10-lines C++ (16ms) / Python BFS Solutions with Explanations
- Author: jianchao-li
- Creation Date: Wed Jul 15 2015 21:25:23 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 10:09:03 GMT+0800 (Singapore Standard Time)

<p>
This problem has a nice BFS structure. Let's illustrate it using the example `nums = [2, 3, 1, 1, 4]` in the problem statement. We are initially at position `0`. Then we can move at most `nums[0]` steps from it. So, after one move, we may reach `nums[1] = 3` or `nums[2] = 1`. So these nodes are reachable in `1` move. From these nodes, we can further move to `nums[3] = 1` and `nums[4] = 4`. Now you can see that the target `nums[4] = 4` is reachable in `2` moves. 

Putting these into codes, we keep two pointers `start` and `end` that record the current range of the starting nodes. Each time after we make a move, update `start` to be `end  + 1` and `end` to be the farthest index that can be reached in `1` move from the current `[start, end]`. 
 
To get an accepted solution, it is important to handle all the edge cases. And the following codes handle all of them in a unified way without using the unclean `if` statements :-)
 
----------
**C++**

    class Solution {
    public:
        int jump(vector<int>& nums) {
            int n = nums.size(), step = 0, start = 0, end = 0;
            while (end < n - 1) {
                step++; 
    			int maxend = end + 1;
    			for (int i = start; i <= end; i++) {
                    if (i + nums[i] >= n - 1) return step;
    				maxend = max(maxend, i + nums[i]);
    			}
                start = end + 1;
                end = maxend;
            }
    		return step;
        }
    };

----------
**Python** 

    class Solution:
        # @param {integer[]} nums
        # @return {integer}
        def jump(self, nums):
            n, start, end, step = len(nums), 0, 0, 0
            while end < n - 1:
                step += 1
                maxend = end + 1
                for i in range(start, end + 1):
                    if i + nums[i] >= n - 1:
                        return step
                    maxend = max(maxend, i + nums[i])
                start, end = end + 1, maxend
            return step
</p>


