---
title: "Max Consecutive Ones II"
weight: 462
#id: "max-consecutive-ones-ii"
---
## Description
<div class="description">
<p>
Given a binary array, find the maximum number of consecutive 1s in this array if you can flip at most one 0.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> [1,0,1,1,0]
<b>Output:</b> 4
<b>Explanation:</b> Flip the first zero will get the the maximum number of consecutive 1s.
    After flipping, the maximum number of consecutive 1s is 4.
</pre>
</p>

<p><b>Note:</b>
<ul>
<li>The input array will only contain <code>0</code> and <code>1</code>.</li>
<li>The length of input array is a positive integer and will not exceed 10,000</li>
</ul>
</p>

<p><b>Follow up:</b><br />
What if the input numbers come in one by one as an <b>infinite stream</b>? In other words, you can't store all numbers coming from the stream as it's too large to hold in memory. Could you solve it efficiently?
</p>
</div>

## Tags
- Two Pointers (two-pointers)

## Companies
- Yandex - 3 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java clean solution easily extensible to flipping k zero and follow-up handled
- Author: yuxiangmusic
- Creation Date: Sun Jan 15 2017 12:48:19 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 16:38:01 GMT+0800 (Singapore Standard Time)

<p>
The idea is to keep a window ```[l, h]``` that contains at most ```k``` zero

The following solution does not handle follow-up, because ```nums[l]``` will need to access previous input stream
```Time: O(n) Space: O(1)```
```
    public int findMaxConsecutiveOnes(int[] nums) {
        int max = 0, zero = 0, k = 1; // flip at most k zero
        for (int l = 0, h = 0; h < nums.length; h++) {
            if (nums[h] == 0)                                           
                zero++;
            while (zero > k)
                if (nums[l++] == 0)
                    zero--;                                     
            max = Math.max(max, h - l + 1);
        }                                                               
        return max;             
    }
```
Now let's deal with follow-up, we need to store up to ```k``` indexes of zero within the window ```[l, h]``` so that we know where to move ```l``` next when the window contains more than ```k``` zero. If the input stream is infinite, then the output could be extremely large because there could be super long consecutive ones. In that case we can use ```BigInteger``` for all indexes. For simplicity, here we will use ```int```
```Time: O(n) Space: O(k)```
```
    public int findMaxConsecutiveOnes(int[] nums) {                 
        int max = 0, k = 1; // flip at most k zero
        Queue<Integer> zeroIndex = new LinkedList<>(); 
        for (int l = 0, h = 0; h < nums.length; h++) {
            if (nums[h] == 0)
                zeroIndex.offer(h);
            if (zeroIndex.size() > k)                                   
                l = zeroIndex.poll() + 1;
            max = Math.max(max, h - l + 1);
        }
        return max;                     
    }
```
Note that setting ```k = 0``` will give a solution to the earlier version [Max Consecutive Ones](https://leetcode.com/problems/max-consecutive-ones/)

For ```k = 1``` we can apply the same idea to simplify the solution. Here ```q``` stores the index of zero within the window ```[l, h]``` so its role is similar to ```Queue``` in the above solution
```
    public int findMaxConsecutiveOnes(int[] nums) {
        int max = 0, q = -1;
        for (int l = 0, h = 0; h < nums.length; h++) {
            if (nums[h] == 0) {
                l = q + 1;
                q = h;
            }
            max = Math.max(max, h - l + 1);
        }                                                               
        return max;             
    }
```
</p>


### Concise Python solution good for follow-up, time O(n), space O(1)
- Author: cmc
- Creation Date: Sat Mar 11 2017 13:39:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 23:29:02 GMT+0800 (Singapore Standard Time)

<p>
store the length of previous and current consecutive 1's (separated by the last 0) as `pre` and `curr` , respectively. <br>

Whenever we get a new number, update these two variables accordingly. The consecutive length would be `pre  + 1 + curr`, where the `1` is a zero that got flipped to 1. (note that `pre` is initialized to `-1`, meaning that we haven't seen any 0 yet)

```
class Solution(object):
    def findMaxConsecutiveOnes(self, nums):
        # previous and current length of consecutive 1 
        pre, curr, maxlen = -1, 0, 0
        for n in nums:
            if n == 0:
                pre, curr = curr, 0
            else:
                curr += 1
            maxlen = max(maxlen, pre + 1 + curr )
        
        return maxlen
```
</p>


### Java Concise O(n) Time O(1) Space
- Author: compton_scatter
- Creation Date: Sun Jan 15 2017 12:35:21 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 19 2018 12:11:34 GMT+0800 (Singapore Standard Time)

<p>
```
public int findMaxConsecutiveOnes(int[] nums) {
     int maxConsecutive = 0, zeroLeft = 0, zeroRight = 0;
     for (int i=0;i<nums.length;i++) {
         zeroRight++;
         if (nums[i] == 0) {
             zeroLeft = zeroRight;
             zeroRight = 0;
         }
         maxConsecutive = Math.max(maxConsecutive, zeroLeft+zeroRight); 
     }
     return maxConsecutive;
}
```
</p>


