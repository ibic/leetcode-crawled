---
title: "Basic Calculator II"
weight: 211
#id: "basic-calculator-ii"
---
## Description
<div class="description">
<p>Implement a basic calculator to evaluate a simple expression string.</p>

<p>The expression string contains only <b>non-negative</b> integers, <code>+</code>, <code>-</code>, <code>*</code>, <code>/</code> operators and empty spaces <code> </code>. The integer division should truncate toward zero.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>&quot;3+2*2&quot;
<strong>Output:</strong> 7
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> &quot; 3/2 &quot;
<strong>Output:</strong> 1</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> &quot; 3+5 / 2 &quot;
<strong>Output:</strong> 5
</pre>

<p><b>Note:</b></p>

<ul>
	<li>You may assume that the given expression is always valid.</li>
	<li><b>Do not</b> use the <code>eval</code> built-in library function.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Facebook - 23 (taggedByAdmin: false)
- Amazon - 20 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- eBay - 3 (taggedByAdmin: false)
- Postmates - 2 (taggedByAdmin: false)
- Uber - 6 (taggedByAdmin: false)
- Snapchat - 3 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Indeed - 3 (taggedByAdmin: false)
- Houzz - 3 (taggedByAdmin: false)
- DoorDash - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Tableau - 3 (taggedByAdmin: false)
- Reddit - 3 (taggedByAdmin: false)
- Intuit - 2 (taggedByAdmin: false)
- Tencent - 2 (taggedByAdmin: false)
- Walmart Labs - 2 (taggedByAdmin: false)
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Share my  java solution
- Author: abner
- Creation Date: Wed Jun 24 2015 11:15:20 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 13:23:06 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
    public int calculate(String s) {
        int len;
        if(s==null || (len = s.length())==0) return 0;
        Stack<Integer> stack = new Stack<Integer>();
        int num = 0;
        char sign = '+';
        for(int i=0;i<len;i++){
            if(Character.isDigit(s.charAt(i))){
                num = num*10+s.charAt(i)-'0';
            }
            if((!Character.isDigit(s.charAt(i)) &&' '!=s.charAt(i)) || i==len-1){
                if(sign=='-'){
                    stack.push(-num);
                }
                if(sign=='+'){
                    stack.push(num);
                }
                if(sign=='*'){
                    stack.push(stack.pop()*num);
                }
                if(sign=='/'){
                    stack.push(stack.pop()/num);
                }
                sign = s.charAt(i);
                num = 0;
            }
        }

        int re = 0;
        for(int i:stack){
            re += i;
        }
        return re;
    }
}
</p>


### 17 lines C++, easy, 20 ms
- Author: StefanPochmann
- Creation Date: Tue Jun 23 2015 01:26:08 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 19:18:48 GMT+0800 (Singapore Standard Time)

<p>
If you don't like the `44 - op` ASCII trick, you can use `op == '+' ? 1 : -1` instead. And wow, I didn't know C++ has `or`. I'm a Python guy and wrote that out of habit and only realized it after getting this accepted :-)

    int calculate(string s) {
        istringstream in('+' + s + '+');
        long long total = 0, term = 0, n;
        char op;
        while (in >> op) {
            if (op == '+' or op == '-') {
                total += term;
                in >> term;
                term *= 44 - op;
            } else {
                in >> n;
                if (op == '*')
                    term *= n;
                else
                    term /= n;
            }
        }
        return total;
    }
</p>


### Python short solution with stack.
- Author: OldCodingFarmer
- Creation Date: Sat Aug 22 2015 23:03:46 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 17 2018 15:13:06 GMT+0800 (Singapore Standard Time)

<p>
        
    def calculate(self, s):
        if not s:
            return "0"
        stack, num, sign = [], 0, "+"
        for i in xrange(len(s)):
            if s[i].isdigit():
                num = num*10+ord(s[i])-ord("0")
            if (not s[i].isdigit() and not s[i].isspace()) or i == len(s)-1:
                if sign == "-":
                    stack.append(-num)
                elif sign == "+":
                    stack.append(num)
                elif sign == "*":
                    stack.append(stack.pop()*num)
                else:
                    tmp = stack.pop()
                    if tmp//num < 0 and tmp%num != 0:
                        stack.append(tmp//num+1)
                    else:
                        stack.append(tmp//num)
                sign = s[i]
                num = 0
        return sum(stack)
</p>


