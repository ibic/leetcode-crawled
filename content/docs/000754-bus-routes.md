---
title: "Bus Routes"
weight: 754
#id: "bus-routes"
---
## Description
<div class="description">
<p>We have a list of bus routes. Each <code>routes[i]</code> is a bus route that the i-th bus&nbsp;repeats forever. For example if <code>routes[0] = [1, 5, 7]</code>, this means that the first&nbsp;bus (0-th indexed) travels in the sequence 1-&gt;5-&gt;7-&gt;1-&gt;5-&gt;7-&gt;1-&gt;... forever.</p>

<p>We start at bus stop <code>S</code> (initially not on a bus), and we want to go to bus stop <code>T</code>. Travelling by buses only, what is the least number of buses we must take to reach our destination? Return -1 if it is not possible.</p>

<pre>
<strong>Example:</strong>
<strong>Input:</strong> 
routes = [[1, 2, 7], [3, 6, 7]]
S = 1
T = 6
<strong>Output:</strong> 2
<strong>Explanation:</strong> 
The best strategy is take the first bus to the bus stop 7, then take the second bus to the bus stop 6.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= routes.length &lt;= 500</code>.</li>
	<li><code>1 &lt;= routes[i].length &lt;= 10^5</code>.</li>
	<li><code>0 &lt;= routes[i][j] &lt; 10 ^ 6</code>.</li>
</ul>

</div>

## Tags
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 6 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Qualtrics - 2 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- DiDi - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Breadth First Search [Accepted]

**Intuition**

Instead of thinking of the stops as nodes (of a graph), think of the buses as nodes.  We want to take the least number of buses, which is a shortest path problem, conducive to using a breadth-first search.

**Algorithm**

We perform a breadth first search on bus numbers.  When we start at `S`, originally we might be able to board many buses, and if we end at `T` we may have many `targets` for our goal state.

One difficulty is to efficiently decide whether two buses are connected by an edge.  They are connected if they share at least one bus stop.  Whether two lists share a common value can be done by set intersection (HashSet), or by sorting each list and using a two pointer approach.

To make our search easy, we will annotate the depth of each node: `info[0] = node, info[1] = depth`.

<iframe src="https://leetcode.com/playground/AEba8t6x/shared" frameBorder="0" width="100%" height="500" name="AEba8t6x"></iframe>

**Complexity Analysis**

* Time Complexity:  Let $$N$$ denote the number of buses, and $$b_i$$ be the number of stops on the $$i$$th bus.

    * To create the graph, in Python we do $$O(\sum (N - i) b_i)$$ work (we can improve this by checking for which of `r1, r2` is smaller), while in Java we did a $$O(\sum b_i \log b_i)$$ sorting step, plus our searches are $$O(N \sum b_i)$$ work.

    * Our (breadth-first) search is on $$N$$ nodes, and each node could have $$N$$ edges, so it is $$O(N^2)$$.

* Space Complexity: $$O(N^2 + \sum b_i)$$ additional space complexity, the size of `graph` and `routes`.  In Java, our space complexity is $$O(N^2)$$ because we do not have an equivalent of `routes`.  Dual-pivot quicksort (as used in `Arrays.sort(int[])`) is an in-place algorithm, so in Java we did not increase our space complexity by sorting.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple Java Solution using BFS
- Author: trotro
- Creation Date: Sun Apr 08 2018 11:05:40 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 10 2018 06:27:33 GMT+0800 (Singapore Standard Time)

<p>
For each of the bus stop, we maintain all the buses (bus routes) that go through it. To do that, we use a HashMap, where bus stop number is the key and all the buses (bus routes) that go through it are added to an ArrayList.

We use BFS, where we process elements in a level-wise manner. We add the Start bus stop in the queue. Next, when we enter the while loop, we add all the bus stops that are reachable by all the bus routes that go via the Start. Thus, if we have the input as [[1, 2, 7], [3, 6, 7]] and Start as 6, then upon processing bus stop 6, we would add bus stops 3 and 7.
With this approach, all the bus stops at a given level, are "equal distance" from the start node, in terms of number of buses that need to be changed. 

To avoid loops, we also maintain a HashSet that stores the buses  that we have already visited.

Note that while in this approach, we use each stop for doing BFS, one could also consider each bus (route) also for BFS.

```
class Solution {
    public int numBusesToDestination(int[][] routes, int S, int T) {
       HashSet<Integer> visited = new HashSet<>();
       Queue<Integer> q = new LinkedList<>();
       HashMap<Integer, ArrayList<Integer>> map = new HashMap<>();
       int ret = 0; 
        
       if (S==T) return 0; 
        
       for(int i = 0; i < routes.length; i++){
            for(int j = 0; j < routes[i].length; j++){
                ArrayList<Integer> buses = map.getOrDefault(routes[i][j], new ArrayList<>());
                buses.add(i);
                map.put(routes[i][j], buses);                
            }       
        }
                
       q.offer(S); 
       while (!q.isEmpty()) {
           int len = q.size();
           ret++;
           for (int i = 0; i < len; i++) {
               int cur = q.poll();
               ArrayList<Integer> buses = map.get(cur);
               for (int bus: buses) {
                    if (visited.contains(bus)) continue;
                    visited.add(bus);
                    for (int j = 0; j < routes[bus].length; j++) {
                        if (routes[bus][j] == T) return ret;
                        q.offer(routes[bus][j]);  
                   }
               }
           }
        }
        return -1;
    }
}
```
</p>


### [C++/Java/Python] BFS Solution
- Author: lee215
- Creation Date: Sun Apr 08 2018 12:42:42 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 24 2019 13:54:36 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**:
The first part loop on routes and record stop to routes mapping in `to_route`.
The second part is general bfs. Take a stop from queue and find all connected route.
The hashset `seen` record all visited stops and we won\'t check a stop for twice.
We can also use a hashset to record all visited routes, or just clear a route after visit.
<br>

**C++:**
```
    int numBusesToDestination(vector<vector<int>>& routes, int S, int T) {
        unordered_map<int, vector<int>> to_routes;
        for (int i = 0; i < routes.size(); ++i)
            for (int j : routes[i])
                to_routes[j].push_back(i);
        queue<pair<int, int>> bfs;
        bfs.push({S, 0});
        unordered_set<int> seen = {S};
        while (!bfs.empty()) {
            int stop = bfs.front().first, bus = bfs.front().second;
            bfs.pop();
            if (stop == T)
                return bus;
            for (int i : to_routes[stop]) {
                for (int j : routes[i]) {
                    if (seen.find(j) == seen.end()) {
                        seen.insert(j);
                        bfs.push({j, bus + 1});
                    }
                }
                routes[i].clear();
            }
        }
        return -1;
    }
```

**Java:**
```
    public int numBusesToDestination(int[][] routes, int S, int T) {
        int n = routes.length;
        HashMap<Integer, HashSet<Integer>> to_routes = new HashMap<>();
        for (int i = 0; i < routes.length; ++i) {
            for (int j : routes[i]) {
                if (!to_routes.containsKey(j))
                    to_routes.put(j, new HashSet<Integer>());
                to_routes.get(j).add(i);
            }
        }
        Queue<int[]> bfs = new ArrayDeque();
        bfs.offer(new int[] {S, 0});
        HashSet<Integer> seen = new HashSet<>();
        seen.add(S);
        boolean[] seen_routes = new boolean[n];
        while (!bfs.isEmpty()) {
            int stop = bfs.peek()[0], bus = bfs.peek()[1];
            bfs.poll();
            if (stop == T) return bus;
            for (int i : to_routes.get(stop)) {
                if (seen_routes[i]) continue;
                for (int j : routes[i]) {
                    if (!seen.contains(j)) {
                        seen.add(j);
                        bfs.offer(new int[] {j, bus + 1});
                    }
                }
                seen_routes[i] = true;
            }
        }
        return -1;
    }
```

**Python:**
```
    def numBusesToDestination(self, routes, S, T):
        to_routes = collections.defaultdict(set)
        for i, route in enumerate(routes):
            for j in route:
                to_routes[j].add(i)
        bfs = [(S, 0)]
        seen = set([S])
        for stop, bus in bfs:
            if stop == T: return bus
            for i in to_routes[stop]:
                for j in routes[i]:
                    if j not in seen:
                        bfs.append((j, bus + 1))
                        seen.add(j)
                routes[i] = []  # seen route
        return -1
```
</p>


### Python BFS With Explanation
- Author: cetingzhou
- Creation Date: Thu Jul 19 2018 05:26:06 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 00:48:45 GMT+0800 (Singapore Standard Time)

<p>
```
# Reference: https://leetcode.com/problems/bus-routes/discuss/122712/Simple-Java-Solution-using-BFS
from collections import deque
class Solution:
    # This is a very good BFS problem.
    # In BFS, we need to traverse all positions in each level firstly, and then go to the next level.
    # Our task is to figure out:
    # 1. What is the level in this problem?
    # 2. What is the position we want in this problem?
    # 3. How to traverse all positions in a level?
    # 
    # For this problem:
    # 1. The level is each time to take bus.
    # 2. The position is all of the stops you can reach for taking one time of bus.
    # 3. Using a queue to record all of the stops can be arrived for each time you take buses.
    def numBusesToDestination(self, routes, S, T):
        """
        :type routes: List[List[int]]
        :type S: int
        :type T: int
        :rtype: int
        """
        # You already at the terminal, so you needn\'t take any bus.
        if S == T: return 0
        
        # You need to record all the buses you can take at each stop so that you can find out all
        # of the stops you can reach when you take one time of bus.
        # the key is stop and the value is all of the buses you can take at this stop.
        stopBoard = {} 
        for bus, stops in enumerate(routes):
            for stop in stops:
                if stop not in stopBoard:
                    stopBoard[stop] = [bus]
                else:
                    stopBoard[stop].append(bus)
        
        # The queue is to record all of the stops you can reach when you take one time of bus.
        queue = deque([S])
        # Using visited to record the buses that have been taken before, because you needn\'t to take them again.
        visited = set()

        res = 0
        while queue:
            # take one time of bus.
            res += 1
            # In order to traverse all of the stops you can reach for this time, you have to traverse
            # all of the stops you can reach in last time.
            pre_num_stops = len(queue)
            for _ in range(pre_num_stops):
                curStop = queue.popleft()
                # Each stop you can take at least one bus, you need to traverse all of the buses at this stop
                # in order to get all of the stops can be reach at this time.
                for bus in stopBoard[curStop]:
                    # if the bus you have taken before, you needn\'t take it again.
                    if bus in visited: continue
                    visited.add(bus)
                    for stop in routes[bus]:
                        if stop == T: return res
                        queue.append(stop)
        return -1
```
</p>


