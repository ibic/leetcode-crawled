---
title: "Number of Submatrices That Sum to Target"
weight: 1050
#id: "number-of-submatrices-that-sum-to-target"
---
## Description
<div class="description">
<p>Given a <code>matrix</code>&nbsp;and a <code>target</code>, return the number of non-empty submatrices that sum to <font face="monospace">target</font>.</p>

<p>A submatrix <code>x1, y1, x2, y2</code> is the set of all cells <code>matrix[x][y]</code> with <code>x1 &lt;= x &lt;= x2</code> and <code>y1 &lt;= y &lt;= y2</code>.</p>

<p>Two submatrices <code>(x1, y1, x2, y2)</code> and <code>(x1&#39;, y1&#39;, x2&#39;, y2&#39;)</code> are different if they have some coordinate&nbsp;that is different: for example, if <code>x1 != x1&#39;</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/02/mate1.jpg" style="width: 242px; height: 242px;" />
<pre>
<strong>Input:</strong> matrix = [[0,1,0],[1,1,1],[0,1,0]], target = 0
<strong>Output:</strong> 4
<strong>Explanation:</strong> The four 1x1 submatrices that only contain 0.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> matrix = [[1,-1],[-1,1]], target = 0
<strong>Output:</strong> 5
<strong>Explanation:</strong> The two 1x2 submatrices, plus the two 2x1 submatrices, plus the 2x2 submatrix.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> matrix = [[904]], target = 0
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= matrix.length &lt;= 100</code></li>
	<li><code>1 &lt;= matrix[0].length &lt;= 100</code></li>
	<li><code>-1000 &lt;= matrix[i] &lt;= 1000</code></li>
	<li><code>-10^8 &lt;= target &lt;= 10^8</code></li>
</ul>

</div>

## Tags
- Array (array)
- Dynamic Programming (dynamic-programming)
- Sliding Window (sliding-window)

## Companies
- Google - 8 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Prerequisites

In this section, we list some problems and concepts that could help with 
the resolution of this particular problem.

**[Optional] Number of Subarrays that Sum to Target**

In this solution we're going to reduce this 2D problem to a 1D one
[Subarray Sum Equals k](https://leetcode.com/problems/subarray-sum-equals-k/).

One of the best solutions for this 1D problem is to use a hashmap 
with key as prefix sum and value as number of subarrays starting from index zero and
having this particular sum value.

If you don't remember this idea well, you may want to check 
Approach 4 in the above article to make reading this article easier.

**[Required] 2D Prefix Sum**

Many problems could be solved using so-called [prefix sum](https://en.wikipedia.org/wiki/Prefix_sum).

In one dimension it's simple: there is an array of numbers 
$$x_0, x_1, x_2, ..., x_n$$ and we're building a second array which is a sum
of prefixes of the input array:

$$
P_0 = x_0 \\
P_1 = x_0 + x_1 \\
... \\
P_n = x_0 + x_1 + x_2 + ... + x_n
$$

Here is how it looks like, we sum up the current value with all values 
on the left:

![append](../Figures/1074/1d_prefix3.png)

> In 2D the idea is basically the same: prefix sum $$P_{mn}$$ is a sum 
of the current value with the integers above or on the left. 

$$
P_{mn} = \sum\limits_{i = 0}^{i = m}\sum\limits_{j = 0}^{j = n}{x_{ij}}
$$

![append](../Figures/1074/2d_prefix.png)

Prefix sum could be computed in $$\mathcal{O}(R \times C)$$ time,
where $$R$$ is the number of rows and $$C$$ is the number of columns.

<iframe src="https://leetcode.com/playground/xmpEesyz/shared" frameBorder="0" width="100%" height="157" name="xmpEesyz"></iframe>

Using a 2D prefix sum, we can now query the sum of any submatrix in $$\mathcal{O}(1)$$ time.
<br /> 
<br />


---
#### Overview: Reduce 2D Problem to 1D

Let's fix two rows: $$r_1$$ and $$r_2$$, and consider all "prefix" matrices which are using 
all rows from $$r_1$$ to $$r_2$$.

![append](../Figures/1074/all_matrices3.png)

Using 2D prefix sum `ps`, one could easily get the sum of each prefix matrix:
`curr_sum = ps[r2][col] - ps[r1 - 1][col]`. 

This sum itself could be considered as a 1D prefix sum because when rows are fixed,
there is just one parameter to play with: the column `col`.

The job is done! The problem is reduced to the 1D problem 
[Number of Subarrays that Sum to Target](https://leetcode.com/problems/subarray-sum-equals-k/),
and to underline the similarity, let's reuse the code from Approach 4 of this 1D problem.

<iframe src="https://leetcode.com/playground/5AprNfeV/shared" frameBorder="0" width="100%" height="259" name="5AprNfeV"></iframe>

We've got pretty nice combination here:

- Use 2D prefix sum to reduce the problem to lots of smaller 1D problems.

- Use 1D prefix sum to solve these 1D problems. 
<br /> 
<br />


---
#### Approach 1: Number of Subarrays that Sum to Target: Horizontal 1D Prefix Sum 

![append](../Figures/1074/all_matrices3.png)

**Algorithm**

- Initialize the result: `count = 0`.

- Compute the number of rows: `r = len(matrix)` and number of columns:
`c = len(matrix[0])`.

- Compute 2D prefix sum `ps`. To simplify the code, 
we allocate one more row and one more column, reserving row 0 and column 0 
for zero values. This way, we avoid computing the first row and the first column
separately.

- Iterate over the rows: r1 from 1 to r, and r2 from r1 to r:
    
    - Inside this double loop, the left and right row boundaries are fixed. 
    Now it's time to initialize a hashmap: 1D prefix sum -> number of matrices which
    use `[r1, r2]` rows and sum to this prefix sum. 
    Sum of empty matrix is equal to zero: `h[0] = 1`.
    
    - Iterate over the columns from 1 to c + 1. At each step:
    
        - Compute current 1D prefix sum `curr_sum` using 
        previously computed 2D prefix sum `ps`: `curr_sum = ps[r2][col] - ps[r1 - 1][col]`.
        
        - The number of times the sum `curr_sum - target` occurred,
        defines the number of matrices which use `r1 ... r2` rows and sum to target.
        Increment the count: `count += h[curr_sum - target]`.
        
        - Add the current 1D prefix sum into hashmap.
        
- Return `count`.

**Implementation**

!?!../Documents/1074_LIS_test.json:1000,408!?!

<iframe src="https://leetcode.com/playground/8DpFS3RT/shared" frameBorder="0" width="100%" height="500" name="8DpFS3RT"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(R^2 C)$$,
where $$R$$ is the number of rows and $$C$$ is the number of columns.

* Space complexity: $$\mathcal{O}(RC)$$ to store 2D prefix sum.  
<br /> 
<br />


---
#### Approach 2: Number of Subarrays that Sum to Target: Vertical 1D Prefix Sum 

In Approach 1, we were fixing two rows, and computing the "horizontal" 1D prefix sum.
One could follow the same logic by fixing two **_columns_**, and computing the "vertical"
1D prefix sum.

![append](../Figures/1074/vertical_matrices2.png)

**Algorithm**

- Initialize the result: `count = 0`.

- Compute the number of rows: `r = len(matrix)` and the number of columns:
`c = len(matrix[0])`.

- Compute 2D prefix sum `ps`. To simplify the code, 
we allocate one more row and one more column, reserving row 0 and column 0 
for zero values.

- Iterate over the columns: c1 from 1 to c, and c2 from c1 to c:
    
    - Inside this double loop, the upper and lower column boundaries are fixed. 
    Now it's time to initialize a hashmap: 1D prefix sum -> number of matrices which
    use `[c1, c2]` columns and sum to this prefix sum. 
    Sum of empty matrix is equal to zero: `h[0] = 1`.
    
    - Iterate over the rows from 1 to r + 1. At each step:
    
        - Compute current 1D prefix sum `curr_sum` using 
        previously computed 2D prefix sum `ps`: `curr_sum = ps[row][c2] - ps[row][c1 - 1]`.
        
        - The number of times the sum `curr_sum - target` occurred,
        defines the number of matrices which use `c1 ... c2` rows and sum to target.
        Increment the count: `count += h[curr_sum - target]`.
        
        - Add the current 1D prefix sum into hashmap.
        
- Return `count`.

**Implementation**

<iframe src="https://leetcode.com/playground/BYei5gVm/shared" frameBorder="0" width="100%" height="500" name="BYei5gVm"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(R C^2)$$,
where $$R$$ is the number of rows and $$C$$ is the number of columns.

* Space complexity: $$\mathcal{O}(RC)$$ to store 2D prefix sum.  
<br /> 
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Find the Subarray with Target Sum
- Author: lee215
- Creation Date: Sun Jun 02 2019 12:03:28 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Apr 24 2020 12:02:58 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
Preaquis: [560. Subarray Sum Equals K](https://leetcode.com/problems/subarray-sum-equals-k/description/)
Find the Subarray with Target Sum in linear time.
<br>

# Explanation
For each row, calculate the prefix sum.
For each pair of columns,
calculate the accumulated sum of rows.
Now this problem is same to, "Find the Subarray with Target Sum".
<br>

# Complexity
Time `O(MN^2)`
Space `O(N)`
<br>

**Java**
```java
    public int numSubmatrixSumTarget(int[][] A, int target) {
        int res = 0, m = A.length, n = A[0].length;
        for (int i = 0; i < m; i++)
            for (int j = 1; j < n; j++)
                A[i][j] += A[i][j - 1];
        Map<Integer, Integer> counter = new HashMap<>();
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                counter.clear();
                counter.put(0, 1);
                int cur = 0;
                for (int k = 0; k < m; k++) {
                    cur += A[k][j] - (i > 0 ? A[k][i - 1] : 0);
                    res += counter.getOrDefault(cur - target, 0);
                    counter.put(cur, counter.getOrDefault(cur, 0) + 1);
                }
            }
        }
        return res;
    }
```

**C++**
```cpp
    int numSubmatrixSumTarget(vector<vector<int>>& A, int target) {
        int res = 0, m = A.size(), n = A[0].size();
        for (int i = 0; i < m; i++)
            for (int j = 1; j < n; j++)
                A[i][j] += A[i][j - 1];

        unordered_map<int, int> counter;
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                counter = {{0,1}};
                int cur = 0;
                for (int k = 0; k < m; k++) {
                    cur += A[k][j] - (i > 0 ? A[k][i - 1] : 0);
                    res += counter.find(cur - target) != counter.end() ? counter[cur - target] : 0;
                    counter[cur]++;
                }
            }
        }
        return res;
    }
```

**Python:**
```py
    def numSubmatrixSumTarget(self, A, target):
        m, n = len(A), len(A[0])
        for row in A:
            for i in xrange(n - 1):
                row[i + 1] += row[i]
        res = 0
        for i in xrange(n):
            for j in xrange(i, n):
                c = collections.defaultdict(int)
                cur, c[0] = 0, 1
                for k in xrange(m):
                    cur += A[k][j] - (A[k][i - 1] if i > 0 else 0)
                    res += c[cur - target]
                    c[cur] += 1
        return res
```

</p>


### [C++] O(n^3) Simple 1D Subarray target sum applied to 2D array
- Author: PhoenixDD
- Creation Date: Sun Jun 02 2019 12:10:20 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 28 2020 13:35:12 GMT+0800 (Singapore Standard Time)

<p>
**Explaination:**
Check out how to solve for 1D array `560. Subarray Sum Equals K` in O(n) time.

The solution for 1D array uses running prefix sum.
* We know that to get sum of a subarray `[j,i]` we can calculate using `SUM[0,i] - SUM [0,j-1]`.
* If this calculation = target then we have a subarray who\'s sum equals target and ends at `i`.
* Hence at any point `i` we need to find number of occurence of `runningSum - target` since `x + target = runningSum`.
* We start moving from 1st element to the last and keep adding the value to running sum.
* We also keep a hash map maintaining number of such sums occured.
* Thus at any point `i` we have sums from [0,i) stored in hashmap where `i` ranges from `0` to `i` excluding `i`. eg: at `i=3`, we have `SUM[0,0`] , `SUM[0,1]` and `SUM[0,2]`.
* At each index `i` we can then query the hashmap to find out number of occurences of `runningSum-target` to get number of subarrays ending at `i`.

Now since we know how to find number of subarrays who\'s sum equals target for 1D array.
We can convert a 2D matrix of values to 1D matrix whose values equal to that of 2D. Such that they have combination of all rows and apply the same technique.
eg: [[1,2,3,4,5],[2,6,7,8,9]]

We can break this down to 1 2x5 matrix and 2 1x5 matrix
Thus the 1D value matrices are:
* We start row wise
* 1st 1x5 matrix is matrix[0] itself [1,2,3,4,5].
* We then add second row to previous 1D matrix and get [3,8,10,12,14].
* We then move on to make matrices with 2nd row of the original matrix as their first row.
* Since 2nd row is the last we end up with last 1D matrix of [2,6,7,8,9].

We use the same technique for each 1D matrix created in above steps and keep adding the result for these individual 1D arrays and return that as the result in the end.

If you notice in the case of 1D array you get results for subarrays of all lengths starting from any point.
so you are checking rectangles of 1x1, 1x2, 1x3 ..... upto length.
When you apply this to a 2D->1D array at each `i` you are checking rectangles of all sizes starting from `i`.
which looks like ( (1x1, 1x2 ...upto length of row then 2x1, 2x2 ...upto length of a row) .... upto length of columns) for each row in original matrix as their 1st row, hence the result includes all combinations of submatrices.

**Note:** You can make this a little bit faster by sacrificing space and precalculating all prefix sums in the 2D matrix and storing them as in `304. Range Sum Query 2D - Immutable` so that we have a constant lookup for calculating sums, this way we avoid recalculating sums of overlapping rows.

**Solution:**
```c++
class Solution {
public:
    int result=0,target;
    unordered_map<int,int> map;
    void get_result(vector<int>& nums)                          //Get number of subarrays that sum to target.
    {
        int sum=0;
        map.clear();
        map[0]++;
        for(int &i:nums)
        {
            sum+=i;
            result+=map[sum-target];       //get number of subarrays who\'s sum equals target and end at i and add result to global result.
            map[sum]++;                    //Add the occurence of running sum to map.
        }
    }
    int numSubmatrixSumTarget(vector<vector<int>>& matrix, int target) 
    {
        this->target=target;
        vector<int> row(matrix[0].size());
        for(int i=0;i<matrix.size();i++)                    //Convert 2D array to 1D by row.
        {
            fill(row.begin(),row.end(),0);                  //Clear vector to start the row with i as starting row.
            for(int j=i;j<matrix.size();j++)
            {
                for(int x=0;x<matrix[0].size();x++)         //Add next row
                    row[x]+=matrix[j][x];
                get_result(row);
            }
        }
        return result;
    }
};
```
</p>


### Simple Python DP solution
- Author: otoc
- Creation Date: Sat Jul 27 2019 11:47:14 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Dec 07 2019 07:44:49 GMT+0800 (Singapore Standard Time)

<p>
Please see and vote for my solution for these similar problems.
[560. Subarray Sum Equals K](https://leetcode.com/problems/subarray-sum-equals-k/discuss/344431/Simple-Python-DP-solution)
[974. Subarray Sums Divisible by K](https://leetcode.com/problems/subarray-sums-divisible-by-k/discuss/344436/Simple-Python-DP-solution)
[325. Maximum Size Subarray Sum Equals k](https://leetcode.com/problems/maximum-size-subarray-sum-equals-k/discuss/344432/Simple-Python-DP-solution)
[1074. Number of Submatrices That Sum to Target](https://leetcode.com/problems/number-of-submatrices-that-sum-to-target/discuss/344440/Simple-Python-DP-solution)
[363. Max Sum of Rectangle No Larger Than K](https://leetcode.com/problems/max-sum-of-rectangle-no-larger-than-k/discuss/445540/Python-bisect-solution-(960ms-beat-71.25))

For each row, calculate the prefix sum. For each pair of columns, calculate the sum of rows. 
Now this problem is changed to problem 560 Subarray Sum Equals K.
```
    def numSubmatrixSumTarget(self, matrix: List[List[int]], target: int) -> int:
        m, n = len(matrix), len(matrix[0])
        for x in range(m):
            for y in range(n - 1):
                matrix[x][y+1] += matrix[x][y]
        res = 0
        for y1 in range(n):
            for y2 in range(y1, n):
                preSums = {0: 1}
                s = 0
                for x in range(m):
                    s += matrix[x][y2] - (matrix[x][y1-1] if y1 > 0 else 0)
                    res += preSums.get(s - target, 0)
                    preSums[s] = preSums.get(s, 0) + 1
        return res
```
</p>


