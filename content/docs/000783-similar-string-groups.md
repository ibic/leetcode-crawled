---
title: "Similar String Groups"
weight: 783
#id: "similar-string-groups"
---
## Description
<div class="description">
<p>Two strings <code>X</code>&nbsp;and <code>Y</code>&nbsp;are similar if we can swap two letters (in different positions) of <code>X</code>, so that&nbsp;it equals <code>Y</code>. Also two strings <code>X</code> and <code>Y</code> are similar if they are equal.</p>

<p>For example, <code>&quot;tars&quot;</code>&nbsp;and <code>&quot;rats&quot;</code>&nbsp;are similar (swapping at positions <code>0</code> and <code>2</code>), and <code>&quot;rats&quot;</code> and <code>&quot;arts&quot;</code> are similar, but <code>&quot;star&quot;</code> is not similar to <code>&quot;tars&quot;</code>, <code>&quot;rats&quot;</code>, or <code>&quot;arts&quot;</code>.</p>

<p>Together, these form two connected groups by similarity: <code>{&quot;tars&quot;, &quot;rats&quot;, &quot;arts&quot;}</code> and <code>{&quot;star&quot;}</code>.&nbsp; Notice that <code>&quot;tars&quot;</code> and <code>&quot;arts&quot;</code> are in the same group even though they are not similar.&nbsp; Formally, each group is such that a word is in the group if and only if it is similar to at least one other word in the group.</p>

<p>We are given a list <code>A</code> of strings.&nbsp; Every string in <code>A</code> is an anagram of every other string in <code>A</code>.&nbsp; How many groups are there?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> A = ["tars","rats","arts","star"]
<strong>Output:</strong> 2
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= A.length &lt;= 2000</code></li>
	<li><code>1 &lt;= A[i].length &lt;= 1000</code></li>
	<li><code>A.length * A[i].length &lt;= 20000</code></li>
	<li>All words in <code>A</code>&nbsp;consist of lowercase letters only.</li>
	<li>All words in <code>A</code> have the same length and are anagrams of each other.</li>
	<li>The judging time limit has been increased for this question.</li>
</ul>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Union Find (union-find)
- Graph (graph)

## Companies
- Facebook - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Snapchat - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

---
#### Approach #1: Piecewise [Accepted]

**Intuition**

Let `W = A[0].length`.  It is clear that we can determine in $$O(W)$$ time, whether two words from `A` are similar.

One attempt is a standard kind of brute force: for each pair of words, let's draw an edge between these words if they are similar.  We can do this in $$O(N^2 W)$$ time.  After, finding the connected components can be done in $$O(N^2)$$ time naively (each node may have up to $$N-1$$ edges), (or $$O(N)$$ with a union-find structure.)  The total complexity is $$O(N^2 W)$$.

Another attempt is to enumerate all neighbors of a word.  A `word` has up to $$\binom{W}{2}$$ neighbors, and if that `neighbor` is itself a given word, we know that `word` and `neighbor` are connected by an edge.  In this way, we can build the graph in $$O(N W^3)$$ time, and again take $$O(N^2)$$ or $$O(N)$$ time to analyze the number of connected components.

One insight is that between these two approaches, we can choose which approach works better.  If we have very few words, we want to use the first approach; if we have very short words, we want to use the second approach.  We'll piecewise add these two approaches (with complexity $$O(N^2 W)$$ and $$O(N W^3)$$), to create an approach with $$O(NW\min(N, W^2))$$ complexity.


**Algorithm**

We will build some underlying graph with `N` nodes, where nodes `i` and `j` are connected if and only if `A[i]` is similar to `A[j]`, then look at the number of connected components.

There are a few challenges involved in this problem, but each challenge is relatively straightforward.  

* Use a helper function `similar(word1, word2)` that is `true` if and only if two given words are similar.

* Enumerate all neighbors of a word, and discover when it is equal to a given word.

* Use either a union-find structure or a depth-first search, to calculate the number of connected components of the underlying graph.  We've showcased a union-find structure in this solution, with notes of a depth-first search in the comments.

For more details, see the implementations below.

<iframe src="https://leetcode.com/playground/9TzgDukt/shared" frameBorder="0" width="100%" height="500" name="9TzgDukt"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(NW \min(N, W^2))$$, where $$N$$ is the length of `A`, and $$W$$ is the length of each given word.

* Space Complexity:  $$O(NW^3)$$.  If $$N < W^2$$, the space complexity is $$O(N)$$.  Otherwise, the space complexity is $$O(NW^3)$$: for each of $$NW^2$$ neighbors we store a word of length $$W$$.  (Plus, we store $$O(NW^2)$$ node indices ("buckets") which is dominated by the $$O(NW^3)$$ term.)  Because $$W^2 <= N$$ in this case, we could also write the space complexity as $$O(N^2 W)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Short C++ solution at 220ms, using disjoint set
- Author: mzchen
- Creation Date: Sun May 20 2018 12:21:04 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 18:19:11 GMT+0800 (Singapore Standard Time)

<p>
```
bool similar(string &a, string &b) {
    int n = 0;
    for (int i = 0; i < a.size(); i++)
        if (a[i] != b[i] && ++n > 2)
            return false;
    return true;
}

int numSimilarGroups(vector<string>& A) {
    disjoint_set ds(A.size());
    for (int i = 0; i < A.size(); i++)
        for (int j = i + 1; j < A.size(); j++)
            if (similar(A[i], A[j]))
                ds.join(i, j);
    return ds.size();
}
```

The standard disjoint_set class is defined below:
```
class disjoint_set {
    vector<int> v;
    int sz;
public:
    disjoint_set(int n) {
        makeset(n);
    }

    void makeset(int n) {
        v.resize(n);
        iota(v.begin(), v.end(), 0);
        sz = n;
    }

    int find(int i) {
        if (i != v[i])
            v[i] = find(v[i]);
        return v[i];
    }
    
    void join(int i, int j) {
        int ri = find(i), rj = find(j);
        if (ri != rj) {
            v[ri] = rj;
            sz--;
        }
    }
    
    int size() {
        return sz;
    }
};
```
</p>


### Simple Java Solution using DFS
- Author: ashish53v
- Creation Date: Sun May 20 2018 11:09:32 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 20 2018 11:44:44 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int numSimilarGroups(String[] A) {
        if(A.length < 2) return A.length;
        int res = 0;
        for(int i=0;i<A.length;i++){
            if(A[i] == null) continue;
            String str = A[i];
            A[i] = null;
            res++;
            dfs(A,str);
        }
        return res;
    }
    public void dfs(String[] arr,String str){
        for(int i=0;i<arr.length;i++){
            if(arr[i] == null) continue;
            if(helper(str,arr[i])){// both string str and arr[i] belong in same group
                String s = arr[i];
                arr[i] = null;
                dfs(arr,s);
            }
        }
    }
    public boolean helper(String s,String t){
        int res = 0, i = 0;
        while(res <= 2 && i < s.length()){
            if(s.charAt(i) != t.charAt(i)) res++;
            i++;
        }
        return res == 2;
    }
}
```
</p>


### python O(mn * min(m, n)) solution, union find
- Author: Jason003
- Creation Date: Fri Mar 15 2019 21:22:19 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Mar 15 2019 21:22:19 GMT+0800 (Singapore Standard Time)

<p>
```python
class Solution:
    def numSimilarGroups(self, A: List[str]) -> int:
        m, n = len(A[0]), len(A)
        A = set(A)
        p, res = {s : s for s in A}, len(A) # p: parent
        def find(s):
            while s != p[s]: 
                p[s] = p[p[s]]
                s = p[s]
            return s
        def judge(a, b):
            cnt = 0
            for i, j in zip(a, b):
                if i != j:
                    if cnt < 2: cnt += 1
                    else: return False
            return cnt == 2
        if m > n:
            for a, b in itertools.combinations(A, 2): # O(m * n ^ 2)
                if judge(a, b):
                    ra, rb = find(a), find(b)
                    if ra != rb: res, p[ra] = res - 1, rb
        else: 
            for a in A: # O(n * m ^ 2)
                for i, j in itertools.combinations(range(m), 2):
                    b = a[:i] + a[j] + a[i + 1: j] + a[i] + a[j + 1:]
                    if b in A: 
                        ra, rb = find(a), find(b)
                        if ra != rb: res, p[ra] = res - 1, rb
        return res
```
</p>


