---
title: "Super Ugly Number"
weight: 296
#id: "super-ugly-number"
---
## Description
<div class="description">
<p>Write a program to find the <code>n<sup>th</sup></code> super ugly number.</p>

<p>Super ugly numbers are positive numbers whose all prime factors are in the given prime list <code>primes</code> of size <code>k</code>.</p>

<p><b>Example:</b></p>

<pre>
<b>Input:</b> n = 12, <code>primes</code> = <code>[2,7,13,19]</code>
<b>Output:</b> 32 
<strong>Explanation: </strong><code>[1,2,4,7,8,13,14,16,19,26,28,32] </code>is the sequence of the first 12 
             super ugly numbers given <code>primes</code> = <code>[2,7,13,19]</code> of size 4.</pre>

<p><b>Note:</b></p>

<ul>
	<li><code>1</code> is a super ugly number for any given <code>primes</code>.</li>
	<li>The given numbers in <code>primes</code> are in ascending order.</li>
	<li>0 &lt; <code>k</code> &le; 100, 0 &lt; <code>n</code> &le; 10<sup>6</sup>, 0 &lt; <code>primes[i]</code> &lt; 1000.</li>
	<li>The n<sup>th</sup> super ugly number is guaranteed to fit in a 32-bit signed integer.</li>
</ul>

</div>

## Tags
- Math (math)
- Heap (heap)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java three methods, 23ms, 36 ms, 58ms(with heap), performance explained
- Author: crackAlgo
- Creation Date: Thu Jan 21 2016 03:43:29 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 05:53:34 GMT+0800 (Singapore Standard Time)

<p>
Basic idea is same as ugly number II, new ugly number is generated by multiplying a prime with previous generated ugly number. One catch is need to remove duplicate

Let's start with the **common solution** from ugly number II **36 ms, Theoretically O(kN)**

    public int nthSuperUglyNumberI(int n, int[] primes) {
        int[] ugly = new int[n];
        int[] idx = new int[primes.length];
    
        ugly[0] = 1;
        for (int i = 1; i < n; i++) {
            //find next
            ugly[i] = Integer.MAX_VALUE;
            for (int j = 0; j < primes.length; j++)
                ugly[i] = Math.min(ugly[i], primes[j] * ugly[idx[j]]);
            
            //slip duplicate
            for (int j = 0; j < primes.length; j++) {
                while (primes[j] * ugly[idx[j]] <= ugly[i]) idx[j]++;
            }
        }
    
        return ugly[n - 1];
    }


If you look at the above solution, it has **redundant multiplication** can be avoided, and also two for loops can be consolidated into one. This **trade-off space for speed. 23 ms, Theoretically O(kN)**

    public int nthSuperUglyNumber(int n, int[] primes) {
            int[] ugly = new int[n];
            int[] idx = new int[primes.length];
            int[] val = new int[primes.length];
            Arrays.fill(val, 1);
    
            int next = 1;
            for (int i = 0; i < n; i++) {
                ugly[i] = next;
                
                next = Integer.MAX_VALUE;
                for (int j = 0; j < primes.length; j++) {
                    //skip duplicate and avoid extra multiplication
                    if (val[j] == ugly[i]) val[j] = ugly[idx[j]++] * primes[j];
                    //find next ugly number
                    next = Math.min(next, val[j]);
                }
            }
    
            return ugly[n - 1];
        }

Can we do better? Theoretically yes, by keep the one candidates for each prime in a **heap**, it can improve the theoretical bound to **O( log(k)N )**, but in reality it's **58 ms**. I think it's the result of using higher level object instead of primitive. Can be improved by writing an **index heap** (http://algs4.cs.princeton.edu/24pq/IndexMinPQ.java.html)


    public int nthSuperUglyNumberHeap(int n, int[] primes) {
        int[] ugly = new int[n];
    
        PriorityQueue<Num> pq = new PriorityQueue<>();
        for (int i = 0; i < primes.length; i++) pq.add(new Num(primes[i], 1, primes[i]));
        ugly[0] = 1;
    
        for (int i = 1; i < n; i++) {
            ugly[i] = pq.peek().val;
            while (pq.peek().val == ugly[i]) {
                Num nxt = pq.poll();
                pq.add(new Num(nxt.p * ugly[nxt.idx], nxt.idx + 1, nxt.p));
            }
        }
    
        return ugly[n - 1];
    }
    
    private class Num implements Comparable<Num> {
        int val;
        int idx;
        int p;
    
        public Num(int val, int idx, int p) {
            this.val = val;
            this.idx = idx;
            this.p = p;
        }
    
        @Override
        public int compareTo(Num that) {
            return this.val - that.val;
        }
    }
</p>


### 7 line consice O(kn) c++ solution
- Author: zjh08177
- Creation Date: Fri Dec 04 2015 06:01:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 15:08:31 GMT+0800 (Singapore Standard Time)

<p>
 Keep k pointers and update them in each iteration.   Time complexity is O(kn).

    int nthSuperUglyNumber(int n, vector<int>& primes) {
            vector<int> index(primes.size(), 0), ugly(n, INT_MAX);
            ugly[0]=1;
            for(int i=1; i<n; i++){
                for(int j=0; j<primes.size(); j++) ugly[i]=min(ugly[i],ugly[index[j]]*primes[j]);
                for(int j=0; j<primes.size(); j++) index[j]+=(ugly[i]==ugly[index[j]]*primes[j]);
            }
            return ugly[n-1];
    }
</p>


### Python, generators on a heap
- Author: StefanPochmann
- Creation Date: Thu Dec 03 2015 11:19:51 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Dec 03 2015 11:19:51 GMT+0800 (Singapore Standard Time)

<p>
**Solution 1** ... ~550 ms (updated July 2016, originally was ~1570 ms)

Using generators and `heapq.merge`. Too bad there's no `itertools.unique`.

    def nthSuperUglyNumber(self, n, primes):
        uglies = [1]
        def gen(prime):
            for ugly in uglies:
                yield ugly * prime
        merged = heapq.merge(*map(gen, primes))
        while len(uglies) < n:
            ugly = next(merged)
            if ugly != uglies[-1]:
                uglies.append(ugly)
        return uglies[-1]

---

**Solution 2** ... ~500 ms (updated July 2016, originally was ~1400 ms)

Same thing done differently and it's a bit faster.

    def nthSuperUglyNumber(self, n, primes):
        uglies = [1]
        merged = heapq.merge(*map(lambda p: (u*p for u in uglies), primes))
        uniqed = (u for u, _ in itertools.groupby(merged))
        map(uglies.append, itertools.islice(uniqed, n-1))
        return uglies[-1]
</p>


