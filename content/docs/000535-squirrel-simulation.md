---
title: "Squirrel Simulation"
weight: 535
#id: "squirrel-simulation"
---
## Description
<div class="description">
There&#39;s a tree, a squirrel, and several nuts. Positions are represented by the cells in a 2D grid. Your goal is to find the <b>minimal</b> distance for the squirrel to collect all the nuts and put them under the tree one by one. The squirrel can only take at most <b>one nut</b> at one time and can move in four directions - up, down, left and right, to the adjacent cell. The distance is represented by the number of moves.
<p><b>Example 1:</b></p>

<pre>
<b>Input:</b> 
Height : 5
Width : 7
Tree position : [2,2]
Squirrel : [4,4]
Nuts : [[3,0], [2,5]]
<b>Output:</b> 12
<b>Explanation:</b>
<img src="https://assets.leetcode.com/uploads/2018/10/22/squirrel_simulation.png" style="width: 40%;" />​​​​​
</pre>

<p><b>Note:</b></p>

<ol>
	<li>All given positions won&#39;t overlap.</li>
	<li>The squirrel can take at most one nut at one time.</li>
	<li>The given positions of nuts have no order.</li>
	<li>Height and width are positive integers. 3 &lt;= height * width &lt;= 10,000.</li>
	<li>The given positions contain at least one nut, only one tree and one squirrel.</li>
</ol>

</div>

## Tags
- Math (math)

## Companies
- Square - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Simple Solution

**Algorithm**

We know, the distance between any two points(tree, squirrel, nut) is given by the absolute difference between the corresponding x-coordinates and the corresponding y-coordinates. 

Now, in order to determine the required minimum distance, we need to observe a few points. Firstly, the order in which the nuts are picked doesn't affect the final result, except one of the nuts which needs to be visited first from the squirrel's starting position. For the rest of the nuts, it is mandatory to go from the tree to the nut and then come back as well. 

For the first visited nut, the saving obtained, given by $$d$$, is the difference between the distance between the tree and the current nut & the distance between the current nut and the squirrel. This is because for this nut, we need not travel from the tree to the nut, but need to travel an additional distance from the squirrel's original position to the nut.

While traversing over the $$nuts$$ array and adding the to-and-fro distance, we find out the saving, $$d$$, which can be obtained if the squirrel goes to the current nut first. Out of all the nuts, we find out the nut which maximizes the saving and then deduct this maximum saving from the sum total of the to-and-fro distance of all the nuts.

Note that the first nut to be picked needs not necessarily be the nut closest to the squirrel's start point, but it's the one which maximizes the savings.

![Squirrel_Nuts](../Figures/573_Squirrel.PNG)
{:align="center"}

<iframe src="https://leetcode.com/playground/P6EvMXb6/shared" frameBorder="0" width="100%" height="276" name="P6EvMXb6"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. We need to traverse over the whole $$nuts$$ array once. $$n$$ refers to the size of $$nuts$$ array.

* Space complexity : $$O(1)$$. Constant space is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 5-liner O(|nuts|) Time O(1) Space
- Author: compton_scatter
- Creation Date: Sun May 07 2017 11:02:18 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 16:34:12 GMT+0800 (Singapore Standard Time)

<p>
Proof: Let the minimum distance from each nut to the tree be `a_1, ..., a_n` and let the minimum distance from each nut to the initial squirrel position be `b_1, ..., b_n`. Note that the minimum distance between two positions in the matrix is determined by their Manhattan distance.

Then, if the squirrel were to start at the tree, then the minimum total distance required to collect all the nuts is `2a_1 + ... + 2a_n`. However, since the squirrel starts elsewhere, we just need to substitute one of the `2a_i` terms with `a_i + b_i`. Or equivalently, we replace one of the `a_i` terms in the sum with `b_i`. To minimize the total sum value at the end, we choose `i` that maximizes `a_i - b_i`.

```
public int minDistance(int height, int width, int[] tree, int[] squirrel, int[][] nuts) {
    int sum = 0, maxDiff = Integer.MIN_VALUE;
    for (int[] nut : nuts) {
        int dist = Math.abs(tree[0] - nut[0]) + Math.abs(tree[1] - nut[1]);
        sum += 2*dist;
        maxDiff = Math.max(maxDiff, dist - Math.abs(squirrel[0] - nut[0]) - Math.abs(squirrel[1] - nut[1]));
    }
    return sum - maxDiff;
}
```
</p>


### Java Solution, O(n), Manhattan distance
- Author: shawngao
- Creation Date: Sun May 07 2017 11:09:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 07 2017 11:09:23 GMT+0800 (Singapore Standard Time)

<p>
Time complexity O(n), n = number of nuts.
```
public class Solution {
    public int minDistance(int height, int width, int[] tree, int[] squirrel, int[][] nuts) {
        int n = nuts.length;
        int[] nutToTree = new int[n];
        int[] nutToSquirrel = new int[n];
        
        int sum = 0;
        for (int i = 0; i < n; i++) {
            nutToTree[i] = Math.abs(nuts[i][0] - tree[0]) + Math.abs(nuts[i][1] - tree[1]);
            sum += nutToTree[i] * 2;
            nutToSquirrel[i] = Math.abs(nuts[i][0] - squirrel[0]) + Math.abs(nuts[i][1] - squirrel[1]);
        }
        
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            int dist = sum + nutToSquirrel[i] - nutToTree[i];
            min = Math.min(min, dist);
        }
        
        return min;
    }
}
```
</p>


### Python, Straightforward with Explanation
- Author: awice
- Creation Date: Sun May 07 2017 11:16:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 07 2017 11:16:00 GMT+0800 (Singapore Standard Time)

<p>
Let ```D``` be the taxicab distance, ```D(P, Q) = abs(Px - Qx) + abs(Py - Qy)```.

Suppose the squirrel is already at the tree.  The distance of his path will be:
```S = D(tree, nut_1) + D(nut_1, tree) + D(tree, nut_2) + D(nut_2, tree) + D(tree, nut_3) + ...```
and so
```S = sum_{i=1..N} 2 * D(tree, nut_i)```.

At the beginning, the squirrel goes to some ```nut_k```, then to the tree, then does the usual path described above except doesn't visit ```nut_k```.  Thus, his path has distance:  
```D(squirrel, nut_k) + D(nut_k, tree) + S - (2 * D(tree, nut_k))```
which equals
```S + D(squirrel, nut_k) - D(nut_k, tree)```.

```
def minDistance(self, height, width, tree, squirrel, nuts):
    def taxi(p, q):
        return abs(p[0] - q[0]) + abs(p[1] - q[1])
    
    S = sum(2 * taxi(tree, nut) for nut in nuts)
    return min(S + taxi(squirrel, nut) - taxi(nut, tree) for nut in nuts)
```
</p>


