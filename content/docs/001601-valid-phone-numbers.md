---
title: "Valid Phone Numbers"
weight: 1601
#id: "valid-phone-numbers"
---
## Description
<div class="description">
<p>Given a text file <code>file.txt</code> that contains list of phone numbers (one per line), write a one liner bash script to print all valid phone numbers.</p>

<p>You may assume that a valid phone number must appear in one of the following two formats: (xxx) xxx-xxxx or xxx-xxx-xxxx. (x means a digit)</p>

<p>You may also assume each line in the text file must not contain leading or trailing white spaces.</p>

<p><strong>Example:</strong></p>

<p>Assume that <code>file.txt</code> has the following content:</p>

<pre>
987-123-4567
123 456 7890
(123) 456-7890
</pre>

<p>Your script should output the following valid phone numbers:</p>

<pre>
987-123-4567
(123) 456-7890
</pre>

</div>

## Tags


## Companies
- Apple - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (bash)
```bash
# Read from the file file.txt and output all valid phone numbers to stdout.
re1="^\([0-9]{3}\) [0-9]{3}-[0-9]{4}$"
re2="^[0-9]{3}-[0-9]{3}-[0-9]{4}$"
while read -r line
do
    if [[ $line =~ $re1 || $line =~ $re2 ]]
		then
        echo "$line"
    fi
done < file.txt

```

## Top Discussions
### Three different solutions using grep, sed, and awk
- Author: luangong
- Creation Date: Thu Mar 26 2015 18:33:27 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 05:02:56 GMT+0800 (Singapore Standard Time)

<p>
Using `grep`:

    grep -P '^(\d{3}-|\(\d{3}\) )\d{3}-\d{4}$' file.txt

Using `sed`:

    sed -n -r '/^([0-9]{3}-|\([0-9]{3}\) )[0-9]{3}-[0-9]{4}$/p' file.txt

Using `awk`:

    awk '/^([0-9]{3}-|\([0-9]{3}\) )[0-9]{3}-[0-9]{4}$/' file.txt
</p>


### Grep -e solution with detailed explanation, good for those new to regex
- Author: steve.j.sun
- Creation Date: Sun Jul 03 2016 01:26:06 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 24 2018 11:29:37 GMT+0800 (Singapore Standard Time)

<p>
    grep -e '\(^[0-9]\{3\}-[0-9]\{3\}-[0-9]\{4\}$\)' -e '\(^([0-9]\{3\})[ ]\{1\}[0-9]\{3\}-\([0-9]\{4\}\)$\)'  file.txt

1. In Bash, we use `\` to escape next one trailing character;
2. `^` is used to denote the beginning of a line
3. `$` is used to denote the end of a line
4. `{M}` is used to denote to match exactly `M` times of the previous occurence/regex
5. `(...)` is used to group pattern/regex together

Back to this problem: it requires us to match two patterns, for better readability, I used -e and separate the two patterns into two regexes, the first one matches this case: `xxx-xxx-xxxx` and the second one matches this case: `(xxx) xxx-xxxx`

Please vote this post up if you find it helpful for your understanding!

Cheers!
</p>


### Valid Numbers
- Author: arunadang
- Creation Date: Sun May 31 2020 06:25:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sun May 31 2020 06:25:26 GMT+0800 (Singapore Standard Time)

<p>
```
Please find the solution : 
grep -P \'^(\d{3}-|\(\d{3}\) )\d{3}-\d{4}$\' file.txt

Explaination : 
grep -P \u2018^()

What in these parentheses should come in the beginning.
grep -P \u2018^(\d{3}-
\d{3}  - means 3 digits should come in these parenthesis.
Grep -P \u2018^(\d{3}-|\(\d{3}\) )\u2019
| = Means Or
\(\d{3}\) )\u2019 = \d{3}\ means it should contain 3 digit  and a space

grep -P \u2018^(\d{3}-|\(\d{3}\) )\d{3}-\d{4}\u2019
\d{3}-\d{4} = means 3 digits and 4 digits


Reference : https://www.***.org/regular-expression-grep/
</p>


