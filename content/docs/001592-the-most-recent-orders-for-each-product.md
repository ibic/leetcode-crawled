---
title: "The Most Recent Orders for Each Product"
weight: 1592
#id: "the-most-recent-orders-for-each-product"
---
## Description
<div class="description">
<p>Table: <code>Customers</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| customer_id   | int     |
| name          | varchar |
+---------------+---------+
customer_id is the primary key for this table.
This table contains information about the customers.
</pre>

<p>&nbsp;</p>

<p>Table: <code>Orders</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| order_id      | int     |
| order_date    | date    |
| customer_id   | int     |
| product_id    | int     |
+---------------+---------+
order_id is the primary key for this table.
This table contains information about the orders made by customer_id.
There will be no product ordered by the same user <strong>more than once</strong> in one day.</pre>

<p>&nbsp;</p>

<p>Table: <code>Products</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| product_id    | int     |
| product_name  | varchar |
| price         | int     |
+---------------+---------+
product_id is the primary key for this table.
This table contains information about the Products.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find the most recent&nbsp;order(s) of each product.</p>

<p>Return the result table sorted by <code>product_name</code> in <strong>ascending</strong> order and in case of a tie by the <code>product_id</code> in <strong>ascending</strong> order. If there still a tie, order them by the <code>order_id</code> in <strong>ascending</strong> order.</p>

<p>The query result format is in the following example:</p>

<pre>
<code>Customers</code>
+-------------+-----------+
| customer_id | name      |
+-------------+-----------+
| 1           | Winston   |
| 2           | Jonathan  |
| 3           | Annabelle |
| 4           | Marwan    |
| 5           | Khaled    |
+-------------+-----------+

<code>Orders</code>
+----------+------------+-------------+------------+
| order_id | order_date | customer_id | product_id |
+----------+------------+-------------+------------+
| 1        | 2020-07-31 | 1           | 1          |
| 2        | 2020-07-30 | 2           | 2          |
| 3        | 2020-08-29 | 3           | 3          |
| 4        | 2020-07-29 | 4           | 1          |
| 5        | 2020-06-10 | 1           | 2          |
| 6        | 2020-08-01 | 2           | 1          |
| 7        | 2020-08-01 | 3           | 1          |
| 8        | 2020-08-03 | 1           | 2          |
| 9        | 2020-08-07 | 2           | 3          |
| 10       | 2020-07-15 | 1           | 2          |
+----------+------------+-------------+------------+

<code>Products</code>
+------------+--------------+-------+
| product_id | product_name | price |
+------------+--------------+-------+
| 1          | keyboard     | 120   |
| 2          | mouse        | 80    |
| 3          | screen       | 600   |
| 4          | hard disk    | 450   |
+------------+--------------+-------+

Result table:
+--------------+------------+----------+------------+
| product_name | product_id | order_id | order_date |
+--------------+------------+----------+------------+
| keyboard     | 1          | 6        | 2020-08-01 |
| keyboard     | 1          | 7        | 2020-08-01 |
| mouse        | 2          | 8        | 2020-08-03 |
| screen       | 3          | 3        | 2020-08-29 |
+--------------+------------+----------+------------+
keyboard&#39;s most recent order is in 2020-08-01, it was ordered two times this day.
mouse&#39;s most recent order is in 2020-08-03, it was ordered only once this day.
screen&#39;s most recent order is in 2020-08-29, it was ordered only once this day.
The hard disk was never ordered and we don&#39;t include it in the result table.
</pre>
</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL subquery
- Author: lianj
- Creation Date: Fri Aug 14 2020 03:31:14 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 14 2020 03:31:14 GMT+0800 (Singapore Standard Time)

<p>
```
select b.product_name, a.product_id, a.order_id, a.order_date from Orders a
join Products b
on a.product_id = b.product_id
where (a.product_id, a.order_date) in (
select product_id, max(order_date) as order_date
from Orders
group by product_id)
order by b.product_name, a.product_id, a.order_id
```

logistics: 
* filter out the product_id and the latest date of each of the product
* filter out the records from original orders table
* join the products table to get required information
</p>


### MySQL simple solution with rank()
- Author: vwang0
- Creation Date: Fri Aug 14 2020 11:30:58 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 14 2020 11:30:58 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT product_name, product_id, order_id, order_date
FROM (
    SELECT product_name, P.product_id, order_id, order_date, RANK() OVER (PARTITION BY product_name ORDER BY order_date DESC) rnk
    FROM Orders O
    JOIN Products P
    On O.product_id = P.product_id
) temp
WHERE rnk = 1
ORDER BY product_name, product_id, order_id
```
</p>


### mysql window funtion rank() over()
- Author: eleganthedgehog
- Creation Date: Wed Sep 02 2020 02:25:10 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 02 2020 02:25:10 GMT+0800 (Singapore Standard Time)

<p>
```
select product_name,product_id, order_id, order_date from(
select product_name, product_id,order_id, order_date,
rank() over (partition by product_name, product_id order by order_date desc) as number
from products a join orders b using(product_id)
) tmp1
where number=1
order by 1 asc, product_id asc, order_id asc
```
</p>


