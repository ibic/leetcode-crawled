---
title: "Valid Mountain Array"
weight: 891
#id: "valid-mountain-array"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of integers, return <code>true</code> if and only if it is a <em>valid mountain array</em>.</p>

<p>Recall that A is a mountain array if and only if:</p>

<ul>
	<li><code>A.length &gt;= 3</code></li>
	<li>There exists some <code>i</code> with&nbsp;<code>0 &lt; i&nbsp;&lt; A.length - 1</code>&nbsp;such that:
	<ul>
		<li><code>A[0] &lt; A[1] &lt; ... A[i-1] &lt; A[i] </code></li>
		<li><code>A[i] &gt; A[i+1] &gt; ... &gt; A[A.length - 1]</code></li>
	</ul>
	</li>
</ul>

<br>
<img src="https://assets.leetcode.com/uploads/2019/10/20/hint_valid_mountain_array.png" width="500" />

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[2,1]</span>
<strong>Output: </strong><span id="example-output-1">false</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">[3,5,5]</span>
<strong>Output: </strong><span id="example-output-2">false</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">[0,3,2,1]</span>
<strong>Output: </strong><span id="example-output-3">true</span></pre>
</div>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>0 &lt;= A.length &lt;= 10000</code></li>
	<li><code>0 &lt;= A[i] &lt;= 10000&nbsp;</code></li>
</ol>

<div>
<p>&nbsp;</p>

<div>
<div>&nbsp;</div>
</div>
</div>
</div>

## Tags
- Array (array)

## Companies
- Google - 2 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: One Pass

**Intuition**

If we walk along the mountain from left to right, we have to move strictly up, then strictly down.

**Algorithm**

Let's walk up from left to right until we can't: that has to be the peak.  We should ensure the peak is not the first or last element.  Then, we walk down.  If we reach the end, the array is valid, otherwise its not.

<iframe src="https://leetcode.com/playground/RhPXojUe/shared" frameBorder="0" width="100%" height="395" name="RhPXojUe"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Climb Mountain
- Author: lee215
- Creation Date: Sun Nov 18 2018 12:02:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 18 2018 12:02:41 GMT+0800 (Singapore Standard Time)

<p>
Two people climb from left and from right separately.
If they are climbing the same mountain,
they will meet at the same point.


**C++:**
```
    bool validMountainArray(vector<int>& A) {
        int n = A.size(), i = 0, j = n - 1;
        while (i + 1 < n && A[i] < A[i + 1]) i++;
        while (j > 0 && A[j - 1] > A[j]) j--;
        return i > 0 && i == j && j < n - 1;
    }
```

**Java:**
```
    public boolean validMountainArray(int[] A) {
        int n = A.length, i = 0, j = n - 1;
        while (i + 1 < n && A[i] < A[i + 1]) i++;
        while (j > 0 && A[j - 1] > A[j]) j--;
        return i > 0 && i == j && j < n - 1;
    }
```

**Python:**
```
    def validMountainArray(self, A):
        i, j, n = 0, len(A) - 1, len(A)
        while i + 1 < n and A[i] < A[i + 1]: i += 1
        while j > 0 and A[j - 1] > A[j]: j -= 1
        return 0 < i == j < n - 1
```

</p>


### JAVA 100%
- Author: jungthinkr
- Creation Date: Fri May 17 2019 00:56:32 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 17 2019 00:56:32 GMT+0800 (Singapore Standard Time)

<p>
Have two pointers: a start and end pointer. 
Check to see if next element is valid to increment/decrement. 
Ensure that the start and end pointers moved.
```
class Solution {
    public boolean validMountainArray(int[] A) {
      if (A.length < 3) return false;
      int start = 0;
      int end = A.length-1;
      while (start < end) {
        if (A[start+1] > A[start]) {
          start++;
        } else if (A[end-1] > A[end]) { 
          end--;
        } else {
          break;
        }
      }
      return start != 0 && end != A.length-1 && start == end;
    }
}
```
</p>


### C++ Track Peak
- Author: votrubac
- Creation Date: Sun Nov 18 2018 12:23:38 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 18 2018 12:23:38 GMT+0800 (Singapore Standard Time)

<p>
We are ascending until we find the peak, then descending. If we start ascending again after we found the peak, the mountain array is invalid.

The ```&``` operation will produce ```true``` only when the peak has been reached and there is an ascend (both conditions are true).
```
bool validMountainArray(vector<int>& A) {
    if (A.size() < 3 || A[0] > A[1]) return false;
    auto peak = false;
    for (auto i = 1; i < A.size(); ++i) {
        if (peak & A[i - 1] < A[i] || A[i - 1] == A[i]) return false;
        peak = A[i - 1] > A[i];
    }
    return peak;
}
```
</p>


