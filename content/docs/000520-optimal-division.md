---
title: "Optimal Division"
weight: 520
#id: "optimal-division"
---
## Description
<div class="description">
<p>Given a list of <b>positive integers</b>, the adjacent integers will perform the float division. For example, [2,3,4] -> 2 / 3 / 4.</p>

<p>However, you can add any number of parenthesis at any position to change the priority of operations. You should find out how to add parenthesis to get the <b>maximum</b> result, and return the corresponding expression in string format. <b>Your expression should NOT contain redundant parenthesis.</b></p>

<p><b>Example:</b><br />
<pre>
<b>Input:</b> [1000,100,10,2]
<b>Output:</b> "1000/(100/10/2)"
<b>Explanation:</b>
1000/(100/10/2) = 1000/((100/10)/2) = 200
However, the bold parenthesis in "1000/(<b>(</b>100/10<b>)</b>/2)" are redundant, <br/>since they don't influence the operation priority. So you should return "1000/(100/10/2)". 

Other cases:
1000/(100/10)/2 = 50
1000/(100/(10/2)) = 50
1000/100/10/2 = 0.5
1000/100/(10/2) = 2
</pre>
</p>

<p><b>Note:</b>
<ol>
<li>The length of the input array is [1, 10].</li>
<li>Elements in the given array will be in range [2, 1000].</li>
<li>There is only one optimal division for each test case.</li>
</ol>
</p>
</div>

## Tags
- Math (math)
- String (string)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach #1 Brute Force [Accepted]

**Algorithm**

Brute force of this problem is to divide the list into two parts $$left$$ and $$right$$ and call function for these two parts. We will iterate $$i$$ from $$start$$ to $$end$$ so that $$left=(start,i)$$ and $$right=(i+1,end)$$.

$$left$$ and $$right$$ parts return their maximum and minimum value and corresponding strings.

Minimum value can be found by dividing minimum of left by maximum of right i.e. $$minVal=left.min/right.max$$.

Similarly,Maximum value can be found by dividing maximum of left value by minimum of right value. i.e. $$maxVal=left.max/right.min$$.

Now, how to add parenthesis? As associativity of division operator is from left to right i.e. by default left most divide should be done first, we need not have to add paranthesis to the left part, but we must add parenthesis to the right part.

eg- "2/(3/4)" will be formed as leftPart+"/"+"("+rightPart+")", assuming leftPart is "2" and rightPart is"3/4".

One more point, we also don't require parenthesis to right part when it contains single digit.

eg- "2/3", here left part is "2" and right part is "3" (contains single digit) . 2/(3) is not valid.


<iframe src="https://leetcode.com/playground/CAbJyzm4/shared" frameBorder="0" name="CAbJyzm4" width="100%" height="515"></iframe>


**Complexity Analysis**

* Time complexity : $$O(n!)$$. Number of permutations of expression after applying brackets will be in $$O(n!)$$ where $$n$$ is the number of items in the list.

* Space complexity: $$O(n^2)$$. Depth of recursion tree will be $$O(n)$$ and each node contains string of maximum length $$O(n)$$.

---
#### Approach #2 Using Memorization [Accepted]

**Algorithm**

In the above approach we called optimal function recursively for ever $$start$$ and $$end$$. We can notice that there are many redundant calls in the above approach, we can reduce these calls by using memorization to store the result of different function calls. Here, $$memo$$ array is used for this purpose.

<iframe src="https://leetcode.com/playground/xFgr7Cpd/shared" frameBorder="0" name="xFgr7Cpd" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^3)$$. $$memo$$ array of size $$n^2$$ is filled and filling of each cell of the $$memo$$ array takes $$O(n)$$ time.

* Space complexity : $$O(n^3)$$. $$memo$$ array of size $$n^2$$ where each cell of array contains string of length $$O(n)$$.

---
#### Approach #3 Using some Math [Accepted]

**Algorithm**

Using some simple math we can find the easy solution of this problem. Consider the input in the form of [a,b,c,d], now we have to set priority of
operations to maximize a/b/c/d. We know that to maximize fraction $$p/q$$, $$q$$(denominator) should be minimized. So, to maximize $$a/b/c/d$$  we have to first minimize b/c/d. Now our objective turns to minimize the expression b/c/d.

There are two possible combinations of this expression, b/(c/d) and (b/c)/d.
```
b/(c/d)        (b/c)/d = b/c/d
(b*d)/c        b/(d*c)
d/c            1/(d*c)
```

Obviously, $$d/c > 1/(d*c)$$ for $$d>1$$.

You can see that second combination will always be less than first one for numbers greater than $$1$$. So, the answer will be a/(b/c/d).
Similarly for expression like a/b/c/d/e/f... answer will be a/(b/c/d/e/f...).



<iframe src="https://leetcode.com/playground/wUbJEUre/shared" frameBorder="0" name="wUbJEUre" width="100%" height="309"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$. Single loop to traverse $$nums$$ array.

* Space complexity : $$O(n)$$. $$res$$ variable is used to store the result.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy to understand simple O(n) solution with explanation
- Author: vsmnv
- Creation Date: Sun Apr 16 2017 11:19:25 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 19 2018 20:03:05 GMT+0800 (Singapore Standard Time)

<p>
X1/X2/X3/../Xn will always be equal to (X1/X2) * Y, no matter how you place parentheses. i.e no matter how you place parentheses, X1 always goes to the numerator and X2 always goes to the denominator. Hence you just need to maximize Y. And Y is maximized when it is equal to X3 *..*Xn. So the answer is always X1/(X2/X3/../Xn) = (X1 *X3 *..*Xn)/X2
```
class Solution {
public:
    string optimalDivision(vector<int>& nums) {
        string ans;
        if(!nums.size()) return ans;
        ans = to_string(nums[0]);
        if(nums.size()==1) return ans;
        if(nums.size()==2) return ans + "/" + to_string(nums[1]);
        ans += "/(" + to_string(nums[1]);
        for(int i = 2; i < nums.size();++i)
            ans += "/" + to_string(nums[i]);
        ans += ")";
        return ans;
};
```
</p>


### Java Solution, Backtracking
- Author: shawngao
- Creation Date: Sun Apr 16 2017 11:01:29 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 15:50:01 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    class Result {
        String str;
        double val;
    }
    
    public String optimalDivision(int[] nums) {
        int len = nums.length;
        return getMax(nums, 0, len - 1).str;
    }
    
    private Result getMax(int[] nums, int start, int end) {
        Result r = new Result();
        r.val = -1.0;
        
        if (start == end) {
            r.str = nums[start] + "";
            r.val = (double)nums[start];
        }
        else if (start + 1 == end) {
            r.str = nums[start] + "/" + nums[end];
            r.val = (double)nums[start] / (double)nums[end];
        }
        else {
            for (int i = start; i < end; i++) {
                Result r1 = getMax(nums, start, i);
                Result r2 = getMin(nums, i + 1, end);
                if (r1.val / r2.val > r.val) {
                    r.str = r1.str + "/" + (end - i >= 2 ? "(" + r2.str + ")" : r2.str);
                    r.val = r1.val / r2.val;
                }
            }
        }
        
        //System.out.println("getMax " + start + " " + end + "->" + r.str + ":" + r.val);
        return r;
    }
    
    private Result getMin(int[] nums, int start, int end) {
        Result r = new Result();
        r.val = Double.MAX_VALUE;
        
        if (start == end) {
            r.str = nums[start] + "";
            r.val = (double)nums[start];
        }
        else if (start + 1 == end) {
            r.str = nums[start] + "/" + nums[end];
            r.val = (double)nums[start] / (double)nums[end];
        }
        else {
            for (int i = start; i < end; i++) {
                Result r1 = getMin(nums, start, i);
                Result r2 = getMax(nums, i + 1, end);
                if (r1.val / r2.val < r.val) {
                    r.str = r1.str + "/" + (end - i >= 2 ? "(" + r2.str + ")" : r2.str);
                    r.val = r1.val / r2.val;
                }
            }
        }
        
        //System.out.println("getMin " + start + " " + end + "->" + r.str + ":" + r.val);
        return r;
    }
}
```
</p>


### Brute force with memory in case of your interviewer forbid tricky solution
- Author: fallcreek
- Creation Date: Sun Apr 16 2017 13:11:32 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 20 2018 23:19:46 GMT+0800 (Singapore Standard Time)

<p>
This is an Amazon interview question that could be solved in a very tricky way. Since a lot of people have already posted their solutions, I just post a brute force solution in case of the interviewer want a normal way.

I use recursion. For each recursion, I find the maximum result and also the minimum result. For example, if you want to know the maximum result of A/B, where A and B are also some expressions, then you only need to know the maximum result of A and the minimum result of B. However, if you want to know the maximum result of C/(A/B), then you also need to know the minimum result of A/B. That's why both maximum and minimum should be stored. 

```
// by fallcreek
public class Solution {

    public String optimalDivision(int[] nums) {
        Map<String, pair> memory = new HashMap<>();
        pair sol = divid(nums,0,nums.length-1, memory);
        return sol.maxS;
    }
    
    public pair divid(int[] nums, int start, int end, Map<String, pair> memory){
        String key = start + " " + end;
        if(memory.containsKey(key)) return memory.get(key);
        if(start == end)    return new pair(nums[start], "" + nums[start],nums[start], "" + nums[start]);
        
        pair sol = new pair(0,"",0,"");
        
        for(int i = start; i < end; i++){
            pair left = divid(nums, start, i, memory);
            pair right = divid(nums, i + 1, end, memory);
            
            double min = left.min / right.max;
            String minS = left.minS + "/" + (i + 1 == end ? right.maxS : "(" + right.maxS + ")"); 
            double max = left.max / right.min;
            String maxS = left.maxS + "/" + (i + 1 == end ? right.minS : "(" + right.minS + ")"); 
            if(sol.min == 0 || min < sol.min){
                sol.min = min;
                sol.minS = minS;
            }
            if(max > sol.max){
                sol.max = max;
                sol.maxS = maxS;
            }
        }
        memory.put(key, sol);
        return sol;
    }
}

class pair{
    double min;
    String minS;
    double max;
    String maxS;
    
    public pair(double min, String minS, double max, String maxS){
        this.min = min;
        this.minS = minS;
        this.max = max;
        this.maxS = maxS;
    }
}
```
</p>


