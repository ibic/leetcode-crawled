---
title: "Before and After Puzzle"
weight: 1039
#id: "before-and-after-puzzle"
---
## Description
<div class="description">
<p>Given a list of <code>phrases</code>, generate a list of&nbsp;Before and After puzzles.</p>

<p>A <em>phrase</em> is a string that consists of lowercase English letters and spaces only. No space appears in the start or the end of a phrase. There are&nbsp;no consecutive spaces&nbsp;in a phrase.</p>

<p><em>Before and After&nbsp;puzzles</em> are phrases that are formed by merging&nbsp;two phrases where the <strong>last&nbsp;word of the first&nbsp;phrase</strong> is the same as the <strong>first word of the second phrase</strong>.</p>

<p>Return the&nbsp;Before and After&nbsp;puzzles that can be formed by every two phrases&nbsp;<code>phrases[i]</code>&nbsp;and&nbsp;<code>phrases[j]</code>&nbsp;where&nbsp;<code>i != j</code>. Note that the order of matching two phrases matters, we want to consider both orders.</p>

<p>You should return a list of&nbsp;<strong>distinct</strong>&nbsp;strings <strong>sorted&nbsp;lexicographically</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> phrases = [&quot;writing code&quot;,&quot;code rocks&quot;]
<strong>Output:</strong> [&quot;writing code rocks&quot;]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> phrases = [&quot;mission statement&quot;,
                  &quot;a quick bite to eat&quot;,
&nbsp;                 &quot;a chip off the old block&quot;,
&nbsp;                 &quot;chocolate bar&quot;,
&nbsp;                 &quot;mission impossible&quot;,
&nbsp;                 &quot;a man on a mission&quot;,
&nbsp;                 &quot;block party&quot;,
&nbsp;                 &quot;eat my words&quot;,
&nbsp;                 &quot;bar of soap&quot;]
<strong>Output:</strong> [&quot;a chip off the old block party&quot;,
&nbsp;        &quot;a man on a mission impossible&quot;,
&nbsp;        &quot;a man on a mission statement&quot;,
&nbsp;        &quot;a quick bite to eat my words&quot;,
         &quot;chocolate bar of soap&quot;]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> phrases = [&quot;a&quot;,&quot;b&quot;,&quot;a&quot;]
<strong>Output:</strong> [&quot;a&quot;]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= phrases.length &lt;= 100</code></li>
	<li><code>1 &lt;= phrases[i].length &lt;= 100</code></li>
</ul>

</div>

## Tags
- String (string)

## Companies
- clutter - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ 2 hash maps and set
- Author: votrubac
- Creation Date: Mon Sep 09 2019 02:37:13 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 09 2019 02:37:13 GMT+0800 (Singapore Standard Time)

<p>
Use the hash maps to map first and last words to the *remaining* phrase.

Use set to collect and sort distinct results.
```
vector<string> beforeAndAfterPuzzles(vector<string>& phrases) {
  unordered_map<string, set<string>> first, last;
  set<string> res;
  for (auto p : phrases) {
    auto fp = p.find(\' \'), lp = p.rfind(\' \');
    fp = fp == string::npos ? p.size() : fp;
    lp = lp == string::npos ? 0 : lp + 1;
    for (auto pp : first[p.substr(lp)]) res.insert(p + pp);
    for (auto pp : last[p.substr(0, fp)]) res.insert(pp + p);
    first[p.substr(0, fp)].insert(p.substr(fp));
    last[p.substr(lp)].insert(p.substr(0, lp));
  }
  return vector<string>(begin(res), end(res));
}
```
</p>


### Straightforward Java solution, beats 90%
- Author: ailsagreat
- Creation Date: Fri Sep 20 2019 04:18:25 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 20 2019 04:18:25 GMT+0800 (Singapore Standard Time)

<p>
```
 public List<String> beforeAndAfterPuzzles(String[] phrases) {
        int n = phrases.length;
        Map<String, List<Integer>> map = new HashMap<>();
        // for the head word of each string in phrases as the key, the value is the index of this string
        for(int i = 0; i < n; ++i) {
            String head = head(phrases[i]);
            if (!map.containsKey(head)) {
                map.put(head, new ArrayList<>());
            }
            map.get(head).add(i);
        }
        Set<String> result = new HashSet<>();
        for(int i = 0; i < n; ++i) {
            String tail = tail(phrases[i]);
            if (map.containsKey(tail)) {
                for (Integer j : map.get(tail)) {
                    if (i != j) {
                        result.add(phrases[i] + phrases[j].substring(tail.length()));
                    }
                }
            }
        }
        List<String> list = new ArrayList<>(result);
        Collections.sort(list);
        return list;
    }
    
    static String head(String phrase) {
        String[] array = phrase.split(" ");
        return array[0];
    }
    
    static String tail(String phrase) {
        String[] array = phrase.split(" ");
        return array[array.length - 1];
    }
	```
</p>


### Python solution, 48 ms beat 100%
- Author: Jason003
- Creation Date: Sun Sep 08 2019 00:43:48 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 08 2019 00:43:48 GMT+0800 (Singapore Standard Time)

<p>
```python
class Solution:
    def beforeAndAfterPuzzles(self, phrases: List[str]) -> List[str]:
        first = collections.defaultdict(set)
        last = collections.defaultdict(set)
        res = set()
        for p in phrases:
            strs = p.split(\' \')
            if strs[0] in last:
                res |= {a + p[len(strs[0]):] for a in last[strs[0]]}
            if strs[-1] in first:
                res |= {p + b for b in first[strs[-1]]}
            first[strs[0]].add(p[len(strs[0]):])
            last[strs[-1]].add(p)
        return sorted(list(res))
```
</p>


