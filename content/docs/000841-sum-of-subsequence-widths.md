---
title: "Sum of Subsequence Widths"
weight: 841
#id: "sum-of-subsequence-widths"
---
## Description
<div class="description">
<p>Given an array of integers <code>A</code>, consider all non-empty subsequences of <code>A</code>.</p>

<p>For any sequence S, let the&nbsp;<em>width</em>&nbsp;of S be the difference between the maximum and minimum element of S.</p>

<p>Return the sum of the widths of all subsequences of A.&nbsp;</p>

<p>As the answer may be very large, <strong>return the answer modulo 10^9 + 7</strong>.</p>

<div>
<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">[2,1,3]</span>
<strong>Output: </strong><span id="example-output-1">6</span>
<strong>Explanation:
</strong>Subsequences are [1], [2], [3], [2,1], [2,3], [1,3], [2,1,3].
The corresponding widths are 0, 0, 0, 1, 1, 2, 2.
The sum of these widths is 6.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li><code>1 &lt;= A.length &lt;= 20000</code></li>
	<li><code>1 &lt;= A[i] &lt;= 20000</code></li>
</ul>
</div>

</div>

## Tags
- Array (array)
- Math (math)

## Companies
- Sapient - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Mathematical

**Intuition**

Let's try to count the number of subsequences with minimum `A[i]` and maximum `A[j]`.

**Algorithm**

We can sort the array as it doesn't change the answer.  After sorting the array, this allows us to know that the number of subsequences with minimum `A[i]` and maximum `A[j]` is $$2^{j-i-1}$$.  Hence, the desired answer is:

$$
\sum\limits_{j > i} (2^{j-i-1}) (A_j - A_i)
$$

$$
= \big( \sum\limits_{i = 0}^{n-2} \sum\limits_{j = i+1}^{n-1} (2^{j-i-1}) (A_j) \big) - \big( \sum\limits_{i = 0}^{n-2} \sum\limits_{j = i+1}^{n-1} (2^{j-i-1}) (A_i) \big)
$$

$$
= \big( (2^0 A_1 + 2^1 A_2 + 2^2 A_3 + \cdots) + (2^0 A_2 + 2^1 A_3 + \cdots) + (2^0 A_3 + 2^1 A_4 + \cdots) + \cdots \big)
$$
$$
 - \big( \sum\limits_{i = 0}^{n-2} (2^0 + 2^1 + \cdots + 2^{N-i-2}) (A_i) \big)
$$

$$
= \big( \sum\limits_{j = 1}^{n-1} (2^j - 1) A_j \big) - \big( \sum\limits_{i = 0}^{n-2} (2^{N-i-1} - 1) A_i \big)
$$

$$
= \sum\limits_{i = 0}^{n-1} \big(((2^i - 1) A_i) - ((2^{N-i-1} - 1) A_i)\big)
$$

$$
= \sum\limits_{i = 0}^{n-1} (2^i - 2^{N-i-1}) A_i
$$

<iframe src="https://leetcode.com/playground/8STaskYC/shared" frameBorder="0" width="100%" height="361" name="8STaskYC"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N \log N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$, the space used by `pow2`.  (We can improve this to $$O(1)$$ space by calculating these powers on the fly.)
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/1-line Python] Sort and One Pass
- Author: lee215
- Creation Date: Sun Aug 19 2018 11:02:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 12 2020 23:19:30 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
The order in initial arrays doesn\'t matter,
my first intuition is to sort the array.

For each number `A[i]`:

1. There are `i` smaller numbers,
so there are `2 ^ i` sequences in which `A[i]` is maximum.
we should do `res += A[i] * (2 ^ i)`

2. There are `n - i - 1` bigger numbers,
so there are `2 ^ (n - i - 1)` sequences in which `A[i]` is minimum.
we should do `res -= A[i] * (n - i - 1)`

Done.
<br>

## **Time Complexity**:
Time `O(NlogN)`
Space `O(1)`
<br>


## **FAQ**
**Q. why do we plus mod before return?**
**A** In Cpp and Java, mod on negative number will still get a negative number.
<br>

**C++:**
```cpp
    int sumSubseqWidths(vector<int> A) {
        sort(A.begin(), A.end());
        long c = 1, res = 0, mod = 1e9 + 7, n = A.size();
        for (int i = 0; i < n; ++i, c = c * 2 % mod)
            res = (res + A[i] * c - A[n - i - 1] * c) % mod;
        return (res + mod) % mod;
    }
```

**Java:**
```java
    public int sumSubseqWidths(int[] A) {
        Arrays.sort(A);
        long c = 1, res = 0, mod = (long)1e9 + 7;
        for (int i = 0, n = A.length; i < n; ++i, c = c * 2 % mod)
            res = (res + A[i] * c - A[n - i - 1] * c) % mod;
        return (int)((res + mod) % mod);
    }
```
**1-line Python:**
```py
    def sumSubseqWidths(self, A):
        return sum(((1 << i) - (1 << len(A) - i - 1)) * a for i, a in enumerate(sorted(A))) % (10**9 + 7)
```
</p>


### LeetCode Weekly Contest 98 screencast
- Author: cuiaoxiang
- Creation Date: Sun Aug 19 2018 12:25:36 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 09:00:24 GMT+0800 (Singapore Standard Time)

<p>
https://www.youtube.com/watch?v=KpPDMI02LVQ

Did a really bad job this time. One out-of-boundary issue bite me and I was thinking there must be some LC bug all the time until I stopped screencast.
</p>


### C++ solution, O(N log N) sort + O(N) calculate
- Author: zhoubowei
- Creation Date: Sun Aug 19 2018 11:01:43 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 17 2018 02:42:08 GMT+0800 (Singapore Standard Time)

<p>
We only care about the subsequences and max/min values, so the order of the elements does not matter. Therefore, we sort the array at first.

For a sorted subarray A=[p,......,q], there are 2^(len(A)-2) subsequences which has min value=p, and max value=q. Because we can choose any numbers between p and q, there are 2^(len(A)-2) ways to choose.

For example, a given array is `[0,1,3,4,7]`.
For the subarray `[1,3,4,7]`, there are 2^2 subsequences which has min value=1 and max value=7.

Then we got an O(N^2) solution:
the answer is `sum((max(Ai)-min(Ai))\xD72^(len(Ai)-2))`, for all subarrays `Ai` where `len(Ai) > 1`.  
For the example above, it is:

```
1*2^0+3*2^1+4*2^2+7*2^3       // = x0
     +2*2^0+3*2^1+6*2^2       // = x1
           +1*2^0+4*2^1       // = x2
                 +3*2^0       // = x3
```

***
Of course the efficiency is not enough. Observe the table above\u2191, we can find that

```
x0-1*(2^0+2^1+2^2+2^3)=x1*2
x1-2*(2^0+2^1+2^2)=x2*2
x2-1*(2^0+2^1)=x3*2
x3-3*(2^0)=0
```

we can then calculate x3, x2, x1, x0 one by one: 

```
x4=0;
x3=x4*2 + (A4-A3)*(2^0)
x2=x3*2 + (A3-A2)*(2^0+2^1)
x1=x2*2 + (A2-A1)*(2^0+2^1+2^2)
x0=x1*2 + (A1-A0)*(2^0+2^1+2^2+2^3)

result=x4+x3+x2+x1+x0
```

Here is my code:

```cpp
#define M 1000000007
#define ll long long
class Solution {
public:
    int sumSubseqWidths(vector<int>& A) {
        sort(A.begin(), A.end());
        vector<ll> diff;
        for (int i = 0; i < A.size() - 1; i++) diff.push_back(A[i + 1] - A[i]);
        ll result = 0;
        ll x = 0;
        ll sum2 = 1;
        int len = diff.size();
        reverse(diff.begin(), diff.end());
        for (int i = 0; i < len; i++) {
            x <<= 1;
            x += sum2 * diff[i];
            x %= M;
            sum2 <<= 1;
            sum2++;
            sum2 %= M;
            result += x;
            result %= M;
        }
        return result;
    }
};
```
</p>


