---
title: "Power of Two"
weight: 215
#id: "power-of-two"
---
## Description
<div class="description">
<p>Given an integer <code>n</code>, write a function to determine if it is a power of two.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 1
<strong>Output:</strong> true
<strong>Explanation: </strong>2<sup>0</sup> = 1
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 16
<strong>Output:</strong> true
<strong>Explanation: </strong>2<sup>4</sup> = 16
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 3
<strong>Output:</strong> false
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 4
<strong>Output:</strong> true
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> n = 5
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>-2<sup>31</sup> &lt;= n &lt;= 2<sup>31</sup> - 1</code></li>
</ul>

</div>

## Tags
- Math (math)
- Bit Manipulation (bit-manipulation)

## Companies
- Apple - 2 (taggedByAdmin: false)
- Nvidia - 3 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: true)
- Amazon - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

We're not going to discuss here an obvious $$\mathcal{O}(\log N)$$
time solution

<iframe src="https://leetcode.com/playground/qSpQZbga/shared" frameBorder="0" width="100%" height="174" name="qSpQZbga"></iframe>

Instead, the problem will be solved in $$\mathcal{O}(1)$$ time
with the help of bitwise operators. The idea 
is to discuss such bitwise tricks as 

- How to get / isolate the rightmost 1-bit : `x & (-x)`.

- How to turn off (= set to 0) the rightmost 1-bit : `x & (x - 1)`.

These tricks are often used as something obvious in more 
complex bit-manipulation solutions, 
like for [N Queens problem](https://leetcode.com/articles/n-queens-ii/),
and it's important to recognize them to understand what is going on.
<br /> 
<br />


---
#### Intuition

The idea behind both solutions will be the same: a power of two
in binary representation is one 1-bit, followed by some zeros:

$$1 = (0000 0001)_2$$

$$2 = (0000 0010)_2$$

$$4 = (0000 0100)_2$$

$$8 = (0000 1000)_2$$

A number which is not a power of two, has more than one 1-bit
in its binary representation:

$$3 = (0000 0011)_2$$

$$5 = (0000 0101)_2$$

$$6 = (0000 0110)_2$$

$$7 = (0000 0111)_2$$

The only exception is 0, which should be treated separately.
<br /> 
<br />


---
#### Approach 1: Bitwise Operators : Get the Rightmost 1-bit

**Get/Isolate the Rightmost 1-bit**

Let's first discuss why `x & (-x)` is a way 
to keep the rightmost 1-bit and to set all the other bits to 0.

Basically, that works because of [two's complement](https://en.wikipedia.org/wiki/Two%27s_complement). 
In two's complement notation $$-x$$ is the same as $$\lnot x + 1$$.
In other words, to compute $$-x$$ one has to revert all bits in $$x$$
and then to add 1 to the result.

Adding 1 to $$\lnot x$$ in binary representation means 
to carry that 1-bit till the rightmost 0-bit in $$\lnot x$$
and to set all the lower bits to zero. 
Note, that the rightmost 0-bit in $$\lnot x$$ corresponds to the
rightmost 1-bit in $$x$$. 

> In summary, $$-x$$ is the same as $$\lnot x + 1$$. 
This operation reverts all bits of x except the rightmost 1-bit.

![fig](../Figures/231/twos.png)

Hence, x and -x have just one bit in common - the rightmost 1-bit.
That means that `x & (-x)` would keep that rightmost 1-bit and 
set all the other bits to 0.

![fig](../Figures/231/rightmost.png) 

**Detect Power of Two**

So let's do `x & (-x)` to keep the rightmost 1-bit and to set
all the others bits to zero. 
As discussed above, 
for the power of two it would result in `x` itself, since
a power of two contains just one 1-bit.

Other numbers have more than 1-bit in their binary 
representation and hence for them `x & (-x)` would not be
equal to `x` itself. 

Hence a number is a power of two if `x & (-x) == x`.

![fig](../Figures/231/first2.png) 

**Implementation**

<iframe src="https://leetcode.com/playground/TAS5QcQe/shared" frameBorder="0" width="100%" height="191" name="TAS5QcQe"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$. 

* Space complexity : $$\mathcal{O}(1)$$.
<br /> 
<br />


---
#### Approach 2: Bitwise operators : Turn off the Rightmost 1-bit

**Turn off the Rightmost 1-bit**

Let's first discuss why `x & (x - 1)` is a way 
to set the rightmost 1-bit to zero.

To subtract 1 means to change the rightmost 1-bit to 0 
and to set all the lower bits to 1.  

Now AND operator: the rightmost 1-bit will be turned off
because `1 & 0 = 0`, 
and all the lower bits as well. 

![fig](../Figures/231/turn2.png)

**Detect Power of Two**

The solution is straightforward: 

1. Power of two has just one 1-bit.

2. `x & (x - 1)` sets this 1-bit to zero, 
and hence one has to verify if the result is zero 
`x & (x - 1) == 0`.

![fig](../Figures/231/second2.png)

**Implementation**

<iframe src="https://leetcode.com/playground/Mx7ZWefD/shared" frameBorder="0" width="100%" height="191" name="Mx7ZWefD"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(1)$$. 

* Space complexity : $$\mathcal{O}(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Using n&(n-1) trick
- Author: LeJas
- Creation Date: Mon Jul 06 2015 04:04:12 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 05:34:50 GMT+0800 (Singapore Standard Time)

<p>
Power of 2 means only one bit of n is '1', so use the trick n&(n-1)==0 to judge whether that is the case
 

    class Solution {
    public:
        bool isPowerOfTwo(int n) {
            if(n<=0) return false;
            return !(n&(n-1));
        }
    };
</p>


### 4 different ways to solve -- Iterative / Recursive / Bit operation / Math
- Author: motorix
- Creation Date: Sat Jun 04 2016 19:11:52 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Feb 28 2020 08:58:57 GMT+0800 (Singapore Standard Time)

<p>
This question is not an difficult one, and there are many ways to solve it. 

**Method 1: Iterative**

check if ```n``` can be divided by 2. If yes, divide ```n``` by 2 and check it repeatedly.

    if (n <= 0) return false;
    while (n%2 == 0) n/=2;
    return n == 1;

Time complexity = ```O(log n)```

**Method 2: Recursive**

    return n > 0 && (n == 1 || (n%2 == 0 && isPowerOfTwo(n/2)));

Time complexity = ```O(log n)```
        
**Method 3: Bit operation**

If ```n``` is the power of two:

 - n = 2 ^ 0 = 1 = 0b0000...00000001, and (n - 1) = 0 = 0b0000...0000.
 - n = 2 ^ 1 = 2 = 0b0000...00000010, and (n - 1) = 1 = 0b0000...0001.
 - n = 2 ^ 2 = 4 = 0b0000...00000100, and (n - 1) = 3 = 0b0000...0011.
 - n = 2 ^ 3 = 8 = 0b0000...00001000, and (n - 1) = 7 = 0b0000...0111.

we have n & (n-1) == 0b0000...0000 == 0

Otherwise, n & (n-1) != 0. 

For example,  n =14 = 0b0000...1110, and (n - 1) = 13 = 0b0000...1101.

    return n > 0 && ((n & (n-1)) == 0);

Time complexity = ```O(1)```
        
**Method 4: Math derivation**

Because the range of an integer = -2147483648 (-2^31) ~ 2147483647 (2^31-1), the max possible power of two = 2^30 = 1073741824.

(1) If ```n``` is the power of two, let ```n = 2^k```, where ```k``` is an integer.

We have 2^30 = (2^k) * 2^(30-k), which means (2^30 % 2^k) == 0.

(2) If ```n``` is not the power of two, let ```n = j*(2^k)```, where ```k``` is an integer and ```j``` is an odd number.

We have (2^30 % j*(2^k)) == (2^(30-k) % j) != 0. 

    return n > 0 && (1073741824 % n == 0);

Time complexity = ```O(1)```

--
Update:
Thanks for everyone\'s comment. Following are some other solutions metioned in comments.

**Method 5: Bit count**

Very intuitive. If ```n``` is the power of 2, the bit count of ```n``` is 1. 
Note that ```0b1000...000``` is ```-2147483648```, which is not the power of two, but the bit count is 1.
```
return n > 0 && Integer.bitCount(n) == 1;
```
Time complexity = ```O(1)```
The time complexity of ```bitCount()``` can be done by a fixed number of operations. 
More info in https://stackoverflow.com/questions/109023.

**Method 6: Look-up table**
There are only 31 numbers in total for an 32-bit integer. 
```
return new HashSet<>(Arrays.asList(1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608,16777216, 33554432, 67108864, 134217728, 268435456, 536870912, 1073741824)).contains(n);
```
time complexity = ```O(1)```





</p>


### One line java solution using bitCount
- Author: qxx
- Creation Date: Sun Jul 12 2015 14:44:03 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 14:34:17 GMT+0800 (Singapore Standard Time)

<p>
This is kind of cheating, but the idea is that a power of two in binary form has and only has one "1".

    public class Solution {
        public boolean isPowerOfTwo(int n) {
            return n>0 && Integer.bitCount(n) == 1;
        }
    }
</p>


