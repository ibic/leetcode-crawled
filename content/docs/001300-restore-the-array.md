---
title: "Restore The Array"
weight: 1300
#id: "restore-the-array"
---
## Description
<div class="description">
<p>A program was supposed to print an array of integers. The program forgot to print whitespaces and the array is printed as a string of digits and all we know is that all integers in the array were in the range&nbsp;<code>[1, k]</code>&nbsp;and there are no leading zeros in the array.</p>

<p>Given the string <code>s</code> and the integer <code>k</code>. There can be multiple ways to restore the array.</p>

<p>Return <em>the number of possible array</em> that can be printed as a string <code>s</code>&nbsp;using the mentioned program.</p>

<p>The number of ways could be very large so return it <strong>modulo</strong> <code>10^9 + 7</code></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;1000&quot;, k = 10000
<strong>Output:</strong> 1
<strong>Explanation:</strong> The only possible array is [1000]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;1000&quot;, k = 10
<strong>Output:</strong> 0
<strong>Explanation:</strong> There cannot be an array that was printed this way and has all integer &gt;= 1 and &lt;= 10.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;1317&quot;, k = 2000
<strong>Output:</strong> 8
<strong>Explanation:</strong> Possible arrays are [1317],[131,7],[13,17],[1,317],[13,1,7],[1,31,7],[1,3,17],[1,3,1,7]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;2020&quot;, k = 30
<strong>Output:</strong> 1
<strong>Explanation:</strong> The only possible array is [20,20]. [2020] is invalid because 2020 &gt; 30. [2,020] is ivalid because 020 contains leading zeros.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> s = &quot;1234567890&quot;, k = 90
<strong>Output:</strong> 34
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= s.length &lt;= 10^5</code>.</li>
	<li><code>s</code> consists of only digits and doesn&#39;t contain leading zeros.</li>
	<li><code>1 &lt;= k &lt;= 10^9</code>.</li>
</ul>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Postman - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] DFS Memoization - Clean code
- Author: hiepit
- Creation Date: Sun Apr 19 2020 00:00:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 00:35:09 GMT+0800 (Singapore Standard Time)

<p>
**Java**
```java
class Solution {
    public int numberOfArrays(String s, int k) {
        Integer[] dp = new Integer[s.length()]; // dp[i] is number of ways to print valid arrays from string s start at i
        return dfs(s, k, 0, dp);
    }
    int dfs(String s, long k, int i, Integer[] dp) {
        if (i == s.length()) return 1; // base case -> Found a valid way
        if (s.charAt(i) == \'0\') return 0; // all numbers are in range [1, k] and there are no leading zeros -> So numbers starting with 0 mean invalid!
        if (dp[i] != null) return dp[i];
        int ans = 0;
        long num = 0;
        for (int j = i; j < s.length(); j++) {
            num = num * 10 + s.charAt(j) - \'0\'; // num is the value of the substring s[i..j]
            if (num > k) break; // num must be in range [1, k]
            ans += dfs(s, k, j + 1, dp);
            ans %= 1_000_000_007;
        }
        return dp[i] = ans;
    }
}
```

**C++**
```c++
class Solution {
public:
    int numberOfArrays(string s, int k) {
        vector<int> dp(s.size(), -1); // dp[i] is number of ways to print valid arrays from string s start at i
        return dfs(s, k, 0, dp);
    }
    int dfs(const string& s, long k, int i, vector<int>& dp) {
        if (i == s.size()) return 1; // base case -> Found a valid way
        if (s[i] == \'0\') return 0; // all numbers are in range [1, k] and there are no leading zeros -> So numbers starting with 0 mean invalid!
        if (dp[i] != -1) return dp[i];
        int ans = 0;
        long num = 0;
        for (int j = i; j < s.size(); j++) {
            num = num * 10 + s[j] - \'0\'; // num is the value of the substring s[i..j]
            if (num > k) break; // num must be in range [1, k]
            ans += dfs(s, k, j + 1, dp);
            ans %= 1000000007;
        }
        return dp[i] = ans;
    }
};
```

**Complexity**
- Time: `O(n * log10(k))`, where `n <= 10^5` is the length of string `s`, `k <= 10^9` => `log10(k) <= 9`
Explain: There are total `n` states, each state needs maximum `log10(k)` iteration loops to calculate the result.
- Space: `O(n)`

**Check out more DFS Memoization Methods**
- [1411. Number of Ways to Paint N \xD7 3 Grid](https://leetcode.com/problems/number-of-ways-to-paint-n-3-grid/discuss/574912)
- [140. Word Break II](https://leetcode.com/problems/word-break-ii/discuss/44167)

Please help to **UPVOTE** if this post is useful for you. 
If you have any questions, feel free to comment below.
Happy coding!
</p>


### C++/Java: 3 Approaches
- Author: votrubac
- Creation Date: Sun Apr 19 2020 00:04:09 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Apr 20 2020 07:00:59 GMT+0800 (Singapore Standard Time)

<p>
We will go from easy top-down DP with memoisation, then convert it to bottom-up DP, and then optimize the memory complexity. This is a typical flow when you solve DP problems in an interview.

> Thanks [box_of_donuts](https://leetcode.com/box_of_donuts/) for a suggestion to avoid string compare.

#### Approach 1: Top-Down DP
DFS plus memoisation is the easiest approach to come up with; it can be used to understand the problem and as a basis for a bottom-up solution.

**C++**
```cpp
int dp[100001] = {};
int dfs(string &s, int i, int k) {
    if (i == s.size())
        return 1;
    if (s[i] == \'0\')
        return 0;
    if (!dp[i]) {
        for (long sz = 1, num = 0; i + sz <= s.size(); ++sz) {
            num = num * 10 + s[i + sz - 1] - \'0\';
            if (num > k)
                break;
            dp[i] = (dp[i] + dfs(s, i + sz, k)) % 1000000007;
        }
    }
    return dp[i];
}
int numberOfArrays(string s, int k) {
    return dfs(s, 0, k);
}
```
**Complexity Analysis**
- Time: O(s * log k). Often, it helps converting this solution to bottom-up DP to lock on this.
- Memory: O(s) for the memoisation and stack.

#### Approach 2: Bottom-Up DP
The benefit of bottom-up DP is that it\'s iterative, and we do not need memory for the stack. Also, memory used for tabulation can often be reduced (see the next approach).

For me, it\'s usually hard to come up with a bottom-up DP right off the bat (unless the problem is visual). However, after you work through the top-down approach, it\'s much easier to figure out the tabulation.

**C++**
```cpp
int numberOfArrays(string s, int k) {
    vector<int> dp(s.size() + 1);
    dp[s.size()] = 1; 
    for (int i = s.size() - 1; i >= 0; --i) {
        if (s[i] == \'0\')
            continue;
        for (long sz = 1, n = 0; i + sz <= s.size(); ++sz) {
            n = n * 10 + s[i + sz - 1] - \'0\';
            if (n > k)
                break;
            dp[i] = (dp[i] + dp[i + sz]) % 1000000007;
        }
    }
    return dp[0];
}
```
**Java**
```java
public int numberOfArrays(String s, int k) {
    int dp[] = new int[s.length() + 1];
    dp[s.length()] = 1;
    for (int i = s.length() - 1; i >= 0; --i) {
        if (s.charAt(i) == \'0\')
            continue;
        long n = 0;
        for (int sz = 1; i + sz <= s.length(); ++sz) {
            n = n * 10 + s.charAt(i + sz - 1) - \'0\';
            if (n > k)
                break;
            dp[i] = (dp[i] + dp[i + sz]) % 1000000007;
        }
    }
    return dp[0];
}
```
**Complexity Analysis**
- Time: O(s * log k).
- Memory: O(s) for tabulation. Comparing to top-down DP, we do not need memory for the stack.

#### Approach 3: Memory-Optimized Bottom-Up DP
As you can see from the above code, we never go back more than *k* steps. So, we can use a rolling array technique to lower the memory complexity to O(k).

**C++**
```cpp
int numberOfArrays(string s, int k) {
    int s_sz = s.size(), dp_sz = to_string(k).size() + 1;
    vector<int> dp(dp_sz);
    dp[s_sz % dp_sz] = 1;
    for (int i = s_sz - 1; i >= 0; --i) {
        dp[i % dp_sz] = 0;
        if (s[i] == \'0\')
            continue;
        for (long sz = 1, n = 0; i + sz <= s_sz; ++sz) {
            n = n * 10 + s[i + sz - 1] - \'0\';
            if (n > k)
                break;
            dp[i % dp_sz] = (dp[i % dp_sz] + dp[(i + sz) % dp_sz]) % 1000000007;
        }
    }
    return dp[0];
}
```
- Time: O(s * log k).
- Memory: O(log k).
</p>


### [C++/Python] Simple DP with python
- Author: yanrucheng
- Creation Date: Sun Apr 19 2020 00:00:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 00:04:09 GMT+0800 (Singapore Standard Time)

<p>
**Idea**
`dp[i]` records the **number of possible array that can be printed as a string s[i:]**
suppose the length of `s` is `n`.

- Base case:
    - we know that there is exactly **one** way to construct an empty string, thus `dp[n] = 1`
- States transition:
    - find out all the possible number we can input starting at s[i], thus `dp[i] = sum(dp[j] for all valid j)` where a valid `j` means `1 <= int(s[i:j]) <= k`

**Complexity**
TIme: `O(n)`
Space: `O(n)`, could be optimised to O(#digits of k)

**C++, without using long**
```
class Solution {
public:
    int numberOfArrays(string s, int k) {
        int n = (int)s.size();
        vector<int> dp(n + 1, 0);
        dp[n] = 1; // the base case
        for (int i=n-1; i>=0; --i) {
            int num = s[i] - \'0\', j = i + 1;
            while (num > 0 && num <= k && j < n + 1) {
                dp[i] = (dp[i] + dp[j]) % 1000000007;
		        // a tiny trick to avoid overflow
                num = (j < n && num <= k / 10) ? 10 * num + (s[j] - \'0\') : INT_MAX;
                j++;
            }
        }
        return dp[0];
    }
};
```

**Python 3**
```
class Solution:
    def numberOfArrays(self, s: str, k: int) -> int:
        n = len(s)
        s = [*map(int, s)] + [math.inf] # for easier implementation
        dp = [0] * n + [1]
        for i in range(n - 1, -1, -1):
            num, j = s[i], i + 1
            while 1 <= num <= k and j < n + 1:
                dp[i] = (dp[i] + dp[j]) % 1000000007
                num = 10 * num + s[j]
                j += 1
        return dp[0]
```

</p>


