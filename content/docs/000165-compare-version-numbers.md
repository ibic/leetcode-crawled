---
title: "Compare Version Numbers"
weight: 165
#id: "compare-version-numbers"
---
## Description
<div class="description">
<p>Given two version numbers,&nbsp;<code>version1</code> and <code>version2</code>, compare them.</p>

<ul>
</ul>

<p>Version numbers consist of <strong>one or more revisions</strong> joined by a dot&nbsp;<code>&#39;.&#39;</code>. Each revision&nbsp;consists of <strong>digits</strong>&nbsp;and may contain leading <strong>zeros</strong>. Every revision contains <strong>at least one character</strong>. Revisions are <strong>0-indexed from left to right</strong>, with the leftmost revision being revision 0, the next revision being revision 1, and so on. For example&nbsp;<code>2.5.33</code>&nbsp;and&nbsp;<code>0.1</code>&nbsp;are valid version numbers.</p>

<p>To compare version numbers, compare their revisions in <strong>left-to-right order</strong>. Revisions are compared using their&nbsp;<strong>integer value ignoring any leading zeros</strong>. This means that revisions&nbsp;<code>1</code>&nbsp;and&nbsp;<code>001</code>&nbsp;are considered&nbsp;<strong>equal</strong>. If a version number does not specify a revision at an index, then&nbsp;<strong>treat the revision as&nbsp;<code>0</code></strong>. For example, version&nbsp;<code>1.0</code> is less than version&nbsp;<code>1.1</code>&nbsp;because their revision 0s are the same, but their revision 1s are&nbsp;<code>0</code>&nbsp;and&nbsp;<code>1</code>&nbsp;respectively, and&nbsp;<code>0 &lt; 1</code>.</p>

<p><em>Return the following:</em></p>

<ul>
	<li>If <code>version1 &lt; version2</code>, return <code>-1</code>.</li>
	<li>If <code>version1 &gt; version2</code>, return <code>1</code>.</li>
	<li>Otherwise, return <code>0</code>.</li>
</ul>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> version1 = &quot;1.01&quot;, version2 = &quot;1.001&quot;
<strong>Output:</strong> 0
<strong>Explanation:</strong> Ignoring leading zeroes, both &quot;01&quot; and &quot;001&quot; represent the same integer &quot;1&quot;.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> version1 = &quot;1.0&quot;, version2 = &quot;1.0.0&quot;
<strong>Output:</strong> 0
<strong>Explanation:</strong> version1 does not specify revision 2, which means it is treated as &quot;0&quot;.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> version1 = &quot;0.1&quot;, version2 = &quot;1.1&quot;
<strong>Output:</strong> -1
<strong>Explanation:</strong>&nbsp;version1&#39;s revision 0 is &quot;0&quot;, while version2&#39;s revision 0 is &quot;1&quot;. 0 &lt; 1, so version1 &lt; version2.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> version1 = &quot;1.0.1&quot;, version2 = &quot;1&quot;
<strong>Output:</strong> 1
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> version1 = &quot;7.5.2.4&quot;, version2 = &quot;7.5.3&quot;
<strong>Output:</strong> -1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= version1.length, version2.length &lt;= 500</code></li>
	<li><code>version1</code> and <code>version2</code>&nbsp;only contain digits and <code>&#39;.&#39;</code>.</li>
	<li><code>version1</code> and <code>version2</code>&nbsp;<strong>are valid version numbers</strong>.</li>
	<li>All the given revisions in&nbsp;<code>version1</code> and <code>version2</code>&nbsp;can be stored in&nbsp;a&nbsp;<strong>32-bit integer</strong>.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Amazon - 7 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: true)
- Square - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: true)
- Arista Networks - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Split + Parse, Two Pass

**Intuition**

The first idea is to split both strings by dot character into chunks
and then compare the chunks one by one. 

![traversal](../Figures/165/yoyo3.png)

That works fine if the number of chunks is the same for both versions.
If not, we need to pad the shorter string by adding `.0` at the end of the string with less chunks one or several times, 
so that the number of chunks will be the same. 

![traversal](../Figures/165/diff3.png)

**Algorithm**

- Split both strings by dot character into two arrays.

- Iterate over the longest array and compare chunks one by one.
If one of the arrays is over, virtually add as many zeros as needed
to continue the comparison with the longer array.

    - If two chunks are not equal, return 1 or -1.

- If we're here, the versions are equal. Return 0. 

**Implementation**

<iframe src="https://leetcode.com/playground/mqzSDk23/shared" frameBorder="0" width="100%" height="378" name="mqzSDk23"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N + M + \max(N, M))$$, where
$$N$$ and $$M$$ are lengths of input strings.  
 
* Space complexity : $$\mathcal{O}(N + M)$$ to store arrays 
`nums1` and `nums2`. 


---
#### Approach 2: Two Pointers, One Pass

**Intuition**

Rather than splitting the string all at once with the `split()` function, we could also split the string **_on the fly_**, through which we only need to iterate through the revisions once.

>The idea is that we split the string _chunk by chunk_, _i.e._ each trunk represents a revision in the version number.
The moment we retrieve a trunk from each string, we then compare them.

In this way, one could move along both strings in parallel, retrieve
and compare corresponding chunks. Once both strings are parsed,
the comparison is done as well.

As a result, the process can be done in a **single** pass.

**Algorithm**

First, we define a function named `get_next_chunk(version, n, p)`,
which is to retrieve the next chunk in the string.

This function takes three arguments:
the input string `version`, its length `n`, 
and a pointer `p` set to the first character of chunk to retrieve. 
It returns an integer chunk in-between the pointer `p` and the next dot.
To help with the iteration, it returns as well a pointer set to the first character of the next chunk. 

Here is how one could solve the problem using this function:

- Set a pointer `p1` pointed to the beginning of string `version1` and a pointer `p2` to the beginning of string `version2`: `p1 = p2 = 0`.

- Iterate over both strings in parallel. While `p1 < n1 or p2 < n2`:

    - Retrieve the next chunk `i1` from string `version1` and 
    next chunk `i2` from string `version2` using the above-defined `get_next_chunk`
    function.
    
    - Compare `i1` and `i2`. If they are not equal, return 1 or -1.

- If we're here, the versions are equal. Return 0.

Now let's implement our `get_next_chunk(version, n, p)` function:

- The beginning of chunk is marked by the pointer `p`. 
If `p` is set to the end of string, the string is already parsed.
To continue the comparison, let's add a virtual `.0` at the end of this
string by returning 0.  

- If `p` is not at the end of string, move the pointer `p_end` along the 
string to find the end of chunk.

- Return the chunk `version.substring(p, p_end)`.

**Implementation**

!?!../Documents/165_LIS.json:1000,357!?!

<iframe src="https://leetcode.com/playground/k2mrHTAE/shared" frameBorder="0" width="100%" height="500" name="k2mrHTAE"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(\max(N, M))$$, where
$$N$$ and $$M$$ are lengths of the input strings respectively. It's a one-pass solution.
 
* Space complexity : $$\mathcal{O}(\max(N, M))$$.
  - Despite the fact that we did not keep arrays of revision numbers, we still need some additional space to store a substring of the input string for integer conversion.
  In the worst case, the substring could be of the original string as well.


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Accepted small Java solution.
- Author: pavel-shlyk
- Creation Date: Mon Dec 22 2014 00:00:12 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 03:01:19 GMT+0800 (Singapore Standard Time)

<p>
This code assumes that next level is zero if no mo levels in shorter version number. And than compare levels.


    public int compareVersion(String version1, String version2) {
        String[] levels1 = version1.split("\\.");
        String[] levels2 = version2.split("\\.");
        
        int length = Math.max(levels1.length, levels2.length);
        for (int i=0; i<length; i++) {
        	Integer v1 = i < levels1.length ? Integer.parseInt(levels1[i]) : 0;
        	Integer v2 = i < levels2.length ? Integer.parseInt(levels2[i]) : 0;
        	int compare = v1.compareTo(v2);
        	if (compare != 0) {
        		return compare;
        	}
        }
        
        return 0;
    }
</p>


### Cudos on who invent such a boring question
- Author: why2013
- Creation Date: Tue Dec 16 2014 12:43:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 02:29:53 GMT+0800 (Singapore Standard Time)

<p>
Why would anyone have a version number of:
"19.8.3.17.5.01.0.0.4.0.0.0.0.0.0.0.0.0.0.0.0.0.00.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.000000.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.000000"

This guy must be bored to hell.
</p>


### My 2ms easy solution with C/C++
- Author: XUYAN3
- Creation Date: Mon Apr 06 2015 04:16:42 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 23:09:21 GMT+0800 (Singapore Standard Time)

<p>
    int compareVersion(string version1, string version2) {
        int i = 0; 
        int j = 0;
        int n1 = version1.size(); 
        int n2 = version2.size();
        
        int num1 = 0;
        int num2 = 0;
        while(i<n1 || j<n2)
        {
            while(i<n1 && version1[i]!='.'){
                num1 = num1*10+(version1[i]-'0');
                i++;
            }
            
            while(j<n2 && version2[j]!='.'){
                num2 = num2*10+(version2[j]-'0');;
                j++;
            }
            
            if(num1>num2) return 1;
            else if(num1 < num2) return -1;
            
            num1 = 0;
            num2 = 0;
            i++;
            j++;
        }
        
        return 0;
    }
</p>


