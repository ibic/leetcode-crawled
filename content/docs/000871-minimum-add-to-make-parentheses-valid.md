---
title: "Minimum Add to Make Parentheses Valid"
weight: 871
#id: "minimum-add-to-make-parentheses-valid"
---
## Description
<div class="description">
<p>Given a string&nbsp;<code>S</code> of <code>&#39;(&#39;</code> and <code>&#39;)&#39;</code> parentheses, we add the minimum number of parentheses ( <code>&#39;(&#39;</code> or <code>&#39;)&#39;</code>, and in any positions ) so that the resulting parentheses string is valid.</p>

<p>Formally, a parentheses string is valid if and only if:</p>

<ul>
	<li>It is the empty string, or</li>
	<li>It can be written as <code>AB</code>&nbsp;(<code>A</code> concatenated with <code>B</code>), where <code>A</code> and <code>B</code> are valid strings, or</li>
	<li>It can be written as <code>(A)</code>, where <code>A</code> is a valid string.</li>
</ul>

<p>Given a parentheses string, return the minimum number of parentheses we must add to make the resulting string valid.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;())&quot;</span>
<strong>Output: </strong><span id="example-output-1">1</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">&quot;(((&quot;</span>
<strong>Output: </strong><span id="example-output-2">3</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">&quot;()&quot;</span>
<strong>Output: </strong><span id="example-output-3">0</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-4-1">&quot;()))((&quot;</span>
<strong>Output: </strong><span id="example-output-4">4</span></pre>

<p>&nbsp;</p>
</div>
</div>
</div>

<p><strong>Note:</strong></p>

<ol>
	<li><code>S.length &lt;= 1000</code></li>
	<li><code>S</code> only consists of <code>&#39;(&#39;</code> and <code>&#39;)&#39;</code> characters.</li>
</ol>

<div>
<div>
<div>
<div>&nbsp;</div>
</div>
</div>
</div>
</div>

## Tags
- Stack (stack)
- Greedy (greedy)

## Companies
- Facebook - 10 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- ServiceNow - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Balance

**Intuition and Algorithm**

Keep track of the *balance* of the string: the number of `'('`'s minus the number of `')'`'s.  A string is valid if its balance is 0, plus every prefix has non-negative balance.

The above idea is common with matching brackets problems, but could be difficult to find if you haven't seen it before.

Now, consider the balance of every prefix of `S`.  If it is ever negative (say, -1), we must add a '(' bracket.  Also, if the balance of `S` is positive (say, `+B`), we must add `B` ')' brackets at the end.

<iframe src="https://leetcode.com/playground/VsSocwfS/shared" frameBorder="0" width="100%" height="310" name="VsSocwfS"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `S`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] Straight Forward One Pass
- Author: lee215
- Creation Date: Sun Oct 14 2018 11:18:46 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 16 2019 22:47:46 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**:
To make a string valid,
we can add some `(` on the left,
and add some `)` on the right.
We need to find the number of each.
<br>

## **Explanation**:
`left` records the number of `(` we need to add on the left of `S`.
`right` records the number of `)` we need to add on the right of `S`,
which equals to the number of current opened parentheses.
<br>

Loop char `c` in the string `S`:
`if (c == \'(\')`, we increment `right`,
`if (c == \')\')`, we decrement `right`.
When `right` is already 0, we   increment `left`
Return `left + right` in the end
<br>

## **Time Complexity**:
Time `O(N)`
Space `O(1)`
<br>

**Java:**
```java
    public int minAddToMakeValid(String S) {
        int left = 0, right = 0;
        for (int i = 0; i < S.length(); ++i) {
            if (S.charAt(i) == \'(\') {
                right++;
            } else if (right > 0) {
                right--;
            } else {
                left++;
            }
        }
        return left + right;
    }
```

**C++:**
```cpp
    int minAddToMakeValid(string S) {
        int left = 0, right = 0;
        for (char c : S)
            if (c == \'(\')
                right++;
            else if (right > 0)
                right--;
            else
                left++;
        return left + right;
    }
```

**Python:**
```python
    def minAddToMakeValid(self, S):
        left = right = 0
        for i in S:
            if right == 0 and i == \')\':
                left += 1
            else:
                right += 1 if i == \'(\' else -1
        return left + right
```

</p>


### [Java/Python 3] two one pass codes - space O(n) and O(1), respectively
- Author: rock
- Creation Date: Sun Oct 14 2018 11:03:02 GMT+0800 (Singapore Standard Time)
- Update Date: Fri May 08 2020 00:00:22 GMT+0800 (Singapore Standard Time)

<p>

Loop through the input array.
1. if encounter \'(\', push \'(\' into stack;
2. otherwise, \')\', check if there is a matching \'(\' on top of stack; if no, increase the counter by 1; if yes, pop it out;
3. after the loop, count in the un-matched characters remaining in the stack.


Method 1: 
Time & space: O(n).
```java
    public int minAddToMakeValid(String S) {
        Deque<Character> stk = new ArrayDeque<>();
        int count = 0;
        for (char c : S.toCharArray()) {
            if (c == \'(\') { 
                stk.push(c); 
            }else if (stk.isEmpty()) { 
                ++count; 
            }else { 
                stk.pop(); 
            }
        }
        return count + stk.size();
    }
```
or, make the above the code shorter:

```java
    public int minAddToMakeValid(String S) {
        Deque<Character> stk = new ArrayDeque<>();
        for (char c : S.toCharArray()) {
            if (c == \')\' && !stk.isEmpty() && stk.peek() == \'(\') { // pop out matched pairs.
                stk.pop();  
            }else { // push in unmatched chars.
                stk.push(c); 
            } 
        }
        return stk.size();
    }
```
Method 2: 
Since there is only one kind of char, \'(\', in stack,only a counter will also work.
Time: O(n),  space: O(1).

```java
    public int minAddToMakeValid(String S) {
        int count = 0, stk = 0;
        for (int i = 0; i < S.length(); ++i) {
            if (S.charAt(i) == \'(\') { 
                ++stk; 
            }else if (stk <= 0) { 
                ++count; 
            }else {
                --stk; 
            }
        }
        return count + stk;
    }
```
```python
    def minAddToMakeValid(self, S: str) -> int:
        stk = count = 0
        for c in S:
            if c == \'(\':
                stk += 1
            elif stk > 0:
                stk -= 1    
            else:
                count += 1
        return stk + count
```
</p>


### python  two lines code
- Author: abcd28s
- Creation Date: Mon Nov 12 2018 04:46:48 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 12 2018 04:46:48 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def minAddToMakeValid(self, S):
        """
        :type S: str
        :rtype: int
        """
        
        while "()" in S:
            S = S.replace("()", "")
        
        return len(S)
```
</p>


