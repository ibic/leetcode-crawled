---
title: "All People Report to the Given Manager"
weight: 1555
#id: "all-people-report-to-the-given-manager"
---
## Description
<div class="description">
<p>Table: <code>Employees</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| employee_id   | int     |
| employee_name | varchar |
| manager_id    | int     |
+---------------+---------+
employee_id is the primary key for this table.
Each row of this table indicates that the employee with ID employee_id and name employee_name reports his work to his/her direct manager with manager_id
The head of the company is the employee with employee_id = 1.
</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find&nbsp;<code>employee_id</code>&nbsp;of all employees that directly or indirectly report their work to the head of the company.</p>

<p>The indirect relation between managers will not exceed 3 managers as the company is small.</p>

<p>Return result table in any order without duplicates.</p>

<p>The query result format is in the following example:</p>

<pre>
<code>Employees </code>table:
+-------------+---------------+------------+
| employee_id | employee_name | manager_id |
+-------------+---------------+------------+
| 1           | Boss          | 1          |
| 3           | Alice         | 3          |
| 2           | Bob           | 1          |
| 4           | Daniel        | 2          |
| 7           | Luis          | 4          |
| 8           | Jhon          | 3          |
| 9           | Angela        | 8          |
| 77          | Robert        | 1          |
+-------------+---------------+------------+

<code>Result </code>table:
+-------------+
| employee_id |
+-------------+
| 2           |
| 77          |
| 4           |
| 7           |
+-------------+

The head of the company is the employee with employee_id 1.
The employees with employee_id 2 and 77 report their work directly to the head of the company.
The employee with employee_id 4 report his work indirectly to the head of the company 4 --&gt; 2 --&gt; 1. 
The employee with employee_id 7 report his work indirectly to the head of the company 7 --&gt; 4 --&gt; 2 --&gt; 1.
The employees with employee_id 3, 8 and 9 don&#39;t report their work to head of company directly or indirectly. 
</pre>
</div>

## Tags


## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### MySQL clean solution
- Author: vitbau
- Creation Date: Sun Dec 01 2019 03:32:19 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Feb 08 2020 08:23:02 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT e1.employee_id
FROM Employees e1
JOIN Employees e2
ON e1.manager_id = e2.employee_id
JOIN Employees e3
ON e2.manager_id = e3.employee_id
WHERE e3.manager_id = 1 AND e1.employee_id != 1
```

even cleaner
```
SELECT e1.employee_id
FROM Employees e1,
     Employees e2,
     Employees e3
WHERE e1.manager_id = e2.employee_id
  AND e2.manager_id = e3.employee_id
  AND e3.manager_id = 1 
  AND e1.employee_id != 1
```
</p>


### MS SQL recursive CTE
- Author: serpol
- Creation Date: Wed Nov 27 2019 15:54:04 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Nov 28 2019 02:14:05 GMT+0800 (Singapore Standard Time)

<p>
```
WITH CTE AS (
    SELECT employee_id
    FROM Employees
    WHERE manager_id = 1 AND employee_id != 1
    UNION ALL
    SELECT e.employee_id
    FROM CTE c INNER JOIN Employees e ON c.employee_id = e.manager_id
)
SELECT employee_id
FROM CTE
ORDER BY employee_id
OPTION (MAXRECURSION 3);
```
I set MAXRECURSION to 3 as it is restricted in the task, depending on the real case it can be updated.
</p>


### MySQL recursive for any number of levels in hierarchy
- Author: mlove
- Creation Date: Wed Jul 08 2020 02:51:28 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jul 08 2020 02:56:11 GMT+0800 (Singapore Standard Time)

<p>

```
with recursive cte as
(
select employee_id from employees where manager_id=1 and employee_id!=1
union all
select a.employee_id from employees a join cte b on (b.employee_id=a.manager_id) 
)
select employee_id from cte
```
</p>


