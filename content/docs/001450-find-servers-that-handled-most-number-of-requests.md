---
title: "Find Servers That Handled Most Number of Requests"
weight: 1450
#id: "find-servers-that-handled-most-number-of-requests"
---
## Description
<div class="description">
<p>You have <code>k</code> servers numbered from <code>0</code> to <code>k-1</code> that are being used to handle multiple requests simultaneously. Each server has infinite computational capacity but <strong>cannot handle more than one request at a time</strong>. The requests are assigned to servers according to a specific algorithm:</p>

<ul>
	<li>The <code>i<sup>th</sup></code> (0-indexed) request arrives.</li>
	<li>If all servers are busy, the request is dropped (not handled at all).</li>
	<li>If the <code>(i % k)<sup>th</sup></code> server is available, assign the request to that server.</li>
	<li>Otherwise, assign the request to the next available server (wrapping around the list of servers and starting from 0 if necessary). For example, if the <code>i<sup>th</sup></code> server is busy, try to assign the request to the <code>(i+1)<sup>th</sup></code> server, then the <code>(i+2)<sup>th</sup></code> server, and so on.</li>
</ul>

<p>You are given a <strong>strictly increasing</strong> array <code>arrival</code> of positive integers, where <code>arrival[i]</code> represents the arrival time of the <code>i<sup>th</sup></code> request, and another array <code>load</code>, where <code>load[i]</code> represents the load of the <code>i<sup>th</sup></code> request (the time it takes to complete). Your goal is to find the <strong>busiest server(s)</strong>. A server is considered <strong>busiest</strong> if it handled the most number of requests successfully among all the servers.</p>

<p>Return <em>a list containing the IDs (0-indexed) of the <strong>busiest server(s)</strong></em>. You may return the IDs in any order.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/08/load-1.png" style="width: 389px; height: 221px;" />
<pre>
<strong>Input:</strong> k = 3, arrival = [1,2,3,4,5], load = [5,2,3,3,3] 
<strong>Output:</strong> [1] 
<strong>Explanation:</strong>
All of the servers start out available.
The first 3 requests are handled by the first 3 servers in order.
Request 3 comes in. Server 0 is busy, so it&#39;s assigned to the next available server, which is 1.
Request 4 comes in. It cannot be handled since all servers are busy, so it is dropped.
Servers 0 and 2 handled one request each, while server 1 handled two requests. Hence server 1 is the busiest server.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> k = 3, arrival = [1,2,3,4], load = [1,2,1,2]
<strong>Output:</strong> [0]
<strong>Explanation:</strong>
The first 3 requests are handled by first 3 servers.
Request 3 comes in. It is handled by server 0 since the server is available.
Server 0 handled two requests, while servers 1 and 2 handled one request each. Hence server 0 is the busiest server.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> k = 3, arrival = [1,2,3], load = [10,12,11]
<strong>Output:</strong> [0,1,2]
<strong>Explanation: </strong>Each server handles a single request, so they are all considered the busiest.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> k = 3, arrival = [1,2,3,4,8,9,10], load = [5,2,10,3,1,2,2]
<strong>Output:</strong> [1]
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> k = 1, arrival = [1], load = [1]
<strong>Output:</strong> [0]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= k &lt;= 10<sup>5</sup></code></li>
	<li><code>1 &lt;= arrival.length, load.length &lt;= 10<sup>5</sup></code></li>
	<li><code>arrival.length == load.length</code></li>
	<li><code>1 &lt;= arrival[i], load[i] &lt;= 10<sup>9</sup></code></li>
	<li><code>arrival</code> is <strong>strictly increasing</strong>.</li>
</ul>

</div>

## Tags
- Ordered Map (ordered-map)

## Companies
- Wish - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(nlogn) use both TreeSet and PriorityQueue
- Author: lechen999
- Creation Date: Sun Oct 04 2020 00:00:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 00:01:20 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public List<Integer> busiestServers(int k, int[] arrival, int[] load) {
        int[] counter = new int[k];
        // use a tree to track available servers
        TreeSet<Integer> available = new TreeSet<Integer>();
        for (int num = 0; num < k; num++) {
            available.add(num);
        }
        // use a PQ to maintain the availability at current arrival time
        Queue<int[]> busyServers = new PriorityQueue<>((a, b)->(a[0] - b[0]));
        
        for (int idx = 0; idx < arrival.length; idx++) {
            int curTime = arrival[idx];
            int endTime = curTime + load[idx];
            while (!busyServers.isEmpty() && busyServers.peek()[0] <= curTime) {
                int freedServer = busyServers.poll()[1];
                available.add(freedServer);
            }
            if (available.size() == 0) continue; // all busy
            Integer assignNum = available.ceiling(idx % k);
            if (assignNum == null) {
                assignNum = available.first();
            }
            counter[assignNum]++;
            available.remove(assignNum);
            busyServers.offer(new int[] {endTime, assignNum});
        }
        
        return findMaxesInCounter(counter);
    }
    
    
    
    private List<Integer> findMaxesInCounter(int[] counter) {
        int max = 0;
        for (int i = 0; i < counter.length; i++) {
            max = Math.max(max, counter[i]);
        }
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < counter.length; i++) {
            if (counter[i] == max) {
                result.add(i);
            }
        }
        return result;
    }
}
```
</p>


### Python, using only heaps
- Author: warmr0bot
- Creation Date: Sun Oct 04 2020 00:10:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 01:45:32 GMT+0800 (Singapore Standard Time)

<p>
Solution using three heaps. First heap is used to quickly free up the nodes. Then we split the servers to those that come after the `server_id` which is current server and those that come before, for loopback.
```
def busiestServers(self, k: int, arrival: List[int], load: List[int]) -> List[int]:
	busy_jobs = []  # heap (job_end_time, node) to free up the nodes quickly
	after = [] # heap (nodes) free after current server
	before = list(range(k))  # heap (nodes) to use for loopback
	requests_handled = [0] * k

	for i, (arrvl, ld) in enumerate(zip(arrival, load)):
		server_id = i % k
		if server_id == 0:  # loopback
			after = before
			before = []

		while busy_jobs and busy_jobs[0][0] <= arrvl:
			freed_node = heapq.heappop(busy_jobs)[1]
			if freed_node < server_id: heapq.heappush(before, freed_node)
			else: heapq.heappush(after, freed_node)

		use_queue = after if after else before
		if not use_queue: continue  # request dropped
		using_node = heapq.heappop(use_queue)
		requests_handled[using_node] += 1
		heapq.heappush(busy_jobs, (arrvl + ld, using_node))

	maxreqs = max(requests_handled)
	return [i for i, handled in enumerate(requests_handled) if handled == maxreqs]
```
</p>


### [C++] | Circular Array | Ordered Set | Priority Queue | O(N (log K + log N) | O(K)
- Author: mudit458
- Creation Date: Sun Oct 04 2020 00:01:18 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 00:38:06 GMT+0800 (Singapore Standard Time)

<p>
## **APPROACH**
> Do what it says

## **TECHNIQUE**
> We use the popular technique for circular arrays making it a linear array (by copying itself) 

new_servers = `0` to `2*k - 1`
where `i` and `i + k`th servers are the same
So whenever we change the state of server we do it for both `i` and `i + k`

**FOR NEXT FREE SERVER** after `i % k` 
or 
smallest free server greater than `i` (circularly), we can do `free_servers.lower_bound(i%k) % k` 
last `%k` because `x = i_th` or `x = (i + k)_th` server map to the same `(x % k)_th server`

For freeing the servers you can use a `min_heap`

## **TIME COMPLEXITY**
`O(N * (log K + log N))`

## **SPACE COMPLEXITY**
`O(K + N)`

**Note** There are better and cleaner solutions than this, you can still learn about the circular array technique which can be used on some other problems :)

### **The Technique**
Circular array `[1, 2, 3, ... K]`
Linear array `[1, 2, 3, ... K, 1, 2, 3 ... K-1]`
for this array each subarray of length K is a rotation of the circular array

## **CODE CPP**

```cpp
class Solution {
public:
    vector<int> busiestServers(int k, vector<int>& arr, vector<int>& lo) {
        set<int> free;
        int n = arr.size();
        for (int i = 0; i < 2*k - 1; i ++)
            free.insert(i);
        vector<int> cnt(k, 0);
        priority_queue<pair<int, int>, vector<pair<int, int>>, greater<>> pq;
        for (int i = 0; i < n; i ++)
        {
            int at = arr[i];
            int load = lo[i];
            while (!pq.empty() and pq.top().first <= at)
            {
                auto tt = pq.top();
                int server = tt.second;
                pq.pop();
                free.insert(server);
                free.insert(server + k);
            }
            if (!free.empty()) {
                int server = (*free.lower_bound(i % k)) % k;
                free.erase(server % k);
                free.erase(server % k + k);

                cnt[server] ++;
                pq.push({at + load, server});
            }
        }
        int mv = -1;
        vector<int> res;
        for (int i = 0; i < k; i ++)
        {
            if (cnt[i] > mv)
            {
                mv = cnt[i];
                res = {i};
            }
            else if (cnt[i] == mv)
                res.push_back(i);
        }
        return res;
    }
};
```
</p>


