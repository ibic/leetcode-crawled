---
title: "Iterator for Combination"
weight: 1094
#id: "iterator-for-combination"
---
## Description
<div class="description">
<p>Design an Iterator class, which has:</p>

<ul>
	<li>A constructor that takes a string&nbsp;<code>characters</code>&nbsp;of <strong>sorted distinct</strong> lowercase English letters and a number&nbsp;<code>combinationLength</code> as arguments.</li>
	<li>A function <em>next()</em>&nbsp;that returns the next combination of length <code>combinationLength</code>&nbsp;in <strong>lexicographical order</strong>.</li>
	<li>A function <em>hasNext()</em> that returns <code>True</code>&nbsp;if and only if&nbsp;there exists a next combination.</li>
</ul>

<p>&nbsp;</p>

<p><b>Example:</b></p>

<pre>
CombinationIterator iterator = new CombinationIterator(&quot;abc&quot;, 2); // creates the iterator.

iterator.next(); // returns &quot;ab&quot;
iterator.hasNext(); // returns true
iterator.next(); // returns &quot;ac&quot;
iterator.hasNext(); // returns true
iterator.next(); // returns &quot;bc&quot;
iterator.hasNext(); // returns false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= combinationLength &lt;=&nbsp;characters.length &lt;= 15</code></li>
	<li>There will be at most <code>10^4</code> function calls per test.</li>
	<li>It&#39;s guaranteed that all&nbsp;calls&nbsp;of the function <code>next</code>&nbsp;are valid.</li>
</ul>

</div>

## Tags
- Backtracking (backtracking)
- Design (design)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Disclaimer

This is a review article, and the goal is to propose 5 different
solutions and to discuss a possible interview strategy.
If you'd like to read a more detailed explanation of each 
solution, please check the following articles:

[Combinations](https://leetcode.com/articles/combinations/).

[Subsets](https://leetcode.com/articles/subsets/).

#### Precomputation or Next Combination: Why It's Risky to Use Backtracking

The interpretation of this problem depends on the interviewer. 
There are three possible scenarios:

- You could be asked to implement $$\mathcal{O}(1)$$ runtime by precomputing all the combinations. 

- Or, you could be asked to save space, to use no precomputation and 
to implement the `nextCombination` function to generate 
each new combination from the previous one during the runtime. 

- Or, the interviewer could let you choose one of the problems above
and then asks you to implement the second one as a follow-up. 

That's why it's a risky strategy [to compute combinations using standard backtacking](https://leetcode.com/articles/combinations/).
To precompute all the combinations using backtracking is doable.

<iframe src="https://leetcode.com/playground/5Sct4AaR/shared" frameBorder="0" width="100%" height="500" name="5Sct4AaR"></iframe>

As a follow-up you could be asked to rewrite your algorithm implementing 
`nextCombination` function, and that could be quite stressful during the interview
if you choose backtracking.

#### Overview: Bitmasking and Algorithm L

In this article, we're going to consider Bitmasking and Algorithm L. 
These two approaches could be easily used both for the precomputation, 
and the `nextCombination` function.

![pic](../Figures/1286/overall4.png)

**Bitmasking**

It's more simple to generate numbers than combinations.
So let us generate numbers, and then use their binary representations, bitmasks.

The idea is that we map each bitmask of length n to a combination.
Each bit is mapped to a character, the lowest bit to the last character,
the highest bit - to the first character.

![pic](../Figures/1286/lowest_bit22.png)
 
The character `characters[i]` is present in the combination if 
bit at the `n - 1 - i`*th* position is set.

![pic](../Figures/1286/present22.png)

$$
(1111)_2 - abcd \\
(0011)_2 - cd \\
(0101)_2 - bd \\
(0110)_2 - bc \\
(1001)_2 - ad \\
(1010)_2 - ac \\ 
(1100)_2 - ab
$$

![pic](../Figures/1286/bitmasks2.png)

In this article, we're going to keep bitmasking approach as simple as possible, 
having $$\mathcal{O}(2^N \cdot N)$$ time complexity for the precomputation 
case. 

**Algorithm L by D. E. Knuth**

![pic](../Figures/1286/algorithm_l2.png)

Algorithm L is an efficient BFS approach to generate lexicographic 
(_i.e._ binary sorted) combinations. 
It works by generating the combinations of indexes.

The advantage of this algorithm is that it "jumps" from one combination 
to another instead of brute-forcing $$2^N$$ bitmasks.
In total, there are $$C^k_N$$ combinations of length $$k$$, 
and hence one needs $$\mathcal{O}(k C_N^k)$$ time for the precomputation. 

> This approach is better than simple bitmasking: $$\mathcal{O}(k C_N^k)$$ 
vs $$\mathcal{O}(2^N \cdot N)$$.  

<br/>
<br/>

---
#### Approach 1: Bitmasking: Precomputation

![pic](../Figures/1286/pre_bitmasks2.png)

**Algorithm**

- Generate all possible binary bitmasks of length $$n$$: from $$0$$ to $$2^n - 1$$.

- Use bitmasks with $$k$$ set bits to generate combinations with $$k$$ elements.
If the `n - 1 - j`*th* bit set in the bitmask, it indicates the presence of 
the character `characters[j]` in the combination, and vise versa. 

>Bit manipulation trick.
To test if the `i`*th* bit is set in the bitmask, check the following:
`bitmask & (1 << i) != 0`.
It shifts the first 1-bit i positions to the left
and then uses logical AND operation to eliminate all bits from bitmask 
but `i`*th*. Hence, the result is nonzero only if `i`*th* bit is set in the bitmask.

- Now you have all combinations precomputed. Pop them out one 
by one at each request. 

![pic](../Figures/1286/bitmasking2.png)

**Implementation**

<iframe src="https://leetcode.com/playground/SFhk4TFN/shared" frameBorder="0" width="100%" height="500" name="SFhk4TFN"></iframe>


**Complexity Analysis**

- Time Complexity: 
    - $$\mathcal{O}(2^N \cdot N)$$ to generate $$2^N$$ bitmasks and then count a 
    number of bits set in each bitmask in $$\mathcal{O}(N)$$ time. 
    
    - $$\mathcal{O}(1)$$ runtime, _i.e._ for each `next()` call. 

- Space Complexity: $$\mathcal{O}(k \cdot C_N^k)$$ to keep $$C_N^k$$ 
combinations of length $$k$$.

<br/>
<br/>

---
#### Approach 2: Bitmasking: Next Combination

![pic](../Figures/1286/bitmasks2.png)

During precomputation, we've generated combinations in the _descending_ order.
That was done to pop them out later in the _ascending_ order easily.

> For the runtime generation at each `next()` call, the strategy should be changed:
the combinations should be generated directly in the _ascending_ order. 

**Algorithm**

- Start from the "highest" bitmask: 
$$
1^{(k)}0^{(n - k)} = \underbrace{1...1}_\text{k times} \space \underbrace{0...0}_\text{n-k times}
$$.

- At each step, generate a combination out of the current bitmask.
If the `n - 1 - j`*th* bit set in the bitmask, that means the presence of 
the character `characters[j]` in the combination, and vise versa. 

- Generate the next bitmask. Decrease bitmask gradually till you meet
a bitmask with exactly $$k$$ set bits.  

![pic](../Figures/1286/approach222.png)

**Implementation**

<iframe src="https://leetcode.com/playground/GiPHQEoa/shared" frameBorder="0" width="100%" height="500" name="GiPHQEoa"></iframe>


**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(2^N \cdot N / C_N^k)$$ in average. 
To generate $$C_N^k$$ combinations, one has to parse $$2^N$$ bitmasks and to check 
a number of bits set in each bitmask, 
that makes an average computation cost per combination to be equal to 
$$\mathcal{O}(2^N \cdot N / C_N^k)$$.

- Space Complexity: $$\mathcal{O}(k)$$ to keep the current combination of 
length $$k$$.

<br/>
<br/>

---
#### Approach 3: Algorithm L by D. E. Knuth: Lexicographic Combinations: Precomputation

Algorithm L is an efficient BFS that generates one by one the _combinations 
of indexes_. Here is how it works:

![pic](../Figures/1286/algorithm_l2.png)

$$
3210 - abcd \\
01 - cd \\
02 - bd \\
12 - bc \\
03 - ad \\
13 - ac \\
23 - ab \\
$$

**Algorithm**

The algorithm is quite straightforward:

- Initialize `nums` to be a list of integers from $$0$$ to $$k$$. 
Add $$n$$ as the last element. It serves as a sentinel. 
Set the pointer at the beginning of the list `j = 0`.

- While `j < k`:

    - Convert the first $$k$$ elements (_i.e._ all elements but the sentinel) 
    from `nums` into the combination to save.

    - Find the first number in `nums` such that `nums[j + 1] != nums[j] + 1` 
    and increase it by one `nums[j] += 1` to move to the next combination.

- Now you have all combinations precomputed. Pop them out one 
by one at each request. 

<iframe src="https://leetcode.com/playground/dXraMo5c/shared" frameBorder="0" width="100%" height="500" name="dXraMo5c"></iframe>


**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(k \times C_N^k)$$ for the precomputation
and $$\mathcal{O}(1)$$ during the runtime, i.e. for each `next()` call. 
The algorithm generates new combination from the previous one in 
$$\mathcal{O}(k)$$ time, and then uses $$\mathcal{O}(k)$$ time to save
it for later usage. In total there are $$C_N^k$$ combinations, that makes
precomputation time complexity to be equal to $$\mathcal{O}(k \times C_N^k)$$.
Runtime complexity is $$\mathcal{O}(1)$$.

- Space Complexity: $$\mathcal{O}(k \times C_N^k)$$ to keep $$C_N^k$$ 
combinations of length $$k$$.

<br/>
<br/>

---
#### Approach 4: Algorithm L by D. E. Knuth: Lexicographic Combinations: Next Combination

> For the runtime generation, the strategy should be changed:
the combinations will be generated directly in the _ascending_ order.

![pic](../Figures/1286/l_reversed2.png)

**Algorithm**

- Initialize `nums` as a list of integers from $$0$$ to $$k$$. 

- At each step:

    - Convert nums into the combination to save.
    
    - Generate next combination:
    
        - Set the pointer at the end of the list `j = k - 1`.
    
        - Find the first j such that `nums[j] == n - k + j` 
        and increase `nums[j]` by one `nums[j] += 1`.
        
        - Set `nums[i] = nums[j] + i - j` for 
        every `i` in range `(j + 1, k)` to move to the next combination.
        
**Implementation**

<iframe src="https://leetcode.com/playground/oySbvP6j/shared" frameBorder="0" width="100%" height="500" name="oySbvP6j"></iframe>


**Complexity Analysis**

- Time Complexity: $$\mathcal{O}(k)$$ both for `init()` and `next()` functions. 
The algorithm generates a new combination from the previous one in 
$$\mathcal{O}(k)$$ time. 

- Space Complexity: $$\mathcal{O}(k)$$ to keep the current combination of 
length $$k$$.

<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [ C++ ] Using Bit manipulation | Detail Explain
- Author: suman_buie
- Creation Date: Thu Aug 13 2020 16:14:13 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Aug 20 2020 10:42:14 GMT+0800 (Singapore Standard Time)

<p>
**This Question Look Like Little Complicated But It\'s Not actully In real 
So Lets See One Simple Approch**

Idea :
    step 1 . We Need To determind The all Possiable lexicographical String With Having combinationLength 
            from That Given String
    **Solution Of Step 1 :**
       we are Going To Create A static function OR Global Function Which take that Input string and combinationLength 
       as an argument and return us A set of string having length of combinationLength in lexicographical form
and we can genarete That all string by Using Bitmasking . How??Lets See.. fallowing
        **Imp** Check string length and Then Itarate Through ```1 to that string length```
            And consider Those Num which have ```set bit no ==  combinationLength```
            then Just Take those character That lay on Set bit Possiation and Using of Those Create a String
        Lastly Put each Of string into a String set and return that set

Lets take an Example To understand This Step

	Suppose Input string - "abc" and combinationLength =2
        now mask = 8 which is nothing but 2 power length of of "abc"
        now Itarate from 1 to 8 

       Num             bit_rep           no_of_set_bit       a   b   c            hold_set
        1               0 0 1                 1              0   0   1              nothing   <= because combinationLength != no_of_set_bit
        2               0 1 0                 1              0   1   0              same case like upper one
        3               0 1 1                 2              0   1   1              "bc" <= combinationLength ==no_of
        4               1 0 0                 1              1   0   0              nothing
        5               1 0 1                 2              1   0   1               "ac"
        6               1 1 0                 2              1   1   0               "ab"
        7               1 1 1                 3              1   1   1              nothing <= because combinationLength != no_of_set_bit

        return that set in lexi order ["ab","ac","bc"]
       
    Now We Done Our Main Work
step 2. Take an iterator and initialize whith Set Begin
        and for each ```next()```return it\'s Possiation string from set and increase that iterator
        for ``` hasNext()``` Check iterator reach to then end of set or not and return that



```
set<string>GenaretallComb(string s,int len){
    int mask = 1<<s.length();
    set<string>hold;
    string comString = "";
    for(int no=1;no<mask;no++){
        int num = no,i = 0;
        while(num){
            if(num&1)comString = comString + s[i];
            i++,num>>=1;
        }
        if(comString.length()==len)hold.insert(comString);
        comString = "";
    }
    return hold;
}

class CombinationIterator {
public:
    set<string>st;
    set <string> :: iterator cur;
    CombinationIterator(string characters, int combinationLength) {
        this->st = GenaretallComb(characters,combinationLength);
        this->cur = begin(st);
    }
    
    string next() {
        return cur==end(st)?"":*cur++;
    }
    
    bool hasNext() {
        return cur!=end(st);
    }
};
```

I hope It Help you **:)**
Please **upVote** Then
Any Doubt comments Bellow
Thank You **:)**
**Happy Coding**
</p>


### [Java] No pre-calculation needed for iterator questions
- Author: shune
- Creation Date: Sun Dec 15 2019 05:40:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Dec 15 2019 15:10:56 GMT+0800 (Singapore Standard Time)

<p>
Process the string (str) lexicographically and only store the next result in a stack of characters. Dynamically generate the next combination every-time `next()` is called.
```
class CombinationIterator {
    
    Stack<Character> st; // stack to store the characters leading to the creation of a single combination
	Map<Character, Integer> ch2Idx; // map to store character to index
    String result, str; // str - same as characters. result -- the result string representing a combination
    int combLength; // same as combinationLength
 
    public CombinationIterator(String characters, int combinationLength) {
        combLength = combinationLength;
        ch2Idx = new HashMap();
        str = characters;
        st = new Stack();
        result = "";
		// create the first combination
        for (Character ch : characters.toCharArray()) {
            st.push(ch);
            result = result + ch;
            if (st.size()==combinationLength) break;
        }
        int idx = 0;
		// set up the mapping between character --> index
        for (Character ch : characters.toCharArray()) {
            ch2Idx.put(ch, idx++);
        }
    }
    
    public String next() {
        String currResult = result;
        // process the next result
      
        int idx = str.length()-1;
		// keep on removing the last character from the stack/result till the position where idx can be moved ahead
        while (!st.isEmpty() && ch2Idx.get(st.peek())==idx) {
            st.pop();
            idx--;
            result = result.substring(0, result.length()-1);  
        }
        if (st.isEmpty()) return currResult; // there is no next result to pre-process
        
        idx = ch2Idx.get(st.pop()); // we need to add elements after this idx
        result = result.substring(0, result.length()-1);
        
        while (st.size()!=combLength) {
            Character temp = str.charAt(++idx);
            result+=temp;
            st.push(temp);
        }
        
        return currResult;
    }
    
    public boolean hasNext() {
        return !st.isEmpty();
    }
}


/**
 * Your CombinationIterator object will be instantiated and called as such:
 * CombinationIterator obj = new CombinationIterator(characters, combinationLength);
 * String param_1 = obj.next();
 * boolean param_2 = obj.hasNext();
 */
 ```
</p>


