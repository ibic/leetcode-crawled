---
title: "Buddy Strings"
weight: 803
#id: "buddy-strings"
---
## Description
<div class="description">
<p>Given two strings <code>A</code> and <code>B</code> of lowercase letters, return <code>true</code><em> if you can swap two letters in </em><code>A</code><em> so the result is equal to </em><code>B</code><em>, otherwise, return </em><code>false</code><em>.</em></p>

<p>Swapping letters is defined as taking two indices <code>i</code> and <code>j</code> (0-indexed) such that <code>i != j</code> and swapping the characters at <code>A[i]</code> and <code>A[j]</code>. For example, swapping at indices <code>0</code> and <code>2</code> in <code>&quot;abcd&quot;</code> results in <code>&quot;cbad&quot;</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> A = &quot;ab&quot;, B = &quot;ba&quot;
<strong>Output:</strong> true
<strong>Explanation</strong><strong>:</strong> You can swap A[0] = &#39;a&#39; and A[1] = &#39;b&#39; to get &quot;ba&quot;, which is equal to B.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> A = &quot;ab&quot;, B = &quot;ab&quot;
<strong>Output:</strong> false
<strong>Explanation</strong><strong>:</strong> The only letters you can swap are A[0] = &#39;a&#39; and A[1] = &#39;b&#39;, which results in &quot;ba&quot; != B.
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> A = &quot;aa&quot;, B = &quot;aa&quot;
<strong>Output:</strong> true
<strong>Explanation</strong><strong>:</strong> You can swap A[0] = &#39;a&#39; and A[1] = &#39;a&#39; to get &quot;aa&quot;, which is equal to B.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> A = &quot;aaaaaaabc&quot;, B = &quot;aaaaaaacb&quot;
<strong>Output:</strong> true
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> A = &quot;&quot;, B = &quot;aa&quot;
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= A.length &lt;= 20000</code></li>
	<li><code>0 &lt;= B.length &lt;= 20000</code></li>
	<li><code>A</code> and <code>B</code> consist of lowercase letters.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Facebook - 2 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Enumerate Cases

**Intuition**

If the characters at the index of `i` in both strings are identical, _i.e._  `A[i] == B[i]`, we call the characters at the index `i` as *matched*.

If swapping `A[i]` and `A[j]` would demonstrate that `A` and `B` are buddy strings, then `A[i] == B[j]` and `A[j] == B[i]`.  That means among the four free variables `A[i], A[j], B[i], B[j]`, there are only two cases: either `A[i] == A[j]` or not.

**Algorithm**

Let's work through the cases.

In the case `A[i] == A[j] == B[i] == B[j]`, then the strings `A` and `B` are equal.  So if `A == B`, we should check each index `i` for two matches with the same value.

In the case `A[i] == B[j], A[j] == B[i], (A[i] != A[j])`, the rest of the indices match. 
So if `A` and `B` have only two unmatched indices (say `i` and `j`), we should check that the equalities `A[i] == B[j]` and `A[j] == B[i]` hold.

<iframe src="https://leetcode.com/playground/Bva7gAhc/shared" frameBorder="0" width="100%" height="500" name="Bva7gAhc"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A` and `B`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy Understood
- Author: lee215
- Creation Date: Sun Jun 24 2018 11:07:46 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 21:54:35 GMT+0800 (Singapore Standard Time)

<p>
If `A.length() != B.length()`: no possible swap

If `A == B`, we need swap two same characters. Check is duplicated char in `A`.

In other cases, we find index for `A[i] != B[i]`. There should be only 2 diffs and it\'s our one swap.

**C++:**
```
    bool buddyStrings(string A, string B) {
        if (A.length() != B.length()) return false;
        if (A == B && set<char>(A.begin(), A.end()).size() < A.size()) return true;
        vector<int> dif;
        for (int i = 0; i < A.length(); ++i) if (A[i] != B[i]) dif.push_back(i);
        return dif.size() == 2 && A[dif[0]] == B[dif[1]] && A[dif[1]] == B[dif[0]];
    }
```

**Java:**
```
    public boolean buddyStrings(String A, String B) {
        if (A.length() != B.length()) return false;
        if (A.equals(B)) {
            Set<Character> s = new HashSet<Character>();
            for (char c : A.toCharArray()) s.add(c);
            return s.size() < A.length();
        }
        List<Integer> dif = new ArrayList<>();
        for (int i = 0; i < A.length(); ++i) if (A.charAt(i) != B.charAt(i)) dif.add(i);
        return dif.size() == 2 && A.charAt(dif.get(0)) == B.charAt(dif.get(1)) && A.charAt(dif.get(1)) == B.charAt(dif.get(0));
    }
```
**Python:**
```
    def buddyStrings(self, A, B):
        if len(A) != len(B): return False
        if A == B and len(set(A)) < len(A): return True
        dif = [(a, b) for a, b in zip(A, B) if a != b]
        return len(dif) == 2 and dif[0] == dif[1][::-1]
```
</p>


### Clear C++ Solution
- Author: dnuang
- Creation Date: Wed Jun 27 2018 12:19:59 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 05 2018 23:41:27 GMT+0800 (Singapore Standard Time)

<p>
```
    bool buddyStrings(string A, string B) {
        // Same length
        int len_A = A.size(), len_B = B.size();
        if (len_A != len_B) return false;
        
        // Repeat: same string, A needs repeated char, like "aab" "aab"
        int numChar_A = unordered_set<char>(A.begin(), A.end()).size();
        if (A == B && numChar_A < len_B) return true;
        
        // Swap: There should be only two to change
        vector<int> index_diff;
                        
        for (int i = 0; i < len_A; i++) {
          if (A[i] != B[i]) index_diff.push_back(i);
          if (index_diff.size() > 2) return false;
        } 
        
        return index_diff.size() == 2 &&
               A[index_diff[0]] == B[index_diff[1]] &&
               A[index_diff[1]] == B[index_diff[0]];        
    }
```
</p>


### Java O(1) space, O(n) time
- Author: xiyunyue
- Creation Date: Sun Jun 24 2018 11:19:11 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 14:52:45 GMT+0800 (Singapore Standard Time)

<p>
```
public boolean buddyStrings(String A, String B) {
    if (A == null || B == null || A.length() != B.length())  return false;
    int a = -1, b = -1, diff = 0;
    int[] count = new int[26];
    // check if able to switch with the same character.
    boolean canSwitch = false;
    for (int i = 0; i < A.length(); i++) {
      if (++count[A.charAt(i) - \'a\'] >= 2)  canSwitch = true;
      if (A.charAt(i) != B.charAt(i)) {
        diff++;
        if (a == -1)  a = i;
        else if (b == -1)  b = i;
      }
    }
    return (diff == 0 && canSwitch) || (diff == 2 && A.charAt(a) == B.charAt(b) && A.charAt(b) == B.charAt(a));
  }
```
</p>


