---
title: "Subarrays with K Different Integers"
weight: 943
#id: "subarrays-with-k-different-integers"
---
## Description
<div class="description">
<p>Given an array <code>A</code> of positive integers, call a (contiguous, not necessarily distinct) subarray of <code>A</code> <em>good</em> if the number of different integers in that subarray is exactly <code>K</code>.</p>

<p>(For example, <code>[1,2,3,1,2]</code> has <code>3</code> different integers: <code>1</code>, <code>2</code>, and <code>3</code>.)</p>

<p>Return the number of good subarrays of <code>A</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-1-1">[1,2,1,2,3]</span>, K = <span id="example-input-1-2">2</span>
<strong>Output: </strong><span id="example-output-1">7</span>
<strong>Explanation: </strong>Subarrays formed with exactly 2 different integers: [1,2], [2,1], [1,2], [2,3], [1,2,1], [2,1,2], [1,2,1,2].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>A = <span id="example-input-2-1">[1,2,1,3,4]</span>, K = <span id="example-input-2-2">3</span>
<strong>Output: </strong><span id="example-output-2">3</span>
<strong>Explanation: </strong>Subarrays formed with exactly 3 different integers: [1,2,1,3], [2,1,3], [1,3,4].
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= A.length &lt;= 20000</code></li>
	<li><code>1 &lt;= A[i] &lt;= A.length</code></li>
	<li><code>1 &lt;= K &lt;= A.length</code></li>
</ol>
</div>

## Tags
- Hash Table (hash-table)
- Two Pointers (two-pointers)
- Sliding Window (sliding-window)

## Companies
- Amazon - 4 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Sliding Window

**Intuition**

For convenience, let's denote subarrays by tuples: `(i,j) = [A[i], A[i+1], ..., A[j]]`, and call a subarray *valid* if it has `K` different integers.

For each `j`, let's consider the set $$S_j$$ of all `i` such that the subarray `(i, j)` is valid.

Firstly, $$S_j$$ must be a contiguous interval.  If `i1 < i2 < i3`, `(i1,j)` and `(i3,j)` are valid, but `(i2,j)` is not valid, this is a contradiction because `(i2,j)` must contain more than `K` different elements [as `(i3,j)` contains `K`], but `(i1,j)` [which is a superset of `(i2,j)`] only contains `K` different integers.

So now let's write $$S_j$$ as intervals: $$S_j = [\text{left1}_j, \text{left2}_j]$$.

The second observation is that the endpoints of these intervals must be monotone increeasing - namely, $$\text{left1}_j$$ and $$\text{left2}_j$$ are monotone increasing.  With similar logic to the above, we could construct a proof of this fact, but the intuition is that after adding an extra element to our subarrays, they are already valid, or we need to shrink them a bit to keep them valid.

**Algorithm**

We'll maintain two sliding windows, corresponding to $$\text{left1}_j$$ and $$\text{left2}_j$$.  Each sliding window will be able to count how many different elements there are in the window, and add and remove elements in a queue-like fashion.

<iframe src="https://leetcode.com/playground/wbwDHJg7/shared" frameBorder="0" width="100%" height="500" name="wbwDHJg7"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `A`.

* Space Complexity:  $$O(N)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Sliding Window
- Author: lee215
- Creation Date: Fri Feb 28 2020 17:04:36 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Feb 28 2020 17:05:29 GMT+0800 (Singapore Standard Time)

<p>
**I republished this post.
The original one is deleded by Leetcode without any notification or information.
The only reason that, I included my youtube channel link.
Excusem me, What the HACK!?**
<br>

## **Intuition**:
First you may have feeling of using sliding window.
Then this idea get stuck in the middle.

This problem will be a very typical sliding window,
if it asks the number of subarrays with **at most** K distinct elements.

Just need one more step to reach the folloing equation:
`exactly(K) = atMost(K) - atMost(K-1)`
<br>

## **Explanation**
1. Write/copy a helper function of sliding window,
to get the number of subarrays with at most K distinct elements.
2. Done.
<br>

## **Complexity**:
Time `O(N)` for two passes.
Space `O(K)` at most K elements in the counter

Of course, you can merge 2 for loops into one, if you like.
<br>

**Java:**
```java
    public int subarraysWithKDistinct(int[] A, int K) {
        return atMostK(A, K) - atMostK(A, K - 1);
    }
    int atMostK(int[] A, int K) {
        int i = 0, res = 0;
        Map<Integer, Integer> count = new HashMap<>();
        for (int j = 0; j < A.length; ++j) {
            if (count.getOrDefault(A[j], 0) == 0) K--;
            count.put(A[j], count.getOrDefault(A[j], 0) + 1);
            while (K < 0) {
                count.put(A[i], count.get(A[i]) - 1);
                if (count.get(A[i]) == 0) K++;
                i++;
            }
            res += j - i + 1;
        }
        return res;
    }
```

**C++:**
```cpp
    int subarraysWithKDistinct(vector<int>& A, int K) {
        return atMostK(A, K) - atMostK(A, K - 1);
    }
    int atMostK(vector<int>& A, int K) {
        int i = 0, res = 0;
        unordered_map<int, int> count;
        for (int j = 0; j < A.size(); ++j) {
            if (!count[A[j]]++) K--;
            while (K < 0) {
                if (!--count[A[i]]) K++;
                i++;
            }
            res += j - i + 1;
        }
        return res;
    }
```

**Python:**
```python
    def subarraysWithKDistinct(self, A, K):
        return self.atMostK(A, K) - self.atMostK(A, K - 1)

    def atMostK(self, A, K):
        count = collections.Counter()
        res = i = 0
        for j in range(len(A)):
            if count[A[j]] == 0: K -= 1
            count[A[j]] += 1
            while K < 0:
                count[A[i]] -= 1
                if count[A[i]] == 0: K += 1
                i += 1
            res += j - i + 1
        return res
```
<br>

# More Similar Sliding Window Problems
Here are some similar sliding window problems.
Also find more explanations.
Good luck and have fun.


- 1358. [Number of Substrings Containing All Three Characters](https://leetcode.com/problems/number-of-substrings-containing-all-three-characters/discuss/516977/JavaC++Python-Easy-and-Concise)
- 1248. [Count Number of Nice Subarrays](https://leetcode.com/problems/count-number-of-nice-subarrays/discuss/419378/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
- 1234. [Replace the Substring for Balanced String](https://leetcode.com/problems/replace-the-substring-for-balanced-string/discuss/408978/javacpython-sliding-window/367697)
- 1004. [Max Consecutive Ones III](https://leetcode.com/problems/max-consecutive-ones-iii/discuss/247564/javacpython-sliding-window/379427?page=3)
-  930. [Binary Subarrays With Sum](https://leetcode.com/problems/binary-subarrays-with-sum/discuss/186683/)
-  992. [Subarrays with K Different Integers](https://leetcode.com/problems/subarrays-with-k-different-integers/discuss/234482/JavaC%2B%2BPython-Sliding-Window-atMost(K)-atMost(K-1))
-  904. [Fruit Into Baskets](https://leetcode.com/problems/fruit-into-baskets/discuss/170740/Sliding-Window-for-K-Elements)
-  862. [Shortest Subarray with Sum at Least K](https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/discuss/143726/C%2B%2BJavaPython-O(N)-Using-Deque)
-  209. [Minimum Size Subarray Sum](https://leetcode.com/problems/minimum-size-subarray-sum/discuss/433123/JavaC++Python-Sliding-Window)
<br>
</p>


### C++/Java with picture, prefixed sliding window
- Author: votrubac
- Creation Date: Mon Feb 11 2019 14:05:33 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 11 2019 14:05:33 GMT+0800 (Singapore Standard Time)

<p>
If the problem talks about continuous subarrays or substrings, the sliding window technique may help solve it in a linear time. Such problems are tricky, though the solution is simple once you get it. No wonder I am seeing such problems in almost every interview!

Here, we will take a look at [Subarrays with K Different Integers](https://leetcode.com/problems/subarrays-with-k-different-integers/) (LeetCode Hard), which appeared on LeetCode [weekly contest #123](https://leetcode.com/contest/weekly-contest-123). You can also master the sliding windows technique with these additional problems:
- [Longest Substring Without Repeating Characters](https://leetcode.com/problems/longest-substring-without-repeating-characters/)
- [Longest Substring with At Most Two Distinct Characters](https://leetcode.com/problems/longest-substring-with-at-most-two-distinct-characters/)
- [Longest Substring with At Most K Distinct Characters](https://leetcode.com/problems/longest-substring-with-at-most-k-distinct-characters/)
<!--# Problem Description
Given an array ```A``` of positive integers, call a (contiguous, not necessarily distinct) subarray of ```A``` good if the number of different integers in that subarray is exactly ```K```. For example, ```[1,2,3,1,2]``` has 3 different integers: 1, 2, and 3.

Return the number of good subarrays of ```A```.
### Example
**Input:** A = [1,2,1,2,3], K = 2
**Output:** 7
**Explanation:** Subarrays formed with exactly 2 different integers: [1,2], [2,1], [1,2], [2,3], [1,2,1], [2,1,2], [1,2,1,2].
# Coding Practice
Try solving this problem before moving on to the solutions. It is available on LeetCode Online Judge ([Subarrays with K Different Integers](https://leetcode.com/problems/subarrays-with-k-different-integers/)). Also, as you read through a solution, try implementing it yourself.

LeetCode is my favorite destinations for algorithmic problems. It has 987 problems and counting, with test cases to validate the correctness, as well as computational and memory complexity. There is also a large community discussing different approaches, tips and tricks.
# Brute-Force Solution
We can just iterate through each sub-array using two loops and count the good ones. For the inner loop, we use hash set to count unique numbers; if we the set size becomes larger than ```K```, we break from the inner loop.
```
int subarraysWithKDistinct(vector<int>& A, int K, int res = 0) {
  for (auto i = 0; i < A.size(); ++i) {
    unordered_set<int> s;
    for (auto j = i; j < A.size() && s.size() <= K; ++j) {
      s.insert(A[j]);
      if (s.size() == K) ++res;
    }
  }
  return res;
}
```
### Complexity Analysis
The time complexity of this solution is *O(n * n)*, where *n* is the length of ```A```. This solution is not accepted by the online judge.
-->
# Intuition
If the subarray ```[j, i]``` contains ```K``` unique numbers, and first ```prefix``` numbers also appear in ```[j + prefix, i]``` subarray, we have total ```1 + prefix``` good subarrays. For example, there are 3 unique numers in ```[1, 2, 1, 2, 3]```. First two numbers also appear in the remaining subarray ```[1, 2, 3]```, so we have 1 + 2 good subarrays: ```[1, 2, 1, 2, 3]```, ```[2, 1, 2, 3]``` and ```[1, 2, 3]```.
# Linear Solution
We can iterate through the array and use two pointers for our sliding window (```[j, i]```). The back of the window is always the current position in the array (```i```). The front of the window (```j```) is moved so that A[j] appear only once in the sliding window. In other words, we are trying to shrink our sliding window while maintaining the same number of unique elements.

To do that, we keep tabs on how many times each number appears in our window (```m```). After we add next number to the back of our window, we try to remove as many as possible numbers from the front, until the number in the front appears only once. While removing numbers, we are increasing ```prefix```.

If we collected ```K``` unique numbers, then we found 1 + ```prefix``` sequences, as each removed number would also form a sequence.

If our window reached ```K + 1``` unique numbers, we remove one number from the head (again, that number appears only in the front), and reset ```prefix``` as now we are starting a new sequence. This process is demonstrated step-by-step for the test case below; ```prefix``` are shown as ```+1``` in the green background.
```
[5,7,5,2,3,3,4,1,5,2,7,4,6,2,3,8,4,5,7]
7
```
![image](https://assets.leetcode.com/users/votrubac/image_1549876616.png)

In the code below, we use ```cnt``` to track unique numbers. Since ```1 <= A[i] <= A.size()```, we can use an array instead of hash map to improve the performance.
```
int subarraysWithKDistinct(vector<int>& A, int K, int res = 0) {
  vector<int> m(A.size() + 1);
  for(auto i = 0, j = 0, prefix = 0, cnt = 0; i < A.size(); ++i) {
    if (m[A[i]]++ == 0) ++cnt;
    if (cnt > K) --m[A[j++]], --cnt, prefix = 0;
    while (m[A[j]] > 1) ++prefix, --m[A[j++]];
    if (cnt == K) res += prefix + 1;
  }
  return res;
}
```
Java version:
```
public int subarraysWithKDistinct(int[] A, int K) {
  int res = 0, prefix = 0;
  int[] m = new int[A.length + 1];
  for (int i = 0, j = 0, cnt = 0; i < A.length; ++i) {
    if (m[A[i]]++ == 0) ++cnt;
    if (cnt > K) {
      --m[A[j++]]; --cnt; prefix = 0; 
    }
    while (m[A[j]] > 1) {
      ++prefix; --m[A[j++]]; 
    }
    if (cnt == K) res += prefix + 1;
  }
  return res;
} 
```
### Complexity Analysis
- Time Complexity: *O(n)*, where *n* is the length of ```A```.
- Space Complexity: *O(n)*.
</p>


### One code template to solve all of these problems!
- Author: Lisanaaa
- Creation Date: Mon Feb 11 2019 06:31:17 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 11 2019 06:31:17 GMT+0800 (Singapore Standard Time)

<p>
For [992. Subarrays with K Different Integers](https://leetcode.com/problems/subarrays-with-k-different-integers/):

```python
class Solution:
    def subarraysWithKDistinct(self, A: \'List[int]\', K: \'int\') -> \'int\':
        return self.subarraysWithAtMostKDistinct(A, K) - self.subarraysWithAtMostKDistinct(A, K-1)
    
    def subarraysWithAtMostKDistinct(self, s, k):
        lookup = collections.defaultdict(int)
        l, r, counter, res = 0, 0, 0, 0
        while r < len(s):
            lookup[s[r]] += 1
            if lookup[s[r]] == 1:
                counter += 1
            r += 1   
            while l < r and counter > k:
                lookup[s[l]] -= 1
                if lookup[s[l]] == 0:
                    counter -= 1
                l += 1
            res += r - l 
        return res
```

For [3. Longest Substring Without Repeating Characters](https://leetcode.com/problems/longest-substring-without-repeating-characters/):

```python
class Solution:
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        lookup = collections.defaultdict(int)
        l, r, counter, res = 0, 0, 0, 0
        while r < len(s):
            lookup[s[r]] += 1
            if lookup[s[r]] == 1:
                counter += 1
            r += 1
            while l < r and counter < r - l:
                lookup[s[l]] -= 1
                if lookup[s[l]] == 0:
                    counter -= 1
                l += 1
            res = max(res, r - l)
        return res
```

For [159. Longest Substring with At Most Two Distinct Characters](https://leetcode.com/problems/longest-substring-with-at-most-two-distinct-characters/):

```python
class Solution(object):
    def lengthOfLongestSubstringTwoDistinct(self, s):
        """
        :type s: str
        :rtype: int
        """
        lookup = collections.defaultdict(int)
        l, r, counter, res = 0, 0, 0, 0
        while r < len(s):
            lookup[s[r]] += 1
            if lookup[s[r]] == 1:
                counter += 1
            r += 1   
            while l < r and counter > 2:
                lookup[s[l]] -= 1
                if lookup[s[l]] == 0:
                    counter -= 1
                l += 1
            res = max(res, r - l) 
        return res
```

For [340. Longest Substring with At Most K Distinct Characters](https://leetcode.com/problems/longest-substring-with-at-most-k-distinct-characters/):

```python
class Solution(object):
    def lengthOfLongestSubstringKDistinct(self, s, k):
        """
        :type s: str
        :type k: int
        :rtype: int
        """
        lookup = collections.defaultdict(int)
        l, r, counter, res = 0, 0, 0, 0
        while r < len(s):
            lookup[s[r]] += 1
            if lookup[s[r]] == 1:
                counter += 1
            r += 1   
            while l < r and counter > k:
                lookup[s[l]] -= 1
                if lookup[s[l]] == 0:
                    counter -= 1
                l += 1
            res = max(res, r - l) 
        return res
```
</p>


