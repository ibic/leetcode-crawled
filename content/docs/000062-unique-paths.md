---
title: "Unique Paths"
weight: 62
#id: "unique-paths"
---
## Description
<div class="description">
<p>A robot is located at the top-left corner of a <code>m x n</code> grid (marked &#39;Start&#39; in the diagram below).</p>

<p>The robot can only move either down or right at any point in time. The robot is trying to reach the bottom-right corner of the grid (marked &#39;Finish&#39; in the diagram below).</p>

<p>How many possible unique paths are there?</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img src="https://assets.leetcode.com/uploads/2018/10/22/robot_maze.png" style="width: 400px; height: 183px;" />
<pre>
<strong>Input:</strong> m = 3, n = 7
<strong>Output:</strong> 28
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> m = 3, n = 2
<strong>Output:</strong> 3
<strong>Explanation:</strong>
From the top-left corner, there are a total of 3 ways to reach the bottom-right corner:
1. Right -&gt; Down -&gt; Down
2. Down -&gt; Down -&gt; Right
3. Down -&gt; Right -&gt; Down
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> m = 7, n = 3
<strong>Output:</strong> 28
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> m = 3, n = 3
<strong>Output:</strong> 6
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= m, n &lt;= 100</code></li>
	<li>It&#39;s guaranteed that the answer will be less than or equal to <code>2 * 10<sup>9</sup></code>.</li>
</ul>

</div>

## Tags
- Array (array)
- Dynamic Programming (dynamic-programming)

## Companies
- Amazon - 5 (taggedByAdmin: false)
- Facebook - 4 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Apple - 5 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- Mathworks - 5 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Oracle - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: true)
- Walmart Labs - 2 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Alibaba - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Snapchat - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

--- 

#### Overview

Since robot can move either down or right, there is only one path
to reach the cells in the first row: right->right->...->right.

![traversal](../Figures/62/first_row2.png)

The same is valid for the first column, though the path here is down->down->
...->down.

![traversal](../Figures/62/first_col2.png)

What about the "inner" cells `(m, n)`? To such cell one could move 
either from the upper cell `(m, n - 1)`, or from the cell on the right
`(m - 1, n)`. That means that the total number of paths to move into `(m, n)` cell
is `uniquePaths(m - 1, n) + uniquePaths(m, n - 1)`. 

![traversal](../Figures/62/inner_cell2.png)

Now, one could transform these ideas into 3-liner recursive solution:

<iframe src="https://leetcode.com/playground/nKraafNc/shared" frameBorder="0" width="100%" height="191" name="nKraafNc"></iframe>

This solution is not fast enough to pass all the testcases, though it 
could be used as a starting point for the DP solution.
<br />
<br />


---
#### Approach 1: Dynamic Programming

One could rewrite recursive approach into dynamic programming one.

**Algorithm**

- Initiate 2D array `d[m][n] = number of paths`. To start, put number of paths
equal to 1 for the first row and the first column.
For the simplicity, one could initiate the whole 2D array by ones.

- Iterate over all "inner" cells: `d[col][row] = d[col - 1][row] + d[col][row - 1]`.

- Return `d[m - 1][n - 1]`. 

**Implementation**

!?!../Documents/62_LIS.json:1000,313!?!

<iframe src="https://leetcode.com/playground/nCRoSq9f/shared" frameBorder="0" width="100%" height="310" name="nCRoSq9f"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}(N \times M)$$.

* Space complexity: $$\mathcal{O}(N \times M)$$.
<br /> 
<br />


---
#### Approach 2: Math (Python3 only)

Could one do better than $$\mathcal{O}(N \times M)$$? The answer is yes.

The problem is a classical combinatorial problem: there are 
$$h + v$$ moves to do from start to finish, $$h = m - 1$$ horizontal moves,
and $$v = n - 1$$ vertical ones. 
One could choose when to move to the right, 
i.e. to define $$h$$ horizontal moves, and that will fix vertical ones.
Or, one could choose when to move down, 
i.e. to define $$v$$ vertical moves, and that will fix horizontal ones.

![traversal](../Figures/62/bin4.png)

In other words, we're asked to compute in how many ways one could 
choose $$p$$ elements from $$p + k$$ elements.
In mathematics, that's called [binomial coefficients](https://en.wikipedia.org/wiki/Binomial_coefficient)

$$
C_{h + v}^{h} = C_{h + v}^{v} = \frac{(h + v)!}{h! v!}
$$

The number of horizontal moves to do is $$h = m - 1$$, the number of vertical 
moves is $$v = n - 1$$. That results in a simple formula

$$
C_{h + v}^{h} = \frac{(m + n - 2)!}{(m - 1)! (n - 1)!}
$$ 

The job is done.
Now time complexity will depend on the algorithm to compute factorial 
function $$(m + n - 2)!$$. 
In short, standard computation for $$k!$$ using the definition requires
$$\mathcal{O}(k^2 \log k)$$ time, and that will be not as good as DP algorithm.

[The best known algorithm to compute factorial function is done by Peter Borwein](http://www.cecm.sfu.ca/personal/pborwein/PAPERS/P29.pdf).
The idea is to express the factorial as a product of prime powers,
so that $$k!$$ can be computed in $$\mathcal{O}(k (\log k \log \log k)^2)$$ time.
That's better than $$\mathcal{O}(k^2)$$ and hence beats DP algorithm.

The authors prefer not to discuss here various factorial function implementations,
and hence provide Python3 solution only, with built-in
[divide and conquer factorial algorithm](https://bugs.python.org/issue8692).
If you're interested in factorial algorithms, 
please check out good review on [this page](http://www.luschny.de/math/factorial/description.html).

**Implementation**

<iframe src="https://leetcode.com/playground/EFCQvLdQ/shared" frameBorder="0" width="100%" height="123" name="EFCQvLdQ"></iframe>

**Complexity Analysis**

* Time complexity: $$\mathcal{O}((M + N) (\log (M + N) \log \log (M + N))^2)$$.

* Space complexity: $$\mathcal{O}(1)$$.
<br /> 
<br />

## Accepted Submission (java)
```java
public class Solution {
    public int uniquePaths(int m, int n) {
        int[][] maze = new int[m][n];
        for (int i = 0; i < m; i++) {
            maze[i][0] = 1;
        }
        for (int j = 0; j < n; j++) {
            maze[0][j] = 1;
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                maze[i][j] = maze[i - 1][j] + maze[i][j - 1];
            }
        }
        return maze[m - 1][n - 1];
    }
}
```

## Top Discussions
### C++ DP
- Author: jianchao-li
- Creation Date: Tue Jun 02 2015 15:24:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 11:54:46 GMT+0800 (Singapore Standard Time)

<p>
Since the robot can only move right and down, when it arrives at a point, it either arrives from left or above. If we use `dp[i][j]` for the number of unique paths to arrive at the point `(i, j)`, then the state equation is `dp[i][j] = dp[i][j - 1] + dp[i - 1][j]`. Moreover, we have the base cases `dp[0][j] = dp[i][0] = 1` for all valid `i` and `j`.

```cpp
class Solution {
public:
    int uniquePaths(int m, int n) {
        vector<vector<int>> dp(m, vector<int>(n, 1));
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }
        return dp[m - 1][n - 1];
    }
};
```

The above solution runs in `O(m * n)` time and costs `O(m * n)` space. However, you may have noticed that each time when we update `dp[i][j]`, we only need `dp[i - 1][j]` (at the previous row) and `dp[i][j - 1]` (at the current row). So we can reduce the memory usage to just two rows (`O(n)`).

```cpp
class Solution {
public:
    int uniquePaths(int m, int n) {
        vector<int> pre(n, 1), cur(n, 1);
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                cur[j] = pre[j] + cur[j - 1];
            }
            swap(pre, cur);
        }
        return pre[n - 1];
    }
};
```

Further inspecting the above code, `pre[j]` is just the `cur[j]` before the update. So we can further reduce the memory usage to one row.

```
class Solution {
public:
    int uniquePaths(int m, int n) {
        vector<int> cur(n, 1);
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                cur[j] += cur[j - 1];
            }
        }
        return cur[n - 1];
    }
};
```

Now, you may wonder whether we can further reduce the memory usage to just `O(1)` space since the above code seems to use only two variables (`cur[j]` and `cur[j - 1]`). However, since the whole row `cur` needs to be updated for `m - 1` times (the outer loop) based on old values, all of its values need to be saved and thus `O(1)`-space is impossible. However, if you are having a DP problem without the outer loop and just the inner one, then it will be possible.
</p>


### My AC solution using formula
- Author: d40a
- Creation Date: Wed Aug 13 2014 21:33:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 21:30:54 GMT+0800 (Singapore Standard Time)

<p>
Binomial coefficient:
 

    class Solution {
        public:
            int uniquePaths(int m, int n) {
                int N = n + m - 2;// how much steps we need to do
                int k = m - 1; // number of steps that need to go down
                double res = 1;
                // here we calculate the total possible path number 
                // Combination(N, k) = n! / (k!(n - k)!)
                // reduce the numerator and denominator and get
                // C = ( (n - k + 1) * (n - k + 2) * ... * n ) / k!
                for (int i = 1; i <= k; i++)
                    res = res * (N - k + i) / i;
                return (int)res;
            }
        };

First of all you should understand that we need to do n + m - 2 movements : m - 1 down, n - 1 right, because we start from cell (1, 1).

Secondly, the path it is the sequence of movements( go down / go right), 
therefore we can say that two paths are different 
when there is  i-th (1 .. m + n - 2)  movement in path1 differ  i-th movement in path2.

So, how we can build paths.
Let's choose (n - 1) movements(number of steps to the right) from (m + n - 2), 
and rest (m - 1) is (number of steps down).

I think now it is obvious that count of different paths are all combinations (n - 1) movements from (m + n-2).
</p>


### Math solution, O(1) space
- Author: whitehat
- Creation Date: Fri Jul 24 2015 10:41:15 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 04:52:53 GMT+0800 (Singapore Standard Time)

<p>
This is a combinatorial problem and can be solved without DP. For mxn grid, robot has to move exactly m-1 steps down and n-1 steps right and these can be done in any order.

For the eg., given in question, 3x7 matrix, robot needs to take 2+6 = 8 steps with 2 down and 6 right in any order. That is nothing but a permutation problem. Denote down as 'D' and right as 'R', following is one of the path :-

D R R R D R R R

We have to tell the total number of permutations of the above given word. So, decrease both m & n by 1 and apply following formula:-

Total permutations = (m+n)! / (m! * n!)

Following is my code doing the same :-

    public class Solution {
        public int uniquePaths(int m, int n) {
            if(m == 1 || n == 1)
                return 1;
            m--;
            n--;
            if(m < n) {              // Swap, so that m is the bigger number
                m = m + n;
                n = m - n;
                m = m - n;
            }
            long res = 1;
            int j = 1;
            for(int i = m+1; i <= m+n; i++, j++){       // Instead of taking factorial, keep on multiply & divide
                res *= i;
                res /= j;
            }
                
            return (int)res;
        }
    }
</p>


