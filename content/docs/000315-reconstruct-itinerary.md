---
title: "Reconstruct Itinerary"
weight: 315
#id: "reconstruct-itinerary"
---
## Description
<div class="description">
<p>Given a list of airline tickets represented by pairs of departure and arrival airports <code>[from, to]</code>, reconstruct the itinerary in order. All of the tickets belong to a man who departs from <code>JFK</code>. Thus, the itinerary must begin with <code>JFK</code>.</p>

<p><b>Note:</b></p>

<ol>
	<li>If there are multiple valid itineraries, you should return the itinerary that has the smallest lexical order when read as a single string. For example, the itinerary <code>[&quot;JFK&quot;, &quot;LGA&quot;]</code> has a smaller lexical order than <code>[&quot;JFK&quot;, &quot;LGB&quot;]</code>.</li>
	<li>All airports are represented by three capital letters (IATA code).</li>
	<li>You may assume all tickets form at least one valid itinerary.</li>
	<li>One must use all the tickets once and only once.</li>
</ol>

<p><b>Example 1:</b></p>

<pre>
<code><strong>Input: </strong></code><code>[[&quot;MUC&quot;, &quot;LHR&quot;], [&quot;JFK&quot;, &quot;MUC&quot;], [&quot;SFO&quot;, &quot;SJC&quot;], [&quot;LHR&quot;, &quot;SFO&quot;]]</code>
<strong>Output: </strong><code>[&quot;JFK&quot;, &quot;MUC&quot;, &quot;LHR&quot;, &quot;SFO&quot;, &quot;SJC&quot;]</code>
</pre>

<p><b>Example 2:</b></p>

<pre>
<code><strong>Input: </strong></code><code>[[&quot;JFK&quot;,&quot;SFO&quot;],[&quot;JFK&quot;,&quot;ATL&quot;],[&quot;SFO&quot;,&quot;ATL&quot;],[&quot;ATL&quot;,&quot;JFK&quot;],[&quot;ATL&quot;,&quot;SFO&quot;]]</code>
<strong>Output: </strong><code>[&quot;JFK&quot;,&quot;ATL&quot;,&quot;JFK&quot;,&quot;SFO&quot;,&quot;ATL&quot;,&quot;SFO&quot;]</code>
<strong>Explanation: </strong>Another possible reconstruction is <code>[&quot;JFK&quot;,&quot;SFO&quot;,&quot;ATL&quot;,&quot;JFK&quot;,&quot;ATL&quot;,&quot;SFO&quot;]</code>.
&nbsp;            But it is larger in lexical order.
</pre>

</div>

## Tags
- Depth-first Search (depth-first-search)
- Graph (graph)

## Companies
- Facebook - 7 (taggedByAdmin: false)
- Bloomberg - 3 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Uber - 2 (taggedByAdmin: false)
- Twilio - 6 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Goldman Sachs - 4 (taggedByAdmin: false)
- Qualtrics - 3 (taggedByAdmin: false)
- Apple - 3 (taggedByAdmin: false)
- Citadel - 3 (taggedByAdmin: false)
- Yandex - 2 (taggedByAdmin: false)
- Square - 2 (taggedByAdmin: false)
- Yelp - 5 (taggedByAdmin: false)
- Snapchat - 5 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Overview

Overall, we could consider this problem as a **graph traversal** problem, where an airport can be viewed as a *vertex* in graph and flight between airports as an *edge* in graph.

![pic](../Figures/332/332_graph.png)

We would like to make a few clarification on the input of the problem, since it is not clear in the description of the problem.

As one might notice in the above example, the input graph is NOT what we call a **DAG** (_Directed Acyclic Graph_), since we could find at least a cycle in the graph.

In addition, the graph could even have some duplicate edges (_i.e._ we might have multiple flights with the same origin and destination).
<br/>
<br/>

---
#### Approach 1: Backtracking + Greedy

**Intuition**

As common strategies for problems of graph traversal, we often apply the methodologies of **backtracking** or **greedy**. As it turns out, we can apply both of them for this problem.

>Typically, [backtracking](https://leetcode.com/explore/learn/card/recursion-ii/472/backtracking/) is used to enumerate all possible solutions for a problem, in a trial-fail-and-fallback strategy.

At each airport, one might have several possible destinations to fly to. With backtracking, we enumerate each possible destination. We mark the choice at each iteration (_i.e._ trial) before we move on to the chosen destination. If the destination does not lead to a solution (_i.e._ fail), we would then _fallback_ to the previous state and start another iteration of trial-fail-and-fallback cycle. 

>A [greedy algorithm](https://en.wikipedia.org/wiki/Greedy_algorithm) is any algorithm that follows the problem-solving _heuristic_ of making locally optimal choice at each step, with the intent of reaching the global optimum at the end.

As suggested by its definition, a greedy algorithm does not necessarily lead to a globally optimal solution, but rather a reasonable approximation in exchange of less computing time.

Nonetheless, sometimes it is the way to produce a global optimum for certain problems. This is the case for this problem as well.

At each airport, given a list of possible destinations, while backtracking, at each step we would pick the destination **_greedily_** in lexical order, _i.e._ the one with the smallest lexical order would have its trial first.

With this **_greedy_** strategy, we would ensure that the final solution that we find would have the *smallest lexical order*, because all other solutions that have smaller lexical order have been trialed and failed during the process of backtracking. 


**Algorithm**

Here we explain how we implement a solution for this problem, by combining the strategies of backtracking and greedy.

- As the first step, we build a graph data structure from the given input. This graph should allow us to quickly identify a list of potential destinations, given an origin. Here we adopted the hashmap (or dictionary) data structure, with each entry as `<origin, [destinations]>`. 

- Then due to our greedy strategy, we then should order the destination list for each entry in lexical order. As an alternative solution, one could use `PriorityQueue` data structure in the first step to keep the list of destinations, which would maintain the order at the moment of constructing the list. 

- As the final step, we kick off the backtracking traversal on the above graph, to obtain the final result.

    - At the beginning of the backtracking function, as the bottom case, we check if we have already obtained a valid itinerary.

    - Otherwise, we enumerate the next destinations in order.

    - We mark the status of visit, before and after each backtracking loop.

!?!../Documents/332_LIS.json:1000,317!?!

Note that there is certain code pattern that one can follow in order to implement an algorithm of backtracking. We provide an example in the [Explore card of Recursion II](https://leetcode.com/explore/learn/card/recursion-ii/472/backtracking/2793/).


<iframe src="https://leetcode.com/playground/9SSu2Q26/shared" frameBorder="0" width="100%" height="500" name="9SSu2Q26"></iframe>


**Complexity**

- Time Complexity: $$\mathcal{O}(|E|^d)$$ where $$|E|$$ is the number of total flights and $$d$$ is the maximum number of flights from an airport.

    - It is tricky to estimate the time complexity of the backtracking algorithm, since the algorithm often has an early stopping depending on the input.

    - To calculate a loose upper bound for the time complexity, let us consider it as a combination problem where the goal is to construct a sequence of a specific order, _i.e._ $$|V_1V_2...V_n|$$. For each position  $$V_i$$, we could have $$d$$ choices, _i.e._ at each airport one could have at most $$d$$ possible destinations. Since the length of the sequence is $$|E|$$, the total number of combination would be $$|E|^d$$.

    - In the worst case, our backtracking algorithm would have to enumerate all possible combinations.


- Space Complexity: $$\mathcal{O}(|V| + |E|)$$ where $$|V|$$ is the number of airports and $$|E|$$ is the number of flights.

    - In the algorithm, we use the graph as well as the visit bitmap, which would require the space of $$|V| + |E|$$.

    - Since we applied recursion in the algorithm, which would incur additional memory consumption in the function call stack. The maximum depth of the recursion would be exactly the number of flights in the input, _i.e._ $$|E|$$.

    - As a result, the total space complexity of the algorithm would be $$\mathcal{O}(|V| + 2\cdot|E|) = \mathcal{O}(|V| + |E|)$$.
<br/>
<br/>

---
#### Approach 2: Hierholzer's Algorithm

**Eulerian Cycle**

>In graph theory, an Eulerian trail (or **Eulerian path**) is a trail in a finite graph that visits every edge exactly once (allowing for revisiting vertices). 

In our problem, we are asked to construct an itinerary that uses all the flights (edges), starting from the airport of "JFK". 
As one can see, the problem is actually a variant of [Eulerian path](https://en.wikipedia.org/wiki/Eulerian_path), with a fixed starting point.


>Similarly, an Eulerian circuit or **Eulerian cycle** is an Eulerian trail that starts and ends on the same vertex.

The Eulerian cycle problem has been discussed by [Leonhard Euler](https://en.wikipedia.org/wiki/Leonhard_Euler) back in 1736. Ever since, there have been several algorithms proposed to solve the problem.

In 1873, Hierholzer proposed an efficient algorithm to find the Eulerian cycle in linear time ($$\mathcal{O}(|E|)$$).
One could find more details about the Hierholzer's algorithm in this [course](https://www-m9.ma.tum.de/graph-algorithms/hierholzer/index_en.html).

>The basic idea of Hierholzer's algorithm is the stepwise construction of the Eulerian cycle by connecting _disjunctive circles_.

To be more specific, the algorithm consists of two steps:

* It starts with a random node and then follows an arbitrary unvisited edge to a neighbor. This step is repeated until one returns to the starting node. This yields a first circle in the graph.
<br/>

* If this circle covers all nodes it is an Eulerian cycle and the algorithm is finished. Otherwise, one chooses another node among the cycles' nodes with unvisited edges and constructs another circle, called subtour.

![pic](../Figures/332/332_eulerian_cycle.png)

By connecting all the circles in the above process, we build the Eulerian cycle at the end.

**Eulerian Path**

>To find the Eulerian path, inspired from the original Hierzolher's algorithm, we simply change one condition of loop, rather than stopping at the starting point, we stop at the vertex where we do not have any unvisited edges.

To summarize, the main idea to find the Eulerian path consists of two steps:

- Step 1). Starting from any vertex, we keep following the unused edges until we get **stuck** at certain vertex where we have no more unvisited outgoing edges.

- Step 2). We then backtrack to the nearest neighbor vertex in the current path that has unused edges and we **repeat** the process until all the edges have been used.

The first vertex that we got stuck at would be the **end point** of our _**Eulerian path**_. So if we follow all the stuck _points_ backwards, we could reconstruct the Eulerian path at the end.


**Algorithm**

Now let us get back to our itinerary reconstruction problem. As we know now, it is a problem of Eulerian path, except that we have a fixed starting point.

More importantly, as stated in the problem, the given input is guaranteed to have a solution. So we have one less issue to consider.

As a result, our final algorithm is a bit simpler than the above Eulerian path algorithm, without the backtracking step.

>The essential step is that starting from the fixed starting vertex (airport 'JFK'), we keep following the *ordered* and *unused* edges (flights) until we get **stuck** at certain vertex where we have no more unvisited outgoing edges.

The point that we got stuck would be the _last_ airport that we visit. And then we follow the visited vertex (airport) **backwards**, we would obtain the final itinerary.

Here are some sample implementations which are inspired from a [thread of discussion](https://leetcode.com/problems/reconstruct-itinerary/discuss/78768/Short-Ruby-Python-Java-C%2B%2B) in the forum. 

<iframe src="https://leetcode.com/playground/xd6pksyz/shared" frameBorder="0" width="100%" height="500" name="xd6pksyz"></iframe>


**Discussion**

To better understand the above algorithm, we could look at it from another perspective.

>Actually, we could consider the algorithm as the **postorder DFS** (Depth-First Search) in a directed graph, from a fixed starting point.

As we know that, each input is guaranteed to have a solution. Therefore, the task of the problem can be interpreted as that given a list of flights (_i.e._ edges in graph), we should find an order to use each flight _once and only once_.

In the resulted path, before we visit the last airport (denoted as `V`), we can say that we have already used all the rest flights, _i.e._ if there is any flight starting from `V`, then we must have already taken that before.

>Or to put it another way, before adding the last airport (vertex) in the final path, we have visited all its **outgoing** vertex.

Actually, the above statement applies to each airport in the final itinerary. _Before adding an airport into the final itinerary, we must first visit all its outgoing neighbor vertex._

If we consider the outgoing vertex in a directed graph as children nodes in a tree, one could see the reason why we could consider the algorithm as a sort of **postorder DFS traversal** in a tree.

!?!../Documents/332_RES.json:1000,354!?!


**Complexity**

- Time Complexity: $$\mathcal{O}(|E| \log{\frac{|E|}{|V|}})$$ where $$|E|$$ is the number of edges (flights) in the input.

    - As one can see from the above algorithm, during the DFS process, we would traverse each edge once. Therefore, the complexity of the DFS function would be $$|E|$$.

    - However, before the DFS, we need to sort the outgoing edges for each vertex. And this, unfortunately, dominates the overall complexity.

    - It is though tricky to estimate the complexity of sorting, which depends on the structure of the input graph.

    - In the worst case where the graph is not balanced, _i.e._ the connections are concentered in a single airport. Imagine the graph is of star shape, in this case, the JFK airport would assume half of the flights (since we still need the return flight). As a result, the sorting operation on this airport would be exceptionally expensive, _i.e._ $$N \log{N}$$, where $$N = \frac{|E|}{2}$$. And this would be the final complexity as well, since it dominates the rest of the calculation.

    - Let us consider a less bad case, or an average case, where the graph is less clustered, _i.e._ each node has the equal number of outgoing flights. Under this assumption, each airport would have $$\frac{|E|}{(2\cdot|V|)}$$ number of flights (still we need the return flights). Again, we can plug it into the $$N \log N$$ minimal sorting complexity. In addition, this time, we need to take into consideration all airports, rather than the superhub (JFK) in the above case. As a result, we have $$|V| \cdot (N \log N)$$, where $$N = \frac{|E|}{2\cdot|V|}$$. If we expand the formula, we will obtain the complexity of the average case as $$ \mathcal{O}(\frac{|E|}{2} \log{\frac{|E|}{2\cdot|V|}}) = \mathcal{O}(|E| \log{\frac{|E|}{|V|}})$$


- Space Complexity: $$\mathcal{O}(|V| + |E|)$$ where $$|V|$$ is the number of airports and $$|E|$$ is the number of flights.

    - In the algorithm, we use the graph, which would require the space of $$|V| + |E|$$.

    - Since we applied recursion in the algorithm, which would incur additional memory consumption in the function call stack. The maximum depth of the recursion would be exactly the number of flights in the input, _i.e._ $$|E|$$.

    - As a result, the total space complexity of the algorithm would be $$\mathcal{O}(|V| + 2\cdot|E|) = \mathcal{O}(|V| + |E|)$$.
<br/>
<br/>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Short Ruby / Python / Java / C++
- Author: StefanPochmann
- Creation Date: Fri Feb 05 2016 00:32:53 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jan 20 2020 01:13:40 GMT+0800 (Singapore Standard Time)

<p>
Just Eulerian path. Greedy DFS, building the route backwards when retreating.

More explanation and example under the codes.

Iterative versions inspired by [fangyang](https://leetcode.com/discuss/84706/share-solution-java-greedy-stack-15ms-with-explanation) (I had only thought of recursion, d\'oh).

---

**Ruby**

    def find_itinerary(tickets)
      tickets = tickets.sort.reverse.group_by(&:first)
      route = []
      visit = -> airport {
        visit[tickets[airport].pop()[1]] while (tickets[airport] || []).any?
        route << airport
      }
      visit["JFK"]
      route.reverse
    end

Iterative version:

    def find_itinerary(tickets)
      tickets = tickets.sort.reverse.group_by(&:first)
      route, stack = [], ["JFK"]
      while stack.any?
        stack << tickets[stack[-1]].pop()[1] while (tickets[stack[-1]] || []).any?
        route << stack.pop()
      end
      route.reverse
    end

---

**Python**

    def findItinerary(self, tickets):
        targets = collections.defaultdict(list)
        for a, b in sorted(tickets)[::-1]:
            targets[a] += b,
        route = []
        def visit(airport):
            while targets[airport]:
                visit(targets[airport].pop())
            route.append(airport)
        visit(\'JFK\')
        return route[::-1]

Iterative version:

    def findItinerary(self, tickets):
        targets = collections.defaultdict(list)
        for a, b in sorted(tickets)[::-1]:
            targets[a] += b,
        route, stack = [], [\'JFK\']
        while stack:
            while targets[stack[-1]]:
                stack += targets[stack[-1]].pop(),
            route += stack.pop(),
        return route[::-1]

---

**Java**

    public List<String> findItinerary(String[][] tickets) {
        for (String[] ticket : tickets)
            targets.computeIfAbsent(ticket[0], k -> new PriorityQueue()).add(ticket[1]);
        visit("JFK");
        return route;
    }
    
    Map<String, PriorityQueue<String>> targets = new HashMap<>();
    List<String> route = new LinkedList();
    
    void visit(String airport) {
        while(targets.containsKey(airport) && !targets.get(airport).isEmpty())
            visit(targets.get(airport).poll());
        route.add(0, airport);
    }

Iterative version:

    public List<String> findItinerary(String[][] tickets) {
        Map<String, PriorityQueue<String>> targets = new HashMap<>();
        for (String[] ticket : tickets)
            targets.computeIfAbsent(ticket[0], k -> new PriorityQueue()).add(ticket[1]);
        List<String> route = new LinkedList();
        Stack<String> stack = new Stack<>();
        stack.push("JFK");
        while (!stack.empty()) {
            while (targets.containsKey(stack.peek()) && !targets.get(stack.peek()).isEmpty())
                stack.push(targets.get(stack.peek()).poll());
            route.add(0, stack.pop());
        }
        return route;
    }

---

**C++**

    vector<string> findItinerary(vector<pair<string, string>> tickets) {
        for (auto ticket : tickets)
            targets[ticket.first].insert(ticket.second);
        visit("JFK");
        return vector<string>(route.rbegin(), route.rend());
    }

    map<string, multiset<string>> targets;
    vector<string> route;

    void visit(string airport) {
        while (targets[airport].size()) {
            string next = *targets[airport].begin();
            targets[airport].erase(targets[airport].begin());
            visit(next);
        }
        route.push_back(airport);
    }

---

**Explanation**

First keep going forward until you get stuck. That\'s a good main path already. Remaining tickets form cycles which are found on the way back and get merged into that main path. By writing down the path backwards when retreating from recursion, merging the cycles into the main path is easy - the end part of the path has already been written, the start part of the path hasn\'t been written yet, so just write down the cycle now and then keep backwards-writing the path.

Example:

![enter image description here][1]

From JFK we first visit JFK -> A -> C -> D -> A. There we\'re stuck, so we write down A as the end of the route and retreat back to D. There we see the unused ticket to B and follow it: D -> B -> C -> JFK -> D. Then we\'re stuck again, retreat and write down the airports while doing so: Write down D before the already written A, then JFK before the D, etc. When we\'re back from our cycle at D, the written route is D -> B -> C -> JFK -> D -> A. Then we retreat further along the original path, prepending C, A and finally JFK to the route, ending up with the route JFK -> A -> C -> D -> B -> C -> JFK -> D -> A.

  [1]: https://www.stefan-pochmann.info/misc/reconstruct-itinerary.png
</p>


### Share my solution
- Author: dietpepsi
- Creation Date: Fri Feb 05 2016 04:00:25 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 23:38:00 GMT+0800 (Singapore Standard Time)

<p>
See also [here](http://algobox.org/reconstruct-itinerary/)

All the airports are vertices and tickets are directed edges. Then all these tickets form a directed graph.

The graph must be Eulerian since we know that a Eulerian path exists.

Thus, start from "JFK", we can apply the Hierholzer's algorithm to find a Eulerian path in the graph which is a valid reconstruction.

Since the problem asks for lexical order smallest solution, we can put the neighbors in a min-heap. In this way, we always visit the smallest possible neighbor first in our trip.

    public class Solution {

        Map<String, PriorityQueue<String>> flights;
        LinkedList<String> path;

        public List<String> findItinerary(String[][] tickets) {
            flights = new HashMap<>();
            path = new LinkedList<>();
            for (String[] ticket : tickets) {
                flights.putIfAbsent(ticket[0], new PriorityQueue<>());
                flights.get(ticket[0]).add(ticket[1]);
            }
            dfs("JFK");
            return path;
        }

        public void dfs(String departure) {
            PriorityQueue<String> arrivals = flights.get(departure);
            while (arrivals != null && !arrivals.isEmpty())
                dfs(arrivals.poll());
            path.addFirst(departure);
        }
    }

    79 / 79 test cases passed.
    Status: Accepted
    Runtime: 11 ms
</p>


### Very Straightforward DFS Solution with Detailed Explanations
- Author: yuhao5
- Creation Date: Fri Feb 05 2016 09:21:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 23:38:25 GMT+0800 (Singapore Standard Time)

<p>
The nice thing about DFS is it tries a path, and if that's wrong (i.e. path does not lead to solution), DFS goes one step back and tries another path. It continues to do so until we've found the correct path (which leads to the solution). You need to always bear this nice feature in mind when utilizing DFS to solve problems.

In this problem, the path we are going to find is an itinerary which:
1. uses all tickets to travel among airports
2. preferably in ascending lexical order of airport code

Keep in mind that requirement 1 must be satisfied before we consider 2. If we always choose the airport with the smallest lexical order, this would lead to a perfectly lexical-ordered itinerary, but pay attention that when doing so, there can be a "dead end" somewhere in the tickets such that we are not able visit all airports (or we can't use all our tickets), which is bad because it fails to satisfy requirement 1 of this problem. Thus we need to take a step back and try other possible airports, which might not give us a perfectly ordered solution, but will use all tickets and cover all airports.

Thus it's natural to think about the "backtracking" feature of DFS. We start by building a graph and then sorting vertices in the adjacency list so that when we traverse the graph later, we can guarantee the lexical order of the itinerary can be as good as possible. When we have generated an itinerary, we check if we have used all our airline tickets. If not, we revert the change and try another ticket. We keep trying until we have used all our tickets.

    public class Solution {
        private HashMap<String, List<String>> adjList = new HashMap<>();
        private LinkedList<String> route = new LinkedList<>();
        private int numTickets = 0;
        private int numTicketsUsed = 0;
        
        public List<String> findItinerary(String[][] tickets) {
            if (tickets == null || tickets.length == 0) return route;
            // build graph
            numTickets = tickets.length;
            for (int i = 0; i < tickets.length; ++i) {
                if (!adjList.containsKey(tickets[i][0])) {
                    // create a new list
                    List<String> list = new ArrayList<>();
                    list.add(tickets[i][1]);
                    adjList.put(tickets[i][0], list);
                } else {
                    // add to existing list
                    adjList.get(tickets[i][0]).add(tickets[i][1]);
                }
            }
            // sort vertices in the adjacency list so they appear in lexical order
            for (Map.Entry<String, List<String>> entry : adjList.entrySet()) {
                Collections.sort(entry.getValue());
            }
            
            // start DFS
            route.add("JFK");
            dfsRoute("JFK");
            return route;
        }
        
        private void dfsRoute(String v) {
            // base case: vertex v is not in adjacency list
            // v is not a starting point in any itinerary, or we would have stored it
            // thus we have reached end point in our DFS
            if (!adjList.containsKey(v)) return;
            List<String> list = adjList.get(v);
            for (int i = 0; i < list.size(); ++i) {
                String neighbor = list.get(i);
                // remove ticket(route) from graph
                list.remove(i);
                route.add(neighbor);
                numTicketsUsed++;
                dfsRoute(neighbor);
                // we only return when we have used all tickets
                if (numTickets == numTicketsUsed) return;
                // otherwise we need to revert the changes and try other tickets
                list.add(i, neighbor);
                // This line took me a long time to debug
                // we must remove the last airport, since in an itinerary, the same airport can appear many times!!
                route.removeLast();
                numTicketsUsed--;
            }
        }
        
    }
</p>


