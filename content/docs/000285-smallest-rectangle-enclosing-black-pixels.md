---
title: "Smallest Rectangle Enclosing Black Pixels"
weight: 285
#id: "smallest-rectangle-enclosing-black-pixels"
---
## Description
<div class="description">
<p>An image is represented by a binary matrix with <code>0</code> as a white pixel and <code>1</code> as a black pixel. The black pixels are connected, i.e., there is only one black region. Pixels are connected horizontally and vertically. Given the location <code>(x, y)</code> of one of the black pixels, return the area of the smallest (axis-aligned) rectangle that encloses all black pixels.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input:</strong>
[
  &quot;0010&quot;,
  &quot;0110&quot;,
  &quot;0100&quot;
]
and <code>x = 0<font face="sans-serif, Arial, Verdana, Trebuchet MS">, </font></code><code>y = 2</code>

<strong>Output:</strong> 6
</pre>

</div>

## Tags
- Binary Search (binary-search)

## Companies
- Google - 3 (taggedByAdmin: true)

## Official Solution
[TOC]

## Summary

This article is for intermediate readers. It introduces the following ideas:
Depth First Search (DFS), Breadth First Search (BFS) and Binary Search

## Solution

#### Approach 1: Naive Linear Search

**Intuition**

Traversal all the pixels. Keep the maximum and minimum values of black pixels coordinates.

**Algorithm**

We keep four boundaries, `left`, `right`, `top` and `bottom` of the rectangle.
Note that `left` and `top` are inclusive while `right` and `bottom` are exclusive.
We then traversal all the pixels and update the four boundaries accordingly.

The recipe is following:

* Initialize left, right, top and bottom
* Loop through all `(x, y)` coordinates
  * if `image[x][y]` is black
    * `left = min(left, x)`
    * `right = max(right, x + 1)`
    * `top = min(top, y)`
    * `bottom = max(bottom, y + 1)`
* Return `(right - left) * (bottom - top)`


<iframe src="https://leetcode.com/playground/fn66ntdb/shared" frameBorder="0" name="fn66ntdb" width="100%" height="343"></iframe>

**Complexity Analysis**

* Time complexity : $$O(mn)$$. $$m$$ and $$n$$ are the height and width of the image.

* Space complexity : $$O(1)$$. All we need to store are the four boundaries.

**Comment**
* One may optimize this algorithm to stop early. But it doesn't change the asymptotic performance.
* This naive approach is certainly not the best answer to this problem. However, it gives you a good entry point to tackle the problem. Most of the time the good algorithms come from identifying the repeat calculation a naive approach. And it also sets up a baseline of the time and space complexity, so that one can see whether or not other approaches are better than it.

---
#### Approach 2: DFS or BFS

**Intuition**

Explore all the connected black pixel from the given pixel and update the boundaries.

**Algorithm**

The naive approach did not use the condition that all the black pixels are connected and that one of the black pixels is given.

A simple way to use these facts is to do an exhaustive search starting from the given pixel. Since all the black pixels are connected, DFS or BFS will visit all of them starting from the given black pixel. The idea is similar to what we did for [200. Number of Island](https://leetcode.com/problems/number-of-islands/). Instead of many islands, we have only one island here, and we know one pixel of it.

<iframe src="https://leetcode.com/playground/2XYpt8rw/shared" frameBorder="0" width="100%" height="463" name="2XYpt8rw"></iframe>

**Complexity Analysis**

* Time complexity : $$O(E) = O(B) = O(mn)$$.

Here $$E$$ is the number of edges in the traversed graph. $$B$$ is the total number of black pixels. Since each pixel have four edges at most, $$O(E) = O(B)$$. In the worst case, $$O(B) = O(mn)$$.

* Space complexity : $$O(V) = O(B) = O(mn)$$.

The space complexity is $$O(V)$$ where $$V$$ is the number of vertices in the traversed graph. In this problem $$O(V) = O(B)$$. Again, in the worst case, $$O(B) = O(mn)$$.

**Comment**

Although this approach is better than naive approach when $$B$$ is much smaller than $$mn$$, it is asymptotically the same as approach #1 when $$B$$ is comparable to $$mn$$. And it costs a lot more auxiliary space.

---
#### Approach 3: Binary Search

**Intuition**

Project the 2D image into a 1D array and use binary search to find the boundaries.

**Algorithm**

![matrix projection](../Figures/302_matrix_projection.svg){:width="539px"}
{:align="center"}

*Figure 1. Illustration of image projection.
{:align="center"}

Suppose we have a $$10 \times 11$$ image as shown in figure 1, if we project each column of the image into an entry of row vector `v` with the following rule:

* `v[i] = 1`   if exists `x` such that `image[x][i] = 1`
* `v[i] = 0`   otherwise

That is

> If a column has any black pixel it's projection is black otherwise white.

Similarly, we can do the same for the rows, and project the image into a 1D column vector. The two projected vectors are shown in figure 1.

Now, we claim the following lemma:

*Lemma*
>If there are only one black pixel region, then in a projected 1D array all the black pixels are connected.

*Proof by contradiction*
>Assume to the contrary that there are disconnected black pixels at `i` and `j` where `i < j` in the 1D projection array. Thus, there exists one column `k`, `k` in `(i, j)` and the column `k` in the 2D array has no black pixel. Therefore, in the 2D array there exist at least two black pixel regions separated by column `k` which contradicting the condition of "only one black pixel region".
Therefore, we conclude that all the black pixels in the 1D projection array are connected.

With this lemma, we have the following algorithm:

* Project the 2D array into a column array and a row array
* Binary search to find `left` in the row array within `[0, y)`
* Binary search to find `right` in the row array within `[y + 1, n)`
* Binary search to find `top` in the column array within `[0, x)`
* Binary search to find `bottom` in the column array within `[x + 1, m)`
* Return `(right - left) * (bottom - top)`

However, the projection step cost $$O(mn)$$ time which dominates the entire algorithm.If so, we gain nothing comparing with previous approaches.

The trick is that we do not need to do the projection step as a preprocess. We can do it on the fly, i.e. "don't project the column/row unless needed".

Recall the binary search algorithm in a 1D array, each time we only check one element, the pivot, to decide which half we go next.

In a 2D array, we can do something similar. The only difference here is that the element is not a number but a vector. For example, a `m` by `n` matrix can be seen as `n` column vectors.

In these `n` elements/vectors, we do a binary search to find `left` or `right`. Each time we only check one element/vector, the pivot, to decide which half we go next.
In total it checks $$O(\log n)$$ vectors, and each check is $$O(m)$$ (we simply traverse all the `m` entries of the pivot vector).

So it costs $$O(m \log n)$$ to find `left` and `right`.
Similarly it costs $$O(n \log m)$$ to find `top` and `bottom`. The entire algorithm has a time complexity of $$O(m \log n + n \log m)$$

<iframe src="https://leetcode.com/playground/RX66pbsc/shared" frameBorder="0" width="100%" height="500" name="RX66pbsc"></iframe>

**Complexity Analysis**

* Time complexity : $$O(m \log n + n \log m)$$.

Here, $$m$$ and $$n$$ are the height and width of the image. We embedded a linear search for every iteration of binary search. See previous sections for details.

* Space complexity : $$O(1)$$.

Both binary search and linear search used only constant extra space.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java/Python Binary Search solution with explanation
- Author: dietpepsi
- Creation Date: Sat Nov 07 2015 13:34:27 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 12 2018 23:06:52 GMT+0800 (Singapore Standard Time)

<p>
Suppose we have a 2D array

    "000000111000000"
    "000000101000000"
    "000000101100000"
    "000001100100000"


Imagine we project the 2D array to the bottom axis with the rule "if a column has any black pixel it's projection is black otherwise white". The projected 1D array is
    
    "000001111100000"

> **Theorem**
>
> If there are only one black pixel region, then in a projected 1D array all the black pixels are connected.
> 
> **Proof by contradiction**
> 
> Assume to the contrary that there are disconnected black pixels at `i`
> and `j` where `i < j` in the 1D projection array. Thus there exists one
> column `k`, `k in (i, j)` and and the column `k` in the 2D array has no
> black pixel. Therefore in the 2D array there exists at least 2 black
> pixel regions separated by column `k` which contradicting the condition
> of "only one black pixel region". 
> 
> Therefore we conclude that all the black pixels in the 1D projection
> array is connected.

This means we can do a binary search in each half to find the boundaries, if we know one black pixel's position. And we do know that.

To find the left boundary, do the binary search in the `[0, y)` range and find the first column vector who has any black pixel. 

To determine if a column vector has a black pixel is `O(m)` so the search in total is `O(m log n)`

We can do the same for the other boundaries. The area is then calculated by the boundaries.
Thus the algorithm runs in `O(m log n + n log m)`


**Java**

    private char[][] image;
    public int minArea(char[][] iImage, int x, int y) {
        image = iImage;
        int m = image.length, n = image[0].length;
        int left = searchColumns(0, y, 0, m, true);
        int right = searchColumns(y + 1, n, 0, m, false);
        int top = searchRows(0, x, left, right, true);
        int bottom = searchRows(x + 1, m, left, right, false);
        return (right - left) * (bottom - top);
    }
    private int searchColumns(int i, int j, int top, int bottom, boolean opt) {
        while (i != j) {
            int k = top, mid = (i + j) / 2;
            while (k < bottom && image[k][mid] == '0') ++k;
            if (k < bottom == opt)
                j = mid;
            else
                i = mid + 1;
        }
        return i;
    }
    private int searchRows(int i, int j, int left, int right, boolean opt) {
        while (i != j) {
            int k = left, mid = (i + j) / 2;
            while (k < right && image[mid][k] == '0') ++k;
            if (k < right == opt)
                j = mid;
            else
                i = mid + 1;
        }
        return i;
    }
    //  Runtime: 1 ms



**C++**

    vector<vector<char>> *image;
    int minArea(vector<vector<char>> &iImage, int x, int y) {
        image = &iImage;
        int m = int(image->size()), n = int((*image)[0].size());
        int top = searchRows(0, x, 0, n, true);
        int bottom = searchRows(x + 1, m, 0, n, false);
        int left = searchColumns(0, y, top, bottom, true);
        int right = searchColumns(y + 1, n, top, bottom, false);
        return (right - left) * (bottom - top);
    }
    int searchRows(int i, int j, int low, int high, bool opt) {
        while (i != j) {
            int k = low, mid = (i + j) / 2;
            while (k < high && (*image)[mid][k] == '0') ++k;
            if (k < high == opt)
                j = mid;
            else
                i = mid + 1;
        }
        return i;
    }
    int searchColumns(int i, int j, int low, int high, bool opt) {
        while (i != j) {
            int k = low, mid = (i + j) / 2;
            while (k < high && (*image)[k][mid] == '0') ++k;
            if (k < high == opt)
                j = mid;
            else
                i = mid + 1;
        }
        return i;
    }
    // Runtime: 20 ms

**Python**


    def minArea(self, image, x, y):
        top = self.searchRows(image, 0, x, True)
        bottom = self.searchRows(image, x + 1, len(image), False)
        left = self.searchColumns(image, 0, y, top, bottom, True)
        right = self.searchColumns(image, y + 1, len(image[0]), top, bottom, False)
        return (right - left) * (bottom - top)

    def searchRows(self, image, i, j, opt):
        while i != j:
            m = (i + j) / 2
            if ('1' in image[m]) == opt:
                j = m
            else:
                i = m + 1
        return i

    def searchColumns(self, image, i, j, top, bottom, opt):
        while i != j:
            m = (i + j) / 2
            if any(image[k][m] == '1' for k in xrange(top, bottom)) == opt:
                j = m
            else:
                i = m + 1
        return i
    # Runtime: 56 ms


**Java (DRY)**


    private char[][] image;
    public int minArea(char[][] iImage, int x, int y) {
        image = iImage;
        int m = image.length, n = image[0].length;
        int top = search(0, x, 0, n, true, true);
        int bottom = search(x + 1, m, 0, n, false, true);
        int left = search(0, y, top, bottom, true, false);
        int right = search(y + 1, n, top, bottom, false, false);
        return (right - left) * (bottom - top);
    }
    private boolean isWhite(int mid, int k, boolean isRow) {
        return ((isRow) ? image[mid][k] : image[k][mid]) == '0';
    }
    private int search(int i, int j, int low, int high, boolean opt, boolean isRow) {
        while (i != j) {
            int k = low, mid = (i + j) / 2;
            while (k < high && isWhite(mid, k, isRow)) ++k;
            if (k < high == opt)
                j = mid;
            else
                i = mid + 1;
        }
        return i;
    }
    //  Runtime: 2 ms

**C++ (DRY)**

    vector<vector<char>> *image;
    int minArea(vector<vector<char>> &iImage, int x, int y) {
        image = &iImage;
        int m = int(image->size()), n = int((*image)[0].size());
        int top = search(0, x, 0, n, true, true);
        int bottom = search(x + 1, m, 0, n, false, true);
        int left = search(0, y, top, bottom, true, false);
        int right = search(y + 1, n, top, bottom, false, false);
        return (right - left) * (bottom - top);
    }
    bool isWhite(int mid, int k, bool isRow) {
        return ((isRow) ? (*image)[mid][k]:(*image)[k][mid]) == '0';
    }
    int search(int i, int j, int low, int high, bool opt, bool isRow) {
        while (i != j) {
            int k = low, mid = (i + j) / 2;
            while (k < high && isWhite(mid, k, isRow)) ++k;
            if (k < high == opt)
                j = mid;
            else
                i = mid + 1;
        }
        return i;
    }
    // Runtime: 24 ms



**Python (DRY, from Stefan's cool [solution][1])**

    def minArea(self, image, x, y):
        top = self.search(0, x, lambda mid: '1' in image[mid])
        bottom = self.search(x + 1, len(image), lambda mid: '1' not in image[mid])
        left = self.search(0, y, lambda mid: any(image[k][mid] == '1' for k in xrange(top, bottom)))
        right = self.search(y + 1, len(image[0]), lambda mid: all(image[k][mid] == '0' for k in xrange(top, bottom)))
        return (right - left) * (bottom - top)

    def search(self, i, j, check):
        while i != j:
            mid = (i + j) / 2
            if check(mid):
                j = mid
            else:
                i = mid + 1
        return i
    # Runtime: 56 ms


  [1]: https://leetcode.com/discuss/68407/clear-binary-search-python
</p>


### 1ms Concise Java Binary Search (DFS is 4ms)
- Author: yavinci
- Creation Date: Sat Nov 28 2015 02:17:12 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Sep 04 2018 04:55:57 GMT+0800 (Singapore Standard Time)

<p>
If we don't know programming, how do we find the 4 boundaries  given a black pixel?

Do we need to search every black cell? Absolutely not. 

Intuitively, we would expand from the given `1 * 1` black cell, "aggressively" expand to the 4 boundaries, roughly half of the remaining space. If we don't "cut" any black pixel, we know we go too far and should go back half. 

This is exactly the process of binary search. 

One simple way without any worry about boundary, is as follows:
* Use a vertical line, to jump to the `leftmost` black pixel , in the range of `[0, y]`
* Use a vertical line, to jump to the `rightmost` black pixel, in the range of `[y, n - 1]`
* Use a horizontal line, to jump to the `topmost` black pixel, in the range of `[0, x]`
* Use a horizontal line, to jump to the `bottommost` black pixel, in the range of `[x, m - 1]`

Hope it helps!

    public int minArea(char[][] image, int x, int y) {
        int left = leftmost(image, 0, y, true);
        int right = rightmost(image, y, image[0].length - 1, true);
        int top = leftmost(image, 0, x, false);
        int bottom = rightmost(image, x, image.length - 1, false);
        return (right - left + 1) * (bottom - top + 1);
    }
    
    int leftmost(char[][] image, int min, int max, boolean horizontal) {
        int l = min, r = max;
        while (l < r) {
            int mid = l + (r - l) / 2;
            if (!hasBlack(image, mid, horizontal)) {
                l = mid + 1;
            } else {
                r = mid;
            }
        }
        return l;
    }
    
    int rightmost(char[][] image, int min, int max, boolean horizontal) {
        int l = min, r = max;
        while (l < r) {
            int mid = l + (r - l + 1) / 2;
            if (!hasBlack(image, mid, horizontal)) {
                r = mid - 1;
            } else {
                l = mid;
            }
        }
        return r;
    }
    
    boolean hasBlack(char[][] image, int mid, boolean horizontal) {
        if (horizontal) {
            for (int i = 0; i < image.length; i++) {
                if (image[i][mid] == '1') {
                    return true;
                }
            }
        } else {
            for (int j = 0; j < image[0].length; j++) {
                if (image[mid][j] == '1') {
                    return true;
                }
            }
        }
        return false;
    }

<hr>
Version 2: Another harder but more compact way is as follows:

    public int minArea(char[][] image, int x, int y) {
        int m = image.length, n = image[0].length;
        int colMin = binarySearch(image, true, 0, y, 0, m, true);
        int colMax = binarySearch(image, true, y + 1, n, 0, m, false);
        int rowMin = binarySearch(image, false, 0, x, colMin, colMax, true);
        int rowMax = binarySearch(image, false, x + 1, m, colMin, colMax, false);
        return (rowMax - rowMin) * (colMax - colMin);
    }

    public int binarySearch(char[][] image, boolean horizontal, int lower, int upper, int min, int max, boolean goLower) {
        while(lower < upper) {
            int mid = lower + (upper - lower) / 2;
            boolean inside = false;
            for(int i = min; i < max; i++) {
                if((horizontal ? image[i][mid] : image[mid][i]) == '1') {
                    inside = true; 
                    break;
                }
            }
            if(inside == goLower) {
                upper = mid;
            } else {
                lower = mid + 1;
            }
        }
        return lower;
    }
</p>


### Clear binary search Python
- Author: StefanPochmann
- Creation Date: Sun Nov 08 2015 19:26:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 08 2015 19:26:55 GMT+0800 (Singapore Standard Time)

<p>
Based on my [**shorter Ruby version**](https://leetcode.com/discuss/68335/ruby-binary-search). Still using ruichang's idea to use binary search.

    def minArea(self, image, x, y):
        def first(lo, hi, check):
            while lo < hi:
                mid = (lo + hi) / 2
                if check(mid):
                    hi = mid
                else:
                    lo = mid + 1
            return lo
        top    = first(0, x,             lambda x: '1' in image[x])
        bottom = first(x, len(image),    lambda x: '1' not in image[x])
        left   = first(0, y,             lambda y: any(row[y] == '1' for row in image))
        right  = first(y, len(image[0]), lambda y: all(row[y] == '0' for row in image))
        return (bottom - top) * (right - left)

The `first` function could also use the `bisect` module, for example:

        def first(lo, hi, check):
            self.__getitem__ = check
            return bisect.bisect(self, False, lo, hi)

        def first(lo, hi, check):
            class Checker: __getitem__ = staticmethod(check)
            return bisect.bisect(Checker(), False, lo, hi)

The first one is a bit dirtier, and requires `class Solution:` instead of `class Solution(object):`.
</p>


