---
title: "Valid Palindrome II"
weight: 612
#id: "valid-palindrome-ii"
---
## Description
<div class="description">
<p>
Given a non-empty string <code>s</code>, you may delete <b>at most</b> one character.  Judge whether you can make it a palindrome.
</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> "aba"
<b>Output:</b> True
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> "abca"
<b>Output:</b> True
<b>Explanation:</b> You could delete the character 'c'.
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The string will only contain lowercase characters a-z.
The maximum length of the string is 50000.</li>
</ol>
</p>
</div>

## Tags
- String (string)

## Companies
- Facebook - 74 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 5 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Brute Force [Time Limit Exceeded]

**Intuition and Algorithm**

For each index `i` in the given string, let's remove that character, then check if the resulting string is a palindrome.  If it is, (or if the original string was a palindrome), then we'll return `true`

<iframe src="https://leetcode.com/playground/F8rXiMNb/shared" frameBorder="0" name="F8rXiMNb" width="100%" height="394"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2)$$ where $$N$$ is the length of the string.  We do the following $$N$$ times: create a string of length $$N$$ and iterate over it.

* Space Complexity: $$O(N)$$, the space used by our candidate answer.

---
#### Approach #2: Greedy [Accepted]

**Intuition**

If the beginning and end characters of a string are the same (ie. `s[0] == s[s.length - 1]`), then whether the inner characters are a palindrome (`s[1], s[2], ..., s[s.length - 2]`) uniquely determines whether the entire string is a palindrome.

**Algorithm**

Suppose we want to know whether `s[i], s[i+1], ..., s[j]` form a palindrome.  If `i >= j` then we are done.  If `s[i] == s[j]` then we may take `i++; j--`.  Otherwise, the palindrome must be either `s[i+1], s[i+2],  ..., s[j]` or `s[i], s[i+1], ..., s[j-1]`, and we should check both cases.

<iframe src="https://leetcode.com/playground/46SiEhrv/shared" frameBorder="0" name="46SiEhrv" width="100%" height="360"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$ where $$N$$ is the length of the string.  Each of two checks of whether some substring is a palindrome is $$O(N)$$.

* Space Complexity: $$O(1)$$ additional complexity.  Only pointers were stored in memory.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Easy to Understand Python Solution
- Author: yangshun
- Creation Date: Sun Sep 17 2017 13:31:31 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 11:09:05 GMT+0800 (Singapore Standard Time)

<p>
We can use the standard two-pointer approach that starts at the left and right of the string and move inwards. Whenever there is a mismatch, we can either exclude the character at the left or the right pointer. We then take the two remaining substrings and compare against its reversed and see if either one is a palindrome.

*- Yangshun*

```
class Solution(object):
    def validPalindrome(self, s):
        """
        :type s: str
        :rtype: bool
        """
        # Time: O(n)
        # Space: O(n)
        left, right = 0, len(s) - 1
        while left < right:
            if s[left] != s[right]:
                one, two = s[left:right], s[left + 1:right + 1]
                return one == one[::-1] or two == two[::-1]
            left, right = left + 1, right - 1
        return True
```
</p>


### Java O(n) Time O(1) Space
- Author: compton_scatter
- Creation Date: Sun Sep 17 2017 13:54:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 08 2018 21:26:22 GMT+0800 (Singapore Standard Time)

<p>
```
public boolean validPalindrome(String s) {
    int l = -1, r = s.length();
    while (++l < --r) 
        if (s.charAt(l) != s.charAt(r)) return isPalindromic(s, l, r+1) || isPalindromic(s, l-1, r);
    return true;
}

public boolean isPalindromic(String s, int l, int r) {
    while (++l < --r) 
        if (s.charAt(l) != s.charAt(r)) return false;
    return true;
}
```
</p>


### [C++/Java/Python] Easy and Concise
- Author: lee215
- Creation Date: Sun Sep 17 2017 19:19:59 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 09 2018 17:00:14 GMT+0800 (Singapore Standard Time)

<p>
Check from left and right at the same time until the first different pair.
Now we have something like ```a****b```, where a and b are different.
We need to delete either ```a``` or ```b``` to make it a palindrome.

C++
```
    bool validPalindrome(string s) {
        for (int i = 0, j = s.size() - 1; i < j; i++, j--)
            if (s[i] != s[j]) {
                int i1 = i, j1 = j - 1, i2 = i + 1, j2 = j;
                while (i1 < j1 && s[i1] == s[j1]) {i1++; j1--;};
                while (i2 < j2 && s[i2] == s[j2]) {i2++; j2--;};
                return i1 >= j1 || i2 >= j2;
            }
        return true;
    }
```

Java
```
    public boolean validPalindrome(String s) {
        for (int i = 0, j = s.length() - 1; i < j; i++, j--)
            if (s.charAt(i) != s.charAt(j)) {
                int i1 = i, j1 = j - 1, i2 = i + 1, j2 = j;
                while (i1 < j1 && s.charAt(i1) == s.charAt(j1)) {i1++; j1--;};
                while (i2 < j2 && s.charAt(i2) == s.charAt(j2)) {i2++; j2--;};
                return i1 >= j1 || i2 >= j2;
            }
        return true;
    }
```
Python
`````
def validPalindrome(self, s):
        i = 0
        while i < len(s) / 2 and s[i] == s[-(i + 1)]: i += 1
        s = s[i:len(s) - i]
        return s[1:] == s[1:][::-1] or s[:-1] == s[:-1][::-1]
</p>


