---
title: "Immediate Food Delivery I"
weight: 1542
#id: "immediate-food-delivery-i"
---
## Description
<div class="description">
<p>Table: <code>Delivery</code></p>

<pre>
+-----------------------------+---------+
| Column Name                 | Type    |
+-----------------------------+---------+
| delivery_id                 | int     |
| customer_id                 | int     |
| order_date                  | date    |
| customer_pref_delivery_date | date    |
+-----------------------------+---------+
delivery_id is the primary key of this table.
The table holds information about food delivery to customers that make orders at some date and specify a preferred delivery date (on the same order date or after it).
</pre>

<p>&nbsp;</p>

<p>If the preferred delivery date of the customer is the same as the&nbsp;order date&nbsp;then the order is called <em>immediate</em>&nbsp;otherwise it&#39;s called <em>scheduled</em>.</p>

<p>Write an SQL query to find the&nbsp;percentage of immediate orders in the table,&nbsp;<strong>rounded to 2 decimal places</strong>.</p>

<p>The query result format is in the following example:</p>

<pre>
Delivery table:
+-------------+-------------+------------+-----------------------------+
| delivery_id | customer_id | order_date | customer_pref_delivery_date |
+-------------+-------------+------------+-----------------------------+
| 1           | 1           | 2019-08-01 | 2019-08-02                  |
| 2           | 5           | 2019-08-02 | 2019-08-02                  |
| 3           | 1           | 2019-08-11 | 2019-08-11                  |
| 4           | 3           | 2019-08-24 | 2019-08-26                  |
| 5           | 4           | 2019-08-21 | 2019-08-22                  |
| 6           | 2           | 2019-08-11 | 2019-08-13                  |
+-------------+-------------+------------+-----------------------------+

Result table:
+----------------------+
| immediate_percentage |
+----------------------+
| 33.33                |
+----------------------+
The orders with delivery id 2 and 3 are immediate while the others are scheduled.
</pre>

</div>

## Tags


## Companies
- DoorDash - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### clean mysql 1-liner without case statement
- Author: shawlu
- Creation Date: Fri Aug 30 2019 18:17:37 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 30 2019 18:17:37 GMT+0800 (Singapore Standard Time)

<p>

```sql
select round(100 * sum(order_date = customer_pref_delivery_date) / count(*), 2) as immediate_percentage from Delivery;
```

I\'m maintaining a GitHub repository that documents useful SQL techniques and common pitfalls in interview. Please star/fork if you find it helpful: https://github.com/shawlu95/Beyond-LeetCode-SQL
</p>


### MySQL simple solution
- Author: ye15
- Creation Date: Fri Nov 01 2019 11:23:16 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Nov 01 2019 11:23:16 GMT+0800 (Singapore Standard Time)

<p>

```
SELECT ROUND(100*AVG(order_date = customer_pref_delivery_date), 2) AS immediate_percentage
FROM Delivery;
```
</p>


### MySQL easy to understand query
- Author: SwitchCase
- Creation Date: Fri Aug 30 2019 11:43:36 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Aug 30 2019 11:43:36 GMT+0800 (Singapore Standard Time)

<p>
```
select round( (a.immediate / b.total) * 100.0, 2) as immediate_percentage from
(select count(*) as immediate from delivery where order_date = customer_pref_delivery_date) as a,
(select count(*) as total from delivery) as b
```
</p>


