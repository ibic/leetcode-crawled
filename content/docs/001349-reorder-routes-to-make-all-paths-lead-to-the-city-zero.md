---
title: "Reorder Routes to Make All Paths Lead to the City Zero"
weight: 1349
#id: "reorder-routes-to-make-all-paths-lead-to-the-city-zero"
---
## Description
<div class="description">
<p>There are&nbsp;<code>n</code>&nbsp;cities numbered from&nbsp;<code>0</code>&nbsp;to&nbsp;<code>n-1</code> and&nbsp;<code>n-1</code> roads such that&nbsp;there is only one way to travel between two&nbsp;different cities (this network form a tree).&nbsp;Last year,&nbsp;The ministry of transport&nbsp;decided to orient the roads in one direction because they are too narrow.</p>

<p>Roads are represented by&nbsp;<code>connections</code>&nbsp;where&nbsp;<code>connections[i] = [a, b]</code>&nbsp;represents a&nbsp;road&nbsp;from city&nbsp;<code>a</code>&nbsp;to&nbsp;<code>b</code>.</p>

<p>This year, there will be a big event in the capital (city 0), and many people want to travel to this city.</p>

<p>Your task consists of reorienting&nbsp;some roads such that each city can visit the city&nbsp;0. Return the <strong>minimum</strong> number of edges changed.</p>

<p>It&#39;s <strong>guaranteed</strong> that each city can reach the city 0 after reorder.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/05/13/sample_1_1819.png" style="width: 240px; height: 150px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 6, connections = [[0,1],[1,3],[2,3],[4,0],[4,5]]
<strong>Output:</strong> 3
<strong>Explanation: </strong>Change the direction of edges show in red such that each node can reach the node 0 (capital).</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2020/05/13/sample_2_1819.png" style="width: 380px; height: 60px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 5, connections = [[1,0],[1,2],[3,2],[3,4]]
<strong>Output:</strong> 2
<strong>Explanation: </strong>Change the direction of edges show in red such that each node can reach the node 0 (capital).</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 3, connections = [[1,0],[2,0]]
<strong>Output:</strong> 0
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>2 &lt;= n &lt;= 5 * 10^4</code></li>
	<li><code>connections.length == n-1</code></li>
	<li><code>connections[i].length == 2</code></li>
	<li><code>0 &lt;= connections[i][0], connections[i][1] &lt;= n-1</code></li>
	<li><code>connections[i][0] != connections[i][1]</code></li>
</ul>

</div>

## Tags
- Tree (tree)
- Depth-first Search (depth-first-search)

## Companies
- Google - 4 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++/Java Track Direction
- Author: votrubac
- Creation Date: Sun May 31 2020 12:02:03 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 02 2020 09:51:02 GMT+0800 (Singapore Standard Time)

<p>
Based on the problem description, we have a tree, and node zero is the root. 

However, the direction can point either from a parent to a child (positive), or from a child to its parent (negative). To solve the problem, we traverse the tree and count edges that are directed from a parent to a child. Direction of those edges need to be changed to arrive at zero node.

In the code below, I am using the adjacency list, and the sign indicates the direction. If the index is positive - the direction is from a parent to a child and we need to change it (`change += (to > 0)`).

> Note that we cannot detect the direction for zero (`-0 == 0`), but it does not matter as we start our traversal from zero.

**C++**
```cpp
int dfs(vector<vector<int>> &al, vector<bool> &visited, int from) {
    auto change = 0;
    visited[from] = true;
    for (auto to : al[from])
        if (!visited[abs(to)])
            change += dfs(al, visited, abs(to)) + (to > 0);
    return change;        
}
int minReorder(int n, vector<vector<int>>& connections) {
    vector<vector<int>> al(n);
    for (auto &c : connections) {
        al[c[0]].push_back(c[1]);
        al[c[1]].push_back(-c[0]);
    }
    return dfs(al, vector<bool>(n) = {}, 0);
}
```
**Java**
Tried to do one-line initialization using `Collections.nCopies`, but got TLE. Accepted for same old `for` loop in < 30 milliseconds. 
```java
int dfs(List<List<Integer>> al, boolean[] visited, int from) {
    int change = 0;
    visited[from] = true;
    for (var to : al.get(from))
        if (!visited[Math.abs(to)])
            change += dfs(al, visited, Math.abs(to)) + (to > 0 ? 1 : 0);
    return change;   
}
public int minReorder(int n, int[][] connections) {
    List<List<Integer>> al = new ArrayList<>();
    for(int i = 0; i < n; ++i) 
        al.add(new ArrayList<>());
    for (var c : connections) {
        al.get(c[0]).add(c[1]);
        al.get(c[1]).add(-c[0]);
    }
    return dfs(al, new boolean[n], 0);
}
```
**Complexity Analysis**
- Time: O(n). We visit each node once.
- Memory: O(n). We store *n* nodes in the adjacency list, with *n - 1* edges in total.

**Bonus: Minimalizm Version**
Instead of `visited`, we can just pass the previous index to prevent going back, as suggested by [zed_b](https://leetcode.com/zed_b). This is possible because every node has only one parent in the tree.
```cpp
int dfs(vector<vector<int>> &al, int prev, int node) {
    return accumulate(begin(al[node]), end(al[node]), 0, [&](int sum, int to) {
        return sum + (abs(to) == prev ? 0 : dfs(al, node, abs(to)) + (to > 0));
    });    
}
int minReorder(int n, vector<vector<int>>& connections) {
    vector<vector<int>> al(n);
    for (auto &c : connections) {
        al[c[0]].push_back(c[1]);
        al[c[1]].push_back(-c[0]);
    }
    return dfs(al, 0, 0);
}
```
</p>


### Simple Explanation | DFS with edge deletion | Idea | Code | Comments | Q & A
- Author: interviewrecipes
- Creation Date: Sun May 31 2020 12:03:09 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jun 05 2020 11:51:45 GMT+0800 (Singapore Standard Time)

<p>
**Key Idea**
The base case that we know is - All outgoing edges from 0 must be reversed. So add the count into our final result. 
At this stage, we can say all edges are coming into node 0. What is the next thing we need to do?
Make sure that all nodes that are connected to 0 have all edges coming into them.
E.g. if nodes x and y are connected to 0. After first step, there will be edges from x to 0 and y to 0. Now we want to make sure that all nodes that are directly connected to x, must have an edge coming into x. Similarly for y. If we keep on doing this recursively, we will get the answer at the end. Along the way, we will keep counting the edges that we reversed.

**Implementation**
For the ease of implementation, I have created two separate graph, one for outgoing edges and another for incoming. Everytime an edge is traversed, I erase the opposite edge so that I don\u2019t hit the same node twice.

**Time Complexity** 
O(n) where n is number of nodes in the tree.

**Thanks**
Is there anything that is still unclear? Let me know and I can elaborate. 
Please upvote if you find this helpful, it will be encouraging.

**Q&A**
```
Q1. 

// Why we need to traverse both incoming as well as outgoing edges?
// Ans. 
// We want all edges to come towards 0. Reversing outgoing edges
// from 0 is necessary - you can understand this easily.
// Then you need to recursively orient all outgoing edges from 
// those connected nodes too.
// But why do we need to recursively call dfs() even for incoming
// edges? 
// The answer is, we don\'t know the orientation of edges that are 
// connected to that neighbor node.
// For example - say 0 has an incoming edge from 1 and 1 has one other
// outgoing edge to 2. i.e. graph is like 0 <-- 1 --> 2.
// Although, you don\'t need to reverse 0 <-- 1 edge, you still have to 
// make sure that all other edges are coming towards 1. When you call
// dfs with node 1, you will then recognize the incorrect orientation
// of 1 --> 2 edge. This is why traversing incoming edges is important.
```
```
Q2.

// Why do we need to erase edges?
// Ans.
// To avoid double counting. 
// We increment the count everytime we see an outgoing edge. We don\'t 
// increment for incoming. However, an incoming edge for one node is 
// the outgoing edge for the other.
// In the previous example, 0 <-- 1 --> 2.
// For node 0, we won\'t increment the count because there are no outgoing
// edges. But when we are at 1, there are two outgoing edges. But 1 --> 0 
// is already oriented towards 0 and we don\'t want to reverse that. How 
// will we distiguish between correctly oriented edges vs incorrectly 
// oriented ones in general? Easier approach is to remove those correctly 
// oriented edges immediately when we know their orientation is correct.
```



**Code**
C++
```
class Solution {
public:
    unordered_map<int, unordered_set<int>> out, in;
    int ans = 0;
    
    void dfs(int node) {
        
        // Why we need to traverse both incoming as well as outgoing edges?
        // Ans. 
        // We want all edges to come towards 0. Reversing outgoing edges
        // from 0 is necessary - you can understand this easily.
        // Then you need to recursively orient all outgoing edges from 
        // those connected nodes too.
        // But why do we need to recursively call dfs() even for incoming
        // edges? 
        // The answer is, we don\'t know the orientation of edges that are 
        // connected to that neighbor node.
        // For example - say 0 has an incoming edge from 1 and 1 has one other
        // outgoing edge to 2. i.e. graph is like 0 <-- 1 --> 2.
        // Although, you don\'t need to reverse 0 <-- 1 edge, you still have to 
        // make sure that all other edges are coming towards 1. When you call
        // dfs with node 1, you will then recognize the incorrect orientation
        // of 1 --> 2 edge. This is why traversing incoming edges is important.
        
        // Why do we need to erase edges?
        // Ans.
        // To avoid double counting. 
        // We increment the count everytime we see an outgoing edge. We don\'t 
        // increment for incoming. However, an incoming edge for one node is 
        // the outgoing edge for the other.
        // In the previous example, 0 <-- 1 --> 2.
        // For node 0, we won\'t increment the count because there are no outgoing
        // edges. But when we are at 1, there are two outgoing edges. But 1 --> 0 
        // is already oriented towards 0 and we don\'t want to reverse that. How 
        // will we distiguish between correctly oriented edges vs incorrectly 
        // oriented ones in general? Easier approach is to remove those correctly 
        // oriented edges immediately when we know their orientation is correct.
        
        for (int x: out[node]) {
            // There is an edge (node->x). We must reverse it.
            ans++; 
			// delete, edge (x <- node)
            in[x].erase(node);
			// recurse.
            dfs(x);
        }
        for (int x:in[node]) {
		    // The edge is (x <- node). So delete (node -> x).
			out[x].erase(node);
			// recurse.
            dfs(x);
        }
    }
    
    int minReorder(int n, vector<vector<int>>& connections) {
        ans = 0;
		// make graph.
        for (auto x: connections) {
            out[x[0]].insert(x[1]);
            in[x[1]].insert(x[0]);
        }
		// start with node 0.
        dfs(0);
        return ans;
    }
};
```

**Did you check the visuals of the other problem in the contest?**
https://leetcode.com/problems/maximum-area-of-a-piece-of-cake-after-horizontal-and-vertical-cuts/discuss/661995/

</p>


### [Python3] Easy Short DFS
- Author: localhostghost
- Creation Date: Sun May 31 2020 12:06:50 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 07 2020 01:06:27 GMT+0800 (Singapore Standard Time)

<p>
Start from `node 0` (the capital) and dfs on the path and see if the path is 
in the same direction as the traversal. If it is on the same direction that 
means we need to reverse it because it can never get to the capital.

```
class Solution:
    def minReorder(self, n: int, connections: List[List[int]]) -> int:
        self.res = 0    
        roads = set()
        graph = collections.defaultdict(list)
        for u, v in connections:
            roads.add((u, v))
            graph[v].append(u)
            graph[u].append(v)
        def dfs(u, parent):
            self.res += (parent, u) in roads
            for v in graph[u]:
                if v == parent:
                    continue
                dfs(v, u)    
        dfs(0, -1)
        return self.res
```
Don\'t forget to upvote if you found this useful. Thank you.
</p>


