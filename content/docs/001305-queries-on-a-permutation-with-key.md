---
title: "Queries on a Permutation With Key"
weight: 1305
#id: "queries-on-a-permutation-with-key"
---
## Description
<div class="description">
<p>Given the array <code>queries</code> of positive integers between <code>1</code> and <code>m</code>, you have to process all <code>queries[i]</code> (from <code>i=0</code> to <code>i=queries.length-1</code>) according to the following rules:</p>

<ul>
	<li>In the beginning, you have the permutation <code>P=[1,2,3,...,m]</code>.</li>
	<li>For the current <code>i</code>, find the position of <code>queries[i]</code> in the permutation <code>P</code> (<strong>indexing from 0</strong>) and then move this at the beginning of the permutation <code>P.</code>&nbsp;Notice that the position of <code>queries[i]</code> in <code>P</code> is the result for <code>queries[i]</code>.</li>
</ul>

<p>Return an array containing the result for the given <code>queries</code>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> queries = [3,1,2,1], m = 5
<strong>Output:</strong> [2,1,2,1] 
<strong>Explanation:</strong> The queries are processed as follow: 
For i=0: queries[i]=3, P=[1,2,3,4,5], position of 3 in P is <strong>2</strong>, then we move 3 to the beginning of P resulting in P=[3,1,2,4,5]. 
For i=1: queries[i]=1, P=[3,1,2,4,5], position of 1 in P is <strong>1</strong>, then we move 1 to the beginning of P resulting in P=[1,3,2,4,5]. 
For i=2: queries[i]=2, P=[1,3,2,4,5], position of 2 in P is <strong>2</strong>, then we move 2 to the beginning of P resulting in P=[2,1,3,4,5]. 
For i=3: queries[i]=1, P=[2,1,3,4,5], position of 1 in P is <strong>1</strong>, then we move 1 to the beginning of P resulting in P=[1,2,3,4,5]. 
Therefore, the array containing the result is [2,1,2,1].  
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> queries = [4,1,2,2], m = 4
<strong>Output:</strong> [3,1,2,0]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> queries = [7,5,5,8,3], m = 8
<strong>Output:</strong> [6,5,0,7,5]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= m &lt;= 10^3</code></li>
	<li><code>1 &lt;= queries.length &lt;= m</code></li>
	<li><code>1 &lt;= queries[i] &lt;= m</code></li>
</ul>
</div>

## Tags
- Array (array)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Python] Fenwick tree, O(n log n)
- Author: awice
- Creation Date: Sun Apr 12 2020 12:11:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 12 2020 12:11:00 GMT+0800 (Singapore Standard Time)

<p>
Let\'s use a map `vimap` from values to index positions.  Initially, all the values will start at positions [n+1, ..., 2n]; every time we pop a value, it will be placed again at positions n, n-1, n-2, etc.

We also use a fenwick tree.  This data structure is kind of like a virtual array `A` that lets us do two things in O(n log n) time: `.add(i, x)` is like `A[i] += x`, and `.sum(i)` is like `sum(A[:i+1])`.

Now for every query value, we can use the fenwick tree to find the rank of that query value.

```
class Fenwick:
    def __init__(self, n):
        sz = 1
        while sz <= n:
            sz *= 2
        self.size = sz
        self.data = [0] * sz

    def sum(self, i):
        s = 0
        while i > 0:
            s += self.data[i]
            i -= i & -i
        return s

    def add(self, i, x):
        while i < self.size:
            self.data[i] += x
            i += i & -i

class Solution2(object):
    def processQueries(self, queries, n):
        fenw = Fenwick(2 * n)
        vimap = {}
        for i in range(1, n + 1):
            fenw.add(i + n, 1)
            vimap[i] = i + n
        cur = n
        
        ans = []
        for q in queries:
            i = vimap.pop(q)
            rank = fenw.sum(i-1)
            ans.append(rank)
            
            vimap[q] = cur
            fenw.add(i, -1)
            fenw.add(cur, 1)
            cur -= 1
        return ans
```
</p>


### [Python,C++] NlogN Short Fenwick Tree With Detailed Explanations
- Author: aravind-kumar
- Creation Date: Sun Apr 12 2020 12:18:14 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 14 2020 15:05:36 GMT+0800 (Singapore Standard Time)

<p>
Naive approach :
Find the position of the element, 
Pick every element in quire[I] and insert it in the beginning of the array
Update the position of all the elements after the 0

We can implement, it using a vector
The time complexity = O(n^2) and space complexity = O(n)

Better Approach :
We can observe that we have 3 operations that needed to be performed
1.Push_front : takes O(n)
2.Find the index of a number : O(1) if use hashmap 
3.Update the index\u2019s of the rest of the elements from index 1 : takes O(n)

We can keep the elements in a sorted manner using a treeset/set.
 However finding the position of element by looking at the number of elements < or > element[I] would take O(n) which brings the time complexity to O(n^2)

An efficient Data structure that can find the number of elements < or > an element[I] is Fenwick tree.

I use fenwick tree to solve the problem.
The idea is instead of pushing the element to the front i.e assigning an index 0 for every query.
I assign -1 for the first query, -2 for the second query, -3 for the third \u2026
By doing so we can observe that range of index\u2019s = [-m,m]
However we know that array index cannot be negative, Hence i encode it such that 
-m = 0
-m+1 = 1
-m+2 = 2
.
.
.
m=2m.
Basically right shifting the range to start from 0. 
By doing so our new range is [0,2m]

Operations we perform:
	We initialize our Fenwick tree of size 2m
	Set all the values from [1\u2026m] i.e [m..2m] to 1
	For every query 
		we find its postion by finding the number of set elements < the given query.
		we set it\'s postion to 0 in fenwick tree
		set its new to -1,-2,-3 i.e (m,m-1,m-2)

Time complextiy : O(nlogn), space(2m)
		
```
class Solution:
    def processQueries(self, queries: List[int], m: int) -> List[int]:
        
        tree = [0] * ((2*m) + 1)
        res = []
        
        def update(i,val):
            while i<len(tree):
                tree[i]+=val
                i+=(i&(-i))
    
        def prefixSum(i):
            s=0
            while i>0:
                s+=tree[i]
                i-=(i&(-i))
            return s
        
        hmap = collections.defaultdict(int)
        
        for i in range(1,m+1):
            hmap[i] = i+m
            update(i+m,1)

        for i in queries:
            res.append(prefixSum(hmap[i])-1)
            update(hmap[i],-1)
            update(m,1)
            hmap[i] = m
            m-=1

        return res
```
C++:
```
class Solution {
public:
    void update(vector<int>& tree,int i,int val) {
        while(i<tree.size()) {
            tree[i]+=val;
            i+=(i&(-i));
        }
    }
    
    int getSum(vector<int>& tree,int i) {
        int s = 0;
        while(i>0) {
            s+=tree[i];
            i-=(i&(-i));
        }
        return s;
    }
    
    vector<int> processQueries(vector<int>& queries, int m) {
        vector<int> res,tree((2*m)+1,0);
        unordered_map<int,int> hmap;
        for(int i=1;i<=m;++i) {
            hmap[i] = i+m;
            update(tree,i+m,1);
        }

        for(int querie : queries) {
            res.push_back(getSum(tree,hmap[querie])-1);
            update(tree,hmap[querie],-1);
            update(tree,m,1);
            hmap[querie] = m;
            m--;
        }  
        return res;     
    }
};
```
		
		
</p>


### Java solution using LinkedList
- Author: too-young-too-naive
- Creation Date: Sun Apr 12 2020 12:10:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 12 2020 12:10:50 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int[] processQueries(int[] queries, int m) {
        LinkedList<Integer> P = new LinkedList<>();
        for (int i=1; i<=m;i++)
            P.add(i);
        
        int[] res = new int[queries.length];
        
        for (int i=0; i<queries.length; i++) {
            int q = queries[i];
            int idx = P.indexOf(q);
            int val = P.get(idx);
            P.remove(idx);
            P.addFirst(val);
            res[i] = idx;
        }
        
        return res;
    }
}
```
</p>


