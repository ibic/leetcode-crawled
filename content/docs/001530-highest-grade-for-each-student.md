---
title: "Highest Grade For Each Student"
weight: 1530
#id: "highest-grade-for-each-student"
---
## Description
<div class="description">
<p>Table: <code>Enrollments</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| student_id    | int     |
| course_id     | int     |
| grade         | int     |
+---------------+---------+
(student_id, course_id) is the primary key of this table.

</pre>

<p>Write a SQL query to find the highest grade with its corresponding course for each student. In case of a tie, you should find the course with the smallest&nbsp;<code>course_id</code>. The output must be sorted by increasing <code>student_id</code>.</p>

<p>The query result format is in the following example:</p>

<pre>
Enrollments table:
+------------+-------------------+
| student_id | course_id | grade |
+------------+-----------+-------+
| 2          | 2         | 95    |
| 2          | 3         | 95    |
| 1          | 1         | 90    |
| 1          | 2         | 99    |
| 3          | 1         | 80    |
| 3          | 2         | 75    |
| 3          | 3         | 82    |
+------------+-----------+-------+

Result table:
+------------+-------------------+
| student_id | course_id | grade |
+------------+-----------+-------+
| 1          | 2         | 99    |
| 2          | 2         | 95    |
| 3          | 3         | 82    |
+------------+-----------+-------+
</pre>

</div>

## Tags


## Companies
- Coursera - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Clear SQL solurion
- Author: ellie7947
- Creation Date: Tue Jul 09 2019 11:24:45 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 09 2019 11:24:45 GMT+0800 (Singapore Standard Time)

<p>
SELECT student_id, MIN(course_id) AS course_id, grade
FROM Enrollments
WHERE (student_id, grade) IN 
(SELECT student_id, MAX(grade)
FROM Enrollments
GROUP BY student_id) 
GROUP BY student_id, grade
ORDER BY student_id
</p>


### SQL Solution using row_number Faster than 100% users
- Author: swapna1487
- Creation Date: Tue Jul 09 2019 12:47:16 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 09 2019 12:48:16 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT t.student_id, t.course_id, t.grade
FROM 
	(SELECT student_id, course_id, grade, 
	row_number() over (partition by student_id order by grade desc, course_id asc) as r 
	FROM Enrollments) t
WHERE t.r=1
ORDER BY t.student_id asc
```
</p>


### Simple 3 line code solution
- Author: sophiesu0827
- Creation Date: Wed Dec 18 2019 06:34:31 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Dec 18 2019 06:34:31 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT DISTINCT student_id,MIN(course_id) AS course_id,grade FROM Enrollments
WHERE (student_id,grade) IN (SELECT DISTINCT student_id, max(grade) FROM Enrollments GROUP BY student_id)
GROUP BY student_id ORDER BY student_id;
```
</p>


