---
title: "Intersection of Two Arrays"
weight: 332
#id: "intersection-of-two-arrays"
---
## Description
<div class="description">
<p>Given two arrays, write a function to compute their intersection.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>nums1 = <span id="example-input-1-1">[1,2,2,1]</span>, nums2 = <span id="example-input-1-2">[2,2]</span>
<strong>Output: </strong><span id="example-output-1">[2]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>nums1 = <span id="example-input-2-1">[4,9,5]</span>, nums2 = <span id="example-input-2-2">[9,4,9,8,4]</span>
<strong>Output: </strong><span id="example-output-2">[9,4]</span></pre>
</div>

<p><b>Note:</b></p>

<ul>
	<li>Each element in the result must be unique.</li>
	<li>The result can be in any order.</li>
</ul>

<p>&nbsp;</p>

</div>

## Tags
- Hash Table (hash-table)
- Two Pointers (two-pointers)
- Binary Search (binary-search)
- Sort (sort)

## Companies
- Facebook - 9 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Oracle - 6 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Lyft - 11 (taggedByAdmin: false)
- Apple - 4 (taggedByAdmin: false)
- Indeed - 4 (taggedByAdmin: false)
- JPMorgan - 2 (taggedByAdmin: false)
- Two Sigma - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Two Sets

**Intuition**

The naive approach would be to iterate along the first array `nums1`
and to check for each value if this value in `nums2` or not. 
If yes - add the value to output. Such an approach would result 
in a pretty bad
$$\mathcal{O}(n \times m)$$ time complexity, where `n` and `m` are 
arrays' lengths.

> To solve the problem in linear time, let's use the structure `set`,
which provides `in/contains` operation in $$\mathcal{O}(1)$$ time in
average case.

The idea is to convert both arrays into sets, and then iterate over 
the smallest set checking the presence of each element in the larger set.
Time complexity of this approach is $$\mathcal{O}(n + m)$$ in the average case.

!?!../Documents/349_LIS.json:1000,352!?!

**Implementation**

<iframe src="https://leetcode.com/playground/QbhdgTQp/shared" frameBorder="0" width="100%" height="395" name="QbhdgTQp"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n + m)$$, where `n` and `m` are 
arrays' lengths. $$\mathcal{O}(n)$$ time is used to convert `nums1`
into set, $$\mathcal{O}(m)$$ time is used to convert `nums2`, and
`contains/in` operations are $$\mathcal{O}(1)$$ in the average case.
 
* Space complexity : $$\mathcal{O}(m + n)$$ in the worst case when
all elements in the arrays are different.
<br />
<br />


---
#### Approach 2: Built-in Set Intersection

**Intuition**

There are built-in intersection facilities,
which provide $$\mathcal{O}(n + m)$$ time complexity in the 
average case and $$\mathcal{O}(n \times m)$$ time complexity in the 
worst case. 

> In Python it's [intersection operator](https://wiki.python.org/moin/TimeComplexity#set), 
in Java - [retainAll() function](https://docs.oracle.com/javase/8/docs/api/java/util/AbstractCollection.html#retainAll-java.util.Collection-).

**Implementation**

<iframe src="https://leetcode.com/playground/ukbkHM7f/shared" frameBorder="0" width="100%" height="310" name="ukbkHM7f"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(n + m)$$ in the average case 
 and $$\mathcal{O}(n \times m)$$ [in the worst case
 when load factor is high enough](https://wiki.python.org/moin/TimeComplexity#set).
 
* Space complexity : $$\mathcal{O}(n + m)$$ in the worst case when
all elements in the arrays are different.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Three Java Solutions
- Author: divingboy89
- Creation Date: Thu May 19 2016 00:41:25 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 04:39:27 GMT+0800 (Singapore Standard Time)

<p>
Use two hash sets

Time complexity: O(n)

    public class Solution {
        public int[] intersection(int[] nums1, int[] nums2) {
            Set<Integer> set = new HashSet<>();
            Set<Integer> intersect = new HashSet<>();
            for (int i = 0; i < nums1.length; i++) {
                set.add(nums1[i]);
            }
            for (int i = 0; i < nums2.length; i++) {
                if (set.contains(nums2[i])) {
                    intersect.add(nums2[i]);
                }
            }
            int[] result = new int[intersect.size()];
            int i = 0;
            for (Integer num : intersect) {
                result[i++] = num;
            }
            return result;
        }
    }


Sort both arrays, use two pointers

Time complexity: O(nlogn)

    public class Solution {
        public int[] intersection(int[] nums1, int[] nums2) {
            Set<Integer> set = new HashSet<>();
            Arrays.sort(nums1);
            Arrays.sort(nums2);
            int i = 0;
            int j = 0;
            while (i < nums1.length && j < nums2.length) {
                if (nums1[i] < nums2[j]) {
                    i++;
                } else if (nums1[i] > nums2[j]) {
                    j++;
                } else {
                    set.add(nums1[i]);
                    i++;
                    j++;
                }
            }
            int[] result = new int[set.size()];
            int k = 0;
            for (Integer num : set) {
                result[k++] = num;
            }
            return result;
        }
    }

Binary search

Time complexity: O(nlogn)

    public class Solution {
        public int[] intersection(int[] nums1, int[] nums2) {
            Set<Integer> set = new HashSet<>();
            Arrays.sort(nums2);
            for (Integer num : nums1) {
                if (binarySearch(nums2, num)) {
                    set.add(num);
                }
            }
            int i = 0;
            int[] result = new int[set.size()];
            for (Integer num : set) {
                result[i++] = num;
            }
            return result;
        }
        
        public boolean binarySearch(int[] nums, int target) {
            int low = 0;
            int high = nums.length - 1;
            while (low <= high) {
                int mid = low + (high - low) / 2;
                if (nums[mid] == target) {
                    return true;
                }
                if (nums[mid] > target) {
                    high = mid - 1;
                } else {
                    low = mid + 1;
                }
            }
            return false;
        }
    }
</p>


### 8ms concise C++ using unordered_set
- Author: Kenigma
- Creation Date: Thu May 19 2016 03:12:04 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 15:26:11 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
        vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
            unordered_set<int> m(nums1.begin(), nums1.end());
            vector<int> res;
            for (auto a : nums2)
                if (m.count(a)) {
                    res.push_back(a);
                    m.erase(a);
                }
            return res;
        }
    };
</p>


### Small C++ solution
- Author: StefanPochmann
- Creation Date: Fri May 20 2016 21:25:35 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 00:24:29 GMT+0800 (Singapore Standard Time)

<p>
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        set<int> s(nums1.begin(), nums1.end());
        vector<int> out;
        for (int x : nums2)
            if (s.erase(x))
                out.push_back(x);
        return out;
    }
</p>


