---
title: "Minimum ASCII Delete Sum for Two Strings"
weight: 633
#id: "minimum-ascii-delete-sum-for-two-strings"
---
## Description
<div class="description">
<p>Given two strings <code>s1, s2</code>, find the lowest ASCII sum of deleted characters to make two strings equal.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> s1 = "sea", s2 = "eat"
<b>Output:</b> 231
<b>Explanation:</b> Deleting "s" from "sea" adds the ASCII value of "s" (115) to the sum.
Deleting "t" from "eat" adds 116 to the sum.
At the end, both strings are equal, and 115 + 116 = 231 is the minimum sum possible to achieve this.
</pre>
</p>

<p><b>Example 2:</b><br />
<pre>
<b>Input:</b> s1 = "delete", s2 = "leet"
<b>Output:</b> 403
<b>Explanation:</b> Deleting "dee" from "delete" to turn the string into "let",
adds 100[d]+101[e]+101[e] to the sum.  Deleting "e" from "leet" adds 101[e] to the sum.
At the end, both strings are equal to "let", and the answer is 100+101+101+101 = 403.
If instead we turned both strings into "lee" or "eet", we would get answers of 433 or 417, which are higher.
</pre>
</p>

<p><b>Note:</b>
<li><code>0 < s1.length, s2.length <= 1000</code>.</li>
<li>All elements of each string will have an ASCII value in <code>[97, 122]</code>.</li> 
</p>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- TripleByte - 4 (taggedByAdmin: true)

## Official Solution
[TOC]


#### Approach #1: Dynamic Programming [Accepted]

**Intuition and Algorithm**

Let `dp[i][j]` be the answer to the problem for the strings `s1[i:], s2[j:]`.

When one of the input strings is empty, the answer is the ASCII-sum of the other string.  We can calculate this cumulatively using code like `dp[i][s2.length()] = dp[i+1][s2.length()] + s1.codePointAt(i)`.

When `s1[i] == s2[j]`, we have `dp[i][j] = dp[i+1][j+1]` as we can ignore these two characters.

When `s1[i] != s2[j]`, we will have to delete at least one of them.  We'll have `dp[i][j]` as the minimum of the answers after both deletion options.

The solutions presented will use *bottom-up* dynamic programming.

**Python**
```python
class Solution(object):
    def minimumDeleteSum(self, s1, s2):
        dp = [[0] * (len(s2) + 1) for _ in xrange(len(s1) + 1)]

        for i in xrange(len(s1) - 1, -1, -1):
            dp[i][len(s2)] = dp[i+1][len(s2)] + ord(s1[i])
        for j in xrange(len(s2) - 1, -1, -1):
            dp[len(s1)][j] = dp[len(s1)][j+1] + ord(s2[j])

        for i in xrange(len(s1) - 1, -1, -1):
            for j in xrange(len(s2) - 1, -1, -1):
                if s1[i] == s2[j]:
                    dp[i][j] = dp[i+1][j+1]
                else:
                    dp[i][j] = min(dp[i+1][j] + ord(s1[i]),
                                   dp[i][j+1] + ord(s2[j]))

        return dp[0][0]
```

**Java**
```java
class Solution {
    public int minimumDeleteSum(String s1, String s2) {
        int[][] dp = new int[s1.length() + 1][s2.length() + 1];

        for (int i = s1.length() - 1; i >= 0; i--) {
            dp[i][s2.length()] = dp[i+1][s2.length()] + s1.codePointAt(i);
        }
        for (int j = s2.length() - 1; j >= 0; j--) {
            dp[s1.length()][j] = dp[s1.length()][j+1] + s2.codePointAt(j);
        }
        for (int i = s1.length() - 1; i >= 0; i--) {
            for (int j = s2.length() - 1; j >= 0; j--) {
                if (s1.charAt(i) == s2.charAt(j)) {
                    dp[i][j] = dp[i+1][j+1];
                } else {
                    dp[i][j] = Math.min(dp[i+1][j] + s1.codePointAt(i),
                                        dp[i][j+1] + s2.codePointAt(j));
                }
            }
        }
        return dp[0][0];
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(M*N)$$, where $$M, N$$ are the lengths of the given strings.  We use nested for loops: each loop is $$O(M)$$ and $$O(N)$$ respectively.

* Space Complexity: $$O(M*N)$$, the space used by `dp`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++, DP, with explanation
- Author: zestypanda
- Creation Date: Sun Oct 22 2017 12:18:36 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 12 2018 12:00:26 GMT+0800 (Singapore Standard Time)

<p>
This is clearly a DP problem.  
```
dp[i][j] is the cost for s1.substr(0,i) and s2.substr(0, j). Note s1[i], s2[j] not included in the substring.

Base case: dp[0][0] = 0
target: dp[m][n]

if s1[i-1] = s2[j-1]   // no deletion
    dp[i][j] = dp[i-1][j-1];
else   // delete either s1[i-1] or s2[j-1]
    dp[i][j] = min(dp[i-1][j]+s1[i-1], dp[i][j-1]+s2[j-1]);
``` 
We can use a 2D vector, or an optimized O(n) extra space. See below. The run time is O(mn).
```
class Solution {
public:
    int minimumDeleteSum(string s1, string s2) {
        int m = s1.size(), n = s2.size();
        vector<vector<int>> dp(m+1, vector<int>(n+1, 0));
        for (int j = 1; j <= n; j++)
            dp[0][j] = dp[0][j-1]+s2[j-1];
        for (int i = 1; i <= m; i++) {
            dp[i][0] = dp[i-1][0]+s1[i-1];
            for (int j = 1; j <= n; j++) {
                if (s1[i-1] == s2[j-1])
                    dp[i][j] = dp[i-1][j-1];
                else 
                    dp[i][j] = min(dp[i-1][j]+s1[i-1], dp[i][j-1]+s2[j-1]);
            }
        }
        return dp[m][n];
    }
};
```
Optimized O(n) extra space
```
class Solution {
public:
    int minimumDeleteSum(string s1, string s2) {
        int m = s1.size(), n = s2.size();
        vector<int> dp(n+1, 0);
        for (int j = 1; j <= n; j++)
            dp[j] = dp[j-1]+s2[j-1];
        for (int i = 1; i <= m; i++) {
            int t1 = dp[0];
            dp[0] += s1[i-1];
            for (int j = 1; j <= n; j++) {
                int t2 = dp[j];
                dp[j] = s1[i-1] == s2[j-1]? t1:min(dp[j]+s1[i-1], dp[j-1]+s2[j-1]);
                t1 = t2;
            }
        }
        return dp[n];
    }
};
```
</p>


### [Java]{DP}(With Explanation)
- Author: _thalaiva_
- Creation Date: Sun Oct 22 2017 13:14:38 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 21 2018 02:39:12 GMT+0800 (Singapore Standard Time)

<p>
Very Similar to Longest Common Subsequence Problem.

Let, s1 & s2 be the two strings with 1 based indexes.
Now assume, dp[i][j] = minimumDeleteSum( s1[0,i], s2[0,j])

**Base case**:
When either of the strings is empty, then whole of the other string has to be deleted.
for e.g. if s1 = "", s2 = "abc", then only way we could match these strings by deleting characters is by dropping 'a','b','c' of s2 to make it empty like s1.

Thus, whenever one of them is empty(i.e. i==0 or j==0) then answer is sum of ASCII code of the characters of the other string.

Hence the ***1st*** rule: **dp[i][j] =** 

* **sum_ascii(s2) -> if i==0**
* **sum_ascii(s1) -> if j==0**

**Non-Base case**

Of the two strings, if both of their last characters match then certainly the answer comes from skipping those characters. 
i.e. Answer("zca","bza") = Answer("zc","bz")

Hence the ***2nd*** rule: **dp[i][j] =** 

* **dp[i-1][j-1] -> if s1[i]==s2[j]**

Finally, if the last characters are different then its one of the three situations:

* drop s1's last character (ASCII(s1's last) + dp[i-1][j])
* drop s2's last character (ASCII(s2's last) + dp[i][j-1])
* drop both last characters (ASCII(s1's last) + ASCII(s2's last) + dp[i-1[[j-1])

Hence the ***3rd*** rule: **dp[i][j] =** 

* **Min((ASCII(s1's last) + dp[i-1][j]),(ASCII(s2's last) + dp[i][j-1]),(ASCII(s1's last) + ASCII(s2's last) + dp[i-1[[j-1]))**

Combining these 3 rules gives us an elegant solution.
```
public int minimumDeleteSum(String s1, String s2) {
        int m = s1.length();
        int n = s2.length();
        int[][] dp = new int[m+1][n+1];
        for(int i=0;i<=m;i++){
            for(int j=0;j<=n;j++){
                if(i==0 || j==0){
                    int a = 0;
                    for(int z=1;z<=Math.max(j,i);z++){
                        a += (i==0?s2.charAt(z-1):s1.charAt(z-1));
                    }
                    dp[i][j] = a;
                }
                else if(s1.charAt(i-1)==s2.charAt(j-1)){
                    dp[i][j] = dp[i-1][j-1];
                }
                else{
                    dp[i][j] = Math.min(s1.charAt(i-1)+dp[i-1][j],s2.charAt(j-1)+dp[i][j-1]);
                    dp[i][j] = Math.min(dp[i][j],s1.charAt(i-1)+s2.charAt(j-1)+dp[i-1][j-1]);
                }
            }
        }
        return dp[m][n];
    }
```
</p>


### for those who have no clue : step by step
- Author: RohanPrakash
- Creation Date: Thu May 21 2020 01:46:46 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Aug 29 2020 20:14:59 GMT+0800 (Singapore Standard Time)

<p>
before getting onto DP, One must master the art of recurssion
In this post , WE will learn how to solve this/approach question\'s on on Dynamic Programming Question
 
**Step 1 : Understanding the problem** 
In this question we are given two strings , and asked to get the sum of ASCII values, to make string same,
RE-WORDING question : get the sum of all characters which do not form longest Common Subsequence.

**Step 2 : Logic Building and Recurrance relation**
**sub step 1 : Analyzing different casses and Logic building** 
there can be only two cases
```
case 1 : some ith and jth character matches in both string
case 2 : it doesn\'t matches

so we have two casses , lets us analyze them one by one 
case 1 : if some ith and jth character matches then we can reduce the ASCII sum if we include both ith and jth character in sequence, so we dont add their\'s ASCII values 
case 2 : ith and jth character doesn\'t matches , so we have 2 option for these
	option 1 : skip ith character assuming jth character might be useful later on , so we add ASCII of ith and recurr for rest 
	option 2 : skip jth character assuming ith matches somewhere late in string , same as option 1 for other string
```
**sub step 2 : gettign basecases and writing recurrence relation**
Base cases
```
if string_A or string_B is empty : then our ans is sum of all ASCII of non empty string
if we have finished strings : our ans is zero
```
Recurrance relation
```
if string_a[ ith ] EQUALS string_b [jth] // skip these as these will definately help in making string same , and help in reducing sum of ASCII
 sum = get_sum_for( string_a ith+1 , string_b jth+1)
 
if string_a[ ith ] NOT EQUALS string_b [jth] // then all minimum of all the three options 
option 1 : sum = ASCII of string_a[ith] + get_sum_for( string_a ith+1, string_b jth)  // assuming jth to be useful
option 2 : sum = ASCII of string_b[jth] + get_sum_for( string_a ith, string_b jth +1) // assuming ith to be useful

ans = min of ( option 1 , option 2 )
```

**step 3 : so now lets code pure Recurssive solution (TLE)** 
```
int dead_end_sum(string &s , int i)
    {
        int sum = 0;
        for( ; i<s.length() ; i++)
            sum+=int(s[i]);
        return sum;
    }
    
    int sub(string &a, string &b, int i, int j)
    {
        int n = a.length();
        int m = b.length();
        int sum = 0;
        if(i==n || j==m)
        {
            if(i==n and j==m)   return 0;
            return (i==n) ? dead_end_sum(b,j) : dead_end_sum(a,i);   // on of the string is empty at this point , remember base case... 
        }
    
        if(a[i] == b[j])    // 1s case , character matched
            sum = sub(a,b,i+1,j+1);
        else
        {   // characters didn\'t matched ie Our\'s second case
            sum = min({ sub(a,b,i+1,j) + int(a[i]) ,                // option 1
                        sub(a,b,i,j+1) + int(b[j]) });               // option 2
        }      
        return sum;
    }
    int minimumDeleteSum(string a, string b) {
        
        return sub(a,b,0,0);
        
        return 0;
    }
```

**step 4 : Convert recurssion to Top-down Dymanic(ACCEPTED)** 
so we know we are computing for same sub problems again and again, so what we can do is maintain a matrix to store answers for our subproblem , so if at any point we need to recompute it , we can avoid re-computation by using that value, ( optimizing recurssion )
```
vector<vector<int>> dp; // we will store all our answers here and then get values for all future references
    
    int dead_end_sum(string &s , int i) // one of the string id empty , so all ASCII sums from ith character till end of string
    {
        int sum = 0;
        for( ; i<s.length() ; i++)
            sum+=int(s[i]);
        return sum;
    }
    
    int sub(string &a, string &b, int i, int j) // sub problem
    {
        int n = a.length();
        int m = b.length();
        int sum = 0;
        if(i==n || j==m)
        {
            if(i==n and j==m)   return 0;
            return (i==n) ? dead_end_sum(b,j) : dead_end_sum(a,i);
        }
        
        if(dp[i][j] != -1)            return dp[i][j];      // we know the answer so no need to recompute , reture it as it is.
    
        if(a[i] == b[j])
            sum = sub(a,b,i+1,j+1);
        else
        {
            sum = min({ sub(a,b,i+1,j) + int(a[i]) ,                // option  1
                        sub(a,b,i,j+1) + int(b[j])});              // option 2
        }      
        dp[i][j] = sum;     // we store our answer at each step
        return sum;
    }
    int minimumDeleteSum(string a, string b) {
        
        // making of DP matrix to store result and initilizing it to -1
        dp = vector<vector<int>>(a.length()+1, vector<int>(b.length()+1 , -1));
        return sub(a,b,0,0);
        
        return 0;
    }
```
**step 5 : so now you know both the approaches , so try coming up with botto up DP yourself.**
`hint : strat from base case , go all the way to the top`
This is how DP table will look like 
![image](https://assets.leetcode.com/users/_ro/image_1589998557.png)

```
int minimumDeleteSum(string a, string b) {
        
    int n = a.size();
    int m = b.size();
    vector<vector<int>> dp(n+1 , vector<int> (m+1));

    for(int i=1 ; i<=n ; i++)                 // base case filling up
        dp[i][0] = dp[i-1][0] + a[i-1];

    for(int j=1; j<=m ; j++)                 // base case filling up
        dp[0][j] = dp[0][j-1] + b[j-1];

    for(int i=1 ; i<=n ; i++)
    {
        for(int j=1 ; j<=m ; j++)
        {
            if(a[i-1] == b[j-1])
                dp[i][j] = dp[i-1][j-1];
            else
                dp[i][j] = min( dp[i-1][j] + a[i-1] ,
                                dp[i][j-1] + b[j-1] );
        }
    }
    return dp[n][m];
    }
```

**thanks for reading**
**previous tutorial on DP**  
https://leetcode.com/problems/minimum-cost-for-tickets/discuss/630868/explanation-from-someone-who-took-2-hours-to-solve
https://leetcode.com/problems/ones-and-zeroes/discuss/814077/Dedicated-to-Beginners
https://leetcode.com/problems/dungeon-game/discuss/745340/post-Dedicated-to-beginners-of-DP
</p>


