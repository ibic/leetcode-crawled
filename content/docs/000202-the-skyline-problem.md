---
title: "The Skyline Problem"
weight: 202
#id: "the-skyline-problem"
---
## Description
<div class="description">
<p>A city&#39;s skyline is the outer contour of the silhouette formed by all the buildings in that city when viewed from a distance. Now suppose you are <b>given the locations and height of all the buildings</b> as shown on a cityscape photo (Figure A), write a program to <b>output the skyline</b> formed by these buildings collectively (Figure B).</p>
<a href="/static/images/problemset/skyline1.jpg" target="_blank"><img alt="Buildings" src="https://assets.leetcode.com/uploads/2018/10/22/skyline1.png" style="max-width: 45%; border-width: 0px; border-style: solid;" /> </a> <a href="/static/images/problemset/skyline2.jpg" target="_blank"> <img alt="Skyline Contour" src="https://assets.leetcode.com/uploads/2018/10/22/skyline2.png" style="max-width: 45%; border-width: 0px; border-style: solid;" /> </a>

<p>The geometric information of each building is represented by a triplet of integers <code>[Li, Ri, Hi]</code>, where <code>Li</code> and <code>Ri</code> are the x coordinates of the left and right edge of the ith building, respectively, and <code>Hi</code> is its height. It is guaranteed that <code>0 &le; Li, Ri &le; INT_MAX</code>, <code>0 &lt; Hi &le; INT_MAX</code>, and <code>Ri - Li &gt; 0</code>. You may assume all buildings are perfect rectangles grounded on an absolutely flat surface at height 0.</p>

<p>For instance, the dimensions of all buildings in Figure A are recorded as: <code>[ [2 9 10], [3 7 15], [5 12 12], [15 20 10], [19 24 8] ] </code>.</p>

<p>The output is a list of &quot;<b>key points</b>&quot; (red dots in Figure B) in the format of <code>[ [x1,y1], [x2, y2], [x3, y3], ... ]</code> that uniquely defines a skyline. <b>A key point is the left endpoint of a horizontal line segment</b>. Note that the last key point, where the rightmost building ends, is merely used to mark the termination of the skyline, and always has zero height. Also, the ground in between any two adjacent buildings should be considered part of the skyline contour.</p>

<p>For instance, the skyline in Figure B should be represented as:<code>[ [2 10], [3 15], [7 12], [12 0], [15 10], [20 8], [24, 0] ]</code>.</p>

<p><b>Notes:</b></p>

<ul>
	<li>The number of buildings in any input list is guaranteed to be in the range <code>[0, 10000]</code>.</li>
	<li>The input list is already sorted in ascending order by the left x position <code>Li</code>.</li>
	<li>The output list must be sorted by the x position.</li>
	<li>There must be no consecutive horizontal lines of equal height in the output skyline. For instance, <code>[...[2 3], [4 5], [7 5], [11 5], [12 7]...]</code> is not acceptable; the three lines of height 5 should be merged into one in the final output as such: <code>[...[2 3], [4 5], [12 7], ...]</code></li>
</ul>

</div>

## Tags
- Divide and Conquer (divide-and-conquer)
- Heap (heap)
- Binary Indexed Tree (binary-indexed-tree)
- Segment Tree (segment-tree)
- Line Sweep (line-sweep)

## Companies
- Microsoft - 4 (taggedByAdmin: true)
- Amazon - 4 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: true)
- Google - 3 (taggedByAdmin: true)
- Adobe - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Databricks - 2 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: false)
- Salesforce - 3 (taggedByAdmin: false)
- Houzz - 2 (taggedByAdmin: false)
- Tableau - 2 (taggedByAdmin: false)
- Yelp - 0 (taggedByAdmin: true)
- Twitter - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
  
#### Approach 1: Divide and Conquer 

**Solution template**

The problem is a classical example of divide and conquer 
approach, and typically implemented exactly the same way as 
merge sort algorithm.

Let's follow here a solution template for divide and conquer
problems :

- Define the base case(s).

- Split the problem into subproblems and solve them recursively.

- Merge the subproblems solutions into the problem solution.

**Algorithm**

getSkyline for `n` buildings :

- If `n == 0` : return an empty list.

- If `n == 1` : return the skyline for one building (it's straightforward).

- `leftSkyline` = getSkyline for the first n/2 buildings.

- `rightSkyline` = getSkyline for the last n/2 buildings.

- Merge `leftSkyline` and `rightSkyline`.

Now let's discuss each step in more details.

**Base cases**

The first base case is an empty `buildings` list.
Then the skyline is an empty list, too.

The second base case is the only one building in the list, when 
the skyline construction is quite straightforward.

![bla](../Figures/218/base5.png)

**How to split the problem**

The idea is the same as for merge sort : at each step 
split the list exactly in two parts : from `0` to `n/2` and
 from `n/2` to `n`, and then construct 
the skylines recursively for each part.

![bla](../Figures/218/split5.png)

**How to merge two skylines**

The algorithm for merge function is quite straightforward 
and based on the same merge sort logic : the height of an output skyline is 
always a maximum between the left and right skylines. 

![bla](../Figures/218/merge5.png)

Let's use here two
pointers `pR` and `pL` to track the current element index in both 
skylines, and three integers `leftY`, `rightY`, and `currY` to track the current height
for the `left` skyline, `right` skyline and the merged skyline.

mergeSkylines (left, right) :

- currY = leftY = rightY = 0

- While we're in the region where both skylines are present 
(`pR < nR` and `pL < nL`) :
    
    - Pick up the element with the smallest `x` coordinate. If it's 
    an element from the left skyline, move `pL` and update `leftY`.
    If it's 
    an element from the right skyline, move `pR` and update `rightY`.

    - Compute the largest height at the current point :
    `maxY = max(leftY, rightY)`.
    
    - Update an output skyline by `(x, maxY)` point, if `maxY` is not equal to `currY`.
    
- While there are still elements in the left skyline (`pL < nL`), 
process them following the same logic as above.

- While there are still elements in the right skyline (`pR < nR`), 
process them following the same logic as above.

- Return output skyline.

> Here are three usecases to illustrate the merge algorithm execution

![bla](../Figures/218/merge3.png)

![bla](../Figures/218/merge4.png)

![bla](../Figures/218/merge7.png)

**Implementation**

<iframe src="https://leetcode.com/playground/inWJEEty/shared" frameBorder="0" width="100%" height="500" name="inWJEEty"></iframe>

* Time complexity : $$\mathcal{O}(N \log N)$$, where $$N$$ is number of
buildings. The problem is an example of 
[Master Theorem case II](https://en.wikipedia.org/wiki/Master_theorem_(analysis_of_algorithms)#Case_2_example)
 : $$T(N) = 2 T(\frac{N}{2}) + 2N$$, that results in $$\mathcal{O}(N \log N)$$
 time complexity.
* Space complexity : $$\mathcal{O}(N)$$ to keep the output.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### (Guaranteed) Really Detailed and Good (Perfect) Explanation of The Skyline Problem
- Author: liuyifly06
- Creation Date: Wed Mar 09 2016 01:23:42 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 21 2018 14:00:23 GMT+0800 (Singapore Standard Time)

<p>
Please, do not ever say detailed explanation if you are not explaining you thoughts very clear!
Just posting code with some comments is not explanation at all, OK? 
 
All the posts I have seen with explanations is just different style of obscure.

Check the following link, that is authentic detailed explanation! your understanding is almost GUARANTEED!

[https://briangordon.github.io/2014/08/the-skyline-problem.html][1]
 


  [1]: https://briangordon.github.io/2014/08/the-skyline-problem.html
</p>


### Short Java solution
- Author: jinwu
- Creation Date: Wed Aug 26 2015 02:10:52 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 10:10:52 GMT+0800 (Singapore Standard Time)

<p>
    	public List<int[]> getSkyline(int[][] buildings) {
	    List<int[]> result = new ArrayList<>();
	    List<int[]> height = new ArrayList<>();
	    for(int[] b:buildings) {
	        height.add(new int[]{b[0], -b[2]});
	        height.add(new int[]{b[1], b[2]});
	    }
	    Collections.sort(height, (a, b) -> {
	            if(a[0] != b[0]) 
	                return a[0] - b[0];
	            return a[1] - b[1];
	    });
	    Queue<Integer> pq = new PriorityQueue<>((a, b) -> (b - a));
	    pq.offer(0);
	    int prev = 0;
	    for(int[] h:height) {
	        if(h[1] < 0) {
	            pq.offer(-h[1]);
	        } else {
	            pq.remove(h[1]);
	        }
	        int cur = pq.peek();
	        if(prev != cur) {
	            result.add(new int[]{h[0], cur});
	            prev = cur;
	        }
	    }
	    return result;
	}
</p>


### Once for all, explanation with clean Java code(O(n^2)time, O(n) space)
- Author: rainforestin
- Creation Date: Sat Oct 31 2015 13:13:58 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 09:49:26 GMT+0800 (Singapore Standard Time)

<p>
Though I came up with  a solution using PriorityQueue and BST, this problems still confuses me. To make it more clear, I went through it several times and investigated several good solutions on this forum.

Here is my explanation which tries to make understanding this easier and may help you write a bug-free solution quickly.

When visiting all start points and end points in order:

Observations:

1. If a position is shadowed by other buildings

        1. height of that building is larger than the building to which current
            position belong;
        2. the start point of that building must be smaller(or equal to) than this
            position;
        3. the end point of that building must be larger(or equal to) than this
            position;

Tus we have:

    1. when you reach a start point, the height of current building immediately
        takes effect which means it could possibly affect the contour or shadow
        others when mixed with other following buildings;
    2. when you reach a end point, the height of current building will stop its
        influences;
    3. our target exists at the position where height change happens and there
        is nothing above it shadowing it;

Obviously, to implement the idea that 'current height takes effect' and 'find out whether current height is shadowed by other buildings',  we need a mechanism to store current taking effect heights, meanwhile, figure out which one is the maximum, delete it if needed efficiently, which hints us to use a priority queue or BST.

Thus, our algorithm could be summarised in following pseudo code:

    for position in sorted(all start points and all end points)
           if this position is a start point
                  add its height
           else if this position is a end point
                  delete its height
           compare current max height with previous max height, if different, add
           current position together with this new max height to our result, at the
           same time, update previous max height to current max height;

To implement this algorithm, here are some concrete examples:

1. In my implementation, I use a PriorityQueue to store end point values when visiting a start point, and store the [height, end point value] into a TreeMap. Thus:
      1. when moving to next start point value, I can compare the next start point value with elements in PriorityQueue, thus achieving visiting all start points and end points in order(exploits the fact that start points are already sorted);
      2. Meantime, I can get current max height from TreeMap in O(logn);
      3. However, to delete a height when visiting a end point, I have to use 'map.values.remove()' which is a method defined in Collection interface and tends to be slower(O(n) is this case, plz correct me if I'm wrong);

My code can be found at https://leetcode.com/discuss/62617/short-and-clean-java-solution-heap-and-treemap

2. Following is wujin's implementation(plz refer to https://leetcode.com/discuss/54201/short-java-solution). This one is quite straightforward, clean and clever.

Firstly, please notice what we need to achieve:

      1. visit all start points and all end points in order;
      2. when visiting a point, we need to know whether it is a start point or a
          end point, based on which we can add a height or delete a height from
          our data structure;

To achieve this, his implementation:

      1. use a int[][] to collect all [start point, - height] and [end point, height]
          for every building;
      2. sort it, firstly based on the first value, then use the second to break
          ties;

Thus,

      1. we can visit all points in order;
      2. when points have the same value, higher height will shadow the lower one;
      3. we know whether current point is a start point or a end point based on the
          sign of its height;

His code is as follows(clear and concise) as reference with my comment(again, https://leetcode.com/discuss/54201/short-java-solution):

    public List<int[]> getSkyline(int[][] buildings) {
        List<int[]> result = new ArrayList<>();
        List<int[]> height = new ArrayList<>();
        for(int[] b:buildings) {
            // start point has negative height value
            height.add(new int[]{b[0], -b[2]});
            // end point has normal height value
            height.add(new int[]{b[1], b[2]}); 
        }

        // sort $height, based on the first value, if necessary, use the second to
        // break ties
        Collections.sort(height, (a, b) -> {
                if(a[0] != b[0]) 
                    return a[0] - b[0];
                return a[1] - b[1];
        });

        // Use a maxHeap to store possible heights
        Queue<Integer> pq = new PriorityQueue<>((a, b) -> (b - a));

        // Provide a initial value to make it more consistent
        pq.offer(0);

        // Before starting, the previous max height is 0;
        int prev = 0;

        // visit all points in order
        for(int[] h:height) {
            if(h[1] < 0) { // a start point, add height
                pq.offer(-h[1]);
            } else {  // a end point, remove height
                pq.remove(h[1]);
            }
            int cur = pq.peek(); // current max height;
      
            // compare current max height with previous max height, update result and 
            // previous max height if necessary
            if(prev != cur) {
                result.add(new int[]{h[0], cur});
                prev = cur;
            }
        }
        return result;
    }

Hopefully now, you can write a good solution in a blink with good understanding...
</p>


