---
title: "Department Highest Salary"
weight: 1478
#id: "department-highest-salary"
---
## Description
<div class="description">
<p>The <code>Employee</code> table holds all employees. Every employee has an Id, a salary, and there is also a column for the department Id.</p>

<pre>
+----+-------+--------+--------------+
| Id | Name  | Salary | DepartmentId |
+----+-------+--------+--------------+
| 1  | Joe   | 70000  | 1            |
| 2 &nbsp;| Jim &nbsp; | 90000 &nbsp;| 1 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;|
| 3  | Henry | 80000  | 2            |
| 4  | Sam   | 60000  | 2            |
| 5  | Max   | 90000  | 1            |
+----+-------+--------+--------------+
</pre>

<p>The <code>Department</code> table holds all departments of the company.</p>

<pre>
+----+----------+
| Id | Name     |
+----+----------+
| 1  | IT       |
| 2  | Sales    |
+----+----------+
</pre>

<p>Write a SQL query to find employees who have the highest salary in each of the departments.&nbsp;For the above tables, your SQL query should return the following rows (order of rows does not matter).</p>

<pre>
+------------+----------+--------+
| Department | Employee | Salary |
+------------+----------+--------+
| IT         | Max      | 90000  |
| IT &nbsp; &nbsp; &nbsp; &nbsp; | Jim &nbsp; &nbsp; &nbsp;| 90000 &nbsp;|
| Sales      | Henry    | 80000  |
+------------+----------+--------+
</pre>

<p><strong>Explanation:</strong></p>

<p>Max and Jim both have&nbsp;the highest salary in the IT department and Henry has the highest salary in the Sales department.</p>

</div>

## Tags


## Companies
- Amazon - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- Wayfair - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using `JOIN` and `IN` clause [Accepted]

**Algorithm**

Since the **Employee** table contains the *Salary* and *DepartmentId* information, we can query the highest salary in a department.

```sql
SELECT
    DepartmentId, MAX(Salary)
FROM
    Employee
GROUP BY DepartmentId;
```
>Note: There might be multiple employees having the same highest salary, so it is safe not to include the employee name information in this query.

```
| DepartmentId | MAX(Salary) |
|--------------|-------------|
| 1            | 90000       |
| 2            | 80000       |
```

Then, we can join table **Employee** and **Department**, and query the (DepartmentId, Salary) are in the temp table using `IN` statement as below.

**MySQL**

```sql
SELECT
    Department.name AS 'Department',
    Employee.name AS 'Employee',
    Salary
FROM
    Employee
        JOIN
    Department ON Employee.DepartmentId = Department.Id
WHERE
    (Employee.DepartmentId , Salary) IN
    (   SELECT
            DepartmentId, MAX(Salary)
        FROM
            Employee
        GROUP BY DepartmentId
	)
;
```
```
| Department | Employee | Salary |
|------------|----------|--------|
| Sales      | Henry    | 80000  |
| IT         | Max      | 90000  |
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Three accpeted solutions
- Author: kent-huang
- Creation Date: Thu Jan 22 2015 09:01:44 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 00:15:03 GMT+0800 (Singapore Standard Time)

<p>
    SELECT D.Name AS Department ,E.Name AS Employee ,E.Salary 
    FROM
    	Employee E,
    	(SELECT DepartmentId,max(Salary) as max FROM Employee GROUP BY DepartmentId) T,
    	Department D
    WHERE E.DepartmentId = T.DepartmentId 
      AND E.Salary = T.max
      AND E.DepartmentId = D.id

    SELECT D.Name,A.Name,A.Salary 
    FROM 
    	Employee A,
    	Department D   
    WHERE A.DepartmentId = D.Id 
      AND NOT EXISTS 
      (SELECT 1 FROM Employee B WHERE B.Salary > A.Salary AND A.DepartmentId = B.DepartmentId) 

    SELECT D.Name AS Department ,E.Name AS Employee ,E.Salary 
    from 
    	Employee E,
    	Department D 
    WHERE E.DepartmentId = D.id 
      AND (DepartmentId,Salary) in 
      (SELECT DepartmentId,max(Salary) as max FROM Employee GROUP BY DepartmentId)
</p>


### Simple solution, easy to understand
- Author: lemonxixi
- Creation Date: Thu Aug 27 2015 09:00:56 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 06 2018 14:59:03 GMT+0800 (Singapore Standard Time)

<p>
    SELECT dep.Name as Department, emp.Name as Employee, emp.Salary 
    from Department dep, Employee emp 
    where emp.DepartmentId=dep.Id 
    and emp.Salary=(Select max(Salary) from Employee e2 where e2.DepartmentId=dep.Id)
</p>


### Sharing my simple solution
- Author: mahdy
- Creation Date: Mon Feb 09 2015 10:59:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 06 2018 14:46:47 GMT+0800 (Singapore Standard Time)

<p>
    Select Department.Name, emp1.Name, emp1.Salary from 
    Employee emp1 join Department on emp1.DepartmentId = Department.Id
    where emp1.Salary = (Select Max(Salary) from Employee emp2 where emp2.DepartmentId = emp1.DepartmentId);
</p>


