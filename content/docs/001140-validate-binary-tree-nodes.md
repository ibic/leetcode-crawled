---
title: "Validate Binary Tree Nodes"
weight: 1140
#id: "validate-binary-tree-nodes"
---
## Description
<div class="description">
<p>You have&nbsp;<code>n</code> binary tree nodes&nbsp;numbered from <code>0</code>&nbsp;to <code>n - 1</code> where node&nbsp;<code>i</code>&nbsp;has two children&nbsp;<code>leftChild[i]</code>&nbsp;and&nbsp;<code>rightChild[i]</code>, return&nbsp;<code>true</code>&nbsp;if and only if <strong>all</strong> the given nodes form <strong>exactly one</strong> valid binary tree.</p>

<p>If node&nbsp;<code>i</code>&nbsp;has no left child then&nbsp;<code>leftChild[i]</code>&nbsp;will equal&nbsp;<code>-1</code>, similarly for the right child.</p>

<p>Note that the nodes have no values and that we only use the node numbers in this problem.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/08/23/1503_ex1.png" style="width: 195px; height: 287px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 4, leftChild = [1,-1,3,-1], rightChild = [2,-1,-1,-1]
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/08/23/1503_ex2.png" style="width: 183px; height: 272px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 4, leftChild = [1,-1,3,-1], rightChild = [2,3,-1,-1]
<strong>Output:</strong> false
</pre>

<p><strong>Example 3:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/08/23/1503_ex3.png" style="width: 82px; height: 174px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 2, leftChild = [1,0], rightChild = [-1,-1]
<strong>Output:</strong> false
</pre>

<p><strong>Example 4:</strong></p>

<p><strong><img alt="" src="https://assets.leetcode.com/uploads/2019/08/23/1503_ex4.png" style="width: 470px; height: 191px;" /></strong></p>

<pre>
<strong>Input:</strong> n = 6, leftChild = [1,-1,-1,4,-1,-1], rightChild = [2,-1,-1,5,-1,-1]
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^4</code></li>
	<li><code>leftChild.length == rightChild.length == n</code></li>
	<li><code>-1 &lt;= leftChild[i], rightChild[i] &lt;= n - 1</code></li>
</ul>

</div>

## Tags
- Graph (graph)

## Companies
- Facebook - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++] Find Root + DFS
- Author: PhoenixDD
- Creation Date: Sun Feb 23 2020 12:00:56 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Feb 27 2020 13:21:07 GMT+0800 (Singapore Standard Time)

<p>
**Observation**

A tree has a single root node (in-degree = 0) and all other nodes have in-degree = 1. We can use this observation to find the root. We can then traverse the tree using that root to check if all nodes have been visited exactly once.

**Solution**

```
class Solution {
public:
   int countNodes(vector<int> &l,vector<int> &r,int root)   // DFS from root to validate that all nodes are visited.
    {
        if(root==-1)
            return 0;
        return 1+countNodes(l,r,l[root])+countNodes(l,r,r[root]);
    }
    bool validateBinaryTreeNodes(int n, vector<int>& leftChild, vector<int>& rightChild) 
    {
        vector<int> inDegree(n,0);
        int root=-1;
        for(int i=0;i<leftChild.size();i++)
            if(leftChild[i]!=-1&&inDegree[leftChild[i]]++==1)  //If in-degree exceeds 1 return false.
                return false;
            else if(rightChild[i]!=-1&&inDegree[rightChild[i]]++==1)  //If in-degree exceeds 1 return false.
                return false;
        for(int i=0;i<leftChild.size();i++)    //Find root and also check for multiple roots.
            if(inDegree[i]==0)  //If in-degree = 0 and has children it\'s a root.
                if(root==-1)   //Store the root.
                    root=i;
                else    //We have multiple roots, return false
                    return false;
        if(root==-1)
            return false;
        return countNodes(leftChild,rightChild,root)==n;
    }
};
```
**Complexity**

Space: `O(n).`
Time: `O(n).`
# Solution asuming nodes are ordered [Wrong, but passed OJ on 2/23/2020]

**Observation**

Imagine traversing a tree in a breadth first search fashion. We notice two things:
1) Each node that we are traversing obviously should have a parent.
2) We only visit a node once.

Let\'s use this observation to formulate our solution.

**Solution**

Traverse the children by their parent nodes(0,1,2,3.....n-1). At each moment check to see if we have visited the node\'s parent. If not, return false as we can\'t reach the children unless we have visited their parent. Also check if the child has already been visited, if yes return false, as we know from our observation that each node is visited only once.
During traversal, mark the nodes as visited.
```c++
class Solution {
public:
    bool validateBinaryTreeNodes(int n, vector<int>& leftChild, vector<int>& rightChild) 
    {
        vector<bool> visited(n,false);
        visited[0]=true;                   	//Mark root as visited.
        for(int i=0;i<leftChild.size();i++)
        {
            if(!visited[i]||leftChild[i]!=-1&&visited[leftChild[i]]||rightChild[i]!=-1&&visited[rightChild[i]])	//If 1. parent isn\'t visited or 2. Child has previously been visited return false.
                return false;
            if(leftChild[i]!=-1)			//Mark left child as visited.
                visited[leftChild[i]]=true;
            if(rightChild[i]!=-1)			//Mark right child as visited.
                visited[rightChild[i]]=true;
        }
        return true;
    }
};
```
**Complexity**
Space: `O(n).`
Time: `O(n).`
</p>


### THE STANDARD CODE IS WRONG
- Author: zerotrac2
- Creation Date: Sun Feb 23 2020 12:30:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Feb 23 2020 12:30:36 GMT+0800 (Singapore Standard Time)

<p>
For the case
```
4
[1, 2, 0, -1]
[-1, -1, -1, -1]
```
The standard code gives the answer `True`, while there is a loop `0->1->2->0` which means the graph is not a tree.
</p>


### Only root = 0? Speechless
- Author: lee215
- Creation Date: Sun Feb 23 2020 12:08:29 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Feb 24 2020 10:08:07 GMT+0800 (Singapore Standard Time)

<p>
Only tests cases `root = 0` ?
No wonder that OJ solution must be wrong.
It cannot even know what the test case are, how could the standard solution be right?

Not surprised at all. The moment I read the problem as Q2 in the contest, I know it will be joke. 
I intend to to write a wrong solution and get accepted in second.
"I bet no bullet in your gun"

Same thing happened last week.
leetcode.com/problems/construct-target-array-with-multiple-sums/discuss/510256/JavaC++Python-Backtrack-OJ-is-wrong/452696

</p>


