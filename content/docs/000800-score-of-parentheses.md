---
title: "Score of Parentheses"
weight: 800
#id: "score-of-parentheses"
---
## Description
<div class="description">
<p>Given a balanced parentheses string <code>S</code>, compute the score of the string based on the following rule:</p>

<ul>
	<li><code>()</code> has score 1</li>
	<li><code>AB</code> has score <code>A + B</code>, where A and B are balanced parentheses strings.</li>
	<li><code>(A)</code> has score <code>2 * A</code>, where A is a balanced parentheses string.</li>
</ul>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">&quot;()&quot;</span>
<strong>Output: </strong><span id="example-output-1">1</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">&quot;(())&quot;</span>
<strong>Output: </strong><span id="example-output-2">2</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">&quot;()()&quot;</span>
<strong>Output: </strong><span id="example-output-3">2</span>
</pre>

<div>
<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-4-1">&quot;(()(()))&quot;</span>
<strong>Output: </strong><span id="example-output-4">6</span>
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>S</code> is a balanced parentheses string, containing only <code>(</code> and <code>)</code>.</li>
	<li><code>2 &lt;= S.length &lt;= 50</code></li>
</ol>
</div>
</div>
</div>
</div>

</div>

## Tags
- String (string)
- Stack (stack)

## Companies
- Amazon - 4 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Divide and Conquer

**Intuition**

Split the string into `S = A + B` where `A` and `B` are balanced parentheses strings, and `A` is the smallest possible non-empty prefix of `S`.

**Algorithm**

Call a balanced string *primitive* if it cannot be partitioned into two non-empty balanced strings.

By keeping track of `balance` (the number of `(` parentheses minus the number of `)` parentheses), we can partition `S` into primitive substrings `S = P_1 + P_2 + ... + P_n`.  Then, `score(S) = score(P_1) + score(P_2) + ... + score(P_n)`, by definition.

For each primitive substring `(S[i], S[i+1], ..., S[k])`, if the string is length 2, then the score of this string is 1.  Otherwise, it's twice the score of the substring `(S[i+1], S[i+2], ..., S[k-1])`.

<iframe src="https://leetcode.com/playground/GKmN6DZd/shared" frameBorder="0" width="100%" height="446" name="GKmN6DZd"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the length of `S`.  An example worst case is `(((((((....)))))))`.

* Space Complexity:  $$O(N)$$, the size of the implied call stack.
<br />
<br />


---
#### Approach 2: Stack

**Intuition and Algorithm**

Every position in the string has a *depth* - some number of matching parentheses surrounding it.  For example, the dot in `(()(.()))` has depth 2, because of these parentheses: `(__(.__))`

Our goal is to maintain the score at the current depth we are on.  When we see an opening bracket, we increase our depth, and our score at the new depth is 0.  When we see a closing bracket, we add twice the score of the previous deeper part - except when counting `()`, which has a score of 1.

For example, when counting `(()(()))`, our stack will look like this:

* `[0, 0]` after parsing `(`
* `[0, 0, 0]` after `(`
* `[0, 1]` after `)`
* `[0, 1, 0]` after `(`
* `[0, 1, 0, 0]` after `(`
* `[0, 1, 1]` after `)`
* `[0, 3]` after `)`
* `[6]` after `)`

<iframe src="https://leetcode.com/playground/UF3khZm2/shared" frameBorder="0" width="100%" height="327" name="UF3khZm2"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `S`.

* Space Complexity:  $$O(N)$$, the size of the stack.
<br />
<br />


---
#### Approach 3: Count Cores

**Intuition**

The final sum will be a sum of powers of 2, as every *core* (a substring `()`, with score 1) will have it's score multiplied by 2 for each exterior set of parentheses that contains that core.

**Algorithm**

Keep track of the `balance` of the string, as defined in *Approach #1*.  For every `)` that immediately follows a `(`, the answer is `1 << balance`, as `balance` is the number of exterior set of parentheses that contains this core.

<iframe src="https://leetcode.com/playground/6RxTcMFD/shared" frameBorder="0" width="100%" height="344" name="6RxTcMFD"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N)$$, where $$N$$ is the length of `S`.

* Space Complexity:  $$O(1)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [C++/Java/Python] O(1) Space
- Author: lee215
- Creation Date: Sun Jun 24 2018 11:05:51 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 26 2019 22:49:31 GMT+0800 (Singapore Standard Time)

<p>
# **Approach 0: Stack**

`cur` record the score at the current layer level.

If we meet `\'(\'`,
we push the current score to `stack`,
enter the next inner layer level,
and reset `cur = 0`.

If we meet `\')\'`,
the `cur` score will be doubled and will be at least 1.
We exit the current layer level,
and set `cur = stack.pop() + cur`

**Complexity:** `O(N)` time and `O(N)` space

**Java**
```java
    public int scoreOfParentheses(String S) {
        Stack<Integer> stack = new Stack<>();
        int cur = 0;
        for (char c : S.toCharArray()) {
            if (c == \'(\') {
                stack.push(cur);
                cur = 0;
            } else {
                cur = stack.pop() + Math.max(cur * 2, 1);
            }
        }
        return cur;
    }
```

**C++**
```cpp
    int scoreOfParentheses(string S) {
        stack<int> stack;
        int cur = 0;
        for (char i : S)
            if (i == \'(\') {
                stack.push(cur);
                cur = 0;
            }
            else {
                cur += stack.top() + max(cur, 1);
                stack.pop();
            }
        return cur;
    }
```

**Python**
```py
    def scoreOfParentheses(self, S):
        stack, cur = [], 0
        for i in S:
            if i == \'(\':
                stack.append(cur)
                cur = 0
            else:
                cur += stack.pop() + max(cur, 1)
        return cur
```
<br>

# **Approach 1: Array**

Same as stack, I do it with an array.
We change a pointer instead of pushing/popping repeatedly.

Complexity: `O(N)` time and `O(N)` space


**Java:**
```java
    public int scoreOfParentheses(String S) {
        int res[] = new int[30], i = 0;
        for (char c : S.toCharArray())
            if (c == \'(\') res[++i] = 0;
            else res[i - 1] += Math.max(res[i--] * 2, 1);
        return res[0];
    }
```
**C++:**
```cpp
    int scoreOfParentheses(string S) {
        int res[30] = {0}, i = 0;
        for (char c : S)
            if (c == \'(\') res[++i] = 0;
            else res[i - 1] += max(res[i] * 2, 1), i--;
        return res[0];
    }
```
**Python:**
```py
    def scoreOfParentheses(self, S):
        res, i = [0] * 30, 0
        for c in S:
            i += 1 if c == \'(\' else -1
            res[i] = res[i] + max(res[i + 1] * 2, 1) if c == \')\' else 0
        return res[0]
```
<br>

# **Follow-Up**
Can you solve it in `O(1)` space?
<br>

# **Approach 2: O(1) Space**

We count the number of layers.
If we meet `\'(\'` layers number `l++`
else we meet `\')\'` layers number `l--`

If we meet `"()"`, we know the number of layer outside,
so we can calculate the score `res += 1 << l`.

**C++:**
```cpp
    int scoreOfParentheses(string S) {
        int res = 0, l = 0;
        for (int i = 0; i < S.length(); ++i) {
            if (S[i] == \'(\') l++; else l--;
            if (S[i] == \')\' && S[i - 1] == \'(\') res += 1 << l;
        }
        return res;
    }
```

**Java:**
```jave
    public int scoreOfParentheses(String S) {
        int res = 0, l = 0;
        for (int i = 0; i < S.length(); ++i) {
            if (S.charAt(i) == \'(\') l++; else l--;
            if (S.charAt(i) == \')\' && S.charAt(i - 1) == \'(\') res += 1 << l;
        }
        return res;
    }
```
**Python:**
```py
    def scoreOfParentheses(self, S):
        res = l = 0
        for a, b in itertools.izip(S, S[1:]):
            if a + b == \'()\': res += 2 ** l
            l += 1 if a == \'(\' else -1
        return res
```
<br>

# More Good Stack Problems
Here are some problems that impressed me.
Good luck and have fun.

[1130. Minimum Cost Tree From Leaf Values](https://leetcode.com/problems/minimum-cost-tree-from-leaf-values/discuss/339959/One-Pass-O(N)-Time-and-Space)
[907. Sum of Subarray Minimums](https://leetcode.com/problems/sum-of-subarray-minimums/discuss/170750/C++JavaPython-Stack-Solution)
[901. Online Stock Span](https://leetcode.com/problems/online-stock-span/discuss/168311/C++JavaPython-O(1))
[856. Score of Parentheses](https://leetcode.com/problems/score-of-parentheses/discuss/141777/C++JavaPython-O(1)-Space)
[503. Next Greater Element II](https://leetcode.com/problems/next-greater-element-ii/discuss/98270/JavaC++Python-Loop-Twice)
496. Next Greater Element I
84. Largest Rectangle in Histogram
42. Trapping Rain Water

</p>


### Java solution using Stack
- Author: wangzi6147
- Creation Date: Sun Jun 24 2018 11:01:43 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 10:05:41 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
    public int scoreOfParentheses(String S) {
        Stack<Integer> stack = new Stack<>();
        for (char c : S.toCharArray()) {
            if (c == \'(\') {
                stack.push(-1);
            } else {
                int cur = 0;
                while (stack.peek() != -1) {
                    cur += stack.pop();
                }
                stack.pop();
                stack.push(cur == 0 ? 1 : cur * 2);
            }
        }
        int sum = 0;
        while (!stack.isEmpty()) {
            sum += stack.pop();
        }
        return sum;
    }
}
```
</p>


### 1-line Python
- Author: lee215
- Creation Date: Sun Jun 24 2018 11:06:22 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 17 2018 05:42:18 GMT+0800 (Singapore Standard Time)

<p>
```
    def scoreOfParentheses(self, S):
        return eval(S.replace(\')(\', \')+(\').replace(\'()\', \'1\').replace(\')\', \')*2\'))
```

</p>


