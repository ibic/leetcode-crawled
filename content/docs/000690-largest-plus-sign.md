---
title: "Largest Plus Sign"
weight: 690
#id: "largest-plus-sign"
---
## Description
<div class="description">
<p>
In a 2D <code>grid</code> from (0, 0) to (N-1, N-1), every cell contains a <code>1</code>, except those cells in the given list <code>mines</code> which are <code>0</code>.  What is the largest axis-aligned plus sign of <code>1</code>s contained in the grid?  Return the order of the plus sign.  If there is none, return 0.
</p><p>
An "<i>axis-aligned plus sign of <code>1</code>s</i> of order <b>k</b>" has some center <code>grid[x][y] = 1</code> along with 4 arms of length <code>k-1</code> going up, down, left, and right, and made of <code>1</code>s.  This is demonstrated in the diagrams below.  Note that there could be <code>0</code>s or <code>1</code>s beyond the arms of the plus sign, only the relevant area of the plus sign is checked for 1s.
</p><p>

<p><b>Examples of Axis-Aligned Plus Signs of Order k:</b><br /><pre>
Order 1:
000
0<b>1</b>0
000

Order 2:
00000
00<b>1</b>00
0<b>111</b>0
00<b>1</b>00
00000

Order 3:
0000000
000<b>1</b>000
000<b>1</b>000
0<b>11111</b>0
000<b>1</b>000
000<b>1</b>000
0000000
</pre></p>

<p><b>Example 1:</b><br /><pre>
<b>Input:</b> N = 5, mines = [[4, 2]]
<b>Output:</b> 2
<b>Explanation:</b>
11111
11111
1<b>1</b>111
<b>111</b>11
1<b>1</b>011
In the above grid, the largest plus sign can only be order 2.  One of them is marked in bold.
</pre></p>

<p><b>Example 2:</b><br /><pre>
<b>Input:</b> N = 2, mines = []
<b>Output:</b> 1
<b>Explanation:</b>
There is no plus sign of order 2, but there is of order 1.
</pre></p>

<p><b>Example 3:</b><br /><pre>
<b>Input:</b> N = 1, mines = [[0, 0]]
<b>Output:</b> 0
<b>Explanation:</b>
There is no plus sign, so return 0.
</pre></p>

<p><b>Note:</b><br><ol>
<li><code>N</code> will be an integer in the range <code>[1, 500]</code>.</li>
<li><code>mines</code> will have length at most <code>5000</code>.</li>
<li><code>mines[i]</code> will be length 2 and consist of integers in the range <code>[0, N-1]</code>.</li>
<li><i>(Additionally, programs submitted in C, C++, or C# will be judged with a slightly smaller time limit.)</i></li>
</ol></p>
</div>

## Tags
- Dynamic Programming (dynamic-programming)

## Companies
- Uber - 2 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Facebook - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Brute Force [Time Limit Exceeded]

**Intuition and Algorithm**

For each possible center, find the largest plus sign that could be placed by repeatedly expanding it.
We expect this algorithm to be $$O(N^3)$$, and so take roughly $$500^3 = (1.25) * 10^8$$ operations.  This is a little bit too big for us to expect it to run in time.

<iframe src="https://leetcode.com/playground/FTxv4pCP/shared" frameBorder="0" width="100%" height="412" name="FTxv4pCP"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^3)$$, as we perform two outer loops ($$O(N^2)$$), plus the inner loop involving `k` is $$O(N)$$.

* Space Complexity: $$O(\text{mines.length})$$.

---

#### Approach #2: Dynamic Programming [Accepted]

**Intuition**

How can we improve our bruteforce?  One way is to try to speed up the inner loop involving `k`, the order of the candidate plus sign.
If we knew the longest possible arm length $$L_u, L_l, L_d, L_r$$ in each direction from a center, we could know the order $$\min(L_u, L_l, L_d, L_r)$$ of a plus sign at that center.  We could find these lengths separately using dynamic programming.

**Algorithm**

For each (cardinal) direction, and for each coordinate `(r, c)` let's compute the `count` of that coordinate: the longest line of `'1'`s starting from `(r, c)` and going in that direction.
With dynamic programming, it is either 0 if `grid[r][c]` is zero, else it is `1` plus the count of the coordinate in the same direction.
For example, if the direction is left and we have a row like `01110110`, the corresponding count values are `01230120`, and the integers are either 1 more than their successor, or 0.
For each square, we want `dp[r][c]` to end up being the minimum of the 4 possible counts.  At the end, we take the maximum value in `dp`.

<iframe src="https://leetcode.com/playground/Jm5C6Q8j/shared" frameBorder="0" width="100%" height="500" name="Jm5C6Q8j"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N^2)$$, as the work we do under two nested for loops is $$O(1)$$.

* Space Complexity: $$O(N^2)$$, the size of `dp`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java/C++/Python O(N^2) solution using only one grid matrix
- Author: fun4LeetCode
- Creation Date: Mon Jan 15 2018 02:23:52 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Nov 11 2019 08:08:26 GMT+0800 (Singapore Standard Time)

<p>
**Algorithms**: For each position `(i, j)` of the `grid` matrix, we try to extend in each of the four directions (left, right, up, down) as long as possible, then take the minimum length of `1`\'s out of the four directions as the order of the largest axis-aligned plus sign centered at position `(i, j)`.

---

**Optimizations**: Normally we would need a total of five matrices to make the above idea work -- one matrix for the `grid` itself and four more matrices for each of the four directions. However, these five matrices can be combined into one using two simple tricks:

1. For each position `(i, j)`, we are only concerned with the minimum length of `1`\'s out of the four directions. This implies we may combine the four matrices into one by only keeping track of the minimum length.

2. For each position `(i, j)`, the order of the largest axis-aligned plus sign centered at it will be `0` if and only if `grid[i][j] == 0`. This implies we may further combine the `grid` matrix with the one obtained above.

---

**Implementations**:

1. Create an `N-by-N` matrix `grid`, with all elements initialized with value `N`.
2. Reset those elements to `0` whose positions are in the `mines` list.
3. For each position `(i, j)`, find the maximum length of `1`\'s in each of the four directions and set `grid[i][j]` to the minimum of these four lengths. Note that there is a simple recurrence relation relating the maximum length of `1`\'s at current position with previous position for each of the four directions (labeled as `l`, `r`, `u`, `d`).
4. Loop through the `grid` matrix and choose the maximum element which will be the largest axis-aligned plus sign of `1`\'s contained in the grid.

---

**Solutions**: Here is a list of solutions for Java/C++/Python based on the above ideas. All solutions run at `O(N^2)` time with `O(N^2)` extra space. Further optimizations are possible such as keeping track of the maximum plus sign currently available and terminating as early as possible if no larger plus sign can be found for current row/column.

**Note**: For those of you who got confused by the logic within the first nested for-loop, refer to [andier\'s comment](https://leetcode.com/problems/largest-plus-sign/discuss/113314/JavaC++Python-O(N2)-solution-using-only-one-grid-matrix/114381) and [my comment](https://leetcode.com/problems/largest-plus-sign/discuss/113314/JavaC++Python-O(N2)-solution-using-only-one-grid-matrix/114382) below for a more clear explanation.

**Java solution:**

```
public int orderOfLargestPlusSign(int N, int[][] mines) {
    int[][] grid = new int[N][N];
        
    for (int i = 0; i < N; i++) {
        Arrays.fill(grid[i], N);
    }
        
    for (int[] m : mines) {
        grid[m[0]][m[1]] = 0;
    }
        
    for (int i = 0; i < N; i++) {
        for (int j = 0, k = N - 1, l = 0, r = 0, u = 0, d = 0; j < N; j++, k--) {
            grid[i][j] = Math.min(grid[i][j], l = (grid[i][j] == 0 ? 0 : l + 1));  // left direction
            grid[i][k] = Math.min(grid[i][k], r = (grid[i][k] == 0 ? 0 : r + 1));  // right direction
            grid[j][i] = Math.min(grid[j][i], u = (grid[j][i] == 0 ? 0 : u + 1));  // up direction
            grid[k][i] = Math.min(grid[k][i], d = (grid[k][i] == 0 ? 0 : d + 1));  // down direction
        }
    }
        
    int res = 0;
        
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            res = Math.max(res, grid[i][j]);
        }
    }
        
    return res;
}
```

<br>

**C++ solution:**

```
int orderOfLargestPlusSign(int N, vector<vector<int>>& mines) {
    vector<vector<int>> grid(N, vector<int>(N, N));
        
    for (auto& m : mines) {
        grid[m[0]][m[1]] = 0;
    }
        
    for (int i = 0; i < N; i++) {
        for (int j = 0, k = N - 1, l = 0, r = 0, u = 0, d = 0; j < N; j++, k--) {
            grid[i][j] = min(grid[i][j], l = (grid[i][j] == 0 ? 0 : l + 1));
            grid[i][k] = min(grid[i][k], r = (grid[i][k] == 0 ? 0 : r + 1));
            grid[j][i] = min(grid[j][i], u = (grid[j][i] == 0 ? 0 : u + 1));
            grid[k][i] = min(grid[k][i], d = (grid[k][i] == 0 ? 0 : d + 1));
        }
    }
        
    int res = 0;
        
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            res = max(res, grid[i][j]);
        }
    }
        
    return res;
}
```

<br>

**Python solution:**

```
def orderOfLargestPlusSign(self, N, mines):
    """
    :type N: int
    :type mines: List[List[int]]
    :rtype: int
    """
    grid = [[N] * N for i in range(N)]
        
    for m in mines:
        grid[m[0]][m[1]] = 0
            
    for i in range(N):
        l, r, u, d = 0, 0, 0, 0
            
        for j, k in zip(range(N), reversed(range(N))):
            l = l + 1 if grid[i][j] != 0 else 0
            if l < grid[i][j]:
                grid[i][j] = l
            
            r = r + 1 if grid[i][k] != 0 else 0
            if r < grid[i][k]:
                grid[i][k] = r

            u = u + 1 if grid[j][i] != 0 else 0
            if u < grid[j][i]:
                grid[j][i] = u
                
            d = d + 1 if grid[k][i] != 0 else 0
            if d < grid[k][i]:
                grid[k][i] = d
        
    res = 0
        
    for i in range(N):
        for j in range(N):
            if res < grid[i][j]:
                res = grid[i][j]
                
    return res
```
</p>


### Python 250ms solution
- Author: pedro
- Creation Date: Mon Jan 15 2018 09:51:05 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 20 2018 03:22:58 GMT+0800 (Singapore Standard Time)

<p>
```
from bisect import bisect_right

class Solution(object):
    def orderOfLargestPlusSign(self, N, mines):
        """
        :type N: int
        :type mines: List[List[int]]
        :rtype: int
        """
        rows = [[-1, N] for _ in xrange(N)]
        cols = [[-1, N] for _ in xrange(N)]
        for r, c in mines:
            rows[r].append(c)
            cols[c].append(r)
        for i in xrange(N):
            rows[i].sort()
            cols[i].sort()
        mxp = 0
        for r in xrange(N):
            for i in xrange(len(rows[r])-1):
                left_b = rows[r][i]
                right_b = rows[r][i+1]
                for c in xrange(left_b+mxp+1, right_b-mxp):
                    idx = bisect_right(cols[c], r)-1
                    up_b = cols[c][idx]
                    down_b = cols[c][idx+1]
                    mxp = max(mxp, min(c-left_b, right_b-c, r-up_b, down_b-r))
        return mxp
     ```
</p>


### java simple DP solution, using one matrix, easy to undersatnd
- Author: YizeLiu
- Creation Date: Sun Jul 08 2018 00:53:34 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 06 2018 23:51:24 GMT+0800 (Singapore Standard Time)

<p>
```
    public int orderOfLargestPlusSign(int N, int[][] mines) {
        int[][]M = new int[N][N];
        for(int[]arr : M){
            Arrays.fill(arr, 1);
        }
        for(int[]arr : mines){
            M[arr[0]][arr[1]] = 0;
        }
        for(int i = 0; i < N; i++){
            int count = 0;   //left
            for (int j = 0; j < N; j++) {
                if (M[i][j] != 0) {
                    count++;
                } else {
                    count = 0;
                }
                M[i][j] = count;
            }
            count = 0; //right
            for(int j = N - 1; j >= 0; j--){
                if (M[i][j] != 0){
                    count++;
                } else {
                    count = 0;
                }
                M[i][j] = Math.min(M[i][j], count);
            }
        }
        int result = 0;
        for(int j = 0; j < N; j++){
            int count = 0;
            for(int i = 0; i < N; i++){ //up
                if(M[i][j] != 0){
                    count++;
                } else {
                    count = 0;
                }
                M[i][j] = Math.min(M[i][j], count);
            }
            count = 0; //down
            for(int i = N-1; i >=0; i--){
                if(M[i][j] != 0){
                    count++;
                } else {
                    count = 0;
                }
                M[i][j] = Math.min(M[i][j], count);
                result = Math.max(result, M[i][j]);
            }
        }
        return result;
    }
```
</p>


