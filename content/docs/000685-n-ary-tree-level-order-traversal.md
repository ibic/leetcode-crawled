---
title: "N-ary Tree Level Order Traversal"
weight: 685
#id: "n-ary-tree-level-order-traversal"
---
## Description
<div class="description">
<p>Given an n-ary tree, return the <i>level order</i> traversal of its nodes&#39; values.</p>

<p><em>Nary-Tree input serialization&nbsp;is represented in their level order traversal, each group of children is separated by the null value (See examples).</em></p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<p><img src="https://assets.leetcode.com/uploads/2018/10/12/narytreeexample.png" style="width: 100%; max-width: 300px;" /></p>

<pre>
<strong>Input:</strong> root = [1,null,3,2,4,null,5,6]
<strong>Output:</strong> [[1],[3,2,4],[5,6]]
</pre>

<p><strong>Example 2:</strong></p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2019/11/08/sample_4_964.png" style="width: 296px; height: 241px;" /></p>

<pre>
<strong>Input:</strong> root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
<strong>Output:</strong> [[1],[2,3,4,5],[6,7,8,9,10],[11,12,13],[14]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The height of the n-ary tree is less than or equal to <code>1000</code></li>
	<li>The total number of nodes is between <code>[0,&nbsp;10^4]</code></li>
</ul>

</div>

## Tags
- Tree (tree)
- Breadth-first Search (breadth-first-search)

## Companies
- Amazon - 3 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Breadth-first Search using a Queue

**Intuition**

We want to make a list of sub-lists, where each sub-list is the values from one *row* in the tree. The rows should be in the order they appear from top to bottom.

Because we're traversing the tree, starting with the nodes nearest to the root and then working our way down to the nodes furthest from the root, this is a type of **breadth-first search**. To do a breadth-first search, we use a **queue**. Recall that a queue is a data structure that we put items in one end, and take them out of the other. We call it First In, First Out (FIFO) because the first items that go in should be the first to come out. It works the same way as the queue you have to wait in to get into a busy stadium.

A **stack** would be the *wrong* data structure to use here. Stacks are used for **depth-first search** (there is a convoluted way you could do it, but it's not *sensible*).

Let's start by using the most basic of queue-based traversal algorithms on the tree to see what it does. This is a fundamental algorithm you should be aiming to *memorize*.

```java
List<Integer> values = new ArrayList<>();
Queue<Node> queue = new LinkedList<>();
queue.add(root);
while (!queue.isEmpty()) {
    Node nextNode = queue.remove();
    values.add(nextNode.val);
    for (Node child : nextNode.children) {
        queue.add(child);
    }
}
```

Make a list to put integers in, and a queue to put nodes on. Put the root node onto the queue, and then while the queue is not empty, take a node off the queue, add its value to the list, and add each of its children onto the queue. Note that we are putting **Node** objects onto the queue, *not* integers. If we were to only put the integer values, we'd have no way of getting the child nodes out of them.

Let's see what we get when we use this algorithm to traverse the tree (don't worry about the inner lists yet, we're ensuring we have a way of getting the nodes from left to right and then top to bottom).

!?!../Documents/429_bfs_intro.json:1000,500!?!

Indeed, it does return the nodes from left to right, then top to bottom. Next we'll be looking at how we can take this basic algorithm and modify it so that we have each level in its own sub-list. Have a go at modifying the above algorithm on your own first.

**Algorithm**

The basic breadth-first search algorithm above got us part of the way, but we still need to do those sub-lists, and also make sure our code works if the root is null (a tree with no data).

We need to create a new sub-list each time we're starting a new layer, and we need to insert all nodes from the layer into that sub-list. A good way we can do this is by checking the current ```size``` of the queue at the start of the ```while``` loop body. Then, we can have another loop that processes that number of nodes. This way, we are guaranteed to be processing *one layer* for *each* iteration of the ```while``` loop so can put all nodes within the same iteration into the same sub-list. On the first iteration of the while loop, we only have **1** node: the root node. So we'll loop around the inner loop once, removing the root node, and put all of its children onto the queue. Then in the second iteration, we'll remove all the children from the queue (as that's the number of times we'll loop around the inner loop) and put all the grandchildren onto the queue. And so forth.

It's very important to use a **Queue** type for ```nodesToExplore```, and *not* a Vector, List, or Array. Removing items off the front of those other data structures is an $$O(n)$$ operation because all the remaining elements are moved along to fill the gap. A Queue is designed so that it is $$O(1)$$.

<iframe src="https://leetcode.com/playground/JBRULb72/shared" frameBorder="0" width="100%" height="429" name="JBRULb72"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$, where $$n$$ is the number of nodes.

    Each node is getting added to the queue, removed from the queue, and added to the result exactly once.

* Space complexity : $$O(n)$$.

    We are using a queue to keep track of nodes we still need to visit the children of. At most, the queue will have 2 layers of the tree on it at any given time. In the worst case, this is all of the nodes. In the best case, it is just 1 node (if we have a tree that is equivalent to a linked list). The average case is difficult to calculate without knowing something of the trees we can expect to see, but in balanced trees, half or more of the nodes are often in the lowest 2 layers. So we should go with the worst case of $$O(n)$$, and know that the average case is probably similar.

<br />

---

#### Approach 2: Simplified Breadth-first Search

**Intuition**

A variant of the above approach is to make a new list on each iteration instead of using a single queue. This makes the code slightly simpler because we lose the ```size``` variable and the counting loop, which are a potential source of off-by-one errors.

**Algorithm**

<iframe src="https://leetcode.com/playground/NV6P7JzS/shared" frameBorder="0" width="100%" height="463" name="NV6P7JzS"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$, where $$n$$ is the number of nodes.

* Space complexity : $$O(n)$$. Same as above, we always have lists containing levels of nodes.

<br />

---

#### Approach 3: Recursion

**Intuition**

We can use recursion for this problem. Often we can't use recursion for a breadth-first search (which is what a level-order traversal is). That is because breadth-first search is based on using a **queue**, whereas recursion is using the runtime **stack** and so is suited to depth-first search. In this case, however, we are putting all the values into a list before returning it. This means it's okay for us to get them in a different order to what they'll appear in the final list. As long as we know what level each node is from, and ensure they are in the correct order within each level, it will work.


**Algorithm**

<iframe src="https://leetcode.com/playground/cqQgxvu2/shared" frameBorder="0" width="100%" height="378" name="cqQgxvu2"></iframe>

The iterative approach traversed the nodes in level order, whereas the recursive approach traversed them from left to right. While it was still easy to put them into the correct order using the recursive approach for this particular question, it could be problematic in practice. Often when we do a level-order traversal (or a breadth-first search), we are using the **Iterator** pattern and instead of storing the values in a list like we did here, the nodes are obtained one-by-one and processed. The iterator approach getting the nodes in the correct order will be much more useful for this use case. This is especially true with huge trees (e.g. links on a web page that you need to crawl and index).

Stack-based *iterative* approaches using a similar strategy to the recursion are *possible* too, however, they are not a good approach because this is supposed to be a breadth-first search problem (which uses queues) and they don't have the elegance of the recursive approach. If you used a stack for this question in an interview, you might leave your interviewer wondering why you felt the need to make the problem more difficult than it needed to be! For this reason, I've chosen not to include them in this article.

**Complexity Analysis**

* Time complexity : $$O(n)$$, where $$n$$ is the number of nodes.

* Space complexity : $$O(\log n)$$ average case and $$O(n) worst case. The space used is on the runtime stack.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python 5 lines BFS solution 
- Author: cenkay
- Creation Date: Fri Jul 13 2018 01:23:04 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 19:10:21 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def levelOrder(self, root):
        q, ret = [root], []
        while any(q):
            ret.append([node.val for node in q])
            q = [child for node in q for child in node.children if child]
        return ret
```
</p>


### Basic C++ iterative solution with detailed explanations. Super easy for beginners.
- Author: JerryWuX
- Creation Date: Sun Aug 12 2018 15:39:59 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 05:18:04 GMT+0800 (Singapore Standard Time)

<p>
Beats 98.27%. It\'s pretty easy to do a further optimization, but as the title suggests, it\'s for beginners.
Before you rush into coding,  it\'s highly recommended to finish the queue & stack explore section, where you can review the basics of BFS traversal, so that you are familiar with all techniques needed! 
- Here\'s the link:
https://leetcode.com/explore/learn/card/queue-stack/231/practical-application-queue/1376/

- Another good source to understand this problem:
https://www.geeksforgeeks.org/level-order-tree-traversal/

This algo can be applied to the following problems with some tweaks. Try them out!

- two slighly modified problems:
https://leetcode.com/problems/binary-tree-level-order-traversal/
https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/
- Calculate the depth of a tree!
https://leetcode.com/problems/maximum-depth-of-binary-tree/

**Note**: It\'s definitely not the best solution, but please take it as the minimum requirement before you come up with any other elegent solutions to solve this problem.

**edited Aug 19 2018 : line-by-line comments for pure beginners.
```
class Solution {
public:
    vector<vector<int>> levelOrder(Node* root) 
	{
        if (root == NULL) 
			return vector<vector<int>>(); // We could also "return {};" here thanks to C++11. 
        vector<vector<int>> res; // Define a vector of vector for storing values of nodes. Data type: int
        queue<Node*> q; // Define the queue. Data type : pointers that point to nodes
        q.push(root); // Push the root node
        while (!q.empty()) // Whenever the queue is not empty
        {
            int size = q.size(); // Store the size of queue, which is the number of nodes in the current level
            vector<int> curLevel; // Store the result per level. 
            for (int i = 0; i < size; i++) // For each node of the current level
            {
                Node* tmp = q.front(); // Get the first node from the queue
                q.pop(); // Pop this node since we no longer need it.
                curLevel.push_back(tmp -> val); // Store values of tmp nodes
                for (auto n : tmp -> children) // Push every child node of the tmp node back to the queue. FIFO(first in first out)
                     q.push(n); 
            }
            res.push_back(curLevel); // Store the current level values to res.
        }
        return res; 
    }
};
```
</p>


### Java Solution
- Author: waiyip
- Creation Date: Thu May 31 2018 10:06:04 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 27 2018 11:23:06 GMT+0800 (Singapore Standard Time)

<p>
```
public List<List<Integer>> levelOrder(Node root) {
        List<List<Integer>> ret = new LinkedList<>();
        
        if (root == null) return ret;
        
        Queue<Node> queue = new LinkedList<>();
        
        queue.offer(root);
        
        while (!queue.isEmpty()) {
            List<Integer> curLevel = new LinkedList<>();
            int len = queue.size();
            for (int i = 0; i < len; i++) {
                Node curr = queue.poll();
                curLevel.add(curr.val);
                for (Node c : curr.children)
                    queue.offer(c);
            }
            ret.add(curLevel);
        }
        
        return ret;
    }
```
</p>


