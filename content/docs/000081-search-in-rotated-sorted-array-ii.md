---
title: "Search in Rotated Sorted Array II"
weight: 81
#id: "search-in-rotated-sorted-array-ii"
---
## Description
<div class="description">
<p>Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.</p>

<p>(i.e., <code>[0,0,1,2,2,5,6]</code> might become <code>[2,5,6,0,0,1,2]</code>).</p>

<p>You are given a target value to search. If found in the array return <code>true</code>, otherwise return <code>false</code>.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [2<code>,5,6,0,0,1,2]</code>, target = 0
<strong>Output:</strong> true
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [2<code>,5,6,0,0,1,2]</code>, target = 3
<strong>Output:</strong> false</pre>

<p><strong>Follow up:</strong></p>

<ul>
	<li>This is a follow up problem to&nbsp;<a href="/problems/search-in-rotated-sorted-array/description/">Search in Rotated Sorted Array</a>, where <code>nums</code> may contain duplicates.</li>
	<li>Would this affect the run-time complexity? How and why?</li>
</ul>

</div>

## Tags
- Array (array)
- Binary Search (binary-search)

## Companies
- Facebook - 2 (taggedByAdmin: false)
- Amazon - 5 (taggedByAdmin: false)
- Google - 5 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Binary Search

This problem is an extension to [33. Search in Rotated Sorted Array](https://leetcode.com/problems/search-in-rotated-sorted-array/). The only difference is that this problem allows duplicate elements.

**Intuition**

Recall that after rotating a sorted array, what we get is two sorted arrays appended to each other.

![rotating a sorted array](../Figures/81/1.png)

Let's refer to the first sorted array as `F` and second as `S`.

![rotating a sorted array](../Figures/81/2.png)

Also, we can observe that all the elements of the second array `S` will be smaller or equal to the first element `start` of `F`.

![rotating a sorted array](../Figures/81/3.png)

With this observation in mind, we can easily tell which of the 2 arrays (`F` or `S`) does a `target` element lie in by just comparing it with the first element of the array. 

Let's say we are looking for element `target` in array `arr`:

- Case 1: If `target > arr[start]`: `target` exists in the first array `F`.

![rotating a sorted array](../Figures/81/4.png)

- Case 2: If `target < arr[start]`: `target` exists in the second array `S`.
 
![rotating a sorted array](../Figures/81/5.png)
	
- Case 3: If `target == arr[start]`: `target` obviously exists in the first array `F`, but it might also be present in the 	second array `S`. 
	
![rotating a sorted array](../Figures/81/6.png)

Let's define a helper function that tells us which array a target element might be present in:

<iframe src="https://leetcode.com/playground/Fwigvfe9/shared" frameBorder="0" width="100%" height="123" name="Fwigvfe9"></iframe>	

**Algorithm**

Recall that in standard binary search, we keep two pointers (i.e. `start` and `end`) to track the search scope in an `arr` array. We then divide the search space in three parts `[start, mid)`, `[mid, mid]`, `(mid, end]`. Now, we continue to look for our `target` element in one of these search spaces.

By identifying the positions of both `arr[mid]` and `target` in `F` and `S`, we can reduce search space in the very same way as in standard binary search:

- Case 1: `arr[mid]` lies in `F`, `target` lies in `S`: Since `S` starts after `F` ends, we know that element lies here:`(mid, end]`.
	
![rotating a sorted array](../Figures/81/7.png)
	
- Case 2: `arr[mid]` lies in the `S`, `target` lies in `F`: Similarly, we know that element lies here: `[start, mid)`.

![rotating a sorted array](../Figures/81/8.png)
	
- Case 3: Both `arr[mid]` and `target` lie in `F`: since both of them are in same sorted array, we can compare `arr[mid]` and `target` in order to decide how to reduce search space.

![rotating a sorted array](../Figures/81/9.png)

- Case 4: Both `arr[mid]` and `target` lie in `S`: Again, since both of them are in same sorted array, we can compare `arr[mid]` and `target` in order to decide how to reduce search space.

![rotating a sorted array](../Figures/81/10.png)

But there is a catch, if `arr[mid]` equals `arr[start]`, then we know that `arr[mid]` might belong to both `F` and `S` and hence we cannot find the relative position of `target` from it.

![rotating a sorted array](../Figures/81/11.png)

<iframe src="https://leetcode.com/playground/cRffu89G/shared" frameBorder="0" width="100%" height="123" name="cRffu89G"></iframe>

In this case, we have no option but to move to next search space iteratively. Hence, there are certain search spaces that allow a binary search, and some search spaces that don't.

<iframe src="https://leetcode.com/playground/FPVLJfQZ/shared" frameBorder="0" width="100%" height="500" name="FPVLJfQZ"></iframe>

**Complexity Analysis**

* Time complexity : $$O(N)$$ worst case, $$O(\log N)$$ best case, where $$N$$ is the length of the input array.

	Worst case: This happens when all the elements are the same and we search for some different element. At each step, we will only be able to reduce the search space by 1 since `arr[mid]` equals `arr[start]` and it's not possible to decide the relative position of `target` from `arr[mid]`.
	Example: [1, 1, 1, 1, 1, 1, 1], target = 2.
	
	Best case: This happens when all the elements are distinct. At each step, we will be able to divide our search space into half just like a normal binary search.
	
This also answers the following follow-up question:

1) Would this (having duplicate elements) affect the run-time complexity? How and why?

As we can see, by having duplicate elements in the array, we often miss the opportunity to apply binary search in certain search spaces. Hence, we get $$O(N)$$ worst case (with duplicates) vs $$O(\log N)$$ best case complexity (without duplicates).

* Space complexity : $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### My 8ms C++ solution (o(logn) on average, o(n) worst case)
- Author: LeJas
- Creation Date: Sun Jun 21 2015 01:36:41 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 03:24:20 GMT+0800 (Singapore Standard Time)

<p>
The idea is the same as the previous one without duplicates

    1) everytime check if targe == nums[mid], if so, we find it.
    2) otherwise, we check if the first half is in order (i.e. nums[left]<=nums[mid]) 
      and if so, go to step 3), otherwise, the second half is in order,   go to step 4)
    3) check if target in the range of [left, mid-1] (i.e. nums[left]<=target < nums[mid]), if so, do search in the first half, i.e. right = mid-1; otherwise, search in the second half left = mid+1;
    4)  check if target in the range of [mid+1, right] (i.e. nums[mid]<target <= nums[right]), if so, do search in the second half, i.e. left = mid+1; otherwise search in the first half right = mid-1;

The only difference is that due to the existence of duplicates, we can have nums[left] == nums[mid] and in that case, the first half could be out of order (i.e. NOT in the ascending order, e.g. [3 1 2 3 3 3 3]) and we have to deal this case separately. In that case, it is guaranteed that nums[right] also equals to nums[mid], so what we can do is to check if nums[mid]== nums[left] == nums[right] before the original logic, and if so, we can move left and right both towards the middle by 1. and repeat.  

    class Solution {
    public:
        bool search(vector<int>& nums, int target) {
            int left = 0, right =  nums.size()-1, mid;
            
            while(left<=right)
            {
                mid = (left + right) >> 1;
                if(nums[mid] == target) return true;
    
                // the only difference from the first one, trickly case, just updat left and right
                if( (nums[left] == nums[mid]) && (nums[right] == nums[mid]) ) {++left; --right;}
    
                else if(nums[left] <= nums[mid])
                {
                    if( (nums[left]<=target) && (nums[mid] > target) ) right = mid-1;
                    else left = mid + 1; 
                }
                else
                {
                    if((nums[mid] < target) &&  (nums[right] >= target) ) left = mid+1;
                    else right = mid-1;
                }
            }
            return false;
        }
    };
</p>


### Python easy to understand solution (with comments).
- Author: OldCodingFarmer
- Creation Date: Tue Aug 04 2015 17:23:39 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 11 2018 22:45:22 GMT+0800 (Singapore Standard Time)

<p>
        
    def search(self, nums, target):
        l, r = 0, len(nums)-1
        while l <= r:
            mid = l + (r-l)//2
            if nums[mid] == target:
                return True
            while l < mid and nums[l] == nums[mid]: # tricky part
                l += 1
            # the first half is ordered
            if nums[l] <= nums[mid]:
                # target is in the first half
                if nums[l] <= target < nums[mid]:
                    r = mid - 1
                else:
                    l = mid + 1
            # the second half is ordered
            else:
                # target is in the second half
                if nums[mid] < target <= nums[r]:
                    l = mid + 1
                else:
                    r = mid - 1
        return False
</p>


### Neat JAVA solution using binary search
- Author: Meowgadeth
- Creation Date: Sun Sep 27 2015 04:56:15 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 09:42:11 GMT+0800 (Singapore Standard Time)

<p>
        public boolean search(int[] nums, int target) {
            int start = 0, end = nums.length - 1, mid = -1;
            while(start <= end) {
                mid = (start + end) / 2;
                if (nums[mid] == target) {
                    return true;
                }
                //If we know for sure right side is sorted or left side is unsorted
                if (nums[mid] < nums[end] || nums[mid] < nums[start]) {
                    if (target > nums[mid] && target <= nums[end]) {
                        start = mid + 1;
                    } else {
                        end = mid - 1;
                    }
                //If we know for sure left side is sorted or right side is unsorted
                } else if (nums[mid] > nums[start] || nums[mid] > nums[end]) {
                    if (target < nums[mid] && target >= nums[start]) {
                        end = mid - 1;
                    } else {
                        start = mid + 1;
                    }
                //If we get here, that means nums[start] == nums[mid] == nums[end], then shifting out
                //any of the two sides won't change the result but can help remove duplicate from
                //consideration, here we just use end-- but left++ works too
                } else {
                    end--;
                }
            }
            
            return false;
        }

In case anyone wonders, yes I agree that we don't need to check two parts. It's just that Doing that can slightly boost the performance, no asymptotic difference though.
</p>


