---
title: "Find Pivot Index"
weight: 645
#id: "find-pivot-index"
---
## Description
<div class="description">
<p>Given an array of integers <code>nums</code>, write a method that returns the &quot;pivot&quot; index of this array.</p>

<p>We define the pivot index as the index where the sum of all the numbers to the left of the index is equal to the sum of all the numbers to the right of the index.</p>

<p>If no such index exists, we should return -1. If there are multiple pivot indexes, you should return the left-most pivot index.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,7,3,6,5,6]
<strong>Output:</strong> 3
<strong>Explanation:</strong>
The sum of the numbers to the left of index 3 (nums[3] = 6) is equal to the sum of numbers to the right of index 3.
Also, 3 is the first index where this occurs.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> nums = [1,2,3]
<strong>Output:</strong> -1
<strong>Explanation:</strong>
There is no index that satisfies the conditions in the problem statement.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The length of <code>nums</code> will be in the range <code>[0, 10000]</code>.</li>
	<li>Each element <code>nums[i]</code> will be an integer in the range <code>[-1000, 1000]</code>.</li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Goldman Sachs - 9 (taggedByAdmin: false)
- Facebook - 5 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Citadel - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- Salesforce - 3 (taggedByAdmin: false)
- Expedia - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- GoDaddy - 3 (taggedByAdmin: false)
- Tesla - 3 (taggedByAdmin: false)
- Indeed - 2 (taggedByAdmin: false)
- Radius - 0 (taggedByAdmin: true)
- Coupang - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

#### Approach #1: Prefix Sum [Accepted]

**Intuition and Algorithm**

We need to quickly compute the sum of values to the left and the right of every index.

Let's say we knew `S` as the sum of the numbers, and we are at index `i`.  If we knew the sum of numbers `leftsum` that are to the left of index `i`, then the other sum to the right of the index would just be `S - nums[i] - leftsum`.  

As such, we only need to know about `leftsum` to check whether an index is a pivot index in constant time.  Let's do that: as we iterate through candidate indexes `i`, we will maintain the correct value of `leftsum`.

<iframe src="https://leetcode.com/playground/4TwwA59e/shared" frameBorder="0" width="100%" height="242" name="4TwwA59e"></iframe>

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the length of `nums`.

* Space Complexity: $$O(1)$$, the space used by `leftsum` and `S`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### This is a very poorly described problem
- Author: pandora111
- Creation Date: Tue Jun 05 2018 05:09:48 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 03:40:48 GMT+0800 (Singapore Standard Time)

<p>
I ran into a test case where given input:
[-1,-1,-1,0,1,1] 
the expected output is: 0.

How can the pivot be index 0 where there is no numbers to the left of index 0? Since when do you define the sum of non-existent numbers to be zero??
</p>


### Short Python O(n) time O(1) space with Explanation
- Author: yangshun
- Creation Date: Sun Nov 12 2017 13:22:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 01:50:29 GMT+0800 (Singapore Standard Time)

<p>
As we iterate through the array of numbers, we need to keep track of the sum of the values on the current number's left and its right. The following debugger trace demonstrates the values of the variables in each loop before the `left == right` line

Input: `[1, 7, 3, 6, 5, 6]`

1. `index`: 0, `num`: 1, `left`: 0, `right`: 27
1. `index`: 1, `num`: 7, `left`: 1, `right`: 20
1. `index`: 2, `num`: 3, `left`: 8, `right`: 17
1. `index`: 3, `num`: 6, `left`: 11, `right`: 11 <-- Found!!!

*- Yangshun*

```
class Solution(object):
    def pivotIndex(self, nums):
        # Time: O(n)
        # Space: O(1)
        left, right = 0, sum(nums)
        for index, num in enumerate(nums):
            right -= num
            if left == right:
                return index
            left += num
        return -1
```
</p>


### [Java/C++] Clean Code
- Author: alexander
- Creation Date: Sun Nov 12 2017 12:25:49 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 23:34:10 GMT+0800 (Singapore Standard Time)

<p>
**Java**
```
class Solution {
    public int pivotIndex(int[] nums) {
        int total = 0, sum = 0
        for (int num : nums) total += num;
        for (int i = 0; i < nums.length; sum += nums[i++])
            if (sum * 2 == total - nums[i]) return i;
        return -1;  
    }
}
```
**C++**
```
class Solution {
public:
    int pivotIndex(vector<int>& nums) {
        int total = 0;
        for (int num : nums) total += num;
        int sum = 0;
        for (int i = 0; i < nums.size(); sum += nums[i++])
            if (sum * 2 == total - nums[i])
                return i;
        
        return -1;
    }
};
```
</p>


