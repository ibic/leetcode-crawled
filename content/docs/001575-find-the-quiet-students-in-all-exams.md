---
title: "Find the Quiet Students in All Exams"
weight: 1575
#id: "find-the-quiet-students-in-all-exams"
---
## Description
<div class="description">
<p>Table: <code>Student</code></p>

<pre>
+---------------------+---------+
| Column Name         | Type    |
+---------------------+---------+
| student_id          | int     |
| student_name        | varchar |
+---------------------+---------+
student_id is the primary key for this table.
student_name is the name of the student.</pre>

<p>&nbsp;</p>

<p>Table: <code>Exam</code></p>

<pre>
+---------------+---------+
| Column Name   | Type    |
+---------------+---------+
| exam_id       | int     |
| student_id    | int     |
| score         | int     |
+---------------+---------+
(exam_id, student_id) is the primary key for this table.
Student with student_id got score points in exam with id exam_id.
</pre>

<p>&nbsp;</p>

<p>A &quot;quite&quot; student is the one who took at least one exam and didn&#39;t score neither the high score nor the low score.</p>

<p>Write an SQL query to report the students (student_id, student_name) being &quot;quiet&quot; in <strong>ALL</strong> exams.</p>

<p>Don&#39;t return the student who has never taken any exam. Return the result table <strong>ordered</strong> by student_id.</p>

<p>The query result format is in the following example.</p>

<p>&nbsp;</p>

<pre>
Student table:
+-------------+---------------+
| student_id  | student_name  |
+-------------+---------------+
| 1           | Daniel        |
| 2           | Jade          |
| 3           | Stella        |
| 4           | Jonathan      |
| 5           | Will          |
+-------------+---------------+

Exam table:
+------------+--------------+-----------+
| exam_id    | student_id   | score     |
+------------+--------------+-----------+
| 10         |     1        |    70     |
| 10         |     2        |    80     |
| 10         |     3        |    90     |
| 20         |     1        |    80     |
| 30         |     1        |    70     |
| 30         |     3        |    80     |
| 30         |     4        |    90     |
| 40         |     1        |    60     |
| 40         |     2        |    70     |
| 40         |     4        |    80     |
+------------+--------------+-----------+

Result table:
+-------------+---------------+
| student_id  | student_name  |
+-------------+---------------+
| 2           | Jade          |
+-------------+---------------+

For exam 1: Student 1 and 3 hold the lowest and high score respectively.
For exam 2: Student 1 hold both highest and lowest score.
For exam 3 and 4: Studnet 1 and 4 hold the lowest and high score respectively.
Student 2 and 5 have never got the highest or lowest in any of the exam.
Since student 5 is not taking any exam, he is excluded from the result.
So, we only return the information of Student 2.</pre>

</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 10 lines, very simple solution
- Author: poppyfff
- Creation Date: Sun Apr 19 2020 03:51:22 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Apr 19 2020 03:51:22 GMT+0800 (Singapore Standard Time)

<p>
```
WITH cte AS(
    SELECT exam_id, exam.student_id, student_name, score, RANK() OVER(PARTITION BY exam_id ORDER BY score) rk1, RANK() OVER(PARTITION BY exam_id ORDER BY score DESC) rk2 
    FROM exam LEFT JOIN student
    ON exam.student_id = student.student_id
)

SELECT DISTINCT student_id, student_name
FROM cte
WHERE student_id NOT IN (SELECT student_id FROM cte WHERE rk1 = 1 or rk2 = 1)
ORDER BY student_id
```
</p>


### Easy to Understand Solution with Comments
- Author: chinhau
- Creation Date: Tue Jul 21 2020 01:46:57 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jul 21 2020 01:46:57 GMT+0800 (Singapore Standard Time)

<p>
Please upvote this solution if you find it useful!

-- With CTE : Rank - high score, low score - partition by exam_id order by score

	WITH cte AS 
	(
		SELECT exam_id, student_id, 
		RANK() OVER(partition by exam_id order by score DESC) AS "high_score",
		RANK() OVER(partition by exam_id order by score) AS "low_score"
		FROM Exam 
	)

	SELECT DISTINCT e.student_id, s.student_name
	FROM Exam e LEFT JOIN Student s ON s.student_id = e.student_id
	WHERE e.student_id NOT IN (SELECT student_id FROM cte WHERE high_score = 1 OR low_score = 1)
</p>


### 227 ms, faster than 100.00% of MySQL
- Author: yehong
- Creation Date: Fri Apr 17 2020 07:31:38 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Apr 17 2020 07:31:38 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT s.student_id, s.student_name
FROM Student s
JOIN (
    SELECT a.student_id, COUNT(a.exam_id) AS total_exam,
        SUM(
            CASE
                WHEN a.score > min_score AND a.score < max_score THEN 1
                ELSE 0
            END
        ) AS quite_exam
    FROM Exam a
    JOIN (
        SELECT exam_id, MIN(score) AS min_score, MAX(score) AS max_score
        FROM Exam
        GROUP BY exam_id
    ) b
    ON a.exam_id = b.exam_id
    GROUP BY a.student_id
) c
ON s.student_id = c.student_id
WHERE c.total_exam = c.quite_exam
```
</p>


