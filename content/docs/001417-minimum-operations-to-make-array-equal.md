---
title: "Minimum Operations to Make Array Equal"
weight: 1417
#id: "minimum-operations-to-make-array-equal"
---
## Description
<div class="description">
<p>You have an array <code>arr</code> of length <code>n</code> where <code>arr[i] = (2 * i) + 1</code> for all valid values of <code>i</code> (i.e. <code>0 &lt;= i &lt; n</code>).</p>

<p>In one operation, you can select two indices <code>x</code>&nbsp;and <code>y</code> where <code>0 &lt;= x, y &lt; n</code> and subtract <code>1</code> from <code>arr[x]</code> and add <code>1</code> to <code>arr[y]</code>&nbsp;(i.e. perform <code>arr[x] -=1&nbsp;</code>and <code>arr[y] += 1</code>).&nbsp;The goal is to make all the elements of the array <strong>equal</strong>. It is <strong>guaranteed</strong> that all the elements of the array can be made equal using some operations.</p>

<p>Given an integer <code>n</code>, the length of the array. Return <em>the minimum number of operations</em> needed to make&nbsp;all the elements of arr equal.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 3
<strong>Output:</strong> 2
<strong>Explanation:</strong> arr = [1, 3, 5]
First operation choose x = 2 and y = 0, this leads arr to be [2, 3, 4]
In the second operation choose x = 2 and y = 0 again, thus arr = [3, 3, 3].
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 6
<strong>Output:</strong> 9
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 10^4</code></li>
</ul>
</div>

## Tags
- Math (math)

## Companies
- Brillio - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java 2 Lines
- Author: hobiter
- Creation Date: Sun Aug 16 2020 12:05:17 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Aug 19 2020 14:34:52 GMT+0800 (Singapore Standard Time)

<p>
if odd length:
need 2, 4 .. , 2 * ( n / 2) ops, that is n / 2 * (2 +  2 * (n / 2)) / 2 = n / 2 * (n / 2 + 1) ;

if even length:
need 1, 3 .. , 2 * ( n / 2)  - 1 ops, that is n / 2 * (1 +  2 * (n / 2) - 1) / 2 = n / 2 * (n / 2) ;
```
    public int minOperations(int n) {
        int cnt = n / 2;
        return cnt * (cnt + n % 2);
    }
```
</p>


### [JAVA] O(1) Constant time solution with detailed explanation.
- Author: pramitb
- Creation Date: Sun Aug 16 2020 12:16:07 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 16 2020 13:45:00 GMT+0800 (Singapore Standard Time)

<p>
Basic Approach:
            if n is odd, suppose n=5.
                The array is :
                1 3 5 7 9.
                Here, we will have the middle element as 5. 
                We take 2 from 7 and add to 3 to make each one 5.
                We take 4 from 9 and add to 1 to make each one 5.
                Total steps: 2+4=6. (sum of first n/2 even numbers)  
				Sum of first k EVEN numbers = 
				Sum(i=1 to k) {2 * i} = 2 * ( Sum(i=1 to k){ i } )
												= 2 * (k*(k+1))/2
												= k*(k+1)
           if n is even, suppose n=6.
                The array is :
                1 3 5 7 9 11.
                Here, we will have the middle element as (5+7)/2=6.
                We take 1 from 7 and add to 5 to make each one 6.
                We take 3 from 9 and add to 3 to make each one 6.
                We take 5 from 11 and add to 1 to make each one 6.
                Total steps: 1+3+5=9. (sum of first n/2 odd numbers) 
				Sum of first k ODD numbers = k * k.
```
class Solution {
    public int minOperations(int n) {
	// Take care of overflow if n is too large.
        if(n%2==1){
            n/=2;
            return (n*(n+1));
        }        
        n/=2;
        return n*n;
    }
}
```
Thanks for reading. Please share and upvote.
</p>


### Self Explanatory Code | Explained with Steps
- Author: ngytlcdsalcp1
- Creation Date: Sun Aug 16 2020 12:05:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 16 2020 12:09:14 GMT+0800 (Singapore Standard Time)

<p>
arr[i] =  `2*i + 1` ==> which will generate sequence of first odd numbers 
let n = 5 => [1, 3, 5, 7, 9]
let n = 6 => [1, 3, 5, 7, 9, 11]

optimal way to make all equal is to make all numbers equals to n.

we will club two numbers together like 1, 9 take them to 5, 3, 7 take them to 5  =>  4 + 2 = 6

```
class Solution {
    public int minOperations(int n) {
        int op = 0;
        int odd = 1;
        while(odd < n) {
            op += n - odd;
            odd += 2;
        }
        return op;
    }
}
```
</p>


