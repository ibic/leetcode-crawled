---
title: "Remove Covered Intervals"
weight: 1102
#id: "remove-covered-intervals"
---
## Description
<div class="description">
<p>Given a list of <code>intervals</code>, remove all intervals that are covered by another interval in the list.</p>

<p>Interval <code>[a,b)</code> is covered by&nbsp;interval <code>[c,d)</code> if and only if <code>c &lt;= a</code> and <code>b &lt;= d</code>.</p>

<p>After doing so, return <em>the number of remaining intervals</em>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[1,4],[3,6],[2,8]]
<strong>Output:</strong> 2
<b>Explanation: </b>Interval [3,6] is covered by [2,8], therefore it is removed.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[1,4],[2,3]]
<strong>Output:</strong> 1
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[0,10],[5,12]]
<strong>Output:</strong> 2
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[3,10],[4,10],[5,11]]
<strong>Output:</strong> 2
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input:</strong> intervals = [[1,2],[1,4],[3,4]]
<strong>Output:</strong> 1
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= intervals.length &lt;= 1000</code></li>
	<li><code>intervals[i].length == 2</code></li>
	<li><code>0 &lt;= intervals[i][0] &lt;&nbsp;intervals[i][1] &lt;= 10^5</code></li>
	<li>All the intervals are <strong>unique</strong>.</li>
</ul>
</div>

## Tags
- Greedy (greedy)
- Sort (sort)
- Line Sweep (line-sweep)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Greedy Algorithm

**Solution Pattern**

> The idea of greedy algorithm is to pick the _locally_
optimal move at each step, which would lead to the _globally_ optimal solution.

Typical greedy solution has $$\mathcal{O}(N \log N)$$ time complexity 
and consists of two steps:

- Figure out how to sort the input data.
That would take $$\mathcal{O}(N \log N)$$ time, and
could be done directly by sorting or indirectly by using heap data structure. 
Usually sorting is better than heap usage because of gain in space.

- Parse the sorted input in $$\mathcal{O}(N)$$ time to construct a solution. 

In the case of already sorted input,
the greedy solution could have $$\mathcal{O}(N)$$ time complexity,
[here is an example](https://leetcode.com/articles/gas-station/).

**Intuition**

Let us figure out how to sort the input. 
The idea to sort by start point is pretty obvious, because it simplifies
further parsing:

![traversal](../Figures/1288/sort.png)

Let us consider two subsequent intervals after sorting. Since 
sorting ensures that `start1 < start2`, it's sufficient to compare the end boundaries:   

- If `end1 < end2`, the intervals won't completely cover one another, though they have some overlapping.

![traversal](../Figures/1288/dont_cover2.png) 

- If `end1 >= end2`, the interval 2 is covered by the interval 1.

![traversal](../Figures/1288/cover.png) 

**Edge case: How to treat intervals which share start point**

> We've missed an important edge case in the previous discussion:
what if two intervals share the start point, _i.e._ `start1 == start2`? 

The above algorithm will fail because it cannot distinguish these two situations as follows: 

![traversal](../Figures/1288/share.png) 

One of the intervals is covered by another, but if we sort only by the start point, 
we would not know which one. Hence, we need to sort by the end point as well.

> If two intervals share the same start point,
one has to put the longer interval in front.

This way the above algorithm would work fine here as well. 
Moreover, it can deal with more complex cases, like the one below:

![traversal](../Figures/1288/complex.png)

**Algorithm**

- Sort in the ascending order by the start point. If two intervals share 
the same start point, put the longer one to be the first.

- Initiate the number of non-covered intervals: `count = 0`.

- Iterate over sorted intervals and compare end points. 
    
    - If the current interval is not covered by the previous one 
    `end > prev_end`, increase the number of non-covered intervals.
    Assign the current interval to be previous for the next step.
    
    - Otherwise, the current interval is covered by the previous one.
    Do nothing.

- Return `count`.

**Implementation**

!?!../Documents/1288_LIS.json:1000,302!?!

<iframe src="https://leetcode.com/playground/iEDhw7a3/shared" frameBorder="0" width="100%" height="497" name="iEDhw7a3"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(N \log N)$$ since the sorting dominates the complexity of the algorithm.
 
* Space complexity : $$\mathcal{O}(N)$$ or $$\mathcal{O}(\log{N})$$

  - The space complexity of the sorting algorithm depends on the implementation of each program language.

  - For instance, the `sorted()` function in Python is implemented with the [Timsort](https://en.wikipedia.org/wiki/Timsort) algorithm whose space complexity is $$\mathcal{O}(N)$$.

  - In Java, the [Arrays.sort()](https://docs.oracle.com/javase/8/docs/api/java/util/Arrays.html#sort-byte:A-) is implemented as a variant of quicksort algorithm whose space complexity is $$\mathcal{O}(\log{N})$$.


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Sort Solution
- Author: lee215
- Creation Date: Sun Dec 15 2019 00:03:16 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 01 2020 18:43:25 GMT+0800 (Singapore Standard Time)

<p>
# Intuition
Imagine that, after removing all covered intervals,
all intervals must have different bounds,
After sorting, their left and right bound are increasing at the same time.
<br>

# Test Case
Here are some useful small test cases for debugging.
`[[1,2],[1,3]]`
`[[1,3],[1,8],[5,8]]`
`[[1,6],[4,6],[4,8]]`
<br>

# Solution 1, sort
Sort the array, and note the previous `left` and `right` bound.
For evert interval `v`,
if `v[0] > left && v[1] > right`,
It\'s a new uncovered interval,
so we increment `++res`.

Complexity: time `O(NlogN)`, space `O(1)`
<br>

**Java:**
```java
    public int removeCoveredIntervals(int[][] A) {
        int res = 0, left = -1, right = -1;
        Arrays.sort(A, (a, b) -> a[0] - b[0]);
        for (int[] v : A) {
            if (v[0] > left && v[1] > right) {
                left = v[0];
                ++res;
            }
            right = Math.max(right, v[1]);
        }
        return res;
    }
```

**C++**
```cpp
    int removeCoveredIntervals(vector<vector<int>>& A) {
        int res = 0, left = -1, right = -1;
        sort(A.begin(), A.end());
        for (auto& v: A) {
            if (v[0] > left && v[1] > right) {
                left = v[0];
                ++res;
            }
            right = max(right, v[1]);
        }
        return res;
    }
```


# Solution 2, sort left ascending and right decending
In this solution, we sort on left first.
When left are same, we sort right in decending order.

For example: [[1,5],[1,4],[1,3],[1,2]]

Complexity: time `O(NlogN)`, space `O(1)`
<br>

**Java**
```java
    public int removeCoveredIntervals2(int[][] A) {
        int res = 0, right = 0;
        Arrays.sort(A, (a, b) -> a[0] != b[0] ? a[0] - b[0] : b[1] - a[1]);
        for (int[] v : A) {
            if (v[1] > right) {
                ++res;
                right = v[1];
            }
        }
        return res;
    }
```
**Python:**
```python
    def removeCoveredIntervals(self, A):
        res = right = 0
        A.sort(key=lambda a: (a[0], -a[1]))
        for i, j in A:
            res += j > right
            right = max(right, j)
        return res
```

</p>


### [Java/Python 3] Simple codes w/ explanation and analysis.
- Author: rock
- Creation Date: Sun Dec 15 2019 00:04:01 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Dec 16 2019 21:17:07 GMT+0800 (Singapore Standard Time)

<p>
**Sort intervals in such an order that only previous ones are possible to cover current one.**
1. Sort by the left bound, and when left bounds are equal, sort right bounds by reverse order; Therefore, **no interval can cover previous ones**;
2. Loop through the `intervals`, whenever current right most bound <  next interval\'s right bound, it means current interval can NOT cover next interval, update right most bound and increase counter by 1.
 
```java
    public int removeCoveredIntervals(int[][] intervals) {
        Arrays.sort(intervals, (i, j) -> (i[0] == j[0] ? j[1] - i[1] : i[0] - j[0]));
        int count = 0, cur = 0;
        for (int[] a : intervals) {
            if (cur < a[1]) {
                cur = a[1];
                ++count;
            }
        }
        return count;        
    }
```
```python
    def removeCoveredIntervals(self, intervals: List[List[int]]) -> int:
        count = cur = 0
        for _, r in sorted(intervals, key=lambda i: (i[0], -i[1])):
            if cur < r:
                cur = r
                count += 1
        return count
```

**Analysis:**

Time: `O(nlogn)`, space: `O(1)` - excluding sorting space, where `n` = `intervals.length`.
</p>


### 2 Solutions | Easy to Understand | Sort and Count overlap intervals
- Author: poiuytrewq1as
- Creation Date: Sun Oct 04 2020 15:54:44 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 04 2020 17:06:28 GMT+0800 (Singapore Standard Time)

<p>
This problem is very similar to other intervals problem -
`Merge Intervals`, `Insert Intervals` and `Non-Overlapping Intervals`

**Steps:**
1. Sort the intervals based on start of intervals.
2. Create a currInt[] = {-1, -1}
3. Scan all the intervals and compare it with currInt - 3 cases
	* interval lies in currInt jsut update overlap++;
	* currInt lies in interval update currInt with interval and overlap++;
	* not fully overlap just update currInt with interval

4. return total interval - overlap ones

**Detailed Video Explanation**  https://youtu.be/vF8C-MaT2bI?t=0



**Count Overlapping Intervals**

```
class Solution {
    public int removeCoveredIntervals(int[][] intervals) {
        Arrays.sort(intervals, (a, b) -> a[0] - b[0]);
        int[] currInt = {-1, -1};
        int overlap = 0;
        for(int[] interval: intervals) {
            if(currInt[0] <= interval[0] && currInt[1] >= interval[1]) {
                overlap++;
            } else {
                if(currInt[0] >= interval[0] && currInt[1] <= interval[1]) 
                    overlap++;
                currInt = interval;
            }
        }
        
        return intervals.length - overlap;
    }
}
```

************

**Count Non-Overlapping Ones**

```
class Solution {
    public int removeCoveredIntervals(int[][] intervals) {
        Arrays.sort(intervals, (a, b) -> a[0] - b[0]);
        int[] currInt = {-1, -1};
        int result = 0;
        for(int[] interval: intervals) {
            if(currInt[0] < interval[0] && currInt[1] < interval[1]) {
                currInt[0] = interval[0];
                result++;
            }
            currInt[1] = Math.max(currInt[1], interval[1]);
        }
        
        return result;
    }
}
```

`TC - O(nlogn)`
`SC - O(1)`


if you like solution **upvote**.
</p>


