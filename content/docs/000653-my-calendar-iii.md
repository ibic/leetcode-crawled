---
title: "My Calendar III"
weight: 653
#id: "my-calendar-iii"
---
## Description
<div class="description">
<p>Implement a <code>MyCalendarThree</code> class to store your events. A new event can <b>always</b> be added.</p>

<p>Your class will have one method, <code>book(int start, int end)</code>. Formally, this represents a booking on the half open interval <code>[start, end)</code>, the range of real numbers <code>x</code> such that <code>start &lt;= x &lt; end</code>.</p>

<p>A <i>K-booking</i> happens when <b>K</b> events have some non-empty intersection (ie., there is some time that is common to all K events.)</p>

<p>For each call to the method <code>MyCalendar.book</code>, return an integer <code>K</code> representing the largest integer such that there exists a <code>K</code>-booking in the calendar.</p>
Your class will be called like this: <code>MyCalendarThree cal = new MyCalendarThree();</code> <code>MyCalendarThree.book(start, end)</code>

<p><b>Example 1:</b></p>

<pre>
MyCalendarThree();
MyCalendarThree.book(10, 20); // returns 1
MyCalendarThree.book(50, 60); // returns 1
MyCalendarThree.book(10, 40); // returns 2
MyCalendarThree.book(5, 15); // returns 3
MyCalendarThree.book(5, 10); // returns 3
MyCalendarThree.book(25, 55); // returns 3
<b>Explanation:</b> 
The first two events can be booked and are disjoint, so the maximum K-booking is a 1-booking.
The third event [10, 40) intersects the first event, and the maximum K-booking is a 2-booking.
The remaining events cause the maximum K-booking to be only a 3-booking.
Note that the last event locally causes a 2-booking, but the answer is still 3 because
eg. [10, 20), [10, 40), and [5, 15) are still triple booked.
</pre>

<p>&nbsp;</p>

<p><b>Note:</b></p>

<ul>
	<li>The number of calls to <code>MyCalendarThree.book</code> per test case will be at most <code>400</code>.</li>
	<li>In calls to <code>MyCalendarThree.book(start, end)</code>, <code>start</code> and <code>end</code> are integers in the range <code>[0, 10^9]</code>.</li>
</ul>

<p>&nbsp;</p>
</div>

## Tags
- Segment Tree (segment-tree)
- Ordered Map (ordered-map)

## Companies
- Google - 2 (taggedByAdmin: true)
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

#### Approach #1: Boundary Count [Accepted]

**Intuition and Algorithm**

When booking a new event `[start, end)`, count `delta[start]++` and `delta[end]--`.  When processing the values of `delta` in sorted order of their keys, the largest such value is the answer.

In Python, we sort the set each time instead, as there is no analog to *TreeMap* available.

<iframe src="https://leetcode.com/playground/zbM2oKJX/shared" frameBorder="0" width="100%" height="378" name="zbM2oKJX"></iframe>


**Complexity Analysis**

* Time Complexity: $$O(N^2)$$, where $$N$$ is the number of events booked.  For each new event, we traverse `delta` in $$O(N)$$ time.  In Python, this is $$O(N^2 \log N)$$ owing to the extra sort step.

* Space Complexity: $$O(N)$$, the size of `delta`.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++] Clean Code
- Author: alexander
- Creation Date: Sun Nov 26 2017 04:18:16 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 12:57:26 GMT+0800 (Singapore Standard Time)

<p>
**Summarize**
`This is to find the maximum number of concurrent ongoing event at any time.`

We can log the `start` & `end` of each event on the timeline, each `start` add a new ongoing event at that time, each `end` terminate an ongoing event. Then we can scan the timeline to figure out the maximum number of ongoing event at any time.

The most intuitive data structure for `timeline` would be `array`, but the time spot we have could be very `sparse`, so we can use `sorted map` to simulate the time line to save space.

**Java**
```
class MyCalendarThree {
    private TreeMap<Integer, Integer> timeline = new TreeMap<>();
    public int book(int s, int e) {
        timeline.put(s, timeline.getOrDefault(s, 0) + 1); // 1 new event will be starting at [s]
        timeline.put(e, timeline.getOrDefault(e, 0) - 1); // 1 new event will be ending at [e];
        int ongoing = 0, k = 0;
        for (int v : timeline.values())
            k = Math.max(k, ongoing += v);
        return k;
    }
}
```
**C++**
```
class MyCalendarThree {
    map<int, int> timeline;
public:
    int book(int s, int e) {
        timeline[s]++; // 1 new event will be starting at [s]
        timeline[e]--; // 1 new event will be ending at [e];
        int ongoing = 0, k = 0;
        for (pair<int, int> t : timeline)
            k = max(k, ongoing += t.second);
        return k;
    }
};
```
</p>


### [C++] Map Solution,  beats 95%+
- Author: lee215
- Creation Date: Wed Oct 03 2018 09:07:04 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 11 2019 00:10:46 GMT+0800 (Singapore Standard Time)

<p>
## Explanation
We use a map `count`, use time points as the key.
If `count[t]` means that,
from the time point `t`,
we have `count[t]` events at the same time,
until the next time point after `t`.

Next let me explain the line of map operation
`count.emplace(t, (--count.lower_bound(t))->second)`.

If the map `count` already has the time point `t` as the key,
no insertion happen and the map doesn\'t change.
`count.emplace(t, value)` will return a pair of `<iterator, false>`

If the map `count` doesn\'t contain the time point `t` as the key,
`count.emplace(t, value)` will return a pair of `<iterator, true>`

`count.upper_bound(s)` will return the first time itertor after the time point `t`.
`--count.upper_bound(s)` will get the first time itertor before the time point `t`.
`(--count.upper_bound(s))->second` will get the event number at the time point `t`.
We insert this value to `count[t]`,
`count.emplace(t, value)` will return a pair of `<iterator, true>`

Now we need to do is,
iterate and increment all the time points in the time window `start` and `end`.
<br>

## Complexity
The total runtime is about 40ms, beats 95%.
`book(int s, int e)` iterates only time points between the start and end.
This halp reduce half runtme, compared with the solution iteration on whole map.
<br>

**C++**
```cpp
    map<int, int> count = {{-1, 0}};
    int res = 0;
    int book(int s, int e) {
        auto start = count.emplace(s, (--count.upper_bound(s))->second);
        auto end = count.emplace(e, (--count.upper_bound(e))->second);
        for (auto i = start.first; i != end.first; ++i)
            res = max(res, ++(i->second));
        return res;
    }
```
</p>


### Four AC Solution: TreeMap, BST, Segment Tree with Diagrams, Beats 100% time & space, 8ms, 39.4MB
- Author: devenps
- Creation Date: Wed Jun 24 2020 00:54:05 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Sep 25 2020 16:21:49 GMT+0800 (Singapore Standard Time)

<p>
**1. TreeMap Solution [Easiest]**

The idea is to add 2 entries in TreeMap, one for start and second for end time. Increment the **activeEvents** count by 1 for each **start time** and decrement the count for each **end time**.
And keep tracking the **maxCounts**.

```
class MyCalendarThree {
    Map<Integer, Integer> map;
    public MyCalendarThree() {
        map = new TreeMap<>();
    }
    
    public int book(int start, int end) {
        map.put(start, map.getOrDefault(start, 0) + 1);
        map.put(end, map.getOrDefault(end, 0) - 1);

        int activeEvents = 0, maxEvents = 0;
        for (int val : map.values()) {
            activeEvents += val;
            maxEvents = Math.max(maxEvents, activeEvents);
        }
        
        return maxEvents;
    }
}
```


**2. BST Solution [Fastest Solution - Beats 100% Time and Space] - Motivation for writing this acticle**

Store the start, end time, and event count in BST node
event count: this represents the count of parallel events running in start to end time range.

So keep adding nodes like we add nodes in BST if there is no overlap between both time windows. But if there are overlaps, then we can split it into multiple nodes which might me 1,2,3 nodes depending upon below 7 conditions:

**Condition 1:** Both overlaps fully, so no need to break the nodes, just increment the count.
![image](https://assets.leetcode.com/users/images/8516824a-af99-4639-b4a6-a970da84f555_1592929041.7091556.png)

**Condition 2:** No Overlap from Left Side, Split into 2 nodes
![image](https://assets.leetcode.com/users/images/ac53f66a-096b-4bbb-99d9-97c15d45d686_1592929517.1465816.png)

**Condition 3:** No Overlap from Right Side, Split into 2 nodes smiliar to **Condition 2**

**Condition 4:** Overlap from Left Side, Split it into 3 Nodes

![image](https://assets.leetcode.com/users/images/2da0c457-5a9d-4ff7-9dc7-300ee09996ab_1592929937.475058.png)

**Condition 5:** Overlap from Right Side, Split it into 3 Nodes smiliar to **Condition 4**

**Condition 6:** New Booking is subinterval of Existing Booking, Split it into 3 Nodes

![image](https://assets.leetcode.com/users/images/64550e99-bff2-4182-8fd8-dc6e32893ab5_1592930536.8304143.png)

**Condition 7:** Existing Booking is subinterval of new booking, Split it into 3 Nodes

![image](https://assets.leetcode.com/users/images/b51a0d47-a20a-436d-a87c-751b2deb19b5_1592930931.2199392.png)


```
class MyCalendarThree {
    TreeNode tree;
    int max = 0;

    public MyCalendarThree() {
    }

    public int book(int start, int end) {
        tree = book(start, end, tree, 1);
        return max;
    }

    public TreeNode book(int start, int end, TreeNode root, int count) {
        if (root == null) { 
			// Base condition to add node in BST
            root = new TreeNode(start, end, count);
            max = Math.max(max, root.count);
        } else if (start >= root.end) { 
			// No overlap, add  node in right subtree
            root.right = book(start, end, root.right, count);
        } else if (end <= root.start) { 
			// No overlap, add node in left subtree
            root.left = book(start, end, root.left, count);
        } else { 
			// Overlap, break nodes into multiple nodes (1/2/3 nodes)
            int startMin = Math.min(start, root.start);
            int startMax = Math.max(start, root.start);
            int endMin = Math.min(end, root.end);
            int endMax = Math.max(end, root.end);
            if (startMin < startMax) { 
				// No need to add new node if both are equal, instead update the count of current node.
                root.left = book(startMin, startMax, root.left, start < root.start ? count : root.count);
            }

            if (endMin < endMax) { 
				// No need to add new node if both are equal, instead update the count of current node.
                root.right = book(endMin, endMax, root.right, end > root.end ? count : root.count);
            }

            root.start = startMax;
            root.end = endMin;
            root.count+=count;
            max = Math.max(max, root.count);
        }

        return root;
    }

    private static class TreeNode {
        int count;
        int start;
        int end;
        TreeNode left;
        TreeNode right;

        TreeNode(int start, int end, int count) {
            this.start = start;
            this.end = end;
            this.count = count;
        }
    }
}
```



**3. Lazy Propagation Segment Tree Solution [Tried this solution just for practice this data structure]**

Standard Code for Lazy Propagation Segment Tree. [Video is attached in the last for this data structure]

Only one thing is tricky here is that we can\'t directly use array here to store your segment tree and lazy tree because **start and end time range can be order of 10^9**, otherwise it will give you **Memory Limit Exceeded**. 

But if you see the contraints of the question, then total numbers of calls will be **at most 400** so total entries for start and end events will be **400*2=800**. 

**So this gives me motivation of using hashmap instead of array since it will be sparse array so store only which is needed.**

```
class MyCalendarThree {
    Map<Integer, Integer> tree;
    Map<Integer, Integer> lazyTree;

    public MyCalendarThree() {
        tree = new HashMap<>();
        lazyTree = new HashMap<>();
    }

    public int book(int start, int end) {
        update(start, end - 1, 0, 0, 1000000000);
        return tree.getOrDefault(0, 0) + lazyTree.getOrDefault(0, 0);
    }

    private void update(int fromIndex, int toIndex, int pos, int low, int high) {
        if (low > high) return;
		
		// update the node of segment tree
        if (lazyTree.getOrDefault(pos, 0) != 0) {
            tree.put(pos, tree.getOrDefault(pos, 0) + lazyTree.get(pos));
            if (low != high) {
                lazyTree.put(2 * pos + 1, lazyTree.getOrDefault(2 * pos + 1, 0) + lazyTree.get(pos));
                lazyTree.put(2 * pos + 2, lazyTree.getOrDefault(2 * pos + 2, 0) + lazyTree.get(pos));
            }

            lazyTree.remove(pos);
        }

        // No overlap
        if (high < fromIndex || low > toIndex) return;

        // complete overlap
        if (fromIndex <= low && high <= toIndex) {
            tree.put(pos, tree.getOrDefault(pos, 0) + 1);

            if (low != high) {
                lazyTree.put(2 * pos + 1, lazyTree.getOrDefault(2 * pos + 1, 0) + 1);
                lazyTree.put(2 * pos + 2, lazyTree.getOrDefault(2 * pos + 2, 0) + 1);
            }

            return;
        }

        // Partial Overlap
        int mid = (high + low) / 2;
        update(fromIndex, toIndex, 2 * pos + 1, low, mid);
        update(fromIndex, toIndex, 2 * pos + 2, mid + 1, high);
        int val = Math.max(tree.getOrDefault(2 * pos + 1, 0), tree.getOrDefault(2 * pos + 2, 0));
        tree.put(pos, val);
	}
}
```

**4. Lazy Propagation Segment Tree Solution with TreeNode**

The intuition for this approach is similar as 3rd approach. We need to create only such entries/nodes which is needed. So instead of using array or hashmap, I am using TreeNode for storing values.

```
class MyCalendarThree {
    TreeNode root;

    public MyCalendarThree() {
        root = new TreeNode(0, 1000000000);
    }

    public int book(int start, int end) {
        update(start, end - 1, root);
        return root.val;
    }

    public void update(int fromIndex, int toIndex, TreeNode root) {
        if (fromIndex <= root.low && root.high <= toIndex) {
            root.lazyVal++;
        }
		
		// Update the node value
        root.val += root.lazyVal;
        if (root.low != root.high) {
            int mid = (root.low + root.high) / 2;
            if (root.left == null){
                root.left = new TreeNode(root.low, mid);
            }

            if (root.right == null){
                root.right = new TreeNode(mid+1, root.high);
            }

            root.left.lazyVal += root.lazyVal;
            root.right.lazyVal += root.lazyVal;
        }

        root.lazyVal = 0;

        // No overlap && Full overlap
        if (fromIndex > root.high || toIndex < root.low || fromIndex <= root.low && root.high <= toIndex){
            return;
        }

        // Partial overlap
        update(fromIndex, toIndex, root.left);
        update(fromIndex, toIndex, root.right);

        root.val = Math.max(root.left.val, root.right.val);
    }

    private static class TreeNode {
        int val;
        int lazyVal;
        int low;
        int high;
        TreeNode left;
        TreeNode right;

        TreeNode(int lo, int hi) {
            this.low = lo;
            this.high = hi;
        }
    }
}
```

[[Good Video Tutorial For Lazy Propagation Segment Tree](https://www.youtube.com/watch?v=xuoQdt5pHj0&t=444s)]


Similiar Problems:
* [Meeting Rooms](https://leetcode.com/problems/meeting-rooms)
* [Meeting Rooms II](https://leetcode.com/problems/meeting-rooms-ii)
* [My Calendar I](https://leetcode.com/problems/my-calendar-i)
* [My Calendar II](https://leetcode.com/problems/my-calendar-ii)
* [Car Pooling](https://leetcode.com/problems/car-pooling)
* [Corporate Flight Bookings](https://leetcode.com/problems/corporate-flight-bookings)

Please do let me know, if anything is incorrect.
Please upvote if you have reached till this point. \uD83D\uDE0B
</p>


