---
title: "Print Binary Tree"
weight: 587
#id: "print-binary-tree"
---
## Description
<div class="description">
<p>Print a binary tree in an m*n 2D string array following these rules: </p>

<ol>
<li>The row number <code>m</code> should be equal to the height of the given binary tree.</li>
<li>The column number <code>n</code> should always be an odd number.</li>
<li>The root node's value (in string format) should be put in the exactly middle of the first row it can be put. The column and the row where the root node belongs will separate the rest space into two parts (<b>left-bottom part and right-bottom part</b>). You should print the left subtree in the left-bottom part and print the right subtree in the right-bottom part. The left-bottom part and the right-bottom part should have the same size. Even if one subtree is none while the other is not, you don't need to print anything for the none subtree but still need to leave the space as large as that for the other subtree. However, if two subtrees are none, then you don't need to leave space for both of them. </li>
<li>Each unused space should contain an empty string <code>""</code>.</li>
<li>Print the subtrees following the same rules.</li>
</ol>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b>
     1
    /
   2
<b>Output:</b>
[["", "1", ""],
 ["2", "", ""]]
</pre>
</p>


<p><b>Example 2:</b><br />
<pre>
<b>Input:</b>
     1
    / \
   2   3
    \
     4
<b>Output:</b>
[["", "", "", "1", "", "", ""],
 ["", "2", "", "", "", "3", ""],
 ["", "", "4", "", "", "", ""]]
</pre>
</p>

<p><b>Example 3:</b><br />
<pre>
<b>Input:</b>
      1
     / \
    2   5
   / 
  3 
 / 
4 
<b>Output:</b>

[["",  "",  "", "",  "", "", "", "1", "",  "",  "",  "",  "", "", ""]
 ["",  "",  "", "2", "", "", "", "",  "",  "",  "",  "5", "", "", ""]
 ["",  "3", "", "",  "", "", "", "",  "",  "",  "",  "",  "", "", ""]
 ["4", "",  "", "",  "", "", "", "",  "",  "",  "",  "",  "", "", ""]]
</pre>
</p>

<p><b>Note:</b>
The height of binary tree is in the range of [1, 10].
</p>
</div>

## Tags
- Tree (tree)

## Companies
- Uber - 4 (taggedByAdmin: false)
- LinkedIn - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Poynt - 0 (taggedByAdmin: true)

## Official Solution
[TOC]


## Solution

---
#### Approach 1: Recursive Solution

We start by initializing a $$res$$ array with the dimensions being $$height \cdot 2^{height}-1$$. Here, $$height$$ refers to the number of levels in the given tree. In order to fill this $$res$$ array with the required elements, initially, we fill the complete array with `""` .  After this we make use of a recursive function `fill(res, root, i, l, r)` which fills the $$res$$ array such that the current element has to be filled in $$i^{th}$$ row, and the column being the middle of the indices $$l$$ and $$r$$, where $$l$$ and $$r$$ refer to the left and the right boundaries of the columns in which the current element can be filled.

In every recursive call, we do as follows:

1. If we've reached the end of the tree, i.e. if root==null, return.

2. Determine the column in which the current element($$root$$) needs to be filled, which is the middle of $$l$$ and $$r$$, given by say, $$j$$. The row number is same as $$i$$. Put the current element at $$res[i][j]$$.

3. Make the recursive call for the left child of the $$root$$ using `fill(res, root.left, i + 1, l, (l + r) / 2)`.

4. Make the recursive call for the right child of the $$root$$ using `fill(res, root.right, i + 1, (l + r + 1) / 2, r)`.

Note, that in the last two recursive calls, we update the row number(level of the tree). This ensures that the child nodes fit into the correct row. We also update the column boundaries appropriately based on the $$l$$ and $$r$$ values.

Further, to determine the $$height$$ also, we make use of recursive funtion `getHeight(root)`, which returns the height of the tree starting from the $$root$$ node. We traverse into all the branches possible in the tree recursively and find the depth of the longest branch.

At the end, we convert the $$res$$ array into the required list format, before returning the results.

<iframe src="https://leetcode.com/playground/hKNuwcwb/shared" frameBorder="0" width="100%" height="480" name="hKNuwcwb"></iframe>

**Complexity Analysis**

* Time complexity : $$O(h \cdot 2^h)$$. We need to fill the $$res$$ array of size $$h \cdot 2^h - 1$$. Here, $$h$$ refers to the height of the given tree.

* Space complexity : $$O(h \cdot 2^h)$$.  $$res$$ array of size $$h \cdot 2^h - 1$$ is used.

---
#### Approach 2: Using Queue (BFS)

**Algorithm**

We can also solve the problem by making use of Breadth First Search's idea. For this, we make use of a class $$Params$$ which stores the parameters of a $$node$$ of  the tree, including its value, its level in the tree($$i$$), and the left($$l$$) and right($$r$$) boundaries of the columns in which this element can be filled in the result to be returned.

We start by initializing a $$res$$ array as in the previous approach. After this, we add the parametrized $$root$$ of the tree into a $$queue$$. After this, we do the following at every step.

1. Remove an element, $$p$$,  from the front of the $$queue$$. 

2. Add this element at its correct position in the $$res$$ array given by $$res[p.i][(p.l + p.r) / 2]$$. Here, the values $$i$$, $$l$$ and $$r$$ refer to the column/level number, and the left and right boundaries permissible for putting the current node into $$res$$. These are obtained from the node's parameters, which have been associated with it before putting it into the $$queue$$.

3. If the left child of $$p$$ exists, put it at the back of the $$queue$$, in a parametized form, by appropriately updating the level as the next level and the boundaries permissible as well.

4. If the right child of $$p$$ exists, put it at the back of the $$queue$$, in a parametized form, by appropriately updating the level as the next level and the boundaries permissible as well.

5. Continue steps 1. to 4. till the $$queue$$ becomes empty. 

At the end, we again convert the $$res$$ array into the required list format, before returning the results.


<iframe src="https://leetcode.com/playground/VXnYopjG/shared" frameBorder="0" width="100%" height="500" name="VXnYopjG"></iframe>

**Complexity Analysis**

* Time complexity : $$O(h \cdot 2^h)$$. We need to fill the $$res$$ array of size $$h \cdot 2^h - 1$$. Here, $$h$$ refers to the height of the given tree.

* Space complexity : $$O(h \cdot 2^h)$$.  $$res$$ array of size $$h \cdot 2^h - 1$$ is used.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python recursive solution, easy to understand
- Author: kitt
- Creation Date: Tue Aug 08 2017 11:56:29 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 14:18:49 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution(object):
    def printTree(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[str]]
        """
        def get_height(node):
            return 0 if not node else 1 + max(get_height(node.left), get_height(node.right))
        
        def update_output(node, row, left, right):
            if not node:
                return
            mid = (left + right) / 2
            self.output[row][mid] = str(node.val)
            update_output(node.left, row + 1 , left, mid - 1)
            update_output(node.right, row + 1 , mid + 1, right)
            
        height = get_height(root)
        width = 2 ** height - 1
        self.output = [[''] * width for i in xrange(height)]
        update_output(node=root, row=0, left=0, right=width - 1)
        return self.output
```
</p>


### Java Recursive Solution
- Author: compton_scatter
- Creation Date: Sun Aug 06 2017 11:47:15 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 04:20:54 GMT+0800 (Singapore Standard Time)

<p>
```
public List<List<String>> printTree(TreeNode root) {
    List<List<String>> res = new LinkedList<>();
    int height = root == null ? 1 : getHeight(root);
    int rows = height, columns = (int) (Math.pow(2, height) - 1);
    List<String> row = new ArrayList<>();
    for(int i = 0; i < columns; i++)  row.add("");
    for(int i = 0; i < rows; i++)  res.add(new ArrayList<>(row));
    populateRes(root, res, 0, rows, 0, columns - 1);
    return res;
}

public void populateRes(TreeNode root, List<List<String>> res, int row, int totalRows, int i, int j) {
    if (row == totalRows || root == null) return;
    res.get(row).set((i+j)/2, Integer.toString(root.val));
    populateRes(root.left, res, row+1, totalRows, i, (i+j)/2 - 1);
    populateRes(root.right, res, row+1, totalRows, (i+j)/2+1, j);
}

public int getHeight(TreeNode root) {
     if (root == null) return 0;
     return 1 + Math.max(getHeight(root.left), getHeight(root.right));
}
```
</p>


### Java Iterative Level Order Traversal with Queue
- Author: zhuolikevin
- Creation Date: Sun Aug 06 2017 21:40:10 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 18:25:29 GMT+0800 (Singapore Standard Time)

<p>
It could be fairly easy when we made our first observation on the problem. For the output matrix, the number of rows is **height of the tree**. What about the number of columns?

- `row = 1 => col = 1 = 2^1 - 1`
- `row = 2 => col = 3 = 2^2 - 1`
- `row = 3 => col = 7 = 2^3 - 1`
- `row = 4 => col = 15 = 2^4 - 1`
`...`
- `row = m => col = 2^m - 1`

This can be derived from the number of leaves of a full tree (i.e `2^(height - 1)`) with spaces joined (i.e `2^(height - 1) - 1`).

Then we can fill the node in level by level. Another observation is **we always print a node at the center of its subtree index range**. What I mean is for the left or right child of a `node`, the subtree rooted at the child will use half of the indices of the `node`.

- `root` is at the center of `left` and `right`, say `mid`
- `root.left` (if not null) is at the center of `left` and `mid - 1`
- `root.right` (if not null) is at the center of `mid + 1` and `right`

Then we can easily have our solution as we always keep track of the `left` and `right` of the node.

```
public class Solution {
    public List<List<String>> printTree(TreeNode root) {
        List<List<String>> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        
        int rows = getHeight(root);
        int cols = (int)Math.pow(2, rows) - 1;
        for (int i = 0; i < rows; i++) {
            List<String> row = new ArrayList<>();
            for (int j = 0; j < cols; j++) {
                row.add("");
            }
            res.add(row);
        }
        
        Queue<TreeNode> queue = new LinkedList<>();
        Queue<int[]> indexQ = new LinkedList<>();
        queue.offer(root);
        indexQ.offer(new int[] { 0, cols - 1 });
        int row = -1;
        while (!queue.isEmpty()) {
            row++;
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode cur = queue.poll();
                int[] indices = indexQ.poll();
                
                if (cur == null) {
                    continue;
                }
                
                int left = indices[0];
                int right = indices[1];
                int mid = left + (right - left) / 2;
                res.get(row).set(mid, String.valueOf(cur.val));
                
                queue.offer(cur.left);
                queue.offer(cur.right);
                indexQ.offer(new int[] { left, mid - 1 });
                indexQ.offer(new int[] { mid + 1, right });
            }
        }
        
        return res;
    }
    
    private int getHeight(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return Math.max(getHeight(root.left), getHeight(root.right)) + 1;
    }
}
```
</p>


