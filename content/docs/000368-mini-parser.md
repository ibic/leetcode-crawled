---
title: "Mini Parser"
weight: 368
#id: "mini-parser"
---
## Description
<div class="description">
<p>Given a nested list of integers represented as a string, implement a parser to deserialize it.</p>

<p>Each element is either an integer, or a list -- whose elements may also be integers or other lists.</p>

<p><b>Note:</b> You may assume that the string is well-formed:</p>

<ul>
	<li>String is non-empty.</li>
	<li>String does not contain white spaces.</li>
	<li>String contains only digits <code>0-9</code>, <code>[</code>, <code>-</code> <code>,</code>, <code>]</code>.</li>
</ul>

<p>&nbsp;</p>

<p><b>Example 1:</b></p>

<pre>
Given s = &quot;324&quot;,

You should return a NestedInteger object which contains a single integer 324.
</pre>

<p>&nbsp;</p>

<p><b>Example 2:</b></p>

<pre>
Given s = &quot;[123,[456,[789]]]&quot;,

Return a NestedInteger object containing a nested list with 2 elements:

1. An integer containing value 123.
2. A nested list containing two elements:
    i.  An integer containing value 456.
    ii. A nested list with one element:
         a. An integer containing value 789.
</pre>

<p>&nbsp;</p>

</div>

## Tags
- String (string)
- Stack (stack)

## Companies
- Google - 3 (taggedByAdmin: false)
- Airbnb - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### An Java Iterative Solution
- Author: AlexTheGreat
- Creation Date: Mon Aug 15 2016 02:30:49 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 07:29:41 GMT+0800 (Singapore Standard Time)

<p>
This approach will just iterate through every char in the string (no recursion).
* If encounters '[', push current NestedInteger to stack and start a new one.
* If encounters ']', end current NestedInteger and pop a NestedInteger from stack to continue.
* If encounters ',', append a new number to curr NestedInteger, if this comma is not right after a brackets.
* Update index l and r, where l shall point to the start of a integer substring, while r shall points to the end+1 of substring.


Java Code:

    public NestedInteger deserialize(String s) {
        if (s.isEmpty())
            return null;
        if (s.charAt(0) != '[') // ERROR: special case
            return new NestedInteger(Integer.valueOf(s));
            
        Stack<NestedInteger> stack = new Stack<>();
        NestedInteger curr = null;
        int l = 0; // l shall point to the start of a number substring; 
                   // r shall point to the end+1 of a number substring
        for (int r = 0; r < s.length(); r++) {
            char ch = s.charAt(r);
            if (ch == '[') {
                if (curr != null) {
                    stack.push(curr);
                }
                curr = new NestedInteger();
                l = r+1;
            } else if (ch == ']') {
                String num = s.substring(l, r);
                if (!num.isEmpty())
                    curr.add(new NestedInteger(Integer.valueOf(num)));
                if (!stack.isEmpty()) {
                    NestedInteger pop = stack.pop();
                    pop.add(curr);
                    curr = pop;
                }
                l = r+1;
            } else if (ch == ',') {
                if (s.charAt(r-1) != ']') {
                    String num = s.substring(l, r);
                    curr.add(new NestedInteger(Integer.valueOf(num)));
                }
                l = r+1;
            }
        }
        
        return curr;
    }
</p>


### Clarification
- Author: zchen39
- Creation Date: Wed Aug 24 2016 14:06:50 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 10:53:09 GMT+0800 (Singapore Standard Time)

<p>
No offense but this problem seriously needs some more explanation and grammar check. I want to add a few clarification as follows so it saves you some time:

1. the add() method adds a NestedInteger object to the caller. e.g.:
outer = NestedInteger() # []
nested = NestedInteger(5) 
outer2 = nested
outer.add(nested) # outer is now [5]
outer2.add(outer) # outer2 is now [5, [5]]

"Set this NestedInteger to hold a nested list and adds a nested integer elem to it." cannot be more vague.

2. '-' means negative. It's not a delimiter.

3. For test cases like "324" you need to return something like NestedInteger(324) not "[324]". 

4. A list cannot have multiple consecutive integers. e.g. "321, 231" is invalid. I guess it's for difficulty purposes.
</p>


### Python & C++ solutions
- Author: StefanPochmann
- Creation Date: Sun Aug 14 2016 23:00:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 03:11:43 GMT+0800 (Singapore Standard Time)

<p>
## Python using `eval`:

    def deserialize(self, s):
        def nestedInteger(x):
            if isinstance(x, int):
                return NestedInteger(x)
            lst = NestedInteger()
            for y in x:
                lst.add(nestedInteger(y))
            return lst
        return nestedInteger(eval(s))

## Python one-liner

    def deserialize(self, s):
        return NestedInteger(s) if isinstance(s, int) else reduce(lambda a, x: a.add(self.deserialize(x)) or a, s, NestedInteger()) if isinstance(s, list) else self.deserialize(eval(s))

## Python Golf (136 bytes or 31 bytes)
```
class Solution:deserialize=d=lambda S,s,N=NestedInteger:s<[]and N(s)or s<''and reduce(lambda a,x:a.add(S.d(x))or a,s,N())or S.d(eval(s))
```
Or abusing how the judge judges (yes, this gets accepted):
```
class Solution:deserialize=eval
```

## Python parsing char by char

Here I turned the input string into a list with sentinel for convenience.

    def deserialize(self, s):
        def nestedInteger():
            num = ''
            while s[-1] in '1234567890-':
                num += s.pop()
            if num:
                return NestedInteger(int(num))
            s.pop()
            lst = NestedInteger()
            while s[-1] != ']':
                lst.add(nestedInteger())
                if s[-1] == ',':
                    s.pop()
            s.pop()
            return lst
        s = list(' ' + s[::-1])
        return nestedInteger()

## C++ using `istringstream`
```
class Solution {
public:
    NestedInteger deserialize(string s) {
        istringstream in(s);
        return deserialize(in);
    }
private:
    NestedInteger deserialize(istringstream &in) {
        int number;
        if (in >> number)
            return NestedInteger(number);
        in.clear();
        in.get();
        NestedInteger list;
        while (in.peek() != ']') {
            list.add(deserialize(in));
            if (in.peek() == ',')
                in.get();
        }
        in.get();
        return list;
    }
};
```
</p>


