---
title: "Word Pattern II"
weight: 274
#id: "word-pattern-ii"
---
## Description
<div class="description">
<p>Given a <code>pattern</code> and a string <code>s</code>, return <code>true</code><em> if </em><code>s</code><em> <strong>matches</strong> the </em><code>pattern</code><em>.</em></p>

<p>A string <code>s</code> <b>matches</b> a <code>pattern</code> if there is some <strong>bijective mapping</strong> of single characters to strings such that if each character in <code>pattern</code> is replaced by the string it maps to, then the resulting string is <code>s</code>. A <strong>bijective mapping</strong> means that no two characters map to the same string, and no character maps to two different strings.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> pattern = &quot;abab&quot;, s = &quot;redblueredblue&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong> One possible mapping is as follows:
&#39;a&#39; -&gt; &quot;red&quot;
&#39;b&#39; -&gt; &quot;blue&quot;</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> pattern = &quot;aaaa&quot;, s = &quot;asdasdasdasd&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong> One possible mapping is as follows:
&#39;a&#39; -&gt; &quot;asd&quot;
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> pattern = &quot;abab&quot;, s = &quot;asdasdasdasd&quot;
<strong>Output:</strong> true
<strong>Explanation:</strong> One possible mapping is as follows:
&#39;a&#39; -&gt; &quot;a&quot;
&#39;b&#39; -&gt; &quot;sdasd&quot;
Note that &#39;a&#39; and &#39;b&#39; cannot both map to &quot;asd&quot; since the mapping is a bijection.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> pattern = &quot;aabb&quot;, s = &quot;xyzabcxzyabc&quot;
<strong>Output:</strong> false
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= pattern.length &lt;= 20</code></li>
	<li><code>0 &lt;= s.length &lt;= 50</code></li>
	<li><code>pattern</code> and <code>s</code> consist of only lower-case English letters.</li>
</ul>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Uber - 4 (taggedByAdmin: true)
- Pony.ai - 4 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Dropbox - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Share my Java backtracking solution
- Author: jeantimex
- Creation Date: Sat Oct 10 2015 11:50:05 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 11:02:51 GMT+0800 (Singapore Standard Time)

<p>
We can solve this problem using backtracking, we just have to keep trying to use a character in the pattern to match different length of substrings in the input string, keep trying till we go through the input string and the pattern.

For example, input string is `"redblueredblue"`, and the pattern is `"abab"`, first let's use `'a'` to match `"r"`, `'b'` to match `"e"`, then we find that `'a'` does not match `"d"`, so we do backtracking, use `'b'` to match `"ed"`, so on and so forth ...

When we do the recursion, if the pattern character exists in the hash map already, we just have to see if we can use it to match the same length of the string. For example, let's say we have the following map:

`'a': "red"`

`'b': "blue"`

now when we see `'a'` again, we know that it should match `"red"`, the length is `3`, then let's see if `str[i ... i+3]` matches `'a'`, where `i` is the current index of the input string. Thanks to [StefanPochmann][1]'s suggestion, in Java we can elegantly use `str.startsWith(s, i)` to do the check.

Also thanks to [i-tikhonov][2]'s suggestion, we can use a hash set to avoid duplicate matches, if character `a` matches string `"red"`, then character `b` cannot be used to match `"red"`. In my opinion though, we can say apple (pattern `'a'`) is "fruit", orange (pattern `'o'`) is "fruit", so they can match the same string, anyhow, I guess it really depends on how the problem states.

The following code should pass OJ now, if we don't need to worry about the duplicate matches, just remove the code that associates with the hash set.

    public class Solution {
      
      public boolean wordPatternMatch(String pattern, String str) {
        Map<Character, String> map = new HashMap<>();
        Set<String> set = new HashSet<>();
        
        return isMatch(str, 0, pattern, 0, map, set);
      }
      
      boolean isMatch(String str, int i, String pat, int j, Map<Character, String> map, Set<String> set) {
        // base case
        if (i == str.length() && j == pat.length()) return true;
        if (i == str.length() || j == pat.length()) return false;
        
        // get current pattern character
        char c = pat.charAt(j);
        
        // if the pattern character exists
        if (map.containsKey(c)) {
          String s = map.get(c);
          
          // then check if we can use it to match str[i...i+s.length()]
          if (!str.startsWith(s, i)) {
            return false;
          }
          
          // if it can match, great, continue to match the rest
          return isMatch(str, i + s.length(), pat, j + 1, map, set);
        }
        
        // pattern character does not exist in the map
        for (int k = i; k < str.length(); k++) {
          String p = str.substring(i, k + 1);

          if (set.contains(p)) {
            continue;
          }

          // create or update it
          map.put(c, p);
          set.add(p);
          
          // continue to match the rest
          if (isMatch(str, k + 1, pat, j + 1, map, set)) {
            return true;
          }

          // backtracking
          map.remove(c);
          set.remove(p);
        }
        
        // we've tried our best but still no luck
        return false;
      }
      
    }


  [1]: https://leetcode.com/discuss/user/StefanPochmann
  [2]: https://leetcode.com/discuss/user/i-tikhonov
</p>


### *Java* HashSet + backtracking (2ms beats 100%)
- Author: ElementNotFoundException
- Creation Date: Tue Feb 02 2016 04:50:52 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 10 2018 14:45:26 GMT+0800 (Singapore Standard Time)

<p>
The idea might not be so different, but I tried to optimize the solution as much as I could. In concrete:

 - Instead of using HashMap, I use a String array to store the character --> String mapping
 - Instead of trying all combinations, I only try necessary/possible ones.

I'd like to explain the second point a little bit more: Suppose we are successful in mapping the first `i` characters in `pattern`, and we are now at the `j` location of `str`. If `i+1`'s character in pattern is not already mapped, then we would want to try all possible substrings in `str`, that is, the substring could be of length 1 (`j`'s character), or length 2 (`j` and `j+1`), etc. Normally we would try up to the end of `str`, but this is really not necessary, because we have to spare enough space future characters in `pattern`. If this is not clear enough, let's take the following as an example:

    pattern = "abca"
    str = "xxxyyzzxxx"

Suppose we have successfully matched `a` to `xxx` and `b` to `yy`, and now we are at the third character of pattern, i.e., character `c`. We have not mapped `c` to anything, so we could try any of the following:

    z
    zz
    zzx
    zzxx
    zzxxx

But do we really need to try them all? The answer is NO. Because we have already mapped `a` and `b`, and under this constraint, we have to save enough space for the fourth character of pattern, i.e., `a`. In other words, we can at most try `z` and `zz`, otherwise we are doomed to return false when we reach the fourth character `a`. This is what `endPoint` is about in the code.

Code in Java:

    public boolean wordPatternMatch(String pattern, String str) {
        String[] map = new String[26]; // mapping of characters 'a' - 'z'
        HashSet<String> set = new HashSet<>(); // mapped result of 'a' - 'z'
        return wordPatternMatch(pattern, str, map, set, 0, str.length()-1, 0, pattern.length()-1);
    }
    private boolean wordPatternMatch(String pattern, String str, String[] map, HashSet<String> set, int start, int end, int startP, int endP) {
    	if(startP==endP+1 && start==end+1) return true; // both pattern and str are exhausted
    	if((startP>endP && start<=end) || (startP<endP && start>end)) return false; // either of pattern or str is exhausted

    	char ch = pattern.charAt(startP);
    	String matched = map[ch-'a'];
    	if(matched!=null) { // ch is already mapped, then continue
    		int count = matched.length();
    		return start+count<=end+1 && matched.equals(str.substring(start, start+count)) // substring equals previously mapped string
    				&& wordPatternMatch(pattern, str, map, set, start+matched.length(), end, startP+1, endP); // moving forward
    	}
    	else {
    	    int endPoint = end;
    	    for(int i=endP; i>startP; i--) {
    	        endPoint -= map[pattern.charAt(i)-'a']==null ? 1 : map[pattern.charAt(i)-'a'].length();
    	    }
    		for(int i=start; i<=endPoint; i++) { // try every possible mapping, from 1 character to the end
    			matched = str.substring(start, i+1);
    			if(set.contains(matched)) continue; // different pattern cannot map to same substring

    			map[ch-'a'] = matched; // move forward, add corresponding mapping and set content
    			set.add(matched);

    			if(wordPatternMatch(pattern, str, map, set, i+1, end, startP+1, endP)) return true;

    			else { // backtracking, remove corresponding mapping and set content
    				map[ch-'a'] = null;
    				set.remove(matched);
    			}
    		}
    	}
    	return false; // exhausted
    }

If you are interested in my other posts, please feel free to check my Github page here: [https://github.com/F-L-A-G/Algorithms-in-Java][1]


  [1]: https://github.com/F-L-A-G/Algorithms-in-Java
</p>


### 20 lines JAVA clean solution, easy to understand
- Author: DREAM123
- Creation Date: Sun Oct 11 2015 06:53:21 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 11 2015 06:53:21 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
    Map<Character,String> map =new HashMap();
    Set<String> set =new HashSet();
    public boolean wordPatternMatch(String pattern, String str) {
        if(pattern.isEmpty()) return str.isEmpty();
        if(map.containsKey(pattern.charAt(0))){
            String value= map.get(pattern.charAt(0));
            if(str.length()<value.length() || !str.substring(0,value.length()).equals(value)) return false;
            if(wordPatternMatch(pattern.substring(1),str.substring(value.length()))) return true;
        }else{
            for(int i=1;i<=str.length();i++){
                if(set.contains(str.substring(0,i))) continue;
                map.put(pattern.charAt(0),str.substring(0,i));
                set.add(str.substring(0,i));
                if(wordPatternMatch(pattern.substring(1),str.substring(i))) return true;
                set.remove(str.substring(0,i));
                map.remove(pattern.charAt(0));
            }
        }
        return false;
    }
}
</p>


