---
title: "Combinations"
weight: 77
#id: "combinations"
---
## Description
<div class="description">
<p>Given two integers <em>n</em> and <em>k</em>, return all possible combinations of <em>k</em> numbers out of 1 ... <em>n</em>.</p>

<p>You may return the answer in <strong>any order</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> n = 4, k = 2
<strong>Output:</strong>
[
  [2,4],
  [3,4],
  [2,3],
  [1,2],
  [1,3],
  [1,4],
]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> n = 1, k = 1
<strong>Output:</strong> [[1]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 20</code></li>
	<li><code>1 &lt;= k &lt;= n</code></li>
</ul>

</div>

## Tags
- Backtracking (backtracking)

## Companies
- Apple - 3 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Yahoo - 2 (taggedByAdmin: false)
- Microsoft - 4 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Approach 1: Backtracking

**Algorithm**

[Backtracking](https://en.wikipedia.org/wiki/Backtracking) 
is an algorithm for finding all
solutions by exploring all potential candidates.
If the solution candidate turns to be _not_ a solution 
(or at least not the _last_ one), 
backtracking algorithm discards it by making some changes 
on the previous step, *i.e.* _backtracks_ and then try again.

Here is a backtrack function 
which takes a first integer to add and 
a current combination as arguments `backtrack(first, curr)`.

* If the current combination is done - add it to output.

* Iterate over the integers from `first` to `n`.

    * Add integer `i` into the current combination `curr`.

    * Proceed to add more integers into the combination : 
    `backtrack(i + 1, curr)`.

    * Backtrack by removing `i` from `curr`.

**Implementation**

!?!../Documents/77_LIS.json:1000,528!?!

<iframe src="https://leetcode.com/playground/7SP4C8Q6/shared" frameBorder="0" width="100%" height="500" name="7SP4C8Q6"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(k C_N^k)$$, where 
$$C_N^k = \frac{N!}{(N - k)! k!}$$ is a number of combinations to build.
`append / pop` (`add / removeLast`) operations are constant-time ones
and the only consuming part here is to append the built combination of
length `k` to the output.
 
* Space complexity : $$\mathcal{O}(C_N^k)$$ to keep all
the combinations for an output.
<br />
<br />


---
#### Approach 2: Lexicographic (binary sorted) combinations

**Intuition**

The idea here is not just to get the combinations but
to generate them in a lexicographic sorted order. 

![postorder](../Figures/77/77_lex.png)

**Algorithm**

The algorithm is quite straightforward : 

* Initiate `nums` as a list of integers from `1` to `k`. Add `n + 1`
as a last element, it will serve as a sentinel. 
Set the pointer in the beginning of the list `j = 0`.

* While `j < k` :
    
    * Add the first k elements from `nums` into the output, i.e.
    all elements but the sentinel.
    
    * Find the first number in nums such that `nums[j] + 1 != nums[j + 1]`
    and increase it by one `nums[j]++` to move to the next combination.

**Implementation**

<iframe src="https://leetcode.com/playground/XjdDUhMi/shared" frameBorder="0" width="100%" height="446" name="XjdDUhMi"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(k C_N^k)$$, where 
$$C_N^k = \frac{N!}{(N - k)! k!}$$ is a number of combinations to build.

    The external while loop is executed $$C_N^k$$ times since the
number of combinations is $$C_N^k$$. 
The inner while loop is performed $$C_{N - j}^{k - j}$$ times for a given `j`. 
In average over $$C_N^k$$ visits from the external loop 
that results in less than one execution per visit.   
Hence the most consuming part here is to append each combination 
of length $$k$$ ($$C_N^k$$ combinations in total) to the output,
that takes $$\mathcal{O}(k C_N^k)$$ time.

    You could notice that the second algorithm is much faster than the 
    first one despite they both have the same time complexity.
    It's a consequence of dealing 
    with the recursive call stack frame for the first algorithm,
    and the effect is much more pronounced in Python than in Java.
    
* Space complexity : $$\mathcal{O}(C_N^k)$$ to keep all
the combinations for an output. 

**Links**

[Donald E. Knuth, The Art of Computer Programming, 4A (2011)](https://www-cs-faculty.stanford.edu/~knuth/taocp.html)

## Accepted Submission (java)
```java
public class Solution {
    public List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> r = new ArrayList<List<Integer>>();
        if (n < k) {
            return r;
        }

        for (int i = 1; i <= n - k + 1; i++) {
            List<Integer> li = new ArrayList<Integer>();
            li.add(i);
            r.add(li);
        }

        for (int i = 2; i <= k; i++) {
            List<List<Integer>> swap = new ArrayList<List<Integer>>();
            for (List<Integer> li : r) {
                int lasti = li.get(li.size() - 1);
                for (int ili = lasti + 1; ili <= n - k + i; ili++) {
                    List<Integer> newLi = new ArrayList<Integer>();
                    newLi.addAll(li);
                    newLi.add(ili);
                    swap.add(newLi);
                }
            }
            r = swap;
        }

        return r;
    }
}
```

## Top Discussions
### Backtracking Solution Java
- Author: fabrizio3
- Creation Date: Fri Apr 10 2015 17:15:05 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 17:09:57 GMT+0800 (Singapore Standard Time)

<p>
        public static List<List<Integer>> combine(int n, int k) {
    		List<List<Integer>> combs = new ArrayList<List<Integer>>();
    		combine(combs, new ArrayList<Integer>(), 1, n, k);
    		return combs;
    	}
    	public static void combine(List<List<Integer>> combs, List<Integer> comb, int start, int n, int k) {
    		if(k==0) {
    			combs.add(new ArrayList<Integer>(comb));
    			return;
    		}
    		for(int i=start;i<=n;i++) {
    			comb.add(i);
    			combine(combs, comb, i+1, n, k-1);
    			comb.remove(comb.size()-1);
    		}
    	}
</p>


### Short Iterative C++ Answer 8ms
- Author: hengyi
- Creation Date: Fri Oct 09 2015 13:47:58 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 13:21:48 GMT+0800 (Singapore Standard Time)

<p>
    class Solution {
    public:
    	vector<vector<int>> combine(int n, int k) {
    		vector<vector<int>> result;
    		int i = 0;
    		vector<int> p(k, 0);
    		while (i >= 0) {
    			p[i]++;
    			if (p[i] > n) --i;
    			else if (i == k - 1) result.push_back(p);
    			else {
    			    ++i;
    			    p[i] = p[i - 1];
    			}
    		}
    		return result;
    	}
    };
</p>


### A short recursive Java solution based on C(n,k)=C(n-1,k-1)+C(n-1,k)
- Author: vision57
- Creation Date: Wed Apr 22 2015 22:30:33 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Apr 22 2015 22:30:33 GMT+0800 (Singapore Standard Time)

<p>
Basically, this solution follows the idea of the mathematical formula C(n,k)=C(n-1,k-1)+C(n-1,k).

Here C(n,k) is divided into two situations. Situation one, number n is selected, so we only need to select k-1 from n-1 next. Situation two, number n is not selected, and the rest job is selecting k from n-1.

    public class Solution {
        public List<List<Integer>> combine(int n, int k) {
            if (k == n || k == 0) {
                List<Integer> row = new LinkedList<>();
                for (int i = 1; i <= k; ++i) {
                    row.add(i);
                }
                return new LinkedList<>(Arrays.asList(row));
            }
            List<List<Integer>> result = this.combine(n - 1, k - 1);
            result.forEach(e -> e.add(n));
            result.addAll(this.combine(n - 1, k));
            return result;
        }
    }
</p>


