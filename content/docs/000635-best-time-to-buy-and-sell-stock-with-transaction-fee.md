---
title: "Best Time to Buy and Sell Stock with Transaction Fee"
weight: 635
#id: "best-time-to-buy-and-sell-stock-with-transaction-fee"
---
## Description
<div class="description">
<p>Your are given an array of integers <code>prices</code>, for which the <code>i</code>-th element is the price of a given stock on day <code>i</code>; and a non-negative integer <code>fee</code> representing a transaction fee.</p>
<p>You may complete as many transactions as you like, but you need to pay the transaction fee for each transaction.  You may not buy more than 1 share of a stock at a time (ie. you must sell the stock share before you buy again.)</p>
<p>Return the maximum profit you can make.</p>

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> prices = [1, 3, 2, 8, 4, 9], fee = 2
<b>Output:</b> 8
<b>Explanation:</b> The maximum profit can be achieved by:
<li>Buying at prices[0] = 1</li><li>Selling at prices[3] = 8</li><li>Buying at prices[4] = 4</li><li>Selling at prices[5] = 9</li>The total profit is ((8 - 1) - 2) + ((9 - 4) - 2) = 8.
</pre>
</p>

<p><b>Note:</b>
<li><code>0 < prices.length <= 50000</code>.</li>
<li><code>0 < prices[i] < 50000</code>.</li>
<li><code>0 <= fee < 50000</code>.</li>
</p>
</div>

## Tags
- Array (array)
- Dynamic Programming (dynamic-programming)
- Greedy (greedy)

## Companies
- Amazon - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: true)
- Bloomberg - 0 (taggedByAdmin: true)

## Official Solution
[TOC]


#### Approach #1: Dynamic Programming [Accepted]

**Intuition and Algorithm**

At the end of the `i`-th day, we maintain `cash`, the maximum profit we could have if we did not have a share of stock, and `hold`, the maximum profit we could have if we owned a share of stock.

To transition from the `i`-th day to the `i+1`-th day, we either sell our stock `cash = max(cash, hold + prices[i] - fee)` or buy a stock `hold = max(hold, cash - prices[i])`.  At the end, we want to return `cash`.  We can transform `cash` first without using temporary variables because selling and buying on the same day can't be better than just continuing to hold the stock.

**Python**
```python
class Solution(object):
    def maxProfit(self, prices, fee):
        cash, hold = 0, -prices[0]
        for i in range(1, len(prices)):
            cash = max(cash, hold + prices[i] - fee)
            hold = max(hold, cash - prices[i])
        return cash
```

**Java**
```java
class Solution {
    public int maxProfit(int[] prices, int fee) {
        int cash = 0, hold = -prices[0];
        for (int i = 1; i < prices.length; i++) {
            cash = Math.max(cash, hold + prices[i] - fee);
            hold = Math.max(hold, cash - prices[i]);
        }
        return cash;
    }
}
```

**Complexity Analysis**

* Time Complexity: $$O(N)$$, where $$N$$ is the number of prices.

* Space Complexity: $$O(1)$$, the space used by `cash` and `hold`.

## Accepted Submission (python3)
```python3
#
# @lc app=leetcode id=714 lang=python3
#
# [714] Best Time to Buy and Sell Stock with Transaction Fee
#
from typing import List

class Solution:
    def maxProfit(self, prices: List[int], fee: int) -> int:
        if len(prices) == 0:
            return 0
        s0 = 0
        s1 = -prices[0]
        for price in prices[1:]:
            s1 = max(s1, s0 - price)
            s0 = max(s0, s1 + price - fee)
        return s0

if __name__ == '__main__':
    sol = Solution()
    sampleInput = [1, 3, 2, 8, 4, 9]
    sampleFee = 2
    failedInput = [9,8,7,1,2]
    failedFee = 3
    input1 = [1036,2413,2776,825,2640,31,1560,2917,4282,783,3146,2600,1939,694,4284,3881,554,167,372,4620,3037,1175,1075,3845,4981,4495,3406,4228,2807,4774,4526,3914,2633,3762,1570,2334,616,1648,1914,2900,349,2428,4013,1964,4020,1882,629,240,2595,2902,3419,292,224,4437,4918,632,3701,3840,3996,2129,3345,3979,1954,781,1576,1084,3250,4517,3106,2133,309,4520,2225,4366,4628,1303,4373,1266,3181,558,3855,3447,4335,2115,4603,661,1715,3972,2846,342,686,787,273,2575,100,2860,3587,4236,3862,2238,3471,3123,431,4489,1551,596,4037,4986,594,2386,326,628,1363,2377,4986,3780,3853,2670,2852,3519,2998,4083,3392,2394,1083,3958,4082,1506,2322,2715,4901,2555,4097,3748,4717,3901,3329,4616,3334,2603,3705,631,3541,555,508,464,4495,4463,3616,31,2177,3307,1011,2759,751,1537,1000,292,3921,1442,2726,4677,792,82,2580,609,4758,3190,1958,913,955,1259,1634,4729,2672,1761,1467,2347,4295,2049,4708,1452,3411,1428,4078,2627,3785,2432,2916,492,1108,1691,972,3823,4086,2115,1925,1454,291,3266,300,2539,2681,2084,4633,1084,1061,1043,1304,2205,410,4332,2567,703,529,4273,3684,308,3164,4876,3108,4993,4555,1237,4753,549,2795,3426,819,2897,825,2514,3419,1854,3209,3766,2794,4117,4668,2162,1571,2446,1480,974,1090,3903,4655,4452,1451,2953,1241,842,1750,3847,3053,4395,4338,1493,1660,1569,3418,3029,4416,2056,2283,3392,2032,4354,803,4959,3630,2080,1553,873,4050,1986,2328,55,4602,1430,4238,4326,3382,4845,4968,1903,423,4717,2427,4618,2644,4541,380,3404,4880,2577,1640,189,2692,3788,818,4091,4730,611,1776,3594,4746,580,2083,4183,3355,3063,658,4532,3318,3902,556,2249,4653,2118,1529,4793,4935,4259,3542,1705,2839,1436,3918,564,3277,2988,2460,3213,4445,4238,1954,2213,1748,939,1149,1408,2408,1781,1618,1457,2123,3366,826,2094,16,1161,3337,1864,433,1303,4800,4667,4769,1026,3440,1072,4725,6,1263,4184,2728,1315,2091,3032,2071,2672,4557,1916,638,2133,2687,2408,1677,344,697,1699,8,480,655,2656,4983,455,1611,1726,692,392,1921,2555,3549,3740,3840,3062,3420,2428,1169,4570,389,3509,2169,3290,1680,1733,1765,2518,3260,3644,765,4521,269,2501,4014,1743,239,4908,1656,4433,3647,2612,4872,387,3091,4011,564,4421,810,3623,3451,4108,1428,475,3755,4484,3527,3062,4706,3424,2678,2411,4446,2556,4305,1305,646,1458,4471,1689,4556,3851,1245,1197,3785,1175,2904,302,2422,4302,2148,2338,4288,375,2824,1623,3717,1142,4254,192,783,1963,2225,1209,1746,3072,2737,4640,4919,3614,804,4029,1751,2360,3789,4445,2283,2769,2833,4452,2978,2809,4532,4365,2124,3541,2658,2902,4688,3980,1543,4041,1420,1452,1284,66,19,947,932,3244,3374,1910,2561,3466,4104,1667,589,3048,730,1770,1241,2270,4016,2835,604,4771,514,3854,3427,1875,2038,3067,3216,4732,3735,4440,2855,4958,4569,1685,3539,4589,3512,3143,898,3004,3072,2573,3163,2522,3927,330,3874,363,1900,1629,1156,4259,2747,3445,4513,2867,52,3870,1761,619,3308,4380,1101,2592,4852,4140,174,3997,4617,3500,3028,907,2355,759,374,2429,412,2132,3973,3583,3028,2070,2235,2659,1053,2558,753,1221,1185,2225,1593,3554,3703,332,2843,3349,3871,4389,6,2768,4382,902,417,191,2107,2838,4958,3905,4966,3937,1105,4150,2682,3396,818,2297,2077,2032,3340,2478,127,4379,954,2593,3454,1230,2308,3694,2179,4134,653,3808,4043,2069,660,4515,4189,4876,1784,4166,342,1766,3305,1980,1909,4115,4115,1461,2061,838,3112,122,656,4856,4822,3468,2111,2700,4124,4663,2948,3029,4182,3847,4760,1323,1505,308,128,874,583,2671,1315,747,2682,2841,67,2712,2703,4471,2952,3081,464,655,57,1460,1395,682,2447,2590,4624,1578,64,4060,2975,1236,831,3313,1432,2589,3777,1868,1720,45,3311,4532,2672,454,752,4839,4717,748,4323,2999,3491,631,1407,1453,4611,4263,3366,584,2014,2396,1902,4569,3002,1938,3998,4093,1899,3071,2815,1974,302,1641,2836,565,264,1332,3319,3689,2181,3873,4883,3849,1991,4633,4556,3866,142,2903,3181,740,3311,2071,280,714,2440,3950,290,3580,738,1604,3631,1989,1299,836,1913,224,1066,1741,1551,1735,4601,2024,4570,4192,1723,3949,3696,1419,1760,697,4764,3405,4443,199,717,4568,3252,2016,2151,1741,2613,2736,4053,814,4282,3392,615,1998,3294,3663,559,4278,4626,55,1418,2056,3191,3181,1732,1887,2517,3180,2154,2166,3096,3930,2721,4332,427,4332,4237,3928,2262,4657,2202,922,3711,1921,4728,2236,2441,622,233,293,1466,1891,1222,3693,3261,2605,3486,102,3612,1897,2698,3524,3567,613,3834,1583,1482,4734,2339,752,1428,4121,3267,3518,4652,3119,1818,4596,3181,3159,4069,3375,3762,1386,3054,3052,67,2246,1493,2738,2835,4906,303,1107,3111,1525,1739,437,2941,545,1458,993,1871,640,4047,2017,4971,4917,701,4811,4335,3221,4187,4414,756,3069,3052,812,3135,928,1264,3356,4518,2136,2691,2638,3156,4909,2944,3920,4609,1856,654,4643,2932,309,3613,4479,4173,1848,165,1171,592,3233,3151,4009,3952,2624,38,2616,2056,841,1764,4667,1526,125,3963,933,3951,2151,2110,4666,1000,1985,3868,2735,635,277,1129,572,2136,980,2731,556,3012,2900,2180,1912,2799,1771,4441,2666,3958,4381,3677,4218,1276,3512,4868,4579,2307,3952,3544,651,1300,218,489,2837,3737,509,3421,879,4353,4695]
    fee1 = 655
    print(sol.maxProfit(input1, fee1))


```

## Top Discussions
### Most consistent ways of dealing with the series of stock problems
- Author: fun4LeetCode
- Creation Date: Sun Oct 22 2017 13:10:23 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 05:42:28 GMT+0800 (Singapore Standard Time)

<p>
Note: this is a repost of my original post [here](https://discuss.leetcode.com/topic/97310/most-consistent-ways-of-dealing-with-the-series-of-stock-problems) with updated solutions for this problem ([714. Best Time to Buy and Sell Stock with Transaction Fee](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/description/)). If you are only looking for solutions, you can go directly to each section in part **`II -- Applications to specific cases`**.

---
Up to this point, I believe you have finished the following series of stock problems: 
1. [121. Best Time to Buy and Sell Stock](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/#/description)
2. [122. Best Time to Buy and Sell Stock II](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/#/description)
3. [123. Best Time to Buy and Sell Stock III](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/#/description)
4. [188. Best Time to Buy and Sell Stock IV](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iv/#/description)
5. [309. Best Time to Buy and Sell Stock with Cooldown](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-cooldown/#/description)
6. [714. Best Time to Buy and Sell Stock with Transaction Fee](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/description/)

For each problem, we\'ve got a couple of excellent posts explaining how to approach it. However, most of the posts failed to identify the connections among these problems and made it hard to develop a consistent way of dealing with this series of problems. Here I will introduce the most generalized solution applicable to all of these problems, and its specialization to each of the six problems above.
 
---
**`I -- General cases`**

The idea begins with the following question: **Given an array representing the price of stocks on each day, what determines the maximum profit we can obtain?**

Most of you can quickly come up with answers like "it depends on which day we are and how many transactions we are allowed to complete". Sure, those are important factors as they manifest themselves in the problem descriptions. However, there is a hidden factor that is not so obvious but vital in determining the maximum profit, which is elaborated below.

First let\'s spell out the notations to streamline our analyses. Let `prices` be the stock price array with length `n`, `i` denote the `i-th` day (`i` will go from `0` to `n-1`), `k` denote the maximum number of transactions allowed to complete, `T[i][k]` be the maximum profit that could be gained at the end of the `i-th` day with at most `k` transactions. Apparently we have base cases: `T[-1][k] = T[i][0] = 0`, that is, no stock or no transaction yield no profit (note the first day has `i = 0` so `i = -1` means no stock). Now if we can somehow relate `T[i][k]` to its subproblems like `T[i-1][k], T[i][k-1], T[i-1][k-1], ...`, we will have a working recurrence relation and the problem can be solved recursively. So how do we achieve that?

The most straightforward way would be looking at actions taken on the `i-th` day. How many options do we have? The answer is three: **buy**, **sell**, **rest**. Which one should we take? The answer is: we don\'t really know, but to find out which one is easy. We can try each option and then choose the one that maximizes our profit, provided there are no other restrictions. However, we do have an extra restriction saying no multiple transactions are allowed at the same time, meaning if we decide to **buy** on the `i-th` day, there should be `0` stock held in our hand before we buy; if we decide to **sell** on the `i-th` day, there should be exactly `1` stock held in our hand before we sell. The number of stocks held in our hand is the hidden factor mentioned above that will affect the action on the `i-th` day and thus affect the maximum profit.

Therefore our definition of `T[i][k]` should really be split into two: `T[i][k][0]` and `T[i][k][1]`, where the **former** denotes the maximum profit at the end of the `i-th` day with at most `k` transactions and with `0` stock in our hand AFTER taking the action, while the **latter** denotes the maximum profit at the end of the `i-th` day with at most `k` transactions and with `1` stock in our hand AFTER taking the action. Now the base cases and the recurrence relations can be written as:

1. Base cases:
`T[-1][k][0] = 0, T[-1][k][1] = -Infinity`
`T[i][0][0] = 0, T[i][0][1] = -Infinity`

2. Recurrence relations:
`T[i][k][0] = max(T[i-1][k][0], T[i-1][k][1] + prices[i])`
`T[i][k][1] = max(T[i-1][k][1], T[i-1][k-1][0] - prices[i])`

For the base cases, `T[-1][k][0] = T[i][0][0] = 0` has the same meaning as before while `T[-1][k][1] = T[i][0][1] = -Infinity` emphasizes the fact that it is impossible for us to have `1` stock in hand if there is no stock available or no transactions are allowed.

For `T[i][k][0]` in the recurrence relations, the actions taken on the `i-th` day can only be **rest** and **sell**, since we have `0` stock in our hand at the end of the day. `T[i-1][k][0]` is the maximum profit if action **rest** is taken, while `T[i-1][k][1] + prices[i]` is the maximum profit if action **sell** is taken. Note that the maximum number of allowable transactions remains the same, due to the fact that a transaction consists of two actions coming as a pair -- **buy** and **sell**. Only action **buy** will change the maximum number of transactions allowed (well, there is actually an alternative interpretation, see my [comment](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/discuss/108870/Most-consistent-ways-of-dealing-with-the-series-of-stock-problems/111002) below).

For `T[i][k][1]` in the recurrence relations, the actions taken on the `i-th` day can only be **rest** and **buy**, since we have `1` stock in our hand at the end of the day. `T[i-1][k][1]` is the maximum profit if action **rest** is taken, while `T[i-1][k-1][0] - prices[i]` is the maximum profit if action **buy** is taken. Note that the maximum number of allowable transactions decreases by one, since buying on the `i-th` day will use one transaction, as explained above.

To find the maximum profit at the end of the last day, we can simply loop through the `prices` array and update `T[i][k][0]` and `T[i][k][1]` according to the recurrence relations above. The final answer will be `T[i][k][0]` (we always have larger profit if we end up with `0` stock in hand).

---
**`II -- Applications to specific cases`**

The aforementioned six stock problems are classified by the value of `k`, which is the maximum number of allowable transactions (the last two also have additional requirements such as "cooldown" or "transaction fee"). I will apply the general solution to each of them one by one.

<br>

**Case I: `k = 1`**

For this case, we really have two unknown variables on each day: `T[i][1][0]` and `T[i][1][1]`, and the recurrence relations say:

`T[i][1][0] = max(T[i-1][1][0], T[i-1][1][1] + prices[i])`
`T[i][1][1] = max(T[i-1][1][1], T[i-1][0][0] - prices[i]) = max(T[i-1][1][1], -prices[i])`

where we have taken advantage of the base case`T[i][0][0] = 0` for the second equation.

It is straightforward to write the `O(n)` time and `O(n)` space solution, based on the two equations above. However, if you notice that the maximum profits on the `i-th` day actually only depend on those on the `(i-1)-th` day, the space can be cut down to `O(1)`. Here is the space-optimized solution:

```
public int maxProfit(int[] prices) {
    int T_i10 = 0, T_i11 = Integer.MIN_VALUE;
        
    for (int price : prices) {
        T_i10 = Math.max(T_i10, T_i11 + price);
        T_i11 = Math.max(T_i11, -price);
    }
        
    return T_i10;
}
```
Now let\'s try to gain some insight of the solution above. If we examine the part inside the loop more carefully, `T_i11` really just represents the maximum value of the negative of all stock prices up to the `i-th` day, or equivalently the minimum value of all the stock prices. As for `T_i10`, we just need to decide which action yields a higher profit, sell or rest. And if action sell is taken, the price at which we bought the stock is `T_i11`, i.e., the minimum value before the `i-th` day. This is exactly what we would do in reality if we want to gain maximum profit. I should point out that this is not the only way of solving the problem for this case. You may find some other nice solutions [here](https://discuss.leetcode.com/topic/19853/kadane-s-algorithm-since-no-one-has-mentioned-about-this-so-far-in-case-if-interviewer-twists-the-input).

<br>

**Case II: `k = +Infinity`**

If `k` is positive infinity, then there isn\'t really any difference between `k` and `k - 1` (wonder why? see my [comment](https://discuss.leetcode.com/topic/107998/most-consistent-ways-of-dealing-with-the-series-of-stock-problems/9) below), which implies `T[i-1][k-1][0] = T[i-1][k][0]` and `T[i-1][k-1][1] = T[i-1][k][1]`. Therefore, we still have two unknown variables on each day: `T[i][k][0]` and `T[i][k][1]` with `k = +Infinity`, and the recurrence relations say:

`T[i][k][0] = max(T[i-1][k][0], T[i-1][k][1] + prices[i])`
`T[i][k][1] = max(T[i-1][k][1], T[i-1][k-1][0] - prices[i]) = max(T[i-1][k][1], T[i-1][k][0] - prices[i])`

where we have taken advantage of the fact that `T[i-1][k-1][0] = T[i-1][k][0]` for the second equation. The `O(n)` time and `O(1)` space solution is as follows:

```
public int maxProfit(int[] prices) {
    int T_ik0 = 0, T_ik1 = Integer.MIN_VALUE;
    
    for (int price : prices) {
        int T_ik0_old = T_ik0;
        T_ik0 = Math.max(T_ik0, T_ik1 + price);
        T_ik1 = Math.max(T_ik1, T_ik0_old - price);
    }
    
    return T_ik0;
}
```
(**Note**: The caching of the old values of `T_ik0`, that is, the variable `T_ik0_old`, is [unnecessary](https://discuss.leetcode.com/topic/107998/most-consistent-ways-of-dealing-with-the-series-of-stock-problems/24). Special thanks to [0x0101](https://discuss.leetcode.com/user/0x0101) and [elvina](https://discuss.leetcode.com/user/elvina) for clarifying this.)

This solution suggests a greedy strategy of gaining maximum profit: as long as possible, buy stock at each local minimum and sell at the immediately followed local maximum. This is equivalent to finding increasing subarrays in `prices` (the stock price array), and buying at the beginning price of each subarray while selling at its end price. It\'s easy to show that this is the same as accumulating profits as long as it is profitable to do so, as demonstrated in this [post](https://discuss.leetcode.com/topic/726/is-this-question-a-joke).

<br>

**Case III: `k = 2`**

Similar to the case where `k = 1`, except now we have four variables instead of two on each day: `T[i][1][0]`, `T[i][1][1]`, `T[i][2][0]`, `T[i][2][1]`, and the recurrence relations are:

`T[i][2][0] = max(T[i-1][2][0], T[i-1][2][1] + prices[i])`
`T[i][2][1] = max(T[i-1][2][1], T[i-1][1][0] - prices[i])`
`T[i][1][0] = max(T[i-1][1][0], T[i-1][1][1] + prices[i])`
`T[i][1][1] = max(T[i-1][1][1], -prices[i])`

where again we have taken advantage of the base case`T[i][0][0] = 0` for the last equation. The `O(n)` time and `O(1)` space solution is as follows:

```
public int maxProfit(int[] prices) {
    int T_i10 = 0, T_i11 = Integer.MIN_VALUE;
    int T_i20 = 0, T_i21 = Integer.MIN_VALUE;
        
    for (int price : prices) {
        T_i20 = Math.max(T_i20, T_i21 + price);
        T_i21 = Math.max(T_i21, T_i10 - price);
        T_i10 = Math.max(T_i10, T_i11 + price);
        T_i11 = Math.max(T_i11, -price);
    }
        
    return T_i20;
}
```
which is essentially the same as the one given [here](https://discuss.leetcode.com/topic/5934/is-it-best-solution-with-o-n-o-1).

<br>

**Case IV: `k is arbitrary`**

This is the most general case so on each day we need to update all the maximum profits with different `k` values corresponding to `0` or `1` stocks in hand at the end of the day. However, there is a minor optimization we can do if `k` exceeds some critical value, beyond which the maximum profit will no long depend on the number of allowable transactions but instead will be bound by the number of available stocks (length of the `prices` array). Let\'s figure out what this critical value will be.

A profitable transaction takes at least two days (buy at one day and sell at the other, provided the buying price is less than the selling price). If the length of the `prices` array is `n`, the maximum number of profitable transactions is `n/2` (integer division). After that no profitable transaction is possible, which implies the maximum profit will stay the same. Therefore the critical value of `k` is `n/2`. If the given `k` is no less than this value, i.e., `k >= n/2`, we can extend `k` to positive infinity and the problem is equivalent to **`Case II`**.

The following is the `O(kn)` time and `O(k)` space solution. Without the optimization, the code will be met with TLE for large `k` values.

```
public int maxProfit(int k, int[] prices) {
    if (k >= prices.length >>> 1) {
        int T_ik0 = 0, T_ik1 = Integer.MIN_VALUE;
    
        for (int price : prices) {
            int T_ik0_old = T_ik0;
            T_ik0 = Math.max(T_ik0, T_ik1 + price);
            T_ik1 = Math.max(T_ik1, T_ik0_old - price);
        }
        
        return T_ik0;
    }
        
    int[] T_ik0 = new int[k + 1];
    int[] T_ik1 = new int[k + 1];
    Arrays.fill(T_ik1, Integer.MIN_VALUE);
        
    for (int price : prices) {
        for (int j = k; j > 0; j--) {
            T_ik0[j] = Math.max(T_ik0[j], T_ik1[j] + price);
            T_ik1[j] = Math.max(T_ik1[j], T_ik0[j - 1] - price);
        }
    }
        
    return T_ik0[k];
}
```

The solution is similar to the one found in this [post](https://discuss.leetcode.com/topic/8984/a-concise-dp-solution-in-java). Here I used backward looping for the `T` array to avoid using temporary variables. It turns out that it is [possible](https://discuss.leetcode.com/topic/107998/most-consistent-ways-of-dealing-with-the-series-of-stock-problems/26) to do forward looping without temporary variables, too.

<br>

**Case V: `k = +Infinity but with cooldown`**

This case resembles **`Case II`** very much due to the fact that they have the same `k` value, except now the recurrence relations have to be modified slightly to account for the "**cooldown**" requirement. The original recurrence relations for **`Case II`** are given by

`T[i][k][0] = max(T[i-1][k][0], T[i-1][k][1] + prices[i])`
`T[i][k][1] = max(T[i-1][k][1], T[i-1][k][0] - prices[i])`

But with "cooldown", we cannot buy on the `i-th` day if a stock is sold on the `(i-1)-th` day. Therefore, in the second equation above, instead of `T[i-1][k][0]`, we should actually use `T[i-2][k][0]` if we intend to buy on the `i-th` day. Everything else remains the same and the new recurrence relations are

`T[i][k][0] = max(T[i-1][k][0], T[i-1][k][1] + prices[i])`
`T[i][k][1] = max(T[i-1][k][1], T[i-2][k][0] - prices[i])`

And here is the `O(n)` time and `O(1)` space solution:

```
public int maxProfit(int[] prices) {
    int T_ik0_pre = 0, T_ik0 = 0, T_ik1 = Integer.MIN_VALUE;
    
    for (int price : prices) {
        int T_ik0_old = T_ik0;
        T_ik0 = Math.max(T_ik0, T_ik1 + price);
        T_ik1 = Math.max(T_ik1, T_ik0_pre - price);
        T_ik0_pre = T_ik0_old;
    }
    
    return T_ik0;
}
```

[dietpepsi](https://discuss.leetcode.com/user/dietpepsi) shared a very nice solution [here](https://discuss.leetcode.com/topic/30421/share-my-thinking-process) with thinking process, which turns out to be the same as the one above.

<br>

**Case VI: `k = +Infinity but with transaction fee`**

Again this case resembles **`Case II`** very much as they have the same `k` value, except now the recurrence relations need to be modified slightly to account for the "**transaction fee**" requirement. The original recurrence relations for **`Case II`** are given by

`T[i][k][0] = max(T[i-1][k][0], T[i-1][k][1] + prices[i])`
`T[i][k][1] = max(T[i-1][k][1], T[i-1][k][0] - prices[i])`

Since now we need to pay some fee (denoted as `fee`) for each transaction made, the profit after buying or selling the stock on the `i-th` day should be subtracted by this amount, therefore the new recurrence relations will be either

`T[i][k][0] = max(T[i-1][k][0], T[i-1][k][1] + prices[i])`
`T[i][k][1] = max(T[i-1][k][1], T[i-1][k][0] - prices[i] - fee)`

or

`T[i][k][0] = max(T[i-1][k][0], T[i-1][k][1] + prices[i] - fee)`
`T[i][k][1] = max(T[i-1][k][1], T[i-1][k][0] - prices[i])`

Note we have two options as for when to subtract the `fee`. This is because (as I mentioned above) each transaction is characterized by two actions coming as a pair - - **buy** and **sell**. The fee can be paid either when we buy the stock (corresponds to the first set of equations) or when we sell it (corresponds to the second set of equations). The following are the `O(n)` time and `O(1)` space solutions corresponding to these two options, where for the second solution we need to pay attention to possible overflows.


**Solution I** -- pay the fee when buying the stock:
```
public int maxProfit(int[] prices, int fee) {
    int T_ik0 = 0, T_ik1 = Integer.MIN_VALUE;
    
    for (int price : prices) {
        int T_ik0_old = T_ik0;
        T_ik0 = Math.max(T_ik0, T_ik1 + price);
        T_ik1 = Math.max(T_ik1, T_ik0_old - price - fee);
    }
        
    return T_ik0;
}
```

**Solution II** -- pay the fee when selling the stock:
 
```
public int maxProfit(int[] prices, int fee) {
    long T_ik0 = 0, T_ik1 = Integer.MIN_VALUE;
    
    for (int price : prices) {
        long T_ik0_old = T_ik0;
        T_ik0 = Math.max(T_ik0, T_ik1 + price - fee);
        T_ik1 = Math.max(T_ik1, T_ik0_old - price);
    }
        
    return (int)T_ik0;
}
```
---
**`III -- Summary`**

In summary, the most general case of the stock problem can be characterized by three factors, the ordinal of the day `i`, the maximum number of allowable transactions `k`, and the number of stocks in our hand at the end of the day. I have shown the recurrence relations for the maximum profits and their termination conditions, which leads to the `O(nk)` time and `O(k)` space solution. The results are then applied to each of the six cases, with the last two using slightly modified recurrence relations due to the additional requirements. I should mention that [peterleetcode](https://discuss.leetcode.com/user/peterleetcode) also introduced a nice solution [here](https://discuss.leetcode.com/topic/4766/a-clean-dp-solution-which-generalizes-to-k-transactions) which generalizes to arbitrary `k` values. If you have a taste, take a look.

Hope this helps and happy coding!
</p>


### 2 solutions, 2 states DP solutions, clear explanation!
- Author: Joy4fun
- Creation Date: Thu Oct 26 2017 11:27:22 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 02:14:22 GMT+0800 (Singapore Standard Time)

<p>
Given any <strong><code>day I</strong></code>, its max profit status boils down to one of the two status below:

**(1) buy status**:
<strong><code>buy[i]</strong></code> represents the **max profit** at <strong><code>day i</strong></code> in **buy status**, given that **the last action you took is a buy action** at <strong><code>day K</strong></code>, where <strong><code>K<=i</strong></code>. **And you have the right to sell at <strong><code>day i+1</strong></code>, or do nothing.**
**(2) sell status**:
<strong><code>sell[i]</strong></code> represents the **max profit** at <strong><code>day i</strong></code> in **sell status**, given that **the last action you took is a sell action** at <strong><code>day K</strong></code>, where <strong><code>K<=i</strong></code>. **And you have the right to buy at <strong><code>day i+1</strong></code>, or do nothing.**

Let\'s walk through from **base case**.

**Base case:**
We can start from **buy status**, which means we buy stock at <strong><code>day 0</strong></code>.
<strong><code>buy[0]=-prices[0]</strong></code>;
Or we can start from **sell status**, which means we sell stock at <strong><code>day 0</strong></code>. 
Given that we don\'t have any stock at hand in day 0, we set sell status to be 0.
<strong><code>sell[0]=0</strong></code>;	      

**Status transformation:**
At <strong><code>day i</strong></code>, we may **buy** stock (from **previous sell status**) or do nothing (from **previous buy status**):
<strong><code>buy[i] = Math.max(buy[i - 1], sell[i - 1] - prices[i]);</strong></code>
Or
At <strong><code>day i</strong></code>, we may **sell** stock (from previous **buy status**) or keep holding (from **previous sell status**):
<strong><code>sell[i] = Math.max(sell[i - 1], buy[i - 1] + prices[i]); </strong></code>

**Finally:**
We will return <strong><code>sell[last_day]</strong></code> as our result, which represents the max profit at the last day, given that you took sell action at any day before the last day.

**We can apply transaction fee at either buy status or sell status.**  




So here come our two solutions:

**Solution I** -- pay the fee when buying the stock:

    public int maxProfit(int[] prices, int fee) {
	        if (prices.length <= 1) return 0;
	        int days = prices.length, buy[] = new int[days], sell[] = new int[days];
	        buy[0]=-prices[0]-fee;
	        for (int i = 1; i<days; i++) {
	            buy[i] = Math.max(buy[i - 1], sell[i - 1] - prices[i] - fee); // keep the same as day i-1, or buy from sell status at day i-1
	            sell[i] = Math.max(sell[i - 1], buy[i - 1] + prices[i]); // keep the same as day i-1, or sell from buy status at day i-1
	        }
	        return sell[days - 1];
	    }

**Solution II** -- pay the fee when selling the stock:

        public int maxProfit(int[] prices, int fee) {
	        if (prices.length <= 1) return 0;
	        int days = prices.length, buy[] = new int[days], sell[] = new int[days];
	        buy[0]=-prices[0];
	        for (int i = 1; i<days; i++) {
	            buy[i] = Math.max(buy[i - 1], sell[i - 1] - prices[i]); // keep the same as day i-1, or buy from sell status at day i-1
	            sell[i] = Math.max(sell[i - 1], buy[i - 1] + prices[i] - fee); // keep the same as day i-1, or sell from buy status at day i-1
	        }
	        return sell[days - 1];
	    }
</p>


### C++, concise solution, O(n) time O(1) space
- Author: zestypanda
- Creation Date: Sun Oct 22 2017 12:16:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Oct 14 2018 13:53:14 GMT+0800 (Singapore Standard Time)

<p>
The solution maintains two states:
```
s0 = profit having no stock
s1 = profit having 1 stock
```
The code iterates through the stock prices, and updates s0, s1 respectively. The run time is O(n).
```
update s0 by selling the stock from s1, so s0 = max(s0, s1+p);
update s1 by buying the stock from s0, so s1 = max(s1, s0-p-fee);
```
```
class Solution {
public:
    int maxProfit(vector<int>& prices, int fee) {
        int s0 = 0, s1 = INT_MIN; 
        for(int p:prices) {
            int tmp = s0;
            s0 = max(s0, s1+p);
            s1 = max(s1, tmp-p-fee);
        }
        return s0;
    }
};
</p>


