---
title: "Divide Array Into Increasing Sequences"
weight: 1025
#id: "divide-array-into-increasing-sequences"
---
## Description
<div class="description">
<p>Given a <strong>non-decreasing</strong> array of positive integers&nbsp;<code>nums</code>&nbsp;and an integer <code>K</code>, find out if this array can be divided into one or more <strong>disjoint increasing subsequences of length at least</strong> <code>K</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>nums = <span id="example-input-1-1">[1,2,2,3,3,4,4]</span>, K = <span id="example-input-1-2">3</span>
<strong>Output: </strong><span id="example-output-1">true</span>
<strong>Explanation: </strong>
The array can be divided into the two subsequences [1,2,3,4] and [2,3,4] with lengths at least 3 each.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>nums = <span id="example-input-2-1">[5,6,6,7,8]</span>, K = <span id="example-input-2-2">3</span>
<strong>Output: </strong><span id="example-output-2">false</span>
<strong>Explanation: </strong>
There is no way to divide the array using the conditions required.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li><code>1 &lt;= nums.length&nbsp;&lt;= 10^5</code></li>
	<li><code>1 &lt;= K &lt;= nums.length</code></li>
	<li><code>1 &lt;= nums[i] &lt;= 10^5</code></li>
</ol>

</div>

## Tags
- Math (math)

## Companies
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] One Pass, O(1) Space and Prove
- Author: lee215
- Creation Date: Sun Jul 14 2019 00:01:30 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 14 2019 00:23:26 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
For example 2: `A = [5,6,6,7,8]`
we have two 6, so we have to split `A` into at least two subsequence.
We want `K = 3` numbers in each subsequence, so we need at least  `K * 2 = 6` numbers.
But we have only `A.length = 5` numbers.
<br>

## **Explanation**
So the idea is that, find the maximum quantity of same numbers in `A`.
As `A` is sorted already, we can do this in one pass and `O(1)` space.
`cur` is the current length of same number until `A[i]`.
If `A[i - 1] < A[i]`, we reset `cur = 1`. Otherwise increment `cur = cur + 1`.

If `n < K * groups`, it\'s impossible to satisfy the condition, we return `false`.
Otherwise, we have enough numbers in hand and we can easily meet the requirement:
<br>

## Prove
Provide a way to split here:
1. Find the number of `groups` as in the solution, assume it equals to `M`
2. Assign `A[i]` to `groups[i % M]`.
3. Done.
<br>

## **Complexity**
Time `O(N)` for one pass, and you can return false earlier.
Space `O(1)` for no extra space.
<br>

**Java:**
```java
    public boolean canDivideIntoSubsequences(int[] A, int K) {
        int cur = 1, groups = 1, n = A.length;
        for (int i = 1; i < n; ++i) {
            cur = A[i - 1] < A[i] ?  1 : cur + 1;
            groups = Math.max(groups, cur);
        }
        return n >= K * groups;
    }
```

**C++:**
```cpp
    bool canDivideIntoSubsequences(vector<int>& A, int K) {
        int cur = 1, groups = 1, n = A.size();
        for (int i = 1; i < n; ++i) {
            cur = A[i - 1] <  A[i] ? 1 : cur + 1;
            groups = max(groups, cur);
        }
        return n >= K * groups;
    }
```

**Python, 1 line, O(N) space:**
```python
    def canDivideIntoSubsequences(self, A, K):
        return len(A) >= K * max(collections.Counter(A).values())
```

</p>


### [Java] 6 liner and 7 liner w/ explanation and complexity analysis.
- Author: rock
- Creation Date: Sun Jul 14 2019 00:01:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 14 2019 02:20:26 GMT+0800 (Singapore Standard Time)

<p>
1. Loop through the input array to count the frequency of the mode (most frequent number), say, `modeFreq`;
2. At least `modeFreq` sequences needed to avoid duplicates in any sequence;
3. We can always distribute all elements to the above sequences, minimum of which has `nums.length / modeFreq` elements.

```
    public boolean canDivideIntoSubsequences(int[] nums, int K) {
        int modeFreq = 0;
        int[] cnt = new int[100_001];
        for (int n : nums)
            if (++cnt[n] > modeFreq)
                modeFreq = cnt[n];
        return nums.length / modeFreq >= K;
    }
```
Time: O(n). space: O(100001).

---

**Method 2:**
Get rid of the counting array to save space; count the consecutively equal elements, get the max out of them:

**Note:** `prev` is initialized with a dummy value `-1` to make sure it is not equal to `nums[0]`.
```
    public boolean canDivideIntoSubsequences(int[] nums, int K) {
        int modeFreq = 1,prev = -1, cnt = 1;
        for (int cur : nums) {
            cnt = prev == cur ? ++cnt : 1;
            prev = cur; 
            modeFreq = Math.max(cnt, modeFreq);
        }
        return nums.length / modeFreq >= K;
    }
```
Time: O(n), space: O(1).
</p>


### Python Solution with Explanation
- Author: davyjing
- Creation Date: Sun Jul 14 2019 00:14:03 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jul 14 2019 00:16:29 GMT+0800 (Singapore Standard Time)

<p>
The problem is equivalent to seperating the list into several sets such that in each set there is no duplicated elements.

```
class Solution:
    def canDivideIntoSubsequences(self, nums: List[int], K: int) -> bool:
        count = collections.Counter(nums)
        c = max(count.values())
        return K*c <= len(nums)
```
</p>


