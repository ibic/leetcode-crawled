---
title: "Design Twitter"
weight: 338
#id: "design-twitter"
---
## Description
<div class="description">
<p>Design a simplified version of Twitter where users can post tweets, follow/unfollow another user and is able to see the 10 most recent tweets in the user's news feed. Your design should support the following methods:</p>

<p>
<ol>
<li><b>postTweet(userId, tweetId)</b>: Compose a new tweet.</li>
<li><b>getNewsFeed(userId)</b>: Retrieve the 10 most recent tweet ids in the user's news feed. Each item in the news feed must be posted by users who the user followed or by the user herself. Tweets must be ordered from most recent to least recent.</li>
<li><b>follow(followerId, followeeId)</b>: Follower follows a followee.</li>
<li><b>unfollow(followerId, followeeId)</b>: Follower unfollows a followee.</li>
</ol>
</p>

<p><b>Example:</b>
<pre>
Twitter twitter = new Twitter();

// User 1 posts a new tweet (id = 5).
twitter.postTweet(1, 5);

// User 1's news feed should return a list with 1 tweet id -> [5].
twitter.getNewsFeed(1);

// User 1 follows user 2.
twitter.follow(1, 2);

// User 2 posts a new tweet (id = 6).
twitter.postTweet(2, 6);

// User 1's news feed should return a list with 2 tweet ids -> [6, 5].
// Tweet id 6 should precede tweet id 5 because it is posted after tweet id 5.
twitter.getNewsFeed(1);

// User 1 unfollows user 2.
twitter.unfollow(1, 2);

// User 1's news feed should return a list with 1 tweet id -> [5],
// since user 1 is no longer following user 2.
twitter.getNewsFeed(1);
</pre>
</p>
</div>

## Tags
- Hash Table (hash-table)
- Heap (heap)
- Design (design)

## Companies
- Huawei - 2 (taggedByAdmin: false)
- Yelp - 5 (taggedByAdmin: false)
- Oracle - 2 (taggedByAdmin: false)
- DoorDash - 6 (taggedByAdmin: false)
- Twitter - 0 (taggedByAdmin: true)
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java OO Design with most efficient function getNewsFeed
- Author: UpTheHell
- Creation Date: Tue Jun 14 2016 10:24:45 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 00:24:23 GMT+0800 (Singapore Standard Time)

<p>
```java
public class Twitter {
	private static int timeStamp=0;

	// easy to find if user exist
	private Map<Integer, User> userMap;

	// Tweet link to next Tweet so that we can save a lot of time
	// when we execute getNewsFeed(userId)
	private class Tweet{
		public int id;
		public int time;
		public Tweet next;

		public Tweet(int id){
			this.id = id;
			time = timeStamp++;
			next=null;
		}
	}


	// OO design so User can follow, unfollow and post itself
	public class User{
		public int id;
		public Set<Integer> followed;
		public Tweet tweet_head;

		public User(int id){
			this.id=id;
			followed = new HashSet<>();
			follow(id); // first follow itself
			tweet_head = null;
		}

		public void follow(int id){
			followed.add(id);
		}

		public void unfollow(int id){
			followed.remove(id);
		}


		// everytime user post a new tweet, add it to the head of tweet list.
		public void post(int id){
			Tweet t = new Tweet(id);
			t.next=tweet_head;
			tweet_head=t;
		}
	}




	/** Initialize your data structure here. */
	public Twitter() {
		userMap = new HashMap<Integer, User>();
	}

	/** Compose a new tweet. */
	public void postTweet(int userId, int tweetId) {
		if(!userMap.containsKey(userId)){
			User u = new User(userId);
			userMap.put(userId, u);
		}
		userMap.get(userId).post(tweetId);

	}



	// Best part of this.
	// first get all tweets lists from one user including itself and all people it followed.
	// Second add all heads into a max heap. Every time we poll a tweet with 
	// largest time stamp from the heap, then we add its next tweet into the heap.
	// So after adding all heads we only need to add 9 tweets at most into this 
	// heap before we get the 10 most recent tweet.
	public List<Integer> getNewsFeed(int userId) {
		List<Integer> res = new LinkedList<>();

		if(!userMap.containsKey(userId))   return res;

		Set<Integer> users = userMap.get(userId).followed;
		PriorityQueue<Tweet> q = new PriorityQueue<Tweet>(users.size(), (a,b)->(b.time-a.time));
		for(int user: users){
			Tweet t = userMap.get(user).tweet_head;
			// very imporant! If we add null to the head we are screwed.
			if(t!=null){
				q.add(t);
			}
		}
		int n=0;
		while(!q.isEmpty() && n<10){
		  Tweet t = q.poll();
		  res.add(t.id);
		  n++;
		  if(t.next!=null)
			q.add(t.next);
		}

		return res;

	}

	/** Follower follows a followee. If the operation is invalid, it should be a no-op. */
	public void follow(int followerId, int followeeId) {
		if(!userMap.containsKey(followerId)){
			User u = new User(followerId);
			userMap.put(followerId, u);
		}
		if(!userMap.containsKey(followeeId)){
			User u = new User(followeeId);
			userMap.put(followeeId, u);
		}
		userMap.get(followerId).follow(followeeId);
	}

	/** Follower unfollows a followee. If the operation is invalid, it should be a no-op. */
	public void unfollow(int followerId, int followeeId) {
		if(!userMap.containsKey(followerId) || followerId==followeeId)
			return;
		userMap.get(followerId).unfollow(followeeId);
	}
}

/**
 * Your Twitter object will be instantiated and called as such:
 * Twitter obj = new Twitter();
 * obj.postTweet(userId,tweetId);
 * List<Integer> param_2 = obj.getNewsFeed(userId);
 * obj.follow(followerId,followeeId);
 * obj.unfollow(followerId,followeeId);
 */
```
</p>


### Python solution
- Author: StefanPochmann
- Creation Date: Sat Jun 11 2016 22:47:10 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 14:36:56 GMT+0800 (Singapore Standard Time)

<p>
    class Twitter(object):
    
        def __init__(self):
            self.timer = itertools.count(step=-1)
            self.tweets = collections.defaultdict(collections.deque)
            self.followees = collections.defaultdict(set)
    
        def postTweet(self, userId, tweetId):
            self.tweets[userId].appendleft((next(self.timer), tweetId))
    
        def getNewsFeed(self, userId):
            tweets = heapq.merge(*(self.tweets[u] for u in self.followees[userId] | {userId}))
            return [t for _, t in itertools.islice(tweets, 10)]
    
        def follow(self, followerId, followeeId):
            self.followees[followerId].add(followeeId)
    
        def unfollow(self, followerId, followeeId):
            self.followees[followerId].discard(followeeId)
</p>


### Java OOD solution with detailed explanation
- Author: isabel2
- Creation Date: Sun Jun 19 2016 03:02:08 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 06:25:27 GMT+0800 (Singapore Standard Time)

<p>
OOD design:
data structure need in the Tweet system:

	1. A data structure that save the following relationship
	2. A data structure that save the tweets posted 

Based on the requirement of method 3: we should get our followees' tweets and select the most recent 10 tweet. So there should have a timestamp inside the tweet. So we create a new class to represent a tweet

	3. A class Tweet containing timestamp


There are some tips in the system:

	1. One should get the tweets of itself, which means the followee must contain itself
	2. Since the followee must contains itself, it cannot unfollow itself(unfollow add this constraint)
	3. The followees must be identical
According to the analysis above, we have these data struture in this class:

	1. A inner class Tweet(tweetId, timePosted)
	2. A HashMap(follower, Set(followees))
	3. A HashMap(UserId, List(Tweet))
	4. A Static int timeStamp
	5. A int Maximum number of feed(can adjust if needed, optional)

For method 3, just simply use min heap to get the most recent 10.

    public class Twitter {
    
    private static class Tweet{
        int tweetId;
        int timePosted;
        public Tweet(int tId, int time){
            tweetId = tId;
            timePosted = time;
        }
    }
    
    static int timeStamp;
    int feedMaxNum;
    Map<Integer, Set<Integer>> followees;
    Map<Integer, List<Tweet>> tweets;
    
    /** Initialize your data structure here. */
    public Twitter() {
        timeStamp = 0;
        feedMaxNum = 10;
        followees = new HashMap<>();
        tweets = new HashMap<>();
    }
    
    /** Compose a new tweet. */
    public void postTweet(int userId, int tweetId) {
        if(!tweets.containsKey(userId)) {
            tweets.put(userId, new LinkedList<Tweet>());
            follow(userId, userId);  //follow itself
        }
        tweets.get(userId).add(0, new Tweet(tweetId, timeStamp++)); //add new tweet on the first place
    }
    
    /** Retrieve the 10 most recent tweet ids in the user's news feed. Each item in the news feed must be posted by users who the user followed or by the user herself. Tweets must be ordered from most recent to least recent. */
    public List<Integer> getNewsFeed(int userId) {
        //min heap that the earliest tweet is on the top
        PriorityQueue<Tweet> feedHeap = new PriorityQueue<>(new Comparator<Tweet>(){
            public int compare(Tweet t1, Tweet t2){
                return t1.timePosted - t2.timePosted;
            }
        });

        //add tweets of the followees
        Set<Integer> myFollowees = followees.get(userId);
        if(myFollowees != null){
            for(int followeeId : myFollowees){
                List<Tweet> followeeTweets = tweets.get(followeeId);
                if(followeeTweets == null) continue;
                for(Tweet t : followeeTweets){
                    if(feedHeap.size() < feedMaxNum) feedHeap.add(t);
                    else{
                        if(t.timePosted <= feedHeap.peek().timePosted) break;
                        else{
                            feedHeap.add(t);
                            feedHeap.poll(); //remove the oldest tweet
                        }
                    }
                }
            }
        }
        List<Integer> myFeed = new LinkedList<>();
        while(!feedHeap.isEmpty()){
            myFeed.add(0, feedHeap.poll().tweetId);
        }
        return myFeed;
    }
    
    /** Follower follows a followee. If the operation is invalid, it should be a no-op. */
    public void follow(int followerId, int followeeId) {
        if(!followees.containsKey(followerId)) followees.put(followerId, new HashSet<Integer>());
        followees.get(followerId).add(followeeId);
    }
    
    /** Follower unfollows a followee. If the operation is invalid, it should be a no-op. */
    public void unfollow(int followerId, int followeeId) {
        if(!followees.containsKey(followerId) || followerId == followeeId) return; //cannot unfollow itself
        followees.get(followerId).remove(followeeId);
    }
}
</p>


