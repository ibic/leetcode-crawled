---
title: "Insertion Sort List"
weight: 147
#id: "insertion-sort-list"
---
## Description
<div class="description">
<p>Sort a linked list using insertion sort.</p>

<ol>
</ol>

<p><img alt="" src="https://upload.wikimedia.org/wikipedia/commons/0/0f/Insertion-sort-example-300px.gif" style="height:180px; width:300px" /><br />
<small>A graphical example of insertion sort. The partial sorted list (black) initially contains only the first element in the list.<br />
With each iteration one element (red) is removed from the input data and inserted in-place into the sorted list</small><br />
&nbsp;</p>

<ol>
</ol>

<p><strong>Algorithm of Insertion Sort:</strong></p>

<ol>
	<li>Insertion sort iterates, consuming one input element each repetition, and growing a sorted output list.</li>
	<li>At each iteration, insertion sort removes one element from the input data, finds the location it belongs within the sorted list, and inserts it there.</li>
	<li>It repeats until no input elements remain.</li>
</ol>

<p><br />
<strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> 4-&gt;2-&gt;1-&gt;3
<strong>Output:</strong> 1-&gt;2-&gt;3-&gt;4
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> -1-&gt;5-&gt;3-&gt;4-&gt;0
<strong>Output:</strong> -1-&gt;0-&gt;3-&gt;4-&gt;5
</pre>

</div>

## Tags
- Linked List (linked-list)
- Sort (sort)

## Companies
- Microsoft - 3 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---
#### Overview


[Insertion sort](https://en.wikipedia.org/wiki/Insertion_sort) is an intuitive sorting algorithm, although it is much less efficient than the more advanced algorithms such as quicksort or merge sort.

Often that we perform the sorting algorithm on an [Array](https://leetcode.com/explore/learn/card/fun-with-arrays) structure,
this problem though asks us to perform the insertion sort on a **linked list** data structure, which makes the implementation a bit challenging.

In this article, we will present some tricks to manipulate the linked list, which would help us to simplify the logics of implementation.


---
#### Approach 1: Insertion Sort

**Intuition**

Let us first review the idea of insertion sort algorithm, which can be broke down into the following steps:

- First of all, we create an empty list which would be used to hold the results of sorting.

- We then iterate through each element in the _input_ list. For each element, we need to find a proper position in the resulting list to insert the element, so that the order of the resulting list is maintained.

- As one can see, once the iteration in the above step terminates, we will obtain the resulting list where the elements are _ordered_.

Now, let us walk through a simple example, by applying the above intuition.

Given the input list `input=[4, 3, 5]`, we have initially an empty resulting list `result=[]`.

- We then iterate over the input list. For the first element `4`, we need to find a proper position in the resulting list to place it.
Since the resulting list is still empty, we then simply _append_ it to the resulting list, _i.e._ `result=[4]`.

![step 1](../Figures/147/147_linked_list_step_1.png)

- Now for the second element (_i.e._ `3`) in the input list, similarly we need to insert it properly into the resulting list.
As one can see, we need to insert it right before the element `4`.
As a result, the resulting list becomes `[3, 4]`.

![step 2](../Figures/147/147_linked_list_step_2.png)

- Finally, for the last element (_i.e._ `5`) in the input list, as it turns out, the proper position to place it is the _tail_ of the resulting list.
With this last iteration, we obtain a _sorted_ list as `result=[3, 4, 5]`.

![step 3](../Figures/147/147_linked_list_step_3.png)


**Algorithm**

To translate the above intuition into the implementation, we applied two **tricks**.

>The first trick is that we will create a `pseudo_head` node which serves as a pointer pointing to the resulting list.

More precisely, this node facilitates us to always get a _hold_ on the resulting list, especially when we need to insert a new element to the head of the resulting list.
One will see later in more details how it can greatly simplify the logic.

In a _singly-linked list_, each node has only one pointer that points to the next node.
If we would like to insert a new node (say `B`) before certain node (say `A`), we need to know the node (say `C`) that is currently before the node `A`, _i.e._ `C -> A`.
With the reference in the node `C`, we could now insert the new node, _i.e._ `C -> B -> A`.

Given the above insight, in order to insert a new element into a singly-linked list, we apply another trick.

>The idea is that we use a _**pair of pointers**_ (namely `prev_node -> next_node`) which serve as place-holders to guard the position where in-between we would insert a new element (_i.e._ `prev_node -> new_node -> next_node`).

With the same example before, _i.e._ `input=[4, 3, 5]`, we illustrate what the above helper pointers look like at the moment of insertion, in the following graph:

![pointers](../Figures/147/147_pointers.png)

Here are some sample implementations based on the above ideas:

<iframe src="https://leetcode.com/playground/hETXDyUV/shared" frameBorder="0" width="100%" height="500" name="hETXDyUV"></iframe>



**Complexity Analysis**

Let $$N$$ be the number of elements in the input list.

- Time Complexity: $$\mathcal{O}(N^2)$$ 

    - First of all, we run an iteration over the input list.

    - At each iteration, we insert an element into the resulting list. In the worst case where the position to insert is the tail of the list, we have to walk through the entire resulting list.

    - As a result, the total steps that we need to walk in the worst case would be $$\sum_{i=1}^{N} i = \frac{N(N+1)}{2}$$.

    - To sum up, the overall time complexity of the algorithm is $$\mathcal{O}(N^2)$$.


- Space Complexity: $$\mathcal{O}(1)$$

    - We used some pointers within the algorithm. However, their memory consumption is constant regardless of the input.

    - **Note**, we did not create new nodes to hold the values of input list, but simply _reorder_ the existing nodes.


---

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### An easy and clear way to sort ( O(1) space )
- Author: mingzixiecuole
- Creation Date: Sun Feb 08 2015 07:43:30 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 06:39:00 GMT+0800 (Singapore Standard Time)

<p>
    public ListNode insertionSortList(ListNode head) {
    		if( head == null ){
    			return head;
    		}
    		
    		ListNode helper = new ListNode(0); //new starter of the sorted list
    		ListNode cur = head; //the node will be inserted
    		ListNode pre = helper; //insert node between pre and pre.next
    		ListNode next = null; //the next node will be inserted
    		//not the end of input list
    		while( cur != null ){
    			next = cur.next;
    			//find the right place to insert
    			while( pre.next != null && pre.next.val < cur.val ){
    				pre = pre.next;
    			}
    			//insert between pre and pre.next
    			cur.next = pre.next;
    			pre.next = cur;
    			pre = helper;
    			cur = next;
    		}
    		
    		return helper.next;
    	}
</p>


### Thoughts from a Google interviewer
- Author: Han_V
- Creation Date: Sun Mar 29 2015 07:31:13 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 05 2018 00:43:05 GMT+0800 (Singapore Standard Time)

<p>
One of the quotes is 

> For God's sake, don't try sorting a linked list during the interview

http://steve-yegge.blogspot.nl/2008/03/get-that-job-at-google.html

So it might be better to actually copy the values into an array and sort them there.
</p>


### Explained C++ solution (24ms)
- Author: jianchao-li
- Creation Date: Wed May 27 2015 23:45:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 22:22:46 GMT+0800 (Singapore Standard Time)

<p>
Keep a sorted partial list (`head`) and start from the second node (`head -> next`), each time when we see a node with `val` smaller than its previous node, we scan from the `head` and find the position that the node should be inserted. Since a node may be inserted before `head`, we create a `dummy` head that points to `head`.

```cpp
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* insertionSortList(ListNode* head) {
        ListNode* dummy = new ListNode(0);
        dummy -> next = head;
        ListNode *pre = dummy, *cur = head;
        while (cur) {
            if ((cur -> next) && (cur -> next -> val < cur -> val)) {
                while ((pre -> next) && (pre -> next -> val < cur -> next -> val)) {
                    pre = pre -> next;
                }
                ListNode* temp = pre -> next;
                pre -> next = cur -> next;
                cur -> next = cur -> next -> next;
                pre -> next -> next = temp;
                pre = dummy;
            }
            else {
                cur = cur -> next;
            }
        }
        return dummy -> next;
    }
};
```
</p>


