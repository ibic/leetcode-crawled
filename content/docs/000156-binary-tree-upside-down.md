---
title: "Binary Tree Upside Down"
weight: 156
#id: "binary-tree-upside-down"
---
## Description
<div class="description">
<p>Given the <code>root</code> of a binary tree, turn the tree upside down and return <em>the new root</em>.</p>

<p>You can turn a binary tree upside down with the following steps:</p>

<ol>
	<li>The original left child becomes the new root.</li>
	<li>The original root becomes the new right child.</li>
	<li>The original right child&nbsp;becomes the new left child.</li>
</ol>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/08/29/main.jpg" style="width: 100%; height: 100%;" /></p>

<p>The mentioned steps are done level by level, it is <strong>guaranteed</strong> that every node in the given tree has either <strong>0 or 2 children</strong>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/08/29/updown.jpg" style="width: 800px; height: 161px;" />
<pre>
<strong>Input:</strong> root = [1,2,3,4,5]
<strong>Output:</strong> [4,5,2,null,null,3,1]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> root = []
<strong>Output:</strong> []
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> root = [1]
<strong>Output:</strong> [1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li>The number of nodes in the tree will be in the range <code>[0, 10]</code>.</li>
	<li><code>1 &lt;= Node.val &lt;= 10</code></li>
	<li><code>Every node has either 0 or 2 children.</code></li>
</ul>

</div>

## Tags
- Tree (tree)

## Companies
- LinkedIn - 4 (taggedByAdmin: true)
- Google - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java recursive (O(logn) space) and iterative solutions (O(1) space) with explanation and figure
- Author: yfcheng
- Creation Date: Sat Mar 26 2016 04:40:39 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Sep 30 2020 07:12:11 GMT+0800 (Singapore Standard Time)

<p>
This is not a very intuitive problem for me, I have to spent quite a while drawing figures to understand it.  As shown in the figure, 1 shows the original tree, you can think about it as a comb, with 1, 2, 4 form the bone, and 3, 5 as the teeth.  All we need to do is flip the teeth direction as shown in figure 2.  We will remove the link 1--3, 2--5, and add link 2--3, and 4--5.  And node 4 will be the new root.  

As the recursive solution, we will keep recurse on the left child and once we are are done, we found the newRoot, which is 4 for this case.  At this point, we will need to set the new children for node 2, basically the new left node is 3, and right node is 1.  Here is the recursive solution:


![image](https://assets.leetcode.com/users/images/6fca1ddd-2791-444a-9f94-75b4a49913f9_1601421082.2188914.png)



Recursive:

    public TreeNode upsideDownBinaryTree(TreeNode root) {
        if(root == null || root.left == null) {
            return root;
        }
        
        TreeNode newRoot = upsideDownBinaryTree(root.left);
        root.left.left = root.right;   // node 2 left children
        root.left.right = root;         // node 2 right children
        root.left = null;
        root.right = null;
        return newRoot;
    }

For the iterative solution, it follows the same thought, the only thing we need to pay attention to is to save the node information that will be overwritten.  

![image](https://assets.leetcode.com/users/images/cc5350ec-0158-4582-9163-fcbe37dd5cd6_1601421096.4278862.png)



    public TreeNode upsideDownBinaryTree(TreeNode root) {
        TreeNode curr = root;
        TreeNode next = null;
        TreeNode temp = null;
        TreeNode prev = null;
        
        while(curr != null) {
            next = curr.left;
            
            // swapping nodes now, need temp to keep the previous right child
            curr.left = temp;
            temp = curr.right;
            curr.right = prev;
            
            prev = curr;
            curr = next;
        }
        return prev;
    }  

</p>


### Clean Java solution
- Author: jeantimex
- Creation Date: Fri Jul 10 2015 04:32:11 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 13:37:40 GMT+0800 (Singapore Standard Time)

<p>
    public TreeNode upsideDownBinaryTree(TreeNode root) {
      if (root == null || root.left == null && root.right == null)
        return root;
    
      TreeNode newRoot = upsideDownBinaryTree(root.left);
      
      root.left.left = root.right;
      root.left.right = root;
      
      root.left = null;
      root.right = null;
          
      return newRoot;
    }
</p>


### Easy O(n) iteration solution [Java]
- Author: Yida
- Creation Date: Sat Dec 13 2014 03:37:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Dec 13 2014 03:37:08 GMT+0800 (Singapore Standard Time)

<p>
    public class Solution {
        public TreeNode UpsideDownBinaryTree(TreeNode root) {
            TreeNode curr = root;
            TreeNode prev = null;
            TreeNode next = null;
            TreeNode temp = null;
            
            while (curr != null) {
                next = curr.left;
                curr.left = temp;
                temp = curr.right;
                curr.right = prev;
                prev = curr;
                curr = next;
            }
            
            return prev;
        }
    }

    Just think about how you can save the tree information 
    you need before changing the tree structure.
</p>


