---
title: "Insufficient Nodes in Root to Leaf Paths"
weight: 1059
#id: "insufficient-nodes-in-root-to-leaf-paths"
---
## Description
<div class="description">
<p>Given the <code>root</code>&nbsp;of a binary tree, consider all <em>root to leaf paths</em>: paths from the root&nbsp;to any leaf.&nbsp; (A leaf is a node with no children.)</p>

<p>A <code>node</code> is <em>insufficient</em> if&nbsp;<strong>every</strong> such root to leaf path intersecting this <code>node</code> has sum strictly less than&nbsp;<code>limit</code>.</p>

<p>Delete all insufficient nodes simultaneously, and return the root of the resulting&nbsp;binary tree.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong><img alt="" src="https://assets.leetcode.com/uploads/2019/06/05/insufficient-11.png" style="width: 482px; height: 200px;" />
Input: </strong>root = <span id="example-input-1-1">[1,2,3,4,-99,-99,7,8,9,-99,-99,12,13,-99,14]</span>, limit = <span id="example-input-1-2">1</span>
<strong><img alt="" src="https://assets.leetcode.com/uploads/2019/06/05/insufficient-2.png" style="width: 258px; height: 200px;" />
Output: </strong><span id="example-output-1">[1,2,3,4,null,null,7,8,9,null,14]</span>
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong><img alt="" src="https://assets.leetcode.com/uploads/2019/06/05/insufficient-3.png" style="width: 292px; height: 200px;" />
Input: </strong>root = <span id="example-input-2-1">[5,4,8,11,null,17,4,7,1,null,null,5,3]</span>, limit = <span id="example-input-2-2">22</span>
<strong><img alt="" src="https://assets.leetcode.com/uploads/2019/06/05/insufficient-4.png" style="width: 264px; height: 200px;" />
Output: </strong><span id="example-output-2">[5,4,8,11,null,17,4,7,null,null,null,5]</span></pre>

<p>&nbsp;</p>

<p><strong>Example 3:</strong></p>

<pre>
<strong><img alt="" src="https://assets.leetcode.com/uploads/2019/06/11/screen-shot-2019-06-11-at-83301-pm.png" style="width: 188px; height: 150px;" />
Input: </strong>root = <span>[1,2,-3,-5,null,4,null]</span>, limit = -1
<img alt="" src="https://assets.leetcode.com/uploads/2019/06/11/screen-shot-2019-06-11-at-83517-pm.png" style="width: 122px; height: 150px;" /><strong>
Output: </strong><span>[1,null,-3,4]</span></pre>
</div>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ol>
	<li>The given tree will have between <code>1</code> and <code>5000</code> nodes.</li>
	<li><code>-10^5&nbsp;&lt;= node.val &lt;= 10^5</code></li>
	<li><code>-10^9 &lt;= limit&nbsp;&lt;= 10^9</code></li>
</ol>

<div>
<div>&nbsp;</div>
</div>

</div>

## Tags
- Depth-first Search (depth-first-search)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [ANNOUNCEMENT] Weekly Contest 140 has been declared as *unrated*
- Author: mrphantom
- Creation Date: Mon Jun 10 2019 04:33:19 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Jun 12 2019 14:06:52 GMT+0800 (Singapore Standard Time)

<p>
Hello everyone,

~~We have decided to rejudge this problem (140th LeetCode Weekly Contest&#39;s Q3 - Insufficient Nodes in Root to Leaf Paths) because of judging error.~~

~~Only the submissions which got &quot;Wrong Answer&quot; will be rejudged. All the submissions which got &quot;AC&quot; during contest, would be kept as such(even if they are incorrect).~~

~~Contest ranking will be updated as soon as rejudging finishes!~~

Edit: Even though this contest\'s Q3 was rejudged and ranking was updated, after careful consideration, we decided to declare "Weekly Contest 140" as unrated which means it won\'t have any effect on the global ranking. 

We apologize for the quality of some problems in the last few contests. While all our contest problems undergo a review and get tested by several people, a few issues have slipped past our review process.  To prevent such mistakes from happening in the future, we are working on improving our problem review process.
</p>


### [Java/C++/Python] Easy and Concise Recursion
- Author: lee215
- Creation Date: Sun Jun 09 2019 12:48:03 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Jun 11 2019 20:25:44 GMT+0800 (Singapore Standard Time)

<p>
## Foreword
LeetCode\'s OJ was wrong and it\'s fixed.
<br>

## Intuition
If root is leaf,
we compare the `limit` and `root.val`,
and return the result directly.

If root is leaf,
we recursively call the function on root\'s children with `limit = limit - root.val`.

Note that if a node become a new leaf,
it means it has no valid path leading to an original leaf,
we need to remove it.
<br>

## Explanation
```
if root.left == root.right == null, root is leaf with no child {
    if root.val < limit, we return null;
    else we return root.
}
if root.left != null, root has left child {
    Recursively call sufficientSubset function on left child,
    with limit = limit - root.val
}
if root.right != null, root has right child {
    Recursively call sufficientSubset function on right child,
    with limit = limit - root.val
}
if root.left == root.right == null,
root has no more children, no valid path left,
we return null;
else we return root.
```
<br>

## Complexity
Time O(N)
Space O(height) for recursion management.
<br>

**Java:**
```
    public TreeNode sufficientSubset(TreeNode root, int limit) {
        if (root == null)
            return null;
        if (root.left == null && root.right == null)
            return root.val < limit ? null : root;
        root.left = sufficientSubset(root.left, limit - root.val);
        root.right = sufficientSubset(root.right, limit - root.val);
        return root.left == root.right ? null : root;
    }
```

**C++:**
```
    TreeNode* sufficientSubset(TreeNode* root, int limit) {
        if (root->left == root->right)
            return root->val < limit ? NULL : root;
        if (root->left)
            root->left = sufficientSubset(root->left, limit - root->val);
        if (root->right)
            root->right = sufficientSubset(root->right, limit - root->val);
        return root->left == root->right ? NULL : root;
    }
```

**Python:**
```
    def sufficientSubset(self, root, limit):
        if root.left == root.right:
            return None if root.val < limit else root
        if root.left:
            root.left = self.sufficientSubset(root.left, limit - root.val)
        if root.right:
            root.right = self.sufficientSubset(root.right, limit - root.val)
        return root if root.left or root.right else None
```

</p>


### A leaf is a node with no children.
- Author: beet
- Creation Date: Sun Jun 09 2019 12:01:43 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jun 09 2019 12:04:56 GMT+0800 (Singapore Standard Time)

<p>
-> A leaf is a node with at most one children. (in this problem)
</p>


