---
title: "Numbers With Repeated Digits"
weight: 966
#id: "numbers-with-repeated-digits"
---
## Description
<div class="description">
<p>Given a positive integer <code>N</code>, return the number of positive integers less than or equal to <code>N</code> that have at least 1 repeated digit.</p>

<p>&nbsp;</p>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-1-1">20</span>
<strong>Output: </strong><span id="example-output-1">1</span>
<strong>Explanation: </strong>The only positive number (&lt;= 20) with at least 1 repeated digit is 11.
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-2-1">100</span>
<strong>Output: </strong><span id="example-output-2">10</span>
<strong>Explanation: </strong>The positive numbers (&lt;= 100) with atleast 1 repeated digit are 11, 22, 33, 44, 55, 66, 77, 88, 99, and 100.
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong><span id="example-input-3-1">1000</span>
<strong>Output: </strong><span id="example-output-3">262</span>
</pre>
</div>

<p>&nbsp;</p>

<p><strong><span>Note:</span></strong></p>

<ol>
	<li><code>1 &lt;= N &lt;= 10^9</code></li>
</ol>
</div>
</div>
</div>

## Tags
- Math (math)
- Dynamic Programming (dynamic-programming)

## Companies
- Akuna Capital - 3 (taggedByAdmin: false)
- Google - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python] Count the Number Without Repeated Digit
- Author: lee215
- Creation Date: Sun Mar 17 2019 12:01:15 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 17 2019 12:01:15 GMT+0800 (Singapore Standard Time)

<p>
## **Intuition**
Count `res` the Number Without Repeated Digit
Then the number with repeated digits = N - res

Similar as
788. Rotated Digits
902. Numbers At Most N Given Digit Set


## **Explanation**:

1. Transform `N + 1` to arrayList
2. Count the number with digits < n
3. Count the number with same prefix

For example,
if `N = 8765`, `L = [8,7,6,6]`,
the number without repeated digit can the the following format:
`XXX`
`XX`
`X`
`1XXX ~ 7XXX`
`80XX ~ 86XX`
`870X ~ 875X`
`8760 ~ 8765`


## **Time Complexity**:
the number of permutations `A(m,n)` is `O(1)`
We count digit by digit, so it\'s `O(logN)`

<br>

**Java:**
```
    public int numDupDigitsAtMostN(int N) {
        // Transform N + 1 to arrayList
        ArrayList<Integer> L = new ArrayList<Integer>();
        for (int x = N + 1; x > 0; x /= 10)
            L.add(0, x % 10);

        // Count the number with digits < N
        int res = 0, n = L.size();
        for (int i = 1; i < n; ++i)
            res += 9 * A(9, i - 1);

        // Count the number with same prefix
        HashSet<Integer> seen = new HashSet<>();
        for (int i = 0; i < n; ++i) {
            for (int j = i > 0 ? 0 : 1; j < L.get(i); ++j)
                if (!seen.contains(j))
                    res += A(9 - i, n - i - 1);
            if (seen.contains(L.get(i))) break;
            seen.add(L.get(i));
        }
        return N - res;
    }


    public int A(int m, int n) {
        return n == 0 ? 1 : A(m, n - 1) * (m - n + 1);
    }
```

**Python:**
```
    def numDupDigitsAtMostN(self, N):
        L = map(int, str(N + 1))
        res, n = 0, len(L)

        def A(m, n):
            return 1 if n == 0 else A(m, n - 1) * (m - n + 1)

        for i in range(1, n): res += 9 * A(9, i - 1)
        s = set()
        for i, x in enumerate(L):
            for y in range(0 if i else 1, x):
                if y not in s:
                    res += A(9 - i, n - i - 1)
            if x in s: break
            s.add(x)
        return N - res
```

</p>


### Python O(logN) solution with clear explanation
- Author: heqingy
- Creation Date: Sun Mar 17 2019 13:55:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 17 2019 13:55:53 GMT+0800 (Singapore Standard Time)

<p>
The number of non-repeated digits can be easily calculated with permutaiton. We only need to exclude all the non-repeated digits to get the answer.

Let\'s first consider about the cases where N=10^k
**N=10**
the free digits are marked as `*`, so we only need to consider about `*` and `1*`
* `*`: obviously all 1-digit numbers are non-repeated, so non-repeated number = 9
* `1*`: we only need to consider about `1* <= 10`, so non-repeated number = 1

Thus, the result for N=10 is:
`N - #non_repeat(*) - #non_repeat(1*) = 10 - 9 - 1 = 0`

**N=100**
the free digits are marked as `*`, so we only need to consider about `*`, `**`, and `1**`
* `*`: obviously all 1-digit numbers are non-repeated, so non-repeated number = 9
* `**`: this can be calculated with permutation: leading digit has 9 options(1-9) and the last 1 digit has `10-1` options, thus the total permuation is `9 * permutation(9, 1)=81`. i.e: non-repeated number = 81
* `1**`: we only need to consider about `1**<=100`, so non-repeated number =0

Thus, the result for N=100 is:
`N - #non_repeat(*) - #non_repeat(**) - #non_repeat(1**) = 100 - 9 - 81 = 10`

**N=1000**
`#non_repeat(***) = 9 * permutation(9, 2) = 9 * 9 * 8 = 648`
similarly, we can get:
`N - #non_repeat(*) - #non_repeat(**) - #non_repeat(***) - #non_repeat(1***) = 1000 - 9 - 81 - 648 = 282`

Now, let\'s consider a more general case:
**N=12345**
actually, we can get the count of non-repeated numbers  by counting all non-repeated numbers in following patterns:

```
    *
   **
  ***
 ****
10***
11*** (prefix repeated, skip)
120**
121** (prefix repeated, skip)
122** (prefix repeated, skip)
1230*
1231* (prefix repeated, skip)
1232* (prefix repeated, skip)
1233* (prefix repeated, skip)
12340
12341 (prefix repeated, skip)
12342
12343
12344 (prefix repeated, skip)
12345
```

and use N to minus the count we will get the answer.

Reference implementation:
```
# given number n, see whether n has repeated number
def has_repeated(n):
    str_n = str(n)
    return len(set(str_n)) != len(str_n)

def permutation(n, k):
    prod = 1
    for i in range(k):
        prod *= (n-i)
    return prod

# calculate number of non-repeated n-digit numbers
# note: the n-digit number can\'t start with 0
# i.e: n_digit_no_repeat(2) calculates the non-repeated
#   numbers in range [10, 99] (inclusive)
def n_digit_no_repeat(n):
    if n == 1:
        return 9
    else:
        return  9 * permutation(9, n-1)

class Solution(object):
    def numDupDigitsAtMostN(self, N):
        """
        :type N: int
        :rtype: int
        """        
        N_str = str(N)
        n_digit = len(N_str)
        digits = map(int, N_str)
        result = N - 1
        prefix = 0
        for i in range(1, n_digit):
            result -= n_digit_no_repeat(i)
        for i in range(n_digit):
            # when we fix the most significant digit, it 
            # can\'t be zero
            start = 0 if i else 1
            for j in range(start, digits[i]):
                if has_repeated(prefix * 10 + j):
                    continue
                if i != n_digit-1:
                    result -= permutation(10-i-1, n_digit-1-i)
                else:
                    # optmized from `result -= has_repeated(prefix*10+j)`
                    result -= 1
            prefix = prefix*10 + digits[i]
        return result + has_repeated(N)
```
</p>


### Share my O(logN) C++ DP solution with proof and explanation
- Author: KJer
- Creation Date: Tue Mar 19 2019 15:52:55 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Mar 19 2019 15:52:55 GMT+0800 (Singapore Standard Time)

<p>
---
## 1. Problem

---
Given a positive integer ```N```, return the number of positive integers less than or equal to ```N``` that have at least 1 repeated digit.

**Example 1:**
```
Input: 20
Output: 1
Explanation: The only positive number (<= 20) with at least 1 repeated digit is 11.
```

**Example 2:**
```
Input: 100
Output: 10
Explanation: The positive numbers (<= 100) with at least 1 repeated digit are 11, 22, 33, 44, 55, 66, 77, 88, 99, and 100.
```

**Example 3:**
```
Input: 1000
Output: 262
```
**Note:**
* 1 <= N <= 10^9

---
## 2. Thinking process

---
#### 2.1 Analysis

---
The problem is to return 

>#### T(N) = the number of positive integers less than or equal to N that have **at least 1 repeated digit**.

Suppose

>#### S(N) = the number of positive integers less than or equal to N that have **NO repeated digits**.

The answer can be expressed as

>#### T(N) = N - S(N).

Later, the calculation of S(N) will be focused on.

---
#### 2.2 Find the rules

---

- From **1** to **9**, there are **9** positive integers that have **NO repeated digits**.

- From **10** to **99**,
  - From **10** to **19**, there are **9** positive integers that have **NO repeated digits**. (Only **11** has repeated digits)
  - From **20** to **29**, there are **9** positive integers that have **NO repeated digits**. (Only **22** has repeated digits)
  - From **30** to **39**, there are **9** positive integers that have **NO repeated digits**. (Only **33** has repeated digits)
  - From **40** to **49**, there are **9** positive integers that have **NO repeated digits**. (Only **44** has repeated digits)
  - From **50** to **59**, there are **9** positive integers that have **NO repeated digits**. (Only **55** has repeated digits)
  - From **60** to **69**, there are **9** positive integers that have **NO repeated digits**. (Only **66** has repeated digits)
  - From **70** to **79**, there are **9** positive integers that have **NO repeated digits**. (Only **77** has repeated digits)
  - From **80** to **89**, there are **9** positive integers that have **NO repeated digits**. (Only **88** has repeated digits)
  - From **90** to **99**, there are **9** positive integers that have **NO repeated digits**. (Only **99** has repeated digits)
  there are **9 \xD7 9 = 81** positive integers that have **NO repeated digits**.  
  
- From **100** to **999**,
  - From **100** to **199**,
    - From **100** to **109**, there are **8** positive integers that have **NO repeated digits**. (**100** and **101** have repeated digits)
    - From **110** to **119**, there are **0** positive integers that have **NO repeated digits**. (**ALL numbers** have repeated digits because of the prefix **11**)
    - From **120** to **129**, there are **8** positive integers that have **NO repeated digits**. (**121** and **122** have repeated digits)
    - From **130** to **139**, there are **8** positive integers that have **NO repeated digits**. (**131** and **133** have repeated digits)
    - From **140** to **149**, there are **8** positive integers that have **NO repeated digits**. (**141** and **144** have repeated digits)
    - From **150** to **159**, there are **8** positive integers that have **NO repeated digits**. (**151** and **155** have repeated digits)
    - From **160** to **169**, there are **8** positive integers that have **NO repeated digits**. (**161** and **166** have repeated digits)
    - From **170** to **179**, there are **8** positive integers that have **NO repeated digits**. (**171** and **177** have repeated digits)
    - From **180** to **189**, there are **8** positive integers that have **NO repeated digits**. (**181** and **188** have repeated digits)
    - From **190** to **199**, there are **8** positive integers that have **NO repeated digits**. (**191** and **199** have repeated digits)
    there are **9 \xD7 8 = 72** positive integers that have **NO repeated digits**.
  - .....

Let\'s think about all positive integers **from 100 to 199**.
 
They can be generated by 

>#### adding a new digit from **0** to **9** to the end of all positive integers from **10** to **19**.
In order to generate a new positive integer that has **NO** repeated digits,
- To **10**: **10 has NO repeated digits**. There are **8 choices (0 and 1 can NOT be chosen)**.
- To **11**: **11 has repeated digits**. There are **0 choices (0 to 9 can NOT be chosen)**.
- To **12**: **12 has NO repeated digits**. There are **8 choices (1 and 2 can NOT be chosen)**.
- To **13**: **13 has NO repeated digits**. There are **8 choices (1 and 3 can NOT be chosen)**.
- To **14**: **14 has NO repeated digits**. There are  **8 choices (1 and 4 can NOT be chosen)**.
- To **15**: **15 has NO repeated digits**. There are  **8 choices (1 and 5 can NOT be chosen)**.
- To **16**: **16 has NO repeated digits**. There are  **8 choices (1 and 6 can NOT be chosen)**.
- To **17**: **17 has NO repeated digits**. There are  **8 choices (1 and 7 can NOT be chosen)**.
- To **18**: **18 has NO repeated digits**. There are  **8 choices (1 and 8 can NOT be chosen)**.
- To **19**: **19 has NO repeated digits**. There are  **8 choices (1 and 9 can NOT be chosen)**.

Are there rules?

---
>#### **Rule A:** 
>#### **A k-digit positive integer with NO repeated digits can ONLY be generated from (k - 1)-digit positive integers with NO repeated digits (k > 1).**

Proof:

If a (k - 1)-digit positive integer has repeated digits (e.g. **11**), 
after adding a digit (**0** to **9**) to its end, the generated k-digit integer (e.g. 110, 111, ..., 119) **STILL has repeated digits**.
That\'s to say, a k-digit positive integer with **NO** repeated digits can **ONLY** be generated from (k - 1)-digit positive integers with **NO** repeated digits.

---

>#### **Rule B:** 
>#### **To generate a k-digit positive integer with NO repeated digits,** 
>#### **there are (10 - k + 1) digits that can be added to (k - 1)-digit positive integers with NO repeated digits (k > 1).**

Proof:

A (k - 1)-digit positive integer **with NO repeated digits** has **k - 1 distinct digits**.
When generating a k-digit positive integer **with NO repeated digits** from it, 
since **k - 1** digits in **0** to **9** have been **used**, there are **10 - k + 1** choices for the digit to be added.


---
#### 2.3 Recursion formula

---
Definition:

>#### **f(i, j, k): The number of i-digit positive integers with NO repeated digits in the interval [j, k]. (i > 0, j \u2264 k, j and k are i-digit positive integers).**


Based on the **Rule A and Rule B in Section 2.2**, the recursion formula is

>#### **f(i, j, k) = k - j + 1. i = 1.**
>#### **f(i + 1, 10j, 10k + 9) = f(i, j, k) \xD7 (10 - i). others.**

---
#### 2.4 Normal case analysis


---

In order to illustrate the usage of the recursion formula in Section 2.3, we take a normal case for calculation.

If **N = 26334**,

From **Section 2.3**,
 
- From **1** to **9**, f(1, 1, 9) = 9.
- From **10** to **99**, f(2, 10, 99) = f(1, 1, 9) \xD7 (10 - 1) = 9 \xD7 9 = 81.
- From **100** to **999**, f(3, 100, 999) = f(2, 10, 99) \xD7 (10 - 2) = 81 \xD7 8 = 648.
- From **1000** to **9999**, f(4, 1000, 9999) = f(3, 100, 999) \xD7 (10 - 3) = 648 \xD7 7 = 4536.

If all values are added together, the sum is

>#### **S(9999) = f(1, 1, 9) + f(2, 10, 99) + f(3, 100, 999) + f(4, 1000, 9999) = 9 + 81 + 648 + 4536 = 5274.**

Now the number of positive integers **with NO repeated digits less than or equal to 9999** has been calculated, which is the **first part of the whole result**.


---
How about the rest?

The rest part is the number of positive integers **with NO repeated digits in interval [10000, 26334]**, which is

>#### **P = f(5, 10000, 26334)**.

How can the recursion formula be applied here?

---
Since our target is to **calculate f(5, 10000, 26334)**, the **calculation series** is

>#### **f(1, 1, 2), f(2, 10, 26), f(3, 100, 263), f(4, 1000, 2633), f(5, 10000, 26334).**


- From **1** to **2**, **f(1, 1, 2) = 2**.


- From **10** to **29**, by applying the recursion formula, **f(2, 10, 29) = f(1, 1, 2) \xD7 (10 - 1) = 2 \xD7 9 = 18**.

  - From **27 to 29**, there are **3** positive integers **with NO repeated digits**, which means **f(2, 27, 29) = 3**.   

  - **f(2, 10, 26) = f(2, 10, 29) - f(2, 27, 29) = 18 - 3 = 15**.
  

- From **100** to **269**, by applying the recursion formula, **f(3, 100, 269) = f(2, 10, 26) \xD7 (10 - 2) = 15 \xD7 8 = 120**.

  - From **264 to 269**, there are **5** positive integers **with NO repeated digits** (**except 266**), which means **f(3, 264, 269) = 5**.
  
  - **f(3, 100, 263) = f(3, 100, 269) - f(3, 264, 269) = 120 - 5 = 115**.
  
  
- From **1000** to **2639**, by applying the recursion formula, **f(4, 1000, 2639) = f(3, 100, 263) \xD7 (10 - 3) = 115 \xD7 7 = 805**.

  - From **2634 to 2639**, there are **5** positive integers **with NO repeated digits** (**except 2636**), which means **f(4, 2634, 2639) = 5**.
  
  - **f(4, 1000, 2633) = f(4, 1000, 2639) - f(4, 2634, 2639) = 805 - 5 = 800**. 

  
- From **10000** to **26339**, by applying the recursion formula, **f(4, 10000, 26339) = f(4, 1000, 2633) \xD7 (10 - 4) = 800 \xD7 6 = 4800**.

  - From **26335 to 26339**, there are **NO** positive integers **with NO repeated digits** (**due to the prefix "2633"**), which means **f(5, 26335, 26339) = 0**.
  
  - **f(5, 10000, 26334) = f(4, 10000, 26339) - f(5, 26335, 26339) = 4800 - 0 = 4800**.

This is the **second part of the whole result**.

---
Then

>#### **S(26334) = S(9999) + f(5, 10000, 26334) = 5274 + 4800 = 10074.**


The final answer is

>#### **T(26334) = 26334 - S(26334) = 26334 - 10074 = 16260.**

---
#### 2.4 Algorithm


---
**Special case**:
- If N < 10, return 0.

---
**Get digits**:
- Initialization:

  - Set k = 0. i = N.

- Loop: while i > 0, 
  
  - Set k = k + 1. (Now, k is the digit length of N).

  - Set i = i / 10. 
  
- Initialization: 

  - Set j = N.

  - Array digit with size = k. (saving all digits in N)
  
- Loop: iterate i from 0 to k - 1

  - Set digit[k - 1 - i] = j mod 10.
  
  - Set j = j / 10.

---
**Get first part**:

- Initialization: 

  - Array noDupBase with size = k - 1. 
  
  - Set noDupBaseSum = 0. (calculate first part)

- Loop: iterate i from 0 to k - 2.

  - If i = 0, noDupBase[i] = 9. Calculate f(1, 1, 9).
  
  - Else, noDupBase[i] = noDupBase[i - 1] \xD7 (10 - i). Calculate f(i, 10^(i - 1), 10^i - 1).
  
  - Set noDupBaseSum = noDupBaseSum + noDupBase[i].

---
**Get second part**:

- Initialization: 

  - Set boolean value duplicate = false. (prefix duplicate) 
  
  - Array count with size = 10.(record the digits\' count in prefix). 
  
  - Array noDupRes with size = k. (calculate second part)
  
- Loop: iterate i from 0 to k - 1.

  - If i = 0, noDupBase[i] = 9. Calculate f(1, 1, 9).
  
  - Else, noDupBase[i] = noDupBase[i - 1] \xD7 (10 - i).
  
  - If NOT duplicate
  
    - Set diff = 0.
	
	- Loop: iterate j from digit[i] + 1 to 9
	
	  - If count[j] = 0, Set diff = diff + 1.
	  
	- Set noDupRes[i] = noDupRes[i] - diff.
	
	- Set count[digit[i]] = count[digit[i]] + 1.
	
	- If count[digit[i]] > 1, Set duplicate = true.

---
**Get final answer**:
	
- return N - (noDupBaseSum + noDupRes[k - 1]).


---
## 4. Complexity Analysis

---

#### 4.1 Time complexity

---

>#### The time complexity is **O(logN)**.

---

#### 4.2 Space complexity

---

>#### The space complexity is **O(logN)**.

---
## 5. Code

---
```
class Solution {
public:
    int numDupDigitsAtMostN(int N) {
        if(N < 10) return 0;
        int k = 0;
        for(int i = N; i > 0; i /= 10) k++;
        int digit[k] = {0};
        for(int i = 0, j = N; i < k; i++, j /= 10) digit[k - 1 - i] = j % 10;
        
        int noDupBaseSum = 0;
        int noDupBase[k - 1] = {0};
        for(int i = 0; i < k - 1; i++)
        {
            noDupBase[i] = i == 0 ? 9 : noDupBase[i - 1] * (10 - i);
            noDupBaseSum += noDupBase[i];
        }
        
        int count[10] = {0};
        int noDupRes[k] = {0};
        bool duplicate = false;
        for(int i = 0; i < k; i++)
        {
            noDupRes[i] = i == 0 ? 9 : noDupRes[i - 1] * (10 - i);
            if(!duplicate)
            {
                int diff = 0;
                for(int j = digit[i] + 1; j < 10; j++) diff += count[j] == 0;
                noDupRes[i] -= diff;
                count[digit[i]]++;
                if(count[digit[i]] > 1) duplicate = true;
            }
        }
        return N - (noDupBaseSum + noDupRes[k - 1]);
    }
};
</p>


