---
title: "Reaching Points"
weight: 717
#id: "reaching-points"
---
## Description
<div class="description">
<p>A move consists of taking a point <code>(x, y)</code> and transforming it to either <code>(x, x+y)</code> or <code>(x+y, y)</code>.</p>

<p>Given a starting point <code>(sx, sy)</code> and a target point <code>(tx, ty)</code>, return <code>True</code> if and only if a sequence of moves exists to transform the point <code>(sx, sy)</code> to <code>(tx, ty)</code>. Otherwise, return <code>False</code>.</p>

<pre>
<strong>Examples:</strong>
<strong>Input:</strong> sx = 1, sy = 1, tx = 3, ty = 5
<strong>Output:</strong> True
<strong>Explanation:</strong>
One series of moves that transforms the starting point to the target is:
(1, 1) -&gt; (1, 2)
(1, 2) -&gt; (3, 2)
(3, 2) -&gt; (3, 5)

<strong>Input:</strong> sx = 1, sy = 1, tx = 2, ty = 2
<strong>Output:</strong> False

<strong>Input:</strong> sx = 1, sy = 1, tx = 1, ty = 1
<strong>Output:</strong> True

</pre>

<p><strong>Note:</strong></p>

<ul>
	<li><code>sx, sy, tx, ty</code> will all be integers in the range <code>[1, 10^9]</code>.</li>
</ul>

</div>

## Tags
- Math (math)

## Companies
- Goldman Sachs - 31 (taggedByAdmin: false)
- Twitter - 2 (taggedByAdmin: false)
- Quora - 4 (taggedByAdmin: false)
- Uber - 3 (taggedByAdmin: false)
- Salesforce - 6 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: true)
- Coursera - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Exhaustive Search [Time Limit Exceeded]

**Intuition and Algorithm**

Each point has two children as specified in the problem.  We try all such points recursively.

<iframe src="https://leetcode.com/playground/9XKMkUpz/shared" frameBorder="0" width="100%" height="174" name="9XKMkUpz"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(2^{tx + ty})$$, a loose bound found by considering every move as `(x, y) -> (x+1, y)` or `(x, y) -> (x, y+1)` instead.

* Space Complexity:  $$O(tx * ty)$$, the size of the implicit call stack.

---
#### Approach #2: Dynamic Programming [Time Limit Exceeded]

**Intuition and Algorithm**

As in *Approach #1*, we search the children of every point recursively, except we use a set `seen` so that we don't repeat work.

<iframe src="https://leetcode.com/playground/C3vYDhnk/shared" frameBorder="0" width="100%" height="429" name="C3vYDhnk"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(tx * ty)$$, as at most `tx * ty` points are searched once per point.

* Space Complexity:  $$O(tx * ty)$$, the size of the implicit call stack.

---
#### Approach #3: Work Backwards (Naive Variant) [Time Limit Exceeded]

**Intuition**

Every parent point `(x, y)` has two children, `(x, x+y)` and `(x+y, y)`.  However, every point `(x, y)` only has one parent candidate `(x-y, y)` if `x >= y`, else `(x, y-x)`.  This is because we never have points with negative coordinates.

<br />
<center>
    <img src="../Figures/780/tree.png" alt="Diagram of successive parents of the target point" width="350"/>
</center>
<br />

Looking at previous successive parents of the target point, we can find whether the starting point was an ancestor.  For example, if the target point is `(19, 12)`, the successive parents must have been `(7, 12)`, `(7, 5)`, and `(2, 5)`; so `(2, 5)` is a starting point of `(19, 12)`.

**Algorithm**

Repeatedly subtract the smaller of `{tx, ty}` from the larger of `{tx, ty}`.  The answer is true if and only if we eventually reach `sx, sy`.

<iframe src="https://leetcode.com/playground/CQbY7BGd/shared" frameBorder="0" width="100%" height="242" name="CQbY7BGd"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\max(tx, ty))$$.  If say `ty = 1`, we could be subtracting `tx` times.

* Space Complexity:  $$O(1)$$.

---
#### Approach #4: Work Backwards (Modulo Variant) [Accepted]

**Intuition**

As in *Approach #3*, we work backwards to find the answer, trying to transform the target point to the starting point via applying the parent operation `(x, y) -> (x-y, y) or (x, y-x)` [depending on which one doesn't have negative coordinates.]

We can speed up this transformation by bundling together parent operations.  

**Algorithm**

Say `tx > ty`.  We know that the next parent operations will be to subtract `ty` from `tx`, until such time that `tx = tx % ty`.  When both `tx > ty` and `ty > sy`, we can perform all these parent operations in one step, replacing `while tx > ty: tx -= ty` with `tx %= ty`.

Otherwise, if say `tx > ty and ty <= sy`, then we know `ty` will not be changing (it can only decrease).  Thus, only `tx` will change, and it can only change by subtracting by `ty`.  Hence, `(tx - sx) % ty == 0` is a necessary and sufficient condition for the problem's answer to be True.

The analysis above was for the case `tx > ty`, but the case `ty > tx` is similar.  When `tx == ty`, no more moves can be made.

<iframe src="https://leetcode.com/playground/GcmNWyhM/shared" frameBorder="0" width="100%" height="344" name="GcmNWyhM"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(\log(\max{(tx, ty)}))$$.  The analysis is similar to the analysis of the Euclidean algorithm, and we assume that the modulo operation can be done in $$O(1)$$ time.

* Space Complexity:  $$O(1)$$.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/C++/Python] Modulo from the End
- Author: lee215
- Creation Date: Mon Feb 12 2018 00:52:29 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Mar 07 2020 15:19:17 GMT+0800 (Singapore Standard Time)

<p>
**Basic idea:**
If we start from ```sx,sy```, it will be hard to find ```tx, ty```.
If we start from ```tx,ty```, we can find only one path to go back to ```sx, sy```.
I cut down one by one at first and I got TLE. So I came up with remainder.

**First line:**
if 2 target points are still bigger than 2 starting point, we reduce target points.
**Second line:**
check if we reduce target points to (x, y+kx) or (x+ky, y)

**Time complexity**
I will say ```O(logN)``` where ```N = max(tx,ty)```.

**C++:**
```cpp
    bool reachingPoints(int sx, int sy, int tx, int ty) {
        while (sx < tx && sy < ty)
            if (tx < ty) ty %= tx;
            else tx %= ty;
        return sx == tx && sy <= ty && (ty - sy) % sx == 0 ||
               sy == ty && sx <= tx && (tx - sx) % sy == 0;
    }
```
**Java:**
```java
    public boolean reachingPoints(int sx, int sy, int tx, int ty) {
        while (sx < tx && sy < ty)
            if (tx < ty) ty %= tx;
            else tx %= ty;
        return sx == tx && sy <= ty && (ty - sy) % sx == 0 ||
               sy == ty && sx <= tx && (tx - sx) % sy == 0;
    }
```
**Python:**
```py
    def reachingPoints(self, sx, sy, tx, ty):
        while sx < tx and sy < ty:
            tx, ty = tx % ty, ty % tx
        return sx == tx and sy <= ty and (ty - sy) % sx == 0 or \
               sy == ty and sx <= tx and (tx - sx) % sy == 0
```
</p>


### Detailed explanation. | full through process | Java 100% beat
- Author: nits2010
- Creation Date: Thu Sep 05 2019 22:10:19 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 23 2019 16:27:18 GMT+0800 (Singapore Standard Time)

<p>
**Top Down apprach : Naive/initial thinking**


 If you observe that you have two actions to take at any moment for (x,y)
 1. `(x , x+y)`
 2. `(x+y, y)`
 That means, we can have two choices at `(x,y)` which forms it like a binary tree. And one of the leaf node probably consist the `(tx, ty)`

Top Down:
 1. Do same as ask, run from top to bottom of binary tree. Find the leaf node which satisfy the condition.
 sx=1, sy=1, tx=3, ty=5
Visulatization 
![image](https://assets.leetcode.com/users/nits2010/image_1567692289.png)


  Complexity: The height of binary tree depends on tx,ty. All the values which are either greater then tx or ty will be discarded as from that we can\'t reach the tx,ty.
 Hence, the height of tree would be Max(tx,ty)= N ..`total complexity O(2^N)`

```
 public static boolean reachingPoints(int sx, int sy, int tx, int ty) {

        if (sx == tx && sy == ty)
            return true;

        if (sx > tx || sy > ty)
            return false;

        return (reachingPoints(sx + sy, sy, tx, ty) || reachingPoints(sx, sx + sy, tx, ty));
    }
```


**Optimization : Bottom up**


 In above approach, we need to drill down till the `tx` or `ty`. If you see, for each child, there is only 1 way to reach parent (eventually root) in binary tree.
 Which means, instead of starting from `(sx,sy)` and go down, we can start from `(tx,ty)` and go up till you hit one of the condition like `sx >= tx or sy>= ty` {revers of top down condition}
 then you from that point you can simply check does it is possible to reach or not.

 or sx=1, sy=1, tx=3, ty=5
 
 ![image](https://assets.leetcode.com/users/nits2010/image_1567692483.png)


 now we met the base condition, [1,2] can only be translate for [1,1] when ty >= sy {2>1} and (ty-sy)%sx == 0 { (2-1)%1 == 0}

` why (ty-sy)%sx == 0?`
 since
` sy` will translate to ty only by `(sx+sy)`, if they translate then` (sx, sy+k*sx) = ty  for some k`
 `sy+k*sx = ty => (ty-sy) / sx = k.`
 Since `sx,sy,tx,ty` are all integer, then `k` has to be a `integer` which means, there must be a integer `k` that help us to reach `ty`. Which makes reminder to be `0`
 Hence 
**` (ty-sy) % sx == 0`**

**`Complexity : O(log(n)) where n = Max(tx,ty)`**
```
 public static boolean reachingPoints(int sx, int sy, int tx, int ty) {

        while (sx < tx && sy < ty)
            if (tx < ty)
                ty %= tx;
            else
                tx %= ty;

      if(sx == tx && sy <= ty && (ty - sy) % sx == 0)
            return true; 
        
         return sy == ty && sx <= tx && (tx - sx) % sy == 0;
    }
```


Edit :
@ShenTM Thanks for pointing out the problem. While pasting the code,  i mistkely pased wrong condition which i correct later once this got failed. 

I\'ve corrected now. Working fine now. 

Runtime: 0 ms, faster than 100.00% of Java online submissions for Reaching Points.
Memory Usage: 33.3 MB, less than 12.50% of Java online submissions for Reaching Points.

</p>


### Easy to understand diagram and recursive solution
- Author: wen_jia
- Creation Date: Sat Feb 02 2019 13:11:50 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Feb 02 2019 13:11:50 GMT+0800 (Singapore Standard Time)

<p>
![image](https://assets.leetcode.com/users/wen_jia/image_1549084374.png)

The above image shows the path that is taken from (64, 27) to the root node (3, 7) within the tree of all pairs that can be generated from the root.

Note that this is essentially https://en.wikipedia.org/wiki/Euclidean_algorithm with different initial conditions.

```
class Solution {
public:
    bool recurse(int sx, int sy, int tx, int ty) {
        if (tx < sx) {
            return false;
        }
        else if (tx == sx) {
            return ((ty - sy) % sx) == 0;
        }
        else {
            return recurse(sy, sx, ty % tx, tx);
        }
    }
    
    bool reachingPoints(int sx, int sy, int tx, int ty) {
        if (tx < ty) {
            return recurse(sx, sy, tx, ty);
        }
        else {
            return recurse(sy, sx, ty, tx);
        }
    }
};
```
</p>


