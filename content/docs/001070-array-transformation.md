---
title: "Array Transformation"
weight: 1070
#id: "array-transformation"
---
## Description
<div class="description">
<p>Given an initial array <code>arr</code>, every day you produce a new array using the array of the previous day.</p>

<p>On the <code>i</code>-th day, you do the following operations on the array of day&nbsp;<code>i-1</code>&nbsp;to produce the array of day <code>i</code>:</p>

<ol>
	<li>If an element is smaller than both its left neighbor and its right neighbor, then this element is incremented.</li>
	<li>If an element is bigger than both its left neighbor and its right neighbor, then this element is decremented.</li>
	<li>The first&nbsp;and last elements never change.</li>
</ol>

<p>After some days, the array does not change. Return that final array.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> arr = [6,2,3,4]
<strong>Output:</strong> [6,3,3,4]
<strong>Explanation: </strong>
On the first day, the array is changed from [6,2,3,4] to [6,3,3,4].
No more operations can be done to this array.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> arr = [1,6,3,4,3,5]
<strong>Output:</strong> [1,4,4,4,4,5]
<strong>Explanation: </strong>
On the first day, the array is changed from [1,6,3,4,3,5] to [1,5,4,3,4,5].
On the second day, the array is changed from [1,5,4,3,4,5] to [1,4,4,4,4,5].
No more operations can be done to this array.
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>3 &lt;= arr.length &lt;= 100</code></li>
	<li><code>1 &lt;= arr[i] &lt;= 100</code></li>
</ul>

</div>

## Tags
- Array (array)

## Companies
- Virtu Financial - 2 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### C++ space O(1) solution
- Author: Yushi_Lu
- Creation Date: Tue Nov 12 2019 06:43:01 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Nov 12 2019 06:43:01 GMT+0800 (Singapore Standard Time)

<p>
The straight forward way is to use another array to take note of the previous array, and use the previous array to construct a new one. But this will incur O(N) space complexity. We use three variables to record the `prev, curr, next` variable in each run. After each step, we update these three value. Besides, we use a bool var to tell us whether there is an update or not. If not, we break out of the while loop and return the result array.
```
vector<int> transformArray(vector<int>& arr) {
	if (arr.size() <= 2) return arr;
	bool changed = true;
	int cnt = 0;
	while (changed ) {
		changed = false;
		// cn?t++;
		int prev = arr[0], curr = arr[1], next = arr[2];

		for (int i = 1; i < arr.size() - 1; i++) {
			// cout<<prev<<"  "<<curr<<"  "<<next<<endl;
			if (curr < prev && curr < next) {
				arr[i] = arr[i] + 1;
				changed = true;
			}
			else if (curr > prev && curr > next) {
				arr[i] = arr[i] - 1;
				changed = true;
			}
			if (i == arr.size() - 2) break;
			prev = curr;
			curr = next;
			next = arr[i + 2];
		}

	}
	return arr;
}
```
</p>


### [Java/Python 3] Bruteforce
- Author: rock
- Creation Date: Sun Nov 03 2019 00:22:59 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 03 2019 08:51:55 GMT+0800 (Singapore Standard Time)

<p>
```
    public List<Integer> transformArray(int[] arr) {
        int[] ans = new int[arr.length];
        while (!Arrays.equals(ans, arr)) {
            ans = arr.clone();
            for (int i = 1; i < arr.length - 1; ++i) {
                if (ans[i - 1] < ans[i] && ans[i] > ans[i + 1]) {
                    --arr[i];
                }else if ( ans[i - 1] > ans[i] && ans[i] < ans[i + 1]) {
                    ++arr[i];
                }
            }
        }
        return arr.length < 3 ? arr : Arrays.stream(ans).boxed().collect(Collectors.toList());
    }
```
```
    def transformArray(self, arr: List[int]) -> List[int]:
        ans = [0] * len(arr)
        while ans != arr:
            ans = arr[:]
            for i in range(1, len(arr) - 1):
                if ans[i - 1] < ans[i] > ans[i + 1]:
                    arr[i] -= 1
                elif ans[i - 1] > ans[i] < ans[i + 1]:
                    arr[i] += 1
        return arr if len(arr) < 3 else ans
```
or

```
    def transformArray(self, arr: List[int]) -> List[int]:
        ans = [0] * len(arr)
        while ans != arr:
            ans = arr[:]
            for i in range(1, len(arr) - 1):
                arr[i] += (ans[i - 1] > ans[i] < ans[i + 1]) - (ans[i - 1] < ans[i] > ans[i + 1])
        return arr if len(arr) < 3 else ans
```
</p>


### [Python3] 4-liner
- Author: cenkay
- Creation Date: Sun Nov 03 2019 00:03:08 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Nov 03 2019 00:34:05 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution:
    def transformArray(self, arr: List[int], change: bool = True) -> List[int]:
        while change:
            new = arr[:1] + [b + (a > b < c) - (a < b > c) for a, b, c in zip(arr, arr[1:], arr[2:])] + arr[-1:]
            arr, change = new, arr != new
        return arr
```
</p>


