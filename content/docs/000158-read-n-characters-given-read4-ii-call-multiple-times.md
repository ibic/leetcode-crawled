---
title: "Read N Characters Given Read4 II - Call multiple times"
weight: 158
#id: "read-n-characters-given-read4-ii-call-multiple-times"
---
## Description
<div class="description">
<p>Given a file and assume that you can only read the file using a given method&nbsp;<code>read4</code>, implement a method <code>read</code> to read <em>n</em> characters. <strong>Your method <code>read</code> may be called multiple times.</strong></p>

<p>&nbsp;</p>

<p><b>Method read4: </b></p>

<p>The API&nbsp;<code>read4</code> reads 4 consecutive characters from the file, then writes those characters into the buffer array <code>buf</code>.</p>

<p>The return value is the number of actual characters read.</p>

<p>Note that&nbsp;<code>read4()</code> has its own file pointer, much like <code>FILE *fp</code> in C.</p>

<p><b>Definition of read4:</b></p>

<pre>
    Parameter:  char[] buf4
    Returns:    int

Note: buf4[] is destination not source, the results from read4 will be copied to buf4[]
</pre>

<p>Below is a high level example of how <code>read4</code> works:</p>

<p><img alt="" src="https://assets.leetcode.com/uploads/2020/07/01/157_example.png" style="width: 600px; height: 403px;" /></p>

<pre>
<code>File file(&quot;abcde&quot;); // File is &quot;abcde&quot;, initially file pointer (fp) points to &#39;a&#39;
char[] buf = new char[4]; // Create buffer with enough space to store characters
read4(buf4); // read4 returns 4. Now buf = &quot;abcd&quot;, fp points to &#39;e&#39;
read4(buf4); // read4 returns 1. Now buf = &quot;e&quot;, fp points to end of file
read4(buf4); // read4 returns 0. Now buf = &quot;&quot;, fp points to end of file</code>
</pre>

<p>&nbsp;</p>

<p><strong>Method read:</strong></p>

<p>By using the <code>read4</code> method, implement the method&nbsp;<code>read</code> that reads <i>n</i> characters from the file and store it in the&nbsp;buffer array&nbsp;<code>buf</code>. Consider that you <strong>cannot</strong> manipulate the file directly.</p>

<p>The return value is the number of actual characters read.</p>

<p><b>Definition of read: </b></p>

<pre>
    Parameters:	char[] buf, int n
    Returns:	int

Note: buf[] is destination not source, you will need to write the results to buf[]
</pre>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
File file(&quot;abc&quot;);
Solution sol;
// Assume buf is allocated and guaranteed to have enough space for storing all characters from the file.
sol.read(buf, 1); // After calling your read method, buf should contain &quot;a&quot;. We read a total of 1 character from the file, so return 1.
sol.read(buf, 2); // Now buf should contain &quot;bc&quot;. We read a total of 2 characters from the file, so return 2.
sol.read(buf, 1); // We have reached the end of file, no more characters can be read. So return 0.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
File file(&quot;abc&quot;);
Solution sol;
sol.read(buf, 4); // After calling your read method, buf should contain &quot;abc&quot;. We read a total of 3 characters from the file, so return 3.
sol.read(buf, 1); // We have reached the end of file, no more characters can be read. So return 0.
</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<ul>
	<li>Consider that you <strong>cannot</strong> manipulate the file directly, the file is only accesible for <code>read4</code> but&nbsp;<strong>not</strong> for <code>read</code>.</li>
	<li>The <code>read</code> function may be called <strong>multiple times</strong>.</li>
	<li>Please remember to <b>RESET</b> your class variables declared in Solution, as static/class variables are <b>persisted across multiple test cases</b>. Please see <a href="https://leetcode.com/faq/" target="_blank">here</a> for more details.</li>
	<li>You may assume the destination buffer array,&nbsp;<code>buf</code>,&nbsp;is guaranteed to have enough&nbsp;space for storing&nbsp;<em>n</em>&nbsp;characters.</li>
	<li>It is guaranteed that in a given test case the same buffer <code>buf</code> is called by <code>read</code>.</li>
</ul>

</div>

## Tags
- String (string)

## Companies
- Facebook - 25 (taggedByAdmin: true)
- Lyft - 7 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: true)
- Oracle - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Pinterest - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Bloomberg - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### A simple Java code
- Author: totalheap
- Creation Date: Sun Jan 11 2015 04:38:09 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 12:42:02 GMT+0800 (Singapore Standard Time)

<p>
        private int buffPtr = 0;
        private int buffCnt = 0;
        private char[] buff = new char[4];
        public int read(char[] buf, int n) {
            int ptr = 0;
            while (ptr < n) {
                if (buffPtr == 0) {
                    buffCnt = read4(buff);
                }
                if (buffCnt == 0) break;
                while (ptr < n && buffPtr < buffCnt) {
                    buf[ptr++] = buff[buffPtr++];
                }
                if (buffPtr >= buffCnt) buffPtr = 0;
            }
            return ptr;
        }


I used buffer pointer (buffPtr) and buffer Counter (buffCnt) to store the data received in previous calls. In the while loop, if buffPtr reaches current buffCnt, it will be set as zero to be ready to read new data.
</p>


### Clean solution in Java
- Author: lzb700m
- Creation Date: Fri Jul 29 2016 06:33:34 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 20 2018 22:43:50 GMT+0800 (Singapore Standard Time)

<p>
Keep a buffer of size 4 as a class variable, call it **prevBuf**.
Whenever we call read(n), read from **prevBuf** first until all characters in **prevBuf** are consumed (to do this, we need 2 more int variables **prevSize** and **prevIndex**, which tracks the actual size of **prevBuf** and the index of next character to read from **prevBuf**). Then call read4 to read characters into **prevBuf**.
The code is quite clean I think.
```
/* The read4 API is defined in the parent class Reader4.
      int read4(char[] buf); */
// 2ms
// beats 58%
public class Solution extends Reader4 {
    /**
     * @param buf Destination buffer
     * @param n   Maximum number of characters to read
     * @return    The number of characters read
     */
    char[] prevBuf = new char[4];
    int prevSize = 0;
    int prevIndex = 0;
    
    public int read(char[] buf, int n) {
        int counter = 0;
        
        while (counter < n) {
            if (prevIndex < prevSize) {
                buf[counter++] = prevBuf[prevIndex++];
            } else {
                prevSize = read4(prevBuf);
                prevIndex = 0;
                if (prevSize == 0) {
                    // no more data to consume from stream
                    break;
                }
            }
        }
        return counter;
    }
}
```
</p>


### The missing clarification you wish the question provided
- Author: kodewithklossy
- Creation Date: Mon Mar 27 2017 16:43:17 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 12:47:19 GMT+0800 (Singapore Standard Time)

<p>
It took me many hours by try and error as well as looking at many solutions to figure out the following 3 key clarifications the question failed to provide.

With the provided clarifications, it's not difficult to think of a solution using a `char buffer[5]` to store results from `read4()` and then read chars from it.

1. `read4()` has its own file pointer, much like `FILE *fp` in C.

```cpp
// file is "abc", initially fp points to 'a'
read(1) // returns buf = "a", now fp points to 'b'
read(1) // returns buf = "b", now fp points to 'c'
read(2) // returns buf = "c", now fp points to end of file
```
2. `char *buf` is **destination** not source, similar to that of `scanf("%s", buf)`, OJ outputs this `buf` value.

3. Each time `read()` is called, we need to provide a new `buf` to store read characters, therefore, the return value of `int read()` is simply the length of `buf`.

```cpp
class Solution {
private:
    int bp = 0;
    int len = 0;
    char buffer[5];
public:
    int read(char *buf, int n) {
        int i = 0;
        while (i < n) {
            if (bp == len) {
                bp = 0;
                len = read4(buffer);
                if (len == 0)
                    break;
            }
            buf[i++] = buffer[bp++];
        }

        return i;
    }
};
```
</p>


