---
title: "Find Customer Referee"
weight: 1491
#id: "find-customer-referee"
---
## Description
<div class="description">
<p>Given a table <code>customer</code> holding customers information and the referee.</p>

<pre>
+------+------+-----------+
| id   | name | referee_id|
+------+------+-----------+
|    1 | Will |      NULL |
|    2 | Jane |      NULL |
|    3 | Alex |         2 |
|    4 | Bill |      NULL |
|    5 | Zack |         1 |
|    6 | Mark |         2 |
+------+------+-----------+
</pre>

<p>Write a query to return the list of customers <b>NOT</b> referred by the person with id &#39;2&#39;.</p>

<p>For the sample data above, the result is:</p>

<pre>
+------+
| name |
+------+
| Will |
| Jane |
| Bill |
| Zack |
+------+
</pre>

</div>

## Tags


## Companies
- Amazon - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution
---
#### Approach: Using `<>`(`!=`) and `IS NULL` [Accepted]

**Intuition**

Some people come out the following solution by intuition.
```sql
SELECT name FROM customer WHERE referee_Id <> 2;
```

However, this query will only return one result:Zack although there are 4 customers not referred by Jane (including Jane herself). All the customers who were referred by nobody at all (`NULL` value in the referee_id column) don’t show up. But why?

**Algorithm**

MySQL uses three-valued logic -- TRUE, FALSE and UNKNOWN. Anything compared to NULL evaluates to the third value: UNKNOWN. That “anything” includes NULL itself! That’s why MySQL provides the `IS NULL` and `IS NOT NULL` operators to specifically check for NULL.

Thus, one more condition 'referee_id IS NULL' should be added to the WHERE clause as below.

**MySQL**

```sql
SELECT name FROM customer WHERE referee_id <> 2 OR referee_id IS NULL;
```
or
```mysql
SELECT name FROM customer WHERE referee_id != 2 OR referee_id IS NULL;
```

**Tips**

The following solution is also wrong for the same reason as mentioned above. The key is to always use `IS NULL` or `IS NOT NULL` operators to specifically check for NULL value.

```sql
SELECT name FROM customer WHERE referee_id = NULL OR referee_id <> 2;
```

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Simple and easy solution
- Author: Alpher
- Creation Date: Sat Jun 24 2017 03:48:27 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Jun 24 2017 03:48:27 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT name 
FROM customer
WHERE referee_id <> 2 OR referee_id IS NULL
```
</p>


### what's wrong with Oracle compiler???
- Author: firstsnow62
- Creation Date: Mon Jul 29 2019 09:13:11 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Jul 29 2019 09:13:11 GMT+0800 (Singapore Standard Time)

<p>
always complaining \'invalid character\' while the query passed without any issue in mysql
</p>


### Fast Solution using IFNULL
- Author: johngroves
- Creation Date: Mon Aug 07 2017 07:47:17 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 07 2017 07:47:17 GMT+0800 (Singapore Standard Time)

<p>
`SELECT name FROM customer WHERE IFNULL(referee_id,0) <> 2;`
</p>


