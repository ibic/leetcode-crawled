---
title: "Print in Order"
weight: 1606
#id: "print-in-order"
---
## Description
<div class="description">
<p>Suppose we have a class:</p>

<pre>
public class Foo {
&nbsp; public void first() { print(&quot;first&quot;); }
&nbsp; public void second() { print(&quot;second&quot;); }
&nbsp; public void third() { print(&quot;third&quot;); }
}
</pre>

<p>The same instance of <code>Foo</code> will be passed to three different threads. Thread A will call <code>first()</code>, thread B will call <code>second()</code>, and thread C will call <code>third()</code>. Design a mechanism and modify the program&nbsp;to ensure that&nbsp;<code>second()</code>&nbsp;is executed after&nbsp;<code>first()</code>, and&nbsp;<code>third()</code> is executed after&nbsp;<code>second()</code>.</p>

<p>&nbsp;</p>

<p><strong>Example 1:</strong></p>

<pre>
<b>Input:</b> [1,2,3]
<b>Output:</b> &quot;firstsecondthird&quot;
<strong>Explanation:</strong> There are three threads being fired asynchronously. The input [1,2,3] means thread A calls first(), thread B calls second(), and thread C calls third(). &quot;firstsecondthird&quot; is the correct output.
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<b>Input:</b> [1,3,2]
<b>Output:</b> &quot;firstsecondthird&quot;
<strong>Explanation:</strong> The input [1,3,2] means thread A calls first(), thread B calls third(), and thread C calls second(). &quot;firstsecondthird&quot; is the correct output.</pre>

<p>&nbsp;</p>

<p><strong>Note:</strong></p>

<p>We do not know how the threads will be scheduled in the operating system, even though the numbers in the input seems to imply the ordering. The input format you see is mainly&nbsp;to ensure our tests&#39; comprehensiveness.</p>

</div>

## Tags


## Companies
- Adobe - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Amazon - 4 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)
- Google - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

--- 

#### Problems of Concurrency

>The concurrency problems arise from the scenario of [concurrent computing](https://en.wikipedia.org/wiki/Concurrent_computing), where the execution of a program is conducted in multiple processes (or threads) *simultaneously*.

By simultaneousness, the processes or threads are not necessarily running independently in different physical CPUs, but more often they interleave in the same physical CPU. _Note that, the concurrency could apply to either process or thread, we use the words of "process" and "thread" interchangeably in the following sections._

The concurrency is designed to above all enable multitasking, yet it could easily bring some bugs into the program if not applied properly. Depending on the consequences, the problems caused by concurrency can be categorized into three types:

- **race conditions**: the program ends with an undesired output, resulting from the sequence of execution among the processes.
<br/>
- **deadlocks**: the concurrent processes wait for some necessary resources from each other. As a result, none of them can make progress.
<br/>
- **resource starvation**: a process is perpetually denied necessary resources to progress its works.  

In particular, our problem here can be attributed to the race conditions. Before diving into the solutions, we show an example of race condition.

Suppose we have a function called `withdraw(amount)` which deduces certain amount of money from the balance, if the demanding amount is less than the current balance. At the end, the function returns the remaining balance. The function is defined as follows:

<iframe src="https://leetcode.com/playground/KE7gxq3s/shared" frameBorder="0" width="100%" height="174" name="KE7gxq3s"></iframe>

As we can see, in the normal case, we expect that the `balance` would never become negative after the execution of the function, which is also the *desired* behavior of the function. 

However, unfortunately we could run into a *race condition* where the `balance` becomes negative. Here is how it could happen. Imagine we have two threads invoking the function at the same time with different input parameters, _e.g._ for thread #1, `withdraw(amount=400)` and for thread #2, `withdraw(amount=200)`. The execution of the two threads is scheduled as  the graph below, where at each time instance, we run exclusively only a statement from either threads.

![pic](../Figures/1114/1114_race_condition.png)

As one can see, at the end of the above execution flow, we would end up with a negative balance, which is not a desired output.
<br/>
<br/>


#### Race-free Concurrency

The concurrency problems share one common characteristic: multiple processes/threads share some resources (_e.g._ the variable `balance`). Since we cannot eliminate the constraint of resource sharing, the key to prevent the concurrency problems boils down to **_the coordination of resource sharing_**.

The idea is that if we could ensure **_the exclusivity of certain critical code section_** (_e.g._ the statements to check and deduce the balance), we could prevent the program from running into certain inconsistent states.

>The solution to the race condition becomes clear: we need **certain mechanism** that could enforce the exclusivity of certain critical code section, _i.e._ at a given time, only one thread can enter the critical section. 

One can consider the mechanism as a sort of **_lock_** that restricts the access of the critical section. Following the previous example, we apply the lock on the critical section, _i.e._ the statements of balance check and balance deduction. We then rerun the two threads, which could lead to the following flow:

![pic](../Figures/1114/1114_lock.png)

With the mechanism, once a thread enters the critical section, it would prevent other threads from entering the same critical section. For example, at the timestamp #3, the `thread #2` enters the critical section. Then at the next timestamp #4, the `thread #1` could have sneaked into the _dangerous_ critical section if the statement was not protected by the lock. At the end, the two threads run concurrently, while the consistency of the system is maintained, _i.e._ the balance remains positive.

If the thread is not granted with the access of the critical section, we can say that the thread is _blocked_ or put into _sleep_, _e.g._ the `thread #1` is blocked at the timestamp #4. As one can imagine, once the critical section is released, _it would be nice to notify the waiting threads_. For instance, as soon as the `thread #2` releases the critical section at the timestamp #5, the `thread #1` got notified to take over the critical section.
>As a result, it is often the case that the mechanism also comes with the capability to wake up those waiting peers.

To summarize, in order to prevent the race condition in concurrency, we need a mechanism that possess two capabilities: 1). access control on critical section. 2). notification to the blocking threads.
<br/>
<br/>


---
#### Approach 1: Pair Synchronization

**Intuition**

The problem asks us to complete three jobs in order, while each job is running in a separated thread. In order to enforce the execution sequence of the jobs, we could create some dependencies between pairs of jobs, _i.e._ the second job should depend on the completion of the first job and the third job should depend on the completion of the second job.
>The dependency between pairs of jobs construct a *partial order* on the execution sequence of all jobs, _e.g._ with `A < B`, `B < C`, we could obtain the sequence of `A < B < C`.

![pic](../Figures/1114/1114_partial_order.png)

The dependency can be implemented by the concurrency mechanism as we discussed in the previous section. The idea is that we could use a shared variable named `firstJobDone` to coordinate the execution order between the first job and the second job. Similarly, we could use another variable `secondJobDone` to enforce the order of execution between the second and the third jobs.

**Algorithm**

- First of all, we initialize the coordination variables `firstJobDone` and `secondJobDone`, to indicate that the jobs are not done yet.
<br/>
- In the `first()` function, we have no dependency so that we could get straight down to the job. At the end of the function, we then update the variable `firstJobDone` to indicate that the first job is done.
<br/>
- In the `second()` function, we check the status of `firstJobDone`. If it is not updated, we then wait, otherwise we proceed to the task of the second job. And at the end of function, we update the variable `secondJobDone` to mark the completion of the second job.
<br/>
- In the `third()` function, we check the status of the `secondJobDone`. Similarly as the `second()` function, we wait for the signal of the `secondJobDone`, before proceeding to the task of the third job.
<br/>
![pic](../Figures/1114/1114_flow.png)

**Implementation**

The implementation of the above algorithm heavily depends on the programming language that one chooses, since different languages provide different **constructs** for the concurrency mechanism. Though some of the constructs such as [mutex](https://en.wikipedia.org/wiki/Mutual_exclusion) and [semaphore](https://en.wikipedia.org/wiki/Semaphore_(programming)) are present across several programming languages including Java, C++ and Python.

Here we provide a few examples using different constructs across the languages. In particular, one could find a nice [summary](https://leetcode.com/problems/print-in-order/discuss/335939/5-Python-threading-solutions-(Barrier-Lock-Event-Semaphore-Condition)-with-explanation) in the Discussion forum about the concurrency constructs in Python.

<iframe src="https://leetcode.com/playground/Az6t9NHa/shared" frameBorder="0" width="100%" height="500" name="Az6t9NHa"></iframe>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### 5 Python threading solutions (Barrier, Lock, Event, Semaphore, Condition) with explanation
- Author: mereck
- Creation Date: Tue Jul 16 2019 03:21:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Aug 26 2019 21:47:48 GMT+0800 (Singapore Standard Time)

<p>

Raise two barriers. Both wait for two threads to reach them.

First thread can print before reaching the first barrier. Second thread can print before reaching the second barrier. Third thread can print after the second barrier.

```
from threading import Barrier

class Foo:
    def __init__(self):
        self.first_barrier = Barrier(2)
        self.second_barrier = Barrier(2)
            
    def first(self, printFirst):
        printFirst()
        self.first_barrier.wait()
        
    def second(self, printSecond):
        self.first_barrier.wait()
        printSecond()
        self.second_barrier.wait()
            
    def third(self, printThird):
        self.second_barrier.wait()
        printThird()
 ```
 
Start with two locked locks. First thread unlocks the first lock that the second thread is waiting on. Second thread unlocks the second lock that the third thread is waiting on.

```
from threading import Lock

class Foo:
    def __init__(self):
        self.locks = (Lock(),Lock())
        self.locks[0].acquire()
        self.locks[1].acquire()
        
    def first(self, printFirst):
        printFirst()
        self.locks[0].release()
        
    def second(self, printSecond):
        with self.locks[0]:
            printSecond()
            self.locks[1].release()
            
            
    def third(self, printThird):
        with self.locks[1]:
            printThird()
```        
Set events from first and second threads when they are done. Have the second thread wait for first one to set its event. Have the third thread wait on the second thread to raise its event.

```
from threading import Event

class Foo:
    def __init__(self):
        self.done = (Event(),Event())
        
    def first(self, printFirst):
        printFirst()
        self.done[0].set()
        
    def second(self, printSecond):
        self.done[0].wait()
        printSecond()
        self.done[1].set()
            
    def third(self, printThird):
        self.done[1].wait()
        printThird()

```        
Start with two closed gates represented by 0-value semaphores. Second and third thread are waiting behind these gates. When the first thread prints, it opens the gate for the second thread. When the second thread prints, it opens the gate for the third thread.

```
from threading import Semaphore

class Foo:
    def __init__(self):
        self.gates = (Semaphore(0),Semaphore(0))
        
    def first(self, printFirst):
        printFirst()
        self.gates[0].release()
        
    def second(self, printSecond):
        with self.gates[0]:
            printSecond()
            self.gates[1].release()
            
    def third(self, printThird):
        with self.gates[1]:
            printThird()

 ```        

Have all three threads attempt to acquire an RLock via Condition. The first thread can always acquire a lock, while the other two have to wait for the `order` to be set to the right value. First thread sets the order after printing which signals for the second thread to run. Second thread does the same for the third.

```
from threading import Condition

class Foo:
    def __init__(self):
        self.exec_condition = Condition()
        self.order = 0
        self.first_finish = lambda: self.order == 1
        self.second_finish = lambda: self.order == 2

    def first(self, printFirst):
        with self.exec_condition:
            printFirst()
            self.order = 1
            self.exec_condition.notify(2)

    def second(self, printSecond):
        with self.exec_condition:
            self.exec_condition.wait_for(self.first_finish)
            printSecond()
            self.order = 2
            self.exec_condition.notify()

    def third(self, printThird):
        with self.exec_condition:
            self.exec_condition.wait_for(self.second_finish)
            printThird()
```
            

</p>


### [Java] Basic semaphore solution - 8ms, 36MB
- Author: lk00100100
- Creation Date: Fri Jul 12 2019 09:19:55 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Jul 19 2019 03:41:56 GMT+0800 (Singapore Standard Time)

<p>
"Semaphore is a bowl of marbles" - Professor Stark

1. Semaphore is a bowl of marbles (or locks in this case). If you need a marble, and there are none, you wait. You wait until there is one marble and then you take it. If you release(), you will add one marble to the bowl (from thin air). If you release(100), you will add 100 marbles to the bowl (from thin air).
2. The thread calling third() will wait until the end of second() when it releases a \'3\' marble. The second() will wait until the end of first() when it releases a \'2\' marble. Since first() never acquires anything, it will never wait. There is a forced wait ordering.
3. With semaphores, you can start out with 1 marble or 0 marbles or 100 marbles. A thread can take marbles (up until it\'s empty) or put many marbles (out of thin air) at a time.

Upvote and check out my other concurrency solutions.
```
import java.util.concurrent.*;
class Foo {
    Semaphore run2, run3;

    public Foo() {
        run2 = new Semaphore(0);
        run3 = new Semaphore(0);
    }

    public void first(Runnable printFirst) throws InterruptedException {
        printFirst.run();
        run2.release();
    }

    public void second(Runnable printSecond) throws InterruptedException {
        run2.acquire();
        printSecond.run();
        run3.release();
    }

    public void third(Runnable printThird) throws InterruptedException {
        run3.acquire(); 
        printThird.run();
    }
}
```
</p>


### [C++]Why most of the solutions using mutex are wrong+solution
- Author: dev1988
- Creation Date: Thu Jul 25 2019 22:23:39 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Jul 25 2019 22:23:39 GMT+0800 (Singapore Standard Time)

<p>
I actually started learning about threads recently only and even my first attempt was to take 2 mutex and lock/unlock them in order desired by us. That solution is actually accepted in leetcode.

However, while researching for difference between mutex and conditional_variable usage, i realised that the way mutex are being used here are totally wrong.

Some points that must be taken note of are: 
* **Mutex** are used for **mutual exclusion** i.e to safe gaurd the critical sections of a code.
* **Semaphone/condition_variable** are used for **thread synchronisation**(which is what we want to achieve here).
* **Mutex have ownership assigned with them**, that is to say, *the thread that locks a mutex must only unlock it.* Also, we must not unlock a mutex that has not been locked **(This is what most programs have got wrong)**.
* If the mutex is not used as said above, **the behavior is undefined**, which however in our case produces the required result.

References:
1. [If the mutex is not currently locked by the calling thread, it causes undefined behavior.](http://www.cplusplus.com/reference/mutex/mutex/unlock/)
2. [The precondition for calling unlock is holding an ownership of the mutex, according to (std)30.4.1.2](https://stackoverflow.com/questions/43487357/how-stdmutex-got-unlocked-in-different-thread)

I did read few more places the same thing, but i think these do put the point across :)

Now my solution using a condition_variable:

```
class Foo {
public:
    int count = 0;
    mutex mtx;
    condition_variable cv;
    Foo() {
        count = 1;
        //cv.notify_all();
    }

    void first(function<void()> printFirst) {
        
        unique_lock<mutex> lck(mtx);
		// No point of this wait as on start count will be 1, we need to make the other threads wait.
        // while(count != 1){
        //     cv.wait(lck);
        // }
        // printFirst() outputs "first". Do not change or remove this line.

        printFirst();
        count = 2;
        cv.notify_all();
    }

    void second(function<void()> printSecond) {
        unique_lock<mutex> lck(mtx);
        while(count != 2){
            cv.wait(lck);
        }
        // printSecond() outputs "second". Do not change or remove this line.
        printSecond();
        count = 3;
        cv.notify_all();
    }

    void third(function<void()> printThird) {
        unique_lock<mutex> lck(mtx);
        while(count != 3){
            cv.wait(lck);
        }
        // printThird() outputs "third". Do not change or remove this line.
        printThird();
    }
};
```

**Disclaimer**:  I am still learning :P. So, please do enlighten me if you think my understanding of this topic is wrong/misleading.
Thanks for reading :)
</p>


