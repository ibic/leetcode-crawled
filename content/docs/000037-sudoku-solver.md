---
title: "Sudoku Solver"
weight: 37
#id: "sudoku-solver"
---
## Description
<div class="description">
<p>Write a program to solve a Sudoku puzzle by filling the empty cells.</p>

<p>A&nbsp;sudoku solution must satisfy <strong>all of&nbsp;the following rules</strong>:</p>

<ol>
	<li>Each of the digits&nbsp;<code>1-9</code> must occur exactly&nbsp;once in each row.</li>
	<li>Each of the digits&nbsp;<code>1-9</code>&nbsp;must occur&nbsp;exactly once in each column.</li>
	<li>Each of the digits&nbsp;<code>1-9</code> must occur exactly once in each of the 9 <code>3x3</code> sub-boxes of the grid.</li>
</ol>

<p>The <code>&#39;.&#39;</code> character indicates empty cells.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Sudoku-by-L2G-20050714.svg/250px-Sudoku-by-L2G-20050714.svg.png" style="height:250px; width:250px" />
<pre>
<strong>Input:</strong> board = [[&quot;5&quot;,&quot;3&quot;,&quot;.&quot;,&quot;.&quot;,&quot;7&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;6&quot;,&quot;.&quot;,&quot;.&quot;,&quot;1&quot;,&quot;9&quot;,&quot;5&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;9&quot;,&quot;8&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;6&quot;,&quot;.&quot;],[&quot;8&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;6&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;3&quot;],[&quot;4&quot;,&quot;.&quot;,&quot;.&quot;,&quot;8&quot;,&quot;.&quot;,&quot;3&quot;,&quot;.&quot;,&quot;.&quot;,&quot;1&quot;],[&quot;7&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;2&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;6&quot;],[&quot;.&quot;,&quot;6&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;2&quot;,&quot;8&quot;,&quot;.&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;4&quot;,&quot;1&quot;,&quot;9&quot;,&quot;.&quot;,&quot;.&quot;,&quot;5&quot;],[&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;.&quot;,&quot;8&quot;,&quot;.&quot;,&quot;.&quot;,&quot;7&quot;,&quot;9&quot;]]
<strong>Output:</strong> [[&quot;5&quot;,&quot;3&quot;,&quot;4&quot;,&quot;6&quot;,&quot;7&quot;,&quot;8&quot;,&quot;9&quot;,&quot;1&quot;,&quot;2&quot;],[&quot;6&quot;,&quot;7&quot;,&quot;2&quot;,&quot;1&quot;,&quot;9&quot;,&quot;5&quot;,&quot;3&quot;,&quot;4&quot;,&quot;8&quot;],[&quot;1&quot;,&quot;9&quot;,&quot;8&quot;,&quot;3&quot;,&quot;4&quot;,&quot;2&quot;,&quot;5&quot;,&quot;6&quot;,&quot;7&quot;],[&quot;8&quot;,&quot;5&quot;,&quot;9&quot;,&quot;7&quot;,&quot;6&quot;,&quot;1&quot;,&quot;4&quot;,&quot;2&quot;,&quot;3&quot;],[&quot;4&quot;,&quot;2&quot;,&quot;6&quot;,&quot;8&quot;,&quot;5&quot;,&quot;3&quot;,&quot;7&quot;,&quot;9&quot;,&quot;1&quot;],[&quot;7&quot;,&quot;1&quot;,&quot;3&quot;,&quot;9&quot;,&quot;2&quot;,&quot;4&quot;,&quot;8&quot;,&quot;5&quot;,&quot;6&quot;],[&quot;9&quot;,&quot;6&quot;,&quot;1&quot;,&quot;5&quot;,&quot;3&quot;,&quot;7&quot;,&quot;2&quot;,&quot;8&quot;,&quot;4&quot;],[&quot;2&quot;,&quot;8&quot;,&quot;7&quot;,&quot;4&quot;,&quot;1&quot;,&quot;9&quot;,&quot;6&quot;,&quot;3&quot;,&quot;5&quot;],[&quot;3&quot;,&quot;4&quot;,&quot;5&quot;,&quot;2&quot;,&quot;8&quot;,&quot;6&quot;,&quot;1&quot;,&quot;7&quot;,&quot;9&quot;]]
<strong>Explanation:</strong>&nbsp;The input board is shown above and the only valid solution is shown below:

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Sudoku-by-L2G-20050714_solution.svg/250px-Sudoku-by-L2G-20050714_solution.svg.png" style="height:250px; width:250px" />
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>board.length == 9</code></li>
	<li><code>board[i].length == 9</code></li>
	<li><code>board[i][j]</code> is a digit or <code>&#39;.&#39;</code>.</li>
	<li>It is <strong>guaranteed</strong> that the input board has only one solution.</li>
</ul>

</div>

## Tags
- Hash Table (hash-table)
- Backtracking (backtracking)

## Companies
- Amazon - 6 (taggedByAdmin: false)
- Apple - 5 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: false)
- DoorDash - 3 (taggedByAdmin: false)
- Adobe - 2 (taggedByAdmin: false)
- Roblox - 2 (taggedByAdmin: false)
- Oracle - 4 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Wish - 2 (taggedByAdmin: false)
- VMware - 6 (taggedByAdmin: false)
- Arista Networks - 4 (taggedByAdmin: false)
- Expedia - 2 (taggedByAdmin: false)
- Riot Games - 2 (taggedByAdmin: false)
- Uber - 0 (taggedByAdmin: true)
- Snapchat - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---

#### Approach 0: Brute Force

The first idea is to use brut-force 
to generate all possible ways to fill the cells
with numbers from `1` to `9`,
and then check them to keep the solution only. 
That means $$9^{81}$$ operations to do, 
where $$9$$ is a number of available digits 
and $$81$$ is a number of cells to fill.
Hence we're forced to think further how to optimize.
<br />
<br />


---
#### Approach 1: Backtracking

**Conceptions to use**

There are two programming conceptions here which could
help.

> The first one is called _constrained programming_. 

That basically means
to put restrictions after each number placement. One puts a number on the 
board and that immediately excludes this number from further 
usage in the current _row_, _column_ and _sub-box_. That propagates 
_constraints_ and helps to reduce the number of combinations to consider.

![bla](../Figures/37/37_const3.png)

> The second one called _backtracking_. 

Let's imagine that one has already managed to
put several numbers on the board. 
But the combination chosen is not the optimal one and there is no way 
to place the further numbers. What to do? _To backtrack_. 
That means to come back,
to change the previously placed number and try 
to proceed again. If that would not work either, _backtrack_ again.

![bla](../Figures/37/37_backtrack2.png)

**How to enumerate sub-boxes**

> One tip to enumerate sub-boxes: 
let's use `box_index = (row / 3) * 3 + column / 3` 
where `/` is an integer division.

<img src="../Figures/36/36_boxes_2.png" width="500">

**Algorithm**

Now everything is ready to write down the backtrack function 
`backtrack(row = 0, col = 0)`.

* Start from the upper left cell `row = 0, col = 0`. Proceed till the
first free cell.
* Iterate over the numbers from `1` to `9` 
and try to put each number `d` in the `(row, col)` cell.

    * If number `d` is not yet in the current row, column and box :
        
        * Place the `d` in a `(row, col)` cell.
        * Write down that `d` is now present in the current row, column and box.
        * If we're on the last cell `row == 8, col == 8` :
            * That means that we've solved the sudoku.
        * Else
            * Proceed to place further numbers.
        * Backtrack if the solution is not yet here : 
        remove the last number from the `(row, col)` cell.

**Implementation**

<iframe src="https://leetcode.com/playground/9T3dE2W6/shared" frameBorder="0" width="100%" height="500" name="9T3dE2W6"></iframe>

**Complexity Analysis**

* Time complexity is constant here since the board size is fixed and there is no 
N-parameter to measure. 
Though let's discuss the number of operations needed : $$(9!)^9$$. 
Let's consider one row, i.e. not more than $$9$$ cells to fill. 
There are not more than $$9$$ possibilities for the first number to put,
not more than $$9 \times 8$$ for the second one,
not more than $$9 \times 8 \times 7$$ for the third one etc. In total that
results in not more than $$9!$$ possibilities for a just one row,
that means not more than $$(9!)^9$$ operations in total.
Let's compare:

    - $$9^{81} = 196627050475552913618075908526912116283103450944214766927315415537966391196809$$
for the brute force, 

    - and $$(9!)^9 = 109110688415571316480344899355894085582848000000000$$
for the standard backtracking,
i.e. the number of operations is reduced in $$10^{27}$$ times !

* Space complexity : the board size is fixed, and the space is used 
to store board, rows, columns and boxes structures, each contains `81` elements.

## Accepted Submission (python3)
```python3
class Solution:
    AllDigits = set(map(str, range(1,10)))
    def boardToNum(self, board):
        bn = [[int(c) if c != '.' else 0 for c in row] for row in board]
        return bn
    
    def boardFromNum(self, board):
        bn = [[str(i) if i != 0 else '.' for c in row] for row in board]
        return bn
    
    def getLines(self, board, x, y):
        rows = []
        for yy in range(y+1):
            rows.append(list(filter(lambda e: e != '.', board[yy][:x+1])))
        cols = []
        for xx in range(x + 1):
            cols.append(list(filter(lambda e: e != '.', [board[yy][xx] for yy in range(y + 1)])))
        squares = []
        for y3 in range((y + 1) // 3):
            for x3 in range((x + 1) // 3):
                ta = []
                for yy in range(y3 * 3, y3 * 3 + 3):
                    for xx in range(x3 * 3, x3 * 3 + 3):
                        if board[yy][xx] != '.':
                            ta.append(board[yy][xx])
                squares.append(ta)
        return rows + cols + squares
    
    def hasDup(self, line):
        aset = set()
        for e in line:
            if e in aset:
                return True
            aset.add(e)
        return False
    
    def isValid(self, board, x, y):
        for line in self.getLines(board, x, y):
            if self.hasDup(line):
                return False
        return True

    def prevPos(self, x, y):
        if x > 0:
            return (x-1, y)
        elif y > 0:
            return (8, y - 1)
        else:
            return (-1, -1)

    def getPrevDot(self, board, x, y):
        for xx in range(x, -1, -1):
            if board[y][xx] == '.':
                return (xx, y)
        for yy in range(y, -1, -1):
            for xx in range(9):
                if board[yy][xx] == '.':
                    return (xx, yy)
        return (-1, -1)

    def nextPos(self, x, y):
        if x < 8:
            return (x+1, y)
        elif y < 8:
            return (0, y + 1)
        else:
            return (-1, -1)

    def getNextDot(self, board, x, y):
        for xx in range(x, 9):
            if board[y][xx] == '.':
                return (xx, y)
        for yy in range(y + 1, 9):
            for xx in range(9):
                if board[yy][xx] == '.':
                    return (xx, yy)
        return (-1, -1)

    def getRemaining(self, board, x, y):
        existing = set()
        existing.update(board[y])
        for dy in range(9):
            existing.add(board[dy][x])
        ys = y // 3 * 3
        xs = x // 3 * 3
        for yy in range(ys, ys + 3):
            for xx in range(xs, xs + 3):
                existing.add(board[yy][xx])
        remaining = Solution.AllDigits - existing
        return remaining

    def btSlower(self, board, x, y):
        if (x, y) == (-1, -1):
            return True
        remaining = self.getRemaining(board, x, y)
        if len(remaining) == 0:
            return False
        else:
            nx, ny = self.getNextDot(board, *self.nextPos(x, y))
            for n in remaining:
                board[y][x] = n
                if self.btSlower(board, nx, ny):
                    return True
            board[y][x] = '.'
            return False
                
    def buildFillerList(self, board):
        li = []
        for y in range(9):
            for x in range(9):
                if board[y][x] == '.':
                    li.append((x, y))
        return li

    def bt(self, board, fillerPos):
        if fillerPos == self.fillerLen:
            return True
        x, y = self.fillerList[fillerPos]
        remaining = self.getRemaining(board, x, y)
        if len(remaining) == 0:
            return False
        else:
            for n in remaining:
                board[y][x] = n
                if self.bt(board, fillerPos + 1):
                    return True
            board[y][x] = '.'
            return False

    def btIterative(self, board):
        i = 0
        candidates = [[] for _ in range(self.fillerLen)]
        calculated = [False for _ in range(self.fillerLen)]
        while i >= 0:
            if i >= self.fillerLen:
                return True
            x, y = self.fillerList[i]
            if not calculated[i]:
                candidates[i] = self.getRemaining(board, x, y)
                calculated[i] = True
            candi = candidates[i]
            if len(candi) == 0:
                board[y][x] = '.'
                calculated[i] = False
                i -= 1
            else:
                can = candi.pop()
                board[y][x] = can
                i += 1
        return False

    def solveSudoku(self, board):
        """
        :type board: List[List[str]]
        :rtype: void Do not return anything, modify board in-place instead.
        """
        #self.fillerList = []
        #self.fillerLen = 0
        #x, y = self.getNextDot(board, 0, 0)
        #self.btSlower(board, x, y)
        self.fillerList = self.buildFillerList(board)
        self.fillerLen = len(self.fillerList)
        #self.bt(board, 0)
        self.btIterative(board)

```

## Top Discussions
### Straight Forward Java Solution Using Backtracking
- Author: vinebranch
- Creation Date: Sat Apr 04 2015 09:19:45 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 12:22:53 GMT+0800 (Singapore Standard Time)

<p>
Try 1 through 9 for each cell. The time complexity should be 9 ^ m (m represents the number of blanks to be filled in), since each blank can have 9 choices. Details see comments inside code.

```
public class Solution {
    public void solveSudoku(char[][] board) {
        if(board == null || board.length == 0)
            return;
        solve(board);
    }
    
    public boolean solve(char[][] board){
        for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[0].length; j++){
                if(board[i][j] == '.'){
                    for(char c = '1'; c <= '9'; c++){//trial. Try 1 through 9
                        if(isValid(board, i, j, c)){
                            board[i][j] = c; //Put c for this cell
                            
                            if(solve(board))
                                return true; //If it's the solution return true
                            else
                                board[i][j] = '.'; //Otherwise go back
                        }
                    }
                    
                    return false;
                }
            }
        }
        return true;
    }
    
    private boolean isValid(char[][] board, int row, int col, char c){
        for(int i = 0; i < 9; i++) {
            if(board[i][col] != '.' && board[i][col] == c) return false; //check row
            if(board[row][i] != '.' && board[row][i] == c) return false; //check column
            if(board[3 * (row / 3) + i / 3][ 3 * (col / 3) + i % 3] != '.' && 
board[3 * (row / 3) + i / 3][3 * (col / 3) + i % 3] == c) return false; //check 3*3 block
        }
        return true;
    }
}
```
</p>


### Sharing my 2ms C++ solution with comments and explanations.
- Author: 0xF4
- Creation Date: Tue Jan 13 2015 05:00:36 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 13 2018 17:21:17 GMT+0800 (Singapore Standard Time)

<p>
Update: there's a [follow-up 0ms solution which is even more optimized][1]

This is one of the fastest Sudoku solvers I've ever written. It is compact enough - just 150 lines of C++ code with comments. I thought it'd be interesting to share it, since it combines several techniques like reactive network update propagation and backtracking with very aggressive pruning.

The algorithm is online - it starts with an empty board and as you add numbers to it, it starts solving the Sudoku.

Unlike in other solutions where you have bitmasks of allowed/disallowed values per row/column/square, this solution track bitmask for every(!) cell, forming a set of constraints for the allowed values for each particular cell. Once a value is written into a cell, new constraints are immediately propagated to row, column and 3x3 square of the cell. If during this process a value of other cell can be unambiguously deduced - then the value is set, new constraints are propagated, so on.... You can think about this as an implicit reactive network of cells.

If we're lucky (and we'll be lucky for 19 of 20 of Sudokus published in magazines) then Sudoku is solved at the end (or even before!) processing of the input.

Otherwise, there will be empty cells which have to be resolved. Algorithm uses backtracking for this purpose. To optimize it, algorithm starts with the cell with the smallest ambiguity. This could be improved even further by using priority queue (but it's not implemented here). Backtracking is more or less standard, however, at each step we guess the number, the reactive update propagation comes back into play and it either quickly proves that the guess is unfeasible or significantly prunes the remaining search space.

It's interesting to note, that in this case taking and restoring snapshots of the compact representation of the state is faster than doing backtracking rollback by "undoing the moves".

    class Solution {
    	struct cell // encapsulates a single cell on a Sudoku board
    	{
    		uint8_t value; // cell value 1..9 or 0 if unset
    		// number of possible (unconstrained) values for the cell
    		uint8_t numPossibilities;
    		// if bitset[v] is 1 then value can't be v
    		bitset<10> constraints;
    		cell() : value(0), numPossibilities(9),constraints() {};
    	};
    	array<array<cell,9>,9> cells;
    
    	// sets the value of the cell to [v]
    	// the function also propagates constraints to other cells and deduce new values where possible
    	bool set(int i, int j, int v)
    	{ 
    		// updating state of the cell
    		cell& c = cells[i][j];
    		if (c.value == v)
    			return true;
    		if (c.constraints[v])
    			return false;
    		c.constraints = bitset<10>(0x3FE); // all 1s
    		c.constraints.reset(v);
    		c.numPossibilities = 1;
    		c.value = v;
    
    		// propagating constraints
    		for (int k = 0; k<9; k++) {
    			// to the row: 
    			if (i != k && !updateConstraints(k, j, v))
    				return false;
    			// to the column:
    			if (j != k && !updateConstraints(i, k, v))
    				return false;
    			// to the 3x3 square:
    			int ix = (i / 3) * 3 + k / 3;
    			int jx = (j / 3) * 3 + k % 3;
    			if (ix != i && jx != j && !updateConstraints(ix, jx, v))
    				return false;
    		}
    		return true;
    	}
    	// update constraints of the cell i,j by excluding possibility of 'excludedValue'
    	// once there's one possibility left the function recurses back into set()
    	bool updateConstraints(int i, int j, int excludedValue)
    	{
    		cell& c = cells[i][j];
    		if (c.constraints[excludedValue]) {
    			return true;
    		}
    		if (c.value == excludedValue) {
    			return false;
    		}
    		c.constraints.set(excludedValue);
    		if (--c.numPossibilities > 1)
    			return true;
    		for (int v = 1; v <= 9; v++) {
    			if (!c.constraints[v]) {
    				return set(i, j, v);
    			}
    		}
    		assert(false);
    	}
    
    	// backtracking state - list of empty cells
    	vector<pair<int, int>> bt;
    
    	// find values for empty cells
    	bool findValuesForEmptyCells()
    	{
    		// collecting all empty cells
    		bt.clear();
    		for (int i = 0; i < 9; i++) {
    			for (int j = 0; j < 9; j++) {
    				if (!cells[i][j].value)
    					bt.push_back(make_pair(i, j));
    			}
    		}
    		// making backtracking efficient by pre-sorting empty cells by numPossibilities
    		sort(bt.begin(), bt.end(), [this](const pair<int, int>&a, const pair<int, int>&b) {
    			return cells[a.first][a.second].numPossibilities < cells[b.first][b.second].numPossibilities; });
    		return backtrack(0);
    	}
    
    	// Finds value for all empty cells with index >=k
    	bool backtrack(int k)
    	{
    		if (k >= bt.size())
    			return true;
    		int i = bt[k].first;
    		int j = bt[k].second;
    		// fast path - only 1 possibility
    		if (cells[i][j].value)
    			return backtrack(k + 1);
    		auto constraints = cells[i][j].constraints;
    		// slow path >1 possibility.
    		// making snapshot of the state
    		array<array<cell,9>,9> snapshot(cells);
    		for (int v = 1; v <= 9; v++) {
    			if (!constraints[v]) {
    				if (set(i, j, v)) {
    					if (backtrack(k + 1))
    						return true;
    				}
    				// restoring from snapshot,
    				// note: computationally this is cheaper
    				// than alternative implementation with undoing the changes
    				cells = snapshot;
    			}
    		}
    		return false;
    	}
    public:
    	void solveSudoku(vector<vector<char>> &board) {
    		cells = array<array<cell,9>,9>(); // clear array
    		// Decoding input board into the internal cell matrix.
    		// As we do it - constraints are propagated and even additional values are set as we go
    		// (in the case if it is possible to unambiguously deduce them).
    		for (int i = 0; i < 9; i++)
    		{
    			for (int j = 0; j < 9; j++) {
    				if (board[i][j] != '.' && !set(i, j, board[i][j] - '0'))
    					return; // sudoku is either incorrect or unsolvable
    			}
    		}
    		// if we're lucky we've already got a solution,
    		// however, if we have empty cells we need to use backtracking to fill them
    		if (!findValuesForEmptyCells())
    			return; // sudoku is unsolvable
    
    		// copying the solution back to the board
    		for (int i = 0; i < 9; i++)
    		{
    			for (int j = 0; j < 9; j++) {
    				if (cells[i][j].value)
    					board[i][j] = cells[i][j].value + '0';
    			}
    		}
    	}
    };


  [1]: https://leetcode.com/discuss/59649/yet-another-0ms-c-solution
</p>


### Accepted Python solution
- Author: xiaoying10101
- Creation Date: Sat Jan 17 2015 13:02:15 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 25 2018 19:55:08 GMT+0800 (Singapore Standard Time)

<p>
    class Solution:
        # @param board, a 9x9 2D array
        # Solve the Sudoku by modifying the input board in-place.
        # Do not return any value.
        def solveSudoku(self, board):
            self.board = board
            self.solve()
        
        def findUnassigned(self):
            for row in range(9):
                for col in range(9):
                    if self.board[row][col] == ".":
                        return row, col
            return -1, -1
        
        def solve(self):
            row, col = self.findUnassigned()
            #no unassigned position is found, puzzle solved
            if row == -1 and col == -1:
                return True
            for num in ["1","2","3","4","5","6","7","8","9"]:
                if self.isSafe(row, col, num):
                    self.board[row][col] = num
                    if self.solve():
                        return True
                    self.board[row][col] = "."
            return False
                
        def isSafe(self, row, col, ch):
            boxrow = row - row%3
            boxcol = col - col%3
            if self.checkrow(row,ch) and self.checkcol(col,ch) and self.checksquare(boxrow, boxcol, ch):
                return True
            return False
        
        def checkrow(self, row, ch):
            for col in range(9):
                if self.board[row][col] == ch:
                    return False
            return True
        
        def checkcol(self, col, ch):
            for row in range(9):
                if self.board[row][col] == ch:
                    return False
            return True
           
        def checksquare(self, row, col, ch):
            for r in range(row, row+3):
                for c in range(col, col+3):
                    if self.board[r][c] == ch:
                        return False
            return True


It's a simple backtracking solution.
</p>


