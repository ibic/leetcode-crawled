---
title: "Split Array into Fibonacci Sequence"
weight: 786
#id: "split-array-into-fibonacci-sequence"
---
## Description
<div class="description">
<p>Given a string <code>S</code>&nbsp;of digits, such as <code>S = &quot;123456579&quot;</code>, we can split it into a <em>Fibonacci-like sequence</em>&nbsp;<code>[123, 456, 579].</code></p>

<p>Formally, a Fibonacci-like sequence is a list&nbsp;<code>F</code> of non-negative integers such that:</p>

<ul>
	<li><code>0 &lt;= F[i] &lt;= 2^31 - 1</code>, (that is,&nbsp;each integer fits a 32-bit signed integer type);</li>
	<li><code>F.length &gt;= 3</code>;</li>
	<li>and<code> F[i] + F[i+1] = F[i+2] </code>for all <code>0 &lt;= i &lt; F.length - 2</code>.</li>
</ul>

<p>Also, note that when splitting the string into pieces, each piece must not have extra leading zeroes, except if the piece is the number 0 itself.</p>

<p>Return any Fibonacci-like sequence split from <code>S</code>, or return <code>[]</code> if it cannot be done.</p>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>&quot;123456579&quot;
<strong>Output: </strong>[123,456,579]
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>&quot;11235813&quot;
<strong>Output: </strong>[1,1,2,3,5,8,13]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>&quot;112358130&quot;
<strong>Output: </strong>[]
<strong>Explanation: </strong>The task is impossible.
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input: </strong>&quot;0123&quot;
<strong>Output: </strong>[]
<strong>Explanation: </strong>Leading zeroes are not allowed, so &quot;01&quot;, &quot;2&quot;, &quot;3&quot; is not valid.
</pre>

<p><strong>Example 5:</strong></p>

<pre>
<strong>Input: </strong>&quot;1101111&quot;
<strong>Output: </strong>[110, 1, 111]
<strong>Explanation: </strong>The output [11, 0, 11, 11] would also be accepted.
</pre>

<p><strong>Note: </strong></p>

<ol>
	<li><code>1 &lt;= S.length&nbsp;&lt;= 200</code></li>
	<li><code>S</code> contains only digits.</li>
</ol>

</div>

## Tags
- String (string)
- Backtracking (backtracking)
- Greedy (greedy)

## Companies
- Amazon - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

---
#### Approach #1: Brute Force [Accepted]

**Intuition**

The first two elements of the array uniquely determine the rest of the sequence.

**Algorithm**

For each of the first two elements, assuming they have no leading zero, let's iterate through the rest of the string.  At each stage, we expect a number less than or equal to `2^31 - 1` that starts with the sum of the two previous numbers.

<iframe src="https://leetcode.com/playground/p4xZdPbF/shared" frameBorder="0" width="100%" height="500" name="p4xZdPbF"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N^2)$$, where $$N$$ is the length of `S`, and with the requirement that the values of the answer are $$O(1)$$ in length.

* Space Complexity:  $$O(N)$$.

## Accepted Submission (python3)
```python3
def pr(*args, **kwargs):
    #print(*args, **kwargs)
    pass

class Solution:
    MaxInt = 2147483647 # pow(2, 31) - 1
    MaxIntLen = 10 # len(str(pow(2, 31)))

    def bt(self, s, lens, result, start):
        pr(result, start)

        lenr = len(result)
        if lenr >= 3:
            if result[-1] != result[-2] + result[-3]:
                return False
            if start == lens:
                return True

        for i in range(start + 1, min(lens, start+Solution.MaxIntLen) + 1):
            pr("i:", i)
            newIntStr = s[start:i]
            newInt = int(newIntStr)
            if newInt > Solution.MaxInt:
                break
            if newIntStr[0] == '0' and i - start > 1:
                break
            result.append(newInt)
            if self.bt(s, lens, result, i):
                return True
            result.pop()
        return False

    def splitIntoFibonacci(self, S):
        """
        :type S: str
        :rtype: List[int]
        """
        result = []
        s = S
        lens = len(s)
        self.bt(s, lens, result, 0)
        return result

```

## Top Discussions
### short and fast backtracking solution
- Author: touchdown
- Creation Date: Sun May 27 2018 12:38:27 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 02 2018 10:34:08 GMT+0800 (Singapore Standard Time)

<p>
    public List<Integer> splitIntoFibonacci(String S) {
        List<Integer> ans = new ArrayList<>();
        helper(S, ans, 0);
        return ans;
    }

    public boolean helper(String s, List<Integer> ans, int idx) {
        if (idx == s.length() && ans.size() >= 3) {
            return true;
        }
        for (int i=idx; i<s.length(); i++) {
            if (s.charAt(idx) == \'0\' && i > idx) {
                break;
            }
            long num = Long.parseLong(s.substring(idx, i+1));
            if (num > Integer.MAX_VALUE) {
                break;
            }
            int size = ans.size();
            // early termination
            if (size >= 2 && num > ans.get(size-1)+ans.get(size-2)) {
                break;
            }
            if (size <= 1 || num == ans.get(size-1)+ans.get(size-2)) {
                ans.add((int)num);
                // branch pruning. if one branch has found fib seq, return true to upper call
                if (helper(s, ans, i+1)) {
                    return true;
                }
                ans.remove(ans.size()-1);
            }
        }
        return false;
    }
</p>


### Well Commented C++ Backtracking Solution
- Author: yasharmaster
- Creation Date: Sun May 27 2018 17:07:27 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 10:13:34 GMT+0800 (Singapore Standard Time)

<p>
```
class Solution {
public:

    bool backtrack(string &S, int start, vector<int> &nums){        
        int n = S.size();
	// If we reached end of string & we have more than 2 elements
	// in our sequence then return true
        if(start >= n && nums.size()>=3){
            return true;
        }
	// Since \'0\' in beginning is not allowed therefore if the current char is \'0\'
	// then we can use it alone only and cannot extend it by adding more chars at the back.
	// Otherwise we make take upto 10 chars (length of INT_MAX)
        int maxSplitSize = (S[start]==\'0\') ? 1 : 10;
	// Try getting a solution by forming a number with \'i\' chars begginning with \'start\'
        for(int i=1; i<=maxSplitSize && start+i<=S.size(); i++){
            long long num = stoll(S.substr(start, i));
            if(num > INT_MAX)
                return false;
            int sz = nums.size();
	// If fibonacci property is not satisfied then we cannot get a solution
            if(sz >= 2 && nums[sz-1]+nums[sz-2] < num)
                return false;
            if(sz<=1 || nums[sz-1]+nums[sz-2]==num){
                nums.push_back(num);
                if(backtrack(S, start+i, nums))
                    return true;
                nums.pop_back();                
            }
        }
        return false;
    }
    
    vector<int> splitIntoFibonacci(string S) {
        vector<int> nums;
	// Backtrack from 0th char
        backtrack(S, 0, nums);
        return nums;
    }
};
```
</p>


### what is wrong with this testcase?
- Author: kkkkcom
- Creation Date: Fri Jun 15 2018 06:55:00 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 16 2018 12:14:48 GMT+0800 (Singapore Standard Time)

<p>
Sorry to bother, but what is wrong with the below testcase?

Input:
"539834657215398346785398346991079669377161950407626991734534318677529701785098211336528511"
Output:
[539834657,21,539834678,539834699,1079669377,1619504076,2699173453,4318677529,7017850982,11336528511]
Expected:
[]

The answer said it should return empty, but the returned list from my code looks fine though ... Couldn\'t figure out what is wrong here ... Can someone help? The code passed 65/70 testcase
</p>


