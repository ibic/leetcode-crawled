---
title: "Possible Bipartition"
weight: 836
#id: "possible-bipartition"
---
## Description
<div class="description">
<p>Given a set of <code>N</code>&nbsp;people (numbered <code>1, 2, ..., N</code>), we would like to split everyone into two groups of <strong>any</strong> size.</p>

<p>Each person may dislike some other people, and they should not go into the same group.&nbsp;</p>

<p>Formally, if <code>dislikes[i] = [a, b]</code>, it means it is not allowed to put the people numbered <code>a</code> and <code>b</code> into the same group.</p>

<p>Return <code>true</code>&nbsp;if and only if it is possible to split everyone into two groups in this way.</p>

<p>&nbsp;</p>

<div>
<div>
<ol>
</ol>
</div>
</div>

<div>
<p><strong>Example 1:</strong></p>

<pre>
<strong>Input: </strong>N = <span id="example-input-1-1">4</span>, dislikes = <span id="example-input-1-2">[[1,2],[1,3],[2,4]]</span>
<strong>Output: </strong><span id="example-output-1">true</span>
<strong>Explanation</strong>: group1 [1,4], group2 [2,3]
</pre>

<div>
<p><strong>Example 2:</strong></p>

<pre>
<strong>Input: </strong>N = <span id="example-input-2-1">3</span>, dislikes = <span id="example-input-2-2">[[1,2],[1,3],[2,3]]</span>
<strong>Output: </strong><span id="example-output-2">false</span>
</pre>

<div>
<p><strong>Example 3:</strong></p>

<pre>
<strong>Input: </strong>N = <span id="example-input-3-1">5</span>, dislikes = <span id="example-input-3-2">[[1,2],[2,3],[3,4],[4,5],[1,5]]</span>
<strong>Output: </strong><span id="example-output-3">false</span>
</pre>
</div>
</div>
</div>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= N &lt;= 2000</code></li>
	<li><code>0 &lt;= dislikes.length &lt;= 10000</code></li>
	<li><code>dislikes[i].length == 2</code></li>
	<li><code>1 &lt;= dislikes[i][j] &lt;= N</code></li>
	<li><code>dislikes[i][0] &lt; dislikes[i][1]</code></li>
	<li>There does not exist <code>i != j</code> for which <code>dislikes[i] == dislikes[j]</code>.</li>
</ul>
</div>

## Tags
- Depth-first Search (depth-first-search)
- Graph (graph)

## Companies
- Google - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Amazon - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Microsoft - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution
---
#### Approach 1: Depth-First Search

**Intuition**

It's natural to try to assign everyone to a group.  Let's say people in the first group are red, and people in the second group are blue.

If the first person is red, anyone disliked by this person must be blue.  Then, anyone disliked by a blue person is red, then anyone disliked by a red person is blue, and so on.

If at any point there is a conflict, the task is impossible, as every step logically follows from the first step.  If there isn't a conflict, then the coloring was valid, so the answer would be `true`.

**Algorithm**

Consider the graph on `N` people formed by the given "dislike" edges.  We want to check that each connected component of this graph is bipartite.

For each connected component, we can check whether it is bipartite by just trying to coloring it with two colors.  How to do this is as follows: color any node red, then all of it's neighbors blue, then all of those neighbors red, and so on.  If we ever color a red node blue (or a blue node red), then we've reached a conflict.

<iframe src="https://leetcode.com/playground/BXt4xWZE/shared" frameBorder="0" width="100%" height="500" name="BXt4xWZE"></iframe>

**Complexity Analysis**

* Time Complexity:  $$O(N + E)$$, where $$E$$ is the length of `dislikes`.

* Space Complexity:  $$O(N + E)$$.
<br />
<br />

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Python sol by DFS and coloring. [w/ Graph]
- Author: brianchiang_tw
- Creation Date: Wed May 27 2020 17:37:47 GMT+0800 (Singapore Standard Time)
- Update Date: Thu May 28 2020 12:40:04 GMT+0800 (Singapore Standard Time)

<p>
Python sol by DFS and coloring.

---

**Hint**:

Think of **graph**, **node coloring**, and **DFS**.

Abstract model transformation:

**Person** <-> **Node**

P1 and P2 **dislike each other** <-> Node 1 and Node 2 **share one edge**, and they can be **drawed with different two colors** (i.e., for dislike relation)


If we can **draw each dislike pair** with **different two colors**, and **keep the dislike relationship always**, then there exists at least one possible **bipartition**.

---

**Illustration** and **Visualization**:

![image](https://assets.leetcode.com/users/brianchiang_tw/image_1590572143.png)

---

![image](https://assets.leetcode.com/users/brianchiang_tw/image_1590572156.png)


---

**Implementation** by DFS and coloring:

```
class Solution:
    def possibleBipartition(self, N: int, dislikes: List[List[int]]) -> bool:
        
        # Constant defined for color drawing to person
        NOT_COLORED, BLUE, GREEN = 0, 1, -1
        
        # -------------------------------------
        
        def helper( person_id, color ):
            
            # Draw person_id as color
            color_table[person_id] = color
            
            # Draw the_other, with opposite color, in dislike table of current person_id
            for the_other in dislike_table[ person_id ]:
   
                if color_table[the_other] == color:
                    # the_other has the same color of current person_id
                    # Reject due to breaking the relationship of dislike
                    return False

                if color_table[the_other] == NOT_COLORED and (not helper( the_other, -color)):
                    # Other people can not be colored with two different colors. 
					# Therefore, it is impossible to keep dis-like relationship with bipartition.
                    return False
                    
            return True
        
        
        # ------------------------------------------------
		
		
        if N == 1 or not dislikes:
            # Quick response for simple cases
            return True
        
        
        # each person maintain a list of dislike
        dislike_table = collections.defaultdict( list )
        
        # cell_#0 is dummy just for the convenience of indexing from 1 to N
        color_table = [ NOT_COLORED for _ in range(N+1) ]
        
        for p1, p2 in dislikes:
            
            # P1 and P2 dislike each other
            dislike_table[p1].append( p2 )
            dislike_table[p2].append( p1 )
            
        
        # Try to draw dislike pair with different colors in DFS
        for person_id in range(1, N+1):
            
            if color_table[person_id] == NOT_COLORED and (not helper( person_id, BLUE)):
                # Other people can not be colored with two different colors. 
				# Therefore, it is impossible to keep dis-like relationship with bipartition.
                return False 
        
        return True
```

---

Supplement material:

![image](https://assets.leetcode.com/users/brianchiang_tw/image_1590639902.png)

---

Reference:

[1] [Wiki: Bipartite graph](https://en.wikipedia.org/wiki/Bipartite_graph)
</p>


### C++ BFS with detailed explanation
- Author: aravindgk
- Creation Date: Wed May 27 2020 16:37:54 GMT+0800 (Singapore Standard Time)
- Update Date: Wed May 27 2020 19:43:14 GMT+0800 (Singapore Standard Time)

<p>
The question asks us to divide given people into two groups such that no two people in the same group dislike each other. 

For ease of understanding, we can represent this problem as a **graph**, with people being the **vertices** and dislike-pairs being the **edges** of this graph. 

We have to find out if the vertices can be divided into two sets such that there *aren\'t* any edges between vertices of the same set. A graph satisfying this condition is called a **bipartite graph**. For more on bipartite graphs, refer **[here](https://en.wikipedia.org/wiki/Bipartite_graph).**

We try to color the two sets of vertices, with RED and BLUE colors. In a bipartite graph, a RED vertex must be connected only with BLUE vertices and a BLUE vertex must be connected only with RED vertices. In other words, there must NOT be any edge connecting two vertices of the same color. Such an edge will be a **conflict edge**. 

***The presence of conflict edges indicate that bipartition is NOT possible.***

The graph may consist of many connected components. For each connected component, we run our **BFS** algorithm to find conflict edges, if any. For each component, we start by coloring one vertex RED, and all it\'s neighbours BLUE. Then we visit the BLUE vertices and color all their neighbours as RED, and so on. During this process, if we come across any **RED-RED** edge or **BLUE-BLUE** edge, we have found a **conflict edge** and we immediately return **false**, as bipartition will not be possible. 

If no conflict edges are found at the end of the algorithm, it means bipartition is possible, hence we return **true**.

Code for the above algorithm: 
```
#define WHITE 0
#define RED 1
#define BLUE 2

class Solution 
{
public:
    bool possibleBipartition(int N, vector<vector<int>> &edges) 
    {
        vector<vector<int>> adj(N + 1); // adjacency list for undirected graph
        vector<int> color(N + 1, WHITE); // color of each vertex in graph, initially WHITE
        vector<bool> explored(N + 1, false); // to check if each vertex has been explored exactly once
        
        // create adjacency list from given edges
        for (auto &edge: edges)
        {
            int u = edge[0];
            int v = edge[1];
            adj[u].push_back(v);
            adj[v].push_back(u);
        }
        
        // print adjacency list (comment out before submitting)
        for (int i = 0; i <= N; ++i)
        {
            cout << "adj[" << i << "]: ";
            for (int j = 0; j < adj[i].size(); ++j)
            {
                cout << adj[i][j] << " ";
            }
            cout << "\
";
        }
        
        // queue to perform BFS over each connected component in the graph
        // while performing BFS, we check if we encounter any conflicts while
        // coloring the vertices of the graph
        // conflicts indicate that bi-partition is not possible
        queue<int> q;
        
        for (int i = 1; i <= N; ++i)
        {
            if (!explored[i])
            {
                // this component has not been colored yet
				// we color the first vertex RED and push it into the queue
                color[i] = RED;
                q.push(i);
                
                // perform BFS from vertex i
                while (!q.empty())
                {
                    int u = q.front();
                    q.pop();
                    
                    // check if u is already explored 
                    if (explored[u])
                    {
                        continue;
                    }
                    
                    explored[u] = true;
                    
                    // for each neighbor of u, execute this loop
                    for (auto v: adj[u])
                    {
                        // v is u\'s neighboring vertex
                        
                        // checking if there\'s any conflict in coloring
                        if (color[v] == color[u])
                        {
							// conflict edge found, so we return false 
							// as bi-partition will not be possible
                            return false;
                        }
                        
                        // we color v with the opposite color of u
                        if (color[u] == RED)
                        {
                            color[v] = BLUE;
                        }
                        else 
                        {
                            color[v] = RED;
                        }
                        
                        q.push(v);
                    }
                }
            }
        }
        
        // if no conflicts encountered then graph must be bipartite
        // so we return true
        
        return true;
    }
};
```

Time Complexity: O(N + E)
Space Complexity: O(N + E)

where N is the number of people and E is the number of dislike pairs.


</p>


### Java DFS solution
- Author: wangzi6147
- Creation Date: Sun Aug 12 2018 11:02:39 GMT+0800 (Singapore Standard Time)
- Update Date: Wed Oct 24 2018 11:20:28 GMT+0800 (Singapore Standard Time)

<p>
Notes:
`group[i] = 0` means node `i` hasn\'t been visited.
`group[i] = 1` means node `i` has been grouped to `1`.
`group[i] = -1` means node `i` has been grouped to `-1`.
```
class Solution {
    public boolean possibleBipartition(int N, int[][] dislikes) {
        int[][] graph = new int[N][N];
        for (int[] d : dislikes) {
            graph[d[0] - 1][d[1] - 1] = 1;
            graph[d[1] - 1][d[0] - 1] = 1;
        }
        int[] group = new int[N];
        for (int i = 0; i < N; i++) {
            if (group[i] == 0 && !dfs(graph, group, i, 1)) {
                return false;
            }
        }
        return true;
    }
    private boolean dfs(int[][] graph, int[] group, int index, int g) {
        group[index] = g;
        for (int i = 0; i < graph.length; i++) {
            if (graph[index][i] == 1) {
                if (group[i] == g) {
                    return false;
                }
                if (group[i] == 0 && !dfs(graph, group, i, -g)) {
                    return false;
                }
            }
        }
        return true;
    }
}
```
</p>


