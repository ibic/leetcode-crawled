---
title: "Valid Triangle Number"
weight: 554
#id: "valid-triangle-number"
---
## Description
<div class="description">
Given an array consists of non-negative integers,  your task is to count the number of triplets chosen from the array that can make triangles if we take them as side lengths of a triangle.

<p><b>Example 1:</b><br />
<pre>
<b>Input:</b> [2,2,3,4]
<b>Output:</b> 3
<b>Explanation:</b>
Valid combinations are: 
2,3,4 (using the first 2)
2,3,4 (using the second 2)
2,2,3
</pre>
</p>

<p><b>Note:</b><br>
<ol>
<li>The length of the given array won't exceed 1000.</li>
<li>The integers in the given array are in the range of [0, 1000].</li>
</ol>
</p>

</div>

## Tags
- Array (array)

## Companies
- Bloomberg - 4 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Robinhood - 2 (taggedByAdmin: false)
- Amazon - 3 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- Expedia - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Solution

---
#### Approach 1: Brute Force

The condition for the triplets $$(a, b, c)$$ representing the lengths of the sides of a triangle, to form a valid triangle, is that the sum of any two sides should always be greater than the third side alone. i.e. $$a + b > c$$, $$b + c > a$$, $$a + c > b$$. 

The simplest method to check this is to consider every possible triplet in the given $$nums$$ array and checking if the triplet satisfies the three inequalities mentioned above. Thus, we can keep a track of the $$count$$ of the number of triplets satisfying these inequalities. When all the triplets have been considered, the $$count$$ gives the required result.

<iframe src="https://leetcode.com/playground/Lmwm78Nq/shared" frameBorder="0" width="100%" height="293" name="Lmwm78Nq"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^3)$$. Three nested loops are there to check every triplet.

* Space complexity : $$O(1)$$. Constant space is used.
<br>
<br>

---
#### Approach 2: Using Binary Search

**Algorithm**

If we sort the given $$nums$$ array once, we can solve the given problem in a better way. This is because, if we consider a triplet $$(a, b, c)$$ such that $$a &leq; b &leq; c$$, we need not check all the three inequalities for checking the validity of the triangle formed by them. But, only one condition $$a + b > c$$ would suffice. This happens because $$c &geq; b$$ and $$c &geq; a$$. Thus, adding any number to $$c$$ will always produce a sum which is greater than either $$a$$ or $$b$$ considered alone. Thus, the inequalities $$c + a > b$$ and $$c + b > a$$ are satisfied implicitly by virtue of the  property $$a < b < c$$.

From this, we get the idea that we can sort the given $$nums$$ array. Then, for every pair $$(nums[i], nums[j])$$ considered starting from the beginning of the array, such that $$j > i$$(leading to $$nums[j] &geq; nums[i]$$), we can find out the count of elements $$nums[k]$$($$k > j$$), which satisfy the inequality $$nums[k] > nums[i] + nums[j]$$. We can do so for every pair $$(i, j)$$ considered and get the required result.

We can also observe that, since we've sorted the $$nums$$ array, as we traverse towards the right for choosing the index $$k$$(for number $$nums[k]$$), the value of $$nums[k]$$ could increase or remain the same(doesn't decrease relative to the previous value). Thus, there will exist a right limit on the value of index $$k$$, such that the elements satisfy $$nums[k] > nums[i] + nums[j]$$. Any elements beyond this value of $$k$$ won't satisfy this inequality as well, which is obvious.

Thus, if we are able to find this right limit value of $$k$$(indicating the element just greater than $$nums[i] + nums[j]$$), we can conclude that all the elements in $$nums$$ array in the range $$(j+1, k-1)$$(both included) satisfy the required inequality. Thus, the $$count$$ of elements satisfying the inequality will be given by $$(k-1) - (j+1) + 1 = k - j - 1$$.

Since the $$nums$$ array has been sorted now, we can make use of Binary Search to find this right limit of $$k$$. The following animation shows how Binary Search can be used to find the right limit for a simple example.

!?!../Documents/Valid_Triangle_Binary.json:1000,563!?!

Another point to be observed is that once we find a right limit index $$k_{(i,j)}$$ for a particular pair $$(i, j)$$ chosen, when we choose a higher value of $$j$$ for the same value of $$i$$, we need not start searching for the right limit $$k_{(i,j+1)}$$ from the index $$j+2$$. Instead, we can start off from the index $$k_{(i,j)}$$ directly where we left off for the last $$j$$ chosen. 

This holds correct because when we choose a higher value of $$j$$(higher or equal $$nums[j]$$ than the previous one), all the $$nums[k]$$, such that $$k < k_{(i,j)}$$ will obviously satisfy $$nums[i] + nums[j] > nums[k]$$ for the new value of $$j$$ chosen.

By taking advantage of this observation, we can limit the range of Binary Search for $$k$$ to shorter values for increasing values of $$j$$ considered while choosing the pairs $$(i, j)$$.

<iframe src="https://leetcode.com/playground/7vw4QE2V/shared" frameBorder="0" width="100%" height="463" name="7vw4QE2V"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2 \log n)$$. In worst case inner loop will take $$n\log n$$ (binary search applied $$n$$ times).

* Space complexity : $$O(\log n)$$. Sorting takes $$O(\log n)$$ space.
<br>
<br>

---
#### Approach 3: Linear Scan

**Algorithm**

As discussed in the last approach, once we sort the given $$nums$$ array, we need to find the right limit of the index $$k$$ for a pair of indices $$(i, j)$$ chosen to find the $$count$$ of elements satisfying $$nums[i] + nums[j] > nums[k]$$ for the triplet $$(nums[i], nums[j], nums[k])$$ to form a valid triangle. 

We can find this right limit by simply traversing the index $$k$$'s values starting from the index $$k=j+1$$ for a pair $$(i, j)$$ chosen and stopping at the first value of $$k$$ not satisfying the above inequality. Again, the $$count$$ of elements $$nums[k]$$ satisfying $$nums[i] + nums[j] > nums[k]$$ for the pair of indices $$(i, j)$$ chosen is given by $$k - j - 1$$ as discussed in the last approach.

Further, as discussed in the last approach, when we choose a higher value of index $$j$$ for a particular $$i$$ chosen, we need not start from the index $$j + 1$$. Instead, we can start off directly from the value of $$k$$ where we left for the last index $$j$$. This helps to save redundant computations.

The following animation depicts the process:

!?!../Documents/Valid_Triangle_Linear.json:1000,563!?!

<iframe src="https://leetcode.com/playground/P8TpsEf7/shared" frameBorder="0" width="100%" height="310" name="P8TpsEf7"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n^2)$$. Loop of $$k$$ and $$j$$ will be executed $$O(n^2)$$ times in total, because, we do not reinitialize the value of $$k$$ for a new value of $$j$$ chosen(for the same $$i$$). Thus the complexity will be $$O(n^2+n^2)=O(n^2)$$.

* Space complexity : $$O(\log n)$$. Sorting takes $$O(\log n)$$ space.
<br>
<br>

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Java O(n^2) Time O(1) Space
- Author: compton_scatter
- Creation Date: Sun Jun 11 2017 11:15:33 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Nov 07 2019 15:55:30 GMT+0800 (Singapore Standard Time)

<p>
```
public static int triangleNumber(int[] A) {
    Arrays.sort(A);
    int count = 0, n = A.length;
    for (int i=n-1;i>=2;i--) {
        int l = 0, r = i-1;
        while (l < r) {
            if (A[l] + A[r] > A[i]) {
                count += r-l;
                r--;
            }
            else l++;
        }
    }
    return count;
}
```
</p>


### A similar O(n^2) solution to 3-Sum 
- Author: jeantimex
- Creation Date: Wed May 02 2018 15:02:05 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Oct 18 2018 09:40:18 GMT+0800 (Singapore Standard Time)

<p>
This problem is very similar to 3-Sum, in 3-Sum, we can use three pointers (i, j, k and i < j < k) to solve the problem in O(n^2) time for a sorted array, the way we do in 3-Sum is that we first lock pointer i and then scan j and k, if nums[j] + nums[k] is too large, k--, otherwise j++, once we complete the scan, increase pointer i and repeat.

For this problem, once we sort the input array nums, the key to solve the problem is that given nums[k], count the combination of i and j where nums[i] + nums[j] > nums[k] (so that they can form a triangle). If nums[i] + nums[j] is larger than nums[k], we know that there will be j - i combination.

Let\'s take the following array for example, let\'s mark the three pointers:
```
 i                  j   k
[3, 19, 22, 24, 35, 82, 84]
```
because 3 + 82 > 84 and the numbers between 3 and 82 are always larger than 3, so we can quickly tell that there will be j - i combination which can form the triangle, and they are:
```
3,  82, 84
19, 82, 84
22, 82, 84
24, 82, 84
35, 82, 84
```
Now let\'s lock k and point to 35:
```
 i          j   k
[3, 19, 22, 24, 35, 82, 84]
```
because 3 + 24 < 35, if we move j to the left, the sum will become even smaller, so we have to move pointer i to the next number 19, and now we found that 19 + 24 > 35, and we don\'t need to scan 22, we know that 22 must be ok!

Following is the JavaScript solution:
```
const triangleNumber = nums => {
  nums.sort((a, b) => a - b);

  let count = 0;

  for (let k = nums.length - 1; k > 1; k--) {
    for (let i = 0, j = k - 1; i < j;) {
      if (nums[i] + nums[j] > nums[k]) {
        count += j - i;
        j--;
      } else {
        i++;
      }
    }
  }

  return count;
};
```
</p>


### Java Solution, 3 pointers
- Author: shawngao
- Creation Date: Sun Jun 11 2017 11:18:58 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Sep 16 2018 16:07:09 GMT+0800 (Singapore Standard Time)

<p>
Same as https://leetcode.com/problems/3sum-closest

Assume ```a``` is the longest edge, ```b``` and ```c``` are shorter ones, to form a triangle, they need to satisfy ```len(b) + len(c) > len(a)```.

```
public class Solution {
    public int triangleNumber(int[] nums) {
        int result = 0;
        if (nums.length < 3) return result;
        
        Arrays.sort(nums);

        for (int i = 2; i < nums.length; i++) {
            int left = 0, right = i - 1;
            while (left < right) {
                if (nums[left] + nums[right] > nums[i]) {
                    result += (right - left);
                    right--;
                }
                else {
                    left++;
                }
            }
        }
        
        return result;
    }
}
```
</p>


