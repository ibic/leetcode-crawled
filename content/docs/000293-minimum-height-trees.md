---
title: "Minimum Height Trees"
weight: 293
#id: "minimum-height-trees"
---
## Description
<div class="description">
<p>A tree is an undirected graph in which any two vertices are connected by&nbsp;<i>exactly</i>&nbsp;one path. In other words, any connected graph without simple cycles is a tree.</p>

<p>Given a tree of <code>n</code> nodes&nbsp;labelled from <code>0</code> to <code>n - 1</code>, and an array of&nbsp;<code>n - 1</code>&nbsp;<code>edges</code> where <code>edges[i] = [a<sub>i</sub>, b<sub>i</sub>]</code> indicates that there is an undirected edge between the two nodes&nbsp;<code>a<sub>i</sub></code> and&nbsp;<code>b<sub>i</sub></code> in the tree,&nbsp;you can choose any node of the tree as the root. When you select a node <code>x</code> as the root, the result tree has height <code>h</code>. Among all possible rooted trees, those with minimum height (i.e. <code>min(h)</code>)&nbsp; are called <strong>minimum height trees</strong> (MHTs).</p>

<p>Return <em>a list of all <strong>MHTs&#39;</strong> root labels</em>.&nbsp;You can return the answer in <strong>any order</strong>.</p>

<p>The <strong>height</strong> of a rooted tree is the number of edges on the longest downward path between the root and a leaf.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/01/e1.jpg" style="width: 800px; height: 213px;" />
<pre>
<strong>Input:</strong> n = 4, edges = [[1,0],[1,2],[1,3]]
<strong>Output:</strong> [1]
<strong>Explanation:</strong> As shown, the height of the tree is 1 when the root is the node with label 1 which is the only MHT.
</pre>

<p><strong>Example 2:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/09/01/e2.jpg" style="width: 800px; height: 321px;" />
<pre>
<strong>Input:</strong> n = 6, edges = [[3,0],[3,1],[3,2],[3,4],[5,4]]
<strong>Output:</strong> [3,4]
</pre>

<p><strong>Example 3:</strong></p>

<pre>
<strong>Input:</strong> n = 1, edges = []
<strong>Output:</strong> [0]
</pre>

<p><strong>Example 4:</strong></p>

<pre>
<strong>Input:</strong> n = 2, edges = [[0,1]]
<strong>Output:</strong> [0,1]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>1 &lt;= n &lt;= 2 * 10<sup>4</sup></code></li>
	<li><code>edges.length == n - 1</code></li>
	<li><code>0 &lt;= a<sub>i</sub>, b<sub>i</sub> &lt; n</code></li>
	<li><code>a<sub>i</sub> != b<sub>i</sub></code></li>
	<li>All the pairs <code>(a<sub>i</sub>, b<sub>i</sub>)</code> are distinct.</li>
	<li>The given input is <strong>guaranteed</strong> to be a tree and there will be <strong>no repeated</strong> edges.</li>
</ul>

</div>

## Tags
- Breadth-first Search (breadth-first-search)
- Graph (graph)

## Companies
- Amazon - 3 (taggedByAdmin: false)
- Google - 4 (taggedByAdmin: true)
- Snapchat - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Facebook - 2 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Share some thoughts
- Author: dietpepsi
- Creation Date: Fri Nov 27 2015 05:47:08 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 19:44:46 GMT+0800 (Singapore Standard Time)

<p>
**See [here for a better view](http://algobox.org/minimum-height-trees/)**

First let's review some statement for tree in graph theory:

> (1) A tree is an undirected graph in which any two vertices are
> connected by exactly one path.
> 
> (2) Any connected graph who has `n` nodes with `n-1` edges is a tree.
> 
> (3) The degree of a vertex of a graph is the number of
> edges incident to the vertex.
> 
> (4) A leaf is a vertex of degree 1. An internal vertex is a vertex of
> degree at least 2.
> 
> (5) A path graph is a tree with two or more vertices that is not
> branched at all.
> 
> (6) A tree is called a rooted tree if one vertex has been designated
> the root.
> 
> (7) The height of a rooted tree is the number of edges on the longest
> downward path between root and a leaf.

OK. Let's stop here and look at our problem.

Our problem want us to find the minimum height trees and return their root labels. First we can think about a simple case -- a path graph.

For a path graph of `n` nodes, find the minimum height trees is trivial. Just designate the middle point(s) as roots.

Despite its triviality, let design a algorithm to find them.

Suppose we don't know `n`, nor do we have random access of the nodes. We have to traversal. It is very easy to get the idea of two pointers. One from each end and move at the same speed. When they meet or they are one step away, (depends on the parity of `n`), we have the roots we want.

This gives us a lot of useful ideas to crack our real problem.

For a tree we can do some thing similar. We start from every end, by end we mean vertex of degree 1 (aka leaves). We let the pointers move the same speed. When two pointers meet, we keep only one of them, until the last two pointers meet or one step away we then find the roots.

It is easy to see that the last two pointers are from the two ends of the longest path in the graph.

The actual implementation is similar to the BFS topological sort. Remove the leaves, update the degrees of inner vertexes. Then remove the new leaves. Doing so level by level until there are 2 or 1 nodes left. What's left is our answer!

The time complexity and space complexity are both O(n). 

Note that for a tree we always have `V = n`, `E = n-1`.


**Java**

    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        if (n == 1) return Collections.singletonList(0);

        List<Set<Integer>> adj = new ArrayList<>(n);
        for (int i = 0; i < n; ++i) adj.add(new HashSet<>());
        for (int[] edge : edges) {
            adj.get(edge[0]).add(edge[1]);
            adj.get(edge[1]).add(edge[0]);
        }

        List<Integer> leaves = new ArrayList<>();
        for (int i = 0; i < n; ++i)
            if (adj.get(i).size() == 1) leaves.add(i);

        while (n > 2) {
            n -= leaves.size();
            List<Integer> newLeaves = new ArrayList<>();
            for (int i : leaves) {
                int j = adj.get(i).iterator().next();
                adj.get(j).remove(i);
                if (adj.get(j).size() == 1) newLeaves.add(j);
            }
            leaves = newLeaves;
        }
        return leaves;
    }
	
    // Runtime: 53 ms

**Python**

    def findMinHeightTrees(self, n, edges):
        if n == 1: return [0] 
        adj = [set() for _ in xrange(n)]
        for i, j in edges:
            adj[i].add(j)
            adj[j].add(i)

        leaves = [i for i in xrange(n) if len(adj[i]) == 1]

        while n > 2:
            n -= len(leaves)
            newLeaves = []
            for i in leaves:
                j = adj[i].pop()
                adj[j].remove(i)
                if len(adj[j]) == 1: newLeaves.append(j)
            leaves = newLeaves
        return leaves
		
    # Runtime : 104ms
</p>


### Two O(n) solutions
- Author: lixx2100
- Creation Date: Thu Dec 03 2015 09:53:40 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 19:53:23 GMT+0800 (Singapore Standard Time)

<p>
I am sharing two of my solutions, one is based on the longest path, and the other is related to Tree DP.

**Longest Path**

It is easy to see that the root of an MHT has to be the middle point (or two middle points) of the longest path of the tree.
Though multiple longest paths can appear in an unrooted tree, they must share the same middle point(s).

Computing the longest path of a unrooted tree can be done, in O(n) time, by tree dp, or simply 2 tree traversals (dfs or bfs).
The following is some thought of the latter.

Randomly select a node x as the root, do a dfs/bfs to find the node y that has the longest distance from x.
Then y must be one of the endpoints on some longest path.
Let y the new root, and do another dfs/bfs. Find the node z that has the longest distance from y.

Now, the path from y to z is the longest one, and thus its middle point(s) is the answer. [Java Solution][1]


**Tree DP**

Alternatively, one can solve this problem directly by tree dp.
Let dp[i] be the height of the tree when the tree root is i.
We compute dp[0] ... dp[n - 1] by tree dp in a dfs manner.

Arbitrarily pick a node, say node 0, as the root, and do a dfs.
When we reach a node u, and let T be the subtree by removing all u's descendant (see the right figure below).
We maintain a variable acc that keeps track of the length of the longest path in T with one endpoint being u.
Then dp[u] = max(height[u], acc)
Note, acc is 0 for the root of the tree.

                 |                 |
                 .                 .
                /|\               /|\
               * u *             * u *
                /|\
               / | \
              *  v  *

. denotes a single node, and * denotes a subtree (possibly empty).

Now it remains to calculate the new acc for any of u's child, v.
It is easy to see that the new acc is the max of the following
 
 1. acc + 1 --- extend the previous path by edge uv;
 2. max(height[v'] + 2), where v != v' --- see below for an example.
 

                 u
                /|
               / |
              v' v
              |
              .
              .
              .
              |
              .

In fact, the second case can be computed in O(1) time instead of spending a time proportional to the degree of u.
Otherwise, the runtime can be quadratic when the degree of some node is Omega(n).
The trick here is to maintain two heights of each node, the largest height (the conventional height), and the second largest height
(the height of the node after removing the branch w.r.t. the largest height).

Therefore, after the dfs, all dp[i]'s are computed, and the problem can be answered trivially.
The total runtime is still O(n). [Java Solution][2]


  [1]: https://github.com/lydxlx1/LeetCode/blob/master/src/_310.java
  [2]: https://github.com/lydxlx1/LeetCode/blob/master/src/_310_1.java
</p>


### C++ Solution. O(n)-Time, O(n)-Space
- Author: TTester
- Creation Date: Thu Nov 26 2015 13:45:35 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Aug 19 2018 01:57:05 GMT+0800 (Singapore Standard Time)

<p>
The basic idea is **"keep deleting leaves layer-by-layer, until reach the root."**

Specifically, first find all the leaves, then remove them. After removing, some nodes will become new leaves. So we can continue remove them. Eventually, there is only 1 or 2 nodes left.  If there is only one node left, it is the root. If there are 2 nodes, either of them could be a possible root.

**Time Complexity**: Since each node will be removed at most once, the complexity is **O(n)**.

Thanks for pointing out any mistakes.

----
*Updates:
More precisely, if the number of nodes is V, and the number of edges is E. The space complexity is O(V+2E), for storing the whole tree. The time complexity is O(E), because we gradually remove all the neighboring information. As some friends pointing out,  for a tree, if V=n, then E=n-1. Thus both time complexity and space complexity become O(n).*

        class Solution {
        public:
            
            struct Node
            {
                unordered_set<int> neighbor;
                bool isLeaf()const{return neighbor.size()==1;}
            };
            
            vector<int> findMinHeightTrees(int n, vector<pair<int, int>>& edges) {
                
                vector<int> buffer1;
                vector<int> buffer2;
                vector<int>* pB1 = &buffer1;
                vector<int>* pB2 = &buffer2;
                if(n==1)
                {
                    buffer1.push_back(0);
                    return buffer1;
                }
                if(n==2)
                {
                    buffer1.push_back(0);
                    buffer1.push_back(1);
                    return buffer1;
                }
                
                // build the graph
                vector<Node> nodes(n);
                for(auto p:edges)
                {
                    nodes[p.first].neighbor.insert(p.second);
                    nodes[p.second].neighbor.insert(p.first);
                }
                
                // find all leaves
                for(int i=0; i<n; ++i)
                {
                    if(nodes[i].isLeaf()) pB1->push_back(i);
                }
    
                // remove leaves layer-by-layer            
                while(1)
                {
                    for(int i : *pB1)
                    {
                        for(auto n: nodes[i].neighbor)
                        {
                            nodes[n].neighbor.erase(i);
                            if(nodes[n].isLeaf()) pB2->push_back(n);
                        }
                    }
                    if(pB2->empty())
                    {
                        return *pB1;
                    }
                    pB1->clear();
                    swap(pB1, pB2);
                }
                
            }
        };
</p>


