---
title: "Edit Distance"
weight: 72
#id: "edit-distance"
---
## Description
<div class="description">
<p>Given two words <em>word1</em> and <em>word2</em>, find the minimum number of operations required to convert <em>word1</em> to <em>word2</em>.</p>

<p>You have the following 3 operations permitted on a word:</p>

<ol>
	<li>Insert a character</li>
	<li>Delete a character</li>
	<li>Replace a character</li>
</ol>

<p><strong>Example 1:</strong></p>

<pre>
<strong>Input:</strong> word1 = &quot;horse&quot;, word2 = &quot;ros&quot;
<strong>Output:</strong> 3
<strong>Explanation:</strong> 
horse -&gt; rorse (replace &#39;h&#39; with &#39;r&#39;)
rorse -&gt; rose (remove &#39;r&#39;)
rose -&gt; ros (remove &#39;e&#39;)
</pre>

<p><strong>Example 2:</strong></p>

<pre>
<strong>Input:</strong> word1 = &quot;intention&quot;, word2 = &quot;execution&quot;
<strong>Output:</strong> 5
<strong>Explanation:</strong> 
intention -&gt; inention (remove &#39;t&#39;)
inention -&gt; enention (replace &#39;i&#39; with &#39;e&#39;)
enention -&gt; exention (replace &#39;n&#39; with &#39;x&#39;)
exention -&gt; exection (replace &#39;n&#39; with &#39;c&#39;)
exection -&gt; execution (insert &#39;u&#39;)
</pre>

</div>

## Tags
- String (string)
- Dynamic Programming (dynamic-programming)

## Companies
- Google - 7 (taggedByAdmin: false)
- Amazon - 6 (taggedByAdmin: false)
- Facebook - 3 (taggedByAdmin: false)
- Square - 3 (taggedByAdmin: false)
- Uber - 2 (taggedByAdmin: false)
- ByteDance - 2 (taggedByAdmin: false)
- Bloomberg - 4 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- LinkedIn - 2 (taggedByAdmin: false)
- DiDi - 2 (taggedByAdmin: false)
- Yahoo - 4 (taggedByAdmin: false)
- Adobe - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- VMware - 2 (taggedByAdmin: false)
- Paypal - 2 (taggedByAdmin: false)
- Goldman Sachs - 2 (taggedByAdmin: false)
- Qualcomm - 2 (taggedByAdmin: false)
- Atlassian - 2 (taggedByAdmin: false)
- Intuit - 2 (taggedByAdmin: false)

## Official Solution
[TOC]

## Solution

---

#### Intuition

The edit distance algorithm is very popular among 
the data scientists. It's one of the basic algorithms
used for evaluation of machine translation and speech 
recognition. 

The naive approach would be to check for all possible edit 
sequences and choose the shortest one in-between.
That would result in an exponential complexity and it's an overkill
since we actually don't need to have all possible edit sequences 
but just the shortest one. 
<br />
<br />


---
#### Approach 1: Dynamic Programming

The idea would be to reduce the problem to simple ones.
For example, there are two words, `horse` and `ros` and we want to compute
an edit distance `D` for them. One could notice that it seems to be
more simple for short words and so it would be logical to relate
an edit distance `D[n][m]` with the lengths `n` and `m` of input words.

Let's go further and introduce an edit distance `D[i][j]` which is
an edit distance between the first `i` characters of `word1` and 
the first `j` characters of `word2`.

![edit_distance](../Figures/72/72_edit.png)

It turns out that one could compute `D[i][j]`, knowing 
`D[i - 1][j]`, `D[i][j - 1]` and `D[i - 1][j - 1]`.

> There is just one more character to add into one or both strings 
and the formula is quite obvious.

If the last character is the same, *i.e.* `word1[i] = word2[j]` then

$$
D[i][j] = 1 + \min(D[i - 1][j], D[i][j - 1], D[i - 1][j - 1] - 1)
$$

and if not, *i.e.* `word1[i] != word2[j]` we have to
take into account the replacement of the last character 
during the conversion.

$$
D[i][j] = 1 + \min(D[i - 1][j], D[i][j - 1], D[i - 1][j - 1])
$$

So each step of the computation would be done based on the previous computation,
as follows: 

![compute](../Figures/72/72_compute.png)

The obvious base case is an edit distance between the empty string and 
non-empty string that means `D[i][0] = i` and `D[0][j] = j`.

Now we have everything to actually proceed to the computations 

<!--![LIS](../Figures/72/72_tr.gif)-->
!?!../Documents/72_LIS.json:1000,513!?!

<iframe src="https://leetcode.com/playground/YihzPbdt/shared" frameBorder="0" width="100%" height="500" name="YihzPbdt"></iframe>

**Complexity Analysis**

* Time complexity : $$\mathcal{O}(m n)$$ as 
it follows quite straightforward for the inserted loops. 
* Space complexity : $$\mathcal{O}(m n)$$ since at each step we
keep the results of all previous computations.

## Accepted Submission (python3)
```python3
class Solution:
    def minDistance(self, word1, word2):
        """
        :type word1: str
        :type word2: str
        :rtype: int
        """
        m = len(word1)
        n = len(word2)
        arr = [[0 for i in range(n + 1)] for j in range(m + 1)]
        for i in range(1, m + 1):
            arr[i][0] = i
        for i in range(1, n + 1):
            arr[0][i] = i
        for i in range(1, m + 1):
            for j in range(1, n + 1):
                if word1[i - 1] == word2[j - 1]:
                    arr[i][j] = arr[i - 1][j - 1]
                else:
                    li = [
                        arr[i - 1][j - 1],
                        arr[i][j - 1],
                        arr[i - 1][j]
                    ]
                    arr[i][j] = min(li) + 1
        return arr[m][n]
```

## Top Discussions
### C++ O(n)-space DP
- Author: jianchao-li
- Creation Date: Thu Jul 02 2015 21:35:00 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 00:32:52 GMT+0800 (Singapore Standard Time)

<p>
To apply DP, we define the state `dp[i][j]` to be the minimum number of operations to convert `word1[0..i)` to `word2[0..j)`.

For the base case, that is, to convert a string to an empty string, the mininum number of operations (deletions) is just the length of the string. So we have `dp[i][0] = i` and `dp[0][j] = j`.

For the general case to convert `word1[0..i)` to `word2[0..j)`, we break this problem down into sub-problems. Suppose we have already known how to convert `word1[0..i - 1)` to `word2[0..j - 1)` (`dp[i - 1][j - 1]`), if  `word1[i - 1] == word2[j - 1]`, then no more operation is needed and `dp[i][j] = dp[i - 1][j - 1]`.

If `word1[i - 1] != word2[j - 1]`, we need to consider three cases.

 1. **Replace** `word1[i - 1]` by `word2[j - 1]` (`dp[i][j] = dp[i - 1][j - 1] + 1`);
 2. If `word1[0..i - 1) = word2[0..j)` then **delete** `word1[i - 1]` (`dp[i][j] = dp[i - 1][j] + 1`);
 3. If `word1[0..i) + word2[j - 1] = word2[0..j)` then **insert** `word2[j - 1]` to `word1[0..i)` (`dp[i][j] = dp[i][j - 1] + 1`).

So when `word1[i - 1] != word2[j - 1]`, `dp[i][j]` will just be the minimum of the above three cases.

```cpp
class Solution {
public:
    int minDistance(string word1, string word2) {
        int m = word1.size(), n = word2.size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1, 0));
        for (int i = 1; i <= m; i++) {
            dp[i][0] = i;
        }
        for (int j = 1; j <= n; j++) {
            dp[0][j] = j;
        }
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (word1[i - 1] == word2[j - 1]) {
                    dp[i][j] = dp[i - 1][j - 1];
                } else {
                    dp[i][j] = min(dp[i - 1][j - 1], min(dp[i][j - 1], dp[i - 1][j])) + 1;
                }
            }
        }
        return dp[m][n];
    }
};
```

Note that each time when we update `dp[i][j]`, we only need `dp[i - 1][j - 1]`, `dp[i][j - 1]` and `dp[i - 1][j]`. We may optimize the space of the code to use only two vectors.

```cpp
class Solution {
public:
    int minDistance(string word1, string word2) {
        int m = word1.size(), n = word2.size();
        vector<int> pre(n + 1, 0), cur(n + 1, 0);
        for (int j = 1; j <= n; j++) {
            pre[j] = j;
        }
        for (int i = 1; i <= m; i++) {
            cur[0] = i;
            for (int j = 1; j <= n; j++) {
                if (word1[i - 1] == word2[j - 1]) {
                    cur[j] = pre[j - 1];
                } else {
                    cur[j] = min(pre[j - 1], min(cur[j - 1], pre[j])) + 1;
                }
            }
            fill(pre.begin(), pre.end(), 0);
            swap(pre, cur);
        }
        return pre[n];
    }
};
```

Or even just one vector.

```cpp
class Solution {
public:
    int minDistance(string word1, string word2) {
        int m = word1.size(), n = word2.size(), pre;
        vector<int> cur(n + 1, 0);
        for (int j = 1; j <= n; j++) {
            cur[j] = j;
        }
        for (int i = 1; i <= m; i++) {
            pre = cur[0];
            cur[0] = i;
            for (int j = 1; j <= n; j++) {
                int temp = cur[j];
                if (word1[i - 1] == word2[j - 1]) {
                    cur[j] = pre;
                } else {
                    cur[j] = min(pre, min(cur[j - 1], cur[j])) + 1;
                }
                pre = temp;
            }
        }
        return cur[n];
    }
};
```
</p>


### Python solutions and intuition
- Author: anderson5
- Creation Date: Mon Aug 13 2018 07:42:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 00:44:46 GMT+0800 (Singapore Standard Time)

<p>
For those having difficulty cracking dynamic programming solutions, I find it easiest to solve by first starting with a naive, but working recursive implementation. It\'s essential to do so, because dynamic programming is basically recursion with caching. With this workflow, deciphering dynamic programming problems becomes just a little more manageable for us normal people. :)

**Thought process:**
Given two strings, we\'re tasked with finding the minimum number of transformations we need to make to arrive with equivalent strings. From the get-go, there doesn\'t seem to be any way around trying all possibilities, and in this, possibilities refers to inserting, deleting, or replacing a character. Recursion is usually a good choice for trying all possilbilities. 

Whenever we write recursive functions, we\'ll need some way to terminate, or else we\'ll end up overflowing the stack via infinite recursion. With strings, the natural state to keep track of is the index. We\'ll need two indexes, one for word1 and one for word2. Now we just need to handle our base cases, and recursive cases. 
What happens when we\'re done with either word? Some thought will tell you that the minimum number of transformations is simply to insert the rest of the other word. This is our base case. What about when we\'re not done with either string? We\'ll either match the currently indexed characters in both strings, or mismatch. In the first case, we don\'t incur any penalty, and we can continue to compare the rest of the strings by recursing on the rest of both strings. In the case of a mismatch, we either insert, delete, or replace. To recap:
1. base case: word1 = "" or word2 = "" => return length of other string
2. recursive case: word1[0] == word2[0] => recurse on word1[1:] and word2[1:]
3. recursive case: word1[0] != word2[0] => recurse by inserting, deleting, or replacing 

And in Python:
```
class Solution:
    def minDistance(self, word1, word2):
        """Naive recursive solution"""
        if not word1 and not word2:
            return 0
        if not word1:
            return len(word2)
        if not word2:
            return len(word1)
        if word1[0] == word2[0]:
            return self.minDistance(word1[1:], word2[1:])
        insert = 1 + self.minDistance(word1, word2[1:])
        delete = 1 + self.minDistance(word1[1:], word2)
        replace = 1 + self.minDistance(word1[1:], word2[1:])
        return min(insert, replace, delete)
```

With a solution in hand, we\'re ecstatic and we go to submit our code. All is well until we see the dreaded red text... **TIME LIMIT EXCEEDED**. What did we do wrong? Let\'s look at a simple example, and for sake of brevity I\'ll annotate the minDistance function as ```md```. 

word1 = "horse"
word2 = "hello"

The tree of recursive calls, 3 levels deep, looks like the following. I\'ve highlighted recursive calls with multiple invocations. So now we see that we\'re repeating work. I\'m not going to try and analyze the runtime of this solution, but it\'s exponential. 

```
md("horse", "hello")
	md("orse", "ello")
		md("orse", "llo")
			md("orse", "lo")
			md("rse", "llo") <- 
			md("rse", "lo")
		md("rse", "ello")
			md("rse", "llo") <-
			md("se", "ello")
			md("se", "llo") <<-
		md("rse", "llo")
			md("rse", "llo") <-
			md("se", "llo") <<-
			md("se", "lo")
```

The way we fix this is by **caching**. We save intermediate computations in a dictionary and if we recur on the same subproblem, instead of doing the same work again, we return the saved value. Here is the memoized solution, where we build from bigger subproblems to smaller subproblems (top-down).
```
class Solution:
    def minDistance(self, word1, word2, i, j, memo):
        """Memoized solution"""
        if i == len(word1) and j == len(word2):
            return 0
        if i == len(word1):
            return len(word2) - j
        if j == len(word2):
            return len(word1) - i

        if (i, j) not in memo:
            if word1[i] == word2[j]:
                ans = self.minDistance2(word1, word2, i + 1, j + 1, memo)
            else: 
                insert = 1 + self.minDistance2(word1, word2, i, j + 1, memo)
                delete = 1 + self.minDistance2(word1, word2, i + 1, j, memo)
                replace = 1 + self.minDistance2(word1, word2, i + 1, j + 1, memo)
                ans = min(insert, delete, replace)
            memo[(i, j)] = ans
        return memo[(i, j)]
```

Of course, an interative implementation is usually better than its recursive counterpart because we don\'t risk blowing up our stack in case the number of recursive calls is very deep. We can also use a 2D array to do essentially the same thing as the dictionary of cached values. When we do this, we build up solutions from smaller subproblems to bigger subproblems (bottom-up). In this case, since we are no longer "recurring" in the traditional sense, we initialize our 2D table with base constraints. The first row and column of the table has known values since if one string is empty, we simply add the length of the non-empty string since that is the minimum number of edits necessary to arrive at equivalent strings. For both the memoized and dynamic programming solutions, the runtime is ```O(mn)``` and the space complexity is ```O(mn)``` where m and n are the lengths of word1 and word2, respectively.
```
class Solution:
    def minDistance(self, word1, word2):
        """Dynamic programming solution"""
        m = len(word1)
        n = len(word2)
        table = [[0] * (n + 1) for _ in range(m + 1)]

        for i in range(m + 1):
            table[i][0] = i
        for j in range(n + 1):
            table[0][j] = j

        for i in range(1, m + 1):
            for j in range(1, n + 1):
                if word1[i - 1] == word2[j - 1]:
                    table[i][j] = table[i - 1][j - 1]
                else:
                    table[i][j] = 1 + min(table[i - 1][j], table[i][j - 1], table[i - 1][j - 1])
        return table[-1][-1]
```


</p>


### Java DP solution - O(nm)
- Author: whitehat
- Creation Date: Sat Aug 08 2015 09:40:45 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Oct 23 2018 15:36:07 GMT+0800 (Singapore Standard Time)

<p>
Let following be the function definition :-

f(i, j) := minimum cost (or steps) required to convert first i characters of word1 to first j characters of word2

Case 1: word1[i] == word2[j], i.e. the ith the jth character matches.

> f(i, j) = f(i - 1, j - 1)

Case 2: word1[i] != word2[j], then we must either insert, delete or replace, whichever is cheaper

> f(i, j) = 1 + min { f(i, j - 1), f(i - 1, j), f(i - 1, j - 1) }

1. f(i, j - 1) represents insert operation
2. f(i - 1, j) represents delete operation
3. f(i - 1, j - 1) represents replace operation

Here, we consider any operation from word1 to word2. It means, when we say insert operation, we insert a new character after word1 that matches the jth character of word2. So, now have to match i characters of word1 to j - 1 characters of word2. Same goes for other 2 operations as well.

Note that the problem is symmetric. The insert operation in one direction (i.e. from word1 to word2) is same as delete operation in other. So, we could choose any direction.

Above equations become the recursive definitions for DP.

Base Case: 

> f(0, k) = f(k, 0) = k

Below is the direct bottom-up translation of this recurrent relation. It is only important to take care of 0-based index with actual code :-

    public class Solution {
        public int minDistance(String word1, String word2) {
            int m = word1.length();
            int n = word2.length();
            
            int[][] cost = new int[m + 1][n + 1];
            for(int i = 0; i <= m; i++)
                cost[i][0] = i;
            for(int i = 1; i <= n; i++)
                cost[0][i] = i;
            
            for(int i = 0; i < m; i++) {
                for(int j = 0; j < n; j++) {
                    if(word1.charAt(i) == word2.charAt(j))
                        cost[i + 1][j + 1] = cost[i][j];
                    else {
                        int a = cost[i][j];
                        int b = cost[i][j + 1];
                        int c = cost[i + 1][j];
                        cost[i + 1][j + 1] = a < b ? (a < c ? a : c) : (b < c ? b : c);
                        cost[i + 1][j + 1]++;
                    }
                }
            }
            return cost[m][n];
        }
    }

Time complexity : If n is the length of word1, m of word2, because of the two indented loops, it is O(nm)
</p>


