---
title: "Contains Duplicate III"
weight: 204
#id: "contains-duplicate-iii"
---
## Description
<div class="description">
<p>Given an array of integers, find out whether there are two distinct indices <i>i</i> and <i>j</i> in the array such that the <b>absolute</b> difference between <b>nums[i]</b> and <b>nums[j]</b> is at most <i>t</i> and the <b>absolute</b> difference between <i>i</i> and <i>j</i> is at most <i>k</i>.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<pre><strong>Input:</strong> nums = [1,2,3,1], k = 3, t = 0
<strong>Output:</strong> true
</pre><p><strong>Example 2:</strong></p>
<pre><strong>Input:</strong> nums = [1,0,1,1], k = 1, t = 2
<strong>Output:</strong> true
</pre><p><strong>Example 3:</strong></p>
<pre><strong>Input:</strong> nums = [1,5,9,1,5,9], k = 2, t = 3
<strong>Output:</strong> false
</pre>
<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>0 &lt;= nums.length &lt;= 2 * 10<sup>4</sup></code></li>
	<li><code>-2<sup>31</sup> &lt;= nums[i]&nbsp;&lt;= 2<sup>31</sup> - 1</code></li>
	<li><code>0 &lt;= k &lt;= 10<sup>4</sup></code></li>
	<li><code>0 &lt;= t &lt;= 2<sup>31</sup> - 1</code></li>
</ul>

</div>

## Tags
- Sort (sort)
- Ordered Map (ordered-map)

## Companies
- Adobe - 3 (taggedByAdmin: false)
- Google - 2 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)
- Microsoft - 2 (taggedByAdmin: false)
- Airbnb - 0 (taggedByAdmin: true)
- Palantir Technologies - 0 (taggedByAdmin: true)

## Official Solution
[TOC]

## Summary
This article is for intermediate readers. It introduces the following ideas:
Binary Search Tree, HashMap, and Buckets.

## Solutions
---
#### Approach #1 (Naive Linear Search) [Time Limit Exceeded]

**Intuition**

Compare each element with the previous $$k$$ elements and see if their difference is at most $$t$$.

**Algorithm**

This problem requires us to find $$i$$ and $$j$$ such that the following conditions are satisfied:

1. <a name="condition-1"></a>$$\bigl| i-j \bigr| \le k$$
2. <a name="condition-2"></a>$$\bigl| \mathrm{nums}[i] - \mathrm{nums}[j] \bigr| \le t$$

The naive approach is the same as [Approach #1 in Contains Duplicate II solution](https://leetcode.com/articles/contains-duplicate-ii/#approach-1-naive-linear-search-time-limit-exceeded), which keeps a virtual sliding window that holds the newest $$k$$ elements. In this way, [Condition 1](#condition-1) above is always satisfied. We then check if [Condition 2](#condition-2) is also satisfied by applying linear search.



<iframe src="https://leetcode.com/playground/n3Cew2am/shared" frameBorder="0" name="n3Cew2am" width="100%" height="207"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n \min(k,n))$$.
It costs $$O(\min(k, n))$$ time for each linear search. Note that we do at most $$n$$ comparisons in one search even if $$k$$ can be larger than $$n$$.

* Space complexity : $$O(1)$$.
We only used constant auxiliary space.
---
#### Approach #2 (Binary Search Tree) [Accepted]

**Intuition**

* If elements in the window are maintained in sorted order, we can apply binary search twice to check if [Condition 2](#condition-2) is satisfied.

* By utilizing self-balancing Binary Search Tree, one can keep the window ordered at all times with logarithmic time `insert` and `delete`.

**Algorithm**

The real bottleneck of [Approach #1](#approach-1-naive-linear-search-time-limit-exceeded) is due to all elements in the sliding window are being scanned to check if [Condition 2](#condition-2) is satisfied. Could we do better?

If elements in the window are in sorted order, we can apply Binary Search twice to search for the two boundaries $$x+t$$ and $$x-t$$ for each element $$x$$.

Unfortunately, the window is unsorted. A common mistake here is attempting to maintain a sorted array. Although searching in a sorted array costs only logarithmic time, keeping the order of the elements after `insert` and `delete` operation is not as efficient. Imagine you have a sorted array with $$k$$ elements and you are adding a new item $$x$$. Even if you can find the correct position in $$O(\log k)$$ time, you still need $$O(k)$$ time to insert $$x$$ into the sorted array. The reason is that you need to shift all elements after the insert position one step backward. The same reasoning applies to removal as well. After removing an item from position $$i$$, you need to shift all elements after $$i$$ one step forward. Thus, we gain nothing in speed compared to the [naive linear search approach](#approach-1-naive-linear-search-time-limit-exceeded) above.

To gain an actual speedup, we need a *dynamic* data structure that supports faster `insert`, `search` and `delete`. Self-balancing Binary Search Tree (BST) is the right data structure. The term *Self-balancing* means the tree automatically keeps its height small after arbitrary `insert` and `delete` operations. Why does self-balancing matter? That is because most operations on a BST take time directly proportional to the height of the tree. Take a look at the following non-balanced BST which is skewed to the left:

```
            6
           /
          5
         /
        4
       /
      3
     /
    2
   /
  1
```
*Figure 1. A non-balanced BST that is skewed to the left.*

Searching in the above BST degrades to *linear* time, which is like searching in a linked list. Now compare to the BST below which is balanced:

```
          4
        /   \
       2     6
      / \   /
     1   3  5
```

*Figure 2. A balanced BST.*

Assume that $$n$$ is the total number of nodes in the tree, a balanced binary tree maintains its height in the order of $$h = \log n$$. Thus it supports $$O(h) = O(\log n)$$ time for each of `insert`, `search` and `delete` operations.

Here is the entire algorithm in pseudocode:

* Initialize an empty BST `set`
* Loop through the array, for each element $$x$$
    * Find the *smallest* element $$s$$ in `set` that is *greater* than or equal to $$x$$, return true if $$s - x \leq t$$
    * Find the *greatest* element $$g$$ in `set` that is *smaller* than or equal to $$x$$, return true if $$x - g \leq t$$
    * Put $$x$$ in `set`
    * If the size of the set is larger than $$k$$, remove the oldest item.
* Return false

One may consider the smallest element $$s$$ that is greater or equal to $$x$$ as the *successor* of $$x$$ in the BST, as in: "What is the next greater value of $$x$$?". Similarly, we consider the greatest element $$g$$ that is smaller or equal to $$x$$ as the *predecessor* of $$x$$ in the BST, as in: "What is the previous smaller value of $$x$$?". These two values $$s$$ and $$g$$ are the two closest neighbors from $$x$$. Thus by checking the distance from them to $$x$$, we can conclude if [Condition 2](#condition-2) is satisfied.


<iframe src="https://leetcode.com/playground/AJAFvbhM/shared" frameBorder="0" name="AJAFvbhM" width="100%" height="360"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n \log (\min(n,k)))$$.
We iterate through the array of size $$n$$. For each iteration, it costs $$O(\log \min(k, n))$$ time (`search`, `insert` or `delete`) in the BST, since the size of the BST is upper bounded by both $$k$$ and $$n$$.

* Space complexity : $$O(\min(n,k))$$.
Space is dominated by the size of the BST, which is upper bounded by both $$k$$ and $$n$$.

**Note**

* When the array's elements and $$t$$'s value are large, they can cause overflow in arithmetic operation. Consider using a larger size data type instead, such as *long*.

* C++'s `std::set`, `std::set::upper_bound` and `std::set::lower_bound` are equivalent to Java's `TreeSet`, `TreeSet::ceiling` and `TreeSet::floor`, respectively. Python does not provide a Self-balancing BST through its library.

---
#### Approach #3 (Buckets) [Accepted]

**Intuition**

Inspired by `bucket sort`, we can achieve linear time complexity in our problem using *buckets* as window.

**Algorithm**

Bucket sort is a sorting algorithm that works by distributing the elements of an array into a number of buckets. Each bucket is then sorted individually, using a different sorting algorithm. Here is an illustration of buckets.

![Illustration of buckets](../Figures/220/220_Buckets.png "Illustration of buckets"){:width="539px"}
{:align="center"}

*Figure 3. Illustration of buckets.*
{:align="center"}

From the above example, we have 8 unsorted integers. We create 5 buckets covering the inclusive ranges of $$[0,9], [10,19], [20, 29], [30, 39], [40, 49]$$ individually. Each of the eight elements is in a particular bucket. For element with value $$x$$, its bucket label is $$x / w$$ and here we have $$w = 10$$. Sort each bucket using some other sorting algorithm and then collect all of them bucket by bucket.

Back to our problem, the critical issue we are trying to solve is:

> 1. For a given element $$x$$ is there an item in the window that is within the range of $$[x-t, x+t]$$?
> 2. Could we do this in constant time?

Let us consider an example where each element is a person's birthday. Your birthday, say some day in *March*, is the new element $$x$$. Suppose that each month has $$30$$ days and you want to know if anyone has a birthday within $$t = 30$$ days of yours. Immediately, we can rule out all other months except *February, March, April*.

The reason we know this is because each birthday belongs to a *bucket* we called *month*! And the range covered by the buckets are the same as distance $$t$$ which simplifies things a lot. Any two elements that are not in the same or adjacent buckets must have a distance greater than $$t$$.

We apply the above bucketing principle and design buckets covering the ranges of $$..., [0,t], [t+1, 2t+1], ...$$. We keep the window using this buckets. Then, each time, all we need to check is the bucket that $$x$$ belongs to and its two adjacent buckets. Thus, we have a constant time algorithm for searching almost duplicate in the window.

One thing worth mentioning is the difference from bucket sort – Each of our buckets contains at most one element at any time, because two elements in a bucket means "almost duplicate" and we can return early from the function. Therefore, a HashMap with an element associated with a bucket label is enough for our purpose.


<iframe src="https://leetcode.com/playground/S8VBJFoK/shared" frameBorder="0" name="S8VBJFoK" width="100%" height="515"></iframe>

**Complexity Analysis**

* Time complexity : $$O(n)$$.
For each of the $$n$$ elements, we do at most three searches, one insert, and one delete on the HashMap, which costs constant time on average. Thus, the entire algorithm costs $$O(n)$$ time.

* Space complexity : $$O(\min(n,k))$$.
Space is dominated by the HashMap, which is linear to the size of its elements. The size of the HashMap is upper bounded by both $$n$$ and $$k$$. Thus the space complexity is $$O(\min(n, k))$$.

## See Also

* [Problem 217 Contains Duplicate](https://leetcode.com/articles/contains-duplicate/)
* [Problem 219 Contains Duplicate II](https://leetcode.com/articles/contains-duplicate-ii/)

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### AC O(N) solution in Java using buckets with explanation
- Author: lx223
- Creation Date: Mon Jun 01 2015 20:08:26 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Oct 27 2018 05:41:10 GMT+0800 (Singapore Standard Time)

<p>
As a followup question, it naturally also requires maintaining a window of size k. When t == 0, it reduces to the previous question so we just reuse the solution.

Since there is now a constraint on the range of the values of the elements to be considered duplicates, it reminds us of doing a range check which is implemented in tree data structure and would take O(LogN) if a balanced tree structure is used, or doing a bucket check which is constant time. We shall just discuss the idea using bucket here.

Bucketing means we map a range of values to the a bucket. For example, if the bucket size is 3, we consider 0, 1, 2 all map to the same bucket. However, if t == 3, (0, 3) is a considered duplicates but does not map to the same bucket. This is fine since we are checking the buckets immediately before and after as well. So, as a rule of thumb, just make sure the size of the bucket is reasonable such that elements having the same bucket is immediately considered duplicates or duplicates must lie within adjacent buckets. So this actually gives us a range of possible bucket size, i.e. t and t + 1. We just choose it to be t and a bucket mapping to be *num / t*.

Another complication is that negative ints are allowed. A simple *num / t* just shrinks everything towards 0. Therefore, we can just reposition every element to start from Integer.MIN_VALUE.

     public class Solution {
        public boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
            if (k < 1 || t < 0) return false;
            Map<Long, Long> map = new HashMap<>();
            for (int i = 0; i < nums.length; i++) {
                long remappedNum = (long) nums[i] - Integer.MIN_VALUE;
                long bucket = remappedNum / ((long) t + 1);
                if (map.containsKey(bucket)
                        || (map.containsKey(bucket - 1) && remappedNum - map.get(bucket - 1) <= t)
                            || (map.containsKey(bucket + 1) && map.get(bucket + 1) - remappedNum <= t))
                                return true;
                if (map.entrySet().size() >= k) {
                    long lastBucket = ((long) nums[i - k] - Integer.MIN_VALUE) / ((long) t + 1);
                    map.remove(lastBucket);
                }
                map.put(bucket, remappedNum);
            }
            return false;
        }
    }

Edits:

Actually, we can use t + 1 as the bucket size to get rid of the case when t == 0. It simplifies the code. The above code is therefore the updated version.
</p>


### Java/Python one pass solution, O(n) time O(n) space using buckets
- Author: dietpepsi
- Creation Date: Tue Oct 20 2015 23:32:21 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Oct 26 2018 03:09:56 GMT+0800 (Singapore Standard Time)

<p>
The idea is like the bucket sort algorithm. Suppose we have consecutive buckets covering the range of nums with each bucket a width of (t+1). If there are two item with difference <= t, one of the two will happen:

    (1) the two in the same bucket
    (2) the two in neighbor buckets

For detailed explanation see my blog [here](http://algobox.org/contains-duplicate-iii/)

**Python**

    def containsNearbyAlmostDuplicate(self, nums, k, t):
        if t < 0: return False
        n = len(nums)
        d = {}
        w = t + 1
        for i in xrange(n):
            m = nums[i] / w
            if m in d:
                return True
            if m - 1 in d and abs(nums[i] - d[m - 1]) < w:
                return True
            if m + 1 in d and abs(nums[i] - d[m + 1]) < w:
                return True
            d[m] = nums[i]
            if i >= k: del d[nums[i - k] / w]
        return False


    # 30 / 30 test cases passed.
    # Status: Accepted
    # Runtime: 56 ms
    # 93.81%


**Java**

    private long getID(long i, long w) {
        return i < 0 ? (i + 1) / w - 1 : i / w;
    }

    public boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
        if (t < 0) return false;
        Map<Long, Long> d = new HashMap<>();
        long w = (long)t + 1;
        for (int i = 0; i < nums.length; ++i) {
            long m = getID(nums[i], w);
            if (d.containsKey(m))
                return true;
            if (d.containsKey(m - 1) && Math.abs(nums[i] - d.get(m - 1)) < w)
                return true;
            if (d.containsKey(m + 1) && Math.abs(nums[i] - d.get(m + 1)) < w)
                return true;
            d.put(m, (long)nums[i]);
            if (i >= k) d.remove(getID(nums[i - k], w));
        }
        return false;
    }
</p>


### Java O(N lg K) solution
- Author: jmnarloch
- Creation Date: Mon Jun 01 2015 16:43:55 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 22 2018 17:56:46 GMT+0800 (Singapore Standard Time)

<p>
This problem requires to maintain a window of size k of the previous values that can be queried for value ranges. The best data structure to do that is Binary Search Tree. As a result maintaining the tree of size k will result in time complexity O(N lg K). In order to check if there exists any value of range abs(nums[i] - nums[j]) to simple queries can be executed both of time complexity O(lg K)

Here is the whole solution using TreeMap.

----------

    public class Solution {
        public boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
            if (nums == null || nums.length == 0 || k <= 0) {
                return false;
            }
    
            final TreeSet<Integer> values = new TreeSet<>();
            for (int ind = 0; ind < nums.length; ind++) {
    
                final Integer floor = values.floor(nums[ind] + t);
                final Integer ceil = values.ceiling(nums[ind] - t);
                if ((floor != null && floor >= nums[ind])
                        || (ceil != null && ceil <= nums[ind])) {
                    return true;
                }
    
                values.add(nums[ind]);
                if (ind >= k) {
                    values.remove(nums[ind - k]);
                }
            }
    
            return false;
        }
    }
</p>


