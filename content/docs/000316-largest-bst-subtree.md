---
title: "Largest BST Subtree"
weight: 316
#id: "largest-bst-subtree"
---
## Description
<div class="description">
<p>Given a binary tree, find the largest subtree which is a Binary Search Tree (BST), where largest means subtree with largest number of nodes in it.</p>

<p><b>Note:</b><br />
A subtree must include all of its descendants.</p>

<p><strong>Example:</strong></p>

<pre>
<strong>Input: </strong>[10,5,15,1,8,null,7]

   10 
   / \ 
<font color="red">  5</font>  15 
<font color="red"> / \</font>   \ 
<font color="red">1   8</font>   7

<strong>Output:</strong> 3
<strong>Explanation: </strong>The Largest BST Subtree in this case is the highlighted one.
             The return value is the subtree&#39;s size, which is 3.
</pre>

<p><b>Follow up:</b><br />
Can you figure out ways to solve it with O(n) time complexity?</p>

</div>

## Tags
- Tree (tree)

## Companies
- Facebook - 7 (taggedByAdmin: false)
- Amazon - 5 (taggedByAdmin: false)
- Bloomberg - 2 (taggedByAdmin: false)
- Google - 3 (taggedByAdmin: false)
- Microsoft - 3 (taggedByAdmin: true)
- Lyft - 3 (taggedByAdmin: false)
- Apple - 2 (taggedByAdmin: false)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### Share my O(n) Java code with brief explanation and comments
- Author: mach7
- Creation Date: Sat Feb 13 2016 04:00:55 GMT+0800 (Singapore Standard Time)
- Update Date: Sat Sep 15 2018 04:53:07 GMT+0800 (Singapore Standard Time)

<p>
> edited code: thanks @hyj143 and @petrichory

    public class Solution {
        
        class Result {  // (size, rangeLower, rangeUpper) -- size of current tree, range of current tree [rangeLower, rangeUpper]
            int size;
            int lower;
            int upper;
            
            Result(int size, int lower, int upper) {
                this.size = size;
                this.lower = lower;
                this.upper = upper;
            }
        }
        
        int max = 0;
        
        public int largestBSTSubtree(TreeNode root) {
            if (root == null) { return 0; }    
            traverse(root);
            return max;
        }
        
        private Result traverse(TreeNode root) {
            if (root == null) { return new Result(0, Integer.MAX_VALUE, Integer.MIN_VALUE); }
            Result left = traverse(root.left);
            Result right = traverse(root.right);
            if (left.size == -1 || right.size == -1 || root.val <= left.upper || root.val >= right.lower) {
                return new Result(-1, 0, 0);
            }
            int size = left.size + 1 + right.size;
            max = Math.max(size, max);
            return new Result(size, Math.min(left.lower, root.val), Math.max(right.upper, root.val));
        }
    }


----------


    /*
        in brute-force solution, we get information in a top-down manner.
        for O(n) solution, we do it in bottom-up manner, meaning we collect information during backtracking. 
    */
    public class Solution {
        
        class Result {  // (size, rangeLower, rangeUpper) -- size of current tree, range of current tree [rangeLower, rangeUpper]
            int size;
            int lower;
            int upper;
            
            Result(int size, int lower, int upper) {
                this.size = size;
                this.lower = lower;
                this.upper = upper;
            }
        }
        
        int max = 0;
        
        public int largestBSTSubtree(TreeNode root) {
            if (root == null) { return 0; }    
            traverse(root, null);
            return max;
        }
        
        private Result traverse(TreeNode root, TreeNode parent) {
            if (root == null) { return new Result(0, parent.val, parent.val); }
            Result left = traverse(root.left, root);
            Result right = traverse(root.right, root);
            if (left.size==-1 || right.size==-1 || root.val<left.upper || root.val>right.lower) {
                return new Result(-1, 0, 0);
            }
            int size = left.size + 1 + right.size;
            max = Math.max(size, max);
            return new Result(size, left.lower, right.upper);
        }
    }
</p>


### Very Short Simple Java O(N) Solution
- Author: Luckman
- Creation Date: Sun Jul 30 2017 15:23:17 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 27 2018 12:44:15 GMT+0800 (Singapore Standard Time)

<p>
```
public class Solution {
    
    // return array for each node: 
    //     [0] --> min
    //     [1] --> max
    //     [2] --> largest BST in its subtree(inclusive)
    
    public int largestBSTSubtree(TreeNode root) {
        int[] ret = largestBST(root);
        return ret[2];
    }
    
    private int[] largestBST(TreeNode node){
        if(node == null){
            return new int[]{Integer.MAX_VALUE, Integer.MIN_VALUE, 0};
        }
        int[] left = largestBST(node.left);
        int[] right = largestBST(node.right);
        if(node.val > left[1] && node.val < right[0]){
            return new int[]{Math.min(node.val, left[0]), Math.max(node.val, right[1]), left[2] + right[2] + 1};
        }else{
            return new int[]{Integer.MIN_VALUE, Integer.MAX_VALUE, Math.max(left[2], right[2])};
        }
    }
}
```
</p>


### Short Python solution
- Author: StefanPochmann
- Creation Date: Fri Feb 12 2016 23:13:15 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Oct 01 2018 04:44:53 GMT+0800 (Singapore Standard Time)

<p>
    def largestBSTSubtree(self, root):
        def dfs(root):
            if not root:
                return 0, 0, float('inf'), float('-inf')
            N1, n1, min1, max1 = dfs(root.left)
            N2, n2, min2, max2 = dfs(root.right)
            n = n1 + 1 + n2 if max1 < root.val < min2 else float('-inf')
            return max(N1, N2, n), n, min(min1, root.val), max(max2, root.val)
        return dfs(root)[0]

My `dfs` returns four values:

- `N` is the size of the largest BST in the tree.
- If the tree is a BST, then `n` is the number of nodes, otherwise it's -infinity.
- If the tree is a BST, then `min` and `max` are the minimum/maximum value in the tree.
</p>


