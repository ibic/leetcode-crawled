---
title: "Monthly Transactions II"
weight: 1548
#id: "monthly-transactions-ii"
---
## Description
<div class="description">
<p>Table: <code>Transactions</code></p>

<pre>
+----------------+---------+
| Column Name    | Type    |
+----------------+---------+
| id             | int     |
| country        | varchar |
| state          | enum    |
| amount         | int     |
| trans_date     | date    |
+----------------+---------+
id is the primary key of this table.
The table has information about incoming transactions.
The state column is an enum of type [&quot;approved&quot;, &quot;declined&quot;].
</pre>

<p>Table: <code>Chargebacks</code></p>

<pre>
+----------------+---------+
| Column Name    | Type    |
+----------------+---------+
| trans_id       | int     |
| charge_date    | date    |
+----------------+---------+
Chargebacks contains basic information regarding incoming chargebacks from some transactions placed in Transactions table.
trans_id is a foreign key to the id column of Transactions table.
Each chargeback corresponds to a transaction made previously even if they were not approved.</pre>

<p>&nbsp;</p>

<p>Write an SQL query to find for each month and country, the number of approved transactions and their total amount, the number of chargebacks and their total amount.</p>

<p><strong>Note</strong>: In your&nbsp;query, given the month and country, ignore&nbsp;rows with all&nbsp;zeros.</p>

<p>The query result format is in the following example:</p>

<pre>
<code>Transactions</code> table:
+------+---------+----------+--------+------------+
| id   | country | state    | amount | trans_date |
+------+---------+----------+--------+------------+
| 101  | US      | approved | 1000   | 2019-05-18 |
| 102  | US      | declined | 2000   | 2019-05-19 |
| 103  | US      | approved | 3000   | 2019-06-10 |
| 104  | US      | approved | 4000   | 2019-06-13 |
| 105  | US      | approved | 5000   | 2019-06-15 |
+------+---------+----------+--------+------------+

<code>Chargebacks</code> table:
+------------+------------+
| trans_id   | trans_date |
+------------+------------+
| 102        | 2019-05-29 |
| 101        | 2019-06-30 |
| 105        | 2019-09-18 |
+------------+------------+

Result table:
+----------+---------+----------------+-----------------+-------------------+--------------------+
| month    | country | approved_count | approved_amount | chargeback_count  | chargeback_amount  |
+----------+---------+----------------+-----------------+-------------------+--------------------+
| 2019-05  | US      | 1              | 1000            | 1                 | 2000               |
| 2019-06  | US      | 3              | 12000           | 1                 | 1000               |
| 2019-09  | US      | 0              | 0               | 1                 | 5000               |
+----------+---------+----------------+-----------------+-------------------+--------------------+
</pre>

</div>

## Tags


## Companies
- Wish - 0 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### simple MySQL solution
- Author: Merciless
- Creation Date: Thu Sep 26 2019 22:25:14 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 26 2019 22:28:45 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT month, country, SUM(CASE WHEN state = "approved" THEN 1 ELSE 0 END) AS approved_count, SUM(CASE WHEN state = "approved" THEN amount ELSE 0 END) AS approved_amount, SUM(CASE WHEN state = "back" THEN 1 ELSE 0 END) AS chargeback_count, SUM(CASE WHEN state = "back" THEN amount ELSE 0 END) AS chargeback_amount
FROM
(
    SELECT LEFT(chargebacks.trans_date, 7) AS month, country, "back" AS state, amount
    FROM chargebacks
    JOIN transactions ON chargebacks.trans_id = transactions.id
    UNION ALL
    SELECT LEFT(trans_date, 7) AS month, country, state, amount
    FROM transactions
    WHERE state = "approved"
) s
GROUP BY month, country
#318 ms
</p>


### MySQL Clean Solution with Detailed Explanation
- Author: sophiesu0827
- Creation Date: Fri Dec 20 2019 05:12:25 GMT+0800 (Singapore Standard Time)
- Update Date: Fri Dec 20 2019 05:22:04 GMT+0800 (Singapore Standard Time)

<p>
```
SELECT 
    DATE_FORMAT(trans_date,"%Y-%m") AS month, country,
    SUM(state=\'approved\') AS approved_count,
    SUM(CASE WHEN state=\'approved\' THEN amount ELSE 0 END) AS approved_amount,
    SUM(state=\'chargeback\') AS chargeback_count,
    SUM(CASE WHEN state=\'chargeback\' THEN amount ELSE 0 END) AS chargeback_amount
FROM
    (SELECT DISTINCT * FROM Transactions
    UNION ALL
    SELECT DISTINCT C.trans_id AS id,T.country,\'chargeback\' AS state, T.amount, C.trans_date FROM Chargebacks C LEFT JOIN Transactions T ON  C.trans_id=T.id ) final
GROUP BY month,country;
```

**Solution explanation:**
Step 1 We want the Chargebacks table look similar to the Transaction table so that we can use union to concatenate all records. At least the fields should be the same. In addition to the chargeback date and transaction id, we will need to get the country and amount information for chargebacks :
![image](https://assets.leetcode.com/users/sophiesu0827/image_1576789659.png)

Step 2 Union all gives us a long table which has all the chargebacks and approvals so that we can calculate counts and amounts based on conditions:
![image](https://assets.leetcode.com/users/sophiesu0827/image_1576789688.png)





</p>


### MySQL solution with explanation: CASE WHEN + UNION ALL  98.23%
- Author: BREEZEYJ
- Creation Date: Sun Mar 08 2020 03:09:24 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Mar 08 2020 03:12:41 GMT+0800 (Singapore Standard Time)

<p>
```
#1.create a temporary table to identity state of \'chargebacks\'
#2.combin ordinary table and temporary table to build a source table for filtering
#3.filtering records based on conditions 

#note: assign records into a column: \'xxx\' AS column_name
#note: compare the difference between \'%m\' and \'%M\' 

SELECT DATE_FORMAT(trans_date, \'%Y-%m\') AS month,
       country,
       SUM(CASE WHEN state = \'approved\' THEN 1 ELSE 0 END) AS approved_count,
       SUM(CASE WHEN state = \'approved\' THEN amount ELSE 0 END) AS approved_amount,
       SUM(CASE WHEN state = \'bc\' THEN 1 ELSE 0 END) AS chargeback_count,
       SUM(CASE WHEN state = \'bc\' THEN amount ELSE 0 END) AS chargeback_amount
FROM
(
    SELECT id, country, \'bc\' AS state, amount, c.trans_date
    FROM Transactions t
    RIGHT JOIN Chargebacks c
    ON t.id = c.trans_id
UNION ALL
    SELECT id, country, state, amount, trans_date 
    FROM Transactions
) tem
GROUP BY country, DATE_FORMAT(trans_date, \'%Y-%m\')
HAVING approved_count + approved_amount + chargeback_count + chargeback_amount <> 0

```
</p>


