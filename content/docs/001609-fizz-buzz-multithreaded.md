---
title: "Fizz Buzz Multithreaded"
weight: 1609
#id: "fizz-buzz-multithreaded"
---
## Description
<div class="description">
<p>Write a program that outputs the string representation of numbers from 1 to&nbsp;<i>n</i>, however:</p>

<ul>
	<li>If the number is divisible by 3, output &quot;fizz&quot;.</li>
	<li>If the number is divisible by 5, output&nbsp;&quot;buzz&quot;.</li>
	<li>If the number is divisible by both 3 and 5, output&nbsp;&quot;fizzbuzz&quot;.</li>
</ul>

<p>For example, for&nbsp;<code>n = 15</code>, we output:&nbsp;<code>1, 2, fizz, 4, buzz, fizz, 7, 8, fizz, buzz, 11, fizz, 13, 14, fizzbuzz</code>.</p>

<p>Suppose you are given the following code:</p>

<pre>
class FizzBuzz {
&nbsp; public FizzBuzz(int n) { ... }&nbsp;              // constructor
  public void fizz(printFizz) { ... }          // only output &quot;fizz&quot;
  public void buzz(printBuzz) { ... }          // only output &quot;buzz&quot;
  public void fizzbuzz(printFizzBuzz) { ... }  // only output &quot;fizzbuzz&quot;
  public void number(printNumber) { ... }      // only output the numbers
}</pre>

<p>Implement a multithreaded version of <code>FizzBuzz</code> with <strong>four</strong> threads. The same instance of <code>FizzBuzz</code> will be passed to four different threads:</p>

<ol>
	<li>Thread A will call&nbsp;<code>fizz()</code>&nbsp;to check for divisibility of 3 and outputs&nbsp;<code>fizz</code>.</li>
	<li>Thread B will call&nbsp;<code>buzz()</code>&nbsp;to check for divisibility of 5 and outputs&nbsp;<code>buzz</code>.</li>
	<li>Thread C will call <code>fizzbuzz()</code>&nbsp;to check for divisibility of 3 and 5 and outputs&nbsp;<code>fizzbuzz</code>.</li>
	<li>Thread D will call <code>number()</code> which should only output the numbers.</li>
</ol>

</div>

## Tags


## Companies


## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python 3] One code each language: Semaphore
- Author: rock
- Creation Date: Sat Sep 21 2019 13:03:16 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 10 2020 15:23:18 GMT+0800 (Singapore Standard Time)

<p>
**Method 1: Semaphore**

Use `number()` method to control the following 4 cases:
1. multiples of 15;
2. multiples of 5;
3. multiples of 3;
4. others.

```java
import java.util.concurrent.*;

class FizzBuzz {
    private int n, counter;
    private Semaphore sem, sem3, sem5, sem15;

    public FizzBuzz(int n) {
        this.n = n;
        sem = new Semaphore(1);
        sem3 = new Semaphore(0);
        sem5 = new Semaphore(0);
        sem15 = new Semaphore(0);
    }

    // printFizz.run() outputs "fizz".
    public void fizz(Runnable printFizz) throws InterruptedException {
        for (int i = 3; i <= n; i += 3) {
            if (i % 5 != 0) { // skip multiples of 15.
                sem3.acquire();
                printFizz.run();
                sem.release();
            }
        }
    }

    // printBuzz.run() outputs "buzz".
    public void buzz(Runnable printBuzz) throws InterruptedException {
        for (int i = 5; i <= n; i += 5) {
            if (i % 3 != 0) { // skip multiples of 15.
                sem5.acquire();
                printBuzz.run();
                sem.release();
            }
        }
    }

    // printFizzBuzz.run() outputs "fizzbuzz".
    public void fizzbuzz(Runnable printFizzBuzz) throws InterruptedException {
        for (int i = 15; i <= n; i += 15) {
            sem15.acquire();
            printFizzBuzz.run();
            sem.release();
        }
    }

    // printNumber.accept(x) outputs "x", where x is an integer.
    public void number(IntConsumer printNumber) throws InterruptedException {
        for (int i = 1; i <= n; ++i) {
            sem.acquire();
            if (i % 15 == 0) {
                sem15.release();
            }else if (i % 5 == 0) {
                sem5.release();
            }else if (i % 3 == 0) {
                sem3.release();
            }else {
                printNumber.accept(i);
                sem.release();
            } 
        }        
    }
}
```
```python
from threading import Semaphore

class FizzBuzz:
    def __init__(self, n: int):
        self.n = n
        self.sem = Semaphore()                   # default is 1
        self.sem3 = Semaphore(0)
        self.sem5 = Semaphore(0)
        self.sem15 = Semaphore(0)

    # printFizz() outputs "fizz"
    def fizz(self, printFizz: \'Callable[[], None]\') -> None:
        for i in range(3, self.n + 1, 3):
            if i % 5 != 0:
                self.sem3.acquire()
                printFizz()
                self.sem.release()
    	

    # printBuzz() outputs "buzz"
    def buzz(self, printBuzz: \'Callable[[], None]\') -> None:
        for i in range(5, self.n + 1, 5):
            if i % 3 != 0:
                self.sem5.acquire()
                printBuzz()
                self.sem.release()
    	

    # printFizzBuzz() outputs "fizzbuzz"
    def fizzbuzz(self, printFizzBuzz: \'Callable[[], None]\') -> None:
        for i in range(15, self.n + 1, 15):
            self.sem15.acquire()
            printFizzBuzz()
            self.sem.release()
		

    # printNumber(x) outputs "x", where x is an integer.
    def number(self, printNumber: \'Callable[[int], None]\') -> None:
        for i in range(1, self.n + 1):
            self.sem.acquire()
            if i % 15 == 0:
                self.sem15.release()
            elif i % 5 == 0:
                self.sem5.release()
            elif i % 3 == 0:   
                self.sem3.release()
            else:
                printNumber(i)     
                self.sem.release()
```

----

</p>


### Java, using synchronized with short explanation.
- Author: Hai_dee
- Creation Date: Thu Sep 19 2019 12:08:01 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 19 2019 12:08:01 GMT+0800 (Singapore Standard Time)

<p>
[Code is at the end]

So I just keep a shared ```currentNumber``` showing where we\'re up to. Each thread has to then check whether or not it should take its turn. If it\'s not the current thread\'s turn, it calls ```wait()```, thus suspending and giving up the monitor. After each increment of ```currentNumber```, we need to call ```notifyAll()``` so that the other threads are woken and can re-check the condition. They will all be given a chance to run 

The implicit locks, (aka monitors) provided by synchronized can be a bit confusing at first to understand, so I recommend reading up on them if you are confused. I personally found these sites useful:
+ https://docs.oracle.com/javase/tutorial/essential/concurrency/index.html
+ https://howtodoinjava.com/java/multi-threading/java-synchronized/

You might notice something slightly unusual in my wait code, i.e.

```
if (currentNumber % 3 != 0 || currentNumber % 5 == 0) {
     wait();
     continue;
}
```

When perhaps you would have expected the more traditional form of:
```
while (currentNumber % 3 != 0 || currentNumber % 5 == 0) {
     wait();
}
```

Seeing as we are always told that waiting conditions must be a loop, right? (Because sometimes it will have to wait more than once for its turn). The reason I have done this though is because otherwise there\'d have to be an additional check in the waiting loop for if the currentNumber value is still below n.
```
while (currentNumber % 3 != 0 || currentNumber % 5 == 0) {
     wait();
	 if (currentNumber > n) return;
}
```

While this does work, I felt it was probably the more convoluted option. But it\'s definitely an option, and I\'d be interested to hear thoughts on this "design" decision. In effect, it is a wait in a loop, it\'s just that it utilises the outer loop\'s condition (by using continue).

I probably could pull some of the repetition into a seperate method. One challenge was that 3 of the methods took a Runnable, and one took an IntConsumer.

```
class FizzBuzz {
    
    private int n;
    private int currentNumber = 1;
    
    public FizzBuzz(int n) {
        this.n = n;
    }

    // printFizz.run() outputs "fizz".
    public synchronized void fizz(Runnable printFizz) throws InterruptedException {
        while (currentNumber <= n) {
            if (currentNumber % 3 != 0 || currentNumber % 5 == 0) {
                wait();
                continue;
            }
            printFizz.run();
            currentNumber += 1;
            notifyAll();
        }
    }

    // printBuzz.run() outputs "buzz".
    public synchronized void buzz(Runnable printBuzz) throws InterruptedException {
        while (currentNumber <= n) {
            if (currentNumber % 5 != 0 || currentNumber % 3 == 0) {
                wait();
                continue;
            }
            printBuzz.run();
            currentNumber += 1;
            notifyAll();
        }
    }

    // printFizzBuzz.run() outputs "fizzbuzz".
    public synchronized void fizzbuzz(Runnable printFizzBuzz) throws InterruptedException {
        while (currentNumber <= n) {
            if (currentNumber % 15 != 0) {
                wait();
                continue;
            }
            printFizzBuzz.run();
            currentNumber += 1;
            notifyAll();
        }
    }

    // printNumber.accept(x) outputs "x", where x is an integer.
    public synchronized void number(IntConsumer printNumber) throws InterruptedException {
        while (currentNumber <= n) {
            if (currentNumber % 3 == 0 || currentNumber % 5 == 0) {
                wait();
                continue;
            }
            printNumber.accept(currentNumber);
            currentNumber += 1;
            notifyAll();
        }
    }
```
</p>


### C++ mutex / condition_variable, please suggest improvements
- Author: grokus
- Creation Date: Mon Sep 23 2019 00:10:36 GMT+0800 (Singapore Standard Time)
- Update Date: Mon Sep 23 2019 00:10:36 GMT+0800 (Singapore Standard Time)

<p>
```
private:
    int n;
    int count;
    mutex m;
    condition_variable cv;

public:
    FizzBuzz(int n) {
        this->n = n;
        this->count = 1;
    }

    void fizz(function<void()> printFizz) {
        while (true) {
            unique_lock<mutex> lock(m);
            while (count <= n && (count % 3 != 0 || count % 5 == 0))
                cv.wait(lock);
            if (count > n) return;
            printFizz();
            ++count;
            cv.notify_all();
        }
    }

    void buzz(function<void()> printBuzz) {
        while (true) {
            unique_lock<mutex> lock(m);
            while (count <= n && (count % 5 != 0 || count % 3 == 0))
                cv.wait(lock);
            if (count > n) return;
            printBuzz();
            ++count;
            cv.notify_all();
        }
    }

	void fizzbuzz(function<void()> printFizzBuzz) {
        while (true) {
            unique_lock<mutex> lock(m);
            while (count <= n && (count % 5 != 0 || count % 3 != 0))
                cv.wait(lock);
            if (count > n) return;
            printFizzBuzz();
            ++count;
            cv.notify_all();
        }
    }

    void number(function<void(int)> printNumber) {
        while (true) {
            unique_lock<mutex> lock(m);
            while (count <= n && (count % 5 == 0 || count % 3 == 0))
                cv.wait(lock);
            if (count > n) return;
            printNumber(count++);
            cv.notify_all();
        }
    }
</p>


