---
title: "Sort the Matrix Diagonally"
weight: 1126
#id: "sort-the-matrix-diagonally"
---
## Description
<div class="description">
<p>Given a <code>m * n</code> matrix <code>mat</code>&nbsp;of integers, sort it diagonally in ascending order from the top-left to the bottom-right then return the sorted array.</p>

<p>&nbsp;</p>
<p><strong>Example 1:</strong></p>
<img alt="" src="https://assets.leetcode.com/uploads/2020/01/21/1482_example_1_2.png" style="width: 500px; height: 198px;" />
<pre>
<strong>Input:</strong> mat = [[3,3,1,1],[2,2,1,2],[1,1,1,2]]
<strong>Output:</strong> [[1,1,1,1],[1,2,2,2],[1,2,3,3]]
</pre>

<p>&nbsp;</p>
<p><strong>Constraints:</strong></p>

<ul>
	<li><code>m ==&nbsp;mat.length</code></li>
	<li><code>n ==&nbsp;mat[i].length</code></li>
	<li><code>1 &lt;= m, n&nbsp;&lt;= 100</code></li>
	<li><code>1 &lt;= mat[i][j] &lt;= 100</code></li>
</ul>
</div>

## Tags
- Array (array)
- Sort (sort)

## Companies
- Databricks - 2 (taggedByAdmin: false)
- Robinhood - 2 (taggedByAdmin: false)
- Quora - 3 (taggedByAdmin: true)

## Official Solution
N.A.

## Accepted Submission (N.A.)
```
N.A.
```

## Top Discussions
### [Java/Python] Straight Forward
- Author: lee215
- Creation Date: Sun Jan 26 2020 00:12:35 GMT+0800 (Singapore Standard Time)
- Update Date: Thu Sep 03 2020 12:25:41 GMT+0800 (Singapore Standard Time)

<p>
## **Explanation**
`A[i][j]` on the same diagonal have same value of `i - j`
For each diagonal,
put its elements together, sort, and set them back.
<br>

## **Complexity**
Time `O(MNlogD)`, where `D` is the length of diagonal with `D = min(M,N)`.
Space `O(MN)`
<br>

**Java**
By @hiepit
```java
    public int[][] diagonalSort(int[][] A) {
        int m = A.length, n = A[0].length;
        HashMap<Integer, PriorityQueue<Integer>> d = new HashMap<>();
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                d.putIfAbsent(i - j, new PriorityQueue<>());
                d.get(i - j).add(A[i][j]);
            }
        }
        for (int i = 0; i < m; ++i)
            for (int j = 0; j < n; ++j)
                A[i][j] = d.get(i - j).poll();
        return A;
    }
```
**C++**
By @chepson
```cpp
    vector<vector<int>> diagonalSort(vector<vector<int>>& mat) {
        int m = mat.size(), n = mat[0].size();
        // all elements on same diagonal have same i-j result.
        unordered_map<int, priority_queue<int, vector<int>, greater<int>>> map; // min priority queue
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                map[i - j].push(mat[i][j]);
            }
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                mat[i][j] = map[i - j].top(); map[i - j].pop();
            }
        }
        return mat;
    }
```
**Python:**
```python
    def diagonalSort(self, A):
        n, m = len(A), len(A[0])
        d = collections.defaultdict(list)
        for i in xrange(n):
            for j in xrange(m):
                d[i - j].append(A[i][j])
        for k in d:
            d[k].sort(reverse=1)
        for i in xrange(n):
            for j in xrange(m):
                A[i][j] = d[i - j].pop()
        return A
```

</p>


### anybody has difficulty in understanding problem like me -__-
- Author: hwei002
- Creation Date: Sun Jan 26 2020 00:07:12 GMT+0800 (Singapore Standard Time)
- Update Date: Tue Apr 14 2020 23:39:31 GMT+0800 (Singapore Standard Time)

<p>
feel frustrated since I thought we are required to sort ALL the numbers and fill it back...ORZ
</p>


### C++ elegant map solution easy to understand
- Author: ritesh14
- Creation Date: Sun Jan 26 2020 00:22:53 GMT+0800 (Singapore Standard Time)
- Update Date: Sun Jan 26 2020 00:22:53 GMT+0800 (Singapore Standard Time)

<p>

    unordered_map<int, vector<int>> mp;
    vector<vector<int>> diagonalSort(vector<vector<int>>& mat) {
        int m=mat.size();
        int n=mat[0].size();
        
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                mp[i-j].push_back(mat[i][j]);
            }
        }
        
        for(int k=-(n-1);k<m;k++) {
            sort(mp[k].begin(),mp[k].end());
        }
        
        for(int i=m-1;i>=0;i--) {
            for(int j=n-1;j>=0;j--) {
                mat[i][j]=mp[i-j].back();
                mp[i-j].pop_back();
            }
        }
        return mat;
    }
</p>


